package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_INSERT_INTO(tok *ast.Token_stmt_INSERT_INTO) {
	var (
		insertion_row rsql.Row
		list_of_rows  []rsql.Row
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_INSERT_INTO)

	switch tok.Ins_instruction_code {
	case rsql.INSTR_STMT_INSERT_VALUES:
		for _, values := range tok.Ins_values_array { // for each VALUES clause

			// copy base table row

			insertion_row = make(rsql.Row, len(tok.Ins_base_row))
			copy(insertion_row, tok.Ins_base_row) // insertion_row contains here default or NULL values

			// fill in with columns to insert

			for k, tok_col := range values {
				bbk.Instr_walk_Token_primary(tok_col) // emit instr for each column of VALUES

				seq_no := tok.Ins_listcol_base_seqno[k]
				insertion_row[seq_no] = tok_col.Prim_dataslot // default or NULL values in insertion_row are overridden
			}

			list_of_rows = append(list_of_rows, insertion_row)
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_INSERT_VALUES, tok.Ins_table_qname, tok.Ins_gtabledef, list_of_rows, tok.Ins_explicit_IDENTITY_provided)

		return

	case rsql.INSTR_STMT_INSERT_SELECT:
		instr_walk_xtable(tok.Ins_xtable)

		bbk2 := (*BBlock)(tok.Ins_conversion_basicblock)

		for _, conversion_token := range tok.Ins_conversion_tokens {
			switch conversion_token.Prim_type {
			case ast.TOK_PRIM_CAST:
				bbk2.instr_emit3ac(conversion_token.Batch_line(), conversion_token.Prim_instruction_code, conversion_token, conversion_token.Prim_left)

			case ast.TOK_PRIM_SYSFUNC: // INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE/TIME/DATETIME
				switch conversion_token.Prim_instruction_code {
				case rsql.INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY,
					rsql.INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR:
					bbk2.instr_emit3ac_result_listexpr_flat(conversion_token.Batch_line(), conversion_token.Prim_instruction_code, conversion_token, conversion_token.Prim_listexpr)

				default:
					rsql.Assert(conversion_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE ||
						conversion_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_TIME ||
						conversion_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME ||
						conversion_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_DATE_TO_VARCHAR ||
						conversion_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_TIME_TO_VARCHAR ||
						conversion_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_DATETIME_TO_VARCHAR)

					sysoperand := conversion_token.Prim_syslanguage
					bbk2.instr_emit3ac_result_listexpr_sysoperand_flat(conversion_token.Batch_line(), conversion_token.Prim_instruction_code, conversion_token, conversion_token.Prim_listexpr, sysoperand)
				}

			default:
				panic("impossible")
			}
		}

		params := rsql.Insert_select_params{
			Ins_cursor:                     tok.Ins_xtable.Cursor(),
			Ins_conversion_basicblock:      tok.Ins_conversion_basicblock,
			Ins_base_row:                   tok.Ins_base_row,
			Ins_explicit_IDENTITY_provided: tok.Ins_explicit_IDENTITY_provided,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_INSERT_SELECT, tok.Ins_table_qname, tok.Ins_gtabledef, params)

		return

	default:
		panic("impossible")
	}
}
