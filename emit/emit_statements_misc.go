package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_USE(tok *ast.Token_stmt_USE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_USE)

	vars_OUT := []rsql.IDataslot{tok.Use_database_name_OUT.Prim_dataslot, tok.Use_database_dbid_OUT.Prim_dataslot, tok.Use_schema_name_OUT.Prim_dataslot, tok.Use_schema_schid_OUT.Prim_dataslot, tok.Use_user_name_OUT.Prim_dataslot, tok.Use_user_palid_OUT.Prim_dataslot}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_USE, tok.Use_database_name, vars_OUT)
}

func (bbk *BBlock) instr_walk_Token_stmt_ASSIGNMENT(tok *ast.Token_stmt_ASSIGNMENT) {

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_ASSIGNMENT)

	// instructions for right expression evaluation

	bbk.Instr_walk_Token_primary(tok.Set_rvalue)

	// assignment instruction to the dataslot of the variable.
	//   This value is not used in the basic block, but is used to convey the value of the variable to the next basic block.
	//   When an assignment is encountered :
	//       - decorator stage: the Symbol in symboltable of the variable points to the result token of the right expression, for use inside the basic block.
	//       - VM stage:        the dataslot of the variable receives the value of the right expression, for use outside the basic block.

	bbk.instr_emit3ac(tok.Batch_line(), tok.Set_instruction_code, tok.Set_lvalue, tok.Set_rvalue) // assignment instruction
}

func (bbk *BBlock) instr_walk_Token_stmt_THROW(tok *ast.Token_stmt_THROW) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_THROW)

	bbk.Instr_walk_Token_primary(tok.Th_error_number)
	bbk.Instr_walk_Token_primary(tok.Th_message)
	bbk.Instr_walk_Token_primary(tok.Th_severity)
	bbk.Instr_walk_Token_primary(tok.Th_state)

	bbk.instr_emit3ac(tok.Batch_line(), rsql.INSTR_STMT_THROW, tok.Th_error_number, tok.Th_message, tok.Th_severity, tok.Th_state)
}

func (bbk *BBlock) instr_walk_Token_stmt_PRINT(tok *ast.Token_stmt_PRINT) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_PRINT)

	for _, tok_arg := range tok.Pr_listexpr {
		bbk.Instr_walk_Token_primary(tok_arg)
	}

	bbk.instr_emit3ac_listexpr_IDataslots(tok.Batch_line(), rsql.INSTR_STMT_PRINT, tok.Pr_listexpr)
}

func (bbk *BBlock) instr_walk_Token_stmt_BEGIN_TRANSACTION(tok *ast.Token_stmt_BEGIN_TRANSACTION) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_BEGIN_TRANSACTION)

	bbk.instr_emit3ac(tok.Batch_line(), rsql.INSTR_STMT_BEGIN_TRANSACTION)
}

func (bbk *BBlock) instr_walk_Token_stmt_COMMIT_TRANSACTION(tok *ast.Token_stmt_COMMIT_TRANSACTION) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_COMMIT_TRANSACTION)

	bbk.instr_emit3ac(tok.Batch_line(), rsql.INSTR_STMT_COMMIT_TRANSACTION)
}

func (bbk *BBlock) instr_walk_Token_stmt_ROLLBACK_TRANSACTION(tok *ast.Token_stmt_ROLLBACK_TRANSACTION) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ROLLBACK_TRANSACTION)

	bbk.instr_emit3ac(tok.Batch_line(), rsql.INSTR_STMT_ROLLBACK_TRANSACTION)
}

func (bbk *BBlock) instr_walk_Token_stmt_ASSERT_(tok *ast.Token_stmt_ASSERT_) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ASSERT_)

	bbk.Instr_walk_Token_primary(tok.As_expr)

	bbk.instr_emit3ac(tok.Batch_line(), rsql.INSTR_STMT_ASSERT_, tok.As_expr)
}

func (bbk *BBlock) instr_walk_Token_stmt_ASSERT_NULL_(tok *ast.Token_stmt_ASSERT_NULL_) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ASSERT_NULL_)

	bbk.Instr_walk_Token_primary(tok.As_expr)

	bbk.instr_emit3ac(tok.Batch_line(), rsql.INSTR_STMT_ASSERT_NULL_, tok.As_expr)
}

func (bbk *BBlock) instr_walk_Token_stmt_ASSERT_ERROR_(tok *ast.Token_stmt_ASSERT_ERROR_) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ASSERT_ERROR_)

	bbk.Instr_walk_Token_primary(tok.As_expr)

	bbk.Instr_walk_Token_primary(tok.As_error_suffix)

	bbk.instr_emit3ac(tok.Batch_line(), rsql.INSTR_STMT_ASSERT_ERROR_, tok.As_expr, tok.As_error_suffix)
}

func (bbk *BBlock) instr_walk_Token_stmt_SET_NOCOUNT(tok *ast.Token_stmt_SET_NOCOUNT) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_SET_NOCOUNT)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SET_NOCOUNT, tok.Sc_nocount)
}
