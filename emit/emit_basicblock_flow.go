package emit

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
)

// BBlock is just an alias for rsql.Basicblock type.
// It is used only in the package "rsql/emit".
//
// We use it instead of rsql.Basicblock, because we want to define methods with basic block as receiver.
// As rsql.Basicblock is defined in another package, BBlock is an alias that allows us to define such methods in this package.
// BBlock is not used outside this package.
//
type BBlock rsql.Basicblock

func (bbk *BBlock) instr_walk_Token_stmt_IF(tok *ast.Token_stmt_IF) (bbk_last *BBlock) {
	var (
		bb_pre        *BBlock
		bb_then_start *BBlock
		bb_then_end   *BBlock
		bb_else_start *BBlock
		bb_else_end   *BBlock
		bb_post       *BBlock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_IF)

	// retrieve basicblocks

	bb_pre = (*BBlock)(tok.If_basicblock_pre)
	bb_then_start = (*BBlock)(tok.If_basicblock_then_start)
	bb_else_start = (*BBlock)(tok.If_basicblock_else_start)
	bb_post = (*BBlock)(tok.If_basicblock_post)

	// generate intructions for condition

	rsql.Assert(bbk == bb_pre)

	bb_pre.Instr_walk_Token_primary(tok.If_cond)

	// generate intructions for then

	rsql.Assert(tok.If_then != nil)

	if tok.If_else == nil { // IF ... THEN ...
		bb_pre.instr_emit3ac_jump_out_true_false(tok.Batch_line(), tok.If_cond.Prim_dataslot.(*data.BOOLEAN), bb_then_start, bb_post)

		// process basicblock for THEN

		bb_then_end = bb_then_start.instr_walk_sequence_of_Token(tok.If_then)
		rsql.Assert(bb_then_end == (*BBlock)(tok.If_basicblock_then_end))

		bb_then_end.instr_emit3ac_jump_out(tok.Batch_line(), bb_post)

	} else { // IF ... THEN ... ELSE ...
		bb_pre.instr_emit3ac_jump_out_true_false(tok.Batch_line(), tok.If_cond.Prim_dataslot.(*data.BOOLEAN), bb_then_start, bb_else_start)

		// process basicblock for THEN

		bb_then_end = bb_then_start.instr_walk_sequence_of_Token(tok.If_then)
		rsql.Assert(bb_then_end == (*BBlock)(tok.If_basicblock_then_end))

		bb_then_end.instr_emit3ac_jump_out(tok.Batch_line(), bb_post)

		// process basicblock for ELSE

		bb_else_end = bb_else_start.instr_walk_sequence_of_Token(tok.If_else)
		rsql.Assert(bb_else_end == (*BBlock)(tok.If_basicblock_else_end))

		bb_else_end.instr_emit3ac_jump_out(tok.Batch_line(), bb_post)
	}

	// basicblock post

	return bb_post
}

func (bbk *BBlock) instr_walk_Token_stmt_WHILE(tok *ast.Token_stmt_WHILE) (bbk_last *BBlock) {
	var (
		bb_pre        *BBlock
		bb_cond_start *BBlock
		bb_while_end  *BBlock
		bb_post       *BBlock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_WHILE)

	// retrieve basicblocks

	bb_pre = (*BBlock)(tok.While_basicblock_pre)
	bb_cond_start = (*BBlock)(tok.While_basicblock_cond_start)
	bb_post = (*BBlock)(tok.While_basicblock_post)

	// generate intructions for jump    basicblock_pre ---> basicblock_while_start

	rsql.Assert(bbk == bb_pre)

	bb_pre.instr_emit3ac_jump_out(tok.Batch_line(), bb_cond_start)

	// generate intructions for condition

	bb_cond_start.Instr_walk_Token_primary(tok.While_cond) //  process basicblock for condition

	bb_cond_start.instr_emit3ac_jump_if_NULL_or_false(tok.Batch_line(), tok.While_cond.Prim_dataslot.(*data.BOOLEAN), bb_post) // if condition is NULL or false, jump to bb_post to exit the loop

	// generate intructions for while body in same basicblock as condition

	rsql.Assert(tok.While_body != nil)

	bb_while_end = bb_cond_start.instr_walk_sequence_of_Token(tok.While_body)
	rsql.Assert(bb_while_end == (*BBlock)(tok.While_basicblock_while_end))

	bb_while_end.instr_emit3ac_jump_out(tok.Batch_line(), bb_cond_start) // at end of body, always jump to start of WHILE

	// basicblock post

	return bb_post
}

func (bbk *BBlock) instr_walk_Token_stmt_BREAK(tok *ast.Token_stmt_BREAK) {
	var (
		bb_post *BBlock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_BREAK)
	rsql.Assert(tok.Br_attached_loop != nil)

	bb_post = (*BBlock)(tok.Br_attached_loop.While_basicblock_post)

	bbk.instr_emit3ac_jump_out(tok.Batch_line(), bb_post) // break is just a jump instruction
}

func (bbk *BBlock) instr_walk_Token_stmt_CONTINUE(tok *ast.Token_stmt_CONTINUE) {
	var (
		bb_cond_start *BBlock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_CONTINUE)
	rsql.Assert(tok.Cont_attached_loop != nil)

	bb_cond_start = (*BBlock)(tok.Cont_attached_loop.While_basicblock_cond_start)

	bbk.instr_emit3ac_jump_out(tok.Batch_line(), bb_cond_start) // continue is just a jump instruction
}

func (bbk *BBlock) instr_walk_Token_stmt_RETURN(tok *ast.Token_stmt_RETURN) {

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_RETURN)
	rsql.Assert(tok.Ret_expression != nil)

	bbk.Instr_walk_Token_primary(tok.Ret_expression) // intructions for return value evaluation

	bbk.instr_emit3ac_jump_RETURN(tok.Batch_line(), tok.Ret_expression.Prim_dataslot.(*data.INT))
}

func (bbk *BBlock) instr_walk_Token_stmt_GOTO(tok *ast.Token_stmt_GOTO) {
	var (
		rsql_err       *rsql.Error
		tok_stmt_label *ast.Token_stmt_LABEL
		bb_label       *BBlock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_GOTO)

	// get label token

	all_labels := tok.Gt_all_labels // dictionary of all labels in the batch

	if tok_stmt_label, rsql_err = all_labels.Get(tok.Gt_label); rsql_err != nil { // panic if label not found
		panic("label doesn't exist")
	}

	// emit jump to label basicblock

	bb_label = (*BBlock)(tok_stmt_label.Lab_basicblock_label)

	bbk.instr_emit3ac_jump_out(tok.Batch_line(), bb_label) // goto is just a jump instruction
}

func (bbk *BBlock) instr_walk_Token_stmt_LABEL(tok *ast.Token_stmt_LABEL) (bbk_last *BBlock) {
	var (
		bb_pre   *BBlock
		bb_label *BBlock
	)

	// retrieve basicblocks

	bb_pre = (*BBlock)(tok.Lab_basicblock_pre)
	bb_label = (*BBlock)(tok.Lab_basicblock_label)

	// generate intructions for jump    bb_pre ---> bb_label

	rsql.Assert(bbk == bb_pre)

	bbk.instr_emit3ac_jump_out(tok.Batch_line(), bb_label)

	// basicblock label

	return bb_label // change basicblock
}
