package emit

import (
	"rsql"
	"rsql/ast"
)

// instr_walk_sequence_of_Token walks a sequence of tokens.
//
// If IF or WHILE statements or a label are encountered, other basic block will be processed.
// A pointer to the last processed basicblock is returned.
//
func (bbk *BBlock) instr_walk_sequence_of_Token(tok ast.Tokener) (bbk_last *BBlock) {

	for tok != nil { // for each token in a chain of tokens
		switch tok.Category() {
		case ast.TOK_STMT_DECLARE_VARIABLE:
			// pass

		case ast.TOK_STMT_ASSIGNMENT:
			bbk.instr_walk_Token_stmt_ASSIGNMENT(tok.(*ast.Token_stmt_ASSIGNMENT))

		case ast.TOK_STMT_THROW:
			bbk.instr_walk_Token_stmt_THROW(tok.(*ast.Token_stmt_THROW))

		case ast.TOK_STMT_PRINT:
			bbk.instr_walk_Token_stmt_PRINT(tok.(*ast.Token_stmt_PRINT))

		case ast.TOK_STMT_BEGIN_TRANSACTION:
			bbk.instr_walk_Token_stmt_BEGIN_TRANSACTION(tok.(*ast.Token_stmt_BEGIN_TRANSACTION))

		case ast.TOK_STMT_COMMIT_TRANSACTION:
			bbk.instr_walk_Token_stmt_COMMIT_TRANSACTION(tok.(*ast.Token_stmt_COMMIT_TRANSACTION))

		case ast.TOK_STMT_ROLLBACK_TRANSACTION:
			bbk.instr_walk_Token_stmt_ROLLBACK_TRANSACTION(tok.(*ast.Token_stmt_ROLLBACK_TRANSACTION))

		case ast.TOK_STMT_ASSERT_:
			bbk.instr_walk_Token_stmt_ASSERT_(tok.(*ast.Token_stmt_ASSERT_))

		case ast.TOK_STMT_ASSERT_NULL_:
			bbk.instr_walk_Token_stmt_ASSERT_NULL_(tok.(*ast.Token_stmt_ASSERT_NULL_))

		case ast.TOK_STMT_ASSERT_ERROR_:
			bbk.instr_walk_Token_stmt_ASSERT_ERROR_(tok.(*ast.Token_stmt_ASSERT_ERROR_))

		case ast.TOK_STMT_USE:
			bbk.instr_walk_Token_stmt_USE(tok.(*ast.Token_stmt_USE))

		case ast.TOK_STMT_IF:
			bbk = bbk.instr_walk_Token_stmt_IF(tok.(*ast.Token_stmt_IF)) // control flow statement, changes current basicblock

		case ast.TOK_STMT_WHILE:
			bbk = bbk.instr_walk_Token_stmt_WHILE(tok.(*ast.Token_stmt_WHILE)) // control flow statement, changes current basicblock

		case ast.TOK_STMT_BREAK:
			bbk.instr_walk_Token_stmt_BREAK(tok.(*ast.Token_stmt_BREAK)) // control flow statement

		case ast.TOK_STMT_CONTINUE:
			bbk.instr_walk_Token_stmt_CONTINUE(tok.(*ast.Token_stmt_CONTINUE)) // control flow statement

		case ast.TOK_STMT_RETURN:
			bbk.instr_walk_Token_stmt_RETURN(tok.(*ast.Token_stmt_RETURN)) // control flow statement

		case ast.TOK_STMT_GOTO:
			bbk.instr_walk_Token_stmt_GOTO(tok.(*ast.Token_stmt_GOTO)) // control flow statement

		case ast.TOK_STMT_LABEL:
			bbk = bbk.instr_walk_Token_stmt_LABEL(tok.(*ast.Token_stmt_LABEL)) // control flow statement, changes current basicblock

		case ast.TOK_STMT_SET_NOCOUNT:
			bbk.instr_walk_Token_stmt_SET_NOCOUNT(tok.(*ast.Token_stmt_SET_NOCOUNT))

		case ast.TOK_STMT_SET_LEXER_OR_PARSER_OPTION: // lexer or parser pragma
			// pass

		case ast.TOK_STMT_ALTER_SERVER_PARAMETER:
			bbk.instr_walk_Token_stmt_ALTER_SERVER_PARAMETER(tok.(*ast.Token_stmt_ALTER_SERVER_PARAMETER))

		case ast.TOK_STMT_CREATE_LOGIN:
			bbk.instr_walk_Token_stmt_CREATE_LOGIN(tok.(*ast.Token_stmt_CREATE_LOGIN))

		case ast.TOK_STMT_ALTER_LOGIN:
			bbk.instr_walk_Token_stmt_ALTER_LOGIN(tok.(*ast.Token_stmt_ALTER_LOGIN))

		case ast.TOK_STMT_DROP_LOGIN:
			bbk.instr_walk_Token_stmt_DROP_LOGIN(tok.(*ast.Token_stmt_DROP_LOGIN))

		case ast.TOK_STMT_CREATE_DATABASE:
			bbk.instr_walk_Token_stmt_CREATE_DATABASE(tok.(*ast.Token_stmt_CREATE_DATABASE))

		case ast.TOK_STMT_ALTER_DATABASE:
			bbk.instr_walk_Token_stmt_ALTER_DATABASE(tok.(*ast.Token_stmt_ALTER_DATABASE))

		case ast.TOK_STMT_DROP_DATABASE:
			bbk.instr_walk_Token_stmt_DROP_DATABASE(tok.(*ast.Token_stmt_DROP_DATABASE))

		case ast.TOK_STMT_CREATE_USER:
			bbk.instr_walk_Token_stmt_CREATE_USER(tok.(*ast.Token_stmt_CREATE_USER))

		case ast.TOK_STMT_ALTER_USER:
			bbk.instr_walk_Token_stmt_ALTER_USER(tok.(*ast.Token_stmt_ALTER_USER))

		case ast.TOK_STMT_DROP_USER:
			bbk.instr_walk_Token_stmt_DROP_USER(tok.(*ast.Token_stmt_DROP_USER))

		case ast.TOK_STMT_CREATE_ROLE:
			bbk.instr_walk_Token_stmt_CREATE_ROLE(tok.(*ast.Token_stmt_CREATE_ROLE))

		case ast.TOK_STMT_ALTER_ROLE:
			bbk.instr_walk_Token_stmt_ALTER_ROLE(tok.(*ast.Token_stmt_ALTER_ROLE))

		case ast.TOK_STMT_DROP_ROLE:
			bbk.instr_walk_Token_stmt_DROP_ROLE(tok.(*ast.Token_stmt_DROP_ROLE))

		case ast.TOK_STMT_GRANT:
			bbk.instr_walk_Token_stmt_GRANT(tok.(*ast.Token_stmt_GRANT))

		case ast.TOK_STMT_DENY:
			bbk.instr_walk_Token_stmt_DENY(tok.(*ast.Token_stmt_DENY))

		case ast.TOK_STMT_REVOKE:
			bbk.instr_walk_Token_stmt_REVOKE(tok.(*ast.Token_stmt_REVOKE))

		case ast.TOK_STMT_ALTER_AUTHORIZATION:
			bbk.instr_walk_Token_stmt_ALTER_AUTHORIZATION(tok.(*ast.Token_stmt_ALTER_AUTHORIZATION))

		case ast.TOK_STMT_CREATE_TABLE:
			bbk.instr_walk_Token_stmt_CREATE_TABLE(tok.(*ast.Token_stmt_CREATE_TABLE))

		case ast.TOK_STMT_ALTER_TABLE:
			bbk.instr_walk_Token_stmt_ALTER_TABLE(tok.(*ast.Token_stmt_ALTER_TABLE))

		case ast.TOK_STMT_DROP_TABLE:
			bbk.instr_walk_Token_stmt_DROP_TABLE(tok.(*ast.Token_stmt_DROP_TABLE))

		case ast.TOK_STMT_CREATE_INDEX:
			bbk.instr_walk_Token_stmt_CREATE_INDEX(tok.(*ast.Token_stmt_CREATE_INDEX))

		case ast.TOK_STMT_ALTER_INDEX:
			bbk.instr_walk_Token_stmt_ALTER_INDEX(tok.(*ast.Token_stmt_ALTER_INDEX))

		case ast.TOK_STMT_DROP_INDEX:
			bbk.instr_walk_Token_stmt_DROP_INDEX(tok.(*ast.Token_stmt_DROP_INDEX))

		case ast.TOK_STMT_INSERT_INTO:
			bbk.instr_walk_Token_stmt_INSERT_INTO(tok.(*ast.Token_stmt_INSERT_INTO))

		case ast.TOK_STMT_BULK_INSERT:
			bbk.instr_walk_Token_stmt_BULK_INSERT(tok.(*ast.Token_stmt_BULK_INSERT))

		case ast.TOK_STMT_BULK_EXPORT:
			bbk.instr_walk_Token_stmt_BULK_EXPORT(tok.(*ast.Token_stmt_BULK_EXPORT))

		case ast.TOK_STMT_SELECT_OR_UNION:
			bbk.instr_walk_Token_stmt_SELECT_or_UNION(tok.(*ast.Token_stmt_SELECT_or_UNION))

		case ast.TOK_STMT_DELETE:
			bbk.instr_walk_Token_stmt_DELETE(tok.(*ast.Token_stmt_DELETE))

		case ast.TOK_STMT_UPDATE:
			bbk.instr_walk_Token_stmt_UPDATE(tok.(*ast.Token_stmt_UPDATE))

		case ast.TOK_STMT_TRUNCATE_TABLE:
			bbk.instr_walk_Token_stmt_TRUNCATE_TABLE(tok.(*ast.Token_stmt_TRUNCATE_TABLE))

		case ast.TOK_STMT_SHRINK_TABLE:
			bbk.instr_walk_Token_stmt_SHRINK_TABLE(tok.(*ast.Token_stmt_SHRINK_TABLE))

		case ast.TOK_STMT_SHOW_LOCKS:
			bbk.instr_walk_Token_stmt_SHOW_LOCKS(tok.(*ast.Token_stmt_SHOW_LOCKS))

		case ast.TOK_STMT_SHOW_WORKERS:
			bbk.instr_walk_Token_stmt_SHOW_WORKERS(tok.(*ast.Token_stmt_SHOW_WORKERS))

		case ast.TOK_STMT_SHOW_INFO:
			bbk.instr_walk_Token_stmt_SHOW_INFO(tok.(*ast.Token_stmt_SHOW_INFO))

		case ast.TOK_STMT_SHOW_COLLATIONS:
			bbk.instr_walk_Token_stmt_SHOW_COLLATIONS(tok.(*ast.Token_stmt_SHOW_COLLATIONS))

		case ast.TOK_STMT_SHOW_LANGUAGES:
			bbk.instr_walk_Token_stmt_SHOW_LANGUAGES(tok.(*ast.Token_stmt_SHOW_LANGUAGES))

		case ast.TOK_STMT_SHOW:
			bbk.instr_walk_Token_stmt_SHOW(tok.(*ast.Token_stmt_SHOW))

		case ast.TOK_STMT_SLEEP:
			bbk.instr_walk_Token_stmt_SLEEP(tok.(*ast.Token_stmt_SLEEP))

		case ast.TOK_STMT_SHUTDOWN:
			bbk.instr_walk_Token_stmt_SHUTDOWN(tok.(*ast.Token_stmt_SHUTDOWN))

		case ast.TOK_STMT_BACKUP:
			bbk.instr_walk_Token_stmt_BACKUP(tok.(*ast.Token_stmt_BACKUP))

		case ast.TOK_STMT_RESTORE:
			bbk.instr_walk_Token_stmt_RESTORE(tok.(*ast.Token_stmt_RESTORE))

		case ast.TOK_STMT_DUMP:
			bbk.instr_walk_Token_stmt_DUMP(tok.(*ast.Token_stmt_DUMP))

		default:
			panic("inst walk unknown tok category")
		}

		tok = tok.Next()

	} // end loop

	return bbk
}

// Instr_walk_all_AST generates instructions for all decorated ast tree.
//
func Instr_walk_all_AST(LABELS *ast.Dictionary_LABEL, tok_ast_anchor ast.Tokener, basicblock_first *rsql.Basicblock) {
	var bbk *BBlock

	bbk = (*BBlock)(basicblock_first) // rsql.Basicblock type is just aliased as BBlock type

	_ = bbk.instr_walk_sequence_of_Token(tok_ast_anchor) // generate instructions, starting from AST anchor token. We don't need the returned pointer to last basic block, and discard it.
}
