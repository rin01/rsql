package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_SLEEP(tok *ast.Token_stmt_SLEEP) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SLEEP)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SLEEP, tok.Sl_duration)
}
