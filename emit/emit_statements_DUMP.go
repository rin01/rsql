package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_DUMP(tok *ast.Token_stmt_DUMP) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_DUMP)

	switch tok.Dp_instruction_code {
	case rsql.INSTR_STMT_DUMP_PARAMETERS:
		params := rsql.Dump_params{
			Dp_filename: tok.Dp_filename,

			Dp_server_default_collation: tok.Dp_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DUMP_PARAMETERS, params)

	case rsql.INSTR_STMT_DUMP_LOGINS:
		params := rsql.Dump_params{
			Dp_filename: tok.Dp_filename,

			Dp_server_default_collation: tok.Dp_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DUMP_LOGINS, params)

	case rsql.INSTR_STMT_DUMP_DATABASE:
		params := rsql.Dump_params{
			Dp_database_name:   tok.Dp_database_name,
			Dp_option_DDL_ONLY: tok.Dp_option_DDL_ONLY,
			Dp_filename:        tok.Dp_filename,

			Dp_server_default_collation: tok.Dp_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DUMP_DATABASE, params)

	default:
		panic("impossible")
	}
}
