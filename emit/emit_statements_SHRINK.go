package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_SHRINK_TABLE(tok *ast.Token_stmt_SHRINK_TABLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_SHRINK_TABLE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHRINK_TABLE, tok.Sk_table_qname, tok.Sk_gtabledef)
}
