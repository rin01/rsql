package emit

import (
	"rsql"
	"rsql/ast"
	"rsql/lex"
)

// instr_emit3ac_any_type appends an instruction to the bytecode list of the current basic block.
// Up to 5 arguments can be passed.
//
//       This function is used to emit code for statement with constant arguments (strings, integers, etc), e.g. "SET SERVER_GLOBAL_PAGE_CACHE_MEMORY 1000000", "CREATE LOGIN", etc.
//       It is also possible to pass *Token_primary arguments.
//
func (bbk *BBlock) instr_emit3ac_any_type(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, args ...interface{}) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	args_count := len(args)

	switch args_count {
	case 0:
		// pass
	case 1:
		instr3ac.Arg0 = args[0]
	case 2:
		instr3ac.Arg0 = args[0]
		instr3ac.Arg1 = args[1]
	case 3:
		instr3ac.Arg0 = args[0]
		instr3ac.Arg1 = args[1]
		instr3ac.Arg2 = args[2]
	case 4:
		instr3ac.Arg0 = args[0]
		instr3ac.Arg1 = args[1]
		instr3ac.Arg2 = args[2]
		instr3ac.Arg3 = args[3]
	case 5:
		instr3ac.Arg0 = args[0]
		instr3ac.Arg1 = args[1]
		instr3ac.Arg2 = args[2]
		instr3ac.Arg3 = args[3]
		instr3ac.Arg4 = args[4]
	default:
		panic("too many arguments in instr")
	}

	// if *Token_primary has been passed as argument, replace it with its dataslot

	if instr3ac.Arg0 != nil {
		if token_primary, ok := instr3ac.Arg0.(*ast.Token_primary); ok == true {
			instr3ac.Arg0 = token_primary.Prim_dataslot
		}
	}

	if instr3ac.Arg1 != nil {
		if token_primary, ok := instr3ac.Arg1.(*ast.Token_primary); ok == true {
			instr3ac.Arg1 = token_primary.Prim_dataslot
		}
	}

	if instr3ac.Arg2 != nil {
		if token_primary, ok := instr3ac.Arg2.(*ast.Token_primary); ok == true {
			instr3ac.Arg2 = token_primary.Prim_dataslot
		}
	}

	if instr3ac.Arg3 != nil {
		if token_primary, ok := instr3ac.Arg3.(*ast.Token_primary); ok == true {
			instr3ac.Arg3 = token_primary.Prim_dataslot
		}
	}

	if instr3ac.Arg4 != nil {
		if token_primary, ok := instr3ac.Arg4.(*ast.Token_primary); ok == true {
			instr3ac.Arg4 = token_primary.Prim_dataslot
		}
	}

	// append instruction

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac appends an instruction to the bytecode list of the current basic block.
// Up to 5 arguments can be passed.
//
//       This function is used mostly to emit code for operators, or statements (e.g. PRINT).
//
func (bbk *BBlock) instr_emit3ac(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, args ...*ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	args_count := len(args)

	switch args_count {
	case 0:
		// pass
	case 1:
		instr3ac.Arg0 = args[0].Prim_dataslot
	case 2:
		instr3ac.Arg0 = args[0].Prim_dataslot
		instr3ac.Arg1 = args[1].Prim_dataslot
	case 3:
		instr3ac.Arg0 = args[0].Prim_dataslot
		instr3ac.Arg1 = args[1].Prim_dataslot
		instr3ac.Arg2 = args[2].Prim_dataslot
	case 4:
		instr3ac.Arg0 = args[0].Prim_dataslot
		instr3ac.Arg1 = args[1].Prim_dataslot
		instr3ac.Arg2 = args[2].Prim_dataslot
		instr3ac.Arg3 = args[3].Prim_dataslot
	case 5:
		instr3ac.Arg0 = args[0].Prim_dataslot
		instr3ac.Arg1 = args[1].Prim_dataslot
		instr3ac.Arg2 = args[2].Prim_dataslot
		instr3ac.Arg3 = args[3].Prim_dataslot
		instr3ac.Arg4 = args[4].Prim_dataslot
	default:
		panic("too many arguments in instr")
	}

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac_result_listexpr_flat appends an instruction to the bytecode list of the current basic block.
// Arguments are passed as a result and a listexpr, which will be "flattened" into the instruction.
//
//       This function is used to emit code for most functions, which have just an argument list "listexpr" that needs to be "flattened".
//
func (bbk *BBlock) instr_emit3ac_result_listexpr_flat(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, result *ast.Token_primary, listexpr []*ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	instr3ac.Arg0 = result.Prim_dataslot

	args_count := len(listexpr)

	switch args_count {
	case 0:
		// pass
	case 1:
		instr3ac.Arg1 = listexpr[0].Prim_dataslot
	case 2:
		instr3ac.Arg1 = listexpr[0].Prim_dataslot
		instr3ac.Arg2 = listexpr[1].Prim_dataslot
	case 3:
		instr3ac.Arg1 = listexpr[0].Prim_dataslot
		instr3ac.Arg2 = listexpr[1].Prim_dataslot
		instr3ac.Arg3 = listexpr[2].Prim_dataslot
	case 4:
		instr3ac.Arg1 = listexpr[0].Prim_dataslot
		instr3ac.Arg2 = listexpr[1].Prim_dataslot
		instr3ac.Arg3 = listexpr[2].Prim_dataslot
		instr3ac.Arg4 = listexpr[3].Prim_dataslot
	default:
		panic("too many arguments in instr")
	}

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac_result_listexpr_sysoperand_flat is same as instr_emit3ac_result_listexpr_flat, except that it has a SYSCOLLATOR or SYSLANGUAGE argument.
//
//       It is used for functions that require a SYSCOLLATOR or SYSLANGUAGE argument, like INSTR_SYSFUNC_CHARINDEX_VARCHAR, INSTR_SYSFUNC_REPLACE_VARCHAR, INSTR_SYSFUNC_DATENAME_MONTH_DATETIME etc.
//
func (bbk *BBlock) instr_emit3ac_result_listexpr_sysoperand_flat(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, result *ast.Token_primary, listexpr []*ast.Token_primary, sysoperand *ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	instr3ac.Arg0 = result.Prim_dataslot

	args_count := len(listexpr)

	switch args_count {
	case 0:
		instr3ac.Arg1 = sysoperand.Prim_dataslot
	case 1:
		instr3ac.Arg1 = listexpr[0].Prim_dataslot
		instr3ac.Arg2 = sysoperand.Prim_dataslot
	case 2:
		instr3ac.Arg1 = listexpr[0].Prim_dataslot
		instr3ac.Arg2 = listexpr[1].Prim_dataslot
		instr3ac.Arg3 = sysoperand.Prim_dataslot
	case 3:
		instr3ac.Arg1 = listexpr[0].Prim_dataslot
		instr3ac.Arg2 = listexpr[1].Prim_dataslot
		instr3ac.Arg3 = listexpr[2].Prim_dataslot
		instr3ac.Arg4 = sysoperand.Prim_dataslot
	default:
		panic("too many arguments in instr")
	}

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac_listexpr_IDataslots appends an instruction to the bytecode list of the current basic block.
// The argument listexpr is not "flattened", but remains a slice in the instruction.
//
//       It is used for e.g. INSTR_STMT_PRINT.
//
func (bbk *BBlock) instr_emit3ac_listexpr_IDataslots(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, listexpr []*ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	list_dataslots := make([]rsql.IDataslot, len(listexpr))
	for i, operand := range listexpr {
		list_dataslots[i] = operand.Prim_dataslot
	}

	instr3ac.Arg0 = list_dataslots

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac_result_listexpr appends an instruction to the bytecode list of the current basic block.
// The argument listexpr is not "flattened", but remains a slice in the instruction.
//
//       It is used for functions like INSTR_SYSFUNC_COALESCE_INT, INSTR_SYSFUNC_CONCAT_VARCHAR.
//
func (bbk *BBlock) instr_emit3ac_result_listexpr(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, result *ast.Token_primary, listexpr_datatype rsql.Datatype_t, listexpr []*ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	instr3ac.Arg0 = result.Prim_dataslot
	instr3ac.Arg1 = listexpr_to_list_dataslots(listexpr_datatype, listexpr)

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac_result_left_listexpr is same as instr_emit3ac_result_listexpr, except that it has a "left" argument.
//
//       It is used for operators like INSTR_IN_LIST_INT, or functions like INSTR_SYSFUNC_CHOOSE_INT.
//
func (bbk *BBlock) instr_emit3ac_result_left_listexpr(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, result *ast.Token_primary, left *ast.Token_primary, listexpr_datatype rsql.Datatype_t, listexpr []*ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	instr3ac.Arg0 = result.Prim_dataslot
	instr3ac.Arg1 = left.Prim_dataslot
	instr3ac.Arg2 = listexpr_to_list_dataslots(listexpr_datatype, listexpr)

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac_result_left_listexpr_sysoperand, except that it has a SYSCOLLATOR or SYSLANGUAGE argument.
//
//       It is used for operators like INSTR_IN_LIST_VARCHAR, or functions like INSTR_SYSFUNC_CHOOSE_VARCHAR or INSTR_SYSFUNC_DATENAME_MONTH_DATETIME.
//
func (bbk *BBlock) instr_emit3ac_result_left_listexpr_sysoperand(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, result *ast.Token_primary, left *ast.Token_primary, listexpr_datatype rsql.Datatype_t, listexpr []*ast.Token_primary, sysoperand *ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	instr3ac.Arg0 = result.Prim_dataslot
	instr3ac.Arg1 = left.Prim_dataslot
	instr3ac.Arg2 = listexpr_to_list_dataslots(listexpr_datatype, listexpr)
	instr3ac.Arg3 = sysoperand.Prim_dataslot

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

// instr_emit3ac_result_listexpr_for_CASE appends an instruction to the bytecode list of the current basic block.
//
//       It is used for CASE instruction.
//
func (bbk *BBlock) instr_emit3ac_result_listexpr_for_CASE(line_no_pos lex.Coord_t, instrcode rsql.Instrcode_t, result *ast.Token_primary, listexpr []*ast.Token_primary) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)
	instr3ac.Code = instrcode

	instr3ac.Arg0 = result.Prim_dataslot
	instr3ac.Arg1 = listexpr_to_list_Case_element(listexpr)

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}
