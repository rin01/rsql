package emit

import (
	"rsql"
	"rsql/ast"
	"rsql/dict"
)

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_SERVER_PARAMETER(tok *ast.Token_stmt_ALTER_SERVER_PARAMETER) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_SERVER_PARAMETER)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_SERVER_PARAMETER, tok.Asp_param, tok.Asp_value_int, tok.Asp_value_string)
}

func (bbk *BBlock) instr_walk_Token_stmt_CREATE_LOGIN(tok *ast.Token_stmt_CREATE_LOGIN) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_CREATE_LOGIN)

	login_params := dict.Login_params{Lgp_name: tok.Cl_login_name, Lgp_password_hash: tok.Cl_password_hash, Lgp_default_database: tok.Cl_default_database, Lgp_default_language: tok.Cl_default_language}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_CREATE_LOGIN, tok.Cl_login_name, login_params)
}

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_LOGIN(tok *ast.Token_stmt_ALTER_LOGIN) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_LOGIN)

	changes := dict.Login_params{Lgp_name: tok.Al_change_name, Lgp_password_hash: tok.Al_change_password_hash, Lgp_default_database: tok.Al_change_default_database, Lgp_default_language: tok.Al_change_default_language, Lgp_disable_option: tok.Al_change_disable_option}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_LOGIN, tok.Al_login_name, changes)
}

func (bbk *BBlock) instr_walk_Token_stmt_DROP_LOGIN(tok *ast.Token_stmt_DROP_LOGIN) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_DROP_LOGIN)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DROP_LOGIN, tok.Dl_login_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_CREATE_DATABASE(tok *ast.Token_stmt_CREATE_DATABASE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_CREATE_DATABASE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_CREATE_DATABASE, tok.Cd_database_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_DATABASE(tok *ast.Token_stmt_ALTER_DATABASE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_DATABASE)

	database_params := dict.Database_params{Dbp_name: tok.Ad_change_name, Dbp_status: tok.Ad_status, Dbp_mode: tok.Ad_mode, Dbp_access: tok.Ad_access}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_DATABASE, tok.Ad_database_name, database_params)
}

func (bbk *BBlock) instr_walk_Token_stmt_DROP_DATABASE(tok *ast.Token_stmt_DROP_DATABASE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_DROP_DATABASE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DROP_DATABASE, tok.Dd_database_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_CREATE_USER(tok *ast.Token_stmt_CREATE_USER) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_CREATE_USER)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_CREATE_USER, tok.Cu_database_name, tok.Cu_user_name, tok.Cu_login_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_USER(tok *ast.Token_stmt_ALTER_USER) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_USER)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_USER, tok.Au_database_name, tok.Au_user_name, tok.Au_change_name, tok.Au_change_login)
}

func (bbk *BBlock) instr_walk_Token_stmt_DROP_USER(tok *ast.Token_stmt_DROP_USER) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_DROP_USER)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DROP_USER, tok.Du_database_name, tok.Du_user_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_CREATE_ROLE(tok *ast.Token_stmt_CREATE_ROLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_CREATE_ROLE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_CREATE_ROLE, tok.Cr_database_name, tok.Cr_role_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_ROLE(tok *ast.Token_stmt_ALTER_ROLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_ROLE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_ROLE, tok.Ar_database_name, tok.Ar_role_name, tok.Ar_change_name, tok.Ar_add_member_name, tok.Ar_drop_member_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_DROP_ROLE(tok *ast.Token_stmt_DROP_ROLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_DROP_ROLE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DROP_ROLE, tok.Dr_database_name, tok.Dr_role_name)
}

func (bbk *BBlock) instr_walk_Token_stmt_GRANT(tok *ast.Token_stmt_GRANT) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_GRANT)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_GRANT, tok.Gr_object_qname, tok.Gr_permission, tok.Gr_list_of_principal_names)
}

func (bbk *BBlock) instr_walk_Token_stmt_DENY(tok *ast.Token_stmt_DENY) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_DENY)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DENY, tok.De_object_qname, tok.De_permission, tok.De_list_of_principal_names)
}

func (bbk *BBlock) instr_walk_Token_stmt_REVOKE(tok *ast.Token_stmt_REVOKE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_REVOKE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_REVOKE, tok.Re_object_qname, tok.Re_permission, tok.Re_list_of_principal_names, tok.Re_from_all)
}

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_AUTHORIZATION(tok *ast.Token_stmt_ALTER_AUTHORIZATION) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_AUTHORIZATION)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_AUTHORIZATION, tok.Aa_database_name, tok.Aa_change_owner_login)
}
