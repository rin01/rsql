package emit

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
)

func listexpr_to_list_VOID(listexpr []*ast.Token_primary) []*data.VOID {

	dataslot_list := make([]*data.VOID, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.VOID)
	}

	return dataslot_list
}

func listexpr_to_list_VARBINARY(listexpr []*ast.Token_primary) []*data.VARBINARY {

	dataslot_list := make([]*data.VARBINARY, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.VARBINARY)
	}

	return dataslot_list
}

func listexpr_to_list_VARCHAR(listexpr []*ast.Token_primary) []*data.VARCHAR {

	dataslot_list := make([]*data.VARCHAR, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.VARCHAR)
	}

	return dataslot_list
}

func listexpr_to_list_BIT(listexpr []*ast.Token_primary) []*data.BIT {

	dataslot_list := make([]*data.BIT, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.BIT)
	}

	return dataslot_list
}

func listexpr_to_list_TINYINT(listexpr []*ast.Token_primary) []*data.TINYINT {

	dataslot_list := make([]*data.TINYINT, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.TINYINT)
	}

	return dataslot_list
}

func listexpr_to_list_SMALLINT(listexpr []*ast.Token_primary) []*data.SMALLINT {

	dataslot_list := make([]*data.SMALLINT, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.SMALLINT)
	}

	return dataslot_list
}

func listexpr_to_list_INT(listexpr []*ast.Token_primary) []*data.INT {

	dataslot_list := make([]*data.INT, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.INT)
	}

	return dataslot_list
}

func listexpr_to_list_BIGINT(listexpr []*ast.Token_primary) []*data.BIGINT {

	dataslot_list := make([]*data.BIGINT, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.BIGINT)
	}

	return dataslot_list
}

func listexpr_to_list_MONEY(listexpr []*ast.Token_primary) []*data.MONEY {

	dataslot_list := make([]*data.MONEY, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.MONEY)
	}

	return dataslot_list
}

func listexpr_to_list_NUMERIC(listexpr []*ast.Token_primary) []*data.NUMERIC {

	dataslot_list := make([]*data.NUMERIC, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.NUMERIC)
	}

	return dataslot_list
}

func listexpr_to_list_FLOAT(listexpr []*ast.Token_primary) []*data.FLOAT {

	dataslot_list := make([]*data.FLOAT, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.FLOAT)
	}

	return dataslot_list
}

func listexpr_to_list_DATE(listexpr []*ast.Token_primary) []*data.DATE {

	dataslot_list := make([]*data.DATE, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.DATE)
	}

	return dataslot_list
}

func listexpr_to_list_TIME(listexpr []*ast.Token_primary) []*data.TIME {

	dataslot_list := make([]*data.TIME, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.TIME)
	}

	return dataslot_list
}

func listexpr_to_list_DATETIME(listexpr []*ast.Token_primary) []*data.DATETIME {

	dataslot_list := make([]*data.DATETIME, len(listexpr))

	for i, operand := range listexpr {
		dataslot_list[i] = operand.Prim_dataslot.(*data.DATETIME)
	}

	return dataslot_list
}

// listexpr_to_list_dataslots creates a list of dataslots, e.g. []*data.INT, []*data.DATE, etc, from a list of Token_primaries.
// This list is returned in an interface{} value.
//
// listexpr_datatype argument indicates the datatype of the elements in listexpr.
//
func listexpr_to_list_dataslots(listexpr_datatype rsql.Datatype_t, listexpr []*ast.Token_primary) interface{} {
	var (
		list_dataslots interface{}
	)

	switch listexpr_datatype {
	case rsql.DATATYPE_VOID:
		list_dataslots = listexpr_to_list_VOID(listexpr)

	case rsql.DATATYPE_VARBINARY:
		list_dataslots = listexpr_to_list_VARBINARY(listexpr)

	case rsql.DATATYPE_VARCHAR:
		list_dataslots = listexpr_to_list_VARCHAR(listexpr)

	case rsql.DATATYPE_BIT:
		list_dataslots = listexpr_to_list_BIT(listexpr)

	case rsql.DATATYPE_TINYINT:
		list_dataslots = listexpr_to_list_TINYINT(listexpr)

	case rsql.DATATYPE_SMALLINT:
		list_dataslots = listexpr_to_list_SMALLINT(listexpr)

	case rsql.DATATYPE_INT:
		list_dataslots = listexpr_to_list_INT(listexpr)

	case rsql.DATATYPE_BIGINT:
		list_dataslots = listexpr_to_list_BIGINT(listexpr)

	case rsql.DATATYPE_MONEY:
		list_dataslots = listexpr_to_list_MONEY(listexpr)

	case rsql.DATATYPE_NUMERIC:
		list_dataslots = listexpr_to_list_NUMERIC(listexpr)

	case rsql.DATATYPE_FLOAT:
		list_dataslots = listexpr_to_list_FLOAT(listexpr)

	case rsql.DATATYPE_DATE:
		list_dataslots = listexpr_to_list_DATE(listexpr)

	case rsql.DATATYPE_TIME:
		list_dataslots = listexpr_to_list_TIME(listexpr)

	case rsql.DATATYPE_DATETIME:
		list_dataslots = listexpr_to_list_DATETIME(listexpr)

	default:
		panic("impossible")
	}

	return list_dataslots
}

func listexpr_to_list_Case_element(listexpr []*ast.Token_primary) []data.Case_element_t {

	element_list := make([]data.Case_element_t, len(listexpr)/2)

	for i, k := 0, 0; i < len(listexpr); i, k = i+2, k+1 {
		element_list[k] = data.Case_element_t{Cond: listexpr[i].Prim_dataslot.(*data.BOOLEAN), Val: listexpr[i+1].Prim_dataslot}
	}

	return element_list
}
