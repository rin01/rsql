package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_BACKUP(tok *ast.Token_stmt_BACKUP) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_BACKUP)

	params := rsql.Backup_params{

		Bk_server_default_collation: tok.Bk_server_default_collation,
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_BACKUP, tok.Bk_database_name, tok.Bk_filename, params)
}
