package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_RESTORE(tok *ast.Token_stmt_RESTORE) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_RESTORE)

	params := rsql.Restore_params{
		Rt_replace: tok.Rt_option_replace,
		Rt_no_user: tok.Rt_option_no_user,
		Rt_verbose: tok.Rt_option_verbose,
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_RESTORE, tok.Rt_database_name, tok.Rt_filename, params)
}
