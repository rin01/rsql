package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_SHUTDOWN(tok *ast.Token_stmt_SHUTDOWN) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHUTDOWN)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHUTDOWN, tok.Sd_nowait)
}
