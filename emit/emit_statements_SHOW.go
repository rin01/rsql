package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_SHOW(tok *ast.Token_stmt_SHOW) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHOW)

	switch tok.Sh_instruction_code {
	case rsql.INSTR_STMT_SHOW_PARAMETER:
		params := rsql.Show_params{
			Sh_output_type: tok.Sh_output_type,
			Sh_template:    tok.Sh_template,
			Sh_option_ALL:  tok.Sh_option_ALL,
			Sh_LIKE:        tok.Sh_LIKE,

			Sh_object_name:              tok.Sh_object_name,
			Sh_LIKE_pattern:             tok.Sh_LIKE_pattern,
			Sh_server_default_collation: tok.Sh_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_PARAMETER, params)

	case rsql.INSTR_STMT_SHOW_LOGIN:
		params := rsql.Show_params{
			Sh_output_type: tok.Sh_output_type,
			Sh_template:    tok.Sh_template,
			Sh_option_ALL:  tok.Sh_option_ALL,
			Sh_LIKE:        tok.Sh_LIKE,

			Sh_object_name:              tok.Sh_object_name,
			Sh_LIKE_pattern:             tok.Sh_LIKE_pattern,
			Sh_server_default_collation: tok.Sh_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_LOGIN, params)

	case rsql.INSTR_STMT_SHOW_DATABASE:
		params := rsql.Show_params{
			Sh_output_type: tok.Sh_output_type,
			Sh_template:    tok.Sh_template,
			Sh_option_ALL:  tok.Sh_option_ALL,
			Sh_LIKE:        tok.Sh_LIKE,

			Sh_database_name:            tok.Sh_database_name,
			Sh_LIKE_pattern:             tok.Sh_LIKE_pattern,
			Sh_server_default_collation: tok.Sh_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_DATABASE, params)

	case rsql.INSTR_STMT_SHOW_USER,
		rsql.INSTR_STMT_SHOW_ROLE:
		params := rsql.Show_params{
			Sh_output_type: tok.Sh_output_type,
			Sh_template:    tok.Sh_template,
			Sh_option_ALL:  tok.Sh_option_ALL,
			Sh_option_PERM: tok.Sh_option_PERM,
			Sh_LIKE:        tok.Sh_LIKE,

			Sh_database_name:            tok.Sh_database_name,
			Sh_object_name:              tok.Sh_object_name,
			Sh_LIKE_pattern:             tok.Sh_LIKE_pattern,
			Sh_server_default_collation: tok.Sh_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), tok.Sh_instruction_code, params)

	case rsql.INSTR_STMT_SHOW_TABLE:
		params := rsql.Show_params{
			Sh_output_type: tok.Sh_output_type,
			Sh_template:    tok.Sh_template,
			Sh_option_ALL:  tok.Sh_option_ALL,
			Sh_option_PERM: tok.Sh_option_PERM,
			Sh_LIKE:        tok.Sh_LIKE,

			Sh_database_name: tok.Sh_database_name,
			Sh_schema_name:   tok.Sh_schema_name,
			Sh_object_name:   tok.Sh_object_name,
			Sh_LIKE_pattern:  tok.Sh_LIKE_pattern,
			Sh_flag_unique:   tok.Sh_flag_unique,

			Sh_server_default_collation: tok.Sh_server_default_collation,
		}

		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_TABLE, params)

	default:
		panic("impossible")
	}

}

func (bbk *BBlock) instr_walk_Token_stmt_SHOW_LOCKS(tok *ast.Token_stmt_SHOW_LOCKS) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHOW_LOCKS)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_LOCKS)
}

func (bbk *BBlock) instr_walk_Token_stmt_SHOW_WORKERS(tok *ast.Token_stmt_SHOW_WORKERS) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHOW_WORKERS)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_WORKERS)
}

func (bbk *BBlock) instr_walk_Token_stmt_SHOW_INFO(tok *ast.Token_stmt_SHOW_INFO) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHOW_INFO)

	params := rsql.Show_info_params{
		Shinf_servername:               tok.Shinf_servername,
		Shinf_server_default_collation: tok.Shinf_server_default_collation,

		Shinf_login:            tok.Shinf_login.Prim_dataslot,
		Shinf_current_user:     tok.Shinf_current_user.Prim_dataslot,
		Shinf_current_database: tok.Shinf_current_database.Prim_dataslot,
		Shinf_current_schema:   tok.Shinf_current_schema.Prim_dataslot,
		Shinf_current_language: tok.Shinf_current_language.Prim_dataslot,
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_INFO, params)
}

func (bbk *BBlock) instr_walk_Token_stmt_SHOW_COLLATIONS(tok *ast.Token_stmt_SHOW_COLLATIONS) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHOW_COLLATIONS)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_COLLATIONS)
}

func (bbk *BBlock) instr_walk_Token_stmt_SHOW_LANGUAGES(tok *ast.Token_stmt_SHOW_LANGUAGES) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHOW_LANGUAGES)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SHOW_LANGUAGES)
}
