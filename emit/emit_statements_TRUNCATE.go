package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_TRUNCATE_TABLE(tok *ast.Token_stmt_TRUNCATE_TABLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_TRUNCATE_TABLE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_TRUNCATE_TABLE, tok.Tt_table_qname, tok.Tt_gtabledef, tok.Tt_shrink_file)
}
