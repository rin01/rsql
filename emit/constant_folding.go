package emit

import (
	"rsql"
	"rsql/ast"
	"rsql/vm"
)

// constfold_walk_Token_primary performs constant folding.
//
//      Walk the decorated AST tree recursively:
//         - if the Token_primary is KIND_RO_LEAF, KIND_VAR_LEAF or KIND_COL_LEAF, just return.
//         - if the Token_primary is an operator or a function, and all its operands are KIND_RO_LEAF, compute the result by running vm.Execute_basicblocks_all() for this instruction only.
//
// The receiver bbk is a basic block created just for constant folding, by Constfold_walk_expression_tree() function. It will at most contain one instruction.
//
func (bbk *BBlock) constfold_walk_Token_primary(tok *ast.Token_primary) {
	var (
		rsql_err              *rsql.Error
		sysoperand            *ast.Token_primary
		constant_folding_flag bool
	)

	// if KIND_RO_LEAF, KIND_VAR_LEAF or KIND_COL_LEAF, it is not an operator nor a function, but a simple data. It may be a literal or a variable

	if tok.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF|rsql.KIND_COL_LEAF) != 0 { // in this case, just return.
		return
	}

	// SYSCOLLATOR and SYSLANGUAGE argument (for the moment, no instruction has both arguments at the same time)
	// note: tok.Prim_syscollator and tok.Prim_syslanguage are KIND_RO_LEAF or KIND_VAR_LEAF, so they don't need to be walked.

	sysoperand = nil
	if tok.Prim_syscollator != nil {
		sysoperand = tok.Prim_syscollator // always a literal
		rsql.Assert(tok.Prim_syslanguage == nil)
	} else if tok.Prim_syslanguage != nil {
		sysoperand = tok.Prim_syslanguage // always a literal or a variable
	}

	// walk operands

	switch tok.Prim_type {
	case ast.TOK_PRIM_OPERATOR:
		switch tok.Prim_cardinality {
		case ast.TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL:

			bbk.constfold_walk_Token_primary(tok.Prim_left) // walk left operand

			rsql.Assert(sysoperand == nil)

			if tok.Prim_left.Prim_dataslot.Kind() != rsql.KIND_RO_LEAF {
				return
			}

			bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left)

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE:

			bbk.constfold_walk_Token_primary(tok.Prim_left) // walk left operand

			bbk.constfold_walk_Token_primary(tok.Prim_right) // walk right operand

			if !(tok.Prim_left.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF && tok.Prim_right.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF && (sysoperand == nil || sysoperand.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF)) {
				return
			}

			if sysoperand == nil {
				bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_right)
			} else {
				bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_right, sysoperand)
			}

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST:
			constant_folding_flag = true

			bbk.constfold_walk_Token_primary(tok.Prim_left) // walk left operand

			if tok.Prim_left.Prim_dataslot.Kind() != rsql.KIND_RO_LEAF {
				constant_folding_flag = false
			}

			for _, operand := range tok.Prim_listexpr {
				bbk.constfold_walk_Token_primary(operand) // walk each operand

				if operand.Prim_dataslot.Kind() != rsql.KIND_RO_LEAF {
					constant_folding_flag = false
				}
			}

			if !(sysoperand == nil || sysoperand.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF) {
				constant_folding_flag = false
			}

			if constant_folding_flag == false {
				return
			}

			if sysoperand == nil {
				bbk.instr_emit3ac_result_left_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_left.Prim_dataslot.Datatype(), tok.Prim_listexpr)
			} else {
				bbk.instr_emit3ac_result_left_listexpr_sysoperand(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_left.Prim_dataslot.Datatype(), tok.Prim_listexpr, sysoperand)
			}

		default:
			panic("cardinality unknown")
		}

	case ast.TOK_PRIM_CAST:
		rsql.Assert(tok.Prim_right == nil)

		bbk.constfold_walk_Token_primary(tok.Prim_left) // walk left operand

		rsql.Assert(sysoperand == nil)

		if tok.Prim_left.Prim_dataslot.Kind() != rsql.KIND_RO_LEAF {
			return
		}

		bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left)

	case ast.TOK_PRIM_SYSFUNC:
		constant_folding_flag = true

		if tok.Prim_sysfunc_descr.Is_volatile_or_aggregate() { // if sysfunc value must be recomputed each time or is an aggregate, it must not be constant folded.
			constant_folding_flag = false
		}

		for _, operand := range tok.Prim_listexpr {
			bbk.constfold_walk_Token_primary(operand) // walk each operand

			if operand.Prim_dataslot.Kind() != rsql.KIND_RO_LEAF {
				constant_folding_flag = false
			}
		}

		if !(sysoperand == nil || sysoperand.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF) {
			constant_folding_flag = false
		}

		if constant_folding_flag == false {
			return
		}

		switch tok.Prim_sysfunc_descr.Sd_id {
		case ast.SYSFUNC_CHOOSE:
			bbk.instr_emit3ac_result_left_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr[0], tok.Prim_listexpr[1].Prim_dataslot.Datatype(), tok.Prim_listexpr[1:])

		case ast.SYSFUNC_COALESCE:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr[0].Prim_dataslot.Datatype(), tok.Prim_listexpr)

		case ast.SYSFUNC_CONCAT:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_VARCHAR, tok.Prim_listexpr)

		case ast.SYSFUNC_TIMEFROMPARTS:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_INT, tok.Prim_listexpr)

		case ast.SYSFUNC_DATETIMEFROMPARTS:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_INT, tok.Prim_listexpr)

		case ast.SYSFUNC_DATETIME2FROMPARTS:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_INT, tok.Prim_listexpr)

		default:
			if sysoperand == nil { // for most functions
				bbk.instr_emit3ac_result_listexpr_flat(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr)
			} else { // e.g. INSTR_SYSFUNC_CHARINDEX_VARCHAR, INSTR_SYSFUNC_REPLACE_VARCHAR, INSTR_SYSFUNC_DATENAME_MONTH_DATETIME etc
				bbk.instr_emit3ac_result_listexpr_sysoperand_flat(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr, sysoperand) // sysoperand can be SYSCOLLATOR or SYSLANGUAGE
			}
		}

	case ast.TOK_PRIM_CASE:
		constant_folding_flag = true

		for _, operand := range tok.Prim_listexpr {
			bbk.constfold_walk_Token_primary(operand) // walk each operand

			if operand.Prim_dataslot.Kind() != rsql.KIND_RO_LEAF {
				constant_folding_flag = false
			}
		}

		if !(sysoperand == nil || sysoperand.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF) {
			constant_folding_flag = false
		}

		if constant_folding_flag == false {
			return
		}

		bbk.instr_emit3ac_result_listexpr_for_CASE(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr)

	default:
		panic("impossible")
	}

	// execute instruction

	if _, rsql_err = vm.Execute_basicblocks_all((*rsql.Context)(nil), (*rsql.Basicblock)(bbk)); rsql_err != nil { // Context is nil because only arithmetic instructions are run, which don't need context info
		panic("impossible")
	}
	tok.Prim_dataslot.Set_kind(rsql.KIND_RO_LEAF) // current token is now considered as a read-only literal

	bbk.Bb_instr3ac_list = bbk.Bb_instr3ac_list[:0] // reset the basicblock instruction list to empty list
	bbk.Bb_jump_target = nil
}

func Constfold_walk_expression_tree(tok *ast.Token_primary) {
	var (
		bbk_dummy           BBlock
		bbk_dummy_instrlist [1]rsql.Instr3ac // default list for one instruction, used by bb_dummy
	)

	bbk_dummy.Bb_instr3ac_list = bbk_dummy_instrlist[:0] // attach bb_dummy list of instruction, which will be used to store the instruction

	bbk_dummy.constfold_walk_Token_primary(tok) // constant fold the expression ast tree
}
