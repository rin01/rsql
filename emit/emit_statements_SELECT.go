package emit

import (
	"rsql"
	"rsql/ast"
	"rsql/csr"
)

func (bbk *BBlock) instr_walk_Token_stmt_SELECT_or_UNION(tok *ast.Token_stmt_SELECT_or_UNION) {
	var (
		xtable        ast.Xtable
		xtable_select *ast.Xtable_select
		curs_orderby  *csr.Cursor_table
		params        *rsql.Select_params
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_SELECT_OR_UNION)

	params = &rsql.Select_params{TOP_value: -1} // default value

	instr_walk_xtable(tok.Su_xtable)

	// create colname list

	colname_list := make([]string, 0, 30) // 30 is good enough

	xtable = tok.Su_xtable

	switch xtable.Type() {
	case ast.XT_SELECT:
		xtable_select = xtable.(*ast.Xtable_select)

		params.TOP_value = xtable_select.Sel_TOP_value

		curs_orderby = xtable_select.Sel_cursor_orderby

		if len(xtable_select.Sel_list_cast_to_var) > 0 { // assignment to @variable
			var cast_to_var_bb *rsql.Basicblock

			cast_to_var_bb = xtable_select.Sel_cast_to_var_basicblock
			bbk := (*BBlock)(cast_to_var_bb)

			for _, cast_token := range xtable_select.Sel_list_cast_to_var {
				switch cast_token.Prim_type {
				case ast.TOK_PRIM_CAST:
					bbk.instr_emit3ac(cast_token.Batch_line(), cast_token.Prim_instruction_code, cast_token, cast_token.Prim_left)

				case ast.TOK_PRIM_SYSFUNC: // INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE/TIME/DATETIME
					rsql.Assert(cast_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE ||
						cast_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_TIME ||
						cast_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME)
					sysoperand := cast_token.Prim_syslanguage
					bbk.instr_emit3ac_result_listexpr_sysoperand_flat(cast_token.Batch_line(), cast_token.Prim_instruction_code, cast_token, cast_token.Prim_listexpr, sysoperand)

				default:
					panic("impossible")
				}
			}

			var_list := make([]rsql.IDataslot, 0, len(xtable_select.Sel_list_cast_to_var))

			for _, tokvar := range xtable_select.Sel_varlist {
				var_list = append(var_list, tokvar.Prim_dataslot)
			}

			params.Set_variables_list = var_list
			params.Set_variables_bb = cast_to_var_bb

			rsql.Assert(len(cast_to_var_bb.Bb_instr3ac_list) > 0)
		}

	case ast.XT_UNION:
		xtable_select = ast.Get_first_SELECT(xtable)
		curs_orderby = xtable.(*ast.Xtable_union).Un_cursor_orderby

	default:
		panic("impossible")
	}

	for _, column := range xtable_select.Sel_columns {
		colname := column.Col_label_original
		if colname == "" && column.Col_expression.Prim_type == ast.TOK_PRIM_IDENTIFIER {
			colname = column.Col_expression.Prim_name
		}

		colname_list = append(colname_list, colname)
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_SELECT_OR_UNION, tok.Su_xtable.Cursor(), curs_orderby, colname_list, params) // curs_orderby and params.Set_variables_list and params.Set_variables_bb may be nil
}

func instr_walk_xtable_join(xtable_join *ast.Xtable_join) {
	var bbk *BBlock

	instr_walk_xtable(xtable_join.Jn_left)
	instr_walk_xtable(xtable_join.Jn_right)

	bbk = (*BBlock)(xtable_join.Jn_cursor.On_basicblock)
	if xtable_join.Jn_on != nil {
		bbk.Instr_walk_Token_primary(xtable_join.Jn_on)
	}
}

func instr_walk_xtable_from(xtable_from *ast.Xtable_from) {
	var bbk *BBlock

	instr_walk_xtable(xtable_from.Fr_list[0])

	bbk = (*BBlock)(xtable_from.Fr_cursor.Where_basicblock)
	if xtable_from.Fr_where != nil {
		bbk.Instr_walk_Token_primary(xtable_from.Fr_where)
	}
}

func instr_walk_xtable_select(xtable_select *ast.Xtable_select) {
	var bbk *BBlock

	if xtable_select.Sel_grouping_flag == true {
		// grouptable feed cursor

		instr_walk_xtable_from(xtable_select.Sel_grouptable_feeding_from)

		// basicblock for GROUP BY expressions, for cursor.Grouptable_insertion_row dataslots

		bbk = (*BBlock)(xtable_select.Sel_cursor.Grouptable_nk_basicblock)

		for _, tok := range xtable_select.Sel_groupby_expressions {
			bbk.Instr_walk_Token_primary(tok)
		}

		// basicblock for arguments of aggregate functions SUM, etc, for cursor.Grouptable_upsert_row dataslots

		bbk = (*BBlock)(xtable_select.Sel_cursor.Grouptable_injection_basicblock)

		for _, tok := range xtable_select.Sel_grouptable_aggrfuncs_to_inject {
			rsql.Assert(tok.Prim_dataslot.Kind() == rsql.KIND_TMP)

			bbk.Instr_walk_Token_primary(tok)
		}
	}

	// basicblock for column expressions

	instr_walk_xtable_from(xtable_select.Sel_from)

	bbk = (*BBlock)(xtable_select.Sel_cursor.Cols_basicblock)
	for _, column := range xtable_select.Sel_columns {
		bbk.Instr_walk_Token_primary(column.Col_expression)
	}
}

func instr_walk_xtable_union(xtable_union *ast.Xtable_union) {
	var bbk *BBlock

	for i, item := range xtable_union.Un_members { // for each member of UNION
		xtable := item.Xtbl
		instr_walk_xtable(xtable)

		bbk = (*BBlock)(xtable_union.Un_cursor.Equalize_basicblocks[i])

		for _, cast_token := range item.Cast_to_equalized_row { // CAST, CONVERT, or plain copy
			switch cast_token.Prim_type {
			case ast.TOK_PRIM_CAST:
				bbk.instr_emit3ac(cast_token.Batch_line(), cast_token.Prim_instruction_code, cast_token, cast_token.Prim_left)

			case ast.TOK_PRIM_SYSFUNC: // INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE/TIME/DATETIME
				rsql.Assert(cast_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE ||
					cast_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_TIME ||
					cast_token.Prim_instruction_code == rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME)
				sysoperand := cast_token.Prim_syslanguage
				bbk.instr_emit3ac_result_listexpr_sysoperand_flat(cast_token.Batch_line(), cast_token.Prim_instruction_code, cast_token, cast_token.Prim_listexpr, sysoperand)

			default:
				panic("impossible")
			}
		}
	}
}

func instr_walk_xtable(xtable ast.Xtable) {

	switch xtable.Type() {
	case ast.XT_TABLE:
		return

	case ast.XT_JOIN:
		instr_walk_xtable_join(xtable.(*ast.Xtable_join))

	case ast.XT_SINGLE:
		// pass

	case ast.XT_FROM:
		instr_walk_xtable_from(xtable.(*ast.Xtable_from))

	case ast.XT_SELECT:
		instr_walk_xtable_select(xtable.(*ast.Xtable_select))

	case ast.XT_UNION:
		instr_walk_xtable_union(xtable.(*ast.Xtable_union))

	default:
		panic("impossible")
	}
}
