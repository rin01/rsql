package emit

import (
	"fmt"

	"rsql"
	"rsql/ast"
)

// Instr_walk_Token_primary recursively appends instructions to the bytecode list of the current basic block, for the whole Token_primary subtree rooting at tok.
//
func (bbk *BBlock) Instr_walk_Token_primary(tok *ast.Token_primary) {
	var (
		sysoperand *ast.Token_primary
	)

	// if KIND_RO_LEAF, KIND_VAR_LEAF or KIND_COL_LEAF, it is not an operator nor a function, but a simple data. It may be a literal, a variable or a column.

	if tok.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF|rsql.KIND_COL_LEAF) != 0 { // in this case, just return.
		return
	}

	// SYSCOLLATOR and SYSLANGUAGE argument (for the moment, no instruction has both arguments at the same time)
	// note: tok.Prim_syscollator and tok.Prim_syslanguage are KIND_RO_LEAF or KIND_VAR_LEAF, so they don't need to be walked.

	sysoperand = nil
	if tok.Prim_syscollator != nil {
		sysoperand = tok.Prim_syscollator // always a literal or a variable
		rsql.Assert(tok.Prim_syslanguage == nil)
	} else if tok.Prim_syslanguage != nil {
		sysoperand = tok.Prim_syslanguage // always a literal or a variable
	}

	// walk left and right operands

	switch tok.Prim_type {
	case ast.TOK_PRIM_OPERATOR:
		switch tok.Prim_cardinality {
		case ast.TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL:

			rsql.Assert(sysoperand == nil)

			bbk.Instr_walk_Token_primary(tok.Prim_left) // left operand

			bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left)

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE:

			bbk.Instr_walk_Token_primary(tok.Prim_left) // left operand

			bbk.Instr_walk_Token_primary(tok.Prim_right) // right operand

			if sysoperand == nil {
				bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_right)
			} else {
				bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_right, sysoperand)
			}

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST:

			bbk.Instr_walk_Token_primary(tok.Prim_left) // left operand

			for _, operand := range tok.Prim_listexpr {
				bbk.Instr_walk_Token_primary(operand) // walk each operand
			}

			if sysoperand == nil {
				bbk.instr_emit3ac_result_left_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_left.Prim_dataslot.Datatype(), tok.Prim_listexpr)
			} else {
				bbk.instr_emit3ac_result_left_listexpr_sysoperand(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_left.Prim_dataslot.Datatype(), tok.Prim_listexpr, sysoperand)
			}

		default:
			panic("cardinality unknown")
		}

	case ast.TOK_PRIM_CAST:

		bbk.Instr_walk_Token_primary(tok.Prim_left) // left operand

		bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left)

		/*
		   case ast.TOK_PRIM_SYSFUNC :

		       for _, operand := range tok.Prim_listexpr {
		           Instr_walk_Token_primary(operand)    // walk each operand
		       }

		       instr_emit3ac(tok.Tok_line, tok.Prim_instruction_code, tok.Prim_dataslot,  tok.Prim_listexpr_count , NULL, e);
		     }

		   else if ( tok->prim_type == TOK_PRIM_SUBEXP )
		     {
		       switch ( tok->prim_subtype )
		         {
		           case TOK_PRIM_SUBEXP_CASE :
		             for ( i=0; i < tok->prim_listexpr_count; i++ )
		               {
		                 Instr_walk_Token_primary(tok->prim_listexpr[i], e);    // walk each operand
		               }

		             instr_emit3ac_sysfunc((Token *)tok, tok->prim_listexpr_count, tok->prim_instruction_code, tok->prim_dataslot, tok, NULL, e);

		             break;

		           default :
		             fatal("bof");
		             break;
		         }
		     }
		*/

	case ast.TOK_PRIM_SYSFUNC:
		for _, tok_arg := range tok.Prim_listexpr {
			bbk.Instr_walk_Token_primary(tok_arg)
		}

		switch tok.Prim_sysfunc_descr.Sd_id {
		case ast.SYSFUNC_CHOOSE:
			bbk.instr_emit3ac_result_left_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr[0], tok.Prim_listexpr[1].Prim_dataslot.Datatype(), tok.Prim_listexpr[1:])

		case ast.SYSFUNC_COALESCE:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr[0].Prim_dataslot.Datatype(), tok.Prim_listexpr)

		case ast.SYSFUNC_CONCAT:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_VARCHAR, tok.Prim_listexpr)

		case ast.SYSFUNC_TIMEFROMPARTS:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_INT, tok.Prim_listexpr)

		case ast.SYSFUNC_DATETIMEFROMPARTS:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_INT, tok.Prim_listexpr)

		case ast.SYSFUNC_DATETIME2FROMPARTS:
			bbk.instr_emit3ac_result_listexpr(tok.Batch_line(), tok.Prim_instruction_code, tok, rsql.DATATYPE_INT, tok.Prim_listexpr)

		default:
			if sysoperand == nil { // for most functions
				bbk.instr_emit3ac_result_listexpr_flat(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr)
			} else { // e.g. INSTR_SYSFUNC_CHARINDEX_VARCHAR, INSTR_SYSFUNC_REPLACE_VARCHAR, INSTR_SYSFUNC_DATENAME_MONTH_DATETIME etc
				bbk.instr_emit3ac_result_listexpr_sysoperand_flat(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr, sysoperand) // sysoperand can be SYSCOLLATOR or SYSLANGUAGE
			}
		}

	case ast.TOK_PRIM_CASE:
		rsql.Assert(tok.Prim_left == nil && tok.Prim_right == nil)

		for _, tok_arg := range tok.Prim_listexpr {
			bbk.Instr_walk_Token_primary(tok_arg)
		}

		bbk.instr_emit3ac_result_listexpr_for_CASE(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_listexpr)

	default:
		panic(fmt.Sprintf("impossible, unknown Prim_type: %s %s %s %s", tok.Prim_type, tok.Prim_subtype, tok.Prim_dataslot.Kind(), tok.Prim_name))
	}
}

// Instr_walk_Token_primary_for_sarg_comparison is similar to Instr_walk_Token_primary_for_sarg_comparison.
// But is is used only by the optimizer, to evaluate the comparison of Sargs:    column  comp  expression
//
// If comp_args_reversed, it means thant the comparison is of the form:    expression  comp  column
//
// Expression should not be walked, as it must be evaluated separately when cursor.Next() method is called the first time.
//
func (bbk *BBlock) Instr_walk_Token_primary_for_sarg_comparison(tok *ast.Token_primary, comp_args_reversed bool) {
	var (
		sysoperand *ast.Token_primary
	)

	// if KIND_RO_LEAF, KIND_VAR_LEAF or KIND_COL_LEAF, it is not an operator nor a function, but a simple data. It may be a literal, a variable or a column.

	if tok.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF|rsql.KIND_COL_LEAF) != 0 { // IT SHOULD NOT HAPPEN. In this case, just panic.
		panic("impossible")
	}

	// SYSCOLLATOR and SYSLANGUAGE argument (for the moment, no instruction has both arguments at the same time)
	// note: tok.Prim_syscollator and tok.Prim_syslanguage are KIND_RO_LEAF or KIND_VAR_LEAF, so they don't need to be walked.

	sysoperand = nil
	if tok.Prim_syscollator != nil {
		sysoperand = tok.Prim_syscollator // always a literal or a variable
		rsql.Assert(tok.Prim_syslanguage == nil)
	} else if tok.Prim_syslanguage != nil {
		sysoperand = tok.Prim_syslanguage // always a literal or a variable
	}

	// walk left and right operands

	rsql.Assert(tok.Prim_type == ast.TOK_PRIM_OPERATOR)
	rsql.Assert(tok.Prim_cardinality == ast.TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS)

	switch comp_args_reversed {
	case false: // column comp expression
		bbk.Instr_walk_Token_primary(tok.Prim_left) // column is left operand. Expression is right operand, which has been separately evaluated and should not be walked again.

	default: // expression comp column
		bbk.Instr_walk_Token_primary(tok.Prim_right) // column is right operand. Expression is left operand, which has been separately evaluated and should not be walked again.
	}

	if sysoperand == nil {
		bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_right)
	} else {
		bbk.instr_emit3ac(tok.Batch_line(), tok.Prim_instruction_code, tok, tok.Prim_left, tok.Prim_right, sysoperand)
	}
}
