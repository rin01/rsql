package emit

import (
	"rsql"
	"rsql/data"
	"rsql/lex"
)

func (bbk *BBlock) instr_emit3ac_jump_out_true_false(line_no_pos lex.Coord_t, dataslot_condition *data.BOOLEAN, basicblock_true *BBlock, basicblock_false *BBlock) {

	rsql.Assert(basicblock_true != nil)
	rsql.Assert(basicblock_false != nil)

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)

	instr3ac.Code = rsql.INSTR_JUMP_OUT_TRUE_FALSE
	instr3ac.Arg0 = dataslot_condition
	instr3ac.Arg1 = (*rsql.Basicblock)(basicblock_true)
	instr3ac.Arg2 = (*rsql.Basicblock)(basicblock_false)

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

func (bbk *BBlock) instr_emit3ac_jump_out(line_no_pos lex.Coord_t, basicblock_target *BBlock) {

	rsql.Assert(basicblock_target != nil)

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)

	instr3ac.Code = rsql.INSTR_JUMP_OUT
	instr3ac.Arg0 = (*rsql.Basicblock)(basicblock_target)

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

func (bbk *BBlock) instr_emit3ac_jump_if_NULL_or_false(line_no_pos lex.Coord_t, dataslot_condition *data.BOOLEAN, basicblock_target *BBlock) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)

	instr3ac.Code = rsql.INSTR_JUMP_IF_NULL_OR_FALSE
	instr3ac.Arg0 = dataslot_condition
	instr3ac.Arg1 = (*rsql.Basicblock)(basicblock_target)

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}

func (bbk *BBlock) instr_emit3ac_jump_RETURN(line_no_pos lex.Coord_t, return_value *data.INT) {

	instr3ac := rsql.Instr3ac{}

	instr3ac.Batch_line_no = line_no_pos.No
	instr3ac.Batch_line_pos = uint32(line_no_pos.Pos)

	instr3ac.Code = rsql.INSTR_JUMP_RETURN
	instr3ac.Arg0 = return_value

	bbk.Bb_instr3ac_list = append(bbk.Bb_instr3ac_list, instr3ac)
}
