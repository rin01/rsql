package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_UPDATE(tok *ast.Token_stmt_UPDATE) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_UPDATE)

	instr_walk_xtable_from(tok.Ud_from)

	bbk2 := (*BBlock)(tok.Ud_basicblock) // instructions to evaluate SET expressions are put in a local basic block

	insertion_base_row := make(rsql.Row, len(tok.Ud_insertion_base_tokens))

	for i, tok_col := range tok.Ud_insertion_base_tokens {
		bbk2.Instr_walk_Token_primary(tok_col) // emit instr for each column
		insertion_base_row[i] = tok_col.Prim_dataslot
	}

	params := rsql.Update_params{
		Ud_insertion_base_row: insertion_base_row,             // contains dataslots from target cursor, or SET expressions
		Ud_cursor:             tok.Ud_from.Cursor(),           // FROM cursor tree
		Ud_target_cursor:      tok.Ud_target_table.Tbl_cursor, // points to target table cursor, inside Ud_cursor tree
		Ud_basicblock:         tok.Ud_basicblock,              // instructions to evaluate SET expressions
		Ud_indexes_to_update:  tok.Ud_indexes_to_update,       // indexes to update. If nil, all indexes must be updated.
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_UPDATE, tok.Ud_target_table.Tbl_qname, params)
}
