package emit

import (
	"rsql"
	"rsql/ast"
	"rsql/dict"
)

func (bbk *BBlock) instr_walk_Token_stmt_CREATE_TABLE(tok *ast.Token_stmt_CREATE_TABLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_CREATE_TABLE)

	if tok.Ct_gtabledef.Base.Td_durability == rsql.DURABILITY_PERMANENT {
		bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_CREATE_TABLE, tok.Ct_database_name, tok.Ct_schema_name, tok.Ct_gtabledef)
	}
}

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_TABLE(tok *ast.Token_stmt_ALTER_TABLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_TABLE)

	table_params := dict.Table_params{
		Table_qname: tok.At_table_qname,

		Change_table_name:          tok.At_change_table_name, // for WITH NAME clause
		Change_table_name_original: tok.At_change_table_name_original,

		Column_name:                 tok.At_column_name,        // for ALTER COLUMN column_name
		Change_column_name:          tok.At_change_column_name, // WITH NAME clause
		Change_column_name_original: tok.At_change_column_name_original,
		Change_column_nullability:   tok.At_change_column_nullability, // NULL, NOT NULL clause
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_TABLE, table_params)
}

func (bbk *BBlock) instr_walk_Token_stmt_DROP_TABLE(tok *ast.Token_stmt_DROP_TABLE) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_DROP_TABLE)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DROP_TABLE, tok.Dt_table_qname)
}

func (bbk *BBlock) instr_walk_Token_stmt_CREATE_INDEX(tok *ast.Token_stmt_CREATE_INDEX) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_CREATE_INDEX)

	index_params := dict.Index_params{
		Table_qname:         tok.Ci_table_qname,
		Index_name:          tok.Ci_index_name,
		Index_name_original: tok.Ci_index_name_original,
		Index_type:          tok.Ci_index_type,
		Cluster_type:        tok.Ci_cluster_type,
		Colname_list:        tok.Ci_colname_list,
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_CREATE_INDEX, index_params)
}

func (bbk *BBlock) instr_walk_Token_stmt_ALTER_INDEX(tok *ast.Token_stmt_ALTER_INDEX) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_ALTER_INDEX)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_ALTER_INDEX, tok.Ai_table_qname, tok.Ai_index_name, tok.Ai_change_index_name, tok.Ai_change_index_name_original)
}

func (bbk *BBlock) instr_walk_Token_stmt_DROP_INDEX(tok *ast.Token_stmt_DROP_INDEX) {
	rsql.Assert(tok.Category() == ast.TOK_STMT_DROP_INDEX)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DROP_INDEX, tok.Di_table_qname, tok.Di_index_name)
}
