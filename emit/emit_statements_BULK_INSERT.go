package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_BULK_INSERT(tok *ast.Token_stmt_BULK_INSERT) {
	var (
		params rsql.Bulk_insert_params
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_BULK_INSERT)

	// create Params

	params = rsql.Bulk_insert_params{
		Bi_opt_codepage:        tok.Bi_opt_codepage,
		Bi_opt_rowterminator:   tok.Bi_opt_rowterminator,
		Bi_opt_fieldterminator: tok.Bi_opt_fieldterminator,
		Bi_opt_firstrow:        tok.Bi_opt_firstrow,
		Bi_opt_lastrow:         tok.Bi_opt_lastrow,
		Bi_opt_keepidentity:    tok.Bi_opt_keepidentity,
		Bi_opt_rtrim:           tok.Bi_opt_rtrim,
	}

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_BULK_INSERT, tok.Bi_table_qname, tok.Bi_filename, params, tok.Bi_gtabledef, tok.Bi_syslanguage.Prim_dataslot)
}
