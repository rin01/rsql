package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_DELETE(tok *ast.Token_stmt_DELETE) {

	rsql.Assert(tok.Category() == ast.TOK_STMT_DELETE)

	instr_walk_xtable_from(tok.De_from)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_DELETE, tok.De_from.Cursor(), tok.De_target_table.Tbl_cursor)
}
