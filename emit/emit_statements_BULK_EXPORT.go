package emit

import (
	"rsql"
	"rsql/ast"
)

func (bbk *BBlock) instr_walk_Token_stmt_BULK_EXPORT(tok *ast.Token_stmt_BULK_EXPORT) {
	var (
		params rsql.Bulk_export_params
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_BULK_EXPORT)

	// create Params

	params = rsql.Bulk_export_params{
		Be_opt_codepage:        tok.Be_opt_codepage,
		Be_opt_rowterminator:   tok.Be_opt_rowterminator,
		Be_opt_fieldterminator: tok.Be_opt_fieldterminator,
		Be_opt_date_format:     tok.Be_opt_date_format,
		Be_opt_time_format:     tok.Be_opt_time_format,
		Be_opt_datetime_format: tok.Be_opt_datetime_format,
	}

	instr_walk_xtable(tok.Be_xtable)

	bbk.instr_emit3ac_any_type(tok.Batch_line(), rsql.INSTR_STMT_BULK_EXPORT, tok.Be_xtable.Cursor(), tok.Be_filename, params)
}
