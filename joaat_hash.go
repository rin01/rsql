package rsql

/*
   Jenkins One-at-a-Time Hash function. (see wikipedia "jenkins hash functions")

   Note : FNV would be another good choice.

   http://www.burtleburtle.net/bob/hash/doobs.html

   For information, Jenkins says this about FNV :

       FNV Hash
       I need to fill this in. Search the web for FNV hash.
       It's faster than my hash on Intel (because Intel has fast multiplication), but slower on most other platforms.
       Preliminary tests suggested it has decent distributions.

       I have three complaints against it.
       First, it's specific about how to reduce the size if you don't use all 32 bits, it's not just a mask.
       Increasing the result size by one bit gives you a completely different hash.
       If you use a hash table that grows by increasing the result size by one bit, one old bucket maps across the entire new table, not to just two new buckets.
       If your algorithm has a sliding pointer for which buckets have been split, that just won't work with FNV.
       Second, it's linear. That means that widely separated things can cancel each other out at least as easily as nearby things.
       Third, since multiplication only affects higher bits, the lowest 7 bits of the state are never affected by the 8th bit of all the input bytes.

       On the plus side, very nearby things never cancel each other out at all.
       This makes FNV a good choice for hashing very short keys (like single English words).
       FNV is more robust than Bernstein's hash. It can handle any byte values, not just ASCII characters.

   A good article on hash functions can be found here : http://www.eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx

*/

// joaat_hash computes a hash value of a byte string.
// It uses Jenkins One-at-a-Time Hash algorithm.
//
func Joaat_hash(bytes []byte) uint32 {

	var hash uint32 = 0

	for _, c := range bytes {
		hash += uint32(c)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func Joaat_hash_str(str string) uint32 {
	var (
		hash uint32 = 0
		c    byte
	)

	for i := 0; i < len(str); i++ {
		c = str[i]
		hash += uint32(c)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}
