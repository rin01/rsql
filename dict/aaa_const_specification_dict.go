package dict

const (
	SPEC_SYSLOGINS_COLLECION_DEFAULT_SIZE      = 50
	SPEC_SYSDATABASES_COLLECION_DEFAULT_SIZE   = 10
	SPEC_SYSSCHEMAS_COLLECION_DEFAULT_SIZE     = 10
	SPEC_SYSTABLES_COLLECION_DEFAULT_SIZE      = 50
	SPEC_SYSPRINCIPALS_COLLECION_DEFAULT_SIZE  = 20
	SPEC_SYSPERMISSIONS_COLLECION_DEFAULT_SIZE = 50
)

const (
	SA_LOGIN_ID  = 1000    // login_id of 'sa'. IMPORTANT: IF YOU CHANGE THIS VALUE, YOU MUST CHANGE THE VALUE IN sql_install_script STRING, IN "install_master_db.go" FILE, AT LINE 'INSERT INTO syslogins'
	TRASHDB_DBID = 100     // dbid of 'trashdb'
	SCHID_DBO    = 0       // all databases will have a 'dbo' schema with this schid
	SCHID_START  = 5000    // first schid (excepted for 'dbo' schemas, which schid are always SCHID_DBO)
	PALID_START  = 30000   // first palid
	TBLID_START  = 7000000 // first tblid
)

const (
	SPEC_WORKERS_MAX_LIMIT                  = 64  // because permissions for each worker are put in a uint64 bitset
	SPEC_READ_TIMEOUT_MIN_LIMIT             = 1   // min read timeout, in seconds
	SPEC_LOCK_TICKER_INTERVAL_MIN_LIMIT     = 200 // min lock ticker interval, in milliseconds
	SPEC_LOCK_TIMEOUT_TICKS_COUNT_MIN_LIMIT = 1   // min timeout ticks count
)

const (
	MAX_SERVERNAME_RUNE_LENGTH = 500 // servername max rune length
)
