package dict

import (
	"log"
	"strconv"

	"github.com/mxk/go-sqlite/sqlite3"

	"rsql"
	"rsql/cache"
)

const SYSIDX_PREFIX = "sysidx$"

// Get_new_index_sysname_string creates a new index name, different from all names in indexmap and different from index_name in base Tabledef.
//
func Get_new_index_sysname_string(gtabledef *rsql.GTabledef) string {

	start_val := int64(0) // int64(rand.Intn(1000000))   I thought random would be good, but 0, 1, 2, 3 ... is better

	for i := start_val; i < start_val+100000; i++ { // it is ok, as there will never be 100000 indexes
		new_name_string := SYSIDX_PREFIX + strconv.FormatInt(i, 10)

		if gtabledef.Base != nil && new_name_string == gtabledef.Base.Td_index_name {
			continue
		}

		if _, ok := gtabledef.Indexmap[new_name_string]; ok == false {
			return new_name_string
		}
	}

	panic("impossible")
}

func nil_if_empty_string(s string) interface{} {
	var (
		res interface{}
	)

	if s != "" {
		res = s
	}
	return res
}

func td2tbl_type(td_type rsql.Td_type_t) string {

	switch td_type {
	case rsql.TD_TYPE_BASE_TABLE:
		return "BASE"
	case rsql.TD_TYPE_INDEX_TABLE:
		return "INDEX"
	default:
		panic("impossible")
	}
}

func td2tbl_index_type(td_index_type rsql.Td_index_type_t) string {

	switch td_index_type {
	case rsql.TD_UNIQUE:
		return "UNIQ"
	case rsql.TD_PRIMARY_KEY:
		return "PK"
	case rsql.TD_INDEX:
		return "IDX"
	default:
		panic("impossible")
	}
}

func coldefs_to_seqnos(coldefs []*rsql.Coldef) string {
	var (
		buff []byte
	)

	buff = make([]byte, 0, rsql.DEFAULT_BYTE_BUFF_CAPACITY)

	for i, coldef := range coldefs {
		buff = strconv.AppendInt(buff, int64(coldef.Cd_base_seqno), 10)
		if i < len(coldefs)-1 {
			buff = append(buff, ',')
		}
	}

	return string(buff)
}

func seqnos_to_coldefs(base_coldefs []*rsql.Coldef, s string) []*rsql.Coldef {
	var (
		col_no      int64
		err         error
		coldef_list []*rsql.Coldef
	)

	coldef_list = make([]*rsql.Coldef, 0, rsql.DEFAULT_COLDEF_SLICE_CAPACITY)

	for s != "" {
		if col_no, s, err = rsql.Read_int64(s, ','); err != nil {
			log.Panicf("MASTER: bad seqnos string found. \"%s\"", s)
		}

		coldef_list = append(coldef_list, base_coldefs[col_no])
	}

	return coldef_list
}

func (dictmgr *Dictionary_manager) Must_load_SYSTABLES() {
	var (
		err       error
		master    *sqlite3.Conn
		stmt      *sqlite3.Stmt
		ok        bool
		systables *Systables_collection

		gtbl_gtblid              int64
		gtbl_dbid                int64
		gtbl_schid               int64
		gtbl_table_name          string
		gtbl_table_name_original string
		gtbl_identity_seed       int64
		gtbl_identity_increment  int64
		gtbl_create_date         string

		table_file_path string

		tbl_tblid               int64
		tbl_dbid                int64
		tbl_schid               int64
		tbl_base_gtblid         int64
		tbl_index_name          string
		tbl_index_name_original string
		tbl_type                string
		tbl_index_type          string
		tbl_coldefs             string
		tbl_nk                  string
		tbl_payload             string
		tbl_status              int64

		td_type         rsql.Td_type_t
		td_index_type   rsql.Td_index_type_t
		td_coldefs      []*rsql.Coldef
		td_nk           []*rsql.Coldef
		td_payload      []*rsql.Coldef
		td_cluster_type rsql.Td_cluster_type_t

		base_coldefs_list       []*rsql.Coldef           // list of Coldefs, making up the column list of a base table
		all_base_coldefs_map    map[int64][]*rsql.Coldef // contains all base_coldefs_list
		base_coldefs_list_count int

		current_tblid   int64
		last_base_seqno int64

		c_tblid              int64
		c_base_seqno         int64
		c_colname            string
		c_colname_original   string
		c_datatype           int64
		c_precision          int64
		c_scale              int64
		c_fixlen_flag        int64
		c_collation_strength int64
		c_collation          string
		c_autonum            int64
		c_NOT_NULL_flag      int64

		gtabledef *rsql.GTabledef
	)

	master = dictmgr.dm_master

	systables = &dictmgr.dm_SYSTABLES

	systables.gtblid_map = make(map[int64]*rsql.GTabledef, SPEC_SYSTABLES_COLLECION_DEFAULT_SIZE)
	systables.name_map = make(map[Systable_namekey_t]*rsql.GTabledef, SPEC_SYSTABLES_COLLECION_DEFAULT_SIZE)

	//===== read all GTabledefs from master =====

	if stmt, err = master.Prepare("SELECT gtbl_gtblid, gtbl_dbid, gtbl_schid, gtbl_table_name, gtbl_table_name_original, gtbl_identity_seed, gtbl_identity_increment, gtbl_create_date FROM sysgtabledefs"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read tabledef record from master

		if err = stmt.Scan(&gtbl_gtblid, &gtbl_dbid, &gtbl_schid, &gtbl_table_name, &gtbl_table_name_original, &gtbl_identity_seed, &gtbl_identity_increment, &gtbl_create_date); err != nil {
			break
		}

		// fill in GTabledef

		gtabledef := &rsql.GTabledef{
			Gtblid: gtbl_gtblid,

			Gdbid:                gtbl_dbid,
			Gschid:               gtbl_schid,
			Gtable_name:          gtbl_table_name,
			Gtable_name_original: gtbl_table_name_original,

			Indexmap: make(map[string]*rsql.Tabledef, rsql.SPEC_GTABLEDEF_INDEXMAP_DEFAULT_SIZE),
			Colmap:   make(map[string]*rsql.Coldef, rsql.SPEC_GTABLEDEF_COLMAP_DEFAULT_SIZE),

			Identity_seed:      gtbl_identity_seed,
			Identity_increment: gtbl_identity_increment,

			Create_date: must_parse_time(gtbl_create_date),
		}

		if _, ok = systables.gtblid_map[gtabledef.Gtblid]; ok == true {
			panic("impossible") // prevented by pk on gtbl_gtblid
		}
		systables.gtblid_map[gtabledef.Gtblid] = gtabledef

		if _, ok = systables.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}]; ok == true {
			panic("impossible")
		}
		systables.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] = gtabledef
	}

	stmt.Close() // release resources held by stmt in C heap

	//===== read all Coldefs, and put coldefs_list in all_coldefs_map =====

	all_base_coldefs_map = make(map[int64][]*rsql.Coldef, 200) // 200 is a good enough default capacity

	if stmt, err = master.Prepare("SELECT c_tblid, c_base_seqno, c_colname, c_colname_original, c_datatype, c_precision, c_scale, c_fixlen_flag, c_collation_strength, c_collation, c_autonum, c_NOT_NULL_flag FROM syscoldefs ORDER BY c_tblid, c_base_seqno"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read coldef record from master

		if err = stmt.Scan(&c_tblid, &c_base_seqno, &c_colname, &c_colname_original, &c_datatype, &c_precision, &c_scale, &c_fixlen_flag, &c_collation_strength, &c_collation, &c_autonum, &c_NOT_NULL_flag); err != nil {
			break
		}

		// create new entry in all_coldefs_map if needed

		if c_base_seqno == 0 {
			if base_coldefs_list_count != 0 {
				all_base_coldefs_map[current_tblid] = base_coldefs_list // save previous coldefs_list
			}
			base_coldefs_list = make([]*rsql.Coldef, 0, rsql.DEFAULT_COLDEF_SLICE_CAPACITY) // create new coldefs_list
			last_base_seqno = -1
			current_tblid = c_tblid
			base_coldefs_list_count++
		}

		// fill in Coldef

		coldef := &rsql.Coldef{
			Cd_colname:          c_colname,
			Cd_colname_original: c_colname_original,

			Cd_datatype:           rsql.Datatype_t(c_datatype),
			Cd_precision:          uint16(c_precision),
			Cd_scale:              uint16(c_scale),
			Cd_fixlen_flag:        c_fixlen_flag != 0,
			Cd_collation_strength: rsql.Collstrength_t(c_collation_strength),
			Cd_collation:          c_collation,

			Cd_autonum:       rsql.Cd_autonum_t(c_autonum),
			Cd_NOT_NULL_flag: c_NOT_NULL_flag != 0,

			Cd_base_seqno: uint16(c_base_seqno),
		}

		base_coldefs_list = append(base_coldefs_list, coldef)

		rsql.Assert(c_tblid == current_tblid)
		rsql.Assert(c_base_seqno == last_base_seqno+1)
		last_base_seqno = c_base_seqno
	}

	if base_coldefs_list_count != 0 {
		all_base_coldefs_map[current_tblid] = base_coldefs_list // save last base_coldefs_list
	}
	last_base_seqno = -1
	current_tblid = 0
	base_coldefs_list = nil

	rsql.Assert(len(all_base_coldefs_map) == base_coldefs_list_count)

	stmt.Close() // release resources held by stmt in C heap

	//===== read all Tabledefs from master =====

	if stmt, err = master.Prepare("SELECT tbl_tblid, tbl_dbid, tbl_schid, tbl_base_gtblid, tbl_index_name, tbl_index_name_original, tbl_type, tbl_index_type, tbl_coldefs, tbl_nk, tbl_payload, tbl_status FROM systabledefs"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read tabledef record from master

		if err = stmt.Scan(&tbl_tblid, &tbl_dbid, &tbl_schid, &tbl_base_gtblid, &tbl_index_name, &tbl_index_name_original, &tbl_type, &tbl_index_type, &tbl_coldefs, &tbl_nk, &tbl_payload, &tbl_status); err != nil {
			break
		}

		// fill in Tabledef

		if base_coldefs_list, ok = all_base_coldefs_map[tbl_base_gtblid]; ok == false {
			log.Panicf("MASTER: coldefs_list not found for table %d", tbl_dbid)
		}

		switch tbl_type {
		case "BASE":
			rsql.Assert(tbl_tblid == tbl_base_gtblid)
			table_file_path = rsql.Tabledef_path(rsql.DIRECTORY_DATA, tbl_dbid, tbl_schid, tbl_tblid)
			td_type = rsql.TD_TYPE_BASE_TABLE
			td_coldefs = base_coldefs_list
			td_nk = seqnos_to_coldefs(base_coldefs_list, tbl_nk)
			td_payload = nil
			td_cluster_type = rsql.TD_CLUSTERED

		case "INDEX":
			rsql.Assert(tbl_tblid != tbl_base_gtblid)
			table_file_path = rsql.Tabledef_path(rsql.DIRECTORY_INDEX, tbl_dbid, tbl_schid, tbl_tblid)
			td_type = rsql.TD_TYPE_INDEX_TABLE
			td_coldefs = seqnos_to_coldefs(base_coldefs_list, tbl_coldefs)
			td_nk = seqnos_to_coldefs(base_coldefs_list, tbl_nk)
			td_payload = seqnos_to_coldefs(base_coldefs_list, tbl_payload)
			td_cluster_type = rsql.TD_NONCLUSTERED

		default:
			panic("impossible")
		}

		switch tbl_index_type {
		case "UNIQ":
			td_index_type = rsql.TD_UNIQUE
		case "PK":
			td_index_type = rsql.TD_PRIMARY_KEY
		case "IDX":
			td_index_type = rsql.TD_INDEX
		default:
			log.Panicf("MASTER: bad index_type \"%s\".", tbl_index_type)
		}

		tabledef := &rsql.Tabledef{
			Td_tblid: tbl_tblid,

			Td_dbid:                tbl_dbid,
			Td_schid:               tbl_schid,
			Td_base_gtblid:         tbl_base_gtblid,
			Td_index_name:          tbl_index_name,
			Td_index_name_original: tbl_index_name_original,

			Td_file_path: table_file_path,

			Td_durability:   rsql.DURABILITY_PERMANENT,
			Td_type:         td_type,
			Td_index_type:   td_index_type,
			Td_cluster_type: td_cluster_type,

			Td_coldefs: td_coldefs,
			Td_nk:      td_nk,
			Td_payload: td_payload,

			Td_status: rsql.Td_status_t(tbl_status),
		}

		if gtabledef, ok = systables.gtblid_map[tbl_base_gtblid]; ok == false {
			log.Panicf("MASTER: missing gtabledef %d.", tbl_base_gtblid)
		}

		switch td_type {
		case rsql.TD_TYPE_BASE_TABLE:
			gtabledef.Base = tabledef
			for _, coldef := range tabledef.Td_coldefs { // fill in gtabledef.Colmap
				gtabledef.Colmap[coldef.Cd_colname] = coldef
			}

		case rsql.TD_TYPE_INDEX_TABLE:
			gtabledef.Indexmap[tabledef.Td_index_name] = tabledef // fill in gtabledef.Indexmap

		default:
			panic("impossible")
		}

		if tbl_tblid > systables.objid_nextval {
			systables.objid_nextval = tbl_tblid
		}
	}

	switch {
	case systables.objid_nextval < TBLID_START:
		systables.objid_nextval = TBLID_START
	default:
		systables.objid_nextval++
	}

	stmt.Close() // release resources held by stmt in C heap
}

func (dictmgr *Dictionary_manager) DB_insert_GTabledef(gtabledef *rsql.GTabledef) {

	rsql.Assert(dictmgr.dm_locked == true)

	// write gtabledef

	args := sqlite3.NamedArgs{
		"$gtbl_gtblid":              gtabledef.Gtblid,
		"$gtbl_dbid":                gtabledef.Gdbid,
		"$gtbl_schid":               gtabledef.Gschid,
		"$gtbl_table_name":          gtabledef.Gtable_name,
		"$gtbl_table_name_original": gtabledef.Gtable_name_original,
		"$gtbl_identity_seed":       gtabledef.Identity_seed,
		"$gtbl_identity_increment":  gtabledef.Identity_increment,
		"$gtbl_create_date":         format_time(gtabledef.Create_date),
	}
	dictmgr.Execute(DICT_STMT_SYSGTABLEDEFS_INSERT, args)
}

func (dictmgr *Dictionary_manager) DB_insert_Tabledef(tabledef *rsql.Tabledef) {
	var (
		coldef       *rsql.Coldef
		itbl_coldefs interface{}
		itbl_nk      interface{}
		itbl_payload interface{}
	)

	rsql.Assert(dictmgr.dm_locked == true)

	// write tabledef

	switch tabledef.Td_type {
	case rsql.TD_TYPE_BASE_TABLE:
		itbl_coldefs = nil
		itbl_nk = nil_if_empty_string(coldefs_to_seqnos(tabledef.Td_nk))
		itbl_payload = nil

	case rsql.TD_TYPE_INDEX_TABLE:
		itbl_coldefs = nil_if_empty_string(coldefs_to_seqnos(tabledef.Td_coldefs))
		itbl_nk = nil_if_empty_string(coldefs_to_seqnos(tabledef.Td_nk))
		itbl_payload = nil_if_empty_string(coldefs_to_seqnos(tabledef.Td_payload))

	default:
		panic("impossible")
	}

	args := sqlite3.NamedArgs{
		"$tbl_tblid":               int64(tabledef.Td_tblid),
		"$tbl_dbid":                int64(tabledef.Td_dbid),
		"$tbl_schid":               int64(tabledef.Td_schid),
		"$tbl_base_gtblid":         int64(tabledef.Td_base_gtblid),
		"$tbl_index_name":          tabledef.Td_index_name,
		"$tbl_index_name_original": tabledef.Td_index_name_original,
		"$tbl_type":                td2tbl_type(tabledef.Td_type),
		"$tbl_index_type":          td2tbl_index_type(tabledef.Td_index_type),
		"$tbl_coldefs":             itbl_coldefs,
		"$tbl_nk":                  itbl_nk,
		"$tbl_payload":             itbl_payload,
		"$tbl_status":              int64(tabledef.Td_status),
		"$tbl_status_text":         tabledef.Td_status.String(),
	}
	dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_INSERT, args)

	// write all columns only for base table

	if tabledef.Td_type == rsql.TD_TYPE_BASE_TABLE {
		for _, coldef = range tabledef.Td_coldefs {
			args := sqlite3.NamedArgs{
				"$c_tblid":              int64(tabledef.Td_tblid),
				"$c_base_seqno":         int64(coldef.Cd_base_seqno),
				"$c_colname":            coldef.Cd_colname,
				"$c_colname_original":   coldef.Cd_colname_original,
				"$c_datatype":           int64(coldef.Cd_datatype),
				"$c_precision":          int64(coldef.Cd_precision),
				"$c_scale":              int64(coldef.Cd_scale),
				"$c_fixlen_flag":        coldef.Cd_fixlen_flag,
				"$c_collation_strength": int64(coldef.Cd_collation_strength),
				"$c_collation":          nil_if_empty_string(coldef.Cd_collation),
				"$c_autonum":            int64(coldef.Cd_autonum),
				"$c_NOT_NULL_flag":      coldef.Cd_NOT_NULL_flag,
			}
			dictmgr.Execute(DICT_STMT_SYSCOLDEFS_INSERT, args)
		}
	}
}

func (dictmgr *Dictionary_manager) DB_change_GTabledef_status_all(gtabledef *rsql.GTabledef, status rsql.Td_status_t) {

	rsql.Assert(dictmgr.dm_locked == true)

	args := sqlite3.NamedArgs{
		"$tbl_tblid":       gtabledef.Base.Td_tblid,
		"$tbl_status":      int64(status),
		"$tbl_status_text": status.String(),
	}
	dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_UPDATE_STATUS, args) // status changes

	for _, indexdef := range gtabledef.Indexmap { // for all index tables
		args := sqlite3.NamedArgs{
			"$tbl_tblid":       indexdef.Td_tblid,
			"$tbl_status":      int64(status),
			"$tbl_status_text": status.String(),
		}
		dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_UPDATE_STATUS, args) // status changes
	}
}

func (dictmgr *Dictionary_manager) DB_change_Tabledef_status(tabledef *rsql.Tabledef, status rsql.Td_status_t) {

	rsql.Assert(dictmgr.dm_locked == true)

	args := sqlite3.NamedArgs{
		"$tbl_tblid":       tabledef.Td_tblid,
		"$tbl_status":      int64(status),
		"$tbl_status_text": status.String(),
	}
	dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_UPDATE_STATUS, args) // status changes
}

func (dictmgr *Dictionary_manager) DB_insert_GTabledef_all(gtabledef *rsql.GTabledef) {

	rsql.Assert(dictmgr.dm_locked == true)

	dictmgr.DB_insert_GTabledef(gtabledef) // write gtabledef

	dictmgr.DB_insert_Tabledef(gtabledef.Base) // write base table

	for _, indexdef := range gtabledef.Indexmap { // write all index tables
		dictmgr.DB_insert_Tabledef(indexdef)
	}
}

func (dictmgr *Dictionary_manager) DB_delete_GTabledef_all(gtblid int64) {

	rsql.Assert(dictmgr.dm_locked == true)

	args := sqlite3.NamedArgs{
		"$gtblid": gtblid,
	}
	dictmgr.Execute(DICT_STMT_SYSGTABLEDEFS_DELETE, args)
	dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_DELETE_GTBLID, args)
	dictmgr.Execute(DICT_STMT_SYSCOLDEFS_DELETE_GTBLID, args)

}

func (dictmgr *Dictionary_manager) DB_delete_Tabledef(tblid int64) {

	rsql.Assert(dictmgr.dm_locked == true)

	args := sqlite3.NamedArgs{
		"$tblid": tblid,
	}
	dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_DELETE, args)

}

// used when the native key is changed.
// The number of files and their names is not changed. Only their content is modified to reflect the new table layout.
//
// THIS FUNCTION IS NOT USED ANY MORE.
//
func (dictmgr *Dictionary_manager) DB_substitute_GTabledef(context *rsql.Context, gtabledef *rsql.GTabledef) (wcache_new *cache.Wcache, rsql_err *rsql.Error) {
	var (
		wcache *cache.Wcache
	)

	rsql.Assert(dictmgr.dm_locked == true)

	wcache = context.Ctx_wcache.(*cache.Wcache)

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	// write to master.db

	dictmgr.DB_change_GTabledef_status_all(gtabledef, rsql.TD_STATUS_BEING_DROPPED)

	dictmgr.COMMIT_and_BEGIN_EXCLUSIVE() // first COMMIT, don't unlock dictmgr, and BEGIN EXCLUSIVE

	//====================== delete and recreate table files ======================

	filebag := wcache.Filebag()

	filebag.Close_file(gtabledef.Base)
	rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_DATA, gtabledef.Base.Td_dbid, gtabledef.Base.Td_schid, gtabledef.Base.Td_tblid)) // remove base table file of old gtabledef

	for _, indexdef := range gtabledef.Indexmap { // remove all index tables
		filebag.Close_file(indexdef)
		rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid)) // remove index table files of old gtabledef
	}

	rsql.Must_create_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_DATA, gtabledef.Base.Td_dbid, gtabledef.Base.Td_schid, gtabledef.Base.Td_tblid), rsql.TBLFILE_PERM) // create base table file
	if rsql_err = wcache.Create_info_page_and_first_zone_allocator(gtabledef.Base); rsql_err != nil {
		return wcache, rsql_err
	}

	for _, indexdef := range gtabledef.Indexmap { // create all index tables
		rsql.Must_create_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid), rsql.TBLFILE_PERM) // create index table files
		if rsql_err = wcache.Create_info_page_and_first_zone_allocator(indexdef); rsql_err != nil {
			return wcache, rsql_err
		}
	}

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	wcache.COMMIT() // table pages are written to disk

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	// write to master.db

	dictmgr.DB_delete_GTabledef_all(gtabledef.Gtblid) // delete old gtabledef completely from master.db

	dictmgr.DB_insert_GTabledef_all(gtabledef) // write new gtabledef with updated native key

	return wcache, nil
}
