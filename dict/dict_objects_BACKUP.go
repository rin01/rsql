package dict

import (
	"sort"

	"golang.org/x/text/collate"

	"rsql"
)

type Backup_permission struct {
	Pm_principal string
	Pm_grant     rsql.Permission_t
	Pm_deny      rsql.Permission_t
}

type Backup_gtabledef_item struct {
	Gt_gtabledef   *rsql.GTabledef
	Gt_permissions []*Backup_permission
}

type Backup_principal_item struct {
	Pal_database_name   string
	Pal_name            string
	Pal_palid           int64
	Pal_type            Pal_type_t
	Pal_login_id        int64
	Pal_system_flag     Pal_sysflag_t
	Pal_member_of       []string
	Lg_name             string // this field and following only for users, not for roles
	Lg_password_hash    string
	Lg_default_database string
	Lg_default_language string
	Lg_disabled_flag    bool
	Lg_system_flag      Lg_sysflag_t // LG_SYSFLAG_SA for 'sa', else 0
}

// Backup_list_of_principals is used to sort the list of principals by alphabetical order.
//
type Backup_list_of_principals struct { // implements Sort interface
	lst  []*Backup_principal_item
	coll *collate.Collator
}

func New_Backup_list_of_principals(lst []*Backup_principal_item, coll *collate.Collator) Backup_list_of_principals {

	return Backup_list_of_principals{lst: lst, coll: coll}
}

func (list Backup_list_of_principals) Len() int {

	return len(list.lst)
}

func (list Backup_list_of_principals) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list Backup_list_of_principals) Less(i, j int) bool {
	var (
		res int
	)

	// sort by database name

	if list.lst[i].Pal_database_name != list.lst[j].Pal_database_name {
		res = list.coll.CompareString(list.lst[i].Pal_database_name, list.lst[j].Pal_database_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	// sort by roles and users

	if list.lst[i].Pal_type != list.lst[j].Pal_type {
		return list.lst[i].Pal_type > list.lst[j].Pal_type
	}

	// sort by system flag

	if list.lst[i].Pal_system_flag != list.lst[j].Pal_system_flag {
		return list.lst[i].Pal_system_flag > list.lst[j].Pal_system_flag // public and dbo are listed first
	}

	// sort by principal names

	if list.lst[i].Pal_name != list.lst[j].Pal_name {
		res = list.coll.CompareString(list.lst[i].Pal_name, list.lst[j].Pal_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	return list.lst[i].Pal_palid < list.lst[j].Pal_palid
}

// Retrieve_sorted_list_of_principals_gtabledefs_for_BACKUP retrieves all information about the database: logins, users, roles, gtabledefs and permissions.
//
func (dictmgr *Dictionary_manager) Retrieve_sorted_list_of_principals_gtabledefs_for_BACKUP(context *rsql.Context, database_name string, params rsql.Backup_params) (list_principals []*Backup_principal_item, list_gtabledefs_perms []*Backup_gtabledef_item, rsql_err *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 40
	)

	var (
		database *Sysdatabase
		dbid     int64
		coll     *collate.Collator
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// only SA or DBO of any database is allowed

	if rsql_err := dictmgr.check_is_SA_or_any_DBO(context); rsql_err != nil {
		return nil, nil, rsql_err
	}

	// get collator

	if coll, rsql_err = rsql.Get_collator(params.Bk_server_default_collation); rsql_err != nil {
		return nil, nil, rsql_err
	}

	// get dbid

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil {
		return nil, nil, rsql_err
	}

	dbid = database.Db_dbid

	//====== make list of principals ======

	list_principals = make([]*Backup_principal_item, 0, LIST_DEFAULT_CAPACITY)

	for _, principal := range dictmgr.dm_SYSPRINCIPALS.palid_map {
		if principal.Pal_dbid != dbid {
			continue
		}

		var login *Syslogin
		var ok bool
		var member_of []string

		member_of = member_of_list(principal, coll)

		item := &Backup_principal_item{
			Pal_database_name: database_name,
			Pal_name:          principal.Pal_name,
			Pal_palid:         principal.Pal_palid,
			Pal_type:          principal.Pal_type,
			Pal_login_id:      principal.Pal_login_id,
			Pal_system_flag:   principal.Pal_system_flag,
			Pal_member_of:     member_of,
		}

		if principal.Pal_type == PAL_TYPE_USER {
			if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[principal.Pal_login_id]; ok == false {
				continue // should never happen
			}

			item.Lg_name = login.Lg_name
			item.Lg_password_hash = login.Lg_password_hash
			item.Lg_default_database = login.Lg_default_database
			item.Lg_default_language = login.Lg_default_language
			item.Lg_disabled_flag = login.Lg_disabled_flag
			item.Lg_system_flag = login.Lg_system_flag
		}

		list_principals = append(list_principals, item)
	}

	// sort the list

	sort.Sort(New_Backup_list_of_principals(list_principals, coll))

	//====== list of gtabledefs ======

	// make list of table names

	list_gtabledefs := make([]*rsql.GTabledef, 0, LIST_DEFAULT_CAPACITY)

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
		if gtabledef.Gdbid != dbid {
			continue
		}

		list_gtabledefs = append(list_gtabledefs, gtabledef)
	}

	sort.Sort(rsql.New_GTabledefs_by_name(list_gtabledefs, coll))

	// create the final list of GTabledefs

	gtblid_map := make(map[int64]*Backup_gtabledef_item, len(list_gtabledefs)) // a map used for lookup in list_gtabledefs_perms

	list_gtabledefs_perms = make([]*Backup_gtabledef_item, 0, len(list_gtabledefs))

	for _, gtabledef := range list_gtabledefs {
		item := &Backup_gtabledef_item{Gt_gtabledef: gtabledef}

		list_gtabledefs_perms = append(list_gtabledefs_perms, item)
		gtblid_map[gtabledef.Gtblid] = item
	}

	//====== permissions ======

	for key, perm := range dictmgr.dm_SYSPERMISSIONS.key_map {
		if item, ok := gtblid_map[key.Objid]; ok == true { // found a permission entry for a table that we want to backup
			var principal *Sysprincipal

			if principal, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[key.Palid]; ok == false {
				continue // should not happen
			}

			if principal.Pal_dbid != dbid {
				continue // should not happen
			}

			item.Gt_permissions = append(item.Gt_permissions, &Backup_permission{Pm_principal: principal.Pal_name, Pm_grant: perm.Pm_grant, Pm_deny: perm.Pm_deny})
		}
	}

	return list_principals, list_gtabledefs_perms, nil
}
