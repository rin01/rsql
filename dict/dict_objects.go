package dict

import (
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/mxk/go-sqlite/sqlite3"

	"rsql"
	"rsql/cache"
	"rsql/hashing"
	"rsql/lang"
	"rsql/lex"
	"rsql/lk"
)

/*  'sa', 'dbo', 'public'
    =====================

        - Login 'sa' can do everything: changing system parameters, create logins, databases, and has implicitly the same rights as a database owner.

        - Each database has a user 'dbo' and a role 'public'.
        - The user 'dbo' is the user that owns the database. It can creates users, roles, tables, permissions in this database.
        - The user 'dbo' is linked by default to login 'sa', but it can be changed by ALTER AUTHORIZATION.

        - Login 'sa' can only be linked to user 'dbo', by using ALTER AUTHORIZATION.
        - If login 'sa' is not linked to 'dbo', 'sa' is no more linked to any user in the database. But it is not important because 'sa' bypasses all security checks, and if its user is requested, 'dbo' is returned.

        - All users are implicitly members of role 'public'.

    'trashdb'
    =========

        - trashdb is a database which you can use or not, as you like.
        - it only exists because the server needs at least one database to work, there is no other reason.
        - when you create a login, the default database is set to 'trashdb', but the connection will fail if you don't create also a user in trashdb for this login.
        - 'sa' can connect without problem to any database, and also to 'trashdb'. The user will be 'dbo'.

*/

//================== Context ===================

func (dictmgr *Dictionary_manager) Authenticate_login(login_name string, password string) (login_name_lc string, login_id int64, login_default_language string, login_default_database string, rsql_err *rsql.Error) {
	var (
		login *Syslogin
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	login_name_lc = strings.ToLower(login_name)

	if login, rsql_err = dictmgr.get_login(login_name_lc); rsql_err != nil {
		return "", 0, "", "", rsql_err
	}

	if hashing.Compare_hash_and_password(login.Lg_password_hash, password) == false {
		return "", 0, "", "", rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_AUTHENTICATION_FAILED, rsql.ERROR_BATCH_ABORT)
	}

	if login.Lg_disabled_flag == true {
		return "", 0, "", "", rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_DISABLED, rsql.ERROR_BATCH_ABORT, login.Lg_name)
	}

	return login_name_lc, login.Lg_login_id, login.Lg_default_language, login.Lg_default_database, nil
}

// check_is_SA checks if Context login is 'sa'.
//
//     IMPORTANT: the login corresponding to context.Ctx_login_id may have been dropped by this session or another session.
//                In this case, the function just returns an error.
//
func (dictmgr *Dictionary_manager) check_is_SA(context *rsql.Context) *rsql.Error {
	var (
		login *Syslogin
		ok    bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[context.Ctx_login_id]; ok == false { // login may have been dropped
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA, rsql.ERROR_BATCH_ABORT)
	}

	if login.Lg_system_flag == LG_SYSFLAG_SA {
		return nil // login is 'sa'
	}

	return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA, rsql.ERROR_BATCH_ABORT)
}

// check_is_SA_or_DBO checks if Context login is 'sa', or is the login linked to the 'dbo' user of the given database.
//
//     IMPORTANT: the login corresponding to context.Ctx_login_id may have been dropped by this session or another session.
//                In this case, the function just returns an error.
//
func (dictmgr *Dictionary_manager) check_is_SA_or_DBO(context *rsql.Context, dbid int64) *rsql.Error {
	var (
		login *Syslogin
		user  *Sysprincipal
		palid int64
		ok    bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[context.Ctx_login_id]; ok == false { // login may have been dropped
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA_OR_DBO, rsql.ERROR_BATCH_ABORT)
	}

	if login.Lg_system_flag == LG_SYSFLAG_SA {
		return nil // login is 'sa'
	}

	if palid, ok = dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: login.Lg_login_id, dbid: dbid}]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA_OR_DBO, rsql.ERROR_BATCH_ABORT) // login has no user for this database
	}

	if user, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[palid]; ok == false {
		log.Panicf("user with palid %d not found", palid)
	}

	if user.Pal_system_flag == PAL_SYSFLAG_USER_DBO {
		return nil // login is owner of dbid database
	}

	return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA_OR_DBO, rsql.ERROR_BATCH_ABORT)
}

// check_is_SA_or_any_DBO checks if Context login is 'sa', or is the login linked to the 'dbo' user of any database.
//
//     IMPORTANT: the login corresponding to context.Ctx_login_id may have been dropped by this session or another session.
//                In this case, the function just returns an error.
//
func (dictmgr *Dictionary_manager) check_is_SA_or_any_DBO(context *rsql.Context) *rsql.Error {
	var (
		login *Syslogin
		user  *Sysprincipal
		palid int64
		ok    bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[context.Ctx_login_id]; ok == false { // login may have been dropped
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA_OR_ANY_DBO, rsql.ERROR_BATCH_ABORT)
	}

	if login.Lg_system_flag == LG_SYSFLAG_SA {
		return nil // login is 'sa'
	}

	// check if login is dbo of any database

	for _, database := range dictmgr.dm_SYSDATABASES.dbid_map {
		if palid, ok = dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: login.Lg_login_id, dbid: database.Db_dbid}]; ok == false {
			continue // login has no user for this database
		}

		if user, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[palid]; ok == false {
			log.Panicf("user with palid %d not found", palid)
		}

		if user.Pal_system_flag == PAL_SYSFLAG_USER_DBO {
			return nil // login is owner of dbid database
		}
	}

	return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA_OR_ANY_DBO, rsql.ERROR_BATCH_ABORT)
}

func (dictmgr *Dictionary_manager) Check_is_SA_or_DBO(context *rsql.Context, database_name string) *rsql.Error {
	var (
		rsql_err *rsql.Error
		login    *Syslogin
		ok       bool
		database *Sysdatabase
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// check if sa

	if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[context.Ctx_login_id]; ok == false { // login may have been dropped
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA_OR_DBO, rsql.ERROR_BATCH_ABORT)
	}

	if login.Lg_system_flag == LG_SYSFLAG_SA {
		return nil // login is 'sa'
	}

	// retrieve dbid

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	// only SA or DBO is allowed

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func (dictmgr *Dictionary_manager) Check_is_SA(context *rsql.Context) *rsql.Error {

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// check if sa

	return dictmgr.check_is_SA(context)
}

//================== auxilliary login->user map ===================

// Map_login_dbid_user store the user palid for each key <login_id, dbid>.
// It is a helper map, to accelerate the lookup.
// Without it, we would have to lookup all elements in MASTER.dm_SYSPRINCIPALS.
//
// It is updated by CREATE USER, ALTER USER, DROP USER, and also by CREATE DATABASE  ('dbo' user is created), and DROP DATABASE (all users are deleted).
//
type Map_login_dbid_user map[Map_login_dbid_key]int64

type Map_login_dbid_key struct {
	login_id int64
	dbid     int64
}

//================== misc functions ===================

func now() time.Time {
	var (
		t   time.Time
		res time.Time
	)

	t = time.Now()

	year, month, day := t.Date()
	hour, minute, second := t.Clock()
	nanosecond := t.Nanosecond()
	res = time.Date(year, month, day, hour, minute, second, nanosecond, time.UTC)

	return res
}

func must_parse_time(s string) time.Time {
	var (
		err error
		t   time.Time
	)

	if t, err = time.Parse("2006-01-02 15:04:05", s); err != nil {
		log.Panic(err)
	}

	return t
}

func format_time(t time.Time) string {

	s := t.Format("2006-01-02 15:04:05")

	return s
}

// login_id_exists_in_sysprincipals checks if login_id is referenced by a user of any database.
//
func (dictmgr *Dictionary_manager) login_id_exists_in_sysprincipals(login_id int64) (db_name string, user_name string, ok bool) {

	rsql.Assert(dictmgr.dm_locked == true)

	sysprincipals := &dictmgr.dm_SYSPRINCIPALS

	for _, user := range sysprincipals.palid_map {
		if user.Pal_type == PAL_TYPE_ROLE { // roles have no login
			continue
		}

		if user.Pal_login_id == login_id {
			db_name = dictmgr.database_name(user.Pal_dbid)
			user_name = user.Pal_name
			return db_name, user_name, true
		}
	}

	return "", "", false
}

// login_id_exists_in_database checks whether login_id is already referenced by a user of database dbid.
//
func (dictmgr *Dictionary_manager) login_id_exists_in_database(dbid int64, login_id int64) (db_name string, user_name string, ok bool) {

	rsql.Assert(dictmgr.dm_locked == true)

	sysprincipals := &dictmgr.dm_SYSPRINCIPALS

	for _, user := range sysprincipals.palid_map {
		if user.Pal_type == PAL_TYPE_ROLE {
			continue
		}

		if user.Pal_dbid == dbid && user.Pal_login_id == login_id {
			db_name = dictmgr.database_name(user.Pal_dbid)
			user_name = user.Pal_name
			return db_name, user_name, true
		}
	}

	return "", "", false
}

func (dictmgr *Dictionary_manager) login_name(login_id int64) string {
	var (
		login *Syslogin
		ok    bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	syslogins := &dictmgr.dm_SYSLOGINS

	if login, ok = syslogins.login_id_map[login_id]; ok == false {
		log.Panicf("login login_id %d not found", login_id)
	}

	return login.Lg_name
}

func (dictmgr *Dictionary_manager) principal_name(palid int64) string {
	var (
		principal *Sysprincipal
		ok        bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	sysprincipals := &dictmgr.dm_SYSPRINCIPALS

	if principal, ok = sysprincipals.palid_map[palid]; ok == false {
		log.Panicf("principal palid %d not found", palid)
	}

	return principal.Pal_name
}

func (dictmgr *Dictionary_manager) database_name(dbid int64) string {
	var (
		database *Sysdatabase
		ok       bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	sysdatabases := &dictmgr.dm_SYSDATABASES

	if database, ok = sysdatabases.dbid_map[dbid]; ok == false {
		log.Panicf("database dbid %d not found", dbid)
	}

	return database.Db_name
}

func (dictmgr *Dictionary_manager) schema_name(dbid int64, schid int64) string {
	var (
		schema *Sysschema
		ok     bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	sysschemas := &dictmgr.dm_SYSSCHEMAS

	if schema, ok = sysschemas.schid_map[Sysschema_schidkey_t{Sch_dbid: dbid, Sch_schid: schid}]; ok == false {
		log.Panicf("schema dbid %d schid %d not found", dbid, schid)
	}

	return schema.Sch_name
}

func (dictmgr *Dictionary_manager) gtable_name(gtblid int64) string {
	var (
		gtabledef *rsql.GTabledef
		ok        bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	systables := &dictmgr.dm_SYSTABLES

	if gtabledef, ok = systables.gtblid_map[gtblid]; ok == false {
		log.Panicf("gtabledef gtblid %d not found", gtblid)
	}

	return gtabledef.Gtable_name
}

func (dictmgr *Dictionary_manager) get_database(database_name string) (*Sysdatabase, *rsql.Error) {
	var (
		database *Sysdatabase
		ok       bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	sysdatabases := &dictmgr.dm_SYSDATABASES

	if database, ok = sysdatabases.name_map[database_name]; ok == false {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name)
	}

	return database, nil
}

func (dictmgr *Dictionary_manager) get_schema(database_name string, schema_name string) (*Sysdatabase, *Sysschema, *rsql.Error) {
	var (
		database *Sysdatabase
		schema   *Sysschema
		ok       bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	sysdatabases := &dictmgr.dm_SYSDATABASES
	sysschemas := &dictmgr.dm_SYSSCHEMAS

	if database, ok = sysdatabases.name_map[database_name]; ok == false {
		return nil, nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name)
	}

	if schema, ok = sysschemas.name_map[Sysschema_namekey_t{Sch_dbid: database.Db_dbid, Sch_name: schema_name}]; ok == false {
		return nil, nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_SCHEMA_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database.Db_name, schema_name)
	}

	return database, schema, nil
}

func (dictmgr *Dictionary_manager) get_database_and_schema_name(dbid int64, schid int64) (database_name string, schema_name string) {
	var (
		database *Sysdatabase
		schema   *Sysschema
		ok       bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	if database, ok = dictmgr.dm_SYSDATABASES.dbid_map[dbid]; ok == false {
		log.Panicf("database with dbid %d not found", dbid)
	}

	if schema, ok = dictmgr.dm_SYSSCHEMAS.schid_map[Sysschema_schidkey_t{Sch_dbid: dbid, Sch_schid: schid}]; ok == false {
		log.Panicf("schema with dbid %d and schid %d not found", dbid, schid)
	}

	return database.Db_name, schema.Sch_name
}

func (dictmgr *Dictionary_manager) get_login(login_name string) (*Syslogin, *rsql.Error) {
	var (
		login *Syslogin
		ok    bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	syslogins := &dictmgr.dm_SYSLOGINS

	if login, ok = syslogins.name_map[login_name]; ok == false {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, login_name)
	}

	return login, nil
}

func (dictmgr *Dictionary_manager) get_login_from_user_name(dbid int64, user_name string) (*Syslogin, *rsql.Error) {
	var (
		database *Sysdatabase
		user     *Sysprincipal
		login    *Syslogin
		ok       bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	syslogins := &dictmgr.dm_SYSLOGINS
	sysprincipals := &dictmgr.dm_SYSPRINCIPALS
	sysdatabases := &dictmgr.dm_SYSDATABASES

	if database, ok = sysdatabases.dbid_map[dbid]; ok == false {
		log.Panicf("database with dbid %d not found", dbid)
	}

	if user, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: dbid, Pal_name: user_name}]; ok == false {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database.Db_name, user_name)
	}

	if user.Pal_type != PAL_TYPE_USER {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_IS_ROLE, rsql.ERROR_BATCH_ABORT, database.Db_name, user_name)
	}

	if login, ok = syslogins.login_id_map[user.Pal_login_id]; ok == false {
		log.Panicf("login with login_id %d not found", user.Pal_login_id)
	}

	return login, nil
}

func (dictmgr *Dictionary_manager) database_schema_gtabledef_from_qname(gtabledef_qname rsql.Object_qname_t) (*Sysdatabase, *Sysschema, *rsql.GTabledef, *rsql.Error) {
	var (
		database_name  string
		schema_name    string
		gtabledef_name string
		database       *Sysdatabase
		schema         *Sysschema
		gtabledef      *rsql.GTabledef
		ok             bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	database_name = gtabledef_qname.Database_name
	schema_name = gtabledef_qname.Schema_name
	gtabledef_name = gtabledef_qname.Object_name

	sysdatabases := &dictmgr.dm_SYSDATABASES
	sysschemas := &dictmgr.dm_SYSSCHEMAS
	systables := &dictmgr.dm_SYSTABLES

	if database, ok = sysdatabases.name_map[database_name]; ok == false {
		return nil, nil, nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name)
	}

	if schema, ok = sysschemas.name_map[Sysschema_namekey_t{Sch_dbid: database.Db_dbid, Sch_name: schema_name}]; ok == false {
		return nil, nil, nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_SCHEMA_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, schema_name)
	}

	if gtabledef, ok = systables.name_map[Systable_namekey_t{Gdbid: database.Db_dbid, Gschid: schema.Sch_schid, Gtable_name: gtabledef_name}]; ok == false {
		return nil, nil, nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OBJECT_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, schema_name, gtabledef_name)
	}

	return database, schema, gtabledef, nil
}

func (dictmgr *Dictionary_manager) dbid_schid_gtblid_from_gtabledef_qname(gtabledef_qname rsql.Object_qname_t) (dbid int64, schid int64, gtblid int64, rsql_err *rsql.Error) {
	var (
		database  *Sysdatabase
		schema    *Sysschema
		gtabledef *rsql.GTabledef
	)

	rsql.Assert(dictmgr.dm_locked == true)

	if database, schema, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(gtabledef_qname); rsql_err != nil {
		return 0, 0, 0, rsql_err
	}

	return database.Db_dbid, schema.Sch_schid, gtabledef.Gtblid, nil
}

//================== SYSPARAMETERS ===================

type Param_update_option_t uint8

const (
	PARAM_UPDATE_AT_RESTART Param_update_option_t = iota // parameter is updated at next restart of the server
	PARAM_UPDATE_IMMEDIATE                               // parameter is updated immediately
)

type Server_parameter_t uint32

// List of server parameters.
//
// WARNING: IF YOU CHANGE THE NUMBERING, YOU MUST ALSO CHANGE THEM IN install_master_db.go.
//
const (
	PARAM_SERVER_DEFAULT_COLLATION Server_parameter_t = iota
	PARAM_SERVER_SERVERNAME
	PARAM_SERVER_WORKERS_MAX
	PARAM_SERVER_GLOBAL_PAGE_CACHE_MEMORY
	PARAM_SERVER_DEFAULT_DATABASE
	PARAM_SERVER_DEFAULT_LANGUAGE
	PARAM_SERVER_QUOTED_IDENTIFIER
	PARAM_SERVER_BULK_DIR
	PARAM_SERVER_DUMP_DIR
	PARAM_SERVER_READ_TIMEOUT
	PARAM_SERVER_LOCK_TICKER_INTERVAL
	PARAM_SERVER_LOCK_TIMEOUT_TICKS_COUNT
	PARAM_SERVER_LOGGING_MAX_SIZE
	PARAM_SERVER_LOGGING_MAX_COUNT
	PARAM_SERVER_LOGGING_LOCALTIME
	PARAM_SERVER_WCACHE_MEMORY_MAX
	PARAM_SERVER_WCACHE_MODIF_MAX
	PARAM_SERVER_BATCH_TEXT_MAX_SIZE
	PARAM_SERVER_BATCH_INSERTS_MAX_COUNT

	SPEC_SYSPARAMETERS_COLLECION_ARRAY_SIZE // size of array of Sysparameter
)

func (sp Server_parameter_t) String() string {
	var s string

	switch sp {
	case PARAM_SERVER_DEFAULT_COLLATION:
		s = "server_default_collation"
	case PARAM_SERVER_SERVERNAME:
		s = "server_servername"
	case PARAM_SERVER_WORKERS_MAX:
		s = "server_workers_max"
	case PARAM_SERVER_GLOBAL_PAGE_CACHE_MEMORY:
		s = "server_global_page_cache_memory"
	case PARAM_SERVER_DEFAULT_DATABASE:
		s = "server_default_database"
	case PARAM_SERVER_DEFAULT_LANGUAGE:
		s = "server_default_language"
	case PARAM_SERVER_QUOTED_IDENTIFIER:
		s = "server_quoted_identifier"
	case PARAM_SERVER_BULK_DIR:
		s = "server_bulk_dir"
	case PARAM_SERVER_DUMP_DIR:
		s = "server_dump_dir"
	case PARAM_SERVER_READ_TIMEOUT:
		s = "server_read_timeout"
	case PARAM_SERVER_LOCK_TICKER_INTERVAL:
		s = "server_lock_ticker_interval"
	case PARAM_SERVER_LOCK_TIMEOUT_TICKS_COUNT:
		s = "server_lock_timeout_ticks_count"
	case PARAM_SERVER_LOGGING_MAX_SIZE:
		s = "server_logging_max_size"
	case PARAM_SERVER_LOGGING_MAX_COUNT:
		s = "server_logging_max_count"
	case PARAM_SERVER_LOGGING_LOCALTIME:
		s = "server_logging_localtime"
	case PARAM_SERVER_WCACHE_MEMORY_MAX:
		s = "server_wcache_memory_max"
	case PARAM_SERVER_WCACHE_MODIF_MAX:
		s = "server_wcache_modif_max"
	case PARAM_SERVER_BATCH_TEXT_MAX_SIZE:
		s = "server_batch_text_max_size"
	case PARAM_SERVER_BATCH_INSERTS_MAX_COUNT:
		s = "server_batch_inserts_max_count"

	default:
		panic("impossible")
	}

	return s
}

type Sysparameters_collection struct {
	param_array []Sysparameter // the collection of parameters is just an array of size SPEC_SYSPARAMETERS_COLLECION_ARRAY_SIZE
}

type Sysparameter struct {
	Param_id                  Server_parameter_t
	Param_name                string
	Param_value_int           int64  // current parameter value
	Param_value_string        string // current parameter value
	Param_value_int_stored    int64  // parameter value as stored in master.db file
	Param_value_string_stored string // parameter value as stored in master.db file
}

func (param *Sysparameter) String() string {

	switch param.Param_id {
	case PARAM_SERVER_DEFAULT_COLLATION,
		PARAM_SERVER_SERVERNAME,
		PARAM_SERVER_DEFAULT_DATABASE,
		PARAM_SERVER_DEFAULT_LANGUAGE,
		PARAM_SERVER_BULK_DIR,
		PARAM_SERVER_DUMP_DIR:
		return param.Param_value_string

	case PARAM_SERVER_WORKERS_MAX,
		PARAM_SERVER_GLOBAL_PAGE_CACHE_MEMORY,
		PARAM_SERVER_QUOTED_IDENTIFIER,
		PARAM_SERVER_READ_TIMEOUT,
		PARAM_SERVER_LOCK_TICKER_INTERVAL,
		PARAM_SERVER_LOCK_TIMEOUT_TICKS_COUNT,
		PARAM_SERVER_LOGGING_MAX_SIZE,
		PARAM_SERVER_LOGGING_MAX_COUNT,
		PARAM_SERVER_LOGGING_LOCALTIME,
		PARAM_SERVER_WCACHE_MEMORY_MAX,
		PARAM_SERVER_WCACHE_MODIF_MAX,
		PARAM_SERVER_BATCH_TEXT_MAX_SIZE,
		PARAM_SERVER_BATCH_INSERTS_MAX_COUNT:
		return strconv.Itoa(int(param.Param_value_int))

	default:
		panic("unexpected ALTER SERVER PARAMETER")
	}
}

// Must_load_params_from_master_db reads the specified parameter directly from master.db.
//
// It is used during server initialization to get the logger rotation parameters.
//
func (dictmgr *Dictionary_manager) Must_load_params_from_master_db(param Server_parameter_t) (value_int int64, value_string string) {
	var (
		err         error
		rsql_err    *rsql.Error
		master      *sqlite3.Conn
		stmt        *sqlite3.Stmt
		param_count int

		param_id           int64
		param_name         string
		param_value_int    int64
		param_value_string string
	)

	// read all parameters from master

	master = dictmgr.dm_master

	if stmt, err = master.Prepare(fmt.Sprintf("SELECT param_id, param_name, param_value_int, param_value_string FROM sysparameters WHERE param_id=%d", param)); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read param record from master

		if err = stmt.Scan(&param_id, &param_name, &param_value_int, &param_value_string); err != nil {
			break
		}

		if param_id == int64(PARAM_SERVER_DEFAULT_LANGUAGE) {
			if param_value_string, rsql_err = lang.Normalize_language(param_value_string); rsql_err != nil { // just to be sure, normalize server default language
				log.Panicf("%s", rsql_err)
			}
		}

		param_count++
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	stmt.Close() // release resources held by stmt in C heap

	if param_count != 1 {
		log.Panicf("parameter %s not found in master.db", param.String())
	}

	return param_value_int, param_value_string
}

// Must_load_SYSPARAMETERS loads all parameters stored in master.db into dictmgr.
// It is done at init, when the server starts.
//
func (dictmgr *Dictionary_manager) Must_load_SYSPARAMETERS() {
	var (
		err           error
		rsql_err      *rsql.Error
		master        *sqlite3.Conn
		stmt          *sqlite3.Stmt
		sysparameters *Sysparameters_collection
		param_count   int

		param_id           int64
		param_name         string
		param_value_int    int64
		param_value_string string
	)

	// read all parameters from master

	master = dictmgr.dm_master
	sysparameters = &dictmgr.dm_SYSPARAMETERS

	sysparameters.param_array = make([]Sysparameter, SPEC_SYSPARAMETERS_COLLECION_ARRAY_SIZE)

	if stmt, err = master.Prepare("SELECT param_id, param_name, param_value_int, param_value_string FROM sysparameters"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read param record from master

		if err = stmt.Scan(&param_id, &param_name, &param_value_int, &param_value_string); err != nil {
			break
		}

		// fill in SYSPARAMETERS

		if param_id == int64(PARAM_SERVER_DEFAULT_LANGUAGE) {
			if param_value_string, rsql_err = lang.Normalize_language(param_value_string); rsql_err != nil { // just to be sure, normalize server default language
				log.Panicf("%s", rsql_err)
			}
		}

		parameter := Sysparameter{
			Param_id:                  Server_parameter_t(param_id),
			Param_name:                param_name,
			Param_value_int:           param_value_int,
			Param_value_string:        param_value_string,
			Param_value_int_stored:    param_value_int,
			Param_value_string_stored: param_value_string,
		}

		sysparameters.param_array[parameter.Param_id] = parameter // fill in SYSPARAMETERS

		param_count++
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	if param_count != int(SPEC_SYSPARAMETERS_COLLECION_ARRAY_SIZE) {
		log.Panicf("MASTER: sysparameters has capacity %d, but %d records have been read.", SPEC_SYSPARAMETERS_COLLECION_ARRAY_SIZE, param_count)
	}

	stmt.Close() // release resources held by stmt in C heap
}

func (dictmgr *Dictionary_manager) Get_sysparameters_int_value(param_id Server_parameter_t) int64 {
	var (
		sysparameters *Sysparameters_collection
		val           int64
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	sysparameters = &dictmgr.dm_SYSPARAMETERS

	val = sysparameters.param_array[param_id].Param_value_int

	return val
}

func (dictmgr *Dictionary_manager) Get_sysparameters_string_value(param_id Server_parameter_t) string {
	var (
		sysparameters *Sysparameters_collection
		val           string
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	sysparameters = &dictmgr.dm_SYSPARAMETERS

	val = sysparameters.param_array[param_id].Param_value_string

	return val
}

// Update_sysparameter_value_int implements ALTER SERVER PARAMETER statement, for changing integer value.
// Option is PARAM_UPDATE_AT_RESTART or PARAM_UPDATE_IMMEDIATE
//
func (dictmgr *Dictionary_manager) Update_sysparameter_value_int(context *rsql.Context, param_id Server_parameter_t, value int64, option Param_update_option_t) *rsql.Error {
	var (
		sysparameters *Sysparameters_collection
	)

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	sysparameters = &dictmgr.dm_SYSPARAMETERS
	// (no checking is done)

	if param_id < 0 || param_id >= SPEC_SYSPARAMETERS_COLLECION_ARRAY_SIZE {
		log.Panicf("server parameter %d unknown", param_id)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	sysparameters.param_array[param_id].Param_value_int_stored = value

	if option == PARAM_UPDATE_IMMEDIATE {
		sysparameters.param_array[param_id].Param_value_int = value
	}

	// write to disk

	args := sqlite3.NamedArgs{
		"$param_id":        int64(param_id),
		"$param_value_int": value,
	}
	dictmgr.Execute(DICT_STMT_SYSPARAMETERS_UPDATE_VALUE_INT, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Update_sysparameter_value_int implements ALTER SERVER PARAMETER statement, for changing string value.
// Option is PARAM_UPDATE_AT_RESTART or PARAM_UPDATE_IMMEDIATE
//
func (dictmgr *Dictionary_manager) Update_sysparameter_value_string(context *rsql.Context, param_id Server_parameter_t, value string, option Param_update_option_t) *rsql.Error {
	var (
		sysparameters *Sysparameters_collection
	)

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	sysparameters = &dictmgr.dm_SYSPARAMETERS

	if param_id < 0 || param_id >= SPEC_SYSPARAMETERS_COLLECION_ARRAY_SIZE {
		log.Panicf("server parameter %d unknown", param_id)
	}

	switch param_id { // do some checks
	case PARAM_SERVER_DEFAULT_DATABASE:
		sysdatabases := &dictmgr.dm_SYSDATABASES

		if _, ok := sysdatabases.name_map[value]; ok == false {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, value)
		}
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	sysparameters.param_array[param_id].Param_value_string_stored = value

	if option == PARAM_UPDATE_IMMEDIATE {
		sysparameters.param_array[param_id].Param_value_string = value
	}

	// write to disk

	args := sqlite3.NamedArgs{
		"$param_id":           int64(param_id),
		"$param_value_string": value,
	}
	dictmgr.Execute(DICT_STMT_SYSPARAMETERS_UPDATE_VALUE_STRING, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

//================== SYSLOGINS ===================

type Lg_sysflag_t int64

const (
	LG_SYSFLAG_SA Lg_sysflag_t = 1 << iota
)

type Syslogins_collection struct {
	login_id_map map[int64]*Syslogin
	name_map     map[string]*Syslogin

	login_id_nextval int64
}

type Syslogin struct {
	Lg_login_id         int64  // unique
	Lg_name             string // unique
	Lg_password_hash    string
	Lg_default_database string
	Lg_default_language string
	Lg_disabled_flag    bool
	Lg_create_date      time.Time
	Lg_system_flag      Lg_sysflag_t // LG_SYSFLAG_SA for 'sa', else 0
}

// Login_params is used to pass login information to VM, for CREATE LOGIN and ALTER LOGIN.
//
type Login_params struct {
	Lgp_name             string
	Lgp_password_hash    string
	Lgp_default_database string
	Lgp_default_language string
	Lgp_disable_option   lex.Auxword_t
}

// Must_load_SYSLOGINS loads all logins stored in master.db into dictmgr.
// It is done at init, when the server starts.
//
func (dictmgr *Dictionary_manager) Must_load_SYSLOGINS() {
	var (
		err       error
		rsql_err  *rsql.Error
		master    *sqlite3.Conn
		stmt      *sqlite3.Stmt
		syslogins *Syslogins_collection
		ok        bool

		login_id               int64
		login_name             string
		login_password_hash    string
		login_default_database string
		login_default_language string
		login_disabled_flag    int64
		login_create_date      string
		login_system_flag      int64
	)

	// read all logins from master

	master = dictmgr.dm_master
	syslogins = &dictmgr.dm_SYSLOGINS

	syslogins.login_id_map = make(map[int64]*Syslogin, SPEC_SYSLOGINS_COLLECION_DEFAULT_SIZE)
	syslogins.name_map = make(map[string]*Syslogin, SPEC_SYSLOGINS_COLLECION_DEFAULT_SIZE)

	if stmt, err = master.Prepare("SELECT login_id, login_name, login_password_hash, login_default_database, login_default_language, login_disabled_flag, login_create_date, login_system_flag FROM syslogins"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read login record from master

		if err = stmt.Scan(&login_id, &login_name, &login_password_hash, &login_default_database, &login_default_language, &login_disabled_flag, &login_create_date, &login_system_flag); err != nil {
			break
		}

		// fill in SYSLOGINS

		if login_default_language, rsql_err = lang.Normalize_language(login_default_language); rsql_err != nil { // just to be sure, normalize login default language
			log.Panicf("%s", rsql_err)
		}

		login := &Syslogin{
			Lg_login_id:         login_id,
			Lg_name:             login_name,
			Lg_password_hash:    login_password_hash,
			Lg_default_database: login_default_database,
			Lg_default_language: login_default_language,
			Lg_disabled_flag:    login_disabled_flag != 0,
			Lg_create_date:      must_parse_time(login_create_date),
			Lg_system_flag:      Lg_sysflag_t(login_system_flag),
		}

		if _, ok = syslogins.login_id_map[login.Lg_login_id]; ok == true {
			panic("impossible") // prevented by pk on login_id
		}

		if _, ok = syslogins.name_map[login.Lg_name]; ok == true {
			panic("impossible") // prevented by unique index on login_name
		}

		syslogins.login_id_map[login.Lg_login_id] = login // fill in SYSLOGINS
		syslogins.name_map[login.Lg_name] = login

		if syslogins.login_id_nextval < login.Lg_login_id {
			syslogins.login_id_nextval = login.Lg_login_id
		}
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	switch {
	case len(syslogins.login_id_map) == 0: // if no login exists yet, first login_id must be SA_LOGIN_ID
		rsql.Assert(syslogins.login_id_nextval == 0)
		syslogins.login_id_nextval = SA_LOGIN_ID

	case syslogins.login_id_nextval < SA_LOGIN_ID:
		log.Panicf("MASTER: syslogins.login_id_nextval must be > %d, but max value found is %d.", SA_LOGIN_ID, syslogins.login_id_nextval)

	default:
		syslogins.login_id_nextval++ // CREATE LOGIN will use this value for the next login_id
	}

	stmt.Close() // release resources held by stmt in C heap
}

// Add_syslogin implements CREATE LOGIN statement.
//
func (dictmgr *Dictionary_manager) Add_syslogin(context *rsql.Context, login_name string, login_params Login_params) *rsql.Error {
	var (
		syslogins *Syslogins_collection
		login     *Syslogin
		ok        bool
	)

	rsql.Assert(login_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	syslogins = &dictmgr.dm_SYSLOGINS

	login = &Syslogin{
		Lg_login_id:         syslogins.login_id_nextval,
		Lg_name:             login_params.Lgp_name,
		Lg_password_hash:    login_params.Lgp_password_hash,
		Lg_default_database: login_params.Lgp_default_database,
		Lg_default_language: login_params.Lgp_default_language,
		Lg_disabled_flag:    false,
		Lg_create_date:      now(),
		Lg_system_flag:      0, // LG_SYSFLAG_SA for 'sa', else 0
	}

	if len(syslogins.login_id_map) == 0 { // if no login exists yet, the first must be 'sa' with login_id = SA_LOGIN_ID
		if !(login.Lg_name == "sa" && login.Lg_login_id == SA_LOGIN_ID) {
			log.Panicf("MASTER: the first login must be 'sa' with login_id %d, but is is '%s' with login_id %d.", SA_LOGIN_ID, login.Lg_name, login.Lg_login_id)
		}

		login.Lg_system_flag = LG_SYSFLAG_SA
	}

	if login.Lg_default_database != rsql.TRASHDB { // check that default database exists
		if _, rsql_err := dictmgr.get_database(login.Lg_default_database); rsql_err != nil {
			return rsql_err
		}
	}

	if _, ok = syslogins.login_id_map[login.Lg_login_id]; ok == true { // login_id duplicate is impossible
		log.Panicf("MASTER: syslogin id %d already exists.", login.Lg_login_id)
	}

	if _, ok = syslogins.name_map[login.Lg_name]; ok == true { // check for login name duplicate
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, login.Lg_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	syslogins.login_id_nextval++

	rsql.Assert(syslogins.login_id_map[login.Lg_login_id] == nil)
	rsql.Assert(syslogins.name_map[login.Lg_name] == nil)

	syslogins.login_id_map[login.Lg_login_id] = login
	syslogins.name_map[login.Lg_name] = login

	// write to master.db

	args := sqlite3.NamedArgs{
		"$login_id":               login.Lg_login_id,
		"$login_name":             login.Lg_name,
		"$login_password_hash":    login.Lg_password_hash,
		"$login_default_database": login.Lg_default_database,
		"$login_default_language": login.Lg_default_language,
		"$login_disabled_flag":    login.Lg_disabled_flag,
		"$login_create_date":      format_time(login.Lg_create_date),
		"$login_system_flag":      int64(login.Lg_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSLOGINS_INSERT, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Change_syslogin implements ALTER LOGIN statement.
//
func (dictmgr *Dictionary_manager) Change_syslogin(context *rsql.Context, login_name string, changes Login_params) *rsql.Error {
	var (
		syslogins *Syslogins_collection
		login     *Syslogin
		ok        bool
	)

	rsql.Assert(login_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	syslogins = &dictmgr.dm_SYSLOGINS

	if login, ok = syslogins.name_map[login_name]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, login_name)
	}

	if changes.Lgp_name == login.Lg_name { // if new name is same as old name, discard the change
		changes.Lgp_name = ""
	}

	if changes.Lgp_name != "" { // if name is changed
		if login.Lg_system_flag != 0 { // forbidden to change "sa" name
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, login_name)
		}

		if _, ok = syslogins.name_map[changes.Lgp_name]; ok == true {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, changes.Lgp_name)
		}
	}

	if changes.Lgp_default_database != "" { // if default database is changed
		if _, rsql_err := dictmgr.get_database(changes.Lgp_default_database); rsql_err != nil { // check that default database exists
			return rsql_err
		}
	}

	if changes.Lgp_disable_option != "" {
		if login.Lg_system_flag != 0 { // forbidden to disable "sa"
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, login_name)
		}
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	if changes.Lgp_name != "" { // if name is changed
		login.Lg_name = changes.Lgp_name

		rsql.Assert(syslogins.name_map[login_name] != nil)
		rsql.Assert(syslogins.name_map[login.Lg_name] == nil)

		delete(syslogins.name_map, login_name)
		syslogins.name_map[login.Lg_name] = login
	}

	if changes.Lgp_password_hash != "" { // if password is changed
		login.Lg_password_hash = changes.Lgp_password_hash
	}

	if changes.Lgp_default_database != "" { // if default database is changed
		login.Lg_default_database = changes.Lgp_default_database
	}

	if changes.Lgp_default_language != "" { // if default language is changed
		login.Lg_default_language = changes.Lgp_default_language
	}

	if changes.Lgp_disable_option != "" { // "" or "enable" or "disable"
		login.Lg_disabled_flag = (changes.Lgp_disable_option == lex.AUXWORD_DISABLE)
	}

	// write to master.db

	args := sqlite3.NamedArgs{
		"$login_id":               login.Lg_login_id, // never changes
		"$login_name":             login.Lg_name,
		"$login_password_hash":    login.Lg_password_hash,
		"$login_default_database": login.Lg_default_database,
		"$login_default_language": login.Lg_default_language,
		"$login_disabled_flag":    login.Lg_disabled_flag,
	}
	dictmgr.Execute(DICT_STMT_SYSLOGINS_UPDATE, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Remove_syslogin implements DROP LOGIN statement.
//
func (dictmgr *Dictionary_manager) Remove_syslogin(context *rsql.Context, login_name string) *rsql.Error {
	var (
		syslogins *Syslogins_collection
		login     *Syslogin
		ok        bool
	)

	rsql.Assert(login_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	syslogins = &dictmgr.dm_SYSLOGINS

	if login, ok = syslogins.name_map[login_name]; ok == false {
		//return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, login_name)
		return rsql.New_Error(rsql.ERROR_DICT, rsql.WARNING_DROP_LOGIN_NOT_FOUND, rsql.ERROR_IS_WARNING, login_name)
	}

	if login.Lg_system_flag != 0 {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, login_name)
	}

	if db_name_x, user_name_x, ok := dictmgr.login_id_exists_in_sysprincipals(login.Lg_login_id); ok == true {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_ID_USER_EXISTS, rsql.ERROR_BATCH_ABORT, db_name_x, user_name_x, login_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	rsql.Assert(syslogins.name_map[login.Lg_name] != nil)
	rsql.Assert(syslogins.login_id_map[login.Lg_login_id] != nil)

	delete(syslogins.name_map, login.Lg_name)
	delete(syslogins.login_id_map, login.Lg_login_id)

	// write to master.db

	args := sqlite3.NamedArgs{
		"$login_id": login.Lg_login_id,
	}
	dictmgr.Execute(DICT_STMT_SYSLOGINS_DELETE, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Get_syslogin_name implements SUSER_NAME function.
//
func (dictmgr *Dictionary_manager) Get_syslogin_name(login_id int64) (login_name string, ok bool) {
	var (
		login *Syslogin
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[login_id]; ok == false {
		return "", false
	}

	return login.Lg_name, true
}

// Get_syslogin_id implements SUSER_ID function.
//
func (dictmgr *Dictionary_manager) Get_syslogin_id(login_name []byte) (login_id int64, ok bool) {
	var (
		login_name_lc string
		login         *Syslogin
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	login_name_lc = strings.ToLower(string(login_name))

	if login, ok = dictmgr.dm_SYSLOGINS.name_map[login_name_lc]; ok == false {
		return 0, false
	}

	return login.Lg_login_id, true
}

// Check_SA checks if login is 'sa'.
//
func (dictmgr *Dictionary_manager) Check_SA(context *rsql.Context) *rsql.Error {

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	return nil
}

//================== SYSPRINCIPALS ===================

type Pal_type_t uint8

const (
	PAL_TYPE_USER Pal_type_t = iota
	PAL_TYPE_ROLE
)

type Pal_sysflag_t int64

const (
	PAL_SYSFLAG_USER_DBO    Pal_sysflag_t = 1 << iota
	PAL_SYSFLAG_ROLE_PUBLIC Pal_sysflag_t = 1 << iota
)

type Sysprincipals_collection struct {
	palid_map map[int64]*Sysprincipal
	name_map  map[Sysprincipal_namekey_t]*Sysprincipal

	palid_nextval int64
}

type Sysprincipal_namekey_t struct { // unique key
	Pal_dbid int64
	Pal_name string
}

// Sysprincipal is a role or a user.
//
type Sysprincipal struct {
	Pal_palid               int64         // palid
	Pal_dbid                int64         // principal always belongs to a database
	Pal_name                string        // unique pal_name in a database
	Pal_type                Pal_type_t    // U user, R role
	Pal_create_date         time.Time     // creation time
	Pal_default_schema_name string        // for U, always "dbo". For R, not used.
	Pal_login_id            int64         // must be unique for each database. For U, references a syslogin entry. For R, not used.
	Pal_system_flag         Pal_sysflag_t // PAL_SYSFLAG_USER_DBO for 'dbo', PAL_SYSFLAG_ROLE_PUBLIC for 'public', else 0

	Pal_member_of Role_list // list of roles the principal is a member of. The role 'public' is never put in this list ('public' membership is implicit for all users. It cannot be added, nor removed.).
}

type Role_list []*Sysprincipal

func (principal *Sysprincipal) Is_member_of(role *Sysprincipal) bool {

	rsql.Assert(role.Pal_type == PAL_TYPE_ROLE)

	for _, r := range principal.Pal_member_of {
		if r == role {
			return true
		}
	}

	return false
}

func (principal *Sysprincipal) Membership_add(role *Sysprincipal) {
	rsql.Assert(role.Pal_type == PAL_TYPE_ROLE)

	if principal.Is_member_of(role) { // if already member of role, just return
		return
	}

	principal.Pal_member_of = append(principal.Pal_member_of, role)
}

func (principal *Sysprincipal) Membership_remove(role *Sysprincipal) {
	rsql.Assert(role.Pal_type == PAL_TYPE_ROLE)

	for i, r := range principal.Pal_member_of {
		if r == role {
			length := len(principal.Pal_member_of)
			principal.Pal_member_of[i], principal.Pal_member_of[length-1] = principal.Pal_member_of[length-1], nil
			principal.Pal_member_of = principal.Pal_member_of[:length-1]
			return
		}
	}
}

// Membership_subtree_contains checks if role exists in the membership subtree of principal.
//
// Returns true if principal is role, or is member, submember, subsubmember etc of role.
//
func (principal *Sysprincipal) Membership_subtree_contains(role *Sysprincipal) bool {

	if principal == role {
		return true
	}

	for _, r := range principal.Pal_member_of {
		if r.Membership_subtree_contains(role) {
			return true
		}
	}

	return false
}

// Must_load_SYSPRINCIPALS loads all principals stored in master.db into dictmgr.
// It is done at init, when the server starts.
//
func (dictmgr *Dictionary_manager) Must_load_SYSPRINCIPALS() {
	var (
		err           error
		master        *sqlite3.Conn
		stmt          *sqlite3.Stmt
		sysprincipals *Sysprincipals_collection
		ok            bool

		pal_palid               int64
		pal_dbid                int64
		pal_name                string
		pal_type                int64
		pal_create_date         string
		pal_default_schema_name string
		pal_login_id            int64
		pal_system_flag         int64

		member          *Sysprincipal
		role            *Sysprincipal
		rm_member_palid int64
		rm_role_palid   int64
	)

	// read all principals from master

	master = dictmgr.dm_master
	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	sysprincipals.palid_map = make(map[int64]*Sysprincipal, SPEC_SYSPRINCIPALS_COLLECION_DEFAULT_SIZE)
	sysprincipals.name_map = make(map[Sysprincipal_namekey_t]*Sysprincipal, SPEC_SYSPRINCIPALS_COLLECION_DEFAULT_SIZE)

	dictmgr.dm_map_login_dbid_user = make(Map_login_dbid_user, SPEC_SYSPRINCIPALS_COLLECION_DEFAULT_SIZE)

	if stmt, err = master.Prepare("SELECT pal_palid, pal_dbid, pal_name, pal_type, pal_create_date, pal_default_schema_name, pal_login_id, pal_system_flag FROM sysprincipals"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read principal record from master

		if err = stmt.Scan(&pal_palid, &pal_dbid, &pal_name, &pal_type, &pal_create_date, &pal_default_schema_name, &pal_login_id, &pal_system_flag); err != nil {
			break
		}

		// fill in SYSPRINCIPALS

		principal := &Sysprincipal{
			Pal_palid:               pal_palid,
			Pal_dbid:                pal_dbid,
			Pal_name:                pal_name,
			Pal_type:                Pal_type_t(pal_type),
			Pal_create_date:         must_parse_time(pal_create_date),
			Pal_default_schema_name: pal_default_schema_name,
			Pal_login_id:            pal_login_id,
			Pal_system_flag:         Pal_sysflag_t(pal_system_flag),
		}

		if _, ok = sysprincipals.palid_map[principal.Pal_palid]; ok == true {
			panic("impossible") // prevented by pk on pal_palid
		}

		if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}]; ok == true {
			panic("impossible") // prevented by unique index on pal_dbid, pal_name
		}

		if principal.Pal_type == PAL_TYPE_USER {
			if _, ok = dictmgr.dm_SYSLOGINS.login_id_map[principal.Pal_login_id]; ok == false {
				log.Panicf("principal %d has unknown Pal_login_id %d", principal.Pal_palid, principal.Pal_login_id)
			}

			rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: principal.Pal_login_id, dbid: principal.Pal_dbid}] == 0)
			dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: principal.Pal_login_id, dbid: principal.Pal_dbid}] = principal.Pal_palid
		}

		sysprincipals.palid_map[principal.Pal_palid] = principal // fill in SYSPRINCIPALS
		sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}] = principal

		if sysprincipals.palid_nextval < principal.Pal_palid {
			sysprincipals.palid_nextval = principal.Pal_palid
		}
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	switch {
	case sysprincipals.palid_nextval < PALID_START:
		sysprincipals.palid_nextval = PALID_START // CREATE USER or CREATE ROLE will use this value for the next palid

	default:
		sysprincipals.palid_nextval++ // CREATE USER or CREATE ROLE will use this value for the next palid
	}

	stmt.Close() // release resources held by stmt in C heap

	//============= load sysrolemembers =============

	if stmt, err = master.Prepare("SELECT rm_member_palid, rm_role_palid FROM sysrolemembers"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read membership record from master

		if err = stmt.Scan(&rm_member_palid, &rm_role_palid); err != nil {
			break
		}

		if member, ok = sysprincipals.palid_map[rm_member_palid]; ok == false {
			log.Panicf("MASTER: member palid %d not found for membership.", rm_member_palid)
		}

		if role, ok = sysprincipals.palid_map[rm_role_palid]; ok == false {
			log.Panicf("MASTER: role palid %d not found for membership.", rm_role_palid)
		}

		member.Membership_add(role)
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	stmt.Close() // release resources held by stmt in C heap
}

// Add_sysprincipal_user implements CREATE USER statement.
//
func (dictmgr *Dictionary_manager) Add_sysprincipal_user(context *rsql.Context, database_name string, user_name string, login_name string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		sysprincipals *Sysprincipals_collection
		principal     *Sysprincipal
		database      *Sysdatabase
		login         *Syslogin
		ok            bool
	)

	rsql.Assert(database_name != "" && user_name != "" && login_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if login, rsql_err = dictmgr.get_login(login_name); rsql_err != nil { // retrieve login
		return rsql_err
	}

	if login.Lg_system_flag == LG_SYSFLAG_SA { // login cannot be sa (only user dbo can have login sa)
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_USER_LOGIN_CANNOT_BE_SET_TO_SA, rsql.ERROR_BATCH_ABORT)
	}

	principal = &Sysprincipal{
		Pal_palid:               sysprincipals.palid_nextval,
		Pal_dbid:                database.Db_dbid,
		Pal_name:                user_name,
		Pal_type:                PAL_TYPE_USER,
		Pal_create_date:         now(),
		Pal_default_schema_name: "dbo", // for U, always "dbo". For R, "".
		Pal_login_id:            login.Lg_login_id,
		Pal_system_flag:         0, // PAL_SYSFLAG_USER_DBO for 'dbo', else 0
	}

	if _, ok = sysprincipals.palid_map[principal.Pal_palid]; ok == true {
		log.Panicf("MASTER: sysprincipal id %d already exists.", principal.Pal_palid)
	}

	if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}]; ok == true { // user name must be unique in database
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, database_name, principal.Pal_name)
	}

	if db_name_x, user_name_x, ok := dictmgr.login_id_exists_in_database(principal.Pal_dbid, principal.Pal_login_id); ok == true { // user's login must be unique in database
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_CREATE_USER_LOGIN_ID_DUPLICATE, rsql.ERROR_BATCH_ABORT, db_name_x, principal.Pal_name, login_name, user_name_x)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	sysprincipals.palid_nextval++

	rsql.Assert(sysprincipals.palid_map[principal.Pal_palid] == nil)
	rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}] == nil)

	sysprincipals.palid_map[principal.Pal_palid] = principal
	sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}] = principal

	rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: principal.Pal_login_id, dbid: principal.Pal_dbid}] == 0)
	dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: principal.Pal_login_id, dbid: principal.Pal_dbid}] = principal.Pal_palid

	// write to master.db

	args := sqlite3.NamedArgs{
		"$pal_palid":               principal.Pal_palid,
		"$pal_dbid":                principal.Pal_dbid,
		"$pal_name":                principal.Pal_name,
		"$pal_type":                int64(principal.Pal_type),
		"$pal_create_date":         format_time(principal.Pal_create_date),
		"$pal_default_schema_name": principal.Pal_default_schema_name,
		"$pal_login_id":            principal.Pal_login_id,
		"$pal_system_flag":         int64(principal.Pal_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_INSERT, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Change_sysprincipal_user implements ALTER USER statement.
//
func (dictmgr *Dictionary_manager) Change_sysprincipal_user(context *rsql.Context, database_name string, user_name string, change_name string, change_login string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		sysprincipals *Sysprincipals_collection
		user          *Sysprincipal
		database      *Sysdatabase
		login         *Syslogin
		ok            bool
	)

	rsql.Assert(database_name != "" && user_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if user, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: user_name}]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, user_name)
	}

	if user.Pal_type != PAL_TYPE_USER {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_IS_ROLE, rsql.ERROR_BATCH_ABORT, database_name, user_name)
	}

	if user.Pal_system_flag != 0 { // forbidden to change 'dbo'
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_USER_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, database_name, user_name)
	}

	if change_name == user.Pal_name { // if new name is same as old name, discard the change
		change_name = ""
	}

	if change_name != "" {
		if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: change_name}]; ok == true {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, database_name, change_name)
		}
	}

	if change_login != "" {
		if login, rsql_err = dictmgr.get_login(change_login); rsql_err != nil { // retrieve login
			return rsql_err
		}

		if login.Lg_system_flag == LG_SYSFLAG_SA { // login cannot be sa (only user dbo can have login sa)
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_USER_LOGIN_CANNOT_BE_SET_TO_SA, rsql.ERROR_BATCH_ABORT)
		}

		if user.Pal_login_id == login.Lg_login_id {
			change_login = ""
		} else {
			if db_name_x, user_name_x, ok := dictmgr.login_id_exists_in_database(database.Db_dbid, login.Lg_login_id); ok == true {
				return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_ALTER_USER_LOGIN_ID_DUPLICATE, rsql.ERROR_BATCH_ABORT, db_name_x, user_name, login.Lg_name, user_name_x)
			}
		}
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	if change_name != "" {
		user.Pal_name = change_name

		rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: user.Pal_dbid, Pal_name: user_name}] != nil)
		rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: user.Pal_dbid, Pal_name: user.Pal_name}] == nil)

		delete(sysprincipals.name_map, Sysprincipal_namekey_t{Pal_dbid: user.Pal_dbid, Pal_name: user_name})
		sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: user.Pal_dbid, Pal_name: user.Pal_name}] = user
	}

	if change_login != "" {
		rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid}] == user.Pal_palid)
		delete(dictmgr.dm_map_login_dbid_user, Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid})

		user.Pal_login_id = login.Lg_login_id

		rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid}] == 0)
		dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid}] = user.Pal_palid
	}

	// write to master.db

	args := sqlite3.NamedArgs{
		"$pal_palid":               user.Pal_palid, // never changes
		"$pal_dbid":                user.Pal_dbid,
		"$pal_name":                user.Pal_name,
		"$pal_type":                int64(user.Pal_type),
		"$pal_create_date":         format_time(user.Pal_create_date),
		"$pal_default_schema_name": user.Pal_default_schema_name,
		"$pal_login_id":            user.Pal_login_id,
		"$pal_system_flag":         int64(user.Pal_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_UPDATE, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Remove_sysprincipal_user implements DROP USER statement.
//
// Cannot remove 'dbo' user.
//
func (dictmgr *Dictionary_manager) Remove_sysprincipal_user(context *rsql.Context, database_name string, user_name string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		sysprincipals *Sysprincipals_collection
		user          *Sysprincipal
		database      *Sysdatabase
		ok            bool
	)

	rsql.Assert(database_name != "" && user_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if user, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: user_name}]; ok == false {
		//return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, user_name)
		return rsql.New_Error(rsql.ERROR_DICT, rsql.WARNING_DROP_USER_NOT_FOUND, rsql.ERROR_IS_WARNING, database_name, user_name)
	}

	if user.Pal_type != PAL_TYPE_USER {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_IS_ROLE, rsql.ERROR_BATCH_ABORT, database_name, user_name)
	}

	if user.Pal_system_flag != 0 {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_USER_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, database_name, user_name)
	}

	if _, ok = sysprincipals.palid_map[user.Pal_palid]; ok == false {
		log.Panicf("MASTER: sysprincipal id %d doesn't exist.", user.Pal_palid)
	}

	if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: user.Pal_dbid, Pal_name: user.Pal_name}]; ok == false {
		log.Panicf("MASTER: sysprincipal %s.%s doesn't exist.", database_name, user.Pal_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	for key, _ := range dictmgr.dm_SYSPERMISSIONS.key_map { // delete user's permissions
		if key.Palid == user.Pal_palid {
			delete(dictmgr.dm_SYSPERMISSIONS.key_map, key)
		}
	}

	rsql.Assert(sysprincipals.palid_map[user.Pal_palid] != nil)
	rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: user.Pal_dbid, Pal_name: user.Pal_name}] != nil)

	delete(sysprincipals.palid_map, user.Pal_palid) // delete user
	delete(sysprincipals.name_map, Sysprincipal_namekey_t{Pal_dbid: user.Pal_dbid, Pal_name: user.Pal_name})

	rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid}] == user.Pal_palid)
	delete(dictmgr.dm_map_login_dbid_user, Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid})

	// write to master.db

	args := sqlite3.NamedArgs{
		"$pal_palid": user.Pal_palid,
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_DELETE, args)

	args = sqlite3.NamedArgs{
		"$palid": user.Pal_palid,
	}
	dictmgr.Execute(DICT_STMT_SYSROLEMEMBERS_DELETE_PALID, args)

	args = sqlite3.NamedArgs{
		"$p_palid": user.Pal_palid,
	}
	dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_DELETE_PRINCIPAL, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Add_sysprincipal_role implements CREATE ROLE statement.
//
func (dictmgr *Dictionary_manager) Add_sysprincipal_role(context *rsql.Context, database_name string, role_name string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		sysprincipals *Sysprincipals_collection
		principal     *Sysprincipal
		database      *Sysdatabase
		ok            bool
	)

	rsql.Assert(database_name != "" && role_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	principal = &Sysprincipal{
		Pal_palid:               sysprincipals.palid_nextval,
		Pal_dbid:                database.Db_dbid,
		Pal_name:                role_name,
		Pal_type:                PAL_TYPE_ROLE,
		Pal_create_date:         now(),
		Pal_default_schema_name: "", // for R, always ""
		Pal_login_id:            0,  // not used
		Pal_system_flag:         0,  // PAL_SYSFLAG_ROLE_PUBLIC for 'public', else 0
	}

	if _, ok = sysprincipals.palid_map[principal.Pal_palid]; ok == true {
		log.Panicf("MASTER: sysprincipal id %d already exists.", principal.Pal_palid)
	}

	if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}]; ok == true {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, database_name, principal.Pal_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	sysprincipals.palid_nextval++

	rsql.Assert(sysprincipals.palid_map[principal.Pal_palid] == nil)
	rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}] == nil)

	sysprincipals.palid_map[principal.Pal_palid] = principal
	sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}] = principal

	// write to master.db

	args := sqlite3.NamedArgs{
		"$pal_palid":               principal.Pal_palid,
		"$pal_dbid":                principal.Pal_dbid,
		"$pal_name":                principal.Pal_name,
		"$pal_type":                int64(principal.Pal_type),
		"$pal_create_date":         format_time(principal.Pal_create_date),
		"$pal_default_schema_name": principal.Pal_default_schema_name,
		"$pal_login_id":            nil, // not used. MUST BE nil, so that UNIQUE constraint on sysprincipals(pal_dbid, pal_login_id) in master.db allows many roles in same database (for sqlite3, nil values are different values)
		"$pal_system_flag":         int64(principal.Pal_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_INSERT, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Change_sysprincipal_role implements ALTER ROLE statement.
//
func (dictmgr *Dictionary_manager) Change_sysprincipal_role(context *rsql.Context, database_name string, role_name string, change_name string, add_member_name string, drop_member_name string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		sysprincipals *Sysprincipals_collection
		role          *Sysprincipal
		database      *Sysdatabase
		add_member    *Sysprincipal
		drop_member   *Sysprincipal
		ok            bool
	)

	rsql.Assert(database_name != "" && role_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if role_name == "public" && add_member_name != "" { // all users are already implicitly members of 'public' role
		return nil
	}

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if role, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: role_name}]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, role_name)
	}

	if role.Pal_type != PAL_TYPE_ROLE {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_IS_USER, rsql.ERROR_BATCH_ABORT, database_name, role_name)
	}

	if change_name == role.Pal_name { // if new name is same as old name, discard the change
		rsql.Assert(add_member_name == "" && drop_member_name == "")
		return nil //----> exit
	}

	if change_name != "" {
		if role.Pal_system_flag != 0 { // forbidden to change 'public' name
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_ROLE_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, database_name, role_name)
		}

		if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: change_name}]; ok == true {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, database_name, change_name)
		}
	}

	if add_member_name != "" {
		if add_member, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: add_member_name}]; ok == false {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, add_member_name)
		}

		if add_member.Pal_type == PAL_TYPE_ROLE {
			if role.Membership_subtree_contains(add_member) { // role is add_member, or is member, submember, subsubmember etc of add_member. It is forbidden to make add_member member of role.
				return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_ROLE_EXISTS_IN_ROLE_SUBTREE, rsql.ERROR_BATCH_ABORT, database_name, add_member_name)
			}
		}

		if add_member.Is_member_of(role) == true {
			// rsql.Println("*************    ALREADY MEMBER *****")
			rsql.Assert(change_name == "" && drop_member_name == "")
			return nil //----> exit
		}
	}

	if drop_member_name != "" {
		if drop_member, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: drop_member_name}]; ok == false {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, drop_member_name)
		}

		if drop_member.Is_member_of(role) == false { // 'public' role has no member (all users are implicitly members of 'public', but it is not listed anywhere)
			// rsql.Println("*************    ALREADY NOT MEMBER *****")
			rsql.Assert(change_name == "" && add_member_name == "")
			return nil //----> exit
		}
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	switch {
	case change_name != "":
		role.Pal_name = change_name

		rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: role.Pal_dbid, Pal_name: role_name}] != nil)
		rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: role.Pal_dbid, Pal_name: role.Pal_name}] == nil)

		delete(sysprincipals.name_map, Sysprincipal_namekey_t{Pal_dbid: role.Pal_dbid, Pal_name: role_name})
		sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: role.Pal_dbid, Pal_name: role.Pal_name}] = role

		// write to master.db

		args := sqlite3.NamedArgs{
			"$pal_palid":               role.Pal_palid, // never changes
			"$pal_dbid":                role.Pal_dbid,
			"$pal_name":                role.Pal_name,
			"$pal_type":                int64(role.Pal_type),
			"$pal_create_date":         format_time(role.Pal_create_date),
			"$pal_default_schema_name": role.Pal_default_schema_name,
			"$pal_login_id":            nil, // not used. MUST BE nil, so that UNIQUE constraint on sysprincipals(pal_dbid, pal_login_id) in master.db allows many roles in same database (for sqlite3, nil values are different values)
			"$pal_system_flag":         int64(role.Pal_system_flag),
		}
		dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_UPDATE, args)

	case add_member_name != "":
		add_member.Membership_add(role)

		// write to master.db

		args := sqlite3.NamedArgs{
			"$rm_member_palid": add_member.Pal_palid,
			"$rm_role_palid":   role.Pal_palid,
		}
		dictmgr.Execute(DICT_STMT_SYSROLEMEMBERS_INSERT, args)

	case drop_member_name != "":
		drop_member.Membership_remove(role)

		// write to master.db

		args := sqlite3.NamedArgs{
			"$rm_member_palid": drop_member.Pal_palid,
			"$rm_role_palid":   role.Pal_palid,
		}
		dictmgr.Execute(DICT_STMT_SYSROLEMEMBERS_DELETE, args)

	default:
		panic("impossible")
	}

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Remove_sysprincipal_role implements DROP ROLE statement.
//
func (dictmgr *Dictionary_manager) Remove_sysprincipal_role(context *rsql.Context, database_name string, role_name string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		sysprincipals *Sysprincipals_collection
		role          *Sysprincipal
		database      *Sysdatabase
		principal     *Sysprincipal
		ok            bool
	)

	rsql.Assert(database_name != "" && role_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if role, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: role_name}]; ok == false {
		//return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, role_name)
		return rsql.New_Error(rsql.ERROR_DICT, rsql.WARNING_DROP_ROLE_NOT_FOUND, rsql.ERROR_IS_WARNING, database_name, role_name)
	}

	if role.Pal_type != PAL_TYPE_ROLE {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_IS_USER, rsql.ERROR_BATCH_ABORT, database_name, role_name)
	}

	if role.Pal_system_flag != 0 {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_ROLE_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, database_name, role_name)
	}

	if _, ok = sysprincipals.palid_map[role.Pal_palid]; ok == false {
		log.Panicf("MASTER: sysprincipal id %d doesn't exist.", role.Pal_palid)
	}

	if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: role.Pal_dbid, Pal_name: role.Pal_name}]; ok == false {
		log.Panicf("MASTER: sysprincipal %s.%s doesn't exist.", database_name, role.Pal_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	for key, _ := range dictmgr.dm_SYSPERMISSIONS.key_map { // delete role's permissions
		if key.Palid == role.Pal_palid {
			delete(dictmgr.dm_SYSPERMISSIONS.key_map, key)
		}
	}

	for _, principal = range sysprincipals.palid_map { // delete role membership for all principals
		principal.Membership_remove(role)
	}

	rsql.Assert(sysprincipals.palid_map[role.Pal_palid] != nil)
	rsql.Assert(sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: role.Pal_dbid, Pal_name: role.Pal_name}] != nil)

	delete(sysprincipals.palid_map, role.Pal_palid) // delete role
	delete(sysprincipals.name_map, Sysprincipal_namekey_t{Pal_dbid: role.Pal_dbid, Pal_name: role.Pal_name})

	// write to master.db

	args := sqlite3.NamedArgs{
		"$pal_palid": role.Pal_palid,
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_DELETE, args)

	args = sqlite3.NamedArgs{
		"$palid": role.Pal_palid,
	}
	dictmgr.Execute(DICT_STMT_SYSROLEMEMBERS_DELETE_PALID, args)

	args = sqlite3.NamedArgs{
		"$p_palid": role.Pal_palid,
	}
	dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_DELETE_PRINCIPAL, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Get_sysprincipal_name implements USER_NAME function.
//
func (dictmgr *Dictionary_manager) Get_sysprincipal_name(dbid int64, palid int64) (principal_name string, ok bool) {
	var (
		principal *Sysprincipal
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if principal, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[palid]; ok == false {
		return "", false
	}

	if principal.Pal_dbid != dbid { // principal not in current db
		return "", false
	}

	return principal.Pal_name, true
}

// Get_sysprincipal_id implements USER_ID function.
//
func (dictmgr *Dictionary_manager) Get_sysprincipal_id(dbid int64, principal_name []byte) (palid int64, ok bool) {
	var (
		principal_name_lc string
		principal         *Sysprincipal
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	principal_name_lc = strings.ToLower(string(principal_name))

	if principal, ok = dictmgr.dm_SYSPRINCIPALS.name_map[Sysprincipal_namekey_t{Pal_dbid: dbid, Pal_name: principal_name_lc}]; ok == false {
		return 0, false
	}

	return principal.Pal_palid, true
}

//================== SYSPERMISSIONS ===================

type Perm_key_t struct {
	Objid int64
	Palid int64
}

type Syspermission_t struct {
	Pm_grant rsql.Permission_t
	Pm_deny  rsql.Permission_t
}

type Syspermissions_collection struct {
	key_map map[Perm_key_t]Syspermission_t
}

// Must_load_SYSPRINCIPALS loads all principals stored in master.db into dictmgr.
// It is done at init, when the server starts.
//
func (dictmgr *Dictionary_manager) Must_load_SYSPERMISSIONS() {
	var (
		err            error
		master         *sqlite3.Conn
		stmt           *sqlite3.Stmt
		syspermissions *Syspermissions_collection
		ok             bool

		objid      int64
		palid      int64
		perm_grant int64
		perm_deny  int64
	)

	// read all permissions from master

	master = dictmgr.dm_master
	syspermissions = &dictmgr.dm_SYSPERMISSIONS

	syspermissions.key_map = make(map[Perm_key_t]Syspermission_t, SPEC_SYSPERMISSIONS_COLLECION_DEFAULT_SIZE)

	if stmt, err = master.Prepare("SELECT p_objid, p_palid, p_grant, p_deny FROM syspermissions"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read permission record from master

		if err = stmt.Scan(&objid, &palid, &perm_grant, &perm_deny); err != nil {
			break
		}

		// fill in SYSPERMISSIONS

		if _, ok = syspermissions.key_map[Perm_key_t{Objid: objid, Palid: palid}]; ok == true {
			panic("impossible") // prevented by pk on <Objid, Palid>
		}

		syspermissions.key_map[Perm_key_t{Objid: objid, Palid: palid}] = Syspermission_t{Pm_grant: rsql.Permission_t(perm_grant), Pm_deny: rsql.Permission_t(perm_deny)} // fill in SYSPERMISSIONS
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	stmt.Close() // release resources held by stmt in C heap
}

// Grant_or_deny_syspermission_for_principals implements GRANT or DENY statement.
//
func (dictmgr *Dictionary_manager) Grant_or_deny_syspermission_for_principals(context *rsql.Context, object_qname rsql.Object_qname_t, perm_grant rsql.Permission_t, perm_deny rsql.Permission_t, list_of_principal_names []string) *rsql.Error {
	var (
		rsql_err            *rsql.Error
		dbid                int64
		gtblid              int64
		principal_palid_set map[string]int64
		principal           *Sysprincipal
		ok                  bool
	)

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if dbid, _, gtblid, rsql_err = dictmgr.dbid_schid_gtblid_from_gtabledef_qname(object_qname); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	sysprincipals := &dictmgr.dm_SYSPRINCIPALS

	principal_palid_set = make(map[string]int64, len(list_of_principal_names)) // it is a set to eliminate duplicates of principal names

	for _, principal_name := range list_of_principal_names {
		if principal_name == "dbo" { // "dbo" bypasses all permissions, so you should not grant or deny permission to him
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_GRANT_DENY_TO_DBO_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		if principal, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: dbid, Pal_name: principal_name}]; ok == false {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, object_qname.Database_name, principal_name)
		}

		principal_palid_set[principal_name] = principal.Pal_palid
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	for _, palid := range principal_palid_set { // change permissions for all principals in list
		dictmgr.grant_or_deny_syspermission(gtblid, palid, perm_grant, perm_deny) // no error can occur here. Dict info in memory and master.db are updated.
	}

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// grant_or_deny_syspermission is a helper function that inserts or updates permission record.
//
// This function cannot fail, as modifications must be applied in memory and on disk in full, and not half-cooked.
//
func (dictmgr *Dictionary_manager) grant_or_deny_syspermission(objid int64, palid int64, perm_grant rsql.Permission_t, perm_deny rsql.Permission_t) {
	var (
		syspermissions         *Syspermissions_collection
		permission             Syspermission_t
		permission_exists_flag bool
		ok                     bool
	)

	rsql.Assert(dictmgr.dm_locked == true)
	rsql.Assert(objid != 0 && palid != 0)

	// fill in permission fields

	syspermissions = &dictmgr.dm_SYSPERMISSIONS

	if permission, ok = syspermissions.key_map[Perm_key_t{Objid: objid, Palid: palid}]; ok == true { // check if permission entry already exists
		permission_exists_flag = true
	}

	if perm_grant != 0 {
		permission.Pm_grant |= perm_grant
		permission.Pm_deny &^= perm_grant
	}

	if perm_deny != 0 {
		permission.Pm_deny |= perm_deny
		permission.Pm_grant &^= perm_deny
	}

	// add or update permission in SYSPERMISSIONS

	syspermissions.key_map[Perm_key_t{Objid: objid, Palid: palid}] = permission

	// write to master.db

	switch {
	case permission_exists_flag == true:
		args := sqlite3.NamedArgs{
			"$p_objid": objid,
			"$p_palid": palid,
			"$p_grant": int64(permission.Pm_grant),
			"$p_deny":  int64(permission.Pm_deny),
		}
		dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_UPDATE, args)

	default:
		args := sqlite3.NamedArgs{
			"$p_objid": objid,
			"$p_palid": palid,
			"$p_grant": int64(permission.Pm_grant),
			"$p_deny":  int64(permission.Pm_deny),
		}
		dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_INSERT, args)
	}
}

// Revoke_syspermission_for_principals implements REVOKE statement.
//
func (dictmgr *Dictionary_manager) Revoke_syspermission_for_principals(context *rsql.Context, object_qname rsql.Object_qname_t, perm rsql.Permission_t, list_of_principal_names []string, from_all bool) *rsql.Error {
	var (
		rsql_err           *rsql.Error
		dbid               int64
		gtblid             int64
		principal_name_set map[string]int64
		principal          *Sysprincipal
		ok                 bool
	)

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if dbid, _, gtblid, rsql_err = dictmgr.dbid_schid_gtblid_from_gtabledef_qname(object_qname); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	sysprincipals := &dictmgr.dm_SYSPRINCIPALS

	switch {
	case from_all == false:
		principal_name_set = make(map[string]int64, len(list_of_principal_names)) // it is a set to eliminate duplicates of principal names

		for _, principal_name := range list_of_principal_names {
			if principal, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: dbid, Pal_name: principal_name}]; ok == false {
				return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, object_qname.Database_name, principal_name)
			}

			principal_name_set[principal.Pal_name] = principal.Pal_palid
		}

	default:
		principal_name_set = make(map[string]int64)

		for _, principal := range sysprincipals.palid_map { // for all principals
			if principal.Pal_dbid == dbid { // which are in dbid
				principal_name_set[principal.Pal_name] = principal.Pal_palid
			}
		}
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	for _, palid := range principal_name_set { // change permissions for all principals in list
		dictmgr.revoke_syspermission(gtblid, palid, perm) // no error can occur here. Dict info in memory and master.db are updated.
	}

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// revoke_syspermission is a helper function that deletes or updates permission record.
//
// This function cannot fail, as modifications must be applied in memory and on disk in full, and not half-cooked.
//
func (dictmgr *Dictionary_manager) revoke_syspermission(objid int64, palid int64, perm rsql.Permission_t) {
	var (
		syspermissions           *Syspermissions_collection
		permission               Syspermission_t
		permission_is_empty_flag bool
		ok                       bool
	)

	rsql.Assert(dictmgr.dm_locked == true)
	rsql.Assert(objid != 0 && palid != 0)

	// fill in permission fields

	syspermissions = &dictmgr.dm_SYSPERMISSIONS

	if permission, ok = syspermissions.key_map[Perm_key_t{Objid: objid, Palid: palid}]; ok == false { // if no permission entry, just return
		return // ----> return
	}

	permission.Pm_grant &^= perm
	permission.Pm_deny &^= perm

	if permission.Pm_grant == 0 && permission.Pm_deny == 0 {
		permission_is_empty_flag = true
	}

	// update or delete permission in SYSPERMISSIONS

	switch {
	case permission_is_empty_flag == true:
		delete(syspermissions.key_map, Perm_key_t{Objid: objid, Palid: palid})

		// write to master.db

		args := sqlite3.NamedArgs{
			"$p_objid": objid,
			"$p_palid": palid,
		}
		dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_DELETE, args)

	default:
		syspermissions.key_map[Perm_key_t{Objid: objid, Palid: palid}] = permission

		// write to master.db

		args := sqlite3.NamedArgs{
			"$p_objid": objid,
			"$p_palid": palid,
			"$p_grant": int64(permission.Pm_grant),
			"$p_deny":  int64(permission.Pm_deny),
		}
		dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_UPDATE, args)
	}
}

//================== SYSDATABASES and SYSCHEMAS ===================

type Db_status_t uint8

const (
	DB_STATUS_BEING_CREATED Db_status_t = 1 + iota
	DB_STATUS_ONLINE
	DB_STATUS_OFFLINE
	DB_STATUS_CORRUPTED
	DB_STATUS_BEING_DROPPED
)

func (status Db_status_t) String() string {

	switch status {
	case DB_STATUS_BEING_CREATED:
		return "BEING_CREATED"
	case DB_STATUS_ONLINE:
		return "ONLINE"
	case DB_STATUS_OFFLINE:
		return "OFFLINE"
	case DB_STATUS_CORRUPTED:
		return "CORRUPTED"
	case DB_STATUS_BEING_DROPPED:
		return "BEING_DROPPED"
	default:
		panic("impossible")
	}
}

type Db_mode_t uint8

const (
	DB_MODE_READ_WRITE Db_mode_t = 1 + iota
	DB_MODE_READ_ONLY
)

func (mode Db_mode_t) String() string {

	switch mode {
	case DB_MODE_READ_WRITE:
		return "READ_WRITE"
	case DB_MODE_READ_ONLY:
		return "READ_ONLY"
	default:
		panic("impossible")
	}
}

type Db_access_t uint8

const (
	DB_ACCESS_RESTRICTED_USER Db_access_t = 1 + iota
	DB_ACCESS_MULTI_USER
)

func (access Db_access_t) String() string {

	switch access {
	case DB_ACCESS_RESTRICTED_USER:
		return "RESTRICTED_USER"
	case DB_ACCESS_MULTI_USER:
		return "MULTI_USER"
	default:
		panic("impossible")
	}
}

type Db_sysflag_t int64

const (
	DB_SYSFLAG_TRASHDB Db_sysflag_t = 1 << iota
)

// Database_info returns information about the status, mode and access of the database, for display.
//
func (database *Sysdatabase) Database_info() string {
	var s string

	// status

	switch database.Db_status {
	case DB_STATUS_BEING_CREATED:
		s = "Being created"
	case DB_STATUS_ONLINE:
		s = ""
	case DB_STATUS_OFFLINE:
		s = "Offline"
	case DB_STATUS_CORRUPTED:
		s = "Corrupted"
	case DB_STATUS_BEING_DROPPED:
		s = "Being dropped"
	default:
		s = fmt.Sprintf("status unknown (%d)", database.Db_status)
	}

	// mode

	if database.Db_mode == DB_MODE_READ_ONLY {
		if s != "" {
			s += ", "
		}

		s += "Read only"
	}

	// access

	if database.Db_access == DB_ACCESS_RESTRICTED_USER {
		if s != "" {
			s += ", "
		}

		s += "Restricted user"
	}

	return s
}

// collection of databases
//
type Sysdatabases_collection struct {
	dbid_map map[int64]*Sysdatabase
	name_map map[string]*Sysdatabase

	dbid_nextval int64 // next dbid value for CREATE DATABASE
}

type Sysdatabase struct {
	Db_dbid        int64
	Db_name        string
	Db_create_date time.Time
	Db_status      Db_status_t  // being_created, recovering, open, closed, corrupted, restoring
	Db_mode        Db_mode_t    // read_write, read_only
	Db_access      Db_access_t  // restricted_user, multi_user
	Db_system_flag Db_sysflag_t // DB_SYSFLAG_TRASHDB for 'trashdb', else 0
}

// Database_params is used by ALTER DATABASE to pass argument to VM.
//
type Database_params struct {
	Dbp_name   string
	Dbp_status lex.Auxword_t
	Dbp_mode   lex.Auxword_t
	Dbp_access lex.Auxword_t
}

// collection of schemas

type Sysschemas_collection struct {
	schid_map map[Sysschema_schidkey_t]*Sysschema
	name_map  map[Sysschema_namekey_t]*Sysschema

	schid_nextval int64 // next schid value for CREATE SCHEMA (not implemented yet)
}

type Sysschema struct { // unique keys are <Sch_dbid, Sch_schid> and <Sch_dbid, Sch_name>
	Sch_dbid  int64
	Sch_schid int64
	Sch_name  string
}

type Sysschema_schidkey_t struct { // unique key
	Sch_dbid  int64
	Sch_schid int64
}

type Sysschema_namekey_t struct { // unique key
	Sch_dbid int64
	Sch_name string
}

// Must_load_SYSDATABASES loads all databases stored in master.db into dictmgr.
// It is done at init, when the server starts.
//
func (dictmgr *Dictionary_manager) Must_load_SYSDATABASES() {
	var (
		err          error
		master       *sqlite3.Conn
		stmt         *sqlite3.Stmt
		sysdatabases *Sysdatabases_collection
		ok           bool

		db_dbid        int64
		db_name        string
		db_create_date string
		db_status      int64
		db_mode        int64
		db_access      int64
		db_system_flag int64
	)

	// read all databases from master

	master = dictmgr.dm_master
	sysdatabases = &dictmgr.dm_SYSDATABASES

	sysdatabases.dbid_map = make(map[int64]*Sysdatabase, SPEC_SYSDATABASES_COLLECION_DEFAULT_SIZE)
	sysdatabases.name_map = make(map[string]*Sysdatabase, SPEC_SYSDATABASES_COLLECION_DEFAULT_SIZE)

	if stmt, err = master.Prepare("SELECT db_dbid, db_name, db_create_date, db_status, db_mode, db_access, db_system_flag FROM sysdatabases"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read database record from master

		if err = stmt.Scan(&db_dbid, &db_name, &db_create_date, &db_status, &db_mode, &db_access, &db_system_flag); err != nil {
			break
		}

		// fill in SYSDATABASES

		database := &Sysdatabase{
			Db_dbid:        db_dbid,
			Db_name:        db_name,
			Db_create_date: must_parse_time(db_create_date),
			Db_status:      Db_status_t(db_status),
			Db_mode:        Db_mode_t(db_mode),
			Db_access:      Db_access_t(db_access),
			Db_system_flag: Db_sysflag_t(db_system_flag),
		}

		if _, ok = sysdatabases.dbid_map[database.Db_dbid]; ok == true {
			panic("impossible") // prevented by pk on dbid
		}

		if _, ok = sysdatabases.name_map[database.Db_name]; ok == true {
			panic("impossible") // prevented by unique index on name
		}

		sysdatabases.dbid_map[database.Db_dbid] = database // fill in SYSDATABASES
		sysdatabases.name_map[database.Db_name] = database

		if sysdatabases.dbid_nextval < database.Db_dbid {
			sysdatabases.dbid_nextval = database.Db_dbid
		}
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	switch {
	case sysdatabases.dbid_nextval == 0: // no database exists yet. Trashdb must be created.
		sysdatabases.dbid_nextval = TRASHDB_DBID

	case sysdatabases.dbid_nextval < TRASHDB_DBID:
		log.Panicf("MASTER: trashdb is not registered with dbid=%d. Highest dbid found is %d.", TRASHDB_DBID, sysdatabases.dbid_nextval)

	default:
		sysdatabases.dbid_nextval++ // CREATE DATABASE will use this value for the next dbid
	}

	stmt.Close() // release resources held by stmt in C heap
}

// Must_load_SYSSCHEMAS loads all schemas stored in master.db into dictmgr.
// It is done at init, when the server starts.
//
func (dictmgr *Dictionary_manager) Must_load_SYSSCHEMAS() {
	var (
		err        error
		master     *sqlite3.Conn
		stmt       *sqlite3.Stmt
		sysschemas *Sysschemas_collection
		ok         bool

		sch_dbid  int64
		sch_schid int64
		sch_name  string
	)

	// read all schemas from master

	master = dictmgr.dm_master
	sysschemas = &dictmgr.dm_SYSSCHEMAS

	sysschemas.schid_map = make(map[Sysschema_schidkey_t]*Sysschema, SPEC_SYSSCHEMAS_COLLECION_DEFAULT_SIZE)
	sysschemas.name_map = make(map[Sysschema_namekey_t]*Sysschema, SPEC_SYSSCHEMAS_COLLECION_DEFAULT_SIZE)

	if stmt, err = master.Prepare("SELECT sch_dbid, sch_schid, sch_name FROM sysschemas"); err != nil {
		log.Panicf("%s", err)
	}

	for err = stmt.Query(); err == nil; err = stmt.Next() {
		// read schema record from master

		if err = stmt.Scan(&sch_dbid, &sch_schid, &sch_name); err != nil {
			break
		}

		// fill in SYSSCHEMAS

		schema := &Sysschema{
			Sch_dbid:  sch_dbid,
			Sch_schid: sch_schid,
			Sch_name:  sch_name,
		}

		if _, ok = sysschemas.schid_map[Sysschema_schidkey_t{Sch_dbid: schema.Sch_dbid, Sch_schid: schema.Sch_schid}]; ok == true {
			panic("impossible") // prevented by pk on dbid, schid
		}

		if _, ok = sysschemas.name_map[Sysschema_namekey_t{Sch_dbid: schema.Sch_dbid, Sch_name: schema.Sch_name}]; ok == true {
			panic("impossible") // prevented by unique index on dbid, name
		}

		sysschemas.schid_map[Sysschema_schidkey_t{Sch_dbid: schema.Sch_dbid, Sch_schid: schema.Sch_schid}] = schema // fill in SYSSCHEMAS
		sysschemas.name_map[Sysschema_namekey_t{Sch_dbid: schema.Sch_dbid, Sch_name: schema.Sch_name}] = schema

		if sysschemas.schid_nextval < schema.Sch_schid {
			sysschemas.schid_nextval = schema.Sch_schid
		}
	}

	if err != io.EOF {
		log.Panicf("%s", err)
	}

	switch {
	case sysschemas.schid_nextval < SCHID_START: // no schema except "dbo" schemas exists, with schid==0
		sysschemas.schid_nextval = SCHID_START // CREATE SCHEMA will use this value for the next schid

	default:
		sysschemas.schid_nextval++ // CREATE SCHEMA will use this value for the next schid
	}

	stmt.Close() // release resources held by stmt in C heap
}

// Add_sysdatabase implements CREATE DATABASE statement.
//
func (dictmgr *Dictionary_manager) Add_sysdatabase(context *rsql.Context, database_name string) *rsql.Error {
	var (
		sysdatabases  *Sysdatabases_collection
		sysschemas    *Sysschemas_collection
		sysprincipals *Sysprincipals_collection
		database      *Sysdatabase
		schema        *Sysschema
		user_dbo      *Sysprincipal
		role_public   *Sysprincipal
		ok            bool
	)

	rsql.Assert(database_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	// prepare new database

	sysdatabases = &dictmgr.dm_SYSDATABASES

	if sysdatabases.dbid_nextval == TRASHDB_DBID && database_name != rsql.TRASHDB {
		log.Panicf("MASTER: create database with dbid=%d must be trashdb", TRASHDB_DBID)
	}

	database = &Sysdatabase{
		Db_dbid:        sysdatabases.dbid_nextval,
		Db_name:        database_name,
		Db_create_date: now(),
		Db_status:      DB_STATUS_BEING_CREATED,
		Db_mode:        DB_MODE_READ_WRITE,
		Db_access:      DB_ACCESS_MULTI_USER,
		Db_system_flag: 0,
	}

	if database.Db_dbid == TRASHDB_DBID {
		database.Db_system_flag = DB_SYSFLAG_TRASHDB
	}

	if _, ok = sysdatabases.dbid_map[database.Db_dbid]; ok == true {
		log.Panicf("MASTER: sysdatabase dbid=[%d] already exists.", database.Db_dbid)
	}

	if _, ok = sysdatabases.name_map[database.Db_name]; ok == true {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, database.Db_name)
	}

	// prepare new schema 'dbo'

	sysschemas = &dictmgr.dm_SYSSCHEMAS

	schema = &Sysschema{Sch_dbid: database.Db_dbid, Sch_schid: SCHID_DBO, Sch_name: "dbo"}

	if _, ok = sysschemas.schid_map[Sysschema_schidkey_t{Sch_dbid: database.Db_dbid, Sch_schid: schema.Sch_schid}]; ok == true {
		log.Panicf("MASTER: sysschema id=[%d, %d] already exists.", database.Db_dbid, schema.Sch_schid)
	}

	if _, ok = sysschemas.name_map[Sysschema_namekey_t{Sch_dbid: database.Db_dbid, Sch_name: schema.Sch_name}]; ok == true {
		log.Panicf("MASTER: sysschema name=[%d, %s] already exists.", database.Db_dbid, schema.Sch_name)
	}

	// prepare new user 'dbo'

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	user_dbo = &Sysprincipal{
		Pal_palid:               sysprincipals.palid_nextval,
		Pal_dbid:                database.Db_dbid,
		Pal_name:                "dbo",
		Pal_type:                PAL_TYPE_USER,
		Pal_create_date:         database.Db_create_date,
		Pal_default_schema_name: "dbo",
		Pal_login_id:            SA_LOGIN_ID,
		Pal_system_flag:         PAL_SYSFLAG_USER_DBO,
	}

	if _, ok = sysprincipals.palid_map[user_dbo.Pal_palid]; ok == true {
		log.Panicf("MASTER: sysprincipal id %d already exists.", user_dbo.Pal_palid)
	}

	if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: user_dbo.Pal_dbid, Pal_name: user_dbo.Pal_name}]; ok == true {
		log.Panicf("MASTER: sysprincipal %s.%s already exists.", database_name, user_dbo.Pal_name)
	}

	// prepare new role 'public'

	role_public = &Sysprincipal{
		Pal_palid:               sysprincipals.palid_nextval + 1,
		Pal_dbid:                database.Db_dbid,
		Pal_name:                "public",
		Pal_type:                PAL_TYPE_ROLE,
		Pal_create_date:         database.Db_create_date,
		Pal_default_schema_name: "",
		Pal_login_id:            0,
		Pal_system_flag:         PAL_SYSFLAG_ROLE_PUBLIC,
	}

	if _, ok = sysprincipals.palid_map[role_public.Pal_palid]; ok == true {
		log.Panicf("MASTER: sysprincipal id %d already exists.", role_public.Pal_palid)
	}

	if _, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: role_public.Pal_dbid, Pal_name: role_public.Pal_name}]; ok == true {
		log.Panicf("MASTER: sysprincipal %s.%s already exists.", database_name, role_public.Pal_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	sysdatabases.dbid_nextval++
	sysprincipals.palid_nextval++ // for user dbo
	sysprincipals.palid_nextval++ // for role public

	sysdatabases.dbid_map[database.Db_dbid] = database
	sysdatabases.name_map[database.Db_name] = database

	sysschemas.schid_map[Sysschema_schidkey_t{Sch_dbid: database.Db_dbid, Sch_schid: schema.Sch_schid}] = schema
	sysschemas.name_map[Sysschema_namekey_t{Sch_dbid: database.Db_dbid, Sch_name: schema.Sch_name}] = schema

	sysprincipals.palid_map[user_dbo.Pal_palid] = user_dbo
	sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: user_dbo.Pal_dbid, Pal_name: user_dbo.Pal_name}] = user_dbo

	rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user_dbo.Pal_login_id, dbid: user_dbo.Pal_dbid}] == 0)
	dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user_dbo.Pal_login_id, dbid: user_dbo.Pal_dbid}] = user_dbo.Pal_palid

	sysprincipals.palid_map[role_public.Pal_palid] = role_public
	sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: role_public.Pal_dbid, Pal_name: role_public.Pal_name}] = role_public

	// write to master.db

	args := sqlite3.NamedArgs{ // write database record
		"$db_dbid":        database.Db_dbid,
		"$db_name":        database.Db_name,
		"$db_create_date": format_time(database.Db_create_date),
		"$db_status":      int64(database.Db_status),
		"$db_status_text": database.Db_status.String(),
		"$db_mode":        int64(database.Db_mode),
		"$db_mode_text":   database.Db_mode.String(),
		"$db_access":      int64(database.Db_access),
		"$db_access_text": database.Db_access.String(),
		"$db_system_flag": int64(database.Db_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSDATABASES_INSERT, args) // status is DB_STATUS_BEING_CREATED

	args = sqlite3.NamedArgs{ // write schema record
		"$sch_dbid":  database.Db_dbid,
		"$sch_schid": SCHID_DBO,
		"$sch_name":  "dbo",
	}
	dictmgr.Execute(DICT_STMT_SYSSCHEMAS_INSERT, args)

	args = sqlite3.NamedArgs{ // write user record
		"$pal_palid":               user_dbo.Pal_palid,
		"$pal_dbid":                user_dbo.Pal_dbid,
		"$pal_name":                user_dbo.Pal_name,
		"$pal_type":                int64(user_dbo.Pal_type),
		"$pal_create_date":         format_time(user_dbo.Pal_create_date),
		"$pal_default_schema_name": user_dbo.Pal_default_schema_name,
		"$pal_login_id":            user_dbo.Pal_login_id,
		"$pal_system_flag":         int64(user_dbo.Pal_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_INSERT, args)

	args = sqlite3.NamedArgs{ // write role record
		"$pal_palid":               role_public.Pal_palid,
		"$pal_dbid":                role_public.Pal_dbid,
		"$pal_name":                role_public.Pal_name,
		"$pal_type":                int64(role_public.Pal_type),
		"$pal_create_date":         format_time(role_public.Pal_create_date),
		"$pal_default_schema_name": role_public.Pal_default_schema_name,
		"$pal_login_id":            nil, // not used. MUST BE nil
		"$pal_system_flag":         int64(role_public.Pal_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_INSERT, args)

	dictmgr.COMMIT_and_BEGIN_EXCLUSIVE() // first COMMIT, don't unlock dictmgr, and BEGIN EXCLUSIVE

	//====================== create directories ======================

	rsql.Must_create_directory(rsql.Db_path(rsql.DIRECTORY_DATA, database.Db_dbid), rsql.DIR_PERM) // create database directory
	rsql.Must_create_directory(rsql.Db_path(rsql.DIRECTORY_INDEX, database.Db_dbid), rsql.DIR_PERM)

	rsql.Must_create_directory(rsql.Schema_path(rsql.DIRECTORY_DATA, database.Db_dbid, schema.Sch_schid), rsql.DIR_PERM) // create schema directory
	rsql.Must_create_directory(rsql.Schema_path(rsql.DIRECTORY_INDEX, database.Db_dbid, schema.Sch_schid), rsql.DIR_PERM)

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	database.Db_status = DB_STATUS_ONLINE

	// write to master.db

	args = sqlite3.NamedArgs{
		"$db_dbid":        database.Db_dbid,
		"$db_status":      int64(DB_STATUS_ONLINE),
		"$db_status_text": DB_STATUS_ONLINE.String(),
	}
	dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_STATUS, args) // status becomes DB_STATUS_ONLINE

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Change_sysdatabase implements ALTER DATABASE statement.
//
func (dictmgr *Dictionary_manager) Change_sysdatabase(context *rsql.Context, database_name string, changes Database_params) *rsql.Error {
	var (
		sysdatabases *Sysdatabases_collection
		database     *Sysdatabase
		ok           bool
		new_status   Db_status_t
		new_mode     Db_mode_t
		new_access   Db_access_t
	)

	rsql.Assert(database_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	sysdatabases = &dictmgr.dm_SYSDATABASES

	if database, ok = sysdatabases.name_map[database_name]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name)
	}

	if database.Db_system_flag != 0 { // e.g. trashdb
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, database_name)
	}

	if changes.Dbp_name == database_name {
		changes.Dbp_name = ""
	}

	if changes.Dbp_name != "" {
		if _, ok = sysdatabases.name_map[changes.Dbp_name]; ok == true {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, changes.Dbp_name)
		}
	}

	if changes.Dbp_status != "" && !(database.Db_status == DB_STATUS_ONLINE || database.Db_status == DB_STATUS_OFFLINE) {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_OPERATION_FORBIDDEN_BECAUSE_STATUS, rsql.ERROR_BATCH_ABORT, database_name, database.Db_status)
	}

	if changes.Dbp_mode != "" && !(database.Db_status == DB_STATUS_ONLINE || database.Db_status == DB_STATUS_OFFLINE) {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_OPERATION_FORBIDDEN_BECAUSE_STATUS, rsql.ERROR_BATCH_ABORT, database_name, database.Db_status)
	}

	if changes.Dbp_access != "" && !(database.Db_status == DB_STATUS_ONLINE || database.Db_status == DB_STATUS_OFFLINE) {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_OPERATION_FORBIDDEN_BECAUSE_STATUS, rsql.ERROR_BATCH_ABORT, database_name, database.Db_status)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	if changes.Dbp_name != "" {
		database.Db_name = changes.Dbp_name

		rsql.Assert(sysdatabases.name_map[database_name] != nil)
		rsql.Assert(sysdatabases.name_map[database.Db_name] == nil)

		delete(sysdatabases.name_map, database_name)
		sysdatabases.name_map[database.Db_name] = database

		// write to master.db

		args := sqlite3.NamedArgs{
			"$db_dbid": database.Db_dbid,
			"$db_name": database.Db_name,
		}
		dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_NAME, args)
	}

	if changes.Dbp_status != "" {
		switch changes.Dbp_status {
		case lex.AUXWORD_ONLINE:
			new_status = DB_STATUS_ONLINE
		case lex.AUXWORD_OFFLINE:
			new_status = DB_STATUS_OFFLINE
		case lex.AUXWORD_CORRUPTED:
			new_status = DB_STATUS_CORRUPTED
		default:
			panic("impossible")
		}

		database.Db_status = new_status

		// write to master.db

		args := sqlite3.NamedArgs{
			"$db_dbid":        database.Db_dbid,
			"$db_status":      int64(new_status),
			"$db_status_text": new_status.String(),
		}
		dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_STATUS, args)
	}

	if changes.Dbp_mode != "" {
		switch changes.Dbp_mode {
		case lex.AUXWORD_READ_ONLY:
			new_mode = DB_MODE_READ_ONLY
		case lex.AUXWORD_READ_WRITE:
			new_mode = DB_MODE_READ_WRITE
		default:
			panic("impossible")
		}

		database.Db_mode = new_mode

		// write to master.db

		args := sqlite3.NamedArgs{
			"$db_dbid":      database.Db_dbid,
			"$db_mode":      int64(new_mode),
			"$db_mode_text": new_mode.String(),
		}
		dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_MODE, args)
	}

	if changes.Dbp_access != "" {
		switch changes.Dbp_access {
		case lex.AUXWORD_RESTRICTED_USER:
			new_access = DB_ACCESS_RESTRICTED_USER
		case lex.AUXWORD_MULTI_USER:
			new_access = DB_ACCESS_MULTI_USER
		default:
			panic("impossible")
		}

		database.Db_access = new_access

		// write to master.db

		args := sqlite3.NamedArgs{
			"$db_dbid":        database.Db_dbid,
			"$db_access":      int64(new_access),
			"$db_access_text": new_access.String(),
		}
		dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_ACCESS, args)
	}

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// used by Install_master_db(), to put trashdb in read only mode at installation.
//
func (dictmgr *Dictionary_manager) Change_trashdb_to_read_only_at_install(context *rsql.Context) *rsql.Error {
	var (
		database_name string
		sysdatabases  *Sysdatabases_collection
		database      *Sysdatabase
		ok            bool
		new_mode      Db_mode_t
	)

	database_name = rsql.TRASHDB
	new_mode = DB_MODE_READ_ONLY

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	sysdatabases = &dictmgr.dm_SYSDATABASES

	if database, ok = sysdatabases.name_map[database_name]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	database.Db_mode = new_mode

	// write to master.db

	args := sqlite3.NamedArgs{
		"$db_dbid":      database.Db_dbid,
		"$db_mode":      int64(new_mode),
		"$db_mode_text": new_mode.String(),
	}
	dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_MODE, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Change_database_status changes the status of the database (DB_STATUS_ONLINE, DB_STATUS_OFFLINE, DB_STATUS_CORRUPTED).
// It is used by the RESTORE command.
//
func (dictmgr *Dictionary_manager) Change_database_status(context *rsql.Context, database_name string, new_status Db_status_t) *rsql.Error {
	var (
		sysdatabases *Sysdatabases_collection
		database     *Sysdatabase
		ok           bool
	)

	rsql.Assert(database_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	sysdatabases = &dictmgr.dm_SYSDATABASES

	if database, ok = sysdatabases.name_map[database_name]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name)
	}

	if database.Db_system_flag != 0 { // e.g. trashdb
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, database_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	database.Db_status = new_status // DB_STATUS_ONLINE, DB_STATUS_OFFLINE, DB_STATUS_CORRUPTED

	// write to master.db

	args := sqlite3.NamedArgs{
		"$db_dbid":        database.Db_dbid,
		"$db_status":      int64(new_status),
		"$db_status_text": new_status.String(),
	}
	dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_STATUS, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Remove_sysdatabase implements DROP DATABASE statement.
//
func (dictmgr *Dictionary_manager) Remove_sysdatabase(context *rsql.Context, database_name string) *rsql.Error {
	var (
		sysdatabases        *Sysdatabases_collection
		sysschemas          *Sysschemas_collection
		database            *Sysdatabase
		ok                  bool
		principal_palid_set map[int64]*Sysprincipal
	)

	rsql.Assert(database_name != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if rsql_err := dictmgr.check_is_SA(context); rsql_err != nil {
		return rsql_err
	}

	sysdatabases = &dictmgr.dm_SYSDATABASES

	if database, ok = sysdatabases.name_map[database_name]; ok == false {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name)
	}

	if database.Db_system_flag != 0 { // e.g. trashdb
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_OPERATION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, database_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	// create a set of all principals (users and roles) in database

	principal_palid_set = make(map[int64]*Sysprincipal)

	for _, principal := range dictmgr.dm_SYSPRINCIPALS.palid_map { // for all principals
		if principal.Pal_dbid == database.Db_dbid { // which are in database
			principal_palid_set[principal.Pal_palid] = principal
		}
	}

	// remove permissions belonging to any principal in principal_palid_set

	for key, _ := range dictmgr.dm_SYSPERMISSIONS.key_map {
		if _, ok := principal_palid_set[key.Palid]; ok == true {
			delete(dictmgr.dm_SYSPERMISSIONS.key_map, key)
		}
	}

	// remove principals (users and roles)

	for _, principal := range principal_palid_set {
		rsql.Assert(dictmgr.dm_SYSPRINCIPALS.palid_map[principal.Pal_palid] != nil)
		rsql.Assert(dictmgr.dm_SYSPRINCIPALS.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}] != nil)

		delete(dictmgr.dm_SYSPRINCIPALS.palid_map, principal.Pal_palid)
		delete(dictmgr.dm_SYSPRINCIPALS.name_map, Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name})

		if principal.Pal_type == PAL_TYPE_USER {
			rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: principal.Pal_login_id, dbid: principal.Pal_dbid}] == principal.Pal_palid)
			delete(dictmgr.dm_map_login_dbid_user, Map_login_dbid_key{login_id: principal.Pal_login_id, dbid: principal.Pal_dbid})
		}
	}

	// remove all gtabledefs belonging to dbid

	for gtblid, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
		if gtabledef.Gdbid == database.Db_dbid {
			rsql.Assert(gtblid == gtabledef.Gtblid)
			rsql.Assert(dictmgr.dm_SYSTABLES.gtblid_map[gtabledef.Gtblid] != nil)
			rsql.Assert(dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] != nil)

			delete(dictmgr.dm_SYSTABLES.gtblid_map, gtabledef.Gtblid)
			delete(dictmgr.dm_SYSTABLES.name_map, Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name})
		}
	}

	// remove all schemas belonging to dbid

	sysschemas = &dictmgr.dm_SYSSCHEMAS

	for key, schema := range sysschemas.schid_map {
		if schema.Sch_dbid == database.Db_dbid {
			rsql.Assert(key.Sch_dbid == schema.Sch_dbid)
			rsql.Assert(key.Sch_schid == schema.Sch_schid)
			rsql.Assert(sysschemas.schid_map[Sysschema_schidkey_t{Sch_dbid: schema.Sch_dbid, Sch_schid: schema.Sch_schid}] != nil)
			rsql.Assert(sysschemas.name_map[Sysschema_namekey_t{Sch_dbid: schema.Sch_dbid, Sch_name: schema.Sch_name}] != nil)

			delete(sysschemas.schid_map, Sysschema_schidkey_t{Sch_dbid: schema.Sch_dbid, Sch_schid: schema.Sch_schid})
			delete(sysschemas.name_map, Sysschema_namekey_t{Sch_dbid: schema.Sch_dbid, Sch_name: schema.Sch_name})
		}
	}

	// remove database

	rsql.Assert(sysdatabases.name_map[database.Db_name] != nil)
	rsql.Assert(sysdatabases.dbid_map[database.Db_dbid] != nil)

	delete(sysdatabases.name_map, database.Db_name)
	delete(sysdatabases.dbid_map, database.Db_dbid)

	// update database status in master.db

	args := sqlite3.NamedArgs{
		"$db_dbid":        database.Db_dbid,
		"$db_status":      int64(DB_STATUS_BEING_DROPPED),
		"$db_status_text": DB_STATUS_BEING_DROPPED.String(),
	}
	dictmgr.Execute(DICT_STMT_SYSDATABASES_UPDATE_STATUS, args)

	dictmgr.COMMIT_and_BEGIN_EXCLUSIVE() // first COMMIT, don't unlock dictmgr, and BEGIN EXCLUSIVE

	//=== delete directories and content ===

	rsql.Must_remove_directory_all(rsql.Db_path(rsql.DIRECTORY_DATA, database.Db_dbid)) // remove database directory and all content. It doesn't panic if directory doesn't exist.
	rsql.Must_remove_directory_all(rsql.Db_path(rsql.DIRECTORY_INDEX, database.Db_dbid))

	// delete permissions, role membership and principals, coldefs and tabledefs records from master.db

	args = sqlite3.NamedArgs{
		"$dbid": database.Db_dbid,
	}

	dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_DELETE_ALL_DBID, args)

	dictmgr.Execute(DICT_STMT_SYSROLEMEMBERS_DELETE_ALL_DBID, args)
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_DELETE_ALL_DBID, args)

	dictmgr.Execute(DICT_STMT_SYSCOLDEFS_DELETE_ALL_DBID, args)
	dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_DELETE_ALL_DBID, args)
	dictmgr.Execute(DICT_STMT_SYSGTABLEDEFS_DELETE_ALL_DBID, args)

	// delete schema and database records from master.db

	args = sqlite3.NamedArgs{
		"$sch_dbid": database.Db_dbid,
	}
	dictmgr.Execute(DICT_STMT_SYSSCHEMAS_IN_DBID_DELETE, args)

	args = sqlite3.NamedArgs{
		"$db_dbid": database.Db_dbid,
	}
	dictmgr.Execute(DICT_STMT_SYSDATABASES_DELETE, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Change_authorization implements ALTER AUTHORIZATION statement.
//
func (dictmgr *Dictionary_manager) Change_authorization(context *rsql.Context, database_name string, change_owner_login string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		sysprincipals *Sysprincipals_collection
		user          *Sysprincipal
		database      *Sysdatabase
		login         *Syslogin
		ok            bool
	)

	rsql.Assert(database_name != "" && change_owner_login != "")

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	sysprincipals = &dictmgr.dm_SYSPRINCIPALS

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil { // retrieve database
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if user, ok = sysprincipals.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: "dbo"}]; ok == false { // find database owner 'dbo'
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, database_name, "dbo")
	}

	if user.Pal_type != PAL_TYPE_USER {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_IS_ROLE, rsql.ERROR_BATCH_ABORT, database_name, "dbo")
	}

	if login, rsql_err = dictmgr.get_login(change_owner_login); rsql_err != nil { // retrieve login
		return rsql_err
	}

	if user.Pal_login_id == login.Lg_login_id {
		return nil
	}

	if db_name_x, user_name_x, ok := dictmgr.login_id_exists_in_database(database.Db_dbid, login.Lg_login_id); ok == true {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_ALTER_USER_LOGIN_ID_DUPLICATE, rsql.ERROR_BATCH_ABORT, db_name_x, "dbo", login.Lg_name, user_name_x)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid}] == user.Pal_palid)
	delete(dictmgr.dm_map_login_dbid_user, Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid})

	user.Pal_login_id = login.Lg_login_id

	rsql.Assert(dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid}] == 0)
	dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: user.Pal_login_id, dbid: user.Pal_dbid}] = user.Pal_palid

	// write to master.db

	args := sqlite3.NamedArgs{
		"$pal_palid":               user.Pal_palid, // never changes
		"$pal_dbid":                user.Pal_dbid,
		"$pal_name":                user.Pal_name,
		"$pal_type":                int64(user.Pal_type),
		"$pal_create_date":         format_time(user.Pal_create_date),
		"$pal_default_schema_name": user.Pal_default_schema_name,
		"$pal_login_id":            user.Pal_login_id,
		"$pal_system_flag":         int64(user.Pal_system_flag),
	}
	dictmgr.Execute(DICT_STMT_SYSPRINCIPALS_UPDATE, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Use_sysdatabase implements USE database statement.
//
func (dictmgr *Dictionary_manager) Use_sysdatabase(login_id int64, login_name_for_info string, database_name string) (dbid int64, schema_name string, schema_schid int64, user_name string, user_palid int64, rsql_err *rsql.Error) {
	var (
		login    *Syslogin
		database *Sysdatabase
		schema   *Sysschema
		user     *Sysprincipal
		ok       bool
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[login_id]; ok == false { // check if login has not been dropped by this session or by another session
		return 0, "", 0, "", 0, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_ID_HAS_BEEN_DROPPED, rsql.ERROR_SESSION_ABORT, login_name_for_info, login_id)
	}

	if database, ok = dictmgr.dm_SYSDATABASES.name_map[database_name]; ok == false {
		return 0, "", 0, "", 0, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS, rsql.ERROR_SESSION_ABORT, database_name) // SESSION_ABORT because we don't want to run subsequent batches if current db is invalid
	}

	schema_name = "dbo"

	if schema, ok = dictmgr.dm_SYSSCHEMAS.name_map[Sysschema_namekey_t{Sch_dbid: database.Db_dbid, Sch_name: schema_name}]; ok == false {
		return 0, "", 0, "", 0, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_SCHEMA_NAME_NOT_EXISTS, rsql.ERROR_SESSION_ABORT, database_name, schema_name)
	}

	switch login_id {
	case SA_LOGIN_ID: // for 'sa', user is 'dbo'
		if user, ok = dictmgr.dm_SYSPRINCIPALS.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: "dbo"}]; ok == false {
			return 0, "", 0, "", 0, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRINCIPAL_NAME_NOT_EXISTS, rsql.ERROR_SESSION_ABORT, database_name, "dbo")
		}

	default:
		if user_palid, ok = dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: login.Lg_login_id, dbid: database.Db_dbid}]; ok == false {
			return 0, "", 0, "", 0, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_LOGIN_HAS_NO_USER_FOR_DATABASE, rsql.ERROR_SESSION_ABORT, login.Lg_name, database_name)
		}

		if user, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[user_palid]; ok == false {
			log.Panicf("user_palid %d not found.", user_palid)
		}
	}

	return database.Db_dbid, schema.Sch_name, schema.Sch_schid, user.Pal_name, user.Pal_palid, nil
}

// Get_sysdatabase_name implements DB_NAME function.
//
func (dictmgr *Dictionary_manager) Get_sysdatabase_name(dbid int64) (database_name string, ok bool) {
	var (
		database *Sysdatabase
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if database, ok = dictmgr.dm_SYSDATABASES.dbid_map[dbid]; ok == false {
		return "", false
	}

	return database.Db_name, true
}

// Get_sysdatabase_id implements DB_ID function.
//
func (dictmgr *Dictionary_manager) Get_sysdatabase_id(database_name []byte) (dbid int64, ok bool) {
	var (
		database_name_lc string
		database         *Sysdatabase
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	database_name_lc = strings.ToLower(string(database_name))

	if database, ok = dictmgr.dm_SYSDATABASES.name_map[database_name_lc]; ok == false {
		return 0, false
	}

	return database.Db_dbid, true
}

// Get_sysschema_name implements SCHEMA_NAME function.
//
func (dictmgr *Dictionary_manager) Get_sysschema_name(dbid int64, schid int64) (schema_name string, ok bool) {
	var (
		schema *Sysschema
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if schema, ok = dictmgr.dm_SYSSCHEMAS.schid_map[Sysschema_schidkey_t{Sch_dbid: dbid, Sch_schid: schid}]; ok == false {
		return "", false
	}

	return schema.Sch_name, true
}

// Get_sysschema_id implements SCHEMA_ID function.
//
func (dictmgr *Dictionary_manager) Get_sysschema_id(dbid int64, schema_name []byte) (schid int64, ok bool) {
	var (
		schema_name_lc string
		schema         *Sysschema
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	schema_name_lc = strings.ToLower(string(schema_name))

	if schema, ok = dictmgr.dm_SYSSCHEMAS.name_map[Sysschema_namekey_t{Sch_dbid: dbid, Sch_name: schema_name_lc}]; ok == false {
		return 0, false
	}

	return schema.Sch_schid, true
}

// Get_sysdatabase_info gets status, mode and access of the database.
// It is used by SHOW INFO.
//
func (dictmgr *Dictionary_manager) Get_sysdatabase_info(database_name string) (info string, rsql_err *rsql.Error) {
	var (
		database *Sysdatabase
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if database, rsql_err = dictmgr.get_database(strings.ToLower(database_name)); rsql_err != nil {
		return "", rsql_err
	}

	info = database.Database_info()
	if info != "" {
		info = "(" + info + ")"
	}

	return info, nil
}

//================== SYSTABLES ===================

type Systables_collection struct {
	gtblid_map map[int64]*rsql.GTabledef              // key is gtblid of gtable
	name_map   map[Systable_namekey_t]*rsql.GTabledef // key is name of gtable

	objid_nextval int64
}

type Systable_namekey_t struct { // unique key
	Gdbid       int64
	Gschid      int64
	Gtable_name string
}

// used by ALTER TABLE statements.
//
type Alter_column_nullability_t uint8

const (
	ALTER_COLUMN_NULLABILITY_NONE Alter_column_nullability_t = 0
	ALTER_COLUMN_NULL             Alter_column_nullability_t = 1
	ALTER_COLUMN_NOT_NULL         Alter_column_nullability_t = 2
)

// used to pass arguments of ALTER TABLE to vm.
//
type Table_params struct {
	Table_qname rsql.Object_qname_t

	Change_table_name          string
	Change_table_name_original string

	Column_name                 string
	Change_column_name          string
	Change_column_name_original string
	Change_column_nullability   Alter_column_nullability_t
}

// used to pass arguments of CREATE INDEX to vm.
//
type Index_params struct {
	Table_qname         rsql.Object_qname_t
	Index_name          string
	Index_name_original string
	Index_type          rsql.Td_index_type_t
	Cluster_type        rsql.Td_cluster_type_t
	Colname_list        []string
}

// Add_systable implements CREATE TABLE statement.
//
func (dictmgr *Dictionary_manager) Add_systable(context *rsql.Context, database_name string, schema_name string, gtabledef *rsql.GTabledef) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		database      *Sysdatabase
		schema        *Sysschema
		systables     *Systables_collection
		base_tabledef *rsql.Tabledef
		ok            bool
		wcache        *cache.Wcache
	)

	rsql.Assert(gtabledef.Gtable_name != "")
	rsql.Assert(context.Ctx_trancount == 0) // not inside a transaction

	wcache = context.Ctx_wcache.(*cache.Wcache)

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	systables = &dictmgr.dm_SYSTABLES

	base_tabledef = gtabledef.Base

	if database, schema, rsql_err = dictmgr.get_schema(database_name, schema_name); rsql_err != nil { // retrieve database and schema
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if _, ok = systables.name_map[Systable_namekey_t{Gdbid: database.Db_dbid, Gschid: schema.Sch_schid, Gtable_name: gtabledef.Gtable_name}]; ok == true {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OBJECT_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, database.Db_name, schema.Sch_name, gtabledef.Gtable_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	gtabledef.Gtblid = systables.objid_nextval
	gtabledef.Gdbid = database.Db_dbid
	gtabledef.Gschid = schema.Sch_schid
	gtabledef.Create_date = now()

	base_tabledef.Td_tblid = gtabledef.Gtblid
	base_tabledef.Td_dbid = database.Db_dbid
	base_tabledef.Td_schid = schema.Sch_schid
	base_tabledef.Td_base_gtblid = gtabledef.Gtblid
	base_tabledef.Td_file_path = rsql.Tabledef_path(rsql.DIRECTORY_DATA, base_tabledef.Td_dbid, base_tabledef.Td_schid, base_tabledef.Td_tblid)

	systables.objid_nextval++

	for _, indexdef := range gtabledef.Indexmap {
		indexdef.Td_tblid = systables.objid_nextval
		indexdef.Td_dbid = database.Db_dbid
		indexdef.Td_schid = schema.Sch_schid
		indexdef.Td_base_gtblid = gtabledef.Gtblid
		indexdef.Td_file_path = rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid)

		systables.objid_nextval++
	}

	rsql.Assert(systables.gtblid_map[gtabledef.Gtblid] == nil)
	rsql.Assert(systables.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] == nil)

	systables.gtblid_map[gtabledef.Gtblid] = gtabledef
	systables.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] = gtabledef

	// write to master.db

	dictmgr.DB_insert_GTabledef_all(gtabledef) // write gtabledef (status is TD_STATUS_BEING_CREATED)

	dictmgr.COMMIT_and_BEGIN_EXCLUSIVE() // first COMMIT, don't unlock dictmgr, and BEGIN EXCLUSIVE

	//====================== create table files ======================

	rsql.Must_create_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_DATA, gtabledef.Base.Td_dbid, gtabledef.Base.Td_schid, gtabledef.Base.Td_tblid), rsql.TBLFILE_PERM) // create base table file
	if rsql_err = wcache.Create_info_page_and_first_zone_allocator(gtabledef.Base); rsql_err != nil {
		goto CREATE_TABLE_ERROR
	}

	for _, indexdef := range gtabledef.Indexmap { // write all index tables
		rsql.Must_create_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid), rsql.TBLFILE_PERM) // create index table file
		if rsql_err = wcache.Create_info_page_and_first_zone_allocator(indexdef); rsql_err != nil {
			goto CREATE_TABLE_ERROR
		}
	}

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	wcache.COMMIT() // table pages are written to disk

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	gtabledef.Base.Td_status = rsql.TD_STATUS_VALID

	for _, indexdef := range gtabledef.Indexmap {
		indexdef.Td_status = rsql.TD_STATUS_VALID
	}

	// write to master.db

	dictmgr.DB_change_GTabledef_status_all(gtabledef, rsql.TD_STATUS_VALID)

	dictmgr.COMMIT() //----- final COMMIT

	return nil

	//=================================================================================================================
	//                               error occurred during creation of table or index
	//=================================================================================================================
CREATE_TABLE_ERROR:
	// remove gtabledef

	rsql.Assert(dictmgr.dm_SYSTABLES.gtblid_map[gtabledef.Gtblid] != nil)
	rsql.Assert(dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] != nil)

	delete(dictmgr.dm_SYSTABLES.gtblid_map, gtabledef.Gtblid)
	delete(dictmgr.dm_SYSTABLES.name_map, Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name})

	// delete table files

	filebag := wcache.Filebag()

	filebag.Close_file(gtabledef.Base)
	rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_DATA, gtabledef.Base.Td_dbid, gtabledef.Base.Td_schid, gtabledef.Base.Td_tblid)) // remove base table file

	for _, indexdef := range gtabledef.Indexmap { // remove all index tables
		filebag.Close_file(indexdef)
		rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid)) // remove index table file
	}

	// delete coldefs and tabledefs records from master.db

	dictmgr.DB_delete_GTabledef_all(gtabledef.Gtblid) // delete gtabledef completely from master.db

	dictmgr.COMMIT() //----- final COMMIT

	return rsql_err
}

// Remove_systable implements DROP TABLE statement.
//
func (dictmgr *Dictionary_manager) Remove_systable(context *rsql.Context, table_qname rsql.Object_qname_t) *rsql.Error {
	var (
		rsql_err  *rsql.Error
		database  *Sysdatabase
		gtabledef *rsql.GTabledef
		//wcache        *cache.Wcache
	)

	rsql.Assert(table_qname.Database_name != "" && table_qname.Schema_name != "" && table_qname.Object_name != "")
	rsql.Assert(context.Ctx_trancount == 0) // not inside a transaction

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if database, _, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(table_qname); rsql_err != nil {

		if rsql_err.Message_id == rsql.ERROR_MASTER_OBJECT_NAME_NOT_EXISTS {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.WARNING_DROP_TABLE_NOT_FOUND, rsql.ERROR_IS_WARNING, table_qname.Database_name, table_qname.Schema_name, table_qname.Object_name)
		}

		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	// remove all permissions from gtabledef

	for key, _ := range dictmgr.dm_SYSPERMISSIONS.key_map {
		if key.Objid == gtabledef.Gtblid {
			delete(dictmgr.dm_SYSPERMISSIONS.key_map, key)
		}
	}

	// remove gtabledef

	rsql.Assert(dictmgr.dm_SYSTABLES.gtblid_map[gtabledef.Gtblid] != nil)
	rsql.Assert(dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] != nil)

	delete(dictmgr.dm_SYSTABLES.gtblid_map, gtabledef.Gtblid)
	delete(dictmgr.dm_SYSTABLES.name_map, Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name})

	// write to master.db

	dictmgr.DB_change_GTabledef_status_all(gtabledef, rsql.TD_STATUS_BEING_DROPPED)

	dictmgr.COMMIT_and_BEGIN_EXCLUSIVE() // first COMMIT, don't unlock dictmgr, and BEGIN EXCLUSIVE

	//=== delete table files ===

	filebag := context.Ctx_wcache.(*cache.Wcache).Filebag()

	filebag.Close_file(gtabledef.Base)
	rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_DATA, gtabledef.Base.Td_dbid, gtabledef.Base.Td_schid, gtabledef.Base.Td_tblid)) // remove base table file

	for _, indexdef := range gtabledef.Indexmap { // remove all index tables
		filebag.Close_file(indexdef)
		rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid)) // remove index table file
	}

	// delete permissions, coldefs and tabledefs records from master.db

	dictmgr.DB_delete_GTabledef_all(gtabledef.Gtblid) // delete gtabledef completely from master.db

	args := sqlite3.NamedArgs{
		"$gtblid": gtabledef.Gtblid,
	}
	dictmgr.Execute(DICT_STMT_SYSPERMISSIONS_DELETE_GTBLID, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Remove_sysindex implements DROP INDEX statement.
//
// It is not allowed to drop a clustered index (aka native index). You must drop the table and recreate a new one.
//
func (dictmgr *Dictionary_manager) Remove_sysindex(context *rsql.Context, table_qname rsql.Object_qname_t, index_name string) *rsql.Error {
	var (
		rsql_err  *rsql.Error
		database  *Sysdatabase
		gtabledef *rsql.GTabledef
		indexdef  *rsql.Tabledef
		ok        bool
		//wcache        *cache.Wcache
	)

	rsql.Assert(table_qname.Database_name != "" && table_qname.Schema_name != "" && table_qname.Object_name != "" && index_name != "")
	rsql.Assert(context.Ctx_trancount == 0) // not inside a transaction

	//wcache = context.Ctx_wcache.(*cache.Wcache)

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if database, _, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(table_qname); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if indexdef, ok = gtabledef.Indexmap[index_name]; ok == false {
		if gtabledef.Base.Td_index_name == index_name {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.WARNING_DROP_CLUSTERED_INDEX_NOT_ALLOWED, rsql.ERROR_IS_WARNING, table_qname.Database_name, table_qname.Schema_name, table_qname.Object_name, index_name)
		}

		return rsql.New_Error(rsql.ERROR_DICT, rsql.WARNING_DROP_INDEX_NOT_FOUND, rsql.ERROR_IS_WARNING, table_qname.Database_name, table_qname.Schema_name, table_qname.Object_name, index_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	// remove indexdef

	rsql.Assert(gtabledef.Indexmap[index_name] == indexdef)
	delete(gtabledef.Indexmap, index_name)

	// write to master.db

	dictmgr.DB_change_Tabledef_status(indexdef, rsql.TD_STATUS_BEING_DROPPED)

	dictmgr.COMMIT_and_BEGIN_EXCLUSIVE() // first COMMIT, don't unlock dictmgr, and BEGIN EXCLUSIVE

	//=== delete index file ===

	filebag := context.Ctx_wcache.(*cache.Wcache).Filebag()

	filebag.Close_file(indexdef)
	rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid)) // remove index table file

	// delete tabledef record from master.db

	dictmgr.DB_delete_Tabledef(indexdef.Td_tblid)

	dictmgr.COMMIT() //----- final COMMIT

	return nil //---> exit

	/*=== for info: this is the code for dropping native key. Probably contains errors. Not used any more, because dropping native index is really not a needed feature. ===

	gtabledef_clone = gtabledef.Clone() // we work on a copy of gtabledef

	index_name = Get_new_index_sysname_string(gtabledef_clone)

	if rsql_err = gtabledef_clone.Modify_native_key(index_name, index_name, rsql.TD_UNIQUE, []string{rsql.ROWID}); rsql_err != nil { // native key of gtabledef_clone is modified, and all its indexdefs too
		return rsql_err
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	rsql.Assert(dictmgr.dm_SYSTABLES.gtblid_map[gtabledef_clone.Gtblid] != nil)
	rsql.Assert(dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef_clone.Gdbid, Gschid: gtabledef_clone.Gschid, Gtable_name: gtabledef_clone.Gtable_name}] != nil)

	dictmgr.dm_SYSTABLES.gtblid_map[gtabledef_clone.Gtblid] = gtabledef_clone
	dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef_clone.Gdbid, Gschid: gtabledef_clone.Gschid, Gtable_name: gtabledef_clone.Gtable_name}] = gtabledef_clone

	rsql.Assert(gtabledef_clone.Get_any_invalid_Tabledef() == nil)

	// write to master.db

	if _, rsql_err = dictmgr.DB_substitute_GTabledef(context, gtabledef_clone); rsql_err != nil { // set status to TD_STATUS_BEING_DROPPED, delete and recreate all files, delete all records for old gtabledef, and create all records for the new gtabledef with TD_STATUS_VALID
		return rsql_err // error here, but no error should occur. What would happen ?
	}

	dictmgr.COMMIT() //----- final COMMIT

	return nil //---> exit
	======*/
}

// Add_sysindex implements CREATE INDEX and ALTER TABLE ADD CONSTRAINT statements.
//
func (dictmgr *Dictionary_manager) Add_sysindex(context *rsql.Context, table_qname rsql.Object_qname_t, index_name string, index_name_original string, index_type rsql.Td_index_type_t, cluster_type rsql.Td_cluster_type_t, keyname_list []string) (gtabledef_new_clustered *rsql.GTabledef, rsql_err *rsql.Error) {
	var (
		database           *Sysdatabase
		gtabledef_original *rsql.GTabledef
		gtabledef          *rsql.GTabledef
		ok                 bool
		indexdef           *rsql.Tabledef
		coldef_list        []*rsql.Coldef
		wcache             *cache.Wcache
	)

	rsql.Assert(table_qname.Database_name != "" && table_qname.Schema_name != "" && table_qname.Object_name != "" && len(keyname_list) != 0)
	rsql.Assert(context.Ctx_trancount == 0) // not inside a transaction

	wcache = context.Ctx_wcache.(*cache.Wcache)

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if database, _, gtabledef_original, rsql_err = dictmgr.database_schema_gtabledef_from_qname(table_qname); rsql_err != nil {
		return nil, rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return nil, rsql_err
	}

	if gtabledef_original.Get_any_invalid_Tabledef() != nil {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_FORBIDDEN_ON_INVALID_GTABLE, rsql.ERROR_BATCH_ABORT, table_qname.Database_name, table_qname.Schema_name, table_qname.Object_name)
	}

	gtabledef = gtabledef_original.Clone() // we work on a copy of gtabledef

	if index_name == "" {
		index_name = Get_new_index_sysname_string(gtabledef)
		index_name_original = index_name
	}

	if _, ok = gtabledef.Indexmap[index_name]; ok == true {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_INDEX_NAME_FOUND, rsql.ERROR_BATCH_ABORT, index_name)
	}

	if gtabledef.Base.Td_index_name == index_name {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_INDEX_NAME_FOUND, rsql.ERROR_BATCH_ABORT, index_name)
	}

	if index_type == rsql.TD_PRIMARY_KEY && cluster_type == rsql.TD_CLUSTER_TYPE_UNDEFINED {
		if gtabledef.Base.Nk_is_rowid() {
			cluster_type = rsql.TD_CLUSTERED
		}
	}

	if cluster_type == rsql.TD_CLUSTER_TYPE_UNDEFINED {
		rsql.Assert(index_type == rsql.TD_PRIMARY_KEY) // another clustered index already exists
		cluster_type = rsql.TD_NONCLUSTERED
	}

	if cluster_type == rsql.TD_CLUSTERED && gtabledef.Base.Nk_is_rowid() == false {
		return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_CLUSTERED_INDEX_ALREADY_EXISTS, rsql.ERROR_BATCH_ABORT, gtabledef.Base.Td_index_name_original)
	}

	if cluster_type == rsql.TD_CLUSTERED {
		if rsql_err = gtabledef.Modify_native_key(index_name, index_name_original, index_type, keyname_list); rsql_err != nil { // native key of gtabledef is modified, and all its indexdefs too
			return nil, rsql_err
		}

		return gtabledef, nil //---> exit. Nothing has been done here. The caller must drop the table and create a new one with this gtabledef.
	}

	//===== create nonclustered index =====

	indexdef = rsql.New_Tabledef(rsql.TD_TYPE_INDEX_TABLE)

	indexdef.Td_tblid = dictmgr.dm_SYSTABLES.objid_nextval

	indexdef.Td_dbid = gtabledef.Gdbid
	indexdef.Td_schid = gtabledef.Gschid
	indexdef.Td_base_gtblid = gtabledef.Gtblid
	indexdef.Td_index_name = index_name
	indexdef.Td_index_name_original = index_name_original

	indexdef.Td_file_path = rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid)

	indexdef.Td_durability = gtabledef.Base.Td_durability
	indexdef.Td_type = rsql.TD_TYPE_INDEX_TABLE
	indexdef.Td_index_type = index_type
	indexdef.Td_cluster_type = rsql.TD_NONCLUSTERED

	indexdef.Td_status = rsql.TD_STATUS_BEING_CREATED

	if coldef_list, rsql_err = rsql.Keyname2coldef_list(gtabledef, keyname_list); rsql_err != nil {
		return nil, rsql_err
	}

	if indexdef.Td_index_type == rsql.TD_PRIMARY_KEY { // check that columns of primary key have NOT NULL clause
		for _, coldef := range coldef_list {
			if coldef.Cd_NOT_NULL_flag == false {
				return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_INVALID_SYNTAX_PRIMARY_KEY_COLUMNS_MUST_BE_NOT_NULL, rsql.ERROR_BATCH_ABORT, coldef.Cd_colname)
			}
		}

		if tabledef_pk := gtabledef.Primary_key(); tabledef_pk != nil { // check that no PRIMARY KEY already exists
			return nil, rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PRIMARY_KEY_ALREADY_EXISTS, rsql.ERROR_BATCH_ABORT, tabledef_pk.Td_index_name)
		}
	}

	indexdef.Td_coldefs = append(indexdef.Td_coldefs[:0], coldef_list...)
	indexdef.Td_nk = append(indexdef.Td_nk[:0], coldef_list...)

	for _, coldef_nk := range gtabledef.Base.Td_nk { // for all columns in native key of base table
		indexdef.Td_payload = append(indexdef.Td_payload, coldef_nk) // put it into payload

		if coldef_nk.Exists_in(indexdef.Td_coldefs) == false { // put it into coldefs list, only if not already exists
			indexdef.Td_coldefs = append(indexdef.Td_coldefs, coldef_nk)
		}
	}

	if indexdef.Td_index_type == rsql.TD_INDEX { // for TD_INDEX index, "rowid" is always the last column in the list, because all indexes must be unique
		rsql.Assert(keyname_list[len(keyname_list)-1] == rsql.ROWID)
	}

	rsql.Assert(gtabledef.Indexmap[index_name] == nil)
	gtabledef.Indexmap[index_name] = indexdef

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	dictmgr.dm_SYSTABLES.objid_nextval++

	rsql.Assert(dictmgr.dm_SYSTABLES.gtblid_map[gtabledef.Gtblid] != nil)
	rsql.Assert(dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] != nil)

	dictmgr.dm_SYSTABLES.gtblid_map[gtabledef.Gtblid] = gtabledef // old gtabledef is replaced by the new one
	dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] = gtabledef

	// write to master.db

	dictmgr.DB_insert_Tabledef(indexdef) // write indexdef. indexdef.Td_status == TD_STATUS_BEING_CREATED.

	dictmgr.COMMIT() // first COMMIT
	dictmgr.UNLOCK() // unlock dictmgr, because creation of index file can take time

	//====================== create index table file ======================

	rsql.Must_create_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid), rsql.TBLFILE_PERM) // create index table file
	if rsql_err = wcache.Create_info_page_and_first_zone_allocator(indexdef); rsql_err != nil {
		goto NONCLUSTERED_INDEX_ERROR
	}

	if rsql_err = rsql.Fill_index_with_records(context, table_qname, gtabledef, indexdef); rsql_err != nil { // can take time if the table is large
		goto NONCLUSTERED_INDEX_ERROR
	}

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	wcache.COMMIT() // table pages are written to disk

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE() // lock dictmgr again

	indexdef.Td_status = rsql.TD_STATUS_VALID

	// write to master.db

	dictmgr.DB_change_Tabledef_status(indexdef, rsql.TD_STATUS_VALID)

	dictmgr.COMMIT() //----- final COMMIT

	return nil, nil

	//================================ error occurred when creation of non-clustered index ============================
NONCLUSTERED_INDEX_ERROR:

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE() // lock dictmgr again

	rsql.Assert(gtabledef.Indexmap[index_name] == indexdef)
	delete(gtabledef.Indexmap, index_name)

	//=== delete index file ===

	filebag := wcache.Filebag()

	filebag.Close_file(indexdef)
	rsql.Must_remove_tabledef_file(rsql.Tabledef_path(rsql.DIRECTORY_INDEX, indexdef.Td_dbid, indexdef.Td_schid, indexdef.Td_tblid)) // remove index table file

	// delete tabledef record from master.db

	dictmgr.DB_delete_Tabledef(indexdef.Td_tblid)

	dictmgr.COMMIT() //----- final COMMIT

	return nil, rsql_err
}

// Change_systable implements ALTER TABLE statement.
// Note that we want to allow changing table name even if table is with invalid status.
//
func (dictmgr *Dictionary_manager) Change_systable(context *rsql.Context, table_qname rsql.Object_qname_t, change_table_name string, change_table_name_original string, column_name string, change_column_name string, change_column_name_original string, change_column_nullability Alter_column_nullability_t) *rsql.Error {
	var (
		rsql_err  *rsql.Error
		database  *Sysdatabase
		schema    *Sysschema
		gtabledef *rsql.GTabledef
		coldef    *rsql.Coldef
		ok        bool
	)

	rsql.Assert(context.Ctx_trancount == 0) // not inside a transaction

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	systables := &dictmgr.dm_SYSTABLES

	if database, schema, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(table_qname); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if gtabledef.Get_any_invalid_Tabledef() != nil && change_table_name == "" {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_FORBIDDEN_ON_INVALID_GTABLE, rsql.ERROR_BATCH_ABORT, table_qname.Database_name, table_qname.Schema_name, table_qname.Object_name)
	}

	switch {
	case change_table_name != "":
		if change_table_name == gtabledef.Gtable_name {
			return nil
		}

		if _, ok = systables.name_map[Systable_namekey_t{Gdbid: database.Db_dbid, Gschid: schema.Sch_schid, Gtable_name: change_table_name}]; ok == true {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OBJECT_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, database.Db_name, schema.Sch_name, change_table_name)
		}

	case column_name != "":
		rsql.Assert(column_name != rsql.ROWID)

		if coldef, ok = gtabledef.Colmap[column_name]; ok == false {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_COLUMN_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, column_name)
		}

		switch {
		case change_column_name != "":
			if change_column_name == coldef.Cd_colname {
				return nil
			}

			if _, ok := gtabledef.Colmap[change_column_name]; ok == true {
				return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_COLUMN_NAME_DUPLICATE, rsql.ERROR_BATCH_ABORT, change_column_name)
			}

		case change_column_nullability == ALTER_COLUMN_NULL:
			if tabledef_pk := gtabledef.Primary_key(); tabledef_pk != nil { // if column is in PRIMARY KEY, it must be NOT NULL
				if coldef.Exists_in(tabledef_pk.Td_nk) {
					return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_PK_COLUMN_MUST_BE_NOT_NULL, rsql.ERROR_BATCH_ABORT, column_name)
				}
			}

		case change_column_nullability == ALTER_COLUMN_NOT_NULL:
			if rsql_err := rsql.Check_column_not_null(context, gtabledef, column_name); rsql_err != nil { // check that column doesn't contain any NULL value
				return rsql_err
			}

		default:
			panic("impossible")
		}

	default:
		panic("impossible")
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	switch {
	case change_table_name != "":
		rsql.Assert(systables.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] != nil)
		rsql.Assert(systables.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: change_table_name}] == nil)

		delete(systables.name_map, Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name})
		gtabledef.Gtable_name = change_table_name
		gtabledef.Gtable_name_original = change_table_name_original
		systables.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}] = gtabledef

		args := sqlite3.NamedArgs{
			"$gtbl_gtblid":              gtabledef.Gtblid,
			"$gtbl_table_name":          gtabledef.Gtable_name,
			"$gtbl_table_name_original": gtabledef.Gtable_name_original,
		}
		dictmgr.Execute(DICT_STMT_SYSGTABLEDEFS_UPDATE_NAME, args)

	case column_name != "":
		switch {
		case change_column_name != "":
			rsql.Assert(gtabledef.Colmap[coldef.Cd_colname] != nil)
			rsql.Assert(gtabledef.Colmap[change_column_name] == nil)

			delete(gtabledef.Colmap, coldef.Cd_colname)
			coldef.Cd_colname = change_column_name
			coldef.Cd_colname_original = change_column_name_original
			gtabledef.Colmap[coldef.Cd_colname] = coldef

			args := sqlite3.NamedArgs{
				"$c_tblid":            gtabledef.Base.Td_tblid,
				"$c_base_seqno":       int64(coldef.Cd_base_seqno),
				"$c_colname":          coldef.Cd_colname,
				"$c_colname_original": coldef.Cd_colname_original,
			}
			dictmgr.Execute(DICT_STMT_SYSCOLDEFS_UPDATE_COLNAME, args)

		case change_column_nullability == ALTER_COLUMN_NULL,
			change_column_nullability == ALTER_COLUMN_NOT_NULL:

			not_null_flag := false
			if change_column_nullability == ALTER_COLUMN_NOT_NULL {
				not_null_flag = true
			}

			coldef.Cd_NOT_NULL_flag = not_null_flag

			args := sqlite3.NamedArgs{
				"$c_tblid":         gtabledef.Base.Td_tblid,
				"$c_base_seqno":    int64(coldef.Cd_base_seqno),
				"$c_NOT_NULL_flag": not_null_flag,
			}
			dictmgr.Execute(DICT_STMT_SYSCOLDEFS_UPDATE_NULLABILITY, args)

		default:
			panic("impossible")
		}

	default:
		panic("impossible")
	}

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Change_sysindex implements ALTER INDEX statement.
//
func (dictmgr *Dictionary_manager) Change_sysindex(context *rsql.Context, table_qname rsql.Object_qname_t, index_name string, change_index_name string, change_index_name_original string) *rsql.Error {
	var (
		rsql_err  *rsql.Error
		database  *Sysdatabase
		gtabledef *rsql.GTabledef
		indexdef  *rsql.Tabledef
		ok        bool
	)

	rsql.Assert(table_qname.Database_name != "" && table_qname.Schema_name != "" && table_qname.Object_name != "" && index_name != "" && change_index_name != "" && change_index_name_original != "")
	rsql.Assert(context.Ctx_trancount == 0) // not inside a transaction

	//=================== lock MASTER and BEGIN EXCLUSIVE ===================

	dictmgr.LOCK_and_BEGIN_EXCLUSIVE()
	defer dictmgr.finalize_transaction_and_UNLOCK()

	//=================== checking of modifications. CHECKING MUST NOT CHANGE DICTIONARY INFORMATION BECAUSE ERROR CAN OCCUR ===================

	if change_index_name == index_name {
		return nil
	}

	if database, _, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(table_qname); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := dictmgr.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if gtabledef.Get_any_invalid_Tabledef() != nil {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_FORBIDDEN_ON_INVALID_GTABLE, rsql.ERROR_BATCH_ABORT, table_qname.Database_name, table_qname.Schema_name, table_qname.Object_name)
	}

	if indexdef, ok = gtabledef.Indexmap[index_name]; ok == false {
		if gtabledef.Base.Td_index_name != index_name {
			return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_INDEX_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, table_qname.Database_name, table_qname.Schema_name, table_qname.Object_name, index_name)
		}

		indexdef = gtabledef.Base
	}

	rsql.Assert(indexdef != nil)

	if _, ok = gtabledef.Indexmap[change_index_name]; ok == true {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_INDEX_NAME_FOUND, rsql.ERROR_BATCH_ABORT, change_index_name)
	}

	if gtabledef.Base.Td_index_name == change_index_name {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_INDEX_NAME_FOUND, rsql.ERROR_BATCH_ABORT, change_index_name)
	}

	//=================== apply modifications. NO ERROR MUST OCCUR UNTIL END OF FUNCTION ===================

	indexdef.Td_index_name = change_index_name
	indexdef.Td_index_name_original = change_index_name_original

	if indexdef != gtabledef.Base {
		delete(gtabledef.Indexmap, index_name)
		gtabledef.Indexmap[indexdef.Td_index_name] = indexdef
	}

	// write to master.db

	args := sqlite3.NamedArgs{
		"$tbl_tblid":               indexdef.Td_tblid,
		"$tbl_index_name":          indexdef.Td_index_name,
		"$tbl_index_name_original": indexdef.Td_index_name_original,
	}
	dictmgr.Execute(DICT_STMT_SYSTABLEDEFS_UPDATE_NAME, args)

	dictmgr.COMMIT() //----- final COMMIT

	return nil
}

// Get_gtabledef returns requested tabledef.
//
func (dictmgr *Dictionary_manager) Get_gtabledef(table_qname rsql.Object_qname_t) (*rsql.GTabledef, *rsql.Error) {
	var (
		rsql_err  *rsql.Error
		gtabledef *rsql.GTabledef
	)

	rsql.Assert(table_qname.Database_name != "" && table_qname.Schema_name != "" && table_qname.Object_name != "")

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if _, _, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(table_qname); rsql_err != nil {
		return nil, rsql_err
	}

	return gtabledef, nil
}

// Get_gtblid returns gtblid.
//
func (dictmgr *Dictionary_manager) Get_gtblid(table_qname rsql.Object_qname_t) (int64, *rsql.Error) {
	var (
		rsql_err  *rsql.Error
		gtabledef *rsql.GTabledef
	)

	rsql.Assert(table_qname.Database_name != "" && table_qname.Schema_name != "" && table_qname.Object_name != "")

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	if _, _, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(table_qname); rsql_err != nil {
		return 0, rsql_err
	}

	return gtabledef.Gtblid, nil
}

// Get_gtable_full_name returns qualified name string of the base table.
// It is used for error message, to get the full base table name, when we pass any tabledef as argument.
//
func (dictmgr *Dictionary_manager) Get_gtable_full_name(tabledef *rsql.Tabledef) string {
	var (
		db_name   string
		sch_name  string
		gtbl_name string
		full_name string
	)

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	db_name = dictmgr.database_name(tabledef.Td_dbid)

	sch_name = dictmgr.schema_name(tabledef.Td_dbid, tabledef.Td_schid)

	gtbl_name = dictmgr.gtable_name(tabledef.Td_base_gtblid)

	full_name = fmt.Sprintf("%s.%s.%s", db_name, sch_name, gtbl_name)

	return full_name
}

// Retrieve_list_of_table_names returns a list of all tables in database.
// It is used by DUMP DATABASE, to have the list of tables to lock.
//
func (dictmgr *Dictionary_manager) Retrieve_list_of_table_names(database_name string) ([]rsql.Object_qname_t, *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 40
	)

	var (
		rsql_err *rsql.Error
		list     []rsql.Object_qname_t
		database *Sysdatabase
		dbid     int64
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// get dbid

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil {
		return nil, rsql_err
	}

	dbid = database.Db_dbid

	// make list of table names

	list = make([]rsql.Object_qname_t, 0, LIST_DEFAULT_CAPACITY)

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
		if dbid != -1 && gtabledef.Gdbid != dbid {
			continue
		}

		database_name, schema_name := dictmgr.get_database_and_schema_name(gtabledef.Gdbid, gtabledef.Gschid)

		list = append(list, rsql.Object_qname_t{Database_name: database_name, Schema_name: schema_name, Object_name: gtabledef.Gtable_name, Object_name_original: gtabledef.Gtable_name})
	}

	return list, nil
}

// Retrieve_list_of_gtabledefs returns a list of all tables in database.
// It is used by BACKUP DATABASE.
//
func (dictmgr *Dictionary_manager) Retrieve_list_of_gtabledefs(database_name string) ([]*rsql.GTabledef, *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 40
	)

	var (
		rsql_err *rsql.Error
		list     []*rsql.GTabledef
		database *Sysdatabase
		dbid     int64
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// get dbid

	if database, rsql_err = dictmgr.get_database(database_name); rsql_err != nil {
		return nil, rsql_err
	}

	dbid = database.Db_dbid

	// make list of table names

	list = make([]*rsql.GTabledef, 0, LIST_DEFAULT_CAPACITY)

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
		if dbid != -1 && gtabledef.Gdbid != dbid {
			continue
		}

		list = append(list, gtabledef)
	}

	return list, nil
}

// Check_all_databases_status_mode_access checks status, mode and access of database.
//
// This function checks if status is DB_STATUS_OFFLINE/CORRUPTED, if mode is DB_MODE_READ_ONLY, and if access is DB_ACCESS_RESTRICTED_USER.
// If the user want to make some action incompatible with these attributes, an error is returned.
//
func (dictmgr *Dictionary_manager) Check_all_databases_status_mode_access(context *rsql.Context, registration *lk.Registration) *rsql.Error {
	var (
		database *Sysdatabase
		ok       bool
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// check database status, access

	for _, entry := range registration.Reg_dbreslist {
		if database, ok = dictmgr.dm_SYSDATABASES.name_map[entry.Db_name]; ok == false {
			continue // database doesn't exist. It can be an error, which will be raised during execution, or CREATE DATABASE.
		}

		if database.Db_status != DB_STATUS_ONLINE {
			if entry.Permissions_required&^rsql.PERMISSION_MODIFY_DATABASE != 0 { // ALTER or DROP DATABASE is allowed even if database is offline
				return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_DB_STATUS_FAILURE, rsql.ERROR_BATCH_ABORT, database.Db_name, database.Db_status)
			}
		}

		if database.Db_mode == DB_MODE_READ_ONLY {
			if entry.Permissions_required&^(rsql.PERMISSION_MODIFY_DATABASE|rsql.PERMISSION_SELECT) != 0 { // we want to be able to ALTER or DROP a READ_ONLY database
				return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_DB_MODE_FAILURE, rsql.ERROR_BATCH_ABORT, database.Db_name, database.Db_mode)
			}
		}

		if database.Db_access == DB_ACCESS_RESTRICTED_USER { // only sa or dbo are allowed to access the database
			if rsql_err := MASTER.check_is_SA_or_DBO(context, database.Db_dbid); rsql_err != nil {
				return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_DB_ACCESS_FAILURE_SA_OR_DBO_ONLY, rsql.ERROR_BATCH_ABORT, database.Db_name, database.Db_access)
			}
		}
	}

	return nil
}

// Check_databases_status_not_corrupted returns an error if database status is DB_STATUS_CORRUPTED.
//
func (dictmgr *Dictionary_manager) Check_databases_status_not_corrupted(database_name string) *rsql.Error {
	var (
		database *Sysdatabase
		ok       bool
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// check database status

	if database, ok = dictmgr.dm_SYSDATABASES.name_map[database_name]; ok == false {
		return nil // database doesn't exist. It can be an error, which will be raised during execution.
	}

	if database.Db_status == DB_STATUS_CORRUPTED {
		return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_DB_STATUS_FAILURE, rsql.ERROR_BATCH_ABORT, database.Db_name, database.Db_status)
	}

	return nil
}

// Check_objects_permissions checks if login has all requested permissions on all objects.
//
func (dictmgr *Dictionary_manager) Check_objects_permissions(context *rsql.Context, registration *lk.Registration) *rsql.Error {
	var (
		rsql_err *rsql.Error
		login    *Syslogin
		ok       bool
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// if login is 'sa', it bypasses permissions

	if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[context.Ctx_login_id]; ok == false { // login not found
		return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_MASTER_LOGIN_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, context.Ctx_login_name_for_info)
	}

	if login.Lg_system_flag == LG_SYSFLAG_SA {
		return nil // login is 'sa' and bypasses permissions check
	}

	// check permissions for all objects

	for _, entry := range registration.Reg_objreslist {
		perm := entry.Permissions_required & rsql.TABLE_PERM_SUID_MASK // keep only PERMISSION_SELECT/UPDATE/INSERT/DELETE
		if perm == 0 {                                                 // e.g. for CREATE TABLE
			continue
		}

		object_qname := rsql.Object_qname_t{Database_name: entry.Db_name, Schema_name: entry.Schema_name, Object_name: entry.Object_name, Object_name_original: entry.Object_name}

		if rsql_err = dictmgr.check_permissions_on_object_for_login(login, object_qname, perm); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

// check_permissions_on_object_for_login checks if current login has permissions on a table.
// Returns an error if login has not the required permissions on object.
//
func (dictmgr *Dictionary_manager) check_permissions_on_object_for_login(login *Syslogin, object_qname rsql.Object_qname_t, permissions_required rsql.Permission_t) *rsql.Error {
	var (
		rsql_err  *rsql.Error
		gtabledef *rsql.GTabledef
		database  *Sysdatabase
		user      *Sysprincipal
		palid     int64
		public    *Sysprincipal
		ok        bool
	)

	rsql.Assert(dictmgr.dm_locked == true)

	// get gtabledef

	if database, _, gtabledef, rsql_err = dictmgr.database_schema_gtabledef_from_qname(object_qname); rsql_err != nil {
		return rsql_err
	}

	//=== get specific permissions on object for user ===

	if palid, ok = dictmgr.dm_map_login_dbid_user[Map_login_dbid_key{login_id: login.Lg_login_id, dbid: gtabledef.Gdbid}]; ok == false {
		return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_MASTER_PERMISSION_NOT_GRANTED_FOR_MISSING_USER, rsql.ERROR_BATCH_ABORT, login.Lg_name, object_qname, database.Db_name)
	}

	if user, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[palid]; ok == false {
		log.Panicf("user with palid %d not found", palid)
	}

	if user.Pal_system_flag == PAL_SYSFLAG_USER_DBO {
		return nil // login is owner of dbid database
	}

	granted, denied := dictmgr.get_permissions_on_object_for_principal(user, gtabledef.Gtblid) // get permissions

	//=== get specific permissions on object for 'public' role ===

	if public, ok = dictmgr.dm_SYSPRINCIPALS.name_map[Sysprincipal_namekey_t{Pal_dbid: gtabledef.Gdbid, Pal_name: "public"}]; ok == false {
		log.Panicf("'public' role not found for database %s", dictmgr.database_name(gtabledef.Gdbid))
	}

	granted_public, denied_public := dictmgr.get_permissions_on_object_for_principal(public, gtabledef.Gtblid)

	granted |= granted_public
	denied |= denied_public

	//=== check permissions ====

	if p := permissions_required & denied; p != 0 {
		return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_MASTER_PERMISSION_DENIED, rsql.ERROR_BATCH_ABORT, p.String_long(), object_qname, user.Pal_name)
	}

	if p := permissions_required &^ granted; p != 0 {
		return rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_MASTER_PERMISSION_NOT_GRANTED, rsql.ERROR_BATCH_ABORT, p.String_long(), object_qname, user.Pal_name)
	}

	return nil
}

// get_permissions_on_object_for_principal returns the granted and denied permissions to the principal on object objid.
// The whole membership tree is walked, to check if permission is denied.
//
// Returns granted and denied permissions that the principal has on the object, including permissions of any role he belongs to, recursively.
//
func (dictmgr *Dictionary_manager) get_permissions_on_object_for_principal(principal *Sysprincipal, objid int64) (perms_granted, perms_denied rsql.Permission_t) {
	var (
		ok      bool
		sysperm Syspermission_t
	)

	if sysperm, ok = dictmgr.dm_SYSPERMISSIONS.key_map[Perm_key_t{Objid: objid, Palid: principal.Pal_palid}]; ok == true { // permission entry found
		perms_granted = sysperm.Pm_grant
		perms_denied = sysperm.Pm_deny
	}

	// walk the whole membership tree, in case permission is denied somewhere

	for _, role := range principal.Pal_member_of { // list of roles the principal is a member of
		perms_granted_aux, perms_denied_aux := dictmgr.get_permissions_on_object_for_principal(role, objid)
		perms_granted |= perms_granted_aux
		perms_denied |= perms_denied_aux
	}

	return perms_granted, perms_denied
}
