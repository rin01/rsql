package dict

import (
	"bytes"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"unicode/utf8"

	"golang.org/x/text/collate"

	"rsql"
	"rsql/like"
)

// Quote_escape replaces all single quotes by two single quotes, to escape them as SQL string.
//
func Quote_escape(s string) string {

	return strings.Replace(s, "'", "''", -1)
}

// Show_list_of_strings is used to sort a list of strings by alphabetical order.
//
type Show_list_of_strings struct { // implements Sort interface
	lst  []string
	coll *collate.Collator
}

func New_Show_list_of_strings(lst []string, coll *collate.Collator) Show_list_of_strings {

	return Show_list_of_strings{lst: lst, coll: coll}
}

func (list Show_list_of_strings) Len() int {

	return len(list.lst)
}

func (list Show_list_of_strings) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list Show_list_of_strings) Less(i, j int) bool {
	var (
		res int
	)

	res = list.coll.CompareString(list.lst[i], list.lst[j])

	if res < 0 {
		return true
	}

	return false
}

//=========================================================
//                    SHOW PARAMETER
//=========================================================

type Show_sysparameter_item struct {
	Param_name  string
	Param_id    Server_parameter_t
	Output_text string
}

// Show_list_of_sysparameters is used to sort the list of parameters by alphabetical order.
//
type Show_list_of_sysparameters struct { // implements Sort interface
	lst  []Show_sysparameter_item
	coll *collate.Collator
}

func New_Show_list_of_sysparameters(lst []Show_sysparameter_item, coll *collate.Collator) Show_list_of_sysparameters {

	return Show_list_of_sysparameters{lst: lst, coll: coll}
}

func (list Show_list_of_sysparameters) Len() int {

	return len(list.lst)
}

func (list Show_list_of_sysparameters) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list Show_list_of_sysparameters) Less(i, j int) bool {

	return list.lst[i].Param_id < list.lst[j].Param_id
}

// Retrieve_sorted_list_of_sysparameters returns a list of parameters, sorted by id.
//
// If params.Sh_object_name == "", all parameters are retrieved. Else, only this parameter is retrieved.
//
// If params.Sh_LIKE_flag == true, params.Sh_LIKE_pattern is a LIKE pattern.
//
func (dictmgr *Dictionary_manager) Retrieve_sorted_list_of_sysparameters(context *rsql.Context, params rsql.Show_params) (header string, list []Show_sysparameter_item, rsql_err *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 20
	)

	var (
		coll          *collate.Collator
		likexp_bundle like.Likexp_bundle
		sbuff         []byte
		tmpl_bytebuff bytes.Buffer
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// only SA or DBO of any database is allowed

	if rsql_err := dictmgr.check_is_SA_or_any_DBO(context); rsql_err != nil {
		return "", nil, rsql_err
	}

	// get collator

	if coll, rsql_err = rsql.Get_collator(params.Sh_server_default_collation); rsql_err != nil {
		return "", nil, rsql_err
	}

	// make list of parameters

	if params.Sh_LIKE {
		if err := likexp_bundle.Parse_likexp(params.Sh_LIKE_pattern, like.NO_ESCAPE_RUNE); err != nil {
			return "", nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_STRING, rsql.ERROR_BATCH_ABORT, params.Sh_LIKE_pattern)
		}
	}

	list = make([]Show_sysparameter_item, 0, LIST_DEFAULT_CAPACITY)

	switch params.Sh_output_type {
	case rsql.SH_OUTPUT_NORMAL:
		header = fmt.Sprintf("%-40s  %s\n", "PARAMETER", "VALUE")
		header += fmt.Sprintf("%-40s  %s", "----------------------------------------", "------------------------")

	case rsql.SH_OUTPUT_ID:
		header = fmt.Sprintf("%-40s  %20s  %s\n", "PARAMETER", "ID", "VALUE")
		header += fmt.Sprintf("%-40s  %20s  %s", "----------------------------------------", "--------------------", "------------------------")
	}

	for _, sysparameter := range dictmgr.dm_SYSPARAMETERS.param_array {
		switch params.Sh_LIKE {
		case false:
			if params.Sh_object_name != "" && sysparameter.Param_name != params.Sh_object_name {
				continue
			}

		default: // LIKE
			sbuff = append(sbuff[:0], sysparameter.Param_name...)
			if like.Match(&likexp_bundle, sbuff, coll) == false {
				continue
			}
		}

		output_text := ""
		switch params.Sh_output_type {
		case rsql.SH_OUTPUT_NORMAL:
			quoted_value, quoted_value_stored := quote_sysparameter_value(sysparameter)
			output_text = fmt.Sprintf("%-40s  %s", sysparameter.Param_name, quoted_value)
			if quoted_value != quoted_value_stored {
				output_text = fmt.Sprintf("%-80s (AFTER SERVER RESTART: %s)", output_text, quoted_value_stored)
			}

		case rsql.SH_OUTPUT_ID:
			quoted_value, quoted_value_stored := quote_sysparameter_value(sysparameter)
			output_text = fmt.Sprintf("%-40s  %20d   %s", sysparameter.Param_name, sysparameter.Param_id, quoted_value)
			if quoted_value != quoted_value_stored {
				output_text = fmt.Sprintf("%-100s (AFTER SERVER RESTART: %s)", output_text, quoted_value_stored)
			}

		case rsql.SH_OUTPUT_SQL:
			output_text = sql_creation_script_sysparameter(sysparameter)

		case rsql.SH_OUTPUT_TEMPLATE:
			quoted_value, _ := quote_sysparameter_value(sysparameter)
			arg_map := map[string]interface{}{"parameter_name": sysparameter.Param_name, "parameter_id": sysparameter.Param_id, "parameter_value": quoted_value}

			tmpl_bytebuff.Reset()
			if err := params.Sh_template.Execute(&tmpl_bytebuff, arg_map); err != nil {
				return "", nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EXECUTE_TEMPLATE, rsql.ERROR_BATCH_ABORT, err)
			}
			output_text = tmpl_bytebuff.String()
		}

		list = append(list, Show_sysparameter_item{Param_name: sysparameter.Param_name, Param_id: sysparameter.Param_id, Output_text: output_text})
	}

	// sort the list

	show_list := New_Show_list_of_sysparameters(list, coll)
	sort.Sort(show_list)

	return header, show_list.lst, nil
}

// quote_sysparameter_value encloses parameter value between [...], or '...', or just output the value if it is a number.
//
func quote_sysparameter_value(sysparameter Sysparameter) (current_value string, stored_value string) {

	switch sysparameter.Param_id {
	case PARAM_SERVER_DEFAULT_COLLATION,
		PARAM_SERVER_DEFAULT_DATABASE,
		PARAM_SERVER_DEFAULT_LANGUAGE:
		current_value = "[" + sysparameter.Param_value_string + "]"
		stored_value = current_value
		if sysparameter.Param_value_string_stored != sysparameter.Param_value_string {
			stored_value = "[" + sysparameter.Param_value_string_stored + "]"
		}

	case PARAM_SERVER_SERVERNAME,
		PARAM_SERVER_BULK_DIR,
		PARAM_SERVER_DUMP_DIR:
		current_value = "'" + Quote_escape(sysparameter.Param_value_string) + "'"
		stored_value = current_value
		if sysparameter.Param_value_string_stored != sysparameter.Param_value_string {
			stored_value = "'" + Quote_escape(sysparameter.Param_value_string_stored) + "'"
		}

	case PARAM_SERVER_WORKERS_MAX,
		PARAM_SERVER_GLOBAL_PAGE_CACHE_MEMORY,
		PARAM_SERVER_READ_TIMEOUT,
		PARAM_SERVER_LOCK_TICKER_INTERVAL,
		PARAM_SERVER_LOCK_TIMEOUT_TICKS_COUNT,
		PARAM_SERVER_LOGGING_MAX_SIZE,
		PARAM_SERVER_LOGGING_MAX_COUNT,
		PARAM_SERVER_WCACHE_MEMORY_MAX,
		PARAM_SERVER_WCACHE_MODIF_MAX,
		PARAM_SERVER_BATCH_TEXT_MAX_SIZE,
		PARAM_SERVER_BATCH_INSERTS_MAX_COUNT:
		current_value = strconv.Itoa(int(sysparameter.Param_value_int))
		stored_value = current_value
		if sysparameter.Param_value_int_stored != sysparameter.Param_value_int {
			stored_value = strconv.Itoa(int(sysparameter.Param_value_int_stored))
		}

	case PARAM_SERVER_QUOTED_IDENTIFIER:
		if sysparameter.Param_value_int != 0 {
			current_value = "ON"
		} else {
			current_value = "OFF"
		}

		stored_value = current_value
		if sysparameter.Param_value_int_stored != sysparameter.Param_value_int {
			if sysparameter.Param_value_int_stored != 0 {
				stored_value = "ON"
			} else {
				stored_value = "OFF"
			}
		}

	case PARAM_SERVER_LOGGING_LOCALTIME:
		if sysparameter.Param_value_int != 0 {
			current_value = "TRUE"
		} else {
			current_value = "FALSE"
		}

		stored_value = current_value
		if sysparameter.Param_value_int_stored != sysparameter.Param_value_int {
			if sysparameter.Param_value_int_stored != 0 {
				stored_value = "TRUE"
			} else {
				stored_value = "FALSE"
			}
		}

	default:
		panic("unexpected ALTER SERVER PARAMETER")
	}

	return current_value, stored_value
}

func sql_creation_script_sysparameter(sysparameter Sysparameter) string {

	quoted_value, _ := quote_sysparameter_value(sysparameter)

	if sysparameter.Param_id == PARAM_SERVER_DEFAULT_COLLATION {
		// it is not allowed to change this option.

		return fmt.Sprintf("-- %s = %s", sysparameter.Param_name, quoted_value)
	}

	return fmt.Sprintf("ALTER SERVER PARAMETER SET %s = %s;", sysparameter.Param_name, quoted_value)
}

//=========================================================
//                    SHOW LOGIN
//=========================================================

type Show_login_item struct {
	Login_name        string
	Login_id          int64
	Login_system_flag Lg_sysflag_t
	Output_text       string
}

// Show_list_of_logins is used to sort the list of logins by alphabetical order.
//
type Show_list_of_logins struct { // implements Sort interface
	lst  []Show_login_item
	coll *collate.Collator
}

func New_Show_list_of_logins(lst []Show_login_item, coll *collate.Collator) Show_list_of_logins {

	return Show_list_of_logins{lst: lst, coll: coll}
}

func (list Show_list_of_logins) Len() int {

	return len(list.lst)
}

func (list Show_list_of_logins) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list Show_list_of_logins) Less(i, j int) bool {
	var (
		res int
	)

	// sort by system flag

	if list.lst[i].Login_system_flag != list.lst[j].Login_system_flag {
		return list.lst[i].Login_system_flag > list.lst[j].Login_system_flag // sa is listed first
	}

	// sort by login names

	if list.lst[i].Login_name != list.lst[j].Login_name {
		res = list.coll.CompareString(list.lst[i].Login_name, list.lst[j].Login_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	// login names are same. Compare Login_id.

	return list.lst[i].Login_id < list.lst[j].Login_id
}

// Retrieve_sorted_list_of_logins returns a list of logins, sorted by alphabetical order.
//
// If params.Sh_object_name == "", all logins are retrieved. Else, only this login is retrieved.
//
// If params.Sh_LIKE_flag == true, params.Sh_LIKE_pattern is a LIKE pattern.
//
func (dictmgr *Dictionary_manager) Retrieve_sorted_list_of_logins(context *rsql.Context, params rsql.Show_params) (header string, list []Show_login_item, rsql_err *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 10
	)

	var (
		coll          *collate.Collator
		likexp_bundle like.Likexp_bundle
		sbuff         []byte
		tmpl_bytebuff bytes.Buffer
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// only SA or DBO of any database is allowed

	if rsql_err := dictmgr.check_is_SA_or_any_DBO(context); rsql_err != nil {
		return "", nil, rsql_err
	}

	// get collator

	if coll, rsql_err = rsql.Get_collator(params.Sh_server_default_collation); rsql_err != nil {
		return "", nil, rsql_err
	}

	// make list of logins

	if params.Sh_LIKE {
		if err := likexp_bundle.Parse_likexp(params.Sh_LIKE_pattern, like.NO_ESCAPE_RUNE); err != nil {
			return "", nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_STRING, rsql.ERROR_BATCH_ABORT, params.Sh_LIKE_pattern)
		}
	}

	list = make([]Show_login_item, 0, LIST_DEFAULT_CAPACITY)

	switch params.Sh_output_type {
	case rsql.SH_OUTPUT_NORMAL:
		header = fmt.Sprintf("%-25s  %-25s  %-16s  %s\n", "LOGIN", "DEFAULT DATABASE", "DEFAULT LANGUAGE", "INFO")
		header += fmt.Sprintf("%-25s  %-25s  %-16s  %s", "-------------------------", "-------------------------", "----------------", "----------")

	case rsql.SH_OUTPUT_ID:
		header = fmt.Sprintf("%-25s  %20s  %-25s  %-16s  %s\n", "LOGIN", "ID", "DEFAULT DATABASE", "DEFAULT LANGUAGE", "INFO")
		header += fmt.Sprintf("%-25s  %20s  %-25s  %-16s  %s", "-------------------------", "--------------------", "-------------------------", "----------------", "----------")
	}

	for _, login := range dictmgr.dm_SYSLOGINS.login_id_map {
		switch params.Sh_LIKE {
		case false:
			if params.Sh_object_name != "" && login.Lg_name != params.Sh_object_name {
				continue
			}

		default: // LIKE
			sbuff = append(sbuff[:0], login.Lg_name...)
			if like.Match(&likexp_bundle, sbuff, coll) == false {
				continue
			}
		}

		login_name_decorated := login.Lg_name
		if login.Lg_system_flag != 0 {
			login_name_decorated += " (*)"
		}

		disabled_string := ""
		if login.Lg_disabled_flag {
			disabled_string = "(DISABLED)"
		}

		output_text := ""
		switch params.Sh_output_type {
		case rsql.SH_OUTPUT_NORMAL:
			output_text = rsql.Rtrim(fmt.Sprintf("%-25s  %-25s  %-16s  %s", login_name_decorated, login.Lg_default_database, login.Lg_default_language, disabled_string))

		case rsql.SH_OUTPUT_ID:
			output_text = rsql.Rtrim(fmt.Sprintf("%-25s  %20d  %-25s  %-16s  %s", login_name_decorated, login.Lg_login_id, login.Lg_default_database, login.Lg_default_language, disabled_string))

		case rsql.SH_OUTPUT_SQL:
			output_text = sql_creation_script_login(login)

		case rsql.SH_OUTPUT_TEMPLATE:
			arg_map := map[string]interface{}{"login_name": login.Lg_name, "login_id": login.Lg_login_id, "default_database_name": login.Lg_default_database, "default_language": login.Lg_default_language, "disabled": login.Lg_disabled_flag}

			tmpl_bytebuff.Reset()
			if err := params.Sh_template.Execute(&tmpl_bytebuff, arg_map); err != nil {
				return "", nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EXECUTE_TEMPLATE, rsql.ERROR_BATCH_ABORT, err)
			}
			output_text = tmpl_bytebuff.String()
		}

		list = append(list, Show_login_item{Login_name: login.Lg_name, Login_id: login.Lg_login_id, Login_system_flag: login.Lg_system_flag, Output_text: output_text})
	}

	// sort the list

	show_list := New_Show_list_of_logins(list, coll)
	sort.Sort(show_list)

	return header, show_list.lst, nil
}

func sql_creation_script_login(login *Syslogin) string {
	var s string

	switch login.Lg_system_flag {
	case LG_SYSFLAG_SA:
		s = fmt.Sprintf("ALTER LOGIN [%s] WITH DEFAULT_DATABASE = [%s], DEFAULT_LANGUAGE = [%s];", login.Lg_name, login.Lg_default_database, login.Lg_default_language)

	default:
		s = fmt.Sprintf("CREATE LOGIN [%s] WITH PASSWORD = 'changeme', DEFAULT_DATABASE = [%s], DEFAULT_LANGUAGE = [%s];", login.Lg_name, login.Lg_default_database, login.Lg_default_language)
	}

	if login.Lg_disabled_flag {
		s += fmt.Sprintf("\nALTER LOGIN [%s] DISABLE;", login.Lg_name)
	}

	return s
}

//=========================================================
//                    SHOW DATABASE
//=========================================================

type Show_database_item struct {
	Database_name string
	Dbid          int64
	System_flag   Db_sysflag_t
	Output_text   string
}

// Show_list_of_databases is used to sort the list of databases by alphabetical order.
//
type Show_list_of_databases struct { // implements Sort interface
	lst  []Show_database_item
	coll *collate.Collator
}

func New_Show_list_of_databases(lst []Show_database_item, coll *collate.Collator) Show_list_of_databases {

	return Show_list_of_databases{lst: lst, coll: coll}
}

func (list Show_list_of_databases) Len() int {

	return len(list.lst)
}

func (list Show_list_of_databases) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list Show_list_of_databases) Less(i, j int) bool {
	var (
		res int
	)

	// sort by system flag

	if list.lst[i].System_flag != list.lst[j].System_flag {
		return list.lst[i].System_flag > list.lst[j].System_flag // trashdb is listed first
	}

	// sort by database names

	if list.lst[i].Database_name != list.lst[j].Database_name {
		res = list.coll.CompareString(list.lst[i].Database_name, list.lst[j].Database_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	// database names are same. Compare Dbid.

	return list.lst[i].Dbid < list.lst[j].Dbid
}

// Retrieve_sorted_list_of_databases returns a list of databases, sorted by alphabetical order.
//
// If params.Sh_database_name == "", all databases are retrieved. Else, only this database is retrieved.
//
// If params.Sh_LIKE_flag == true, params.Sh_LIKE_pattern is a LIKE pattern.
//
func (dictmgr *Dictionary_manager) Retrieve_sorted_list_of_databases(context *rsql.Context, params rsql.Show_params) (header string, list []Show_database_item, rsql_err *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 10
	)

	var (
		database      *Sysdatabase
		dbid          int64
		coll          *collate.Collator
		likexp_bundle like.Likexp_bundle
		sbuff         []byte
		tmpl_bytebuff bytes.Buffer
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// only SA or DBO of any database is allowed

	if rsql_err := dictmgr.check_is_SA_or_any_DBO(context); rsql_err != nil {
		return "", nil, rsql_err
	}

	// get collator

	if coll, rsql_err = rsql.Get_collator(params.Sh_server_default_collation); rsql_err != nil {
		return "", nil, rsql_err
	}

	// get dbid

	dbid = -1

	if params.Sh_database_name != "" { // check if database_name exists
		if database, rsql_err = dictmgr.get_database(params.Sh_database_name); rsql_err != nil {
			return "", nil, rsql_err
		}

		dbid = database.Db_dbid
	}

	// make list of databases

	if params.Sh_LIKE {
		if err := likexp_bundle.Parse_likexp(params.Sh_LIKE_pattern, like.NO_ESCAPE_RUNE); err != nil {
			return "", nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_STRING, rsql.ERROR_BATCH_ABORT, params.Sh_LIKE_pattern)
		}
	}

	list = make([]Show_database_item, 0, LIST_DEFAULT_CAPACITY)

	switch params.Sh_output_type {
	case rsql.SH_OUTPUT_NORMAL:
		header = fmt.Sprintf("%-25s  %-25s  %s\n", "DATABASE", "OWNER LOGIN NAME", "INFO")
		header += fmt.Sprintf("%-25s  %-25s  %s", "-------------------------", "-------------------------", "----------")

	case rsql.SH_OUTPUT_ID:
		header = fmt.Sprintf("%-25s  %20s  %-25s  %s\n", "DATABASE", "ID", "OWNER LOGIN", "INFO")
		header += fmt.Sprintf("%-25s  %20s  %-25s  %s", "-------------------------", "--------------------", "-------------------------", "----------")
	}

	for _, database := range dictmgr.dm_SYSDATABASES.dbid_map {
		if dbid != -1 && database.Db_dbid != dbid {
			continue
		}

		switch params.Sh_LIKE {
		case false:
			if params.Sh_database_name != "" && database.Db_name != params.Sh_database_name {
				continue
			}

		default: // LIKE
			sbuff = append(sbuff[:0], database.Db_name...)
			if like.Match(&likexp_bundle, sbuff, coll) == false {
				continue
			}
		}

		owner_login_name := ""
		if owner_login, rsql_err := dictmgr.get_login_from_user_name(database.Db_dbid, "dbo"); rsql_err != nil {
			owner_login_name = "### dbo not found ###"
		} else {
			owner_login_name = owner_login.Lg_name
		}

		info := database.Database_info()
		if info != "" {
			info = "(" + info + ")"
		}

		database_name_decorated := database.Db_name
		if database.Db_system_flag != 0 {
			database_name_decorated += " (*)"
		}

		output_text := ""
		switch params.Sh_output_type {
		case rsql.SH_OUTPUT_NORMAL:
			output_text = fmt.Sprintf("%-25s  %-25s  %s", database_name_decorated, owner_login_name, info)

		case rsql.SH_OUTPUT_ID:
			output_text = fmt.Sprintf("%-25s  %20d  %-25s  %s", database_name_decorated, database.Db_dbid, owner_login_name, info)

		case rsql.SH_OUTPUT_SQL:
			output_text = sql_creation_script_database(database)

		case rsql.SH_OUTPUT_TEMPLATE:
			arg_map := map[string]interface{}{"database_name": database.Db_name, "database_id": database.Db_dbid, "database_owner": owner_login_name, "database_status": database.Db_status.String(), "database_mode": database.Db_mode.String(), "database_access": database.Db_access.String()}

			tmpl_bytebuff.Reset()
			if err := params.Sh_template.Execute(&tmpl_bytebuff, arg_map); err != nil {
				return "", nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EXECUTE_TEMPLATE, rsql.ERROR_BATCH_ABORT, err)
			}
			output_text = tmpl_bytebuff.String()
		}

		list = append(list, Show_database_item{Database_name: database.Db_name, Dbid: database.Db_dbid, System_flag: database.Db_system_flag, Output_text: output_text})
	}

	// sort the list

	show_list := New_Show_list_of_databases(list, coll)
	sort.Sort(show_list)

	return header, show_list.lst, nil
}

func sql_creation_script_database(database *Sysdatabase) string {
	var (
		buff []byte
	)

	buff = make([]byte, 0, 50) // 50 is good enough

	if database.Db_system_flag == 0 {
		s := fmt.Sprintf("CREATE DATABASE [%s];\n", database.Db_name)
		buff = append(buff, s...)
	}

	if database.Db_status != DB_STATUS_ONLINE {
		s := fmt.Sprintf("ALTER DATABASE [%s] SET OFFLINE;\n", database.Db_name)
		buff = append(buff, s...)
	}

	if database.Db_mode != DB_MODE_READ_WRITE {
		s := fmt.Sprintf("ALTER DATABASE [%s] SET %s;\n", database.Db_name, database.Db_mode.String())
		buff = append(buff, s...)
	}

	if database.Db_access != DB_ACCESS_MULTI_USER {
		s := fmt.Sprintf("ALTER DATABASE [%s] SET %s;\n", database.Db_name, database.Db_access.String())
		buff = append(buff, s...)
	}

	res := string(buff)
	if len(res) > 0 && res[len(res)-1] == '\n' {
		res = res[:len(res)-1]
	}

	return res
}

//=========================================================
//                    SHOW USER and ROLE
//=========================================================

type Show_principal_item struct {
	Pal_database_name string
	Pal_name          string
	Pal_palid         int64
	Pal_type          Pal_type_t
	Pal_login_id      int64
	Pal_system_flag   Pal_sysflag_t
	Output_text       string
}

// Show_list_of_principals is used to sort the list of principals by alphabetical order.
//
type Show_list_of_principals struct { // implements Sort interface
	lst  []Show_principal_item
	coll *collate.Collator
}

func New_Show_list_of_principals(lst []Show_principal_item, coll *collate.Collator) Show_list_of_principals {

	return Show_list_of_principals{lst: lst, coll: coll}
}

func (list Show_list_of_principals) Len() int {

	return len(list.lst)
}

func (list Show_list_of_principals) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list Show_list_of_principals) Less(i, j int) bool {
	var (
		res int
	)

	// sort by database name

	if list.lst[i].Pal_database_name != list.lst[j].Pal_database_name {
		res = list.coll.CompareString(list.lst[i].Pal_database_name, list.lst[j].Pal_database_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	// sort by roles and users

	if list.lst[i].Pal_type != list.lst[j].Pal_type {
		return list.lst[i].Pal_type > list.lst[j].Pal_type
	}

	// sort by system flag

	if list.lst[i].Pal_system_flag != list.lst[j].Pal_system_flag {
		return list.lst[i].Pal_system_flag > list.lst[j].Pal_system_flag // public and dbo are listed first
	}

	// sort by principal names

	if list.lst[i].Pal_name != list.lst[j].Pal_name {
		res = list.coll.CompareString(list.lst[i].Pal_name, list.lst[j].Pal_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	return list.lst[i].Pal_palid < list.lst[j].Pal_palid
}

// Retrieve_sorted_list_of_principals returns a list of principals, sorted by alphabetical order.
//
// If params.Sh_option_ALL == true, principals from all databases are retrieved. Else, only principals in this database are retrieved.
//
// If params.Sh_object_name == "", all principals are retrieved. Else, only principals with this name are retrieved.
//
// If params.Sh_LIKE_flag == true, params.Sh_LIKE_pattern is a LIKE pattern.
//
// If requested_pal_type == PAL_TYPE_USER, users and roles are retrieved, as we usually wants to see users and roles in the same listing.
// If requested_pal_type == PAL_TYPE_ROLE, only roles are retrieved.
//
func (dictmgr *Dictionary_manager) Retrieve_sorted_list_of_principals(context *rsql.Context, params rsql.Show_params, requested_pal_type Pal_type_t) (header string, list []Show_principal_item, sql_membership_script string, rsql_err *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 40
	)

	var (
		database         *Sysdatabase
		dbid             int64
		coll             *collate.Collator
		likexp_bundle    like.Likexp_bundle
		sbuff            []byte
		tmpl_bytebuff    bytes.Buffer
		header_login     string
		header_principal string
	)

	if params.Sh_output_type == rsql.SH_OUTPUT_SQL && params.Sh_option_ALL {
		panic("ALL is not allowed with SH_OUTPUT_SQL")
	}

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// only SA or DBO of any database is allowed

	if rsql_err := dictmgr.check_is_SA_or_any_DBO(context); rsql_err != nil {
		return "", nil, "", rsql_err
	}

	// get collator

	if coll, rsql_err = rsql.Get_collator(params.Sh_server_default_collation); rsql_err != nil {
		return "", nil, "", rsql_err
	}

	// get dbid

	dbid = -1

	if params.Sh_database_name != "" { // check if database_name exists
		if database, rsql_err = dictmgr.get_database(params.Sh_database_name); rsql_err != nil {
			return "", nil, "", rsql_err
		}

		dbid = database.Db_dbid
	}

	// make list of principals

	if params.Sh_LIKE {
		if err := likexp_bundle.Parse_likexp(params.Sh_LIKE_pattern, like.NO_ESCAPE_RUNE); err != nil {
			return "", nil, "", rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_STRING, rsql.ERROR_BATCH_ABORT, params.Sh_LIKE_pattern)
		}
	}

	list = make([]Show_principal_item, 0, LIST_DEFAULT_CAPACITY)

	header_login = "LOGIN"
	header_principal = "USER, ROLE"
	if requested_pal_type == PAL_TYPE_ROLE {
		header_login = ""
		header_principal = "ROLE"
	}

	switch params.Sh_output_type {
	case rsql.SH_OUTPUT_NORMAL:
		header = fmt.Sprintf("%-30s  %-25s  %-2s %-25s  %s\n", header_login, "DATABASE", "", header_principal, "MEMBER OF")
		header += fmt.Sprintf("%-30s  %-25s  %-2s %-25s  %s", "------------------------------", "-------------------------", "--", "-------------------------", "-------------------------")

	case rsql.SH_OUTPUT_ID:
		header = fmt.Sprintf("%-30s  %-25s  %-2s %-25s  %20s  %s\n", header_login, "DATABASE", "", header_principal, "ID", "MEMBER OF")
		header += fmt.Sprintf("%-30s  %-25s  %-2s %-25s  %20s  %s", "------------------------------", "-------------------------", "--", "-------------------------", "--------------------", "-------------------------")
	}

	for _, principal := range dictmgr.dm_SYSPRINCIPALS.palid_map {
		if dbid != -1 && principal.Pal_dbid != dbid {
			continue
		}

		if requested_pal_type == PAL_TYPE_ROLE && principal.Pal_type != PAL_TYPE_ROLE {
			continue
		}

		switch params.Sh_LIKE {
		case false:
			if params.Sh_object_name != "" && principal.Pal_name != params.Sh_object_name {
				continue
			}

		default: // LIKE
			sbuff = append(sbuff[:0], principal.Pal_name...)
			if like.Match(&likexp_bundle, sbuff, coll) == false {
				continue
			}
		}

		var principal_db *Sysdatabase
		var login *Syslogin
		var ok bool

		if principal_db, ok = dictmgr.dm_SYSDATABASES.dbid_map[principal.Pal_dbid]; ok == false {
			panic(fmt.Sprintf("database dbid %d not found", principal.Pal_dbid))
		}

		UR := "U:"
		UR_full := "USER"
		if principal.Pal_type == PAL_TYPE_ROLE {
			UR = "R:"
			UR_full = "ROLE"
		}

		principal_name_decorated := principal.Pal_name
		if principal.Pal_system_flag != 0 {
			principal_name_decorated += " (*)"
		}

		login_name_decorated := "(role)"
		if principal.Pal_type == PAL_TYPE_USER {
			if login, ok = dictmgr.dm_SYSLOGINS.login_id_map[principal.Pal_login_id]; ok == false {
				panic(fmt.Sprintf("login login_id %d not found", principal.Pal_login_id))
			} else {
				login_name_decorated = login.Lg_name
				if login.Lg_disabled_flag {
					login_name_decorated += " (DISABLED)"
				}
			}
		}

		member_of := member_of_list(principal, coll)

		var objperm_list []Show_permissions
		if params.Sh_option_PERM {
			objperm_list = dictmgr.get_permission_list_for_principal(principal.Pal_palid)
			list_of_permissions := New_Show_list_of_permissions(objperm_list, coll, SHOW_PERM_SORT_BY_OBJECT)
			sort.Sort(list_of_permissions)
			objperm_list = list_of_permissions.lst
		}

		output_text := ""
		switch params.Sh_output_type {
		case rsql.SH_OUTPUT_NORMAL:
			output_text = fmt.Sprintf("%-30s  %-25s  %-2s %-25s  %s", login_name_decorated, principal_db.Db_name, UR, principal_name_decorated, "("+strings.Join(member_of, ", ")+")")

			if params.Sh_option_PERM {
				for _, objperm := range objperm_list {
					output_text += fmt.Sprintf("\n                                                                %-30s GRANT: %s    DENY: %s", strings.Join([]string{objperm.Database_name, objperm.Schema_name, objperm.Object_name}, "."), Display_perm(objperm.Pm_grant), Display_perm(objperm.Pm_deny))
				}
			}

		case rsql.SH_OUTPUT_ID:
			output_text = fmt.Sprintf("%-30s  %-25s  %-2s %-25s  %20d  %s", login_name_decorated, principal_db.Db_name, UR, principal_name_decorated, principal.Pal_palid, "("+strings.Join(member_of, ", ")+")")

			if params.Sh_option_PERM {
				for _, objperm := range objperm_list {
					output_text += fmt.Sprintf("\n                                                                %-30s GRANT: %s    DENY: %s", strings.Join([]string{objperm.Database_name, objperm.Schema_name, objperm.Object_name}, "."), Display_perm(objperm.Pm_grant), Display_perm(objperm.Pm_deny))
				}
			}

		case rsql.SH_OUTPUT_SQL:
			if requested_pal_type != principal.Pal_type {
				continue
			}

			output_text = sql_creation_script_principals(principal, principal_db, login)

		case rsql.SH_OUTPUT_TEMPLATE:
			var arg_map map[string]interface{}

			if requested_pal_type != principal.Pal_type {
				continue
			}

			if principal.Pal_type == PAL_TYPE_USER {
				arg_map = map[string]interface{}{"database_name": principal_db.Db_name, "UR": UR_full, "user_name": principal.Pal_name, "member_of": member_of, "login_name": login.Lg_name, "disabled": login.Lg_disabled_flag}
			} else {
				arg_map = map[string]interface{}{"database_name": principal_db.Db_name, "UR": UR_full, "role_name": principal.Pal_name, "member_of": member_of}
			}

			tmpl_bytebuff.Reset()
			if err := params.Sh_template.Execute(&tmpl_bytebuff, arg_map); err != nil {
				return "", nil, "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EXECUTE_TEMPLATE, rsql.ERROR_BATCH_ABORT, err)
			}
			output_text = tmpl_bytebuff.String()
		}

		list = append(list, Show_principal_item{Pal_database_name: principal_db.Db_name, Pal_name: principal.Pal_name, Pal_palid: principal.Pal_palid, Pal_type: principal.Pal_type, Pal_login_id: principal.Pal_login_id, Pal_system_flag: principal.Pal_system_flag, Output_text: output_text})
	}

	// sort the list

	show_list := New_Show_list_of_principals(list, coll)
	sort.Sort(show_list)

	// create ALTER ROLE ADD MEMBER script

	if params.Sh_output_type == rsql.SH_OUTPUT_SQL {
		sql_membership_script = dictmgr.sql_creation_script_membership(show_list.lst)
	}

	return header, show_list.lst, sql_membership_script, nil
}

func member_of_list(principal *Sysprincipal, coll *collate.Collator) (roles []string) {

	if len(principal.Pal_member_of) == 0 {
		return nil
	}

	roles = make([]string, 0, 15)

	for _, role := range principal.Pal_member_of {
		roles = append(roles, role.Pal_name)
	}

	show_list := New_Show_list_of_strings(roles, coll)
	sort.Sort(show_list)

	return show_list.lst
}

func sql_creation_script_principals(principal *Sysprincipal, principal_db *Sysdatabase, login *Syslogin) string {

	if principal.Pal_system_flag != 0 {
		if principal.Pal_system_flag == PAL_SYSFLAG_ROLE_PUBLIC { // public always already exists
			return ""
		}

		if principal.Pal_system_flag == PAL_SYSFLAG_USER_DBO {
			if login.Lg_system_flag == LG_SYSFLAG_SA {
				return ""
			}

			return fmt.Sprintf("ALTER AUTHORIZATION ON DATABASE::[%s] TO [%s];", principal_db.Db_name, login.Lg_name)
		}
	}

	if principal.Pal_type == PAL_TYPE_USER {
		return fmt.Sprintf("CREATE USER [%s] FOR LOGIN [%s];", principal.Pal_name, login.Lg_name)
	}

	return fmt.Sprintf("CREATE ROLE [%s];", principal.Pal_name)
}

func (dictmgr *Dictionary_manager) sql_creation_script_membership(list []Show_principal_item) string {
	var (
		buff []byte
	)

	if len(list) == 0 {
		return ""
	}

	buff = make([]byte, 0, 200)

	for _, elem := range list {
		var ok bool
		var principal *Sysprincipal

		if principal, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[elem.Pal_palid]; ok == false {
			panic("impossible")
		}

		if len(principal.Pal_member_of) == 0 {
			continue
		}

		for _, role := range principal.Pal_member_of {
			buff = append(buff, "ALTER ROLE ["...)
			buff = append(buff, role.Pal_name...)
			buff = append(buff, "] ADD MEMBER ["...)
			buff = append(buff, principal.Pal_name...)
			buff = append(buff, "];\n"...)
		}
	}

	res := string(buff)

	if len(res) > 0 && res[len(res)-1] == '\n' {
		res = res[:len(res)-1]
	}

	return res
}

//=========================================================
//                    SHOW TABLE
//=========================================================

type Show_table_item struct {
	Database_name string
	Schema_name   string
	Table_name    string
	Gtblid        int64
	Output_text   string
}

// Show_list_of_tables is used to sort the list of tables by alphabetical order.
//
type Show_list_of_tables struct { // implements Sort interface
	lst  []Show_table_item
	coll *collate.Collator
}

func New_Show_list_of_tables(lst []Show_table_item, coll *collate.Collator) Show_list_of_tables {

	return Show_list_of_tables{lst: lst, coll: coll}
}

func (list Show_list_of_tables) Len() int {

	return len(list.lst)
}

func (list Show_list_of_tables) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list Show_list_of_tables) Less(i, j int) bool {
	var (
		res int
	)

	if list.lst[i].Database_name != list.lst[j].Database_name {
		res = list.coll.CompareString(list.lst[i].Database_name, list.lst[j].Database_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	// database names are same. Check schema names.

	if list.lst[i].Schema_name != list.lst[j].Schema_name {
		res = list.coll.CompareString(list.lst[i].Schema_name, list.lst[j].Schema_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	// schema names are same. Check table names.

	if list.lst[i].Table_name != list.lst[j].Table_name {
		res = list.coll.CompareString(list.lst[i].Table_name, list.lst[j].Table_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	// table names are same. Compare Gtblid.

	return list.lst[i].Gtblid < list.lst[j].Gtblid
}

// Show_list_of_indexdefs is used to sort the list of indexdefs by alphabetical order.
//
type Show_list_of_indexdefs struct { // implements Sort interface
	lst  []*rsql.Tabledef
	coll *collate.Collator
}

func New_Show_list_of_indexdefs(lst []*rsql.Tabledef, coll *collate.Collator) Show_list_of_indexdefs {

	return Show_list_of_indexdefs{lst: lst, coll: coll}
}

func (list Show_list_of_indexdefs) Len() int {

	return len(list.lst)

}

func (list Show_list_of_indexdefs) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]

}

func (list Show_list_of_indexdefs) Less(i, j int) bool {
	var (
		res int
	)

	switch { // CLUSTERED index appear first
	case list.lst[i].Td_cluster_type < list.lst[j].Td_cluster_type:
		return true
	case list.lst[i].Td_cluster_type > list.lst[j].Td_cluster_type:
		return false
	}

	switch { // PRIMARY KEY appear first, then UNIQUE, then INDEX
	case list.lst[i].Td_index_type < list.lst[j].Td_index_type:
		return true
	case list.lst[i].Td_index_type > list.lst[j].Td_index_type:
		return false
	}

	if list.lst[i].Td_index_name != list.lst[j].Td_index_name {
		res = list.coll.CompareString(list.lst[i].Td_index_name, list.lst[j].Td_index_name)

		if res < 0 {
			return true
		}

		if res > 0 {
			return false
		}
	}

	return list.lst[i].Td_tblid < list.lst[j].Td_tblid
}

// Retrieve_sorted_list_of_GTabledefs returns a list of tables, sorted by alphabetical order.
//
// If params.Sh_option_ALL == true, tables from all databases are retrieved. Else, only tables in this database are retrieved.
//
// If params.Sh_object_name == "", all tables are retrieved. Else, only tables with this name are retrieved.
//
// If params.Sh_LIKE_flag == true, params.Sh_LIKE_pattern is a LIKE pattern.
//
func (dictmgr *Dictionary_manager) Retrieve_sorted_list_of_GTabledefs(context *rsql.Context, params rsql.Show_params) (header string, list []Show_table_item, rsql_err *rsql.Error) {
	const (
		LIST_DEFAULT_CAPACITY = 40
	)

	var (
		database      *Sysdatabase
		dbid          int64
		coll          *collate.Collator
		likexp_bundle like.Likexp_bundle
		sbuff         []byte
		tmpl_bytebuff bytes.Buffer
	)

	//=================== lock MASTER ===================

	dictmgr.LOCK()         //----- lock MASTER
	defer dictmgr.UNLOCK() //----- unlock MASTER

	// only SA or DBO of any database is allowed

	if rsql_err := dictmgr.check_is_SA_or_any_DBO(context); rsql_err != nil {
		return "", nil, rsql_err
	}

	// get collator

	if coll, rsql_err = rsql.Get_collator(params.Sh_server_default_collation); rsql_err != nil {
		return "", nil, rsql_err
	}

	// get dbid

	dbid = -1

	if params.Sh_database_name != "" { // check if database_name exists
		if database, rsql_err = dictmgr.get_database(params.Sh_database_name); rsql_err != nil {
			return "", nil, rsql_err
		}

		dbid = database.Db_dbid
	}

	// compile LIKE expression

	if params.Sh_LIKE {
		if err := likexp_bundle.Parse_likexp(params.Sh_LIKE_pattern, like.NO_ESCAPE_RUNE); err != nil {
			return "", nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_STRING, rsql.ERROR_BATCH_ABORT, params.Sh_LIKE_pattern)
		}
	}

	// make list of GTabledefs

	list = make([]Show_table_item, 0, LIST_DEFAULT_CAPACITY)

	switch params.Sh_output_type {
	case rsql.SH_OUTPUT_NORMAL:
		header = fmt.Sprintf("%-50s\n", "TABLE")
		header += fmt.Sprintf("%-50s", "--------------------------------------------------")

	case rsql.SH_OUTPUT_ID:
		header = fmt.Sprintf("%-50s  %s\n", "TABLE", "FILEPATH")
		header += fmt.Sprintf("%-50s  %s", "--------------------------------------------------", "--------------------")
	}

	list_index := make([]*rsql.Tabledef, 0, 6) // 6 is good enough

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
		if dbid != -1 && gtabledef.Gdbid != dbid {
			continue
		}

		switch params.Sh_LIKE {
		case false:
			if params.Sh_object_name != "" {
				switch params.Sh_flag_unique {
				case false:
					if strings.HasPrefix(gtabledef.Gtable_name, params.Sh_object_name) == false {
						continue
					}

				case true:
					if gtabledef.Gtable_name != params.Sh_object_name { // exact name
						continue
					}
				}
			}

		default: // LIKE
			sbuff = append(sbuff[:0], gtabledef.Gtable_name...)
			if like.Match(&likexp_bundle, sbuff, coll) == false {
				continue
			}
		}

		database_name, schema_name := dictmgr.get_database_and_schema_name(gtabledef.Gdbid, gtabledef.Gschid)

		var palperm_list []Show_permissions
		if params.Sh_option_PERM {
			palperm_list = dictmgr.get_permission_list_for_object(gtabledef.Gtblid)
			list_of_permissions := New_Show_list_of_permissions(palperm_list, coll, SHOW_PERM_SORT_BY_PRINCIPAL)
			sort.Sort(list_of_permissions)
			palperm_list = list_of_permissions.lst
		}

		output_text := ""
		switch params.Sh_output_type {
		case rsql.SH_OUTPUT_NORMAL:
			output_text = fmt.Sprintf("%-50s", strings.Join([]string{database_name, schema_name, gtabledef.Gtable_name}, "."))

			if params.Sh_option_PERM {
				for _, palperm := range palperm_list {
					output_text += fmt.Sprintf("\n                          %s: %-20s     GRANT: %s    DENY: %s", palperm.Pal_RU, palperm.Pal_name, Display_perm(palperm.Pm_grant), Display_perm(palperm.Pm_deny))
				}
			}

		case rsql.SH_OUTPUT_ID:
			output_text = fmt.Sprintf("%-50s  %s", strings.Join([]string{database_name, schema_name, gtabledef.Gtable_name}, "."), gtabledef.Base.Td_file_path)

			// print indexes

			list_index = list_index[:0]

			for _, indexdef := range gtabledef.Indexmap {
				list_index = append(list_index, indexdef)
			}

			list_index_sorter := New_Show_list_of_indexdefs(list_index, coll)
			sort.Sort(list_index_sorter)

			ss := make([]string, 0, 6) // 6 is good enough
			for _, indexdef := range list_index {
				ss = append(ss, fmt.Sprintf("       %-50s  %s", indexdef.Td_index_name, indexdef.Td_file_path))
			}
			if len(ss) != 0 {
				output_text += "\n" + strings.Join(ss, "\n")
			}

			// print permissions

			if params.Sh_option_PERM {
				for _, palperm := range palperm_list {
					output_text += fmt.Sprintf("\n                          %s: %-20s     GRANT: %s    DENY: %s", palperm.Pal_RU, palperm.Pal_name, Display_perm(palperm.Pm_grant), Display_perm(palperm.Pm_deny))
				}
			}

		case rsql.SH_OUTPUT_SQL:
			output_text = Sql_creation_script_table(database_name, schema_name, gtabledef, params.Sh_server_default_collation, coll)
			if params.Sh_option_PERM {
				perm_text := sql_creation_script_permissions(database_name, schema_name, gtabledef.Gtable_name, palperm_list)
				if perm_text != "" {
					output_text += "\n" + perm_text
				}
			}

		case rsql.SH_OUTPUT_TEMPLATE:
			arg_map := map[string]interface{}{"database_name": database.Db_name, "schema_name": schema_name, "table_name": gtabledef.Gtable_name, "table_id": gtabledef.Gtblid}

			tmpl_bytebuff.Reset()
			if err := params.Sh_template.Execute(&tmpl_bytebuff, arg_map); err != nil {
				return "", nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EXECUTE_TEMPLATE, rsql.ERROR_BATCH_ABORT, err)
			}
			output_text = tmpl_bytebuff.String()
		}

		list = append(list, Show_table_item{Database_name: database_name, Schema_name: schema_name, Table_name: gtabledef.Gtable_name, Gtblid: gtabledef.Gtblid, Output_text: output_text})
	}

	// sort the list

	show_list := New_Show_list_of_tables(list, coll)
	sort.Sort(show_list)

	return header, show_list.lst, nil
}

// Sql_creation_script_table creates the CREATE TABLE script for the table.
// It is also used by BACKUP DATABASE statement.
//
// If server_default_collation is not empty string, COLLATE clause will not appear for VARCHAR columns having this collation.
// So, to always specify the COLLATE clause for all VARCHAR columns, pass an empty string (or any string that is not a collation) as argument.
//
// Argument coll is the collation to sort the index names.
//
func Sql_creation_script_table(database_name string, schema_name string, gtabledef *rsql.GTabledef, server_default_collation string, coll *collate.Collator) string {
	var (
		buff                  []byte
		s                     string
		colname_width         int
		list_base_and_indexes []*rsql.Tabledef
	)

	const COLNAME_WIDTH_MIN = 20

	for _, coldef := range gtabledef.Base.Td_coldefs {
		if rune_length := utf8.RuneCountInString(coldef.Cd_colname); rune_length > colname_width {
			colname_width = rune_length
		}
	}

	colname_width += 4 // increase column name width a little for nicer display

	if colname_width < COLNAME_WIDTH_MIN {
		colname_width = COLNAME_WIDTH_MIN
	}

	buff = make([]byte, 0, 500) // 500 is good enough

	s = fmt.Sprintf("CREATE TABLE [%s].[%s] (\n", schema_name, gtabledef.Gtable_name)
	buff = append(buff, s...)

	// print columns

	for _, coldef := range gtabledef.Base.Td_coldefs {
		if coldef.Cd_autonum == rsql.CD_AUTONUM_ROWID {
			continue
		}

		display_datatype := rsql.Datatype_shortdef(coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)

		display_string := ""

		switch {
		case coldef.Cd_autonum == rsql.CD_AUTONUM_IDENTITY:
			display_string = fmt.Sprintf("IDENTITY(%d, %d)", gtabledef.Identity_seed, gtabledef.Identity_increment)

		case coldef.Cd_datatype == rsql.DATATYPE_VARCHAR && coldef.Cd_collation != server_default_collation:
			display_string = "COLLATE " + coldef.Cd_collation
		}

		display_NULL := "NULL"
		if coldef.Cd_NOT_NULL_flag == true {
			display_NULL = "NOT NULL"
		}

		if display_string == "" {
			display_string = display_NULL
		} else {
			display_string += " " + display_NULL
		}

		s = fmt.Sprintf("\t[%-*s %-20s %s,\n", colname_width, coldef.Cd_colname+"]", display_datatype, display_string)
		buff = append(buff, s...)
	}

	// print indexes

	list_base_and_indexes = make([]*rsql.Tabledef, 0, len(gtabledef.Indexmap)+1)

	list_base_and_indexes = append(list_base_and_indexes, gtabledef.Base)

	for _, indexdef := range gtabledef.Indexmap {
		list_base_and_indexes = append(list_base_and_indexes, indexdef)
	}

	sort.Sort(New_Show_list_of_indexdefs(list_base_and_indexes, coll))

	for _, indexdef := range list_base_and_indexes {
		has_sysidx_name := false
		if strings.HasPrefix(indexdef.Td_index_name, SYSIDX_PREFIX) {
			// has_sysidx_name = true       // now, we prefer to always print the index name, even if it has been generated automatically
		}

		buff = append(buff, "\t"...)

		if indexdef.Td_cluster_type == rsql.TD_CLUSTERED && indexdef.Td_nk[0].Cd_autonum == rsql.CD_AUTONUM_ROWID { // if native key is ROWID
			buff = append(buff, "-- "...) // comment the index definition line
		}

		switch indexdef.Td_index_type {
		case rsql.TD_UNIQUE:
			if has_sysidx_name {
				buff = append(buff, "UNIQUE "...)
			} else {
				buff = append(buff, "CONSTRAINT ["...)
				buff = append(buff, indexdef.Td_index_name...)
				buff = append(buff, "] UNIQUE "...)
			}

		case rsql.TD_PRIMARY_KEY:
			if has_sysidx_name {
				buff = append(buff, "PRIMARY KEY "...)
			} else {
				buff = append(buff, "CONSTRAINT ["...)
				buff = append(buff, indexdef.Td_index_name...)
				buff = append(buff, "] PRIMARY KEY "...)
			}

		case rsql.TD_INDEX:
			buff = append(buff, "INDEX ["...)
			buff = append(buff, indexdef.Td_index_name...)
			buff = append(buff, "] "...)

		default:
			panic("impossible")
		}

		if indexdef.Td_cluster_type == rsql.TD_CLUSTERED {
			buff = append(buff, "CLUSTERED "...)
		} else {
			buff = append(buff, "NONCLUSTERED "...)
		}

		buff = append(buff, '(')
		for k, coldef := range indexdef.Td_nk {
			if indexdef.Td_index_type == rsql.TD_INDEX && k == len(indexdef.Td_nk)-1 {
				rsql.Assert(coldef.Cd_autonum == rsql.CD_AUTONUM_ROWID)
				continue
			}

			if k != 0 {
				buff = append(buff, ", "...)
			}
			buff = append(buff, '[')
			buff = append(buff, coldef.Cd_colname...)
			buff = append(buff, ']')
		}

		buff = append(buff, "),\n"...)
	}

	if buff[len(buff)-2] == ',' && buff[len(buff)-1] == '\n' {
		buff = buff[:len(buff)-2]
	}

	buff = append(buff, "\n);"...)

	return string(buff)
}

func sql_creation_script_permissions(database_name string, schema_name string, table_name string, palperm_list []Show_permissions) string {
	var (
		buff []byte
	)

	if len(palperm_list) == 0 {
		return ""
	}

	buff = make([]byte, 0, 100) // 100 is good enough

	for _, palperm := range palperm_list {
		if palperm.Pm_grant != 0 {
			buff = append(buff, fmt.Sprintf("GRANT %s ON [%s].[%s] TO [%s];\n", SQL_string(palperm.Pm_grant), schema_name, table_name, palperm.Pal_name)...)
		}

		if palperm.Pm_deny != 0 {
			buff = append(buff, fmt.Sprintf("DENY  %s ON [%s].[%s] TO [%s];\n", SQL_string(palperm.Pm_deny), schema_name, table_name, palperm.Pal_name)...)
		}
	}

	res := string(buff)

	if len(res) > 0 && res[len(res)-1] == '\n' {
		res = res[:len(res)-1]
	}

	return res
}

//=========================================================
//        SHOW PERM USERS    or   SHOW PERM TABLES
//=========================================================

type Show_perm_sort_by_t uint8

const (
	SHOW_PERM_SORT_BY_OBJECT = 1 << iota
	SHOW_PERM_SORT_BY_PRINCIPAL
)

type Show_permissions struct {
	Objid int64
	Palid int64

	Database_name string
	Schema_name   string
	Object_name   string

	Pal_RU   string // principal is role "R" or user "U"
	Pal_name string

	Pm_grant rsql.Permission_t
	Pm_deny  rsql.Permission_t
}

// Show_list_of_permissions is used to sort the list of permissions by alphabetical order.
//
type Show_list_of_permissions struct { // implements Sort interface
	lst     []Show_permissions
	coll    *collate.Collator
	sort_by Show_perm_sort_by_t
}

func New_Show_list_of_permissions(lst []Show_permissions, coll *collate.Collator, sort_by Show_perm_sort_by_t) Show_list_of_permissions {

	return Show_list_of_permissions{lst: lst, coll: coll, sort_by: sort_by}
}

func (list Show_list_of_permissions) Len() int {

	return len(list.lst)

}

func (list Show_list_of_permissions) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]

}

func (list Show_list_of_permissions) Less(i, j int) bool {
	var (
		res int
	)

	switch list.sort_by {
	case SHOW_PERM_SORT_BY_PRINCIPAL:
		// sort by role or user

		if list.lst[i].Pal_RU != list.lst[j].Pal_RU {

			if list.lst[i].Pal_RU < list.lst[j].Pal_RU {
				return true
			}

			return false
		}

		// both are roles or users. Sort by principal name.

		if list.lst[i].Pal_name != list.lst[j].Pal_name {
			res = list.coll.CompareString(list.lst[i].Pal_name, list.lst[j].Pal_name)

			if res < 0 {
				return true
			}

			if res > 0 {
				return false
			}
		}

		return list.lst[i].Palid < list.lst[j].Palid

	case SHOW_PERM_SORT_BY_OBJECT:
		// sort by database

		if list.lst[i].Database_name != list.lst[j].Database_name {
			res = list.coll.CompareString(list.lst[i].Database_name, list.lst[j].Database_name)

			if res < 0 {
				return true
			}

			if res > 0 {
				return false
			}
		}

		// database names are same. Check schema names.

		if list.lst[i].Schema_name != list.lst[j].Schema_name {
			res = list.coll.CompareString(list.lst[i].Schema_name, list.lst[j].Schema_name)

			if res < 0 {
				return true
			}

			if res > 0 {
				return false
			}
		}

		// schema names are same. Check object names.

		if list.lst[i].Object_name != list.lst[j].Object_name {
			res = list.coll.CompareString(list.lst[i].Object_name, list.lst[j].Object_name)

			if res < 0 {
				return true
			}

			if res > 0 {
				return false
			}
		}

		// object names are same. Compare Objid.

		return list.lst[i].Objid < list.lst[j].Objid

	default:
		panic("impossible")
	}
}

func Display_perm(perm rsql.Permission_t) string {
	var (
		buff [4]byte
	)

	for i, _ := range buff {
		buff[i] = '-'
	}

	if perm&rsql.PERMISSION_SELECT != 0 {
		buff[0] = 'S'
	}
	if perm&rsql.PERMISSION_UPDATE != 0 {
		buff[1] = 'U'
	}
	if perm&rsql.PERMISSION_INSERT != 0 {
		buff[2] = 'I'
	}
	if perm&rsql.PERMISSION_DELETE != 0 {
		buff[3] = 'D'
	}

	return string(buff[:])
}

func SQL_string(perm rsql.Permission_t) string {

	if perm == 0 {
		return ""
	}

	res := make([]string, 0, 4)

	if perm&rsql.PERMISSION_SELECT != 0 {
		res = append(res, "SELECT")
	}

	if perm&rsql.PERMISSION_INSERT != 0 {
		res = append(res, "INSERT")
	}

	if perm&rsql.PERMISSION_UPDATE != 0 {
		res = append(res, "UPDATE")
	}

	if perm&rsql.PERMISSION_DELETE != 0 {
		res = append(res, "DELETE")
	}

	return strings.Join(res, ", ")
}

func (dictmgr *Dictionary_manager) get_permission_list_for_object(objid int64) (palperm_list []Show_permissions) {

	for key, perm := range dictmgr.dm_SYSPERMISSIONS.key_map {
		if key.Objid != objid {
			continue
		}

		var principal *Sysprincipal
		var ok bool
		if principal, ok = dictmgr.dm_SYSPRINCIPALS.palid_map[key.Palid]; ok == false {
			panic(fmt.Sprintf("principal palid %d not found", key.Palid))
		}

		pal_RU := "U"
		if principal.Pal_type == PAL_TYPE_ROLE {
			pal_RU = "R"
		}

		palperm_list = append(palperm_list, Show_permissions{Pal_RU: pal_RU, Pal_name: principal.Pal_name, Pm_grant: perm.Pm_grant, Pm_deny: perm.Pm_deny})
	}

	return palperm_list
}

func (dictmgr *Dictionary_manager) get_permission_list_for_principal(palid int64) (objperm_list []Show_permissions) {

	for key, perm := range dictmgr.dm_SYSPERMISSIONS.key_map {
		if key.Palid != palid {
			continue
		}

		var gtabledef *rsql.GTabledef
		var ok bool
		if gtabledef, ok = dictmgr.dm_SYSTABLES.gtblid_map[key.Objid]; ok == false {
			panic("impossible")
		}

		database_name, schema_name := dictmgr.get_database_and_schema_name(gtabledef.Gdbid, gtabledef.Gschid)

		objperm_list = append(objperm_list, Show_permissions{Database_name: database_name, Schema_name: schema_name, Object_name: gtabledef.Gtable_name, Pm_grant: perm.Pm_grant, Pm_deny: perm.Pm_deny})
	}

	return objperm_list
}
