package dict

import (
	"fmt"
	"io"
	"log"
	"os"

	"github.com/mxk/go-sqlite/sqlite3"

	"rsql"
	"rsql/hashing"
	"rsql/lang"
)

// Install_master_db initializes the current directory, so that it can be used by the database server.
// It creates master.db in the current directory, and directories needed by the database server.
// If the directory is not empty, the function calls log.Fatal().
// If server_listener_address is empty string, it will be set to a default value.
//
func Install_master_db(instance_name string, server_default_collation string, server_default_language string, server_listener_address string) {
	var (
		err                                 error
		rsql_err                            *rsql.Error
		hostname                            string
		servername                          string
		server_default_collation_normalized string
		server_default_language_normalized  string
		current_dir                         *os.File
		fnames                              []string
		master_conn                         *sqlite3.Conn
		hashed_password_base64              string
		password                            string
	)

	// change dir to root directory

	if current_dir, err = os.Open("."); err != nil {
		log.Fatalf("%s", err)
	}

	defer current_dir.Close()

	// check that current directory is empty

	if fnames, err = current_dir.Readdirnames(1); err != nil && err != io.EOF {
		log.Fatalf("%s", err)
	}

	if len(fnames) != 0 {
		log.Fatal("ERROR: Cannot create master.db, because directory is not empty.")
	}

	// normalize default collation and language

	if server_default_collation_normalized, rsql_err = rsql.Normalize_collation(server_default_collation); rsql_err != nil {
		log.Fatalf("server_default_collation %s is not supported or invalid. It must be e.g. en_ci_ai, fr_ci_ai, french_ci_ai. fr_CA_ci_ai, etc.", server_default_collation)
	}

	if server_default_language_normalized, rsql_err = lang.Normalize_language(server_default_language); rsql_err != nil {
		log.Fatalf("server_default_language %s is not supported or invalid. It must be e.g. en_us, en-us, fr-ch, etc.", server_default_language)
	}

	log.Printf("server_default_collation: %s", server_default_collation_normalized)
	log.Printf("server_default_language : %s", server_default_language_normalized)

	// create and open master db

	if master_conn, err = sqlite3.Open(rsql.MASTER_PATH_RWC); err != nil { // open master.db in mode: read, write, create if not exists
		log.Panicf("%s", err)
	}

	// run sql script

	if err := master_conn.Exec(sql_install_script); err != nil { // creates all system tables
		log.Panicf("%s", err)
	}

	// close master db

	master_conn.Close()

	log.Println("master.db is being created.")

	// create 'data', 'index', 'ĵournals', 'logging', 'bulkdir' and 'dumpdir' directories

	rsql.Must_create_directory(rsql.DIRECTORY_DATA, rsql.DIR_PERM)        // create 'data' directory
	rsql.Must_create_directory(rsql.DIRECTORY_INDEX, rsql.DIR_PERM)       // create 'index' directory
	rsql.Must_create_directory(rsql.DIRECTORY_JOURNALS, rsql.DIR_PERM)    // create 'ĵournals' directory
	rsql.Must_create_directory(rsql.DIRECTORY_LOGGING, rsql.DIR_PERM)     // create 'logging' directory
	rsql.Must_create_directory(rsql.DIRECTORY_BULKDIR, rsql.BULKDIR_PERM) // create 'bulkdir' directory
	rsql.Must_create_directory(rsql.DIRECTORY_DUMPDIR, rsql.DUMPDIR_PERM) // create 'dumpdir' directory
	rsql.Must_create_directory(rsql.DIRECTORY_TEMPDIR, rsql.TEMPDIR_PERM) // create 'tempdir' directory

	log.Println("'data', 'index', 'ĵournals', 'logging', 'bulkdir', 'dumpdir' and 'tempdir' directories have been created.")

	// create 'version' file

	rsql.Must_create_ordinary_file(rsql.VERSION_FILENAME, fmt.Sprintf("%s\n", rsql.VERSION))

	// create 'listener_address.txt' file

	if server_listener_address == "" { // if listener address has not been specified
		server_listener_address = "localhost:7777"
	}
	rsql.Must_create_ordinary_file(rsql.LISTENER_ADDRESS_FILENAME, fmt.Sprintf("%s\n", server_listener_address))

	// initialize empty dict in memory

	MASTER.Open()

	MASTER.Load_dict_from_master_db()

	context_SA := rsql.New_context(SA_LOGIN_ID, "sa")

	// update server servername, default collation and language

	if hostname, err = os.Hostname(); err != nil {
		hostname = "unknown_hostname"
	}

	servername = hostname + "/" + instance_name

	if rsql_err = MASTER.Update_sysparameter_value_string(context_SA, PARAM_SERVER_SERVERNAME, servername, PARAM_UPDATE_IMMEDIATE); rsql_err != nil {
		log.Fatal(rsql_err)
	}

	if rsql_err = MASTER.Update_sysparameter_value_string(context_SA, PARAM_SERVER_DEFAULT_COLLATION, server_default_collation_normalized, PARAM_UPDATE_IMMEDIATE); rsql_err != nil {
		log.Fatal(rsql_err)
	}

	if rsql_err = MASTER.Update_sysparameter_value_string(context_SA, PARAM_SERVER_DEFAULT_LANGUAGE, server_default_language_normalized, PARAM_UPDATE_IMMEDIATE); rsql_err != nil {
		log.Fatal(rsql_err)
	}

	// modify 'sa' login with password 'changeme' and default language

	password = "changeme"

	if hashed_password_base64, err = hashing.Generate_hash_from_password(password); err != nil {
		log.Fatal(err)
	}

	login_params := Login_params{Lgp_password_hash: hashed_password_base64, Lgp_default_language: server_default_language_normalized}

	if rsql_err = MASTER.Change_syslogin(context_SA, "sa", login_params); rsql_err != nil {
		log.Fatal(rsql_err)
	}

	// create 'trashdb' database

	if rsql_err = MASTER.Add_sysdatabase(context_SA, rsql.TRASHDB); rsql_err != nil {
		log.Fatal(rsql_err)
	}

	if rsql_err = MASTER.Change_trashdb_to_read_only_at_install(context_SA); rsql_err != nil {
		log.Fatal(rsql_err)
	}

	MASTER.Close()

	log.Println("The instance directory has been successfully initialized.")
}

const sql_install_script string = `

/*========== sysparameters ==========*/

CREATE TABLE sysparameters
(
  param_id             int           not null,   -- primary key
  param_name           varchar       not null,   -- unique index
  param_value_int      int           not null,   -- 0 by default
  param_value_string   varchar       not null,   -- 'none' by default

  constraint sysparameters_pk primary key (param_id)
);

CREATE UNIQUE INDEX sysparameters_unique_name ON sysparameters(param_name);


INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(0,  'server_default_collation',                  0, 'en_ci_ai'      );   -- e.g. en_cs_as, latin1_general_ci_ai, etc
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(1,  'server_servername',                         0, 'RSQL_SERVER'   );   -- any string of your choice
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(2,  'server_workers_max',                       64, ''              );
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(3,  'server_global_page_cache_memory',         100, ''              );   -- in MB
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(4,  'server_default_database',                   0, 'trashdb'       );
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(5,  'server_default_language',                   0, 'en-us'         );   -- e.g. en-us, fr-ch, etc
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(6,  'server_quoted_identifier',                  1, ''              );
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(7,  'server_bulk_dir',                           0, 'bulkdir'       );
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(8,  'server_dump_dir',                           0, 'dumpdir'       );
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(9,  'server_read_timeout',                      30, ''              );   -- read timeout, in seconds
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(10, 'server_lock_ticker_interval',            1000, ''              );   -- lock ticker interval is n milliseconds
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(11, 'server_lock_timeout_ticks_count',          30, ''              );   -- worker waits for locks for n * server_lock_ticker_interval before timing out
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(12, 'server_logging_max_size',                   5, ''              );   -- logging file max size in MB before rotating (0 is infinite)
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(13, 'server_logging_max_count',                 10, ''              );   -- number of rotated logging files to keep (if 0, keep all)
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(14, 'server_logging_localtime',                  1, ''              );   -- if 0, date time appended to logging file name is UTC. Else, local time.
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(15, 'server_wcache_memory_max',                40, ''              );   -- in MB. Default wcache size max is 40 MB
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(16, 'server_wcache_modif_max',            1000000, ''              );   -- in MB. Default max modification size is 1TB
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(17, 'server_batch_text_max_size',         1000000, ''              );   -- in bytes.
INSERT INTO sysparameters (param_id, param_name, param_value_int, param_value_string) VALUES(18, 'server_batch_inserts_max_count',        1000, ''              );   -- in bytes.


/*========== syslogins ==========*/

-- login_default_database is a string, so that if you change 'mydb' to 'mydb_old', and create a new 'mydb', the default database for the login keeps connecting to 'mydb'.
-- If this is not desired, you must change the default_database of the logins.

CREATE TABLE syslogins
(
  login_id                        int              not null,    -- primary key. 1000 is 'sa' (see constant SA_LOGIN_ID)
  login_name                      varchar          not null,    -- unique index
  login_password_hash             varchar          not null,
  login_default_database          varchar          not null,    -- default is trashdb
  login_default_language          varchar          not null,    -- date formatting locale. If not specified during login creation, defaults to server_default_language parameter. E.g. fr-ch
  login_disabled_flag             int              not null,    -- 0 enabled, 1 disabled
  login_create_date               varchar          not null,
  login_system_flag               int              not null,    -- 1 for 'sa', else 0

  constraint syslogins_pk primary key (login_id)
);

CREATE UNIQUE INDEX syslogins_unique_name ON syslogins(login_name);

INSERT INTO syslogins(login_id, login_name, login_password_hash, login_default_database, login_default_language, login_disabled_flag, login_create_date, login_system_flag)
        VALUES(1000, 'sa', '-----(invalid password)------', 'trashdb', 'en-us', 0, strftime('%Y-%m-%d %H:%M:%S','now','localtime'), 1);


/*========== create database ==========*/

-- 'trashdb' dbid is TRASHDB_DBID (100), 'dbo' schid is SCHID_DBO (0), directory is d100/s0
-- only sa can create a database

CREATE TABLE sysdatabases
(
  db_dbid               int                not null,          -- trashdb is TRASHDB_DBID
  db_name               varchar            not null,
  db_create_date        varchar            not null,
  db_status             int                not null,          -- being created, being dropped, recovering, online, offline, corrupted, restoring
  db_status_text        varchar            not null,
  db_mode               int                not null,          -- read write, read only
  db_mode_text          varchar            not null,
  db_access             int                not null,          -- restricted user, multi user
  db_access_text        varchar            not null,
  db_system_flag        int                not null,          -- 1 for trashdb, else 0

  constraint sysdatabases_pk primary key (db_dbid)
);

CREATE UNIQUE INDEX sysdatabases_unique_name ON sysdatabases(db_name);


CREATE TABLE sysschemas   -- create a new 'dbo' schema for each new database
(
  sch_dbid     int         not null,
  sch_schid    int         not null,
  sch_name     varchar     not null,

  constraint sysschemas_pk primary key (sch_dbid, sch_schid)
);

CREATE UNIQUE INDEX sysschemas_unique_dbid_name ON sysschemas(sch_dbid, sch_name);


/*========== create user, create role ==========*/

-- 'dbo' for 'trashdb' is PALID_START (30000)
-- only sa and dbo can create principals
-- a new user with pal_name 'dbo' for login 'sa' is created automatically for each database.
-- in each database, two users cannot map to the same login. Also, two logins cannot map to the same user.

CREATE TABLE sysprincipals
(
  pal_palid                 int                  not null,    -- primary key
  pal_dbid                  int                  not null,
  pal_name                  varchar              not null,    -- unique pal_name in each pal_dbid
  pal_type                  int                  not null,    -- U user, R role
  pal_create_date           varchar              not null,
  pal_default_schema_name   varchar                  null,    -- for U, always 'dbo'. For R, NULL.
  pal_login_id              int                      null,    -- unique pal_login_id in each pal_dbid. For U, points to a syslogin entry. For R, NULL.
  pal_system_flag           int                  not null,    -- 1 for 'dbo', 2 for 'public', else 0

  constraint sysprincipals_pk primary key (pal_palid)
);

CREATE UNIQUE INDEX sysprincipals_unique_name_in_db     ON sysprincipals(pal_dbid, pal_name);
CREATE UNIQUE INDEX sysprincipals_unique_login_id_in_db ON sysprincipals(pal_dbid, pal_login_id);  -- for R, sqlite3 unique index allows many NULL pal_login_id for each pal_dbid.
                                                                                                       --  (note: SQL Server 'unique index' would not allow that, as it considers each NULL to be the same value.
                                                                                                       --         Fortunately, sqlite3 considers many NULLs to have different values, which is what we want here.)

CREATE TABLE sysrolemembers
(
  rm_member_palid       int not null,    -- palid U or R
  rm_role_palid         int not null,    -- palid R

  constraint sysrolemembers_pk primary key (rm_member_palid, rm_role_palid)
);



/*========== grant xxx on xxx to xxx ==========*/

CREATE TABLE syspermissions
(
  p_objid          int not null,     -- PK primary key (p_objid, p_palid)
  p_palid          int not null,     -- PK see above. It is a principal palid (user or role).
  p_grant          int not null,     -- 1 SELECT, 2 UPDATE, 4 INSERT, 8 DELETE. Permissions are XORed.
  p_deny           int not null,     -- 1 SELECT, 2 UPDATE, 4 INSERT, 8 DELETE. Permissions are XORed.

  constraint syspermissions_pk primary key (p_objid, p_palid)
);



/*========== systabledefs and syscoldefs ==========*/

-- GTabledef represents a base table and all associated indexes.
--     It must contain exactly one base table Tabledef, and zero or more index Tabledefs.

CREATE TABLE sysgtabledefs
(
	gtbl_gtblid                int             not null, -- primary key, same as base table tbl_tblid

	gtbl_dbid                  int             not null,
	gtbl_schid                 int             not null,
	gtbl_table_name            varchar         not null,
	gtbl_table_name_original   varchar         not null,

	gtbl_identity_seed         int             not null,
	gtbl_identity_increment    int             not null,

	gtbl_create_date           varchar         not null,

	constraint sysgtabledefs_pk primary key (gtbl_gtblid)
);

CREATE UNIQUE INDEX sysgtabledefs_unique_table_name ON sysgtabledefs (gtbl_dbid, gtbl_schid, gtbl_table_name);

CREATE TABLE systabledefs
(
	tbl_tblid                  int             not null, -- primary key

	tbl_dbid                   int             not null,
	tbl_schid                  int             not null,
	tbl_base_gtblid            int             not null, -- for base table, same as tbl_tblid. For index table, different than tbl_tblid.
	-- tbl_file_path           varchar         not null, -- Note: not used
	tbl_index_name             varchar         not null,
	tbl_index_name_original    varchar         not null,

	-- tbl_durability          varchar         not null, -- Note: not used, as systables only contains permanent tables.
	tbl_type                   varchar         not null, -- BASE: base table, INDEX: index table
	tbl_index_type             varchar         not null, -- UNIQ: unique, PK: primary key, IDX: index
	-- tbl_cluster_type        varchar         not null, -- Note: not used, as base table are always clustered and index tables are always nonclustered.

	tbl_coldefs                varchar             null, -- list of column seqnos, separated by comma. Only used by index tables, not by base tables.
	tbl_nk                     varchar         not null, -- list of column seqnos, separated by comma.
	tbl_payload                varchar             null, -- list of column seqnos, separated by comma. Only used by index tables, not by base tables.

	tbl_status                 int             not null,
	tbl_status_text            varchar         not null,

        constraint systabledefs_pk primary key (tbl_tblid)
);

CREATE UNIQUE INDEX systabledefs_unique_index_name ON systabledefs (tbl_dbid, tbl_schid, tbl_base_gtblid, tbl_index_name);

CREATE TABLE syscoldefs
(
	c_tblid              int     not null, -- primary key
	c_base_seqno         int     not null, -- primary key. Sequential column number 0, 1, 2 etc.

	c_colname            varchar not null,
	c_colname_original   varchar not null,

	c_datatype           int     not null,
	c_precision          int     not null,
	c_scale              int     not null,
	c_fixlen_flag        int     not null,
	c_collation_strength int     not null,
	c_collation          varchar     null,

	c_autonum            int     not null,
	c_NOT_NULL_flag      int     not null,

	c_default_val        varchar     null,
	c_default_type       int         null,

        constraint syscoldefs_pk primary key (c_tblid, c_base_seqno)
);

CREATE UNIQUE INDEX syscoldefs_unique_colname ON syscoldefs (c_tblid, c_colname);

`
