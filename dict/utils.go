package dict

import (
	"fmt"
	"log"
	"sort"
	"sync"

	"rsql"
	"rsql/cache"

	"github.com/mxk/go-sqlite/sqlite3" // http://godoc.org/github.com/mxk/go-sqlite/sqlite3
)

/* ============================================================== */
/*                  dictionary MASTER static variable             */
/* ============================================================== */

var MASTER Dictionary_manager

/* ============================================================== */
/*                  dictionary utility functions                  */
/* ============================================================== */

type Dict_tran_status_t uint8

const (
	DICT_TRAN_STATUS_NONE               Dict_tran_status_t = iota // set by COMMIT sql statement
	DICT_TRAN_STATUS_INSIDE_TRANSACTION                           // set by BEGIN sql statement
)

type Dictionary_manager struct {
	dm_lock   sync.Mutex // only on goroutine at-a-time can access Dictionary_manager
	dm_locked bool       // only used rsql.Assert() by some functions, to ensure that dictionary is locked

	dm_SYSPARAMETERS  Sysparameters_collection
	dm_SYSLOGINS      Syslogins_collection
	dm_SYSDATABASES   Sysdatabases_collection
	dm_SYSSCHEMAS     Sysschemas_collection
	dm_SYSTABLES      Systables_collection // contains all *rsql.GTabledefs
	dm_SYSPRINCIPALS  Sysprincipals_collection
	dm_SYSPERMISSIONS Syspermissions_collection

	dm_map_login_dbid_user Map_login_dbid_user // helper map, to quickly lookup the user corresponding to a login in given database

	dm_master *sqlite3.Conn // points to the master.db sqlite database

	dm_stmt_array  []*sqlite3.Stmt // list of prepared statements used by the dictionary
	dm_stmt_in_use Stmt_kind_t     // only one statment can be in use at any time
	dm_tran_status Dict_tran_status_t
}

// Check_dictionary_maps is only used for debugging purpose. It should not be used in production code.
//
func (dictmgr *Dictionary_manager) Check_dictionary_maps() {
	var (
		principal_user_count int // number of principals with type == PAL_TYPE_USER
	)

	dictmgr.LOCK()
	defer dictmgr.UNLOCK()

	// check that login maps are both valid

	rsql.Assert(len(dictmgr.dm_SYSLOGINS.login_id_map) == len(dictmgr.dm_SYSLOGINS.name_map))

	for _, login := range dictmgr.dm_SYSLOGINS.login_id_map {
		if login_2, ok := dictmgr.dm_SYSLOGINS.name_map[login.Lg_name]; ok == false {
			log.Panicf("login %v with no entry in name map found.", login)
			rsql.Assert(login == login_2)
		}
	}

	// check that principal maps are both valid

	rsql.Assert(len(dictmgr.dm_SYSPRINCIPALS.palid_map) == len(dictmgr.dm_SYSPRINCIPALS.name_map))

	for _, principal := range dictmgr.dm_SYSPRINCIPALS.palid_map {
		if principal_2, ok := dictmgr.dm_SYSPRINCIPALS.name_map[Sysprincipal_namekey_t{Pal_dbid: principal.Pal_dbid, Pal_name: principal.Pal_name}]; ok == false {
			log.Panicf("principal %v with no entry in name map found.", principal)
			rsql.Assert(principal == principal_2)
		}

		if principal.Pal_type == PAL_TYPE_USER {
			principal_user_count++
		}
	}

	// check that database maps are both valid

	rsql.Assert(len(dictmgr.dm_SYSDATABASES.dbid_map) == len(dictmgr.dm_SYSDATABASES.name_map))

	for _, database := range dictmgr.dm_SYSDATABASES.dbid_map {
		if database_2, ok := dictmgr.dm_SYSDATABASES.name_map[database.Db_name]; ok == false {
			log.Panicf("database %v with no entry in name map found.", database)
			rsql.Assert(database == database_2)
		}
	}

	// check that schema maps are both valid

	rsql.Assert(len(dictmgr.dm_SYSSCHEMAS.schid_map) == len(dictmgr.dm_SYSSCHEMAS.name_map))

	for _, schema := range dictmgr.dm_SYSSCHEMAS.schid_map {
		if schema_2, ok := dictmgr.dm_SYSSCHEMAS.name_map[Sysschema_namekey_t{Sch_dbid: schema.Sch_dbid, Sch_name: schema.Sch_name}]; ok == false {
			log.Panicf("schema %v with no entry in name map found.", schema)
			rsql.Assert(schema == schema_2)
		}
	}

	// check that table maps are both valid

	rsql.Assert(len(dictmgr.dm_SYSTABLES.gtblid_map) == len(dictmgr.dm_SYSTABLES.name_map))

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
		if gtabledef_2, ok := dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: gtabledef.Gdbid, Gschid: gtabledef.Gschid, Gtable_name: gtabledef.Gtable_name}]; ok == false {
			log.Panicf("gtabledef %v with no entry in name map found.", gtabledef)
			rsql.Assert(gtabledef == gtabledef_2)
		}
	}

	// check that all permissions have a valid objid and palid

	for key, _ := range dictmgr.dm_SYSPERMISSIONS.key_map {
		if _, ok := dictmgr.dm_SYSPRINCIPALS.palid_map[key.Palid]; ok == false {
			log.Panicf("permission %v with invalid principal found.", key)
		}

		if _, ok := dictmgr.dm_SYSTABLES.gtblid_map[key.Objid]; ok == false {
			log.Panicf("permission %v with invalid object found.", key)
		}
	}

	// check that all principals have valid database, login, and roles

	for _, principal := range dictmgr.dm_SYSPRINCIPALS.palid_map {
		if _, ok := dictmgr.dm_SYSDATABASES.dbid_map[principal.Pal_dbid]; ok == false {
			log.Panicf("principal %v with invalid database found.", principal)
		}

		if principal.Pal_type == PAL_TYPE_USER {
			if _, ok := dictmgr.dm_SYSLOGINS.login_id_map[principal.Pal_login_id]; ok == false {
				log.Panicf("user %v with invalid login found.", principal)
			}
		}

		for _, role := range principal.Pal_member_of {
			if _, ok := dictmgr.dm_SYSPRINCIPALS.palid_map[role.Pal_palid]; ok == false {
				log.Panicf("principal %v with invalid role found %v.", principal, principal.Pal_member_of)
			}

			rsql.Assert(role.Pal_dbid == principal.Pal_dbid)
		}
	}

	// check that all gtabledefs have valid database and schema

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
		dbid := gtabledef.Gdbid
		schid := gtabledef.Gschid

		tabledef := gtabledef.Base // check base table

		if _, ok := dictmgr.dm_SYSDATABASES.dbid_map[dbid]; ok == false {
			log.Panicf("tabledef %v with invalid database found.", tabledef)
		}

		if _, ok := dictmgr.dm_SYSSCHEMAS.schid_map[Sysschema_schidkey_t{Sch_dbid: dbid, Sch_schid: schid}]; ok == false {
			log.Panicf("tabledef %v with invalid schema found.", tabledef)
		}

		for _, tabledef := range gtabledef.Indexmap { // check all index tables
			rsql.Assert(tabledef.Td_dbid == dbid)
			rsql.Assert(tabledef.Td_schid == schid)

			if _, ok := dictmgr.dm_SYSDATABASES.dbid_map[dbid]; ok == false {
				log.Panicf("tabledef %v with invalid database found.", tabledef)
			}

			if _, ok := dictmgr.dm_SYSSCHEMAS.schid_map[Sysschema_schidkey_t{Sch_dbid: dbid, Sch_schid: schid}]; ok == false {
				log.Panicf("tabledef %v with invalid schema found.", tabledef)
			}
		}
	}

	// check that all entries in dictmgr.dm_map_login_dbid_user have valid login, database, and user

	for entry, palid := range dictmgr.dm_map_login_dbid_user {
		if _, ok := dictmgr.dm_SYSLOGINS.login_id_map[entry.login_id]; ok == false {
			log.Panicf("entry %v with invalid login found.", entry)
		}

		if _, ok := dictmgr.dm_SYSDATABASES.dbid_map[entry.dbid]; ok == false {
			log.Panicf("entry %v with invalid database found.", entry)
		}

		if user, ok := dictmgr.dm_SYSPRINCIPALS.palid_map[palid]; ok == false {
			log.Panicf("entry %v with invalid user found.", entry)
			if user.Pal_type != PAL_TYPE_USER {
				log.Panicf("entry %v with principal != PAL_TYPE_USER found.", entry)
			}
		}
	}

	if len(dictmgr.dm_map_login_dbid_user) != principal_user_count {
		log.Panicf("dictmgr.dm_map_login_dbid_user has %d entries, but there are %d users in dictmgr.dm_SYSPRINCIPALS.", len(dictmgr.dm_map_login_dbid_user), principal_user_count)
	}
}

// Print_dictionary_maps is only used for debugging purpose. It should not be used in production code.
//
func (dictmgr *Dictionary_manager) Print_dictionary_maps() {

	dictmgr.LOCK()
	defer dictmgr.UNLOCK()

	// print SYSLOGINS

	fmt.Println("------ LOGINS ------")

	var login_name_slice []string

	for _, login := range dictmgr.dm_SYSLOGINS.login_id_map {
		login_name_slice = append(login_name_slice, login.Lg_name)
	}

	sort.Strings(login_name_slice)

	for _, login_name := range login_name_slice {
		login := dictmgr.dm_SYSLOGINS.name_map[login_name]

		other_info := ""
		if login.Lg_disabled_flag == true {
			other_info = "(disabled)"
		}
		if login.Lg_system_flag != 0 {
			other_info = "(system)"
		}
		fmt.Printf("%6d %-10s %-20s %-20s %-14s\n", login.Lg_login_id, login.Lg_name, login.Lg_default_database, login.Lg_default_language, other_info)
	}

	fmt.Println("")

	// print SYSDATABASES

	fmt.Println("------ DATABASES ------")

	var database_name_slice []string

	for _, database := range dictmgr.dm_SYSDATABASES.dbid_map {
		database_name_slice = append(database_name_slice, database.Db_name)
	}

	sort.Strings(database_name_slice)

	for _, database_name := range database_name_slice {
		database := dictmgr.dm_SYSDATABASES.name_map[database_name]
		fmt.Println("")
		other_info := ""
		if database.Db_system_flag != 0 {
			other_info += "system, "
		}
		if database.Db_status != DB_STATUS_ONLINE {
			other_info += database.Db_status.String() + ", "
		}
		if database.Db_mode != DB_MODE_READ_WRITE {
			other_info += database.Db_mode.String() + ", "
		}
		if database.Db_access != DB_ACCESS_MULTI_USER {
			other_info += database.Db_access.String() + ", "
		}
		if other_info != "" {
			other_info = "(" + other_info[:len(other_info)-2] + ")"
		}
		fmt.Printf("%6d %-20s  %s\n", database.Db_dbid, database.Db_name, other_info)

		//--- principals ---

		var user_name_slice []string
		var role_name_slice []string

		for _, principal := range dictmgr.dm_SYSPRINCIPALS.palid_map {
			if principal.Pal_dbid != database.Db_dbid {
				continue
			}
			if principal.Pal_type == PAL_TYPE_USER {
				user_name_slice = append(user_name_slice, principal.Pal_name)
			} else {
				role_name_slice = append(role_name_slice, principal.Pal_name)
			}
		}

		sort.Strings(user_name_slice)
		sort.Strings(role_name_slice)

		for _, user_name := range user_name_slice {
			user := dictmgr.dm_SYSPRINCIPALS.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: user_name}]

			var member_of_slice []string
			for _, r := range user.Pal_member_of {
				member_of_slice = append(member_of_slice, r.Pal_name)
			}
			sort.Strings(member_of_slice)

			login_name := dictmgr.dm_SYSLOGINS.login_id_map[user.Pal_login_id].Lg_name
			other_info := ""
			switch {
			case user.Pal_system_flag == PAL_SYSFLAG_USER_DBO:
				other_info += "(db owner, system)"
			case user.Pal_system_flag != 0:
				other_info += "(system)"
			}
			fmt.Printf("    %6d U %-10s %-10s %-20s %v\n", user.Pal_palid, user.Pal_name, login_name, other_info, member_of_slice)
		}

		for _, role_name := range role_name_slice {
			role := dictmgr.dm_SYSPRINCIPALS.name_map[Sysprincipal_namekey_t{Pal_dbid: database.Db_dbid, Pal_name: role_name}]

			var member_of_slice []string
			for _, r := range role.Pal_member_of {
				member_of_slice = append(member_of_slice, r.Pal_name)
			}
			sort.Strings(member_of_slice)

			other_info := ""
			if role.Pal_system_flag != 0 {
				other_info = "(system)"
			}
			fmt.Printf("    %6d R %-10s %-10s %-20s %v\n", role.Pal_palid, role.Pal_name, "", other_info, member_of_slice)
		}

		//--- schemas ---

		fmt.Println("")

		var schema_name_slice []string

		for _, schema := range dictmgr.dm_SYSSCHEMAS.schid_map {
			if schema.Sch_dbid != database.Db_dbid {
				continue
			}
			schema_name_slice = append(schema_name_slice, schema.Sch_name)
		}

		sort.Strings(schema_name_slice)

		for _, schema_name := range schema_name_slice {
			schema := dictmgr.dm_SYSSCHEMAS.name_map[Sysschema_namekey_t{Sch_dbid: database.Db_dbid, Sch_name: schema_name}]
			fmt.Printf("    %6d %s\n", schema.Sch_schid, schema.Sch_name)

			//--- gtabledefs ---

			var gtabledef_name_slice []string

			for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
				if gtabledef.Gdbid != database.Db_dbid {
					continue
				}
				gtabledef_name_slice = append(gtabledef_name_slice, gtabledef.Gtable_name)
			}

			sort.Strings(gtabledef_name_slice)

			for _, gtabledef_name := range gtabledef_name_slice {
				gtabledef := dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: database.Db_dbid, Gschid: schema.Sch_schid, Gtable_name: gtabledef_name}]
				rsql.Assert(gtabledef.Gdbid == database.Db_dbid && gtabledef.Gschid == schema.Sch_schid)
				fmt.Printf("                 %6d %s\n", gtabledef.Gtblid, gtabledef.Gtable_name)
			}
		}

	}
}

type Named_GTabledefs struct {
	M_db_name     string
	M_schema_name string
	M_gtabledef   *rsql.GTabledef
}

// Get_named_gtabledefs returns a slice of GTabledefs, sorted by <database_name, schema_name, gtabledef_name>.
//
func (dictmgr *Dictionary_manager) Get_named_gtabledefs() (result []Named_GTabledefs) {

	//--- databases ---

	var database_name_slice []string

	for _, database := range dictmgr.dm_SYSDATABASES.dbid_map {
		database_name_slice = append(database_name_slice, database.Db_name)
	}

	sort.Strings(database_name_slice) // sort by database_name

	for _, database_name := range database_name_slice {
		database := dictmgr.dm_SYSDATABASES.name_map[database_name]

		//--- schemas ---

		var schema_name_slice []string

		for _, schema := range dictmgr.dm_SYSSCHEMAS.schid_map {
			if schema.Sch_dbid != database.Db_dbid {
				continue
			}
			schema_name_slice = append(schema_name_slice, schema.Sch_name)
		}

		sort.Strings(schema_name_slice) // sort by schema_name

		for _, schema_name := range schema_name_slice {
			schema := dictmgr.dm_SYSSCHEMAS.name_map[Sysschema_namekey_t{Sch_dbid: database.Db_dbid, Sch_name: schema_name}]

			//--- gtabledefs ---

			var gtabledef_name_slice []string

			for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map {
				if gtabledef.Gdbid != database.Db_dbid {
					continue
				}
				gtabledef_name_slice = append(gtabledef_name_slice, gtabledef.Gtable_name)
			}

			sort.Strings(gtabledef_name_slice) // sort by gtabledef_name

			for _, gtabledef_name := range gtabledef_name_slice {
				gtabledef := dictmgr.dm_SYSTABLES.name_map[Systable_namekey_t{Gdbid: database.Db_dbid, Gschid: schema.Sch_schid, Gtable_name: gtabledef_name}]
				rsql.Assert(gtabledef.Gdbid == database.Db_dbid && gtabledef.Gschid == schema.Sch_schid)
				//fmt.Printf("                 %6d %s\n", gtabledef.Gtblid, gtabledef.Gtable_name)
				result = append(result, Named_GTabledefs{database.Db_name, schema.Sch_name, gtabledef})
			}
		}
	}

	return result
}

type Stmt_kind_t uint32

func (dictmgr *Dictionary_manager) Open() {
	var (
		err    error
		master *sqlite3.Conn
	)

	if master, err = sqlite3.Open(rsql.MASTER_PATH_RW); err != nil { // master.db is opened here, and will be closed only when the server shutdowns
		log.Panicf("%s", err)
	}

	dictmgr.dm_master = master
}

func (dictmgr *Dictionary_manager) Close() {

	dictmgr.dm_master.Close()
}

// Load_dict_from_master_db loads all records from the master database into memory.
//
// It cannot be called in init(), because the server calls os.Chdir to change the current directory, but init() is run before os.Chdir is called, and the path of master.db would be wrong.
//
func (dictmgr *Dictionary_manager) Load_dict_from_master_db() {

	// create stmt array

	dictmgr.dm_stmt_array = make([]*sqlite3.Stmt, DICT_STMT_ARRAY_SIZE)

	// load SYSDATABASES, SYSCHEMAS, etc

	dictmgr.Must_load_SYSPARAMETERS()
	dictmgr.Must_load_SYSLOGINS()
	dictmgr.Must_load_SYSDATABASES()
	dictmgr.Must_load_SYSSCHEMAS()
	dictmgr.Must_load_SYSTABLES()
	dictmgr.Must_load_SYSPRINCIPALS()
	dictmgr.Must_load_SYSPERMISSIONS()

	// remove databases that are half-created or half-deleted

	var dbs_to_remove []*Sysdatabase

	context_SA := rsql.New_context(SA_LOGIN_ID, "sa")
	context_SA.Ctx_wcache = &cache.Wcache{} // dummy Wcache

	for _, database := range dictmgr.dm_SYSDATABASES.dbid_map {
		if database.Db_status == DB_STATUS_BEING_CREATED || database.Db_status == DB_STATUS_BEING_DROPPED {
			dbs_to_remove = append(dbs_to_remove, database)
		}
	}

	for _, database := range dbs_to_remove {
		log.Printf("Try to remove database \"%s\" having status %s.", database.Db_name, database.Db_status.String())

		if rsql_err := dictmgr.Remove_sysdatabase(context_SA, database.Db_name); rsql_err != nil {
			log.Printf("Failed to remove database \"%s\".", database.Db_name)
			log.Panic(rsql_err)
		}

		log.Printf("Successfully removed database \"%s\".", database.Db_name)
	}

	// remove tables or indexes that are half-created or half-deleted

	var indexes_to_remove []*rsql.Tabledef
	var tables_to_remove []*rsql.GTabledef

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map { // looking for half-created or half-deleted indexes or tables
		if gtabledef.Base.Td_status == rsql.TD_STATUS_BEING_CREATED || gtabledef.Base.Td_status == rsql.TD_STATUS_BEING_DROPPED {
			tables_to_remove = append(tables_to_remove, gtabledef)
			continue // whole gtabledef and all indexes must be removed
		}

		for _, indexdef := range gtabledef.Indexmap {
			if indexdef.Td_status == rsql.TD_STATUS_BEING_CREATED || indexdef.Td_status == rsql.TD_STATUS_BEING_DROPPED {
				indexes_to_remove = append(indexes_to_remove, indexdef)
			}
		}
	}

	for _, indexdef := range indexes_to_remove { // remove half-created or half-deleted indexes
		var (
			database  *Sysdatabase
			schema    *Sysschema
			gtabledef *rsql.GTabledef
			ok        bool
		)

		if database, ok = dictmgr.dm_SYSDATABASES.dbid_map[indexdef.Td_dbid]; ok == false {
			log.Panicf("database %d not found for index \"%s\"", indexdef.Td_dbid, indexdef.Td_index_name)
		}

		if schema, ok = dictmgr.dm_SYSSCHEMAS.schid_map[Sysschema_schidkey_t{Sch_dbid: indexdef.Td_dbid, Sch_schid: indexdef.Td_schid}]; ok == false {
			log.Panicf("schema %s.%d not found for index \"%s\"", database.Db_name, indexdef.Td_schid, indexdef.Td_index_name)
		}

		if gtabledef, ok = dictmgr.dm_SYSTABLES.gtblid_map[indexdef.Td_base_gtblid]; ok == false {
			log.Panicf("gtabledef %s.%s.%d not found for index \"%s\"", database.Db_name, schema.Sch_name, indexdef.Td_base_gtblid, indexdef.Td_index_name)
		}

		log.Printf("Try to remove index \"%s.%s.%s.%s\" having status %s.", database.Db_name, schema.Sch_name, gtabledef.Gtable_name, indexdef.Td_index_name, indexdef.Td_status.String())

		table_qname := rsql.Object_qname_t{Database_name: database.Db_name, Schema_name: schema.Sch_name, Object_name: gtabledef.Gtable_name, Object_name_original: gtabledef.Gtable_name_original}

		if rsql_err := dictmgr.Remove_sysindex(context_SA, table_qname, indexdef.Td_index_name); rsql_err != nil {
			log.Printf("Failed to remove index \"%s.%s.%s.%s\".", database.Db_name, schema.Sch_name, gtabledef.Gtable_name, indexdef.Td_index_name)
			log.Panic(rsql_err)
		}

		log.Printf("Successfully removed index \"%s.%s.%s.%s\".", database.Db_name, schema.Sch_name, gtabledef.Gtable_name, indexdef.Td_index_name)
	}

	for _, gtabledef := range tables_to_remove { // remove half-created or half-deleted tables
		var (
			database *Sysdatabase
			schema   *Sysschema
			ok       bool
		)

		if database, ok = dictmgr.dm_SYSDATABASES.dbid_map[gtabledef.Gdbid]; ok == false {
			log.Panicf("database %d not found for table \"%s\"", gtabledef.Gdbid, gtabledef.Gtable_name)
		}

		if schema, ok = dictmgr.dm_SYSSCHEMAS.schid_map[Sysschema_schidkey_t{Sch_dbid: gtabledef.Gdbid, Sch_schid: gtabledef.Gschid}]; ok == false {
			log.Panicf("schema %s.%d not found for table \"%s\"", database.Db_name, gtabledef.Gschid, gtabledef.Gtable_name)
		}

		log.Printf("Try to remove gtabledef \"%s.%s.%s\" having status %s.", database.Db_name, schema.Sch_name, gtabledef.Gtable_name, gtabledef.Base.Td_status.String())

		table_qname := rsql.Object_qname_t{Database_name: database.Db_name, Schema_name: schema.Sch_name, Object_name: gtabledef.Gtable_name, Object_name_original: gtabledef.Gtable_name_original}

		if rsql_err := dictmgr.Remove_systable(context_SA, table_qname); rsql_err != nil {
			log.Printf("Failed to remove gtabledef \"%s.%s.%s\".", database.Db_name, schema.Sch_name, gtabledef.Gtable_name)
			log.Panic(rsql_err)
		}

		log.Printf("Successfully removed gtabledef \"%s.%s.%s\".", database.Db_name, schema.Sch_name, gtabledef.Gtable_name)
	}

	// lists all corrupted base tables or indexes

	for _, gtabledef := range dictmgr.dm_SYSTABLES.gtblid_map { // looking for corrupted indexes or tables
		var (
			database *Sysdatabase
			schema   *Sysschema
			ok       bool
		)

		if database, ok = dictmgr.dm_SYSDATABASES.dbid_map[gtabledef.Gdbid]; ok == false {
			log.Panicf("database %d not found for table \"%s\"", gtabledef.Gdbid, gtabledef.Gtable_name)
		}

		if schema, ok = dictmgr.dm_SYSSCHEMAS.schid_map[Sysschema_schidkey_t{Sch_dbid: gtabledef.Gdbid, Sch_schid: gtabledef.Gschid}]; ok == false {
			log.Panicf("schema %s.%d not found for table \"%s\"", database.Db_name, gtabledef.Gschid, gtabledef.Gtable_name)
		}

		if gtabledef.Base.Td_status == rsql.TD_STATUS_CORRUPTED {
			log.Printf("Corrupted table found: \"%s.%s.%s\"", database.Db_name, schema.Sch_name, gtabledef.Gtable_name)
			continue // if base table is corrupted, indexes are useless and corrupted too
		}

		for _, indexdef := range gtabledef.Indexmap {
			if indexdef.Td_status == rsql.TD_STATUS_CORRUPTED {
				log.Printf("Corrupted index found: \"%s.%s.%s.%s\"", database.Db_name, schema.Sch_name, gtabledef.Gtable_name, indexdef.Td_index_name)
			}
		}
	}
}

func (dictmgr *Dictionary_manager) Execute(stmt_kind Stmt_kind_t, arg sqlite3.NamedArgs) {
	var (
		err  error
		stmt *sqlite3.Stmt
	)

	rsql.Assert(dictmgr.dm_locked == true && dictmgr.dm_tran_status == DICT_TRAN_STATUS_INSIDE_TRANSACTION)

	stmt = dictmgr.get_stmt(stmt_kind)

	if arg == nil {
		err = stmt.Exec()
	} else {
		err = stmt.Exec(arg)
	}

	if err != nil {
		log.Panicf("Exec() failed. [%s]", err)
	}

	dictmgr.release_stmt(stmt)
}

func (dictmgr *Dictionary_manager) LOCK() {

	dictmgr.dm_lock.Lock()
	dictmgr.dm_locked = true
}

func (dictmgr *Dictionary_manager) UNLOCK() {

	dictmgr.dm_locked = false
	dictmgr.dm_lock.Unlock()
}

func (dictmgr *Dictionary_manager) LOCK_and_BEGIN_EXCLUSIVE() {
	var (
		err  error
		stmt *sqlite3.Stmt
	)

	// lock dictionary

	dictmgr.dm_lock.Lock()
	rsql.Assert(dictmgr.dm_locked == false && dictmgr.dm_tran_status == DICT_TRAN_STATUS_NONE)
	dictmgr.dm_locked = true

	// BEGIN EXCLUSIVE

	dictmgr.dm_tran_status = DICT_TRAN_STATUS_INSIDE_TRANSACTION // dictmgr.get_stmt() on next line asserts this

	stmt = dictmgr.get_stmt(DICT_STMT_BEGIN_EXCLUSIVE)

	if err = stmt.Exec(); err != nil {
		log.Panicf("BEGIN EXCLUSIVE failed. [%s]", err)
	}
	dictmgr.release_stmt(stmt)
}

func (dictmgr *Dictionary_manager) COMMIT_and_BEGIN_EXCLUSIVE() {
	var (
		err  error
		stmt *sqlite3.Stmt
	)

	rsql.Assert(dictmgr.dm_locked == true && dictmgr.dm_tran_status == DICT_TRAN_STATUS_INSIDE_TRANSACTION)

	// COMMIT

	stmt = dictmgr.get_stmt(DICT_STMT_COMMIT)

	if err = stmt.Exec(); err != nil {
		log.Panicf("COMMIT failed. [%s]", err)
	}
	dictmgr.release_stmt(stmt)

	// BEGIN EXCLUSIVE

	stmt = dictmgr.get_stmt(DICT_STMT_BEGIN_EXCLUSIVE)

	if err = stmt.Exec(); err != nil {
		log.Panicf("BEGIN EXCLUSIVE failed. [%s]", err)
	}
	dictmgr.release_stmt(stmt)
}

func (dictmgr *Dictionary_manager) COMMIT() {
	var (
		err  error
		stmt *sqlite3.Stmt
	)

	rsql.Assert(dictmgr.dm_locked == true && dictmgr.dm_tran_status == DICT_TRAN_STATUS_INSIDE_TRANSACTION)

	// COMMIT

	stmt = dictmgr.get_stmt(DICT_STMT_COMMIT)

	if err = stmt.Exec(); err != nil {
		log.Panicf("COMMIT failed. [%s]", err)
	}
	dictmgr.release_stmt(stmt)

	dictmgr.dm_tran_status = DICT_TRAN_STATUS_NONE
}

func (dictmgr *Dictionary_manager) finalize_transaction_and_UNLOCK() {
	var (
		err  error
		stmt *sqlite3.Stmt
	)

	rsql.Assert(dictmgr.dm_locked == true)

	// ROLLBACK if necessary. It happens e.g. if we create a new table but the name already exists. In this case, the caller returns a *rsql.Error, and we want just to undo the BEGIN.

	if dictmgr.dm_tran_status == DICT_TRAN_STATUS_INSIDE_TRANSACTION { // no COMMIT has been issued
		stmt = dictmgr.get_stmt(DICT_STMT_ROLLBACK)

		if err = stmt.Exec(); err != nil {
			log.Panicf("ROLLBACK failed. [%s]", err)
		}
		dictmgr.release_stmt(stmt)

		dictmgr.dm_tran_status = DICT_TRAN_STATUS_NONE
	}

	// unlock dictionary

	dictmgr.dm_locked = false
	dictmgr.dm_lock.Unlock()
}

// get a prepared sqlite3_stmt.
//
//    dict_stmt_kind is :
//      - DICT_STMT_BEGIN
//      - DICT_STMT_COMMIT
//      - DICT_STMT_SYSLOGINS_INSERT
//      - etc
//
func (dictmgr *Dictionary_manager) get_stmt(dict_stmt_kind Stmt_kind_t) *sqlite3.Stmt {
	var (
		err      error
		master   *sqlite3.Conn
		stmt     *sqlite3.Stmt
		stmt_sql string
	)

	rsql.Assert(dictmgr.dm_locked == true && dictmgr.dm_tran_status == DICT_TRAN_STATUS_INSIDE_TRANSACTION && dictmgr.dm_stmt_in_use == DICT_STMT_INVALID)

	/*=== if prepared statement already exists, just return it ===*/

	stmt = dictmgr.dm_stmt_array[dict_stmt_kind] // if stmt exists, sqlite3_reset() has always been applied to it.

	if stmt != nil {
		rsql.Assert(stmt.Busy() == false) // assert stmt has been reset

		dictmgr.dm_stmt_in_use = dict_stmt_kind

		return stmt
	}

	/*=== else, prepare the statement ===*/

	switch dict_stmt_kind {

	case DICT_STMT_BEGIN_EXCLUSIVE:
		stmt_sql = "BEGIN EXCLUSIVE"

	case DICT_STMT_COMMIT:
		stmt_sql = "COMMIT"

	case DICT_STMT_ROLLBACK:
		stmt_sql = "ROLLBACK"

	case DICT_STMT_SYSPARAMETERS_UPDATE_VALUE_STRING:
		stmt_sql = "UPDATE sysparameters SET param_value_string=$param_value_string WHERE param_id=$param_id"

	case DICT_STMT_SYSPARAMETERS_UPDATE_VALUE_INT:
		stmt_sql = "UPDATE sysparameters SET param_value_int=$param_value_int WHERE param_id=$param_id"

	case DICT_STMT_SYSLOGINS_INSERT:
		stmt_sql = "INSERT INTO syslogins(login_id, login_name, login_password_hash, login_default_database, login_default_language, login_disabled_flag, login_create_date, login_system_flag) VALUES($login_id, $login_name, $login_password_hash, $login_default_database, $login_default_language, $login_disabled_flag, $login_create_date, $login_system_flag)"

	case DICT_STMT_SYSLOGINS_UPDATE:
		stmt_sql = "UPDATE syslogins SET login_name=$login_name, login_password_hash=$login_password_hash, login_default_database=$login_default_database, login_default_language=$login_default_language, login_disabled_flag=$login_disabled_flag WHERE login_id=$login_id"

	case DICT_STMT_SYSLOGINS_DELETE:
		stmt_sql = "DELETE FROM syslogins WHERE login_id=$login_id"

	case DICT_STMT_SYSDATABASES_INSERT:
		stmt_sql = "INSERT INTO sysdatabases(db_dbid, db_name, db_create_date, db_status, db_status_text, db_mode, db_mode_text, db_access, db_access_text, db_system_flag) VALUES($db_dbid, $db_name, $db_create_date, $db_status, $db_status_text, $db_mode, $db_mode_text, $db_access, $db_access_text, $db_system_flag)"

	case DICT_STMT_SYSDATABASES_UPDATE_NAME:
		stmt_sql = "UPDATE sysdatabases SET db_name=$db_name WHERE db_dbid=$db_dbid"

	case DICT_STMT_SYSDATABASES_UPDATE_STATUS:
		stmt_sql = "UPDATE sysdatabases SET db_status=$db_status, db_status_text=$db_status_text WHERE db_dbid=$db_dbid"

	case DICT_STMT_SYSDATABASES_UPDATE_MODE:
		stmt_sql = "UPDATE sysdatabases SET db_mode=$db_mode, db_mode_text=$db_mode_text WHERE db_dbid=$db_dbid"

	case DICT_STMT_SYSDATABASES_UPDATE_ACCESS:
		stmt_sql = "UPDATE sysdatabases SET db_access=$db_access, db_access_text=$db_access_text WHERE db_dbid=$db_dbid"

	case DICT_STMT_SYSDATABASES_DELETE:
		stmt_sql = "DELETE FROM sysdatabases WHERE db_dbid=$db_dbid"

	case DICT_STMT_SYSSCHEMAS_INSERT:
		stmt_sql = "INSERT INTO sysschemas(sch_dbid, sch_schid, sch_name) VALUES($sch_dbid, $sch_schid, $sch_name)"

	case DICT_STMT_SYSSCHEMAS_IN_DBID_DELETE:
		stmt_sql = "DELETE FROM sysschemas WHERE sch_dbid=$sch_dbid"

	case DICT_STMT_SYSPRINCIPALS_INSERT:
		stmt_sql = "INSERT INTO sysprincipals(pal_palid, pal_dbid, pal_name, pal_type, pal_create_date, pal_default_schema_name, pal_login_id, pal_system_flag) VALUES($pal_palid, $pal_dbid, $pal_name, $pal_type, $pal_create_date, $pal_default_schema_name, $pal_login_id, $pal_system_flag)"

	case DICT_STMT_SYSPRINCIPALS_UPDATE:
		stmt_sql = "UPDATE sysprincipals SET pal_dbid=$pal_dbid, pal_name=$pal_name, pal_type=$pal_type, pal_create_date=$pal_create_date, pal_default_schema_name=$pal_default_schema_name, pal_login_id=$pal_login_id, pal_system_flag=$pal_system_flag WHERE pal_palid=$pal_palid"

	case DICT_STMT_SYSPRINCIPALS_DELETE:
		stmt_sql = "DELETE FROM sysprincipals WHERE pal_palid=$pal_palid"

	case DICT_STMT_SYSPRINCIPALS_DELETE_ALL_DBID:
		stmt_sql = "DELETE FROM sysprincipals WHERE pal_dbid=$dbid"

	case DICT_STMT_SYSROLEMEMBERS_INSERT:
		stmt_sql = "INSERT INTO sysrolemembers(rm_member_palid, rm_role_palid) VALUES($rm_member_palid, $rm_role_palid)"

	case DICT_STMT_SYSROLEMEMBERS_DELETE:
		stmt_sql = "DELETE FROM sysrolemembers WHERE rm_member_palid=$rm_member_palid and rm_role_palid=$rm_role_palid"

	case DICT_STMT_SYSROLEMEMBERS_DELETE_PALID:
		stmt_sql = "DELETE FROM sysrolemembers WHERE rm_member_palid=$palid OR rm_role_palid=$palid"

	case DICT_STMT_SYSROLEMEMBERS_DELETE_ALL_DBID:
		stmt_sql = "DELETE FROM sysrolemembers WHERE rm_member_palid IN (SELECT pal_palid FROM sysprincipals WHERE pal_dbid=$dbid)"

	case DICT_STMT_SYSCOLDEFS_INSERT:
		stmt_sql = "INSERT INTO syscoldefs(c_tblid, c_base_seqno, c_colname, c_colname_original, c_datatype, c_precision, c_scale, c_fixlen_flag, c_collation_strength, c_collation, c_autonum, c_NOT_NULL_flag) VALUES($c_tblid, $c_base_seqno, $c_colname, $c_colname_original, $c_datatype, $c_precision, $c_scale, $c_fixlen_flag, $c_collation_strength, $c_collation, $c_autonum, $c_NOT_NULL_flag)"

	case DICT_STMT_SYSCOLDEFS_UPDATE_COLNAME:
		stmt_sql = "UPDATE syscoldefs SET c_colname=$c_colname, c_colname_original=$c_colname_original WHERE c_tblid=$c_tblid AND c_base_seqno=$c_base_seqno"

	case DICT_STMT_SYSCOLDEFS_UPDATE_NULLABILITY:
		stmt_sql = "UPDATE syscoldefs SET c_NOT_NULL_flag=$c_NOT_NULL_flag WHERE c_tblid=$c_tblid AND c_base_seqno=$c_base_seqno"

	case DICT_STMT_SYSCOLDEFS_DELETE_GTBLID:
		stmt_sql = "DELETE FROM syscoldefs WHERE c_tblid=$gtblid"

	case DICT_STMT_SYSCOLDEFS_DELETE_ALL_DBID:
		stmt_sql = "DELETE FROM syscoldefs WHERE c_tblid IN (SELECT tbl_tblid FROM systabledefs WHERE tbl_dbid=$dbid)"

	case DICT_STMT_SYSTABLEDEFS_INSERT:
		stmt_sql = "INSERT INTO systabledefs(tbl_tblid, tbl_dbid, tbl_schid, tbl_base_gtblid, tbl_index_name, tbl_index_name_original, tbl_type, tbl_index_type, tbl_coldefs, tbl_nk, tbl_payload, tbl_status, tbl_status_text) VALUES($tbl_tblid, $tbl_dbid, $tbl_schid, $tbl_base_gtblid, $tbl_index_name, $tbl_index_name_original, $tbl_type, $tbl_index_type, $tbl_coldefs, $tbl_nk, $tbl_payload, $tbl_status, $tbl_status_text)"

	case DICT_STMT_SYSTABLEDEFS_UPDATE_STATUS:
		stmt_sql = "UPDATE systabledefs SET tbl_status=$tbl_status, tbl_status_text=$tbl_status_text WHERE tbl_tblid=$tbl_tblid"

	case DICT_STMT_SYSTABLEDEFS_UPDATE_NAME:
		stmt_sql = "UPDATE systabledefs SET tbl_index_name=$tbl_index_name, tbl_index_name_original=$tbl_index_name_original WHERE tbl_tblid=$tbl_tblid"

	case DICT_STMT_SYSTABLEDEFS_DELETE:
		stmt_sql = "DELETE FROM systabledefs WHERE tbl_tblid=$tblid"

	case DICT_STMT_SYSTABLEDEFS_DELETE_GTBLID:
		stmt_sql = "DELETE FROM systabledefs WHERE tbl_base_gtblid=$gtblid"

	case DICT_STMT_SYSTABLEDEFS_DELETE_ALL_DBID:
		stmt_sql = "DELETE FROM systabledefs WHERE tbl_dbid=$dbid"

	case DICT_STMT_SYSGTABLEDEFS_INSERT:
		stmt_sql = "INSERT INTO sysgtabledefs(gtbl_gtblid, gtbl_dbid, gtbl_schid, gtbl_table_name, gtbl_table_name_original, gtbl_identity_seed, gtbl_identity_increment, gtbl_create_date) VALUES($gtbl_gtblid, $gtbl_dbid, $gtbl_schid, $gtbl_table_name, $gtbl_table_name_original, $gtbl_identity_seed, $gtbl_identity_increment, $gtbl_create_date)"

	case DICT_STMT_SYSGTABLEDEFS_UPDATE_NAME:
		stmt_sql = "UPDATE sysgtabledefs SET gtbl_table_name=$gtbl_table_name, gtbl_table_name_original=$gtbl_table_name_original WHERE gtbl_gtblid=$gtbl_gtblid"

	case DICT_STMT_SYSGTABLEDEFS_DELETE:
		stmt_sql = "DELETE FROM sysgtabledefs WHERE gtbl_gtblid=$gtblid"

	case DICT_STMT_SYSGTABLEDEFS_DELETE_ALL_DBID:
		stmt_sql = "DELETE FROM sysgtabledefs WHERE gtbl_dbid=$dbid"

	case DICT_STMT_SYSPERMISSIONS_INSERT:
		stmt_sql = "INSERT INTO syspermissions(p_objid, p_palid, p_grant, p_deny) VALUES ($p_objid, $p_palid, $p_grant, $p_deny)"

	case DICT_STMT_SYSPERMISSIONS_UPDATE:
		stmt_sql = "UPDATE syspermissions SET p_grant=$p_grant, p_deny=$p_deny WHERE p_objid=$p_objid and p_palid=$p_palid"

	case DICT_STMT_SYSPERMISSIONS_DELETE:
		stmt_sql = "DELETE FROM syspermissions WHERE p_objid=$p_objid and p_palid=$p_palid"

	case DICT_STMT_SYSPERMISSIONS_DELETE_PRINCIPAL:
		stmt_sql = "DELETE FROM syspermissions WHERE p_palid=$p_palid"

	case DICT_STMT_SYSPERMISSIONS_DELETE_GTBLID:
		stmt_sql = "DELETE FROM syspermissions WHERE p_objid=$gtblid"

	case DICT_STMT_SYSPERMISSIONS_DELETE_ALL_DBID:
		stmt_sql = "DELETE FROM syspermissions WHERE p_objid IN (SELECT tbl_tblid FROM systabledefs WHERE tbl_dbid=$dbid)"

	default:
		panic("dict_stmt_kind unknown")
	}

	/*=== prepare stmt ===*/

	master = dictmgr.dm_master

	if stmt, err = master.Prepare(stmt_sql); err != nil {
		log.Panicf("sqlite: failed to prepare \"%s\". [%s]", stmt_sql, err)
	}

	dictmgr.dm_stmt_array[dict_stmt_kind] = stmt

	dictmgr.dm_stmt_in_use = dict_stmt_kind

	return stmt
}

func (dictmgr *Dictionary_manager) release_stmt(stmt *sqlite3.Stmt) {

	rsql.Assert(dictmgr.dm_locked == true && dictmgr.dm_tran_status == DICT_TRAN_STATUS_INSIDE_TRANSACTION && dictmgr.dm_stmt_in_use != DICT_STMT_INVALID)

	dictmgr.dm_stmt_in_use = DICT_STMT_INVALID
	stmt.Reset()
}
