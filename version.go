package rsql

import (
	"strings"
)

const VERSION = "0.7.1"

func Check_version(version_string string) bool {

	Assert(VERSION == "0.7.1")

	version_string = strings.TrimSpace(version_string)

	switch version_string { // VERSION server is can work with storage created by these version_strings
	case "0.7.1":
		return true
	case "0.7":
		return true
	}

	return false
}
