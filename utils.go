package rsql

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
)

const (
	DIR_PERM         = 0775
	TBLFILE_PERM     = 0664
	JOURNALFILE_PERM = 0664
	BULKDIR_PERM     = 0777
	DUMPDIR_PERM     = 0777
	TEMPDIR_PERM     = 0777

	DIRECTORY_DATA     = "data" // if a new directory used by the server is added, you must also change Check_forbidden_file_path function
	DIRECTORY_INDEX    = "index"
	DIRECTORY_JOURNALS = "journals"
	DIRECTORY_LOGGING  = "logging"
	DIRECTORY_BULKDIR  = "bulkdir"
	DIRECTORY_DUMPDIR  = "dumpdir"
	DIRECTORY_TEMPDIR  = "tempdir"

	JOURNAL_FILENAME_PREFIX    = "journal_"
	JOURNAL_FILENAME_EXTENSION = ".jrnl"

	NORMAL_FILE_PERM          = 0664
	VERSION_FILENAME          = "version"
	LISTENER_ADDRESS_FILENAME = "listener_address.cfg"
)

// Check_forbidden_file_path returns an error if the path is in a directory reserved for server use.
//
func Check_forbidden_file_path(file_path string) *Error {
	var (
		err               error
		abs_file_path     string
		abs_file_path_dir string
		abs_root_dir      string
	)

	if abs_file_path, err = filepath.Abs(file_path); err != nil {
		return New_Error(ERROR_GENERAL, ERROR_FILEPATH, ERROR_BATCH_ABORT, err)
	}

	abs_file_path_dir = filepath.Dir(abs_file_path)

	if abs_root_dir, err = os.Getwd(); err != nil {
		return New_Error(ERROR_GENERAL, ERROR_FILEPATH, ERROR_BATCH_ABORT, err)
	}

	// file_path cannot be in root dir of the rsql instance

	if abs_file_path_dir == abs_root_dir {
		return New_Error(ERROR_GENERAL, ERROR_SERVER_DIRECTORY, ERROR_BATCH_ABORT, abs_file_path_dir)
	}

	// file_path cannot be in data, index, journals, etc directories of the rsql instance

	if abs_root_dir_xxx := filepath.Join(abs_root_dir, DIRECTORY_DATA); abs_file_path_dir == abs_root_dir_xxx {
		return New_Error(ERROR_GENERAL, ERROR_SERVER_DIRECTORY, ERROR_BATCH_ABORT, abs_file_path_dir)
	}
	if abs_root_dir_xxx := filepath.Join(abs_root_dir, DIRECTORY_INDEX); abs_file_path_dir == abs_root_dir_xxx {
		return New_Error(ERROR_GENERAL, ERROR_SERVER_DIRECTORY, ERROR_BATCH_ABORT, abs_file_path_dir)
	}
	if abs_root_dir_xxx := filepath.Join(abs_root_dir, DIRECTORY_JOURNALS); abs_file_path_dir == abs_root_dir_xxx {
		return New_Error(ERROR_GENERAL, ERROR_SERVER_DIRECTORY, ERROR_BATCH_ABORT, abs_file_path_dir)
	}
	if abs_root_dir_xxx := filepath.Join(abs_root_dir, DIRECTORY_LOGGING); abs_file_path_dir == abs_root_dir_xxx {
		return New_Error(ERROR_GENERAL, ERROR_SERVER_DIRECTORY, ERROR_BATCH_ABORT, abs_file_path_dir)
	}
	if abs_root_dir_xxx := filepath.Join(abs_root_dir, DIRECTORY_TEMPDIR); abs_file_path_dir == abs_root_dir_xxx {
		return New_Error(ERROR_GENERAL, ERROR_SERVER_DIRECTORY, ERROR_BATCH_ABORT, abs_file_path_dir)
	}

	return nil
}

func Must_create_ordinary_file(filename string, content string) {

	if err := ioutil.WriteFile(filename, ([]byte)(content), NORMAL_FILE_PERM); err != nil {
		log.Panicf("%s", err)
	}
}

func Must_read_ordinary_file(filename string) (content string) {
	var (
		err  error
		buff []byte
	)

	if buff, err = ioutil.ReadFile(filename); err != nil {
		if os.IsNotExist(err) {
			log.Fatalf("file \"%s\" not found.", filename)
		}

		log.Fatalf("%s", err)
	}

	return string(buff)
}

func Read_string_from_file(filename string) (content string, err error) {
	var (
		buff []byte
	)

	if buff, err = ioutil.ReadFile(filename); err != nil {
		return "", err
	}

	return string(buff), nil
}

// Must_remove_file removes path file.
// If path doesn't exist, no error is returned.
//
func Must_remove_file(path string) {

	err := os.Remove(path)
	if err == nil || os.IsNotExist(err) {
		return
	}

	log.Panicf("%s", err)
}

// Must_remove_existing_file removes path file.
// If path doesn't exist, panics.
//
func Must_remove_existing_file(path string) {

	err := os.Remove(path)
	if err == nil || os.IsNotExist(err) {
		return
	}

	log.Panicf("%s", err)
}

func Must_create_and_open_flashfile(prefix string) *os.File {
	var (
		err error
		f   *os.File
	)

	if f, err = ioutil.TempFile(DIRECTORY_TEMPDIR, prefix); err != nil {
		log.Fatalf("%s", err)
	}

	if err = os.Remove(f.Name()); err != nil { // file will disappear on closing
		log.Fatalf("%s", err)
	}

	return f
}

func Db_path(prefix_path string, dbid int64) string {

	path := filepath.Join(".", prefix_path, "d"+strconv.FormatInt(dbid, 10))

	return path
}

func Schema_path(prefix_path string, dbid int64, schid int64) string {

	path := filepath.Join(".", prefix_path, "d"+strconv.FormatInt(dbid, 10), "s"+strconv.FormatInt(schid, 10))

	return path
}

func Tabledef_path(prefix_path string, dbid int64, schid int64, tblid int64) (path string) {

	switch prefix_path {
	case DIRECTORY_DATA:
		path = filepath.Join(".", prefix_path, "d"+strconv.FormatInt(dbid, 10), "s"+strconv.FormatInt(schid, 10), "t"+strconv.FormatInt(tblid, 10))

	case DIRECTORY_INDEX:
		path = filepath.Join(".", prefix_path, "d"+strconv.FormatInt(dbid, 10), "s"+strconv.FormatInt(schid, 10), "i"+strconv.FormatInt(tblid, 10))

	default:
		panic("impossible")
	}

	return path
}

func Journal_path(prefix_path string, journal_id int64) string {

	path := filepath.Join(".", prefix_path, JOURNAL_FILENAME_PREFIX+strconv.FormatInt(journal_id, 10)+JOURNAL_FILENAME_EXTENSION)

	return path
}

func Directory_exists(path string) bool {
	var (
		err   error
		finfo os.FileInfo
	)

	// read path info

	if finfo, err = os.Stat(path); err != nil {
		if os.IsNotExist(err) { // path does not exist
			return false
		}
		log.Panicf("%s", err) // other error during reading of path info
	}

	// path exists

	if finfo.IsDir() == true { // and it is a directory
		return true
	}

	return false // it is something else
}

func File_exists(path string) bool {
	var (
		err   error
		finfo os.FileInfo
	)

	// read path info

	if finfo, err = os.Stat(path); err != nil {
		if os.IsNotExist(err) { // path does not exist
			return false
		}
		log.Panicf("%s", err) // other error during reading of path info
	}

	// path exists

	if finfo.Mode().IsRegular() == true { // and it is a regular file
		return true
	}

	return false // it is something else
}

func Must_create_directory(path string, perm os.FileMode) {

	if Directory_exists(path) {
		log.Panicf("Directory \"%s\" already exists.", path)
	}

	if err := os.Mkdir(path, perm); err != nil {
		log.Panicf("%s", err)
	}
}

// Must_remove_directory_all removes all subdirectories and files in path, and path itself.
// If path doesn't exist, no error is returned.
//
func Must_remove_directory_all(path string) {

	if err := os.RemoveAll(path); err != nil { // if the path does not exist, os.RemoveAll returns nil (no error).
		log.Panicf("%s", err)
	}
}

// Must_remove_directory_content removes all content of the directory.

func Must_remove_directory_content(dir string) {
	var (
		err   error
		d     *os.File
		names []string
	)

	if d, err = os.Open(dir); err != nil {
		log.Panicf("%s", err)
	}
	defer d.Close()

	if names, err = d.Readdirnames(-1); err != nil {
		log.Panicf("%s", err)
	}

	for _, name := range names {
		if err = os.RemoveAll(filepath.Join(dir, name)); err != nil {
			log.Panicf("%s", err)
		}
	}
}

func Must_create_tabledef_file(path string, perm os.FileMode) {

	if File_exists(path) {
		log.Panicf("Tabledef file \"%s\" already exists.", path)
	}

	if err := ioutil.WriteFile(path, nil, perm); err != nil {
		log.Panicf("%s", err)
	}
}

// Must_remove_tabledef_file removes path file.
// If path doesn't exist, no error is returned.
//
func Must_remove_tabledef_file(path string) {

	err := os.Remove(path)
	if err == nil || os.IsNotExist(err) {
		return
	}

	log.Panicf("%s", err)
}

// Read_int64 returns the first integer read in string s.
// s is a series of integer separated by separator. Blanks characters are skipped.
// E.g. "35,7  ,   675  "
//
//     This function is simple and can only read integers with <= 18 digits, to avoid int64 overflow. Else, it panics.
//     It is not a problem as it is used to read column numbers.
//
func Read_int64(s string, separator byte) (result int64, tail string, err error) {
	var (
		i int
		c uint8
	)

	// skip leading blanks

	for i = 0; i < len(s); i++ {
		if s[i] != ' ' {
			break
		}
	}
	s = s[i:]

	// read number

	for i = 0; i < len(s); i++ {
		c = s[i]
		if !(c >= '0' && c <= '9') {
			break
		}
		result = result*10 + (int64(c) - '0')
	}
	if i == 0 { // i is number of digits read so far
		return 0, s, errors.New("string parse error: no digit found.")
	}
	if i > 18 {
		panic("integer has > 18 digits")
	}

	s = s[i:]

	// skip trailing blanks

	for i = 0; i < len(s); i++ {
		if s[i] != ' ' {
			break
		}
	}
	s = s[i:]

	// skip separator, or end of string

	switch {
	case len(s) == 0:
		return result, "", nil // last number in string has been read

	case s[0] == separator:
		s = s[1:]
		if len(s) == 0 { // don't allow a string terminated by a separator
			return result, string('\uFFFD'), nil // \uFFFD is the unicode unknown character, so that next time this function is called, an error is returned.
		}
		return result, s, nil // return remaining string

	default:
		return result, s, errors.New("string parse error: bad separator.")
	}
}

// list of prime numbers
//
var PRIMES_SAMPLES = []uint32{
	53,
	97,
	193,
	389,
	769,
	1543,
	3079,
	6151,
	12289,
	24593,
	49157,
	98317,
	196613,
	393241,
	786433,
	1572869,
	3145739,
	6291469,
	12582917,
	25165843,
	50331653,
	100663319,
	201326611,
	402653189,
	805306457,
	1610612741}

func Prime_for_hashtable(count_of_elements uint32) uint32 {
	var (
		n     uint64
		prime uint32
	)

	n = uint64(count_of_elements + count_of_elements/2)

	for _, prime = range PRIMES_SAMPLES {
		if uint64(prime) >= n {
			return prime
		}
	}

	return 1610612741
}

func Get_uint64(bytes []byte) uint64 {
	return uint64(bytes[0]) | uint64(bytes[1])<<8 | uint64(bytes[2])<<16 | uint64(bytes[3])<<24 |
		uint64(bytes[4])<<32 | uint64(bytes[5])<<40 | uint64(bytes[6])<<48 | uint64(bytes[7])<<56
}

func Set_uint64(bytes []byte, v uint64) {
	bytes[0] = byte(v)
	bytes[1] = byte(v >> 8)
	bytes[2] = byte(v >> 16)
	bytes[3] = byte(v >> 24)
	bytes[4] = byte(v >> 32)
	bytes[5] = byte(v >> 40)
	bytes[6] = byte(v >> 48)
	bytes[7] = byte(v >> 56)
}

func Rtrim(s string) string {

	length := len(s)

	for i := length - 1; i >= 0; i-- {
		if s[i] != ' ' {
			return s[:i+1]
		}
	}

	return ""
}
