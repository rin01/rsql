package rsql

import (
	"time"

	"golang.org/x/text/collate"
)

// Fill_index_with_records contains a function defined in rsql/stmt, but used in rsql/dict.
// This variable is created here to avoid circular import issue.
//
var Fill_index_with_records func(context *Context, table_qname Object_qname_t, gtabledef *GTabledef, indexdef *Tabledef) *Error

// Check_column_not_null contains a function defined in rsql/csr, but used in rsql/dict.
// This variable is created here to avoid circular import issue.
//
var Check_column_not_null func(context *Context, gtabledef *GTabledef, column_name string) *Error

type Durability_t uint8

const (
	DURABILITY_PERMANENT Durability_t = iota // normal table, with records stored in its own permanent data file, and modifications are logged in main journal.
	DURABILITY_TEMPORARY                     // not implemented yet. Temporary #table, with records stored in its own temporary data file, and modifications are logged in temporary journal.
	DURABILITY_FLASH                         // variable @table or system generated table, e.g. $grouptable$, with records living as TRANSITORY in temporary journal, until the session terminates. System generated table is discarded as soon as it has been read. Is not affected by COMMIT or ROLLBACK.
)

func (durability Durability_t) String() string {

	switch durability {
	case DURABILITY_PERMANENT:
		return "DURABILITY_PERMANENT"
	case DURABILITY_TEMPORARY:
		return "DURABILITY_TEMPORARY"
	case DURABILITY_FLASH:
		return "DURABILITY_FLASH"
	default:
		panic("impossible")
	}
}

type Td_type_t uint8

const (
	TD_TYPE_BASE_TABLE  Td_type_t = 0
	TD_TYPE_INDEX_TABLE Td_type_t = 1
)

func (t Td_type_t) String() string {
	var (
		s string
	)

	switch t {
	case TD_TYPE_BASE_TABLE:
		s = "TD_TYPE_BASE_TABLE"
	case TD_TYPE_INDEX_TABLE:
		s = "TD_TYPE_INDEX_TABLE"
	default:
		panic("impossible")
	}

	return s
}

type Td_index_type_t uint8

const (
	TD_PRIMARY_KEY Td_index_type_t = 1 << iota
	TD_UNIQUE
	TD_INDEX // physically, an index of type TD_INDEX is made unique by appending ROWID column
)

func (index_type Td_index_type_t) String() string {

	switch index_type {
	case TD_PRIMARY_KEY:
		return "TD_PRIMARY_KEY"
	case TD_UNIQUE:
		return "TD_UNIQUE"
	case TD_INDEX:
		return "TD_INDEX"
	default:
		panic("impossible")
	}
}

type Td_cluster_type_t uint8

const (
	TD_CLUSTER_TYPE_UNDEFINED Td_cluster_type_t = 1 + iota // used only during parsing step and for PRIMARY KEY only, but is replaced as soon as possible by TD_CLUSTERED or TD_NONCLUSTERED.
	TD_CLUSTERED
	TD_NONCLUSTERED // value of TD_NONCLUSTERED must be > TD_CLUSTERED, because it is used by Show_list_of_indexdefs for sorting clustered index first
)

func (cluster_type Td_cluster_type_t) String() string {

	switch cluster_type {
	case TD_CLUSTER_TYPE_UNDEFINED:
		return "TD_CLUSTER_TYPE_UNDEFINEDY"
	case TD_CLUSTERED:
		return "TD_CLUSTERED"
	case TD_NONCLUSTERED:
		return "TD_NONCLUSTERED"
	default:
		panic("impossible")
	}
}

type Td_status_t uint8

const (
	TD_STATUS_BEING_CREATED Td_status_t = iota
	TD_STATUS_VALID
	TD_STATUS_BEING_DROPPED
	TD_STATUS_CORRUPTED
)

func (status Td_status_t) String() string {

	switch status {
	case TD_STATUS_BEING_CREATED:
		return "TD_STATUS_BEING_CREATED"
	case TD_STATUS_VALID:
		return "TD_STATUS_VALID"
	case TD_STATUS_BEING_DROPPED:
		return "TD_STATUS_BEING_DROPPED"
	case TD_STATUS_CORRUPTED:
		return "TD_STATUS_CORRUPTED"
	default:
		panic("impossible")
	}
}

type GTabledef struct {
	Gtblid int64

	Gdbid                int64
	Gschid               int64
	Gtable_name          string // table name
	Gtable_name_original string // original cased, for display usage

	Base     *Tabledef            // CLUSTERED base table (for base table, Td_tblid == Td_base_gtblid == Gtblid)
	Indexmap map[string]*Tabledef // contains all NONCLUSTERED index tables
	Colmap   map[string]*Coldef

	Identity_seed      int64
	Identity_increment int64 // if increment not 0, a column has the property Cd_autonum==CD_AUTONUM_IDENTITY

	Create_date time.Time // creation time
}

func New_GTabledef() *GTabledef {

	gtabledef := &GTabledef{}
	gtabledef.Indexmap = make(map[string]*Tabledef, SPEC_GTABLEDEF_INDEXMAP_DEFAULT_SIZE)
	gtabledef.Colmap = make(map[string]*Coldef, SPEC_GTABLEDEF_COLMAP_DEFAULT_SIZE)

	return gtabledef
}

func (gtabledef *GTabledef) Has_IDENTITY_column() bool {

	return gtabledef.Identity_increment != 0
}

// Get_any_invalid_Tabledef returns the first non TD_STATUS_VALID Tabledef.
// Returs nil if all Tabledefs are TD_STATUS_VALID.
//
func (gtabledef *GTabledef) Get_any_invalid_Tabledef() *Tabledef {

	if gtabledef.Base.Td_status != TD_STATUS_VALID {
		return gtabledef.Base
	}

	for _, indexdef := range gtabledef.Indexmap {
		if indexdef.Td_status != TD_STATUS_VALID {
			return indexdef
		}
	}

	return nil
}

func (gtabledef *GTabledef) Clone() *GTabledef {
	var (
		gtabledef2     *GTabledef
		base_tabledef  *Tabledef
		base_tabledef2 *Tabledef
	)

	// create copy of gtabledef

	gtabledef2 = &GTabledef{}

	gtabledef2.Gtblid = gtabledef.Gtblid

	gtabledef2.Gdbid = gtabledef.Gdbid
	gtabledef2.Gschid = gtabledef.Gschid
	gtabledef2.Gtable_name = gtabledef.Gtable_name
	gtabledef2.Gtable_name_original = gtabledef.Gtable_name_original

	gtabledef2.Base = nil
	gtabledef2.Indexmap = make(map[string]*Tabledef, len(gtabledef.Indexmap))
	gtabledef2.Colmap = make(map[string]*Coldef, len(gtabledef.Colmap))

	gtabledef2.Identity_seed = gtabledef.Identity_seed
	gtabledef2.Identity_increment = gtabledef.Identity_increment

	gtabledef2.Create_date = gtabledef.Create_date

	// clone base_tabledef

	base_tabledef = gtabledef.Base
	base_tabledef2 = &Tabledef{}
	gtabledef2.Base = base_tabledef2

	*base_tabledef2 = *base_tabledef
	base_tabledef2.Td_coldefs = make([]*Coldef, len(base_tabledef.Td_coldefs))
	base_tabledef2.Td_nk = make([]*Coldef, len(base_tabledef.Td_nk))
	base_tabledef2.Td_payload = nil

	for i, coldef := range base_tabledef.Td_coldefs {
		coldef2 := coldef.Clone()
		base_tabledef2.Td_coldefs[i] = coldef2

		gtabledef2.Colmap[coldef2.Cd_colname] = coldef2
	}

	for i, coldef := range base_tabledef.Td_nk {
		base_tabledef2.Td_nk[i] = base_tabledef2.Td_coldefs[coldef.Cd_base_seqno]
	}

	// clone all indexes

	for _, indexdef := range gtabledef.Indexmap {
		indexdef2 := &Tabledef{}

		*indexdef2 = *indexdef
		indexdef2.Td_coldefs = make([]*Coldef, len(indexdef.Td_coldefs))
		indexdef2.Td_nk = make([]*Coldef, len(indexdef.Td_nk))
		indexdef2.Td_payload = make([]*Coldef, len(indexdef.Td_payload))

		for i, coldef := range indexdef.Td_coldefs {
			indexdef2.Td_coldefs[i] = base_tabledef2.Td_coldefs[coldef.Cd_base_seqno]
		}

		for i, coldef := range indexdef.Td_nk {
			indexdef2.Td_nk[i] = base_tabledef2.Td_coldefs[coldef.Cd_base_seqno]
		}

		for i, coldef := range indexdef.Td_payload {
			indexdef2.Td_payload[i] = base_tabledef2.Td_coldefs[coldef.Cd_base_seqno]
		}

		gtabledef2.Indexmap[indexdef2.Td_index_name] = indexdef2
	}

	return gtabledef2
}

// Description of physical storage, for a table or index.
//
//    A nonclustered index is a real index, and is physically like a normal table :
//      - native key is made of columns from the index key list. If not PRIMARY KEY or UNIQUE, ROWID column is included, as native key must be unique.
//      - it is followed by the native key columns of the base table, unless the column already exists in native key of this index.
//
//    The clustered index is the base table, which contains all columns. Records are physically sorted by the native key.
//      - native key is made of columns from the index key list. If not PRIMARY KEY or UNIQUE, ROWID column is included, as native key must be unique.
//
//
// PERMANENT TABLES and FLASHTABLES
//
//    If Td_tblid >= 0: ordinary table
//                      permanent which definition (GTabledef) is stored in master.db.
//                      Use wcache.wpc_journal for modifications, which are copied at commit to the table data file.
//
//    If Td_tblid <  0: flashtable (variable table (DECLARE @a TABLE) or grouptable or sorttable).
//                      temporary table, which is created during parsing stage, and deleted when batch terminates.
//                      Td_tblid is unique only in the batch.
//                      GTabledef for variable table (not grouptable nor sorttable) is put in parser.Temptable_bag during batch duration.
//                      Use wcache.wpc_flashjournal, and no table data file. Flashtables are never committed, but only live in the logfile.
//                      In GTabledef and Tabledef, Td_dbid is trashdb and Td_schid is dbo. This is because some functions could use these info for display usage, but it should not happen.
//                           So, there is no need to lock trashdb nor the flashtable when a flashtable is used.
//
type Tabledef struct {
	Td_tblid int64

	Td_dbid                int64
	Td_schid               int64
	Td_base_gtblid         int64 // for base table, same as Td_tblid. For index, base table tblid.
	Td_index_name          string
	Td_index_name_original string

	Td_file_path string // for permanent table only, not for flashtable

	Td_durability   Durability_t      // DURABILITY_PERMANENT, DURABILITY_TEMPORARY, DURABILITY_FLASH
	Td_type         Td_type_t         // TD_TYPE_BASE_TABLE, TD_TYPE_INDEX_TABLE
	Td_index_type   Td_index_type_t   // TD_UNIQUE, TD_PRIMARY_KEY, TD_INDEX
	Td_cluster_type Td_cluster_type_t // TD_CLUSTERED, TD_NONCLUSTERED, TD_CLUSTER_TYPE_UNDEFINED (exists only temporarily in parsing stage). After parsing, TD_TYPE_BASE_TABLE is always TD_CLUSTERED, and TD_TYPE_INDEX_TABLE is always TD_NONCLUSTERED.

	Td_coldefs []*Coldef // list of all columns stored in physical storage, in physical order
	Td_nk      []*Coldef // list of columns making up the native key of this table. If TD_INDEX, the column rowid is appended to make the native key unique.
	Td_payload []*Coldef // only for TD_TYPE_INDEX_TABLE: list of columns making up the native key of the base table.

	Td_status Td_status_t // TD_STATUS_BEING_CREATED, TD_STATUS_VALID, TD_STATUS_BEING_DROPPED, TD_STATUS_CORRUPTED
}

func New_Tabledef(table_type Td_type_t) *Tabledef {

	tabledef := &Tabledef{}
	tabledef.Td_coldefs = make([]*Coldef, 0, SPEC_TABLEDEF_COLDEFS_SLICE_DEFAULT_CAPACITY)
	tabledef.Td_nk = make([]*Coldef, 0, SPEC_TABLEDEF_NK_SLICE_DEFAULT_CAPACITY)

	if table_type == TD_TYPE_INDEX_TABLE {
		tabledef.Td_payload = make([]*Coldef, 0, SPEC_TABLEDEF_PAYLOAD_SLICE_DEFAULT_CAPACITY)
	}

	tabledef.Td_type = table_type

	return tabledef
}

func (base_tabledef *Tabledef) Nk_is_rowid() bool {

	Assert(base_tabledef.Td_type == TD_TYPE_BASE_TABLE)

	if len(base_tabledef.Td_nk) == 1 && base_tabledef.Td_nk[0].Cd_colname == ROWID {
		Assert(base_tabledef.Td_nk[0].Cd_autonum == CD_AUTONUM_ROWID)
		return true
	}

	return false
}

type Cd_autonum_t uint8

const (
	CD_AUTONUM_NONE     Cd_autonum_t = 0 // CD_AUTONUM_NONE must be 0
	CD_AUTONUM_ROWID    Cd_autonum_t = 1 // must be power of two
	CD_AUTONUM_IDENTITY Cd_autonum_t = 2 //   same
)

type Coldef struct {
	Cd_colname          string
	Cd_colname_original string // original cased, for display usage

	Cd_datatype           Datatype_t
	Cd_precision          uint16
	Cd_scale              uint16
	Cd_fixlen_flag        bool
	Cd_collation_strength Collstrength_t // for VARCHAR, always COLLSTRENGTH_1
	Cd_collation          string         // for VARCHAR, never ""

	Cd_autonum       Cd_autonum_t // CD_AUTONUM_NONE, CD_AUTONUM_ROWID, CD_AUTONUM_IDENTITY
	Cd_NOT_NULL_flag bool         // false if NULL allowed, else true

	Cd_base_seqno uint16 // seqno of the base table
}

func (coldef *Coldef) Exists_in(coldef_list []*Coldef) bool {

	for _, c := range coldef_list {
		if coldef == c {
			return true
		}
	}

	return false
}

func (coldef *Coldef) Clone() *Coldef {

	coldef2 := &Coldef{}

	*coldef2 = *coldef

	return coldef2
}

func Keyname2coldef_list(gtabledef *GTabledef, keyname_list []string) ([]*Coldef, *Error) {
	var (
		coldef_list []*Coldef
		coldef      *Coldef
		ok          bool
	)

	coldef_list = make([]*Coldef, len(keyname_list))

	for i, keyname := range keyname_list {

		if coldef, ok = gtabledef.Colmap[keyname]; ok == false {
			return nil, New_Error(ERROR_DICT, ERROR_INVALID_SYNTAX_INDEX_KEY_NOT_FOUND, ERROR_BATCH_ABORT, keyname)
		}

		if coldef.Exists_in(coldef_list) {
			return nil, New_Error(ERROR_DICT, ERROR_INVALID_SYNTAX_INDEX_DUPLICATE_KEY, ERROR_BATCH_ABORT, keyname)
		}

		coldef_list[i] = coldef
	}

	return coldef_list, nil
}

func (gtabledef *GTabledef) Primary_key() (tabledef_pk *Tabledef) {

	if gtabledef.Base.Td_index_type == TD_PRIMARY_KEY {
		return gtabledef.Base
	}

	for _, indexdef := range gtabledef.Indexmap {
		if indexdef.Td_index_type == TD_PRIMARY_KEY {
			return indexdef
		}
	}

	return nil
}

// Modify_native_key changes the native key of gtabledef.
// It modifies all indexes, so all Tabledefs in gtabledef are modified.
//
//      *** IMPORTANT *** If error, gtabledef is in a garbage state. YOU MUST ALWAYS PASS A CLONE OF gtabledef AS RECEIVER, so that you can discard it if the function returns an error.
//
//                        Also note that the tblid of all Tabledefs are not changed, but they are useless, as CREATE TABLE will overwrite them with new values.
//
func (gtabledef *GTabledef) Modify_native_key(index_name string, index_name_original string, index_type Td_index_type_t, keyname_list []string) *Error {
	var (
		rsql_err      *Error
		base_tabledef *Tabledef
		coldef_list   []*Coldef
	)

	base_tabledef = gtabledef.Base
	Assert(base_tabledef.Td_payload == nil)

	if index_type == TD_INDEX {
		Assert(keyname_list[len(keyname_list)-1] == ROWID)
	}

	// change native key in base_tabledef

	if coldef_list, rsql_err = Keyname2coldef_list(gtabledef, keyname_list); rsql_err != nil {
		return rsql_err
	}

	if index_type == TD_PRIMARY_KEY { // check that columns of primary key have NOT NULL clause
		for _, coldef := range coldef_list {
			if coldef.Cd_NOT_NULL_flag == false {
				return New_Error(ERROR_DICT, ERROR_INVALID_SYNTAX_PRIMARY_KEY_COLUMNS_MUST_BE_NOT_NULL, ERROR_BATCH_ABORT, coldef.Cd_colname)
			}
		}

		if tabledef_pk := gtabledef.Primary_key(); tabledef_pk != nil { // check that no PRIMARY KEY already exists
			return New_Error(ERROR_DICT, ERROR_MASTER_PRIMARY_KEY_ALREADY_EXISTS, ERROR_BATCH_ABORT, tabledef_pk.Td_index_name)
		}
	}

	base_tabledef.Td_index_type = index_type

	// base_tabledef.Td_coldefs is left unchanged
	base_tabledef.Td_nk = coldef_list // coldef_list replace old Td_nk

	// change index_name

	base_tabledef.Td_index_name = index_name
	base_tabledef.Td_index_name_original = index_name_original

	// change payload in all indexes

	for _, indexdef := range gtabledef.Indexmap {
		Assert(indexdef.Td_cluster_type == TD_NONCLUSTERED)

		// indexdef.Td_nk is left unchanged. indexdef.Td_coldefs and indexdef.Td_payload are recreated.

		indexdef.Td_coldefs = make([]*Coldef, 0, SPEC_TABLEDEF_COLDEFS_SLICE_DEFAULT_CAPACITY)
		indexdef.Td_payload = make([]*Coldef, 0, SPEC_TABLEDEF_PAYLOAD_SLICE_DEFAULT_CAPACITY)

		indexdef.Td_coldefs = append(indexdef.Td_coldefs[:0], indexdef.Td_nk...)

		for _, coldef_nk := range base_tabledef.Td_nk { // for all columns in native key of base table
			indexdef.Td_payload = append(indexdef.Td_payload, coldef_nk) // put it into payload

			if coldef_nk.Exists_in(indexdef.Td_coldefs) == false { // put it into coldefs list, only if not already exists
				indexdef.Td_coldefs = append(indexdef.Td_coldefs, coldef_nk)
			}
		}
	}

	return nil
}

// GTabledefs_by_name is used to sort a list of *GTabledefs by alphabetical order.
//
type GTabledefs_by_name struct { // implements Sort interface
	lst  []*GTabledef
	coll *collate.Collator
}

func New_GTabledefs_by_name(lst []*GTabledef, coll *collate.Collator) *GTabledefs_by_name {

	return &GTabledefs_by_name{lst: lst, coll: coll}
}

func (list *GTabledefs_by_name) Len() int {

	return len(list.lst)
}

func (list *GTabledefs_by_name) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list *GTabledefs_by_name) Less(i, j int) bool {
	var (
		res int
	)

	res = list.coll.CompareString(list.lst[i].Gtable_name, list.lst[j].Gtable_name)

	if res < 0 {
		return true
	}

	return false
}

// Indexes_by_name is used to sort a list of index *Tabledefs by alphabetical order.
//
type Indexes_by_name struct {
	lst  []*Tabledef
	coll *collate.Collator
}

func New_Indexes_by_name(lst []*Tabledef, coll *collate.Collator) *Indexes_by_name {

	return &Indexes_by_name{lst: lst, coll: coll}
}

func (list *Indexes_by_name) Len() int {

	return len(list.lst)
}

func (list *Indexes_by_name) Swap(i, j int) {

	list.lst[i], list.lst[j] = list.lst[j], list.lst[i]
}

func (list *Indexes_by_name) Less(i, j int) bool {
	var (
		res int
	)

	res = list.coll.CompareString(list.lst[i].Td_index_name, list.lst[j].Td_index_name)

	if res < 0 {
		return true
	}

	return false
}

//================ temporary tables bag ================

// GTabledef for variable table (not grouptable nor sorttable) is put in parser.Temptable_bag during batch duration.
type Temptable_bag struct {
	bag map[string]*GTabledef
}

func New_Temptable_bag() *Temptable_bag {

	return &Temptable_bag{}
}

func (tablebag *Temptable_bag) Bag() map[string]*GTabledef {

	return tablebag.bag
}

func (tablebag *Temptable_bag) Put(tblname string, gtabledef *GTabledef) *Error {

	if tablebag.bag == nil {
		tablebag.bag = make(map[string]*GTabledef, 5)
	}

	if _, ok := tablebag.bag[tblname]; ok == true {
		return New_Error(ERROR_SQL_SYNTAX, ERROR_TEMPORARY_TABLE_ALREADY_EXISTS, ERROR_BATCH_ABORT, tblname)
	}

	tablebag.bag[tblname] = gtabledef

	return nil
}

func (tablebag *Temptable_bag) Get(tblname string) (*GTabledef, *Error) {
	var (
		gtabledef *GTabledef
		ok        bool
	)

	if gtabledef, ok = tablebag.bag[tblname]; ok == false {
		return nil, New_Error(ERROR_SQL_SYNTAX, ERROR_TEMPORARY_TABLE_NOT_FOUND, ERROR_BATCH_ABORT, tblname)
	}

	return gtabledef, nil
}

func (tablebag *Temptable_bag) Exists(tblname string) bool {

	if _, ok := tablebag.bag[tblname]; ok == false {
		return false
	}

	return true
}

func (tablebag *Temptable_bag) Remove(tblname string) {

	delete(tablebag.bag, tblname)
}
