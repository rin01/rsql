// +build !trial

package rsql

import (
	"math"
)

const (
	MAX_UINT64 = math.MaxUint64
	MAX_INT64  = math.MaxInt64
	MIN_INT64  = math.MinInt64
)

const (
	MASTER_PATH     = "file:master.db" // for available modes, see https://www.sqlite.org/c3ref/open.html
	MASTER_PATH_RW  = MASTER_PATH + "?mode=rw"
	MASTER_PATH_RWC = MASTER_PATH + "?mode=rwc"
)

const SPEC_ERROR_PRINT_BACKTRACE bool = false // if true, backtrace will be output by this function

const DEFAULT_MESSAGE_LOCALE = "en_us"

const TRASHDB = "trashdb"

const ROWID = "rowid"
const ROWID_INTERNAL = "$rowid$" // rowid column name for grouptable and sorttable

const (
	SPEC_GTABLEDEF_INDEXMAP_DEFAULT_SIZE = 4
	SPEC_GTABLEDEF_COLMAP_DEFAULT_SIZE   = 20

	SPEC_TABLEDEF_COLDEFS_SLICE_DEFAULT_CAPACITY = SPEC_GTABLEDEF_COLMAP_DEFAULT_SIZE
	SPEC_TABLEDEF_NK_SLICE_DEFAULT_CAPACITY      = 6
	SPEC_TABLEDEF_PAYLOAD_SLICE_DEFAULT_CAPACITY = SPEC_GTABLEDEF_COLMAP_DEFAULT_SIZE

	SPEC_TABLE_NUMBER_OF_COLUMNS_MAX      = 1024 // DON'T CHANGE IT, because it changes the layout of cache.Page.
	SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX = 11   // DON'T CHANGE IT, because it changes the layout of cache.Page. TODO mettre à 20
	SPEC_TABLE_NUMBER_OF_INDEXES_MAX      = 10   // TODO mettre à 15
)

const (
	DEFAULT_BYTE_BUFF_CAPACITY    = 50 // a good value for general purpose buffer []byte capacity
	DEFAULT_COLDEF_SLICE_CAPACITY = 10 // a good value for default capacity []*Coldef
)
