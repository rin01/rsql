package mdat

import (
	"errors"
	"time"

	"rsql"
)

// MyDatetime contains a value in the range [0001-01-01 00:00:00 ... 10000-01-01 00:00:00[

// note: number of seconds between 0001-01-01 and 10000-01-01 is 315'537'897'600 seconds.

const (
	SECONDS_PER_DAY  = 24 * 3600     // 86400
	SECONDS_PER_WEEK = 24 * 3600 * 7 // 604800

	YEAR_LOWEST       = 1
	YEAR_MAX_SENTINEL = 10000

	UNIX_SEC_LOWEST         = -62135596800 // -719162 days * 24 * 3600
	UNIX_SEC_1900_01_01     = -2208988800  // -25567 days * 24 * 3600
	UNIX_SEC_1970_01_01     = 0
	UNIX_SEC_UPPER_SENTINEL = 253402300800 // 2932897 days * 24 * 3600

	NUMBER_OF_DAYS_FROM_1900_01_01_TO_LOWEST         = -693595 // dateadd(dy, -693595, '19000101') =  '0001-01-01'
	NUMBER_OF_DAYS_FROM_1900_01_01_TO_UPPER_SENTINEL = 2958464 // dateadd(dy, 2958464, '19000101') = '10000-01-01'
	NUMBER_OF_DAYS_FROM_LOWEST_TO_UPPER_SENTINEL     = NUMBER_OF_DAYS_FROM_1900_01_01_TO_UPPER_SENTINEL - NUMBER_OF_DAYS_FROM_1900_01_01_TO_LOWEST
)

var (
	TIME_LOWEST              time.Time // 0001.01.01
	TIME_1900_01_01          time.Time
	TIME_1900_01_02_SENTINEL time.Time // used by Sysfunc_dateadd_hour_TIME() etc
	TIME_1970_01_01          time.Time
	TIME_UPPER_SENTINEL      time.Time // 10000.01.01
)

// MyDatetime represents the value stored in DATE, TIME, and DATETIME datatypes.
//
//     Note: it has not been declared as "type MyDatetime time.Time" because we want to use the time.Time methods Equal(), After(), etc directly on MyDatetime.
//           This way, we avoid type conversion syntax in our code, like time.Time(a) where a is of type MyDatetime.
//
type MyDatetime struct {
	time.Time
}

func (a MyDatetime) String() string {

	return a.Format("2006-01-02 15:04:05.999999999")
}

func Equal(a MyDatetime, b MyDatetime) bool {

	return a.Equal(b.Time)
}

func init() {

	TIME_LOWEST = time.Date(1, time.January, 1, 0, 0, 0, 0, time.UTC)
	TIME_1900_01_01 = time.Date(1900, time.January, 1, 0, 0, 0, 0, time.UTC)
	TIME_1900_01_02_SENTINEL = time.Date(1900, time.January, 2, 0, 0, 0, 0, time.UTC)
	TIME_1970_01_01 = time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC)
	TIME_UPPER_SENTINEL = time.Date(10000, time.January, 1, 0, 0, 0, 0, time.UTC)

	rsql.Assert(UNIX_SEC_LOWEST == TIME_LOWEST.Unix())
	rsql.Assert(UNIX_SEC_1900_01_01 == TIME_1900_01_01.Unix())
	rsql.Assert(UNIX_SEC_1970_01_01 == TIME_1970_01_01.Unix())
	rsql.Assert(UNIX_SEC_UPPER_SENTINEL == TIME_UPPER_SENTINEL.Unix())

	rsql.Assert(NUMBER_OF_DAYS_FROM_1900_01_01_TO_LOWEST == (UNIX_SEC_LOWEST-UNIX_SEC_1900_01_01)/SECONDS_PER_DAY)
	rsql.Assert(NUMBER_OF_DAYS_FROM_1900_01_01_TO_UPPER_SENTINEL == (UNIX_SEC_UPPER_SENTINEL-UNIX_SEC_1900_01_01)/SECONDS_PER_DAY)
}

// Has_no_time_part checks if time part is 00:00:00.0.
//
func (mydatetime MyDatetime) Has_no_time_part() bool {
	var (
		ns        int64
		unix_time int64
	)

	ns = int64(mydatetime.Nanosecond())

	if ns != 0 {
		return false
	}

	unix_time = mydatetime.Unix() // seconds elapsed since January 1, 1970 UTC

	if unix_time%SECONDS_PER_DAY != 0 {
		return false
	}

	return true
}

func Last_day_of_month(year int, month time.Month) int {

	if month == time.February {
		if year%4 == 0 && (year%100 != 0 || year%400 == 0) { // leap year
			return 29
		}
		return 28
	}

	if month <= time.July {
		month++
	}

	if month&0x0001 == 0 {
		return 31
	}

	return 30
}

func Add_month_duration(year int, month time.Month, day int, month_duration int64) (result_year int, result_month time.Month, result_day int, rsql_err *rsql.Error) {
	var (
		target_last_day_of_month        int
		month_index_from_0000_00_00_new int64 // 0-based
		month_0based_new                int64
		month_new                       time.Month
		year_new                        int64
		day_new                         int
	)

	// compute target year and month

	month_index_from_0000_00_00_new = int64(year)*12 + int64(month-1) + month_duration // 0-based

	year_new = month_index_from_0000_00_00_new / 12
	month_0based_new = month_index_from_0000_00_00_new - year_new*12 // 0-based    0...11

	// check year overflow

	if year_new >= YEAR_MAX_SENTINEL || year_new < YEAR_LOWEST {
		return 0, 0, 0, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
	}

	month_new = time.Month(month_0based_new + 1)

	// check that day is valid for target month

	day_new = day
	if day > 28 {
		target_last_day_of_month = Last_day_of_month(int(year_new), month_new)

		if day > target_last_day_of_month {
			day_new = target_last_day_of_month
		}
	}

	return int(year_new), month_new, day_new, nil
}

func Add_second_duration(mydatetime MyDatetime, second_duration int64) (result time.Time, rsql_err *rsql.Error) {
	var (
		ns          int64
		res         time.Time
		unix_time   int64
		unix_result int64
	)

	unix_time = mydatetime.Unix() // seconds elapsed since January 1, 1970 UTC
	ns = int64(mydatetime.Nanosecond())

	if second_duration >= UNIX_SEC_UPPER_SENTINEL-unix_time || second_duration < UNIX_SEC_LOWEST-unix_time {
		return time.Time{}, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
	}

	unix_result = unix_time + second_duration

	res = time.Unix(unix_result, ns).UTC()

	return res, nil
}

func Add_nanosecond_duration(mydatetime MyDatetime, nanosecond_duration int64) (result time.Time, rsql_err *rsql.Error) {
	var (
		res time.Time
	)

	res = mydatetime.Add(time.Duration(nanosecond_duration))

	if res.Before(TIME_LOWEST) || !res.Before(TIME_UPPER_SENTINEL) {
		return time.Time{}, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
	}

	return res, nil
}

// Diff_sec_nsec_absolute returns the difference in seconds and nanoseconds between two dates.
// delta_sec and delta_nsec are always positive or zero.
// If b >= a, sign is 1. Else, sign is -1.
//
func Diff_sec_nsec_absolute(a time.Time, b time.Time) (delta_sec int64, delta_nsec int64, sign int) {
	var (
		a_sec  int64
		b_sec  int64
		a_nsec int64
		b_nsec int64
	)

	b_sec = b.Unix() - UNIX_SEC_LOWEST // seconds since 0001-01-01
	b_nsec = int64(b.Nanosecond())
	a_sec = a.Unix() - UNIX_SEC_LOWEST // seconds since 0001-01-01
	a_nsec = int64(a.Nanosecond())

	// default case:  b >= a

	delta_sec = b_sec - a_sec // positive or zero. Max val is < 315,537,897,600 seconds
	delta_nsec = b_nsec - a_nsec
	sign = 1

	// if b < a, recompute

	if b.Before(a) {
		delta_sec = a_sec - b_sec // positive or zero. Max val is < 315,537,897,600 seconds
		delta_nsec = a_nsec - b_nsec
		sign = -1
	}

	// adjust delta values

	if delta_nsec < 0 {
		delta_nsec += 1e9
		delta_sec--
	}

	rsql.Assert(delta_sec >= 0 && delta_nsec >= 0)

	return delta_sec, delta_nsec, sign
}

// read_unsigned_int64 reads digits from s.
// If no digit read, returns res == -1 and read_digit_count == 0. If res == -1, read_digit_count == 0.
// Leading spaces are skipped.
//
//       It is forbidden to pass max_digit_counts > 18, to avoid int64 overflow.
//
func read_unsigned_int64(s []byte, max_digit_count int) (remaining []byte, res int64, read_digit_count int) {
	var (
		max_count_limit int
		i               int
		c               byte
		total           int64
	)

	if max_digit_count > 18 { // should never happen, as we always pass very small max_digit_count values
		panic("max_digit_count cannot be >= 18, to avoid int64 overflow")
	}

	// skip blanks

	remaining = s

	for i = 0; i < len(remaining); i++ {
		if remaining[i] != ' ' {
			break
		}
	}
	remaining = remaining[i:]

	// read digits

	max_count_limit = max_digit_count
	if max_count_limit > len(remaining) {
		max_count_limit = len(remaining)
	}

	for i = 0; i < max_count_limit; i++ {
		c = remaining[i]
		if c < '0' || c > '9' {
			break
		}
		total = total*10 + (int64(c) - '0')
	}
	remaining = remaining[i:]

	if i == 0 {
		total = -1 // no digit found
	}

	return remaining, total, i
}

// read_separator reads next separator, which can be '.', '/', '-', ':'.
// The function returns remaining string and separator character.
// If no separator found, returns -1.
// Leading spaces are skipped.
//
func read_separator(s []byte) (remaining []byte, separator rune) {
	var (
		c byte
		i int
	)

	// skip blanks

	remaining = s

	for i = 0; i < len(remaining); i++ {
		if remaining[i] != ' ' {
			break
		}
	}
	remaining = remaining[i:]

	// read separator

	if len(remaining) == 0 { // separator not found
		return nil, -1
	}

	c = remaining[0]

	if c == '.' || c == '/' || c == '-' || c == ':' {
		remaining = remaining[1:] // skip separator
		return remaining, rune(c)
	}

	return remaining, -1
}

func skip_blanks(s []byte) (remaining []byte) {
	var (
		i int
	)

	// skip blanks

	for i = 0; i < len(s); i++ {
		if s[i] != ' ' {
			break
		}
	}

	return s[i:]
}

var (
	Error_style_not_supported = errors.New("style not supported")
	Error_wrong_parts_order   = errors.New("wrong date parts order")
	Error_parse_failed        = errors.New("mydatetime.parse(): datetime parsing error")
)

// DMY_from_style is used for parsing date, to convert a style number into a year month day order.
//
func DMY_from_style(style int64) (string, error) {

	switch style {
	case 0:
		panic("impossible")

	case 101, 110:
		return "MDY", nil

	case 103, 104, 105:
		return "DMY", nil

	case 102, 111, 120, 121:
		return "YMD", nil

	default:
		return "", Error_style_not_supported
	}

}

const (
	PARSE_MODE_DATE     int = 1 // in this mode, time part is ignored and forced to 00:00:00.0
	PARSE_MODE_TIME     int = 2 // in this mode, date part is ignored and forced to 1900-01-01
	PARSE_MODE_DATETIME int = 3
)

// Parse converts a string into a time.Time.
//
//     DMY   : 3-letter string describing order of date parts. E.g. "MDY".
//     style : if 0, order of date parts is given by DMY argument. Else, style will override DMY argument. See DMY_from_style().
//     mode  : PARSE_MODE_DATE, PARSE_MODE_TIME, PARSE_MODE_DATETIME
//
// Parse returns an 'error' type and not a *rsql.Error, because the caller will create a customized *rsql.Error.
//
func Parse(s []byte, DMY string, style int64, mode int) (res time.Time, err error) {
	var (
		style_DMY             string
		val_1                 int64
		val_2                 int64
		val_3                 int64
		read_digit_count      int
		digitcount_1          int
		digitcount_3          int
		year                  int64
		month                 int64
		day                   int64
		hour                  int64
		minute                int64
		seconds               int64
		fractions             int64
		fractions_digit_count int
		i                     int
		remaining             []byte
		rem_test              []byte
		date_sep              rune
		date_sep_2nd          rune
		time_sep              rune
	)

	if mode == PARSE_MODE_TIME && style != 0 { // when parsing time, only style == 0 is allowed
		return time.Time{}, Error_style_not_supported
	}

	year = 1900 // default date is 1900-01-01
	month = 1
	day = 1

	if remaining = skip_blanks(s); len(remaining) == 0 { // discard leading blanks. If empty string, return 1900-01-01
		return TIME_1900_01_01, nil
	}

	//===== check if string begins with a date with separator, a date made of 8 digits, or an hour =====

	rem_test = remaining // count digits
	for i = 0; i < len(rem_test); i++ {
		if rem_test[i] < '0' || rem_test[i] > '9' {
			break
		}
	}
	rem_test = rem_test[i:]

	if i == 8 {
		goto LABEL_PARSE_DATE_8_DIGITS
	}

	if i > 4 { // year, month, day or hour part cannot have more that 4 digits
		return time.Time{}, Error_parse_failed
	}

	rem_test = skip_blanks(rem_test) // skip blanks

	if len(rem_test) == 0 {
		return time.Time{}, Error_parse_failed
	}

	switch rem_test[0] { // check separator
	case '.', '/', '-':
		goto LABEL_PARSE_DATE_WITH_SEPARATOR
	case ':':
		goto LABEL_PARSE_TIME
	default:
		return time.Time{}, Error_parse_failed
	}

	//===== parse date part with separator =====
	// e.g. 2014.03.25    25.3.2014    3.25.2014

LABEL_PARSE_DATE_WITH_SEPARATOR:

	if remaining, val_1, digitcount_1 = read_unsigned_int64(remaining, 4); val_1 < 0 { // read at most 4 digits. If no digit read, -1 is returned.
		return time.Time{}, Error_parse_failed
	}

	if remaining, date_sep = read_separator(remaining); date_sep != '.' && date_sep != '/' && date_sep != '-' { // read separator
		return time.Time{}, Error_parse_failed
	}

	if remaining, val_2, _ = read_unsigned_int64(remaining, 2); val_2 < 0 { // read at most 2 digits. If no digit read, -1 is returned.
		return time.Time{}, Error_parse_failed
	}

	if remaining, date_sep_2nd = read_separator(remaining); date_sep_2nd != date_sep { // read separator
		return time.Time{}, Error_parse_failed
	}

	if remaining, val_3, digitcount_3 = read_unsigned_int64(remaining, 4); val_3 < 0 { // read at most 4 digits. If no digit read, -1 is returned.
		return time.Time{}, Error_parse_failed
	}

	if (digitcount_1 == 4 && digitcount_3 > 2) || (digitcount_3 == 4 && digitcount_1 > 2) || (digitcount_1 != 4 && digitcount_3 != 4) {
		return time.Time{}, Error_parse_failed
	}

	// style

	if style != 0 { // style overrides langinfo's DMY and forces date parts order
		if style_DMY, err = DMY_from_style(style); err != nil {
			return time.Time{}, err
		}
		DMY = style_DMY

		switch style_DMY {
		case "DMY", "MDY":
			if digitcount_3 != 4 {
				return time.Time{}, Error_wrong_parts_order
			}
		case "YMD":
			if digitcount_1 != 4 {
				return time.Time{}, Error_wrong_parts_order
			}
		default:
			panic("impossible")
		}
	}

	switch {
	case digitcount_1 == 4: // if first value is 4 digit year, always YMD
		year = val_1
		month = val_2
		day = val_3
	case digitcount_3 == 4 && DMY == "DMY":
		day = val_1
		month = val_2
		year = val_3
	case digitcount_3 == 4 && DMY == "MDY":
		month = val_1
		day = val_2
		year = val_3
	default:
		return time.Time{}, Error_parse_failed
	}

	if len(remaining) == 0 {
		goto LABEL_PROCESS_DATETIME
	}

	if remaining[0] == 'T' && date_sep == '-' && mode == PARSE_MODE_DATETIME {
		remaining = remaining[1:]
		goto LABEL_PARSE_TIME
	}

	if remaining[0] != ' ' { // blank must follow
		return time.Time{}, Error_parse_failed
	}

	remaining = skip_blanks(remaining)

	if len(remaining) == 0 {
		goto LABEL_PROCESS_DATETIME
	}

	goto LABEL_PARSE_TIME

	//===== parse date part with 8 digits =====
	// e.g. 20140325

LABEL_PARSE_DATE_8_DIGITS:

	if remaining, year, read_digit_count = read_unsigned_int64(remaining, 4); read_digit_count != 4 { // read exactly 4 digits
		return time.Time{}, Error_parse_failed
	}

	if remaining, month, read_digit_count = read_unsigned_int64(remaining, 2); read_digit_count != 2 { // read exactly 2 digits
		return time.Time{}, Error_parse_failed
	}

	if remaining, day, read_digit_count = read_unsigned_int64(remaining, 2); read_digit_count != 2 { // read exactly 2 digits
		return time.Time{}, Error_parse_failed
	}

	if len(remaining) == 0 {
		goto LABEL_PROCESS_DATETIME
	}

	if remaining[0] != ' ' { // blank must follow
		return time.Time{}, Error_parse_failed
	}

	remaining = skip_blanks(remaining)

	if len(remaining) == 0 {
		goto LABEL_PROCESS_DATETIME
	}

	goto LABEL_PARSE_TIME

	//===== parse hour =====
	// e.g. 13:30:00    13:30:00.300    13:30     4:5

LABEL_PARSE_TIME:

	if remaining, hour, _ = read_unsigned_int64(remaining, 2); hour < 0 { // read at most 2 digits. If no digit read, -1 is returned.
		return time.Time{}, Error_parse_failed
	}

	if remaining, time_sep = read_separator(remaining); time_sep != ':' { // read separator
		return time.Time{}, Error_parse_failed
	}

	if remaining, minute, _ = read_unsigned_int64(remaining, 2); minute < 0 { // read at most 2 digits. If no digit read, -1 is returned.
		return time.Time{}, Error_parse_failed
	}

	remaining, time_sep = read_separator(remaining) // skip blanks and read optional seconds separator
	if time_sep == ':' {
		if remaining, seconds, _ = read_unsigned_int64(remaining, 2); seconds < 0 { // read at most 2 digits. If no digit read, -1 is returned.
			return time.Time{}, Error_parse_failed
		}

		remaining, time_sep = read_separator(remaining) // skip blanks and read optional fractions separator
		if time_sep == '.' {
			if remaining, fractions, fractions_digit_count = read_unsigned_int64(remaining, 9); fractions < 0 { // read at most 9 digits. If no digit read, -1 is returned.
				return time.Time{}, Error_parse_failed
			}
			time_sep = -1
		}
	}

	if time_sep != -1 {
		return time.Time{}, Error_parse_failed
	}

	remaining = skip_blanks(remaining)

	if len(remaining) > 0 { // nothing should remain
		return time.Time{}, Error_parse_failed
	}

	//===== process datetime =====

LABEL_PROCESS_DATETIME:

	nanoseconds := Nanoseconds_from_fractions(fractions, int64(fractions_digit_count)) // -1 if error

	if (year >= 1 && year <= 9999 &&
		month >= 1 && month <= 12 &&
		day >= 1 && day <= int64(Last_day_of_month(int(year), time.Month(month))) &&
		hour >= 0 && hour <= 23 &&
		minute >= 0 && minute <= 59 &&
		seconds >= 0 && seconds <= 59 &&
		nanoseconds >= 0 && nanoseconds <= 999999999) == false {
		return time.Time{}, Error_parse_failed
	}

	switch mode {
	case PARSE_MODE_DATE:
		hour = 0
		minute = 0
		seconds = 0
		nanoseconds = 0
	case PARSE_MODE_TIME:
		year = 1900
		month = 1
		day = 1
	}

	// fmt.Printf("%04d-%02d-%02d %02d:%02d:%02d.%d %d\n", year, month, day, hour, minute, seconds, fractions, fractions_digit_count)

	res = time.Date(int(year), time.Month(month), int(day), int(hour), int(minute), int(seconds), int(nanoseconds), time.UTC)

	return res, nil
}

// Nanoseconds_from_fractions computes nanoseconds from fractions and precision values.
//       The following conditions must apply. Else -1 is returned, indicating an error.
//         - fractions must be 0...999999999
//         - precision must be 0...9
//         - if precision == 0, fractions must be 0
//         - nanoseconds must be 0...999999999
//
func Nanoseconds_from_fractions(fractions int64, precision int64) (nanoseconds int64) {

	// compute nanoseconds from fractions

	if fractions < 0 || fractions > 999999999 {
		return -1
	}

	switch precision {
	case 0:
		nanoseconds = 0
		if fractions != 0 {
			return -1
		}
	case 1:
		nanoseconds = fractions * 100000000
	case 2:
		nanoseconds = fractions * 10000000
	case 3:
		nanoseconds = fractions * 1000000
	case 4:
		nanoseconds = fractions * 100000
	case 5:
		nanoseconds = fractions * 10000
	case 6:
		nanoseconds = fractions * 1000
	case 7:
		nanoseconds = fractions * 100
	case 8:
		nanoseconds = fractions * 10
	case 9:
		nanoseconds = fractions

	default:
		return -1
	}

	if nanoseconds < 0 || nanoseconds > 999999999 {
		return -1
	}

	return nanoseconds
}

func Get_time_UTC() (result time.Time, rsql_err *rsql.Error) {
	var (
		res time.Time
	)

	res = time.Now().UTC()

	if res.Before(TIME_LOWEST) || !res.Before(TIME_UPPER_SENTINEL) {
		return time.Time{}, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
	}

	return res, nil
}

// a is an absolute time of an event, with location UTC.
// We want to create a time, also with location UTC, but containing the year, month, day, hour, minutes, seconds, ns as seen locally.
//
func UTC_to_local(a time.Time) (result time.Time, rsql_err *rsql.Error) {
	var (
		t   time.Time
		res time.Time
	)

	t = a.Local()

	year, month, day := t.Date()
	hour, minute, second := t.Clock()
	nanosecond := t.Nanosecond()
	res = time.Date(year, month, day, hour, minute, second, nanosecond, time.UTC)

	if res.Before(TIME_LOWEST) || !res.Before(TIME_UPPER_SENTINEL) {
		return time.Time{}, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
	}

	return res, nil
}
