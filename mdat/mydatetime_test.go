package mdat

import (
	"testing"
	"time"
)

func Test_read_unsigned_int64(t *testing.T) {

	var samples = []struct {
		in_str               string
		in_max_digit_count   int
		out_remaining        string
		out_res              int64
		out_read_digit_count int
	}{
		{"", 0, "", -1, 0},
		{"", 1, "", -1, 0},
		{"", 10, "", -1, 0},

		{" ", 0, "", -1, 0},
		{"   ", 1, "", -1, 0},
		{"      ", 10, "", -1, 0},

		{"a", 0, "a", -1, 0},
		{"a", 1, "a", -1, 0},
		{"a", 5, "a", -1, 0},
		{"    a", 5, "a", -1, 0},

		{"3a", 0, "3a", -1, 0},
		{"3a", 1, "a", 3, 1},
		{"3a", 2, "a", 3, 1},
		{"   3a", 10, "a", 3, 1},

		{"3", 0, "3", -1, 0},
		{"3", 1, "", 3, 1},
		{"3", 2, "", 3, 1},
		{"   3", 10, "", 3, 1},

		{"34a", 0, "34a", -1, 0},
		{"34a", 1, "4a", 3, 1},
		{"34a", 2, "a", 34, 2},
		{"  34a", 3, "a", 34, 2},

		{"34567a", 0, "34567a", -1, 0},
		{"34567a", 1, "4567a", 3, 1},
		{"34567a", 4, "7a", 3456, 4},
		{"34567a", 5, "a", 34567, 5},
		{"34567a", 6, "a", 34567, 5},

		{"34567", 0, "34567", -1, 0},
		{"34567", 1, "4567", 3, 1},
		{"34567", 4, "7", 3456, 4},
		{"34567", 5, "", 34567, 5},
		{"34567", 6, "", 34567, 5},
		{"   34567   ", 6, "   ", 34567, 5},

		{"00000", 0, "00000", -1, 0},
		{"00000", 1, "0000", 0, 1},
		{"00000", 4, "0", 0, 4},
		{"00000", 5, "", 0, 5},
		{"00000", 6, "", 0, 5},
		{"   00000   ", 6, "   ", 0, 5},

		{"00007", 0, "00007", -1, 0},
		{"00007", 1, "0007", 0, 1},
		{"00007", 4, "7", 0, 4},
		{"00007", 5, "", 7, 5},
		{"00007", 6, "", 7, 5},
		{"   00007   ", 6, "   ", 7, 5},

		{"9223372036854775807", 0, "9223372036854775807", -1, 0},
		{"9223372036854775807", 1, "223372036854775807", 9, 1},
		{"   999999999999999999", 18, "", 999999999999999999, 18},
		{"   987654321098765437", 18, "", 987654321098765437, 18},
	}

	for _, sample := range samples {

		remaining, res, read_digit_count := read_unsigned_int64([]byte(sample.in_str), sample.in_max_digit_count)

		if string(remaining) != sample.out_remaining || res != sample.out_res || read_digit_count != sample.out_read_digit_count {
			t.Fatalf("error during read_digit_count of \"%s\", max_digit_count %d. Got \"%s\", %d, %d, expected \"%s\", %d, %d", sample.in_str, sample.in_max_digit_count, remaining, res, read_digit_count, sample.out_remaining, sample.out_res, sample.out_read_digit_count)
		}

	}

}

func Test_read_separator(t *testing.T) {

	var samples = []struct {
		in_str        string
		out_remaining string
		out_separator rune
	}{
		{"", "", -1},
		{" ", "", -1},
		{"   ", "", -1},

		{"a", "a", -1},
		{" a", "a", -1},
		{"   a", "a", -1},

		{".", "", '.'},
		{" .", "", '.'},
		{"   .", "", '.'},

		{".a", "a", '.'},
		{" .a", "a", '.'},
		{"   .a", "a", '.'},
		{"   . abddef", " abddef", '.'},

		{"/", "", '/'},
		{" /", "", '/'},
		{"   /", "", '/'},

		{"/a", "a", '/'},
		{" /a", "a", '/'},
		{"   /a", "a", '/'},
		{"   / abddef", " abddef", '/'},

		{"-", "", '-'},
		{" -", "", '-'},
		{"   -", "", '-'},

		{"-a", "a", '-'},
		{" -a", "a", '-'},
		{"   -a", "a", '-'},
		{"   - abddef", " abddef", '-'},

		{":", "", ':'},
		{" :", "", ':'},
		{"   :", "", ':'},

		{":a", "a", ':'},
		{" :a", "a", ':'},
		{"   :a", "a", ':'},
		{"   : abddef", " abddef", ':'},
	}

	for _, sample := range samples {

		remaining, separator := read_separator([]byte(sample.in_str))

		if string(remaining) != sample.out_remaining || separator != sample.out_separator {
			t.Fatalf("error during read_separator of \"%s\". Got \"%s\", '%c', expected \"%s\", '%c'", sample.in_str, remaining, separator, sample.out_remaining, sample.out_separator)
		}

	}

}

func Test_skip_blanks(t *testing.T) {

	var samples = []struct {
		in_str        string
		out_remaining string
	}{
		{"", ""},
		{" ", ""},
		{"   ", ""},
		{"a", "a"},
		{" a", "a"},
		{"        a", "a"},
		{"        abcdefg ", "abcdefg "},
	}

	for _, sample := range samples {

		remaining := skip_blanks([]byte(sample.in_str))

		if string(remaining) != sample.out_remaining {
			t.Fatalf("error during skip_blanks of \"%s\". Got \"%s\", expected \"%s\"", sample.in_str, remaining, sample.out_remaining)
		}

	}

}

func Test_Has_no_time_part(t *testing.T) {
	var (
		err  error
		date MyDatetime
	)

	var samples = []struct {
		in_str string
		out    bool
	}{
		{"1234-01-02 13:30:25.123", false},
		{"1234-01-02 00:00:00", true},
		{"0000-01-01 00:00:00", true},
		{"0000-01-01 00:00:00.000000000", true},
		{"0000-01-01 00:00:00.000000001", false},
		{"0000-01-01 00:00:01", false},
		{"9999-12-31 23:59:58.999999999", false},
		{"9999-12-31 23:59:59", false},
		{"9999-12-31 23:59:00", false},
		{"9999-12-31 23:00:00", false},
		{"9999-12-31 00:00:01", false},
		{"9999-12-31 00:00:00", true},
		{"9999-12-31 00:00:00.000000001", false},
	}

	const format = "2006-01-02 15:04:05"

	for _, sample := range samples {

		date.Time, err = time.Parse(format, sample.in_str)
		if err != nil {
			panic("bad date " + sample.in_str)
		}

		//fmt.Println(date)

		if date.Has_no_time_part() != sample.out {
			t.Fatalf("error for %s", sample.in_str)
		}
	}

}
