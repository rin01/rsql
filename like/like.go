// Package like implements functions used by LIKE operator.
// The expression is parsed into a list of subexpressions, which are '%' placeholder, '_' placeholder, rune set [...] and "substring".
// During execution, the tested string will be compared with the list subexpressions, recursively for '%' placeholder.
//
//        E.g. if LIKE EXPRESSION is '%hello ____[abc0-9]world', the result of the parsing is the following list of Likexp.
//           Likexp LX_TYPE_ZERO_OR_MANY_CHARS     for % placeholder
//           Likexp LX_TYPE_STRING                 for "hello "
//           Likexp LX_TYPE_N_CHARS                for ____ (four _ placeholders)
//           Likexp LX_TYPE_ONE_CHAR_IN_SET        for [abc0-9]
//           Likexp LX_TYPE_STRING                 for "world"
//
//        If LIKE expression is empty string '', parsing result is a nil lxb_list in Likexp_bundle.
//
//        In the same way as equality operator (=), both operands are trimmed for trailing spaces:
//           'hello'   LIKE 'hello'               is true
//           'hello '  LIKE 'hello      '         is true
//           'hello  ' LIKE 'hello       '        is true
//
//        Usage:
//           - Parse_likexp() parses the LIKE expression, and returns a Likexp_bundle object, containing the "compiled" LIKE expression.
//           - Match() determines whether string matches the LIKE expression.
//
package like

import (
	"bytes"
	"fmt"
	"unicode/utf8"

	"golang.org/x/text/collate"
	"golang.org/x/text/unicode/norm"

	"rsql"
)

const (
	NO_ESCAPE_RUNE rune = -1 // passed as ec argument in Parse_likexp(..., NO_ESCAPE_RUNE), to disable escape character

	SPEC_MAX_PERCENT_PLACEHOLDER_COUNT int = 5  // we want to limit the number of % because % tries every possible length for remaining substring, and this can take long.
	SPEC_MAX_LIKEXP_COUNT              int = 30 // we want to avoid too complex LIKE expressions.
)

// Lx_type_t is the type of a Likexp element.
//
type Lx_type_t int

const (
	LX_TYPE_STRING              Lx_type_t = 1 << iota // literal string fragment
	LX_TYPE_N_CHARS                                   // one or many underscore placeholders _
	LX_TYPE_ONE_CHAR_IN_SET                           // rune set [ ]
	LX_TYPE_ONE_CHAR_NOT_IN_SET                       // rune set [^ ]
	LX_TYPE_ZERO_OR_MANY_CHARS                        // one or many % placeholders
	LX_TYPE_INVALID                                   // error occurred during parsing, the whole LIKE expression must be considered as invalid
)

func (t Lx_type_t) String() string {
	var s string

	switch t {
	case LX_TYPE_STRING:
		s = "LX_TYPE_STRING"
	case LX_TYPE_N_CHARS:
		s = "LX_TYPE_N_CHARS"
	case LX_TYPE_ONE_CHAR_IN_SET:
		s = "LX_TYPE_ONE_CHAR_IN_SET"
	case LX_TYPE_ONE_CHAR_NOT_IN_SET:
		s = "LX_TYPE_ONE_CHAR_NOT_IN_SET"
	case LX_TYPE_ZERO_OR_MANY_CHARS:
		s = "LX_TYPE_ZERO_OR_MANY_CHARS"
	case LX_TYPE_INVALID:
		s = "LX_TYPE_INVALID"
	default:
		panic("unknown")
	}

	return s
}

// Likexp_bundle contains the parsing result of a LIKE expression.
//
type Likexp_bundle struct {
	lxb_list                      []*Likexp // contains a list of Likexp
	lxb_percent_placeholder_count int       // count of % placeholders in LIKE expression
	lxb_parsing_failed            bool      // true if parsing failed, e.g. because an empty rune set [] has been found, or there is too many % placeholders. In this case, Match function will just always returns false.
}

func (likexp_bundle_a *Likexp_bundle) Equal(likexp_bundle_b *Likexp_bundle) bool {

	if len(likexp_bundle_a.lxb_list) != len(likexp_bundle_b.lxb_list) ||
		likexp_bundle_a.lxb_percent_placeholder_count != likexp_bundle_b.lxb_percent_placeholder_count ||
		likexp_bundle_a.lxb_parsing_failed == true || // if parsing failure, returns false
		likexp_bundle_b.lxb_parsing_failed == true { //    same
		return false
	}

	for i, likexp_a := range likexp_bundle_a.lxb_list {
		likexp_b := likexp_bundle_b.lxb_list[i]
		if likexp_a.Equal(likexp_b) == false {
			return false
		}
	}

	return true
}

func (likexp_bundle *Likexp_bundle) Hash() uint32 {
	var (
		hash uint32
	)

	if likexp_bundle.lxb_parsing_failed == true { // if parsing failed, always return same number
		return 625437 // utterly arbitrary number
	}

	for _, likexp_elem := range likexp_bundle.lxb_list {
		hash += likexp_elem.Hash()
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (likexp_bundle *Likexp_bundle) String() string {
	var (
		res = make([]byte, 0, 50)
	)
	if likexp_bundle.lxb_parsing_failed {
		return "# FAILED #"
	}

	for _, likexp := range likexp_bundle.lxb_list {
		switch likexp.lx_type {
		case LX_TYPE_STRING:
			res = append(res, likexp.lx_string...) // WARNING: special characters like _ % [ ] are written unescaped. TODO: We should escape them when we have time. But for now, this String() function is not used.
		case LX_TYPE_N_CHARS:
			for i := 0; i < likexp.lx_rune_count; i++ {
				res = append(res, '_')
			}
		case LX_TYPE_ONE_CHAR_IN_SET, LX_TYPE_ONE_CHAR_NOT_IN_SET:
			res = append(res, '[')
			if likexp.lx_type == LX_TYPE_ONE_CHAR_NOT_IN_SET {
				res = append(res, '^')
			}
			for _, v := range likexp.lx_rune_list {
				if v[0] == '-' || v[0] == '\\' || v[0] == ']' || v[0] == '^' {
					res = append(res, '\\') // we put backslash as escape char for display of special character
				}
				res = append(res, v...)
			}
			for i, v := range likexp.lx_rune_range_list_start {
				res = append(res, v...)
				res = append(res, '-')
				res = append(res, likexp.lx_rune_range_list_end[i]...)
			}
			res = append(res, ']')
		case LX_TYPE_ZERO_OR_MANY_CHARS:
			res = append(res, '%')
		default:
			panic("impossible")
		}
	}

	return string(res)
}

type Likexp struct {
	lx_type                  Lx_type_t
	lx_rune_count            int      // for LX_TYPE_STRING: rune count in string. For LX_TYPE_N_CHARS: rune count (number of '_' placeholders)
	lx_string                []byte   // for LX_TYPE_STRING string fragment
	lx_rune_list             [][]byte // for LX_TYPE_ONE_CHAR_IN_SET and LX_TYPE_ONE_CHAR_NOT_IN_SET, list of runes in the set
	lx_rune_range_list_start [][]byte // for LX_TYPE_ONE_CHAR_IN_SET and LX_TYPE_ONE_CHAR_NOT_IN_SET, list of range start runes in the set
	lx_rune_range_list_end   [][]byte // for LX_TYPE_ONE_CHAR_IN_SET and LX_TYPE_ONE_CHAR_NOT_IN_SET, list of range end runes in the set
}

func (le *Likexp) String() string {

	s := fmt.Sprintf("%s\nrune_count : %d\nstring     : <%s>\nrune_list  : %s\nrange_start: %s\nrange_end  : %s", le.lx_type.String(), le.lx_rune_count, le.lx_string, le.lx_rune_list, le.lx_rune_range_list_start, le.lx_rune_range_list_end)

	return s
}

func (le_a *Likexp) Equal(le_b *Likexp) bool {

	if le_a.lx_type != le_b.lx_type ||
		le_a.lx_rune_count != le_b.lx_rune_count ||
		len(le_a.lx_string) != len(le_b.lx_string) ||
		len(le_a.lx_rune_list) != len(le_b.lx_rune_list) ||
		len(le_a.lx_rune_range_list_start) != len(le_b.lx_rune_range_list_start) ||
		len(le_a.lx_rune_range_list_end) != len(le_b.lx_rune_range_list_end) {
		return false
	}

	if len(le_a.lx_string) > 0 && bytes.Equal(le_a.lx_string, le_b.lx_string) == false {
		return false
	}

	for i, elem_a := range le_a.lx_rune_list {
		elem_b := le_b.lx_rune_list[i]
		if bytes.Equal(elem_a, elem_b) == false {
			return false
		}
	}

	for i, elem_a := range le_a.lx_rune_range_list_start {
		elem_b := le_b.lx_rune_range_list_start[i]
		if bytes.Equal(elem_a, elem_b) == false {
			return false
		}
	}

	for i, elem_a := range le_a.lx_rune_range_list_end {
		elem_b := le_b.lx_rune_range_list_end[i]
		if bytes.Equal(elem_a, elem_b) == false {
			return false
		}
	}

	return true
}

func (le *Likexp) Hash() uint32 {
	var (
		hash uint32
	)

	hash = uint32(le.lx_type)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += uint32(le.lx_rune_count)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += rsql.Joaat_hash(le.lx_string)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	for _, elem := range le.lx_rune_list {
		hash += rsql.Joaat_hash(elem)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	for _, elem := range le.lx_rune_range_list_start {
		hash += rsql.Joaat_hash(elem)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	for _, elem := range le.lx_rune_range_list_end {
		hash += rsql.Joaat_hash(elem)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

/************************************************************************/
/*                                                                      */
/*                          parsing functions                           */
/*                                                                      */
/************************************************************************/

// Parse_likexp is the main function that parses a LIKE expression s, and write the compiled result into the input variable likexp_bundle.
// ec is the escape character. If % _  [ ^ - is preceded by the escape character, it will be considered as a normal character.
// To disable ec, pass the constant like.NO_ESCAPE_RUNE.
// An error is returned if too many % placeholders, because it is usually a mistake to put too many of them.
//
func (likexp_bundle *Likexp_bundle) Parse_likexp(s string, ec rune) (err error) {
	var (
		remaining   string
		likexp      *Likexp
		likexp_last *Likexp
		i           int
	)

	// clear likexp_bundle

	*likexp_bundle = Likexp_bundle{}

	// trim trailing spaces from LIKE expression if any.

	for i = len(s) - 1; i >= 0; i-- {
		if s[i] != ' ' {
			break
		}
	}
	s = s[:i+1]

	// parse

	if s == "" {
		return nil
	}

	remaining = s

	for {
		if likexp, remaining = parse_likexp_next_sequence(remaining, ec); likexp == nil { // read %  _  [...]  or a plain string. For LX_TYPE_STRING, lx_rune_count is 0 and will be updated below.
			break // no more likexp
		}

		// if likexp is invalid, return

		if likexp.lx_type == LX_TYPE_INVALID {
			*likexp_bundle = Likexp_bundle{lxb_parsing_failed: true}
			return nil
		}

		// if likexp is LX_TYPE_STRING and follows a LX_TYPE_STRING, concatenate it to the latter

		if likexp.lx_type == LX_TYPE_STRING && likexp_last != nil && likexp_last.lx_type == LX_TYPE_STRING {
			likexp_last.lx_string = norm.NFC.Append(likexp_last.lx_string, likexp.lx_string...) // concatenate
			continue
		}

		// append likexp to list

		if len(likexp_bundle.lxb_list) >= SPEC_MAX_LIKEXP_COUNT {
			*likexp_bundle = Likexp_bundle{lxb_parsing_failed: true}
			return fmt.Errorf("LIKE expression: too many segments in '%s'", s)
		}

		likexp_bundle.lxb_list = append(likexp_bundle.lxb_list, likexp)

		if likexp.lx_type == LX_TYPE_ZERO_OR_MANY_CHARS {
			if likexp_bundle.lxb_percent_placeholder_count >= SPEC_MAX_PERCENT_PLACEHOLDER_COUNT {
				*likexp_bundle = Likexp_bundle{lxb_parsing_failed: true}
				return fmt.Errorf("LIKE expression: too many %% placeholders in '%s'", s)
			}
			likexp_bundle.lxb_percent_placeholder_count++
		}

		likexp_last = likexp
	}

	// update lx_rune_count for LX_TYPE_STRING

	for _, likexp = range likexp_bundle.lxb_list {
		if likexp.lx_type == LX_TYPE_STRING {
			likexp.lx_rune_count = utf8.RuneCount(likexp.lx_string)
		}
	}

	return nil
}

// read_one_rune reads the first rune in s.
// If the escape character ec is read, it is skipped and the next rune is returned as plain character.
//
//     is_special is true if rune is a non-escaped '_', '%' or '['.
//     eos is true if no rune is returned because end of string.
//
func read_one_rune(s string, ec rune) (res rune, is_special bool, eos bool, remaining string) {
	var (
		c            rune
		size         int
		escaped_rune bool
	)

	// if nothing to read, just return

	if s == "" {
		return utf8.RuneError, false, true, ""
	}

	// read one plain rune, or an escaped rune

	if c, size = utf8.DecodeRuneInString(s); c == ec {
		s = s[size:]
		if s == "" {
			return utf8.RuneError, false, true, ""
		}
		c, size = utf8.DecodeRuneInString(s)
		escaped_rune = true
	}

	s = s[size:]

	// check if rune is special character

	if escaped_rune == false && (c == '_' || c == '%' || c == '[') {
		return c, true, false, s
	}

	return c, false, false, s // return normal character
}

type restype_t int

const (
	RESULT_END_OF_RUNE_SET restype_t = 0
	RESULT_RUNE            restype_t = 2
	RESULT_RANGE           restype_t = 4
)

// get_one_rune_or_range_in_set reads one rune in the rune set, or a rune range.
//
//     E.g. [abc0-9rst]   0-9 is a rune range, whereas a, b, c, r, s, t are single runes.
//
//     Returned result_type are:
//         RESULT_END_OF_RUNE_SET    if ']' has just been read, or end of string
//         RESULT_RUNE               if one rune has been read. Available in c_start
//         RESULT_RANGE              if a rune range has been read. Available in c_start and c_end
//
func get_one_rune_or_range_in_set(s string, ec rune) (c_start rune, c_end rune, result_type restype_t, remaining string) {
	var (
		eos bool
	)

	if s == "" { // if no more rune in string
		return 0, 0, RESULT_END_OF_RUNE_SET, ""
	}

	if s[0] == ']' { // if closing bracket of rune set
		return 0, 0, RESULT_END_OF_RUNE_SET, s[1:]
	}

	// read one rune

	remaining = s

	if c_start, _, eos, remaining = read_one_rune(remaining, ec); eos == true { // read one rune. All runes in a set [...] are considered as normal characters, even % _ [ ^ -
		return 0, 0, RESULT_END_OF_RUNE_SET, "" // if no more rune in string
	}

	if !(len(remaining) >= 2 && remaining[0] == '-' && remaining[1] != ']') { // if not rune range
		return c_start, 0, RESULT_RUNE, remaining
	}

	// read rune range

	remaining = remaining[1:] // skip '-' sign

	if c_end, _, eos, remaining = read_one_rune(remaining, ec); eos == true { // read rune. It cannot be ']' (see above).
		return c_start, 0, RESULT_RUNE, "" // escape character has been read, which is not followed by a rune
	}

	if c_start == c_end {
		return c_start, 0, RESULT_RUNE, remaining
	}

	return c_start, c_end, RESULT_RANGE, remaining // rune range is returned
}

// parse_likexp_next_sequence reads a sequence making up the LIKE expression.
//
//    This sequence is one of the following:
//        %        (subsequent % are discarded)
//        _        (subsequent _ are also read)
//       [...]     (a whole rune set is read)
//       hello     (a series of normal characters)
//
// If no more sequence, return nil, "".
//
// If likexp type is LX_TYPE_STRING, lx_rune_count is 0 and must be updated by the caller.
//
func parse_likexp_next_sequence(s string, ec rune) (likexp *Likexp, remaining string) {
	var (
		p                int
		c                rune
		buff             []rune
		rune_range_start rune
		rune_range_end   rune
		restype          restype_t
		is_special       bool
		eos              bool
		remaining_new    string
		string_fragment  []byte
		val_1            rune
		val_2            rune
	)

	if c, is_special, eos, remaining = read_one_rune(s, ec); eos == true {
		return nil, ""
	}

	if is_special == true {

		//=== if current rune is special character, underscore _ placeholder ===

		if c == '_' {
			p = 0
			for p < len(remaining) && remaining[p] == '_' { // eat subsequent '_'
				p++
			}
			likexp = &Likexp{lx_type: LX_TYPE_N_CHARS, lx_rune_count: p + 1}
			return likexp, remaining[p:]
		}

		//=== if current rune is special character, percent % placeholder ===

		if c == '%' {
			p = 0
			for p < len(remaining) && remaining[p] == '%' { // skip subsequent useless '%'
				p++
			}
			likexp = &Likexp{lx_type: LX_TYPE_ZERO_OR_MANY_CHARS}
			return likexp, remaining[p:]
		}

		//=== if current rune is special character, left bracket [  of "rune set" [...] ===

		if c == '[' {
			likexp = &Likexp{lx_type: LX_TYPE_ONE_CHAR_IN_SET}

			if len(remaining) == 0 { // [ at end of string is invalid Likexp
				likexp.lx_type = LX_TYPE_INVALID
				return likexp, ""
			}

			if remaining[0] == ']' { // [] is invalid Likexp
				likexp.lx_type = LX_TYPE_INVALID
				return likexp, ""
			}

			if len(remaining) >= 2 && remaining[0] == '^' && remaining[1] != ']' { // if [^...]    but not [^]
				likexp.lx_type = LX_TYPE_ONE_CHAR_NOT_IN_SET
				remaining = remaining[1:]
			}

			// here, the characters that follow make up the rune set, until ']' character or end of string

		LABEL_FOR_LOOP:
			for {
				rune_range_start, rune_range_end, restype, remaining = get_one_rune_or_range_in_set(remaining, ec)

				switch restype {
				case RESULT_END_OF_RUNE_SET:
					break LABEL_FOR_LOOP

				case RESULT_RUNE:
					for _, val := range likexp.lx_rune_list { // if duplicate, do nothing
						val_1, _ = utf8.DecodeRune(val)
						if val_1 == rune_range_start {
							continue LABEL_FOR_LOOP
						}
					}
					likexp.lx_rune_list = append(likexp.lx_rune_list, []byte(string(rune_range_start))) // append

				case RESULT_RANGE:
					for i, val := range likexp.lx_rune_range_list_start { // if duplicate, do nothing
						val_1, _ = utf8.DecodeRune(val)
						val_2, _ = utf8.DecodeRune(likexp.lx_rune_range_list_end[i])
						if val_1 == rune_range_start && val_2 == rune_range_end {
							continue LABEL_FOR_LOOP
						}
					}
					likexp.lx_rune_range_list_start = append(likexp.lx_rune_range_list_start, []byte(string(rune_range_start))) // append
					likexp.lx_rune_range_list_end = append(likexp.lx_rune_range_list_end, []byte(string(rune_range_end)))       // append
				}
			}

			// if only one character in LX_TYPE_ONE_CHAR_IN_SET, change likexp to LX_TYPE_STRING

			if likexp.lx_type == LX_TYPE_ONE_CHAR_IN_SET && len(likexp.lx_rune_list) == 1 && len(likexp.lx_rune_range_list_start) == 0 {
				likexp.lx_type = LX_TYPE_STRING
				likexp.lx_rune_count = 1
				likexp.lx_string = likexp.lx_rune_list[0]
				likexp.lx_rune_list = nil
			}

			return likexp, remaining
		}
	}

	//=== current rune is normal character, start of a string fragment ===

	for {
		buff = append(buff, c) // append character to result string

		c, is_special, eos, remaining_new = read_one_rune(remaining, ec)
		if eos == true || is_special == true { // if not normal character
			break
		}
		remaining = remaining_new
	}

	string_fragment = []byte(string(buff))

	likexp = &Likexp{lx_type: LX_TYPE_STRING, lx_string: string_fragment} // lx_rune_count will be updated by the caller

	return likexp, remaining
}

/************************************************************************/
/*                                                                      */
/*                        execution functions                           */
/*                                                                      */
/************************************************************************/

func character_is_in_set(c []byte, rune_list [][]byte, rune_range_list_start [][]byte, rune_range_list_end [][]byte, collator *collate.Collator) bool {
	var (
		r       []byte
		r_start []byte
		r_end   []byte
		i       int
	)

	// compare c with all runes in rune_list

	for _, r = range rune_list {
		if collator.Compare(c, r) == 0 {
			return true
		}
	}

	// compare c with all rune ranges in rune range list

	for i, r_start = range rune_range_list_start {
		r_end = rune_range_list_end[i]

		if collator.Compare(c, r_start) >= 0 && collator.Compare(c, r_end) <= 0 {
			return true
		}
	}

	return false
}

func match_ending_string(s []byte, suffix []byte, suffix_rune_count int, collator *collate.Collator) (remaining []byte, ok bool) {

	if suffix_rune_count == 0 {
		return s, true
	}

	if len(s) < suffix_rune_count {
		return nil, false
	}

	// get substring of s with the same rune count as suffix

	n := 0
	aux := s

	for n < suffix_rune_count && len(aux) > 0 {
		_, size := utf8.DecodeLastRune(aux)
		aux = aux[:len(aux)-size]
		n++
	}

	if n != suffix_rune_count {
		return nil, false
	}

	// compare for equality

	if collator.Compare(s[len(aux):], suffix) != 0 {
		return nil, false
	}

	return aux, true
}

func match_ending_n_runes(s []byte, rune_count int) (remaining []byte, ok bool) {

	if rune_count == 0 {
		return s, true
	}

	if len(s) < rune_count {
		return nil, false
	}

	// get substring of s with requested rune count

	n := 0
	aux := s

	for n < rune_count && len(aux) > 0 {
		_, size := utf8.DecodeLastRune(aux)
		aux = aux[:len(aux)-size]
		n++
	}

	if n != rune_count {
		return nil, false
	}

	return aux, true
}

func match_ending_rune_in_set(s []byte, rune_list [][]byte, rune_range_list_start [][]byte, rune_range_list_end [][]byte, collator *collate.Collator) (remaining []byte, ok bool) {

	if len(s) == 0 {
		return nil, false
	}

	_, size := utf8.DecodeLastRune(s)

	c := s[len(s)-size:]

	// check if c is in set

	if character_is_in_set(c, rune_list, rune_range_list_start, rune_range_list_end, collator) {
		return s[:len(s)-size], true
	}

	return nil, false
}

func match_ending_rune_not_in_set(s []byte, rune_list [][]byte, rune_range_list_start [][]byte, rune_range_list_end [][]byte, collator *collate.Collator) (remaining []byte, ok bool) {

	if len(s) == 0 {
		return nil, false
	}

	_, size := utf8.DecodeLastRune(s)

	c := s[len(s)-size:]

	// check if c is in set

	if character_is_in_set(c, rune_list, rune_range_list_start, rune_range_list_end, collator) == false {
		return s[:len(s)-size], true
	}

	return nil, false
}

func match_string(s []byte, prefix []byte, prefix_rune_count int, collator *collate.Collator) (remaining []byte, ok bool) {

	if prefix_rune_count == 0 {
		return s, true
	}

	if len(s) < prefix_rune_count {
		return nil, false
	}

	// quick path: if no match for first runes, return 'false'

	_, size_of_first_rune_in_s := utf8.DecodeRune(s)
	_, size_of_first_rune_in_prefix := utf8.DecodeRune(prefix)

	if collator.Compare(s[:size_of_first_rune_in_s], prefix[:size_of_first_rune_in_prefix]) != 0 {
		return nil, false
	}

	// get substring of s with the same rune count as prefix

	n := 0
	aux := s

	for n < prefix_rune_count && len(aux) > 0 {
		_, size := utf8.DecodeRune(aux)
		aux = aux[size:]
		n++
	}

	if n != prefix_rune_count {
		return nil, false
	}

	// compare for equality

	if collator.Compare(s[:len(s)-len(aux)], prefix) != 0 {
		return nil, false
	}

	return aux, true
}

func match_n_runes(s []byte, rune_count int) (remaining []byte, ok bool) {

	if rune_count == 0 {
		return s, true
	}

	if len(s) < rune_count {
		return nil, false
	}

	// get substring of s with requested rune count

	n := 0
	aux := s

	for n < rune_count && len(aux) > 0 {
		_, size := utf8.DecodeRune(aux)
		aux = aux[size:]
		n++
	}

	if n != rune_count {
		return nil, false
	}

	return aux, true
}

func match_one_rune_in_set(s []byte, rune_list [][]byte, rune_range_list_start [][]byte, rune_range_list_end [][]byte, collator *collate.Collator) (remaining []byte, ok bool) {

	if len(s) == 0 {
		return nil, false
	}

	_, size := utf8.DecodeRune(s)

	c := s[:size]

	// check if c is in set

	if character_is_in_set(c, rune_list, rune_range_list_start, rune_range_list_end, collator) {
		return s[size:], true
	}

	return nil, false
}

func match_one_rune_not_in_set(s []byte, rune_list [][]byte, rune_range_list_start [][]byte, rune_range_list_end [][]byte, collator *collate.Collator) (remaining []byte, ok bool) {

	if len(s) == 0 {
		return nil, false
	}

	_, size := utf8.DecodeRune(s)

	c := s[:size]

	// check if c is in set

	if character_is_in_set(c, rune_list, rune_range_list_start, rune_range_list_end, collator) == false {
		return s[size:], true
	}

	return nil, false
}

// Match is the main function used to check if a string s matches a compiled LIKE expression.
//
func Match(likexp_bundle *Likexp_bundle, s []byte, collator *collate.Collator) bool {
	var (
		remaining       []byte
		remaining_aux   []byte
		ok              bool
		likexp          *Likexp
		likexp_list     []*Likexp
		likexp_list_aux []*Likexp
		i               int
	)

	if likexp_bundle.lxb_parsing_failed == true { // note: if likexp_bundle.lxb_error_too_many_percent_placeholders is true, likexp_bundle.lxb_parsing_failed is also true
		return false
	}

	// trim trailing spaces of input string if any.

	for i = len(s) - 1; i >= 0; i-- {
		if s[i] != ' ' {
			break
		}
	}
	s = s[:i+1]

	// if LIKE expression is empty string

	likexp_list = likexp_bundle.lxb_list

	if len(likexp_list) == 0 {
		if len(s) == 0 {
			return true
		}
		return false
	}

	// if % placeholder exists in the LIKE expression, and if last likexps are fixed length likexps, check them. If success, return the remaining left substring.

	if likexp_bundle.lxb_percent_placeholder_count > 0 { // if % exists

		if likexp_list, s, ok = match_ending_series_of_fixed_length_likexps(likexp_list, s, collator); ok == false {
			// fmt.Println("*** LAST FIXED LIKEXPS NOT MATCH ***")
			return false
		}
	}

	// normal matching for remaining string

	remaining = s

LABEL_MAIN_LOOP:
	for len(likexp_list) > 0 {
		likexp = likexp_list[0]

		// % placeholder

		if likexp.lx_type == LX_TYPE_ZERO_OR_MANY_CHARS {

			likexp_list = likexp_list[1:] // discard % likexp
			if len(likexp_list) == 0 {    // if % placeholder is last Likexp, it matches remaining string
				return true
			}

			for len(remaining) > 0 {
				if likexp_list_aux, remaining_aux, ok = match_series_of_fixed_length_likexps(likexp_list, remaining, collator); ok == true {
					likexp_list = likexp_list_aux
					remaining = remaining_aux
					continue LABEL_MAIN_LOOP
				}
				_, size := utf8.DecodeRune(remaining)
				remaining = remaining[size:]
			}

			return false
		}

		// fixed length segment

		if likexp_list, remaining, ok = match_series_of_fixed_length_likexps(likexp_list, remaining, collator); ok == false {
			return false
		}
	}

	if len(remaining) > 0 { // no more characters should remain
		return false
	}
	return true
}

func match_series_of_fixed_length_likexps(likexp_list []*Likexp, s []byte, collator *collate.Collator) (likexp_list_res []*Likexp, remaining []byte, ok bool) {
	var (
		likexp *Likexp
	)

	remaining = s

	for len(likexp_list) > 0 {
		// fmt.Printf("--------------------<%s>\n", remaining)
		likexp = likexp_list[0]

		switch likexp.lx_type {
		case LX_TYPE_STRING: // eat string
			if remaining, ok = match_string(remaining, likexp.lx_string, likexp.lx_rune_count, collator); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_N_CHARS: // eat n chars
			if remaining, ok = match_n_runes(remaining, likexp.lx_rune_count); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_ONE_CHAR_IN_SET: // eat one rune in set
			if remaining, ok = match_one_rune_in_set(remaining, likexp.lx_rune_list, likexp.lx_rune_range_list_start, likexp.lx_rune_range_list_end, collator); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_ONE_CHAR_NOT_IN_SET: // eat one rune not in set
			if remaining, ok = match_one_rune_not_in_set(remaining, likexp.lx_rune_list, likexp.lx_rune_range_list_start, likexp.lx_rune_range_list_end, collator); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_ZERO_OR_MANY_CHARS:
			return likexp_list, remaining, true

		default:
			panic("impossible")
		}

		likexp_list = likexp_list[1:] // discard first likexp, which successfully matched
	}

	return likexp_list, remaining, true // no more likexp. remaining may be empty (all string has been matched) or not (there remains unmatched trailing characters)
}

func match_ending_series_of_fixed_length_likexps(likexp_list []*Likexp, s []byte, collator *collate.Collator) (likexp_list_res []*Likexp, remaining []byte, ok bool) {
	var (
		likexp *Likexp
	)

	remaining = s

	for len(likexp_list) > 0 {
		// fmt.Printf("....................<%s>\n", remaining)
		likexp = likexp_list[len(likexp_list)-1]

		switch likexp.lx_type {
		case LX_TYPE_STRING: // eat string
			if remaining, ok = match_ending_string(remaining, likexp.lx_string, likexp.lx_rune_count, collator); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_N_CHARS: // eat n chars
			if remaining, ok = match_ending_n_runes(remaining, likexp.lx_rune_count); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_ONE_CHAR_IN_SET: // eat one rune in set
			if remaining, ok = match_ending_rune_in_set(remaining, likexp.lx_rune_list, likexp.lx_rune_range_list_start, likexp.lx_rune_range_list_end, collator); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_ONE_CHAR_NOT_IN_SET: // eat one rune not in set
			if remaining, ok = match_ending_rune_not_in_set(remaining, likexp.lx_rune_list, likexp.lx_rune_range_list_start, likexp.lx_rune_range_list_end, collator); ok == false {
				return nil, nil, false
			}

		case LX_TYPE_ZERO_OR_MANY_CHARS:
			return likexp_list, remaining, true

		default:
			panic("impossible")
		}

		likexp_list = likexp_list[:len(likexp_list)-1] // discard last likexp, which successfully matched
	}

	return likexp_list, remaining, true // no more likexp. remaining may be empty (all string has been matched) or not (there remains unmatched leading characters)
}
