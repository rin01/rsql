package like

import (
	"testing"
)

func Test_Likexp_bundle_equal(t *testing.T) {

	samples := []struct {
		spl_in1 string
		spl_in2 string
		spl_res bool
	}{
		{"", "", true},
		{"a", "a", true},
		{"[a]", "a", true},
		{"as", "as", true},
		{"asdf", "asdf", true},
		{"asdf", "asdf ", true}, // trailing spaces are trimmed
		{"asdf", "asdf     ", true},
		{"a", "a ", true},
		{"a_hello", "a_hello", true},
		{"a_hello_[Ww]orld%xxx", "a_hello_[Ww]orld%%%xxx", true},
		{"a_hello_[Ww]orld%xxx", "a_hello_[^Ww]orld%%%xxx", false},
		{"a_hello_[wW]orld%xxx", "a_hello_[Ww]orld%%%xxx", false},
		{"a_he[l]lo_[Ww]orld%xx[123a-g4t-zq]x", "a_hello_[Ww]orld%%%xx[123123a-g4t-zq321a-g]x", true},
	}

	for _, sample := range samples {

		spl_in1, spl_in2, spl_res := sample.spl_in1, sample.spl_in2, sample.spl_res

		var lb1, lb2 Likexp_bundle

		if err := lb1.Parse_likexp(spl_in1, NO_ESCAPE_RUNE); err != nil {
			t.Fatalf("failure parse 1 <%s>, <%s> %t", spl_in1, spl_in2, spl_res)
		}

		if err := lb2.Parse_likexp(spl_in2, NO_ESCAPE_RUNE); err != nil {
			t.Fatalf("failure parse 2 <%s>, <%s> %t", spl_in1, spl_in2, spl_res)
		}

		if lb2.Equal(&lb1) != spl_res {
			t.Fatalf("failure result <%s>, <%s> %t", spl_in1, spl_in2, spl_res)
		}
	}

}
