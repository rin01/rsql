package vm

import (
	"rsql"
	"rsql/csr"
	"rsql/data"
	"rsql/dict"
	"rsql/stmt"
)

func Execute_basicblock(context *rsql.Context, basicblock *rsql.Basicblock) (retval int64, rsql_err *rsql.Error) {
	var (
		instr_list []rsql.Instr3ac
		instr      *rsql.Instr3ac
	)

	instr_list = basicblock.Bb_instr3ac_list // get basicblock instruction list

	for i := range instr_list {
		rsql_err = nil

		instr = &instr_list[i]

		switch instr.Code {

		//======================================================
		//                   VOID operations                  //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_VOID:
			data.Unary_minus_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID))

		case rsql.INSTR_ADD_VOID:
			data.Add_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_SUBTRACT_VOID:
			data.Subtract_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_MULT_VOID:
			data.Multiply_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_DIV_VOID:
			data.Divide_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_MOD_VOID:
			data.Modulo_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_COMP_EQUAL_VOID:
			data.Comp_equal_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_COMP_GREATER_VOID:
			data.Comp_greater_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_COMP_LESS_VOID:
			data.Comp_less_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_COMP_GREATER_EQUAL_VOID:
			data.Comp_greater_equal_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_COMP_LESS_EQUAL_VOID:
			data.Comp_less_equal_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_COMP_NOT_EQUAL_VOID:
			data.Comp_not_equal_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_IS_NULL_VOID:
			data.Is_null_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID))

		case rsql.INSTR_IS_NOT_NULL_VOID:
			data.Is_not_null_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID))

		case rsql.INSTR_IN_LIST_VOID:
			data.In_list_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.([]*data.VOID))

		case rsql.INSTR_NOT_IN_LIST_VOID:
			data.Not_in_list_VOID(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VOID), instr.Arg2.([]*data.VOID))

		case rsql.INSTR_CASE_VOID:
			data.Case_VOID(instr.Arg0.(*data.VOID), instr.Arg1.([]data.Case_element_t))

		//===== VOID CAST =====

		case rsql.INSTR_CAST_VOID_VOID:
			data.Cast_VOID_to_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_SYSLANGUAGE:
			data.Cast_VOID_to_SYSLANGUAGE(instr.Arg0.(*data.SYSLANGUAGE), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_VARBINARY:
			data.Cast_VOID_to_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_VARCHAR:
			data.Cast_VOID_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_BIT:
			data.Cast_VOID_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_TINYINT:
			data.Cast_VOID_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_SMALLINT:
			data.Cast_VOID_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_INT:
			data.Cast_VOID_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_BIGINT:
			data.Cast_VOID_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_MONEY:
			data.Cast_VOID_to_MONEY(instr.Arg0.(*data.MONEY), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_NUMERIC:
			data.Cast_VOID_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_FLOAT:
			data.Cast_VOID_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_DATE:
			data.Cast_VOID_to_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_TIME:
			data.Cast_VOID_to_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.VOID))

		case rsql.INSTR_CAST_VOID_DATETIME:
			data.Cast_VOID_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.VOID))

		//===== VOID functions =====

		case rsql.INSTR_SYSFUNC_ISNUMERIC_VOID:
			data.Sysfunc_isnumeric_VOID(instr.Arg0.(*data.INT), instr.Arg1.(*data.VOID))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_VOID:
			data.Sysfunc_isnull_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.VOID), instr.Arg2.(*data.VOID))

		case rsql.INSTR_SYSFUNC_IIF_VOID:
			data.Sysfunc_iif_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.VOID), instr.Arg3.(*data.VOID))

		case rsql.INSTR_SYSFUNC_CHOOSE_VOID:
			data.Sysfunc_choose_VOID(instr.Arg0.(*data.VOID), instr.Arg1.(*data.INT), instr.Arg2.([]*data.VOID))

		case rsql.INSTR_SYSFUNC_COALESCE_VOID:
			data.Sysfunc_coalesce_VOID(instr.Arg0.(*data.VOID), instr.Arg1.([]*data.VOID))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_VOID:
			data.Sysfunc_typeof_VOID(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VOID))

		//======================================================
		//                 SYSLANGUAGE operations             //
		//======================================================

		case rsql.INSTR_SYSFUNC_atatDATEFIRST:
			data.Sysfunc_atatdatefirst_SYSLANGUAGE(instr.Arg0.(*data.INT), instr.Arg1.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_atatDATEFORMAT:
			data.Sysfunc_atatdateformat_SYSLANGUAGE(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_atatLANGUAGE:
			data.Sysfunc_atatlanguage_SYSLANGUAGE(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE:
			data.Sysfunc_firstdayofweek_SYSLANGUAGE(instr.Arg0.(*data.SYSLANGUAGE), instr.Arg1.(*data.SYSLANGUAGE), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_DMY_SYSLANGUAGE:
			data.Sysfunc_dmy_SYSLANGUAGE(instr.Arg0.(*data.SYSLANGUAGE), instr.Arg1.(*data.SYSLANGUAGE), instr.Arg2.(*data.VARCHAR))

		//======================================================
		//                 BOOLEAN operations                 //
		//======================================================

		case rsql.INSTR_LOGICAL_UNARY_NOT_BOOLEAN:
			data.Logical_unary_not_BOOLEAN(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BOOLEAN))

		case rsql.INSTR_LOGICAL_AND_BOOLEAN:
			data.Logical_and_BOOLEAN(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.BOOLEAN))

		case rsql.INSTR_LOGICAL_OR_BOOLEAN:
			data.Logical_or_BOOLEAN(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.BOOLEAN))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_BOOLEAN:
			data.Sysfunc_typeof_BOOLEAN(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BOOLEAN))

		//======================================================
		//                VARBINARY operations                //
		//======================================================

		case rsql.INSTR_ADD_VARBINARY:
			data.Add_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_COMP_EQUAL_VARBINARY:
			data.Comp_equal_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_COMP_GREATER_VARBINARY:
			data.Comp_greater_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_COMP_LESS_VARBINARY:
			data.Comp_less_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_COMP_GREATER_EQUAL_VARBINARY:
			data.Comp_greater_equal_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_COMP_LESS_EQUAL_VARBINARY:
			data.Comp_less_equal_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_COMP_NOT_EQUAL_VARBINARY:
			data.Comp_not_equal_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_IS_NULL_VARBINARY:
			data.Is_null_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_IS_NOT_NULL_VARBINARY:
			data.Is_not_null_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_IN_LIST_VARBINARY:
			data.In_list_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.([]*data.VARBINARY))

		case rsql.INSTR_NOT_IN_LIST_VARBINARY:
			data.Not_in_list_VARBINARY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARBINARY), instr.Arg2.([]*data.VARBINARY))

		case rsql.INSTR_CASE_VARBINARY:
			data.Case_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.([]data.Case_element_t))

		//===== VARBINARY CAST =====

		case rsql.INSTR_CAST_VARBINARY_VARBINARY:
			data.Cast_VARBINARY_to_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_CAST_VARBINARY_BIT:
			data.Cast_VARBINARY_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_CAST_VARBINARY_TINYINT:
			data.Cast_VARBINARY_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_CAST_VARBINARY_SMALLINT:
			data.Cast_VARBINARY_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_CAST_VARBINARY_INT:
			data.Cast_VARBINARY_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_CAST_VARBINARY_BIGINT:
			data.Cast_VARBINARY_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VARBINARY))

		//===== VARBINARY functions =====

		case rsql.INSTR_SYSFUNC_LEN_VARBINARY:
			data.Sysfunc_len_VARBINARY(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_ISNUMERIC_VARBINARY:
			data.Sysfunc_isnumeric_VARBINARY(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY: // The user cannot call this function. It is generated by the decorator, for INSERT INTO and UPDATE statement.
			data.Sysfunc_fit_no_truncate_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VARBINARY))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_VARBINARY:
			data.Sysfunc_isnull_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VARBINARY), instr.Arg2.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_IIF_VARBINARY:
			data.Sysfunc_iif_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.VARBINARY), instr.Arg3.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_CHOOSE_VARBINARY:
			data.Sysfunc_choose_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.INT), instr.Arg2.([]*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_COALESCE_VARBINARY:
			data.Sysfunc_coalesce_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.([]*data.VARBINARY))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_VARBINARY:
			data.Sysfunc_typeof_VARBINARY(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARBINARY))

		//======================================================
		//                VARCHAR operations                  //
		//======================================================

		case rsql.INSTR_ADD_VARCHAR:
			data.Add_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR))

		case rsql.INSTR_COMP_EQUAL_VARCHAR:
			data.Comp_equal_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_COMP_GREATER_VARCHAR:
			data.Comp_greater_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_COMP_LESS_VARCHAR:
			data.Comp_less_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_COMP_GREATER_EQUAL_VARCHAR:
			data.Comp_greater_equal_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_COMP_LESS_EQUAL_VARCHAR:
			data.Comp_less_equal_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_COMP_NOT_EQUAL_VARCHAR:
			data.Comp_not_equal_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_LIKE_VARCHAR:
			data.Comp_like_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.REGEXPLIKE), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_NOT_LIKE_VARCHAR:
			data.Comp_not_like_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.REGEXPLIKE), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_IS_NULL_VARCHAR:
			data.Is_null_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_IS_NOT_NULL_VARCHAR:
			data.Is_not_null_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_IN_LIST_VARCHAR:
			data.In_list_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.([]*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_NOT_IN_LIST_VARCHAR:
			data.Not_in_list_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.([]*data.VARCHAR), instr.Arg3.(*data.SYSCOLLATOR))

		case rsql.INSTR_CASE_VARCHAR:
			data.Case_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.([]data.Case_element_t))

		//===== VARCHAR CAST =====

		case rsql.INSTR_CAST_VARCHAR_SYSLANGUAGE:
			data.Cast_VARCHAR_to_SYSLANGUAGE(instr.Arg0.(*data.SYSLANGUAGE), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_VARCHAR:
			data.Cast_VARCHAR_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_BIT:
			data.Cast_VARCHAR_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_TINYINT:
			data.Cast_VARCHAR_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_SMALLINT:
			data.Cast_VARCHAR_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_INT:
			data.Cast_VARCHAR_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_BIGINT:
			data.Cast_VARCHAR_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_MONEY:
			data.Cast_VARCHAR_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_NUMERIC:
			data.Cast_VARCHAR_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_CAST_VARCHAR_FLOAT:
			data.Cast_VARCHAR_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.VARCHAR))

		//===== VARCHAR functions =====

		case rsql.INSTR_SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE:
			data.Sysfunc_compile_VARCHAR_to_REGEXPLIKE(instr.Arg0.(*data.REGEXPLIKE), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_CHARLEN_VARCHAR:
			data.Sysfunc_charlen_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_LEN_VARCHAR:
			data.Sysfunc_len_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_ASCII_VARCHAR:
			data.Sysfunc_ascii_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_UNICODE_VARCHAR:
			data.Sysfunc_unicode_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_CHAR_VARCHAR:
			data.Sysfunc_char_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_NCHAR_VARCHAR:
			data.Sysfunc_nchar_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_LTRIM_VARCHAR:
			data.Sysfunc_ltrim_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_RTRIM_VARCHAR:
			data.Sysfunc_rtrim_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_TRIM_VARCHAR:
			data.Sysfunc_trim_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_LOWER_VARCHAR:
			data.Sysfunc_lower_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_UPPER_VARCHAR:
			data.Sysfunc_upper_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_SPACE_VARCHAR:
			data.Sysfunc_space_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_REPLICATE_VARCHAR:
			data.Sysfunc_replicate_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_LEFT_VARCHAR:
			data.Sysfunc_left_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_RIGHT_VARCHAR:
			data.Sysfunc_right_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_STUFF_VARCHAR:
			data.Sysfunc_stuff_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT), instr.Arg4.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_SUBSTRING_VARCHAR:
			data.Sysfunc_substring_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		case rsql.INSTR_SYSFUNC_CHARINDEX_VARCHAR:
			data.Sysfunc_charindex_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.INT), instr.Arg4.(*data.SYSCOLLATOR))

		case rsql.INSTR_SYSFUNC_REPLACE_VARCHAR:
			data.Sysfunc_replace_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.VARCHAR), instr.Arg4.(*data.SYSCOLLATOR))

		case rsql.INSTR_SYSFUNC_STARTS_WITH_VARCHAR:
			data.Sysfunc_starts_with_VARCHAR(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.INT), instr.Arg4.(*data.SYSCOLLATOR))

		case rsql.INSTR_SYSFUNC_CONCAT_VARCHAR:
			data.Sysfunc_concat_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.([]*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE:
			data.Sysfunc_convert_VARCHAR_to_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_TIME:
			data.Sysfunc_convert_VARCHAR_to_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME:
			data.Sysfunc_convert_VARCHAR_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_ISDATE_VARCHAR:
			data.Sysfunc_isdate_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_ISNUMERIC_VARCHAR:
			data.Sysfunc_isnumeric_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR: // The user cannot call this function. It is generated by the decorator, for INSERT INTO and UPDATE statement.
			data.Sysfunc_fit_no_truncate_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_VARCHAR:
			data.Sysfunc_isnull_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_IIF_VARCHAR:
			data.Sysfunc_iif_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_CHOOSE_VARCHAR:
			data.Sysfunc_choose_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT), instr.Arg2.([]*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_COALESCE_VARCHAR:
			data.Sysfunc_coalesce_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.([]*data.VARCHAR))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_VARCHAR:
			data.Sysfunc_typeof_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		//======================================================
		//                REGEXPLIKE operations               //
		//======================================================

		// no operation exists

		//======================================================
		//                     BIT operations                 //
		//======================================================

		case rsql.INSTR_BITWISE_AND_BIT:
			data.Bitwise_and_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_BITWISE_OR_BIT:
			data.Bitwise_or_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_BITWISE_XOR_BIT:
			data.Bitwise_xor_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_BITWISE_UNARY_NOT_BIT:
			data.Bitwise_unary_not_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_COMP_EQUAL_BIT:
			data.Comp_equal_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_COMP_GREATER_BIT:
			data.Comp_greater_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_COMP_LESS_BIT:
			data.Comp_less_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_COMP_GREATER_EQUAL_BIT:
			data.Comp_greater_equal_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_COMP_LESS_EQUAL_BIT:
			data.Comp_less_equal_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_COMP_NOT_EQUAL_BIT:
			data.Comp_not_equal_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_IS_NULL_BIT:
			data.Is_null_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT))

		case rsql.INSTR_IS_NOT_NULL_BIT:
			data.Is_not_null_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT))

		case rsql.INSTR_IN_LIST_BIT:
			data.In_list_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.([]*data.BIT))

		case rsql.INSTR_NOT_IN_LIST_BIT:
			data.Not_in_list_BIT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIT), instr.Arg2.([]*data.BIT))

		case rsql.INSTR_CASE_BIT:
			data.Case_BIT(instr.Arg0.(*data.BIT), instr.Arg1.([]data.Case_element_t))

		//===== BIT CAST =====

		case rsql.INSTR_CAST_BIT_VARCHAR:
			data.Cast_BIT_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_BIT:
			data.Cast_BIT_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_TINYINT:
			data.Cast_BIT_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_SMALLINT:
			data.Cast_BIT_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_INT:
			data.Cast_BIT_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_BIGINT:
			data.Cast_BIT_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_MONEY:
			data.Cast_BIT_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_NUMERIC:
			data.Cast_BIT_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.BIT))

		case rsql.INSTR_CAST_BIT_FLOAT:
			data.Cast_BIT_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.BIT))

		//===== BIT functions =====

		case rsql.INSTR_SYSFUNC_ISNUMERIC_BIT:
			data.Sysfunc_isnumeric_BIT(instr.Arg0.(*data.INT), instr.Arg1.(*data.BIT))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_BIT:
			data.Sysfunc_isnull_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT), instr.Arg2.(*data.BIT))

		case rsql.INSTR_SYSFUNC_IIF_BIT:
			data.Sysfunc_iif_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.BIT), instr.Arg3.(*data.BIT))

		case rsql.INSTR_SYSFUNC_CHOOSE_BIT:
			data.Sysfunc_choose_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.INT), instr.Arg2.([]*data.BIT))

		case rsql.INSTR_SYSFUNC_COALESCE_BIT:
			data.Sysfunc_coalesce_BIT(instr.Arg0.(*data.BIT), instr.Arg1.([]*data.BIT))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_BIT:
			data.Sysfunc_typeof_BIT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIT))

		//======================================================
		//                   TINYINT operations               //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_TINYINT:
			data.Unary_minus_TINYINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.TINYINT)) // result is SMALLINT

		case rsql.INSTR_ADD_TINYINT:
			data.Add_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_SUBTRACT_TINYINT:
			data.Subtract_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_MULT_TINYINT:
			data.Multiply_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_DIV_TINYINT:
			data.Divide_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_MOD_TINYINT:
			data.Modulo_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_BITWISE_AND_TINYINT:
			data.Bitwise_and_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_BITWISE_OR_TINYINT:
			data.Bitwise_or_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_BITWISE_XOR_TINYINT:
			data.Bitwise_xor_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_BITWISE_UNARY_NOT_TINYINT:
			data.Bitwise_unary_not_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_COMP_EQUAL_TINYINT:
			data.Comp_equal_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_COMP_GREATER_TINYINT:
			data.Comp_greater_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_COMP_LESS_TINYINT:
			data.Comp_less_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_COMP_GREATER_EQUAL_TINYINT:
			data.Comp_greater_equal_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_COMP_LESS_EQUAL_TINYINT:
			data.Comp_less_equal_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_COMP_NOT_EQUAL_TINYINT:
			data.Comp_not_equal_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_IS_NULL_TINYINT:
			data.Is_null_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_IS_NOT_NULL_TINYINT:
			data.Is_not_null_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_IN_LIST_TINYINT:
			data.In_list_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.([]*data.TINYINT))

		case rsql.INSTR_NOT_IN_LIST_TINYINT:
			data.Not_in_list_TINYINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TINYINT), instr.Arg2.([]*data.TINYINT))

		case rsql.INSTR_CASE_TINYINT:
			data.Case_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.([]data.Case_element_t))

		//===== TINYINT CAST =====

		case rsql.INSTR_CAST_TINYINT_VARCHAR:
			data.Cast_TINYINT_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_BIT:
			data.Cast_TINYINT_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_TINYINT:
			data.Cast_TINYINT_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_SMALLINT:
			data.Cast_TINYINT_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_INT:
			data.Cast_TINYINT_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_BIGINT:
			data.Cast_TINYINT_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_MONEY:
			data.Cast_TINYINT_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_NUMERIC:
			data.Cast_TINYINT_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_FLOAT:
			data.Cast_TINYINT_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_CAST_TINYINT_DATETIME:
			data.Cast_TINYINT_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.TINYINT))

		//===== TINYINT functions =====

		case rsql.INSTR_SYSFUNC_ISNUMERIC_TINYINT:
			data.Sysfunc_isnumeric_TINYINT(instr.Arg0.(*data.INT), instr.Arg1.(*data.TINYINT))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_TINYINT:
			data.Sysfunc_isnull_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT), instr.Arg2.(*data.TINYINT))

		case rsql.INSTR_SYSFUNC_IIF_TINYINT:
			data.Sysfunc_iif_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.TINYINT), instr.Arg3.(*data.TINYINT))

		case rsql.INSTR_SYSFUNC_CHOOSE_TINYINT:
			data.Sysfunc_choose_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.INT), instr.Arg2.([]*data.TINYINT))

		case rsql.INSTR_SYSFUNC_COALESCE_TINYINT:
			data.Sysfunc_coalesce_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.([]*data.TINYINT))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_TINYINT:
			data.Sysfunc_typeof_TINYINT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.TINYINT))

		//======================================================
		//                  SMALLINT operations               //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_SMALLINT:
			data.Unary_minus_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_ADD_SMALLINT:
			data.Add_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_SUBTRACT_SMALLINT:
			data.Subtract_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_MULT_SMALLINT:
			data.Multiply_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_DIV_SMALLINT:
			data.Divide_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_MOD_SMALLINT:
			data.Modulo_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_BITWISE_AND_SMALLINT:
			data.Bitwise_and_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_BITWISE_OR_SMALLINT:
			data.Bitwise_or_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_BITWISE_XOR_SMALLINT:
			data.Bitwise_xor_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_BITWISE_UNARY_NOT_SMALLINT:
			data.Bitwise_unary_not_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_COMP_EQUAL_SMALLINT:
			data.Comp_equal_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_COMP_GREATER_SMALLINT:
			data.Comp_greater_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_COMP_LESS_SMALLINT:
			data.Comp_less_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_COMP_GREATER_EQUAL_SMALLINT:
			data.Comp_greater_equal_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_COMP_LESS_EQUAL_SMALLINT:
			data.Comp_less_equal_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_COMP_NOT_EQUAL_SMALLINT:
			data.Comp_not_equal_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_IS_NULL_SMALLINT:
			data.Is_null_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_IS_NOT_NULL_SMALLINT:
			data.Is_not_null_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_IN_LIST_SMALLINT:
			data.In_list_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.([]*data.SMALLINT))

		case rsql.INSTR_NOT_IN_LIST_SMALLINT:
			data.Not_in_list_SMALLINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.SMALLINT), instr.Arg2.([]*data.SMALLINT))

		case rsql.INSTR_CASE_SMALLINT:
			data.Case_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.([]data.Case_element_t))

		//===== SMALLINT CAST =====

		case rsql.INSTR_CAST_SMALLINT_VARCHAR:
			data.Cast_SMALLINT_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_BIT:
			data.Cast_SMALLINT_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_TINYINT:
			data.Cast_SMALLINT_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_SMALLINT:
			data.Cast_SMALLINT_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_INT:
			data.Cast_SMALLINT_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_BIGINT:
			data.Cast_SMALLINT_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_MONEY:
			data.Cast_SMALLINT_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_NUMERIC:
			data.Cast_SMALLINT_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_FLOAT:
			data.Cast_SMALLINT_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_CAST_SMALLINT_DATETIME:
			data.Cast_SMALLINT_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.SMALLINT))

		//===== SMALLINT functions =====

		case rsql.INSTR_SYSFUNC_ISNUMERIC_SMALLINT:
			data.Sysfunc_isnumeric_SMALLINT(instr.Arg0.(*data.INT), instr.Arg1.(*data.SMALLINT))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_SMALLINT:
			data.Sysfunc_isnull_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT), instr.Arg2.(*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_IIF_SMALLINT:
			data.Sysfunc_iif_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.SMALLINT), instr.Arg3.(*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_CHOOSE_SMALLINT:
			data.Sysfunc_choose_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.INT), instr.Arg2.([]*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_COALESCE_SMALLINT:
			data.Sysfunc_coalesce_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.([]*data.SMALLINT))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_SMALLINT:
			data.Sysfunc_typeof_SMALLINT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.SMALLINT))

		//======================================================
		//                   INT operations                   //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_INT:
			data.Unary_minus_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_ADD_INT:
			data.Add_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_SUBTRACT_INT:
			data.Subtract_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_MULT_INT:
			data.Multiply_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_DIV_INT:
			data.Divide_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_MOD_INT:
			data.Modulo_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_BITWISE_AND_INT:
			data.Bitwise_and_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_BITWISE_OR_INT:
			data.Bitwise_or_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_BITWISE_XOR_INT:
			data.Bitwise_xor_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_BITWISE_UNARY_NOT_INT:
			data.Bitwise_unary_not_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_COMP_EQUAL_INT:
			data.Comp_equal_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_COMP_GREATER_INT:
			data.Comp_greater_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_COMP_LESS_INT:
			data.Comp_less_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_COMP_GREATER_EQUAL_INT:
			data.Comp_greater_equal_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_COMP_LESS_EQUAL_INT:
			data.Comp_less_equal_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_COMP_NOT_EQUAL_INT:
			data.Comp_not_equal_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_IS_NULL_INT:
			data.Is_null_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT))

		case rsql.INSTR_IS_NOT_NULL_INT:
			data.Is_not_null_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT))

		case rsql.INSTR_IN_LIST_INT:
			data.In_list_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.([]*data.INT))

		case rsql.INSTR_NOT_IN_LIST_INT:
			data.Not_in_list_INT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.INT), instr.Arg2.([]*data.INT))

		case rsql.INSTR_CASE_INT:
			data.Case_INT(instr.Arg0.(*data.INT), instr.Arg1.([]data.Case_element_t))

		//===== INT CAST =====

		case rsql.INSTR_CAST_INT_VARCHAR:
			data.Cast_INT_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_BIT:
			data.Cast_INT_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_TINYINT:
			data.Cast_INT_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_SMALLINT:
			data.Cast_INT_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_INT:
			data.Cast_INT_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_BIGINT:
			data.Cast_INT_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_MONEY:
			data.Cast_INT_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_NUMERIC:
			data.Cast_INT_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_FLOAT:
			data.Cast_INT_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.INT))

		case rsql.INSTR_CAST_INT_DATETIME:
			data.Cast_INT_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT))

		//===== INT functions =====

		case rsql.INSTR_SYSFUNC_ABS_INT:
			data.Sysfunc_abs_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_CEILING_INT:
			data.Sysfunc_ceiling_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_FLOOR_INT:
			data.Sysfunc_floor_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_SIGN_INT:
			data.Sysfunc_sign_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_POWER_INT:
			data.Sysfunc_power_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ROUND_INT:
			data.Sysfunc_round_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		// MISC

		case rsql.INSTR_SYSFUNC_FORMAT_INT:
			data.Sysfunc_format_INT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_ISNUMERIC_INT:
			data.Sysfunc_isnumeric_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_INT:
			data.Sysfunc_isnull_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_IIF_INT:
			data.Sysfunc_iif_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		case rsql.INSTR_SYSFUNC_CHOOSE_INT:
			data.Sysfunc_choose_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.([]*data.INT))

		case rsql.INSTR_SYSFUNC_COALESCE_INT:
			data.Sysfunc_coalesce_INT(instr.Arg0.(*data.INT), instr.Arg1.([]*data.INT))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_INT:
			data.Sysfunc_typeof_INT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT))

		//======================================================
		//                   BIGINT operations                //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_BIGINT:
			data.Unary_minus_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_ADD_BIGINT:
			data.Add_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_SUBTRACT_BIGINT:
			data.Subtract_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_MULT_BIGINT:
			data.Multiply_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_DIV_BIGINT:
			data.Divide_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_MOD_BIGINT:
			data.Modulo_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_BITWISE_AND_BIGINT:
			data.Bitwise_and_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_BITWISE_OR_BIGINT:
			data.Bitwise_or_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_BITWISE_XOR_BIGINT:
			data.Bitwise_xor_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_BITWISE_UNARY_NOT_BIGINT:
			data.Bitwise_unary_not_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_COMP_EQUAL_BIGINT:
			data.Comp_equal_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_COMP_GREATER_BIGINT:
			data.Comp_greater_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_COMP_LESS_BIGINT:
			data.Comp_less_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_COMP_GREATER_EQUAL_BIGINT:
			data.Comp_greater_equal_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_COMP_LESS_EQUAL_BIGINT:
			data.Comp_less_equal_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_COMP_NOT_EQUAL_BIGINT:
			data.Comp_not_equal_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_IS_NULL_BIGINT:
			data.Is_null_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_IS_NOT_NULL_BIGINT:
			data.Is_not_null_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_IN_LIST_BIGINT:
			data.In_list_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.([]*data.BIGINT))

		case rsql.INSTR_NOT_IN_LIST_BIGINT:
			data.Not_in_list_BIGINT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.BIGINT), instr.Arg2.([]*data.BIGINT))

		case rsql.INSTR_CASE_BIGINT:
			data.Case_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.([]data.Case_element_t))

		//===== BIGINT CAST =====

		case rsql.INSTR_CAST_BIGINT_VARCHAR:
			data.Cast_BIGINT_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_BIT:
			data.Cast_BIGINT_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_TINYINT:
			data.Cast_BIGINT_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_SMALLINT:
			data.Cast_BIGINT_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_INT:
			data.Cast_BIGINT_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_BIGINT:
			data.Cast_BIGINT_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_MONEY:
			data.Cast_BIGINT_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_NUMERIC:
			data.Cast_BIGINT_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_FLOAT:
			data.Cast_BIGINT_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_CAST_BIGINT_DATETIME:
			data.Cast_BIGINT_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.BIGINT))

		//===== BIGINT functions =====

		case rsql.INSTR_SYSFUNC_ABS_BIGINT:
			data.Sysfunc_abs_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_CEILING_BIGINT:
			data.Sysfunc_ceiling_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_FLOOR_BIGINT:
			data.Sysfunc_floor_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_SIGN_BIGINT:
			data.Sysfunc_sign_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_POWER_BIGINT:
			data.Sysfunc_power_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ROUND_BIGINT:
			data.Sysfunc_round_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		// MISC

		case rsql.INSTR_SYSFUNC_FORMAT_BIGINT:
			data.Sysfunc_format_BIGINT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_ISNUMERIC_BIGINT:
			data.Sysfunc_isnumeric_BIGINT(instr.Arg0.(*data.INT), instr.Arg1.(*data.BIGINT))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_BIGINT:
			data.Sysfunc_isnull_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_IIF_BIGINT:
			data.Sysfunc_iif_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.BIGINT), instr.Arg3.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_CHOOSE_BIGINT:
			data.Sysfunc_choose_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.INT), instr.Arg2.([]*data.BIGINT))

		case rsql.INSTR_SYSFUNC_COALESCE_BIGINT:
			data.Sysfunc_coalesce_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.([]*data.BIGINT))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_BIGINT:
			data.Sysfunc_typeof_BIGINT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIGINT))

		//======================================================
		//                   MONEY operations                 //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_MONEY:
			data.Unary_minus_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_ADD_MONEY:
			data.Add_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_SUBTRACT_MONEY:
			data.Subtract_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_MULT_MONEY:
			data.Multiply_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_DIV_MONEY:
			data.Divide_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_COMP_EQUAL_MONEY:
			data.Comp_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_COMP_GREATER_MONEY:
			data.Comp_greater_NUMERIC(instr.Arg0.(*data.BOOLEAN), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_COMP_LESS_MONEY:
			data.Comp_less_NUMERIC(instr.Arg0.(*data.BOOLEAN), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_COMP_GREATER_EQUAL_MONEY:
			data.Comp_greater_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_COMP_LESS_EQUAL_MONEY:
			data.Comp_less_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_COMP_NOT_EQUAL_MONEY:
			data.Comp_not_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), (*data.NUMERIC)(instr.Arg2.(*data.MONEY)))

		case rsql.INSTR_IS_NULL_MONEY:
			data.Is_null_MONEY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.MONEY))

		case rsql.INSTR_IS_NOT_NULL_MONEY:
			data.Is_not_null_MONEY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.MONEY))

		case rsql.INSTR_IN_LIST_MONEY:
			data.In_list_MONEY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.MONEY), instr.Arg2.([]*data.MONEY))

		case rsql.INSTR_NOT_IN_LIST_MONEY:
			data.Not_in_list_MONEY(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.MONEY), instr.Arg2.([]*data.MONEY))

		case rsql.INSTR_CASE_MONEY:
			data.Case_MONEY(instr.Arg0.(*data.MONEY), instr.Arg1.([]data.Case_element_t))

		//===== MONEY CAST =====

		case rsql.INSTR_CAST_MONEY_VARCHAR:
			data.Cast_MONEY_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.MONEY)) // note: it is a MONEY function, because it writes two decimal digits

		case rsql.INSTR_CAST_MONEY_BIT:
			data.Cast_NUMERIC_to_BIT(instr.Arg0.(*data.BIT), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_CAST_MONEY_TINYINT:
			data.Cast_MONEY_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.MONEY)) // note: it is a MONEY function, because NUMERIC truncates instead of rounding

		case rsql.INSTR_CAST_MONEY_SMALLINT:
			data.Cast_MONEY_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.MONEY)) // note: it is a MONEY function, because NUMERIC truncates instead of rounding

		case rsql.INSTR_CAST_MONEY_INT:
			data.Cast_MONEY_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.MONEY)) // note: it is a MONEY function, because NUMERIC truncates instead of rounding

		case rsql.INSTR_CAST_MONEY_BIGINT:
			data.Cast_MONEY_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.MONEY)) // note: it is a MONEY function, because NUMERIC truncates instead of rounding

		case rsql.INSTR_CAST_MONEY_MONEY:
			data.Cast_NUMERIC_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_CAST_MONEY_NUMERIC:
			data.Cast_NUMERIC_to_NUMERIC(instr.Arg0.(*data.NUMERIC), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_CAST_MONEY_FLOAT:
			data.Cast_NUMERIC_to_FLOAT(instr.Arg0.(*data.FLOAT), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		//===== MONEY functions =====

		case rsql.INSTR_SYSFUNC_ABS_MONEY:
			data.Sysfunc_abs_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_CEILING_MONEY:
			data.Sysfunc_ceiling_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_FLOOR_MONEY:
			data.Sysfunc_floor_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_SIGN_MONEY:
			data.Sysfunc_sign_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_POWER_MONEY:
			data.Sysfunc_power_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ROUND_MONEY:
			data.Sysfunc_round_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		// MISC

		case rsql.INSTR_SYSFUNC_FORMAT_MONEY:
			data.Sysfunc_format_NUMERIC(instr.Arg0.(*data.VARCHAR), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_ISNUMERIC_MONEY:
			data.Sysfunc_isnumeric_NUMERIC(instr.Arg0.(*data.INT), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_MONEY:
			data.Sysfunc_isnull_MONEY(instr.Arg0.(*data.MONEY), instr.Arg1.(*data.MONEY), instr.Arg2.(*data.MONEY))

		case rsql.INSTR_SYSFUNC_IIF_MONEY:
			data.Sysfunc_iif_MONEY(instr.Arg0.(*data.MONEY), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.MONEY), instr.Arg3.(*data.MONEY))

		case rsql.INSTR_SYSFUNC_CHOOSE_MONEY:
			data.Sysfunc_choose_MONEY(instr.Arg0.(*data.MONEY), instr.Arg1.(*data.INT), instr.Arg2.([]*data.MONEY))

		case rsql.INSTR_SYSFUNC_COALESCE_MONEY:
			data.Sysfunc_coalesce_MONEY(instr.Arg0.(*data.MONEY), instr.Arg1.([]*data.MONEY))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_MONEY:
			data.Sysfunc_typeof_MONEY(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.MONEY))

		//======================================================
		//                   NUMERIC operations               //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_NUMERIC:
			data.Unary_minus_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_ADD_NUMERIC:
			data.Add_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_SUBTRACT_NUMERIC:
			data.Subtract_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_MULT_NUMERIC:
			data.Multiply_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_DIV_NUMERIC:
			data.Divide_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_COMP_EQUAL_NUMERIC:
			data.Comp_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_COMP_GREATER_NUMERIC:
			data.Comp_greater_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_COMP_LESS_NUMERIC:
			data.Comp_less_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_COMP_GREATER_EQUAL_NUMERIC:
			data.Comp_greater_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_COMP_LESS_EQUAL_NUMERIC:
			data.Comp_less_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_COMP_NOT_EQUAL_NUMERIC:
			data.Comp_not_equal_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_IS_NULL_NUMERIC:
			data.Is_null_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_IS_NOT_NULL_NUMERIC:
			data.Is_not_null_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_IN_LIST_NUMERIC:
			data.In_list_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.([]*data.NUMERIC))

		case rsql.INSTR_NOT_IN_LIST_NUMERIC:
			data.Not_in_list_NUMERIC(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.NUMERIC), instr.Arg2.([]*data.NUMERIC))

		case rsql.INSTR_CASE_NUMERIC:
			data.Case_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.([]data.Case_element_t))

		//===== NUMERIC CAST =====

		case rsql.INSTR_CAST_NUMERIC_VARCHAR:
			data.Cast_NUMERIC_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_BIT:
			data.Cast_NUMERIC_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_TINYINT:
			data.Cast_NUMERIC_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_SMALLINT:
			data.Cast_NUMERIC_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_INT:
			data.Cast_NUMERIC_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_BIGINT:
			data.Cast_NUMERIC_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_MONEY:
			data.Cast_NUMERIC_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_NUMERIC:
			data.Cast_NUMERIC_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_CAST_NUMERIC_FLOAT:
			data.Cast_NUMERIC_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.NUMERIC))

		//===== NUMERIC functions =====

		case rsql.INSTR_SYSFUNC_ABS_NUMERIC:
			data.Sysfunc_abs_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_CEILING_NUMERIC:
			data.Sysfunc_ceiling_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_FLOOR_NUMERIC:
			data.Sysfunc_floor_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_SIGN_NUMERIC:
			data.Sysfunc_sign_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_POWER_NUMERIC:
			data.Sysfunc_power_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ROUND_NUMERIC:
			data.Sysfunc_round_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		// MISC

		case rsql.INSTR_SYSFUNC_FORMAT_NUMERIC:
			data.Sysfunc_format_NUMERIC(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_ISNUMERIC_NUMERIC:
			data.Sysfunc_isnumeric_NUMERIC(instr.Arg0.(*data.INT), instr.Arg1.(*data.NUMERIC))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_NUMERIC:
			data.Sysfunc_isnull_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC), instr.Arg2.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_IIF_NUMERIC:
			data.Sysfunc_iif_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.NUMERIC), instr.Arg3.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_CHOOSE_NUMERIC:
			data.Sysfunc_choose_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.INT), instr.Arg2.([]*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_COALESCE_NUMERIC:
			data.Sysfunc_coalesce_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.([]*data.NUMERIC))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_NUMERIC:
			data.Sysfunc_typeof_NUMERIC(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.NUMERIC))

		//======================================================
		//                   FLOAT operations                 //
		//======================================================

		case rsql.INSTR_UNARY_MINUS_FLOAT:
			data.Unary_minus_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_ADD_FLOAT:
			data.Add_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SUBTRACT_FLOAT:
			data.Subtract_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_MULT_FLOAT:
			data.Multiply_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_DIV_FLOAT:
			data.Divide_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_COMP_EQUAL_FLOAT:
			data.Comp_equal_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_COMP_GREATER_FLOAT:
			data.Comp_greater_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_COMP_LESS_FLOAT:
			data.Comp_less_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_COMP_GREATER_EQUAL_FLOAT:
			data.Comp_greater_equal_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_COMP_LESS_EQUAL_FLOAT:
			data.Comp_less_equal_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_COMP_NOT_EQUAL_FLOAT:
			data.Comp_not_equal_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_IS_NULL_FLOAT:
			data.Is_null_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_IS_NOT_NULL_FLOAT:
			data.Is_not_null_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_IN_LIST_FLOAT:
			data.In_list_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.([]*data.FLOAT))

		case rsql.INSTR_NOT_IN_LIST_FLOAT:
			data.Not_in_list_FLOAT(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.FLOAT), instr.Arg2.([]*data.FLOAT))

		case rsql.INSTR_CASE_FLOAT:
			data.Case_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.([]data.Case_element_t))

		//===== FLOAT CAST =====

		case rsql.INSTR_CAST_FLOAT_VARCHAR:
			data.Cast_FLOAT_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_BIT:
			data.Cast_FLOAT_to_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_TINYINT:
			data.Cast_FLOAT_to_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_SMALLINT:
			data.Cast_FLOAT_to_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_INT:
			data.Cast_FLOAT_to_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_BIGINT:
			data.Cast_FLOAT_to_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_MONEY:
			data.Cast_FLOAT_to_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_NUMERIC:
			data.Cast_FLOAT_to_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_CAST_FLOAT_FLOAT:
			data.Cast_FLOAT_to_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		//===== FLOAT functions =====

		case rsql.INSTR_SYSFUNC_ABS_FLOAT:
			data.Sysfunc_abs_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_CEILING_FLOAT:
			data.Sysfunc_ceiling_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_FLOOR_FLOAT:
			data.Sysfunc_floor_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_SIGN_FLOAT:
			data.Sysfunc_sign_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_POWER_FLOAT:
			data.Sysfunc_power_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ROUND_FLOAT:
			data.Sysfunc_round_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		case rsql.INSTR_SYSFUNC_ACOS_FLOAT:
			data.Sysfunc_acos_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ASIN_FLOAT:
			data.Sysfunc_asin_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ATAN_FLOAT:
			data.Sysfunc_atan_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_ATN2_FLOAT:
			data.Sysfunc_atn2_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_COS_FLOAT:
			data.Sysfunc_cos_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_SIN_FLOAT:
			data.Sysfunc_sin_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_TAN_FLOAT:
			data.Sysfunc_tan_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_COT_FLOAT:
			data.Sysfunc_cot_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_EXP_FLOAT:
			data.Sysfunc_exp_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_LOG_FLOAT:
			data.Sysfunc_log_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_LOG10_FLOAT:
			data.Sysfunc_log10_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_SQRT_FLOAT:
			data.Sysfunc_sqrt_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_SQUARE_FLOAT:
			data.Sysfunc_square_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_PI_FLOAT:
			data.Sysfunc_pi_FLOAT(instr.Arg0.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_DEGREES_FLOAT:
			data.Sysfunc_degrees_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_RADIANS_FLOAT:
			data.Sysfunc_radians_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_RAND_FLOAT:
			data.Sysfunc_rand_FLOAT(instr.Arg0.(*data.FLOAT))

		// MISC

		case rsql.INSTR_SYSFUNC_STR_FLOAT:
			data.Sysfunc_str_FLOAT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		case rsql.INSTR_SYSFUNC_FORMAT_FLOAT:
			data.Sysfunc_format_FLOAT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_ISNUMERIC_FLOAT:
			data.Sysfunc_isnumeric_FLOAT(instr.Arg0.(*data.INT), instr.Arg1.(*data.FLOAT))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_FLOAT:
			data.Sysfunc_isnull_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_IIF_FLOAT:
			data.Sysfunc_iif_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.FLOAT), instr.Arg3.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_CHOOSE_FLOAT:
			data.Sysfunc_choose_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.INT), instr.Arg2.([]*data.FLOAT))

		case rsql.INSTR_SYSFUNC_COALESCE_FLOAT:
			data.Sysfunc_coalesce_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.([]*data.FLOAT))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_FLOAT:
			data.Sysfunc_typeof_FLOAT(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.FLOAT))

		//======================================================
		//                   DATE operations                  //
		//======================================================

		case rsql.INSTR_COMP_EQUAL_DATE:
			data.Comp_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_COMP_GREATER_DATE:
			data.Comp_greater_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_COMP_LESS_DATE:
			data.Comp_less_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_COMP_GREATER_EQUAL_DATE:
			data.Comp_greater_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_COMP_LESS_EQUAL_DATE:
			data.Comp_less_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_COMP_NOT_EQUAL_DATE:
			data.Comp_not_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_IS_NULL_DATE:
			data.Is_null_DATE(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATE))

		case rsql.INSTR_IS_NOT_NULL_DATE:
			data.Is_not_null_DATE(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATE))

		case rsql.INSTR_IN_LIST_DATE:
			data.In_list_DATE(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATE), instr.Arg2.([]*data.DATE))

		case rsql.INSTR_NOT_IN_LIST_DATE:
			data.Not_in_list_DATE(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATE), instr.Arg2.([]*data.DATE))

		case rsql.INSTR_CASE_DATE:
			data.Case_DATE(instr.Arg0.(*data.DATE), instr.Arg1.([]data.Case_element_t))

		//===== DATE CAST =====

		case rsql.INSTR_CAST_DATE_DATE:
			data.Cast_DATE_to_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.DATE))

		case rsql.INSTR_CAST_DATE_DATETIME:
			data.Cast_DATE_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATE))

		//===== DATE functions =====

		// DATEADD

		case rsql.INSTR_SYSFUNC_DATEADD_YEAR_DATE: // year
			data.Sysfunc_dateadd_year_DATETIME((*data.DATETIME)(instr.Arg0.(*data.DATE)), instr.Arg1.(*data.INT), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEADD_QUARTER_DATE: // quarter
			data.Sysfunc_dateadd_quarter_DATETIME((*data.DATETIME)(instr.Arg0.(*data.DATE)), instr.Arg1.(*data.INT), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEADD_MONTH_DATE: // month
			data.Sysfunc_dateadd_month_DATETIME((*data.DATETIME)(instr.Arg0.(*data.DATE)), instr.Arg1.(*data.INT), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEADD_DAY_DATE: // day, dayofyear, weekday
			data.Sysfunc_dateadd_day_DATETIME((*data.DATETIME)(instr.Arg0.(*data.DATE)), instr.Arg1.(*data.INT), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEADD_WEEK_DATE: // week
			data.Sysfunc_dateadd_week_DATETIME((*data.DATETIME)(instr.Arg0.(*data.DATE)), instr.Arg1.(*data.INT), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		// DATEDIFF

		case rsql.INSTR_SYSFUNC_DATEDIFF_YEAR_DATE: // year
			data.Sysfunc_datediff_year_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_QUARTER_DATE: // quarter
			data.Sysfunc_datediff_quarter_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MONTH_DATE: // month
			data.Sysfunc_datediff_month_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_DAY_DATE: // day, dayofyear
			data.Sysfunc_datediff_day_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_WEEK_DATE: // week
			data.Sysfunc_datediff_week_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_HOUR_DATE: // hour
			data.Sysfunc_datediff_hour_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MINUTE_DATE: // minute
			data.Sysfunc_datediff_minute_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_SECOND_DATE: // second
			data.Sysfunc_datediff_second_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), (*data.DATETIME)(instr.Arg2.(*data.DATE)))

		// DATEPART

		case rsql.INSTR_SYSFUNC_DATEPART_YEAR_DATE: // year
			data.Sysfunc_datepart_year_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEPART_QUARTER_DATE: // quarter
			data.Sysfunc_datepart_quarter_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEPART_MONTH_DATE: // month
			data.Sysfunc_datepart_month_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEPART_DAYOFYEAR_DATE: // dayofyear
			data.Sysfunc_datepart_dayofyear_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEPART_DAY_DATE: // day
			data.Sysfunc_datepart_day_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATEPART_WEEK_DATE: // week
			data.Sysfunc_datepart_week_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATEPART_WEEKDAY_DATE: // weekday
			data.Sysfunc_datepart_weekday_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)), instr.Arg2.(*data.SYSLANGUAGE))

		// DATENAME

		case rsql.INSTR_SYSFUNC_DATENAME_YEAR_DATE: // year
			data.Sysfunc_datename_year_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATENAME_QUARTER_DATE: // quarter
			data.Sysfunc_datename_quarter_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATENAME_MONTH_DATE: // month
			data.Sysfunc_datename_month_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATENAME_DAYOFYEAR_DATE: // dayofyear
			data.Sysfunc_datename_dayofyear_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATENAME_DAY_DATE: // day
			data.Sysfunc_datename_day_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_DATENAME_WEEK_DATE: // week
			data.Sysfunc_datename_week_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATENAME_WEEKDAY_DATE: // weekday
			data.Sysfunc_datename_weekday_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)), instr.Arg2.(*data.SYSLANGUAGE))

		// MISC

		case rsql.INSTR_SYSFUNC_EOMONTH_DATE:
			data.Sysfunc_eomonth_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.DATE), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_BOMONTH_DATE:
			data.Sysfunc_bomonth_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.DATE), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_FORMAT_DATE:
			data.Sysfunc_format_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.DATE)), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_CONVERT_DATE_TO_VARCHAR:
			data.Sysfunc_convert_DATE_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATE), instr.Arg2.(*data.INT), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATEFROMPARTS_DATE:
			data.Sysfunc_datefromparts_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT), instr.Arg3.(*data.INT))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_DATE:
			data.Sysfunc_isnull_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.DATE), instr.Arg2.(*data.DATE))

		case rsql.INSTR_SYSFUNC_IIF_DATE:
			data.Sysfunc_iif_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.DATE), instr.Arg3.(*data.DATE))

		case rsql.INSTR_SYSFUNC_CHOOSE_DATE:
			data.Sysfunc_choose_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.INT), instr.Arg2.([]*data.DATE))

		case rsql.INSTR_SYSFUNC_COALESCE_DATE:
			data.Sysfunc_coalesce_DATE(instr.Arg0.(*data.DATE), instr.Arg1.([]*data.DATE))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_DATE:
			data.Sysfunc_typeof_DATE(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATE))

		//======================================================
		//                   TIME operations                  //
		//======================================================

		case rsql.INSTR_COMP_EQUAL_TIME:
			data.Comp_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_COMP_GREATER_TIME:
			data.Comp_greater_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_COMP_LESS_TIME:
			data.Comp_less_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_COMP_GREATER_EQUAL_TIME:
			data.Comp_greater_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_COMP_LESS_EQUAL_TIME:
			data.Comp_less_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_COMP_NOT_EQUAL_TIME:
			data.Comp_not_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_IS_NULL_TIME:
			data.Is_null_TIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TIME))

		case rsql.INSTR_IS_NOT_NULL_TIME:
			data.Is_not_null_TIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TIME))

		case rsql.INSTR_IN_LIST_TIME:
			data.In_list_TIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TIME), instr.Arg2.([]*data.TIME))

		case rsql.INSTR_NOT_IN_LIST_TIME:
			data.Not_in_list_TIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.TIME), instr.Arg2.([]*data.TIME))

		case rsql.INSTR_CASE_TIME:
			data.Case_TIME(instr.Arg0.(*data.TIME), instr.Arg1.([]data.Case_element_t))

		//===== TIME CAST =====

		case rsql.INSTR_CAST_TIME_TIME:
			data.Cast_TIME_to_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.TIME))

		case rsql.INSTR_CAST_TIME_DATETIME:
			data.Cast_TIME_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.TIME))

		//===== TIME functions =====

		// DATEADD

		case rsql.INSTR_SYSFUNC_DATEADD_HOUR_TIME: // hour
			data.Sysfunc_dateadd_hour_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.TIME))

		case rsql.INSTR_SYSFUNC_DATEADD_MINUTE_TIME: // minute
			data.Sysfunc_dateadd_minute_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.TIME))

		case rsql.INSTR_SYSFUNC_DATEADD_SECOND_TIME: // second
			data.Sysfunc_dateadd_second_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.TIME))

		case rsql.INSTR_SYSFUNC_DATEADD_MILLISECOND_TIME: // millisecond
			data.Sysfunc_dateadd_millisecond_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.TIME))

		case rsql.INSTR_SYSFUNC_DATEADD_MICROSECOND_TIME: // microsecond
			data.Sysfunc_dateadd_microsecond_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.TIME))

		case rsql.INSTR_SYSFUNC_DATEADD_NANOSECOND_TIME: // nanosecond
			data.Sysfunc_dateadd_nanosecond_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.TIME))

		// DATEDIFF

		case rsql.INSTR_SYSFUNC_DATEDIFF_HOUR_TIME: // hour
			data.Sysfunc_datediff_hour_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MINUTE_TIME: // minute
			data.Sysfunc_datediff_minute_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_SECOND_TIME: // second
			data.Sysfunc_datediff_second_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MILLISECOND_TIME: // millisecond
			data.Sysfunc_datediff_millisecond_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MICROSECOND_TIME: // microsecond
			data.Sysfunc_datediff_microsecond_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEDIFF_NANOSECOND_TIME: // nanosecond
			data.Sysfunc_datediff_nanosecond_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)), (*data.DATETIME)(instr.Arg2.(*data.TIME)))

		// DATEPART

		case rsql.INSTR_SYSFUNC_DATEPART_HOUR_TIME: // hour
			data.Sysfunc_datepart_hour_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEPART_MINUTE_TIME: // minute
			data.Sysfunc_datepart_minute_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEPART_SECOND_TIME: // second
			data.Sysfunc_datepart_second_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEPART_MILLISECOND_TIME: // millisecond
			data.Sysfunc_datepart_millisecond_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEPART_MICROSECOND_TIME: // microsecond
			data.Sysfunc_datepart_microsecond_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATEPART_NANOSECOND_TIME: // nanosecond
			data.Sysfunc_datepart_nanosecond_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		// DATENAME

		case rsql.INSTR_SYSFUNC_DATENAME_HOUR_TIME: // hour
			data.Sysfunc_datename_hour_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATENAME_MINUTE_TIME: // minute
			data.Sysfunc_datename_minute_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATENAME_SECOND_TIME: // second
			data.Sysfunc_datename_second_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATENAME_MILLISECOND_TIME: // millisecond
			data.Sysfunc_datename_millisecond_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATENAME_MICROSECOND_TIME: // microsecond
			data.Sysfunc_datename_microsecond_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_DATENAME_NANOSECOND_TIME: // nanosecond
			data.Sysfunc_datename_nanosecond_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		// MISC

		case rsql.INSTR_SYSFUNC_FORMAT_TIME:
			data.Sysfunc_format_DATETIME(instr.Arg0.(*data.VARCHAR), (*data.DATETIME)(instr.Arg1.(*data.TIME)), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_CONVERT_TIME_TO_VARCHAR:
			data.Sysfunc_convert_TIME_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.TIME), instr.Arg2.(*data.INT), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_TIMEFROMPARTS_TIME:
			arg_slice_INT := instr.Arg1.([]*data.INT)
			data.Sysfunc_timefromparts_TIME(instr.Arg0.(*data.TIME), arg_slice_INT[0], arg_slice_INT[1], arg_slice_INT[2], arg_slice_INT[3], arg_slice_INT[4])

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_TIME:
			data.Sysfunc_isnull_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.TIME), instr.Arg2.(*data.TIME))

		case rsql.INSTR_SYSFUNC_IIF_TIME:
			data.Sysfunc_iif_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.TIME), instr.Arg3.(*data.TIME))

		case rsql.INSTR_SYSFUNC_CHOOSE_TIME:
			data.Sysfunc_choose_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.INT), instr.Arg2.([]*data.TIME))

		case rsql.INSTR_SYSFUNC_COALESCE_TIME:
			data.Sysfunc_coalesce_TIME(instr.Arg0.(*data.TIME), instr.Arg1.([]*data.TIME))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_TIME:
			data.Sysfunc_typeof_TIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.TIME))

		//======================================================
		//                   DATETIME operations              //
		//======================================================

		case rsql.INSTR_ADD_DATE_TIME_DATETIME:
			data.Add_date_time_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATE), instr.Arg2.(*data.TIME))

		case rsql.INSTR_ADD_DATETIME_INT:
			data.Sysfunc_dateadd_day_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg2.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SUBTRACT_DATETIME_INT:
			data.Sysfunc_datetime_subtract_day_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg2.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_COMP_EQUAL_DATETIME:
			data.Comp_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_COMP_GREATER_DATETIME:
			data.Comp_greater_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_COMP_LESS_DATETIME:
			data.Comp_less_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_COMP_GREATER_EQUAL_DATETIME:
			data.Comp_greater_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_COMP_LESS_EQUAL_DATETIME:
			data.Comp_less_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_COMP_NOT_EQUAL_DATETIME:
			data.Comp_not_equal_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_IS_NULL_DATETIME:
			data.Is_null_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_IS_NOT_NULL_DATETIME:
			data.Is_not_null_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_IN_LIST_DATETIME:
			data.In_list_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.([]*data.DATETIME))

		case rsql.INSTR_NOT_IN_LIST_DATETIME:
			data.Not_in_list_DATETIME(instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*data.DATETIME), instr.Arg2.([]*data.DATETIME))

		case rsql.INSTR_CASE_DATETIME:
			data.Case_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.([]data.Case_element_t))

		//===== DATETIME CAST =====

		case rsql.INSTR_CAST_DATETIME_DATE:
			data.Cast_DATETIME_to_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_CAST_DATETIME_TIME:
			data.Cast_DATETIME_to_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_CAST_DATETIME_DATETIME:
			data.Cast_DATETIME_to_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME))

		//===== DATETIME functions =====

		// DATEADD

		case rsql.INSTR_SYSFUNC_DATEADD_YEAR_DATETIME: // year
			data.Sysfunc_dateadd_year_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_QUARTER_DATETIME: // quarter
			data.Sysfunc_dateadd_quarter_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_MONTH_DATETIME: // month
			data.Sysfunc_dateadd_month_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_DAY_DATETIME: // day, dayofyear, weekday
			data.Sysfunc_dateadd_day_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_WEEK_DATETIME: // week
			data.Sysfunc_dateadd_week_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_HOUR_DATETIME: // hour
			data.Sysfunc_dateadd_hour_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_MINUTE_DATETIME: // minute
			data.Sysfunc_dateadd_minute_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_SECOND_DATETIME: // second
			data.Sysfunc_dateadd_second_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_MILLISECOND_DATETIME: // millisecond
			data.Sysfunc_dateadd_millisecond_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_MICROSECOND_DATETIME: // microsecond
			data.Sysfunc_dateadd_microsecond_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEADD_NANOSECOND_DATETIME: // nanosecond
			data.Sysfunc_dateadd_nanosecond_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.(*data.DATETIME))

		// DATEDIFF

		case rsql.INSTR_SYSFUNC_DATEDIFF_YEAR_DATETIME: // year
			data.Sysfunc_datediff_year_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_QUARTER_DATETIME: // quarter
			data.Sysfunc_datediff_quarter_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MONTH_DATETIME: // month
			data.Sysfunc_datediff_month_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_DAY_DATETIME: // day, dayofyear
			data.Sysfunc_datediff_day_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_WEEK_DATETIME: // week
			data.Sysfunc_datediff_week_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_HOUR_DATETIME: // hour
			data.Sysfunc_datediff_hour_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MINUTE_DATETIME: // minute
			data.Sysfunc_datediff_minute_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_SECOND_DATETIME: // second
			data.Sysfunc_datediff_second_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MILLISECOND_DATETIME: // millisecond
			data.Sysfunc_datediff_millisecond_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_MICROSECOND_DATETIME: // microsecond
			data.Sysfunc_datediff_microsecond_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEDIFF_NANOSECOND_DATETIME: // nanosecond
			data.Sysfunc_datediff_nanosecond_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		// DATEPART

		case rsql.INSTR_SYSFUNC_DATEPART_YEAR_DATETIME: // year
			data.Sysfunc_datepart_year_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_QUARTER_DATETIME: // quarter
			data.Sysfunc_datepart_quarter_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_MONTH_DATETIME: // month
			data.Sysfunc_datepart_month_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_DAYOFYEAR_DATETIME: // dayofyear
			data.Sysfunc_datepart_dayofyear_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_DAY_DATETIME: // day
			data.Sysfunc_datepart_day_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_WEEK_DATETIME: // week
			data.Sysfunc_datepart_week_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATEPART_WEEKDAY_DATETIME: // weekday
			data.Sysfunc_datepart_weekday_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATEPART_HOUR_DATETIME: // hour
			data.Sysfunc_datepart_hour_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_MINUTE_DATETIME: // minute
			data.Sysfunc_datepart_minute_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_SECOND_DATETIME: // second
			data.Sysfunc_datepart_second_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_MILLISECOND_DATETIME: // millisecond
			data.Sysfunc_datepart_millisecond_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_MICROSECOND_DATETIME: // microsecond
			data.Sysfunc_datepart_microsecond_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATEPART_NANOSECOND_DATETIME: // nanosecond
			data.Sysfunc_datepart_nanosecond_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		// DATENAME

		case rsql.INSTR_SYSFUNC_DATENAME_YEAR_DATETIME: // year
			data.Sysfunc_datename_year_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_QUARTER_DATETIME: // quarter
			data.Sysfunc_datename_quarter_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_MONTH_DATETIME: // month
			data.Sysfunc_datename_month_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATENAME_DAYOFYEAR_DATETIME: // dayofyear
			data.Sysfunc_datename_dayofyear_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_DAY_DATETIME: // day
			data.Sysfunc_datename_day_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_WEEK_DATETIME: // week
			data.Sysfunc_datename_week_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATENAME_WEEKDAY_DATETIME: // weekday
			data.Sysfunc_datename_weekday_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATENAME_HOUR_DATETIME: // hour
			data.Sysfunc_datename_hour_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_MINUTE_DATETIME: // minute
			data.Sysfunc_datename_minute_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_SECOND_DATETIME: // second
			data.Sysfunc_datename_second_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_MILLISECOND_DATETIME: // millisecond
			data.Sysfunc_datename_millisecond_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_MICROSECOND_DATETIME: // microsecond
			data.Sysfunc_datename_microsecond_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_DATENAME_NANOSECOND_DATETIME: // nanosecond
			data.Sysfunc_datename_nanosecond_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		// MISC

		case rsql.INSTR_SYSFUNC_EOMONTH_DATETIME:
			data.Sysfunc_eomonth_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_BOMONTH_DATETIME:
			data.Sysfunc_bomonth_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_FORMAT_DATETIME:
			data.Sysfunc_format_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_CONVERT_DATETIME_TO_VARCHAR:
			data.Sysfunc_convert_DATETIME_to_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.INT), instr.Arg3.(*data.SYSLANGUAGE))

		case rsql.INSTR_SYSFUNC_DATETIMEFROMPARTS_DATETIME:
			arg_slice_INT := instr.Arg1.([]*data.INT)
			data.Sysfunc_datetimefromparts_DATETIME(instr.Arg0.(*data.DATETIME), arg_slice_INT[0], arg_slice_INT[1], arg_slice_INT[2], arg_slice_INT[3], arg_slice_INT[4], arg_slice_INT[5], arg_slice_INT[6])

		case rsql.INSTR_SYSFUNC_DATETIME2FROMPARTS_DATETIME:
			arg_slice_INT := instr.Arg1.([]*data.INT)
			data.Sysfunc_datetime2fromparts_DATETIME(instr.Arg0.(*data.DATETIME), arg_slice_INT[0], arg_slice_INT[1], arg_slice_INT[2], arg_slice_INT[3], arg_slice_INT[4], arg_slice_INT[5], arg_slice_INT[6], arg_slice_INT[7])

		case rsql.INSTR_SYSFUNC_GETUTCDATE_DATETIME: // used to assign current utc datetime to _@current_timestamp
			data.Sysfunc_getutcdate_DATETIME(instr.Arg0.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_UTC_TO_LOCAL_DATETIME: // used e.g. for PRINT GETDATE()
			data.Sysfunc_utc_to_local_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME))

		// ISNULL, IIF, CHOOSE, COALESCE

		case rsql.INSTR_SYSFUNC_ISNULL_DATETIME:
			data.Sysfunc_isnull_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME), instr.Arg2.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_IIF_DATETIME:
			data.Sysfunc_iif_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.BOOLEAN), instr.Arg2.(*data.DATETIME), instr.Arg3.(*data.DATETIME))

		case rsql.INSTR_SYSFUNC_CHOOSE_DATETIME:
			data.Sysfunc_choose_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.INT), instr.Arg2.([]*data.DATETIME))

		case rsql.INSTR_SYSFUNC_COALESCE_DATETIME:
			data.Sysfunc_coalesce_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.([]*data.DATETIME))

		// typeof

		case rsql.INSTR_SYSFUNC_TYPEOF_DATETIME:
			data.Sysfunc_typeof_DATETIME(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.DATETIME))

		//======================================================
		//                    RAND functions                  //
		//======================================================

		case rsql.INSTR_SYSFUNC_RANDOM_VARCHAR:
			data.Sysfunc_random_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_RANDOM_INT:
			data.Sysfunc_random_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_RANDOM_BIGINT:
			data.Sysfunc_random_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_RANDOM_NUMERIC:
			data.Sysfunc_random_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.INT), instr.Arg2.(*data.INT))

		case rsql.INSTR_SYSFUNC_RANDOM_FLOAT:
			data.Sysfunc_random_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT), instr.Arg2.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_RANDOM_DATE:
			data.Sysfunc_random_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.DATE), instr.Arg2.(*data.DATE))

		//======================================================
		//          DICTIONARY and SYSTEM functions           //
		//======================================================

		case rsql.INSTR_SYSFUNC_SUSER_NAME_VARCHAR:
			data.Sysfunc_suser_name_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_SUSER_ID_BIGINT:
			data.Sysfunc_suser_id_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_DB_NAME_VARCHAR:
			data.Sysfunc_db_name_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_DB_ID_BIGINT:
			data.Sysfunc_db_id_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_SCHEMA_NAME_VARCHAR:
			data.Sysfunc_schema_name_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_SCHEMA_ID_BIGINT:
			data.Sysfunc_schema_id_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_OBJECT_ID_BIGINT:
			data.Sysfunc_object_id_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.VARCHAR), instr.Arg3.(*data.VARCHAR), instr.Arg4.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_USER_NAME_VARCHAR: // in fact, it returns user or role
			data.Sysfunc_principal_name_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_USER_ID_BIGINT: // in fact, it returns user or role
			data.Sysfunc_principal_id_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT), instr.Arg2.(*data.VARCHAR))

		//======================================================
		//                   misc. functions                  //
		//======================================================

		case rsql.INSTR_SYSFUNC_atatVERSION:
			data.Sysfunc_atatversion(instr.Arg0.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_atatSERVERNAME:
			data.Sysfunc_atatservername(instr.Arg0.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_atatSERVICENAME:
			data.Sysfunc_atatservicename(instr.Arg0.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_atatIDENTITY:
			data.Sysfunc_scope_identity(instr.Arg0.(*data.BIGINT), context) // same function as INSTR_SYSFUNC_SCOPE_IDENTITY below

		case rsql.INSTR_SYSFUNC_atatROWCOUNT:
			data.Sysfunc_atatrowcount(instr.Arg0.(*data.BIGINT), context)

		case rsql.INSTR_SYSFUNC_atatERROR:
			data.Sysfunc_ataterror(instr.Arg0.(*data.INT))

		case rsql.INSTR_SYSFUNC_atatTRANCOUNT:
			data.Sysfunc_atattrancount(instr.Arg0.(*data.INT), context)

		case rsql.INSTR_SYSFUNC_SCOPE_IDENTITY:
			data.Sysfunc_scope_identity(instr.Arg0.(*data.BIGINT), context)

		//======================================================
		//                aggregation functions               //
		//======================================================

		// COUNT

		case rsql.INSTR_SYSFUNC_COUNT_VARBINARY:
			data.Sysfunc_aggr_count_VARBINARY(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_COUNT_VARCHAR:
			data.Sysfunc_aggr_count_VARCHAR(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_COUNT_BIT:
			data.Sysfunc_aggr_count_BIT(instr.Arg0.(*data.INT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_SYSFUNC_COUNT_TINYINT:
			data.Sysfunc_aggr_count_TINYINT(instr.Arg0.(*data.INT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_SYSFUNC_COUNT_SMALLINT:
			data.Sysfunc_aggr_count_SMALLINT(instr.Arg0.(*data.INT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_COUNT_INT:
			data.Sysfunc_aggr_count_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_COUNT_BIGINT:
			data.Sysfunc_aggr_count_BIGINT(instr.Arg0.(*data.INT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_COUNT_MONEY:
			data.Sysfunc_aggr_count_NUMERIC(instr.Arg0.(*data.INT), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_COUNT_NUMERIC:
			data.Sysfunc_aggr_count_NUMERIC(instr.Arg0.(*data.INT), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_COUNT_FLOAT:
			data.Sysfunc_aggr_count_FLOAT(instr.Arg0.(*data.INT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_COUNT_DATE:
			data.Sysfunc_aggr_count_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_COUNT_TIME:
			data.Sysfunc_aggr_count_DATETIME(instr.Arg0.(*data.INT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_COUNT_DATETIME:
			data.Sysfunc_aggr_count_DATETIME(instr.Arg0.(*data.INT), instr.Arg1.(*data.DATETIME))

		// COUNT_BIG

		case rsql.INSTR_SYSFUNC_COUNT_BIG_VARBINARY:
			data.Sysfunc_aggr_count_big_VARBINARY(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_VARCHAR:
			data.Sysfunc_aggr_count_big_VARCHAR(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_BIT:
			data.Sysfunc_aggr_count_big_BIT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_TINYINT:
			data.Sysfunc_aggr_count_big_TINYINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_SMALLINT:
			data.Sysfunc_aggr_count_big_SMALLINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_INT:
			data.Sysfunc_aggr_count_big_INT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_BIGINT:
			data.Sysfunc_aggr_count_big_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_MONEY:
			data.Sysfunc_aggr_count_big_NUMERIC(instr.Arg0.(*data.BIGINT), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_NUMERIC:
			data.Sysfunc_aggr_count_big_NUMERIC(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_FLOAT:
			data.Sysfunc_aggr_count_big_FLOAT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_DATE:
			data.Sysfunc_aggr_count_big_DATETIME(instr.Arg0.(*data.BIGINT), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_TIME:
			data.Sysfunc_aggr_count_big_DATETIME(instr.Arg0.(*data.BIGINT), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_COUNT_BIG_DATETIME:
			data.Sysfunc_aggr_count_big_DATETIME(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.DATETIME))

		// SUM

		case rsql.INSTR_SYSFUNC_SUM_BIT:
			data.Sysfunc_aggr_sum_BIT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_SYSFUNC_SUM_TINYINT:
			data.Sysfunc_aggr_sum_TINYINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_SYSFUNC_SUM_SMALLINT:
			data.Sysfunc_aggr_sum_SMALLINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_SUM_INT:
			data.Sysfunc_aggr_sum_INT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_SUM_BIGINT:
			data.Sysfunc_aggr_sum_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_SUM_MONEY:
			data.Sysfunc_aggr_sum_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_SUM_NUMERIC:
			data.Sysfunc_aggr_sum_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_SUM_FLOAT:
			data.Sysfunc_aggr_sum_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		// MIN

		case rsql.INSTR_SYSFUNC_MIN_VARBINARY:
			data.Sysfunc_aggr_min_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_MIN_VARCHAR:
			data.Sysfunc_aggr_min_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.SYSCOLLATOR))

		case rsql.INSTR_SYSFUNC_MIN_BIT:
			data.Sysfunc_aggr_min_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_SYSFUNC_MIN_TINYINT:
			data.Sysfunc_aggr_min_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_SYSFUNC_MIN_SMALLINT:
			data.Sysfunc_aggr_min_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_MIN_INT:
			data.Sysfunc_aggr_min_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_MIN_BIGINT:
			data.Sysfunc_aggr_min_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_MIN_MONEY:
			data.Sysfunc_aggr_min_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_MIN_NUMERIC:
			data.Sysfunc_aggr_min_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_MIN_FLOAT:
			data.Sysfunc_aggr_min_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_MIN_DATE:
			data.Sysfunc_aggr_min_DATETIME((*data.DATETIME)(instr.Arg0.(*data.DATE)), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_MIN_TIME:
			data.Sysfunc_aggr_min_DATETIME((*data.DATETIME)(instr.Arg0.(*data.TIME)), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_MIN_DATETIME:
			data.Sysfunc_aggr_min_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME))

		// MAX

		case rsql.INSTR_SYSFUNC_MAX_VARBINARY:
			data.Sysfunc_aggr_max_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_SYSFUNC_MAX_VARCHAR:
			data.Sysfunc_aggr_max_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.SYSCOLLATOR))

		case rsql.INSTR_SYSFUNC_MAX_BIT:
			data.Sysfunc_aggr_max_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_SYSFUNC_MAX_TINYINT:
			data.Sysfunc_aggr_max_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_SYSFUNC_MAX_SMALLINT:
			data.Sysfunc_aggr_max_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_SYSFUNC_MAX_INT:
			data.Sysfunc_aggr_max_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_SYSFUNC_MAX_BIGINT:
			data.Sysfunc_aggr_max_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_SYSFUNC_MAX_MONEY:
			data.Sysfunc_aggr_max_NUMERIC((*data.NUMERIC)(instr.Arg0.(*data.MONEY)), (*data.NUMERIC)(instr.Arg1.(*data.MONEY)))

		case rsql.INSTR_SYSFUNC_MAX_NUMERIC:
			data.Sysfunc_aggr_max_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_SYSFUNC_MAX_FLOAT:
			data.Sysfunc_aggr_max_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_SYSFUNC_MAX_DATE:
			data.Sysfunc_aggr_max_DATETIME((*data.DATETIME)(instr.Arg0.(*data.DATE)), (*data.DATETIME)(instr.Arg1.(*data.DATE)))

		case rsql.INSTR_SYSFUNC_MAX_TIME:
			data.Sysfunc_aggr_max_DATETIME((*data.DATETIME)(instr.Arg0.(*data.TIME)), (*data.DATETIME)(instr.Arg1.(*data.TIME)))

		case rsql.INSTR_SYSFUNC_MAX_DATETIME:
			data.Sysfunc_aggr_max_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME))

		//======================================================
		//                 instructions ASSIGN                //
		//======================================================

		case rsql.INSTR_ASSIGN_SYSLANGUAGE:
			rsql_err = data.Assign_SYSLANGUAGE(instr.Arg0.(*data.SYSLANGUAGE), instr.Arg1.(*data.SYSLANGUAGE))

		case rsql.INSTR_ASSIGN_VARBINARY:
			rsql_err = data.Assign_VARBINARY(instr.Arg0.(*data.VARBINARY), instr.Arg1.(*data.VARBINARY))

		case rsql.INSTR_ASSIGN_VARCHAR:
			rsql_err = data.Assign_VARCHAR(instr.Arg0.(*data.VARCHAR), instr.Arg1.(*data.VARCHAR))

		case rsql.INSTR_ASSIGN_BIT:
			rsql_err = data.Assign_BIT(instr.Arg0.(*data.BIT), instr.Arg1.(*data.BIT))

		case rsql.INSTR_ASSIGN_TINYINT:
			rsql_err = data.Assign_TINYINT(instr.Arg0.(*data.TINYINT), instr.Arg1.(*data.TINYINT))

		case rsql.INSTR_ASSIGN_SMALLINT:
			rsql_err = data.Assign_SMALLINT(instr.Arg0.(*data.SMALLINT), instr.Arg1.(*data.SMALLINT))

		case rsql.INSTR_ASSIGN_INT:
			rsql_err = data.Assign_INT(instr.Arg0.(*data.INT), instr.Arg1.(*data.INT))

		case rsql.INSTR_ASSIGN_BIGINT:
			rsql_err = data.Assign_BIGINT(instr.Arg0.(*data.BIGINT), instr.Arg1.(*data.BIGINT))

		case rsql.INSTR_ASSIGN_MONEY:
			rsql_err = data.Assign_MONEY(instr.Arg0.(*data.MONEY), instr.Arg1.(*data.MONEY))

		case rsql.INSTR_ASSIGN_NUMERIC:
			rsql_err = data.Assign_NUMERIC(instr.Arg0.(*data.NUMERIC), instr.Arg1.(*data.NUMERIC))

		case rsql.INSTR_ASSIGN_FLOAT:
			rsql_err = data.Assign_FLOAT(instr.Arg0.(*data.FLOAT), instr.Arg1.(*data.FLOAT))

		case rsql.INSTR_ASSIGN_DATE:
			rsql_err = data.Assign_DATE(instr.Arg0.(*data.DATE), instr.Arg1.(*data.DATE))

		case rsql.INSTR_ASSIGN_TIME:
			rsql_err = data.Assign_TIME(instr.Arg0.(*data.TIME), instr.Arg1.(*data.TIME))

		case rsql.INSTR_ASSIGN_DATETIME:
			rsql_err = data.Assign_DATETIME(instr.Arg0.(*data.DATETIME), instr.Arg1.(*data.DATETIME))

		//======================================================
		//                     THROW statement                //
		//======================================================

		case rsql.INSTR_STMT_THROW:
			rsql_err = stmt.THROW(instr.Arg0.(*data.INT), instr.Arg1.(*data.VARCHAR), instr.Arg2.(*data.INT), instr.Arg3.(*data.TINYINT))

		//======================================================
		//                     PRINT statement                //
		//======================================================

		case rsql.INSTR_STMT_PRINT:
			rsql_err = stmt.PRINT(context, instr.Arg0.([]rsql.IDataslot))

		//======================================================
		//              TRANSACTION statements               //
		//======================================================

		case rsql.INSTR_STMT_BEGIN_TRANSACTION:
			rsql_err = stmt.BEGIN_TRANSACTION(context)

		case rsql.INSTR_STMT_COMMIT_TRANSACTION:
			rsql_err = stmt.COMMIT_TRANSACTION(context)

		case rsql.INSTR_STMT_ROLLBACK_TRANSACTION:
			rsql_err = stmt.ROLLBACK_TRANSACTION(context)

		//======================================================
		//                SET NOCOUNT statement               //
		//======================================================

		case rsql.INSTR_STMT_SET_NOCOUNT:
			rsql_err = stmt.SET_NOCOUNT(context, instr.Arg0.(bool))

		//======================================================
		//                    ASSERT statement                //
		//======================================================

		case rsql.INSTR_STMT_ASSERT_:
			rsql_err = stmt.ASSERT_(instr.Arg0.(*data.BOOLEAN))

		case rsql.INSTR_STMT_ASSERT_NULL_:
			rsql_err = stmt.ASSERT_NULL_(instr.Arg0.(rsql.IDataslot))

		case rsql.INSTR_STMT_ASSERT_ERROR_:
			rsql_err = stmt.ASSERT_ERROR_(instr.Arg0.(rsql.IDataslot), instr.Arg1.(*data.VARCHAR))

		//======================================================
		//               misc dictionary statements           //
		//======================================================

		case rsql.INSTR_STMT_USE: // as this statement updates many global variables, it is always the last instruction in a Basicblock.
			rsql_err = stmt.USE(context, instr.Arg0.(string), instr.Arg1.([]rsql.IDataslot)) // update _@current_db_name, _@current_schema_name, _@current_user_name, etc

		case rsql.INSTR_STMT_ALTER_SERVER_PARAMETER:
			rsql_err = stmt.ALTER_SERVER_PARAMETER(context, instr.Arg0.(dict.Server_parameter_t), instr.Arg1.(int64), instr.Arg2.(string))

		case rsql.INSTR_STMT_CREATE_LOGIN:
			rsql_err = stmt.CREATE_LOGIN(context, instr.Arg0.(string), instr.Arg1.(dict.Login_params))

		case rsql.INSTR_STMT_ALTER_LOGIN:
			rsql_err = stmt.ALTER_LOGIN(context, instr.Arg0.(string), instr.Arg1.(dict.Login_params))

		case rsql.INSTR_STMT_DROP_LOGIN:
			rsql_err = stmt.DROP_LOGIN(context, instr.Arg0.(string))

		case rsql.INSTR_STMT_CREATE_DATABASE:
			rsql_err = stmt.CREATE_DATABASE(context, instr.Arg0.(string))

		case rsql.INSTR_STMT_ALTER_DATABASE:
			rsql_err = stmt.ALTER_DATABASE(context, instr.Arg0.(string), instr.Arg1.(dict.Database_params))

		case rsql.INSTR_STMT_DROP_DATABASE:
			rsql_err = stmt.DROP_DATABASE(context, instr.Arg0.(string))

		case rsql.INSTR_STMT_CREATE_USER:
			rsql_err = stmt.CREATE_USER(context, instr.Arg0.(string), instr.Arg1.(string), instr.Arg2.(string))

		case rsql.INSTR_STMT_ALTER_USER:
			rsql_err = stmt.ALTER_USER(context, instr.Arg0.(string), instr.Arg1.(string), instr.Arg2.(string), instr.Arg3.(string))

		case rsql.INSTR_STMT_DROP_USER:
			rsql_err = stmt.DROP_USER(context, instr.Arg0.(string), instr.Arg1.(string))

		case rsql.INSTR_STMT_CREATE_ROLE:
			rsql_err = stmt.CREATE_ROLE(context, instr.Arg0.(string), instr.Arg1.(string))

		case rsql.INSTR_STMT_ALTER_ROLE:
			rsql_err = stmt.ALTER_ROLE(context, instr.Arg0.(string), instr.Arg1.(string), instr.Arg2.(string), instr.Arg3.(string), instr.Arg4.(string))

		case rsql.INSTR_STMT_DROP_ROLE:
			rsql_err = stmt.DROP_ROLE(context, instr.Arg0.(string), instr.Arg1.(string))

		case rsql.INSTR_STMT_GRANT:
			rsql_err = stmt.GRANT(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(rsql.Permission_t), instr.Arg2.([]string))

		case rsql.INSTR_STMT_DENY:
			rsql_err = stmt.DENY(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(rsql.Permission_t), instr.Arg2.([]string))

		case rsql.INSTR_STMT_REVOKE:
			rsql_err = stmt.REVOKE(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(rsql.Permission_t), instr.Arg2.([]string), instr.Arg3.(bool))

		case rsql.INSTR_STMT_ALTER_AUTHORIZATION:
			rsql_err = stmt.ALTER_AUTHORIZATION(context, instr.Arg0.(string), instr.Arg1.(string))

		case rsql.INSTR_STMT_CREATE_TABLE:
			rsql_err = stmt.CREATE_TABLE(context, instr.Arg0.(string), instr.Arg1.(string), instr.Arg2.(*rsql.GTabledef))

		case rsql.INSTR_STMT_ALTER_TABLE:
			rsql_err = stmt.ALTER_TABLE(context, instr.Arg0.(dict.Table_params))

		case rsql.INSTR_STMT_DROP_TABLE:
			rsql_err = stmt.DROP_TABLE(context, instr.Arg0.(rsql.Object_qname_t))

		case rsql.INSTR_STMT_CREATE_INDEX:
			rsql_err = stmt.CREATE_INDEX(context, instr.Arg0.(dict.Index_params))

		case rsql.INSTR_STMT_ALTER_INDEX:
			rsql_err = stmt.ALTER_INDEX(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(string), instr.Arg2.(string), instr.Arg3.(string))

		case rsql.INSTR_STMT_DROP_INDEX:
			rsql_err = stmt.DROP_INDEX(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(string))

		//======================================================
		//      INSERT, UPDATE, DELETE, SELECT statements     //
		//======================================================

		case rsql.INSTR_STMT_INSERT_VALUES:
			rsql_err = stmt.INSERT_VALUES(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(*rsql.GTabledef), instr.Arg2.([]rsql.Row), instr.Arg3.(bool))

		case rsql.INSTR_STMT_INSERT_SELECT:
			rsql_err = stmt.INSERT_SELECT(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(*rsql.GTabledef), instr.Arg2.(rsql.Insert_select_params))

		case rsql.INSTR_STMT_BULK_INSERT:
			rsql_err = stmt.BULK_INSERT(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(string), instr.Arg2.(rsql.Bulk_insert_params), instr.Arg3.(*rsql.GTabledef), instr.Arg4.(*data.SYSLANGUAGE))

		case rsql.INSTR_STMT_BULK_EXPORT:
			rsql_err = stmt.BULK_EXPORT(context, instr.Arg0.(csr.Cursor), instr.Arg1.(string), instr.Arg2.(rsql.Bulk_export_params))

		case rsql.INSTR_STMT_SELECT_OR_UNION:
			rsql_err = stmt.SELECT_OR_UNION(context, instr.Arg0.(csr.Cursor), instr.Arg1.(*csr.Cursor_table), instr.Arg2.([]string), instr.Arg3.(*rsql.Select_params))

		case rsql.INSTR_STMT_DELETE:
			rsql_err = stmt.DELETE(context, instr.Arg0.(csr.Cursor), instr.Arg1.(*csr.Cursor_table))

		case rsql.INSTR_STMT_UPDATE:
			rsql_err = stmt.UPDATE(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(rsql.Update_params))

		case rsql.INSTR_STMT_TRUNCATE_TABLE:
			rsql_err = stmt.TRUNCATE_TABLE(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(*rsql.GTabledef), instr.Arg2.(bool))

		case rsql.INSTR_STMT_SHRINK_TABLE:
			rsql_err = stmt.SHRINK_TABLE(context, instr.Arg0.(rsql.Object_qname_t), instr.Arg1.(*rsql.GTabledef))

		//======================================================
		//                    SHOW statements                 //
		//======================================================

		case rsql.INSTR_STMT_SHOW_COLLATIONS:
			rsql_err = stmt.SHOW_COLLATIONS(context)

		case rsql.INSTR_STMT_SHOW_LANGUAGES:
			rsql_err = stmt.SHOW_LANGUAGES(context)

		case rsql.INSTR_STMT_SHOW_LOCKS:
			rsql_err = stmt.SHOW_LOCKS(context)

		case rsql.INSTR_STMT_SHOW_WORKERS:
			rsql_err = stmt.SHOW_WORKERS(context)

		case rsql.INSTR_STMT_SHOW_INFO:
			rsql_err = stmt.SHOW_INFO(context, instr.Arg0.(rsql.Show_info_params))

		case rsql.INSTR_STMT_SHOW_TABLE:
			rsql_err = stmt.SHOW_TABLE(context, instr.Arg0.(rsql.Show_params))

		case rsql.INSTR_STMT_SHOW_DATABASE:
			rsql_err = stmt.SHOW_DATABASE(context, instr.Arg0.(rsql.Show_params))

		case rsql.INSTR_STMT_SHOW_LOGIN:
			rsql_err = stmt.SHOW_LOGIN(context, instr.Arg0.(rsql.Show_params))

		case rsql.INSTR_STMT_SHOW_PARAMETER:
			rsql_err = stmt.SHOW_PARAMETER(context, instr.Arg0.(rsql.Show_params))

		case rsql.INSTR_STMT_SHOW_USER:
			rsql_err = stmt.SHOW_USER(context, instr.Arg0.(rsql.Show_params))

		case rsql.INSTR_STMT_SHOW_ROLE:
			rsql_err = stmt.SHOW_ROLE(context, instr.Arg0.(rsql.Show_params))

		//======================================================
		//               BACKUP and DUMP statements           //
		//======================================================

		case rsql.INSTR_STMT_BACKUP:
			rsql_err = stmt.BACKUP(context, instr.Arg0.(string), instr.Arg1.(string), instr.Arg2.(rsql.Backup_params))

		case rsql.INSTR_STMT_RESTORE:
			rsql_err = stmt.RESTORE(context, instr.Arg0.(string), instr.Arg1.(string), instr.Arg2.(rsql.Restore_params))

		case rsql.INSTR_STMT_DUMP_PARAMETERS:
			rsql_err = stmt.DUMP_PARAMETERS(context, instr.Arg0.(rsql.Dump_params))

		case rsql.INSTR_STMT_DUMP_LOGINS:
			rsql_err = stmt.DUMP_LOGINS(context, instr.Arg0.(rsql.Dump_params))

		case rsql.INSTR_STMT_DUMP_DATABASE:
			rsql_err = stmt.DUMP_DATABASE(context, instr.Arg0.(rsql.Dump_params))

		//======================================================
		//                   other statements                 //
		//======================================================

		case rsql.INSTR_STMT_SLEEP:
			rsql_err = stmt.SLEEP(context, instr.Arg0.(float64))

		case rsql.INSTR_STMT_SHUTDOWN:
			rsql_err = stmt.SHUTDOWN(context, instr.Arg0.(bool))

		//======================================================
		//                         jumps                      //
		//======================================================

		case rsql.INSTR_JUMP_OUT_TRUE_FALSE:
			if rsql_err = data.Jump_out_true_false(basicblock, instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*rsql.Basicblock), instr.Arg2.(*rsql.Basicblock)); rsql_err != nil {
				rsql_err.Batch_line_no = instr.Batch_line_no
				rsql_err.Batch_line_pos = instr.Batch_line_pos
				return 0, rsql_err
			}
			return 0, nil

		case rsql.INSTR_JUMP_OUT:
			data.Jump_out(basicblock, instr.Arg0.(*rsql.Basicblock))
			return 0, nil

		case rsql.INSTR_JUMP_IF_NULL_OR_FALSE:
			if rsql_err = data.Jump_if_NULL_or_false(basicblock, instr.Arg0.(*data.BOOLEAN), instr.Arg1.(*rsql.Basicblock)); rsql_err != nil {
				rsql_err.Batch_line_no = instr.Batch_line_no
				rsql_err.Batch_line_pos = instr.Batch_line_pos
				return 0, rsql_err
			}
			if basicblock.Bb_jump_target != nil {
				return 0, nil
			}

		case rsql.INSTR_JUMP_RETURN:
			if retval, rsql_err = data.Jump_return(basicblock, instr.Arg0.(*data.INT)); rsql_err != nil {
				rsql_err.Batch_line_no = instr.Batch_line_no
				rsql_err.Batch_line_pos = instr.Batch_line_pos
				return 0, rsql_err
			}
			return retval, nil

		default:
			panic("unknown instr code")
		} // end switch

		// if the result of an operator or a function is an error, write the line/pos of the instruction.

		if instr.Code < rsql.INSTR_ARG0_IS_EXPRESSION_RESULT_SENTINEL_xxxxxxxxx && instr.Arg0.(rsql.IDataslot).Error() != nil { // for these instructions, Arg0 is always the result dataslot
			result_rsql_err := instr.Arg0.(rsql.IDataslot).Error()
			if result_rsql_err.Batch_line_no == 0 { // as error propagates down the expression tree, just write the line/pos of the instruction which created the error
				result_rsql_err.Batch_line_no = instr.Batch_line_no
				result_rsql_err.Batch_line_pos = instr.Batch_line_pos
			}
		}

		// check if statement returned an error

		if rsql_err != nil {
			if rsql_err.Batch_line_no == 0 { // if error comes from an expression, keep the line/pos of the error.
				rsql_err.Batch_line_no = instr.Batch_line_no // else, update line/pos of the error.
				rsql_err.Batch_line_pos = instr.Batch_line_pos
			}

			if rsql_err.Severity_id != rsql.ERROR_IS_WARNING { // if real error, stop execution
				return 0, rsql_err
			}

			if rsql_err_2 := context.Send_warning(rsql_err); rsql_err_2 != nil { // if just a warning, send it to client and continue
				return 0, rsql_err_2
			}
		}

	} // end loop

	return 0, nil
}

// Execute_basicblocks_all executes all instuctions in basicblocks, and jumping to the next basicblock by evaluating IF or WHILE conditions, or following GOTO label.
// ATTENTION: In context, wcache can have been recreated by COMMIT or ROLLBACK statement.
//
func Execute_basicblocks_all(context *rsql.Context, basicblock_start *rsql.Basicblock) (returned_value int64, rsql_err *rsql.Error) {
	var (
		basicblock    *rsql.Basicblock
		context_dummy rsql.Context // dummy context because during constant folding, Execute_basicblocks_all is called with nil Context, and Execute_basicblock needs a non-nil Context
	)

	if context == nil { // Execute_basicblocks_all is being called by constant folding stage
		context = &context_dummy
	}

	basicblock = basicblock_start

	for basicblock != nil {
		basicblock.Bb_jump_target = nil // nullify target basicblock pointer

		if returned_value, rsql_err = Execute_basicblock(context, basicblock); rsql_err != nil { // if a jump is encountered, basicblock.Bb_jump_target is updated and the execution of the list is stopped
			return 0, rsql_err
		}

		basicblock = basicblock.Bb_jump_target // execute next basicblock

		// check periodically if client is alive

		if basicblock != nil {
			if rsql_err := context.Check_keepalive(); rsql_err != nil {
				return 0, rsql_err
			}
		}
	}

	// all basicblocks have been executed

	return returned_value, nil
}
