package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sync"

	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/data"
)

const (
	MODE_TREE   = "tree"   // print pages as a tree.
	MODE_SERIES = "series" // print given start page, and all subsequent sibling pages, by following Pg_next_no chain.
	MODE_FULL   = "full"   // all pages in the file are read in sequential order. DELETED PAGES ARE ALSO READ !
	MODE_CHECK  = "check"  // checks that at each depth, pages are correctly double-linked to the previous page. Also checks that LEAF pages must have tuple count > 0.
	MODE_LOG    = "log"    // print journal file
)

// global scratch variables.
//
var (
	g_leaf_row rsql.Row // scratch row, in which we deserialize leaf tuples
	g_node_row rsql.Row // scratch row, in which we deserialize node tuples
)

//================================ MODE LOG =====================================

func read_MODE_LOG(file *os.File, read_count_bak_if_status_clear bool) error {
	var (
		err               error
		logpage           cache.Page
		logfile_sector    cache.Logfile_sector
		logpage_count     uint64
		logpage_count_bak uint64
		logpages_to_read  uint64
		i                 uint64 // sequential no of pages in logfile, 1, 2, ...
	)

	// read status page

	err = read_page(&logpage, file, 0)
	if err == io.EOF {
		return nil
	}
	if err != nil {
		return err
	}

	copy(logfile_sector[:], logpage.Pg_canvas[:cache.LOGFILE_SECTOR_SIZE])

	logpage_count = logfile_sector.Plog_count()
	logpage_count_bak = logfile_sector.Plog_count_bak()

	logpages_to_read = logpage_count
	if read_count_bak_if_status_clear {
		logpages_to_read = logpage_count_bak
	}

	fmt.Printf("---------- LOGPAGE 0 --------------\n")
	fmt.Printf("status                : %s\n", logfile_sector.Plog_status())
	fmt.Printf("logpage count         : %d\n", logpage_count)
	fmt.Printf("logpage count bak     : %d\n", logpage_count_bak)
	fmt.Println()

	// read log pages

	for i = 1; i < logpages_to_read; i++ {
		// read page

		err = read_page(&logpage, file, cache.Page_no_t(i))
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		// print page

		fmt.Printf("---------- %d --------------\n", i)
		fmt.Printf("type                     : %s\n", logpage.Pg_td_type())
		fmt.Printf("dbid.schema.(gtblid)tblid: %d.%d.(%d)%d\n", logpage.Pg_dbid(), logpage.Pg_schid(), logpage.Pg_base_gtblid(), logpage.Pg_tblid())
		fmt.Printf("page type                : %s\n", logpage.Pg_type())
		fmt.Printf("page_no                  : %d\n", logpage.Pg_no())
		fmt.Println()
	}

	if i < logpages_to_read {
		fmt.Printf("ALL LOGPAGES FOR THE TRANSACTION HAVE NOT BEEN READ. Requested: %d, read %d\n", logpages_to_read, i)
	}

	return nil
}

//================================ MODE FULL =====================================

func read_MODE_FULL(start_page_no cache.Page_no_t, file *os.File, width_limit int, separator byte, option_ms bool, tabledef *rsql.Tabledef, leaf_row rsql.Row, node_row rsql.Row) error {
	var (
		err          error
		page_no      cache.Page_no_t
		scratch_page cache.Page
	)

	fmt.Println("WARNING: all pages in the file are read in sequential order. DELETED PAGES ARE ALSO READ !")
	fmt.Println()

	page_no = 0
	if start_page_no != cache.PAGE_BOT {
		page_no = start_page_no
	}

	for {
		// read page

		err = read_page(&scratch_page, file, page_no)
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		// print page

		//if scratch_page.Pg_type() == cache.PAGE_TYPE_NODE {
		//	rsql.Println(scratch_page.Pg_canvas[:cache.PAGE_CANVAS_LENGTH])
		//	rsql.Println(scratch_page.Pg_canvas[cache.PAGE_CANVAS_LENGTH:])
		//}

		print_page(&scratch_page, tabledef, leaf_row, node_row, width_limit, separator, option_ms, 0)

		page_no++
	}

	return nil
}

//================================ MODE SERIES =====================================

func read_MODE_SERIES(page_info *cache.Page, start_page_no cache.Page_no_t, file *os.File, width_limit int, separator byte, option_ms bool, tabledef *rsql.Tabledef, leaf_row rsql.Row, node_row rsql.Row) error {
	var (
		err          error
		page_no      cache.Page_no_t
		scratch_page cache.Page
	)

	page_no = cache.PAGE_INFO_PAGE_NO
	if start_page_no != cache.PAGE_BOT {
		page_no = start_page_no
	}

	// read and print start page, and all following pages

	for page_no != cache.PAGE_EOT {

		err = read_page(&scratch_page, file, page_no)
		if err != nil {
			return err
		}

		// print page

		print_page(&scratch_page, tabledef, leaf_row, node_row, width_limit, separator, option_ms, 0)

		page_no = scratch_page.Pg_next_no()
	}

	return nil
}

//================================ MODE TREE =====================================

func read_subtree(page_no cache.Page_no_t, start_page_no cache.Page_no_t, file *os.File, width_limit int, separator byte, option_ms bool, tabledef *rsql.Tabledef, leaf_row rsql.Row, node_row rsql.Row, indenting int) error {
	var (
		err            error
		target_page_no cache.Page_no_t
		scratch_page   cache.Page
	)

	// read page

	err = read_page(&scratch_page, file, page_no)
	if err != nil {
		return err
	}

	// print current page

	print_page(&scratch_page, tabledef, leaf_row, node_row, width_limit, separator, option_ms, indenting)

	// if node page, walk children. If leaf page, count tuples.

	switch scratch_page.Pg_type() {
	case cache.PAGE_TYPE_NODE:
		tuple_count := scratch_page.Pg_tuple_count()

		for i := -1; i < tuple_count; i++ { // -1 is the bottom target page
			target_page_no = btree.Target_page_no_in_node(&scratch_page, i)

			if err := read_subtree(target_page_no, start_page_no, file, width_limit, separator, option_ms, tabledef, leaf_row, node_row, indenting+4); err != nil {
				return err
			}
		}

	case cache.PAGE_TYPE_LEAF:
		g_total_leaf_tuples_count += uint64(scratch_page.Pg_tuple_count())

	case cache.PAGE_TYPE_TABLE_INFO,
		cache.PAGE_TYPE_ZONE_ALLOCATOR:
		// pass

	default:
		panic("file corrupted")
	}

	return nil
}

func read_MODE_TREE(page_info *cache.Page, start_page_no cache.Page_no_t, file *os.File, width_limit int, separator byte, option_ms bool, tabledef *rsql.Tabledef, leaf_row rsql.Row, node_row rsql.Row) error {
	var (
		root_page_no cache.Page_no_t
	)

	// print page_info

	root_page_no = page_info.Pinf_root_page_no()

	print_page(page_info, tabledef, nil, nil, 0, 0, option_ms, 0)

	if root_page_no == 0 { // if table is empty, just return
		return nil
	}

	// read tree

	g_total_leaf_tuples_count = 0

	if start_page_no == cache.PAGE_BOT {
		start_page_no = root_page_no
	}

	if err := read_subtree(start_page_no, start_page_no, file, width_limit, separator, option_ms, tabledef, leaf_row, node_row, 0); err != nil {
		return err
	}

	fmt.Printf("Total number of tuples read in leaf pages: %d\n", g_total_leaf_tuples_count)

	return nil
}

//================================ MODE CHECK =====================================

// Tuple_descr contains a tuple and some other information.
// These objects are put into g_tuples_channel during walking of the btree, and retrieved and checked by a separate goroutine.
//
type Tuple_descr struct {
	page_type cache.Pg_type_t
	page_no   cache.Page_no_t
	tuple_no  int
	tuple     rsql.Tuple
}

func (tupledescr *Tuple_descr) Copy_from(src Tuple_descr) {

	tupledescr.page_type = src.page_type
	tupledescr.page_no = src.page_no
	tupledescr.tuple_no = src.tuple_no

	tupledescr.tuple = append(tupledescr.tuple[:0], src.tuple...)
}

// These objects are put into g_page_links_channel, and a separate goroutine checks if all pages are properly linked.
//
type Page_link struct {
	pg_type    cache.Pg_type_t
	pg_depth   int
	pg_no      cache.Page_no_t
	pg_prev_no cache.Page_no_t
	pg_next_no cache.Page_no_t
}

func New_page_link(page *cache.Page) Page_link {
	page_link := Page_link{
		pg_type:    page.Pg_type(),
		pg_depth:   page.Pg_page_depth(),
		pg_no:      page.Pg_no(),
		pg_prev_no: page.Pg_prev_no(),
		pg_next_no: page.Pg_next_no(),
	}

	return page_link
}

var (
	g_tuples_channel                              chan Tuple_descr
	wg                                            sync.WaitGroup
	g_total_leaf_tuples_count_by_ordering_checker uint64 // number of leaf tuples, counted by the checker during ordering checking

	g_page_links_channel                     chan Page_link // pg_prev_no, pg_no, and pg_next_no
	g_total_leaf_pages_count_by_link_checker uint64         // number of leaf pages, counted by the checker during page link checking

	g_total_leaf_distance uint64

	g_pinf_total_leaf_tuples_count uint64
	g_pinf_total_leaf_pages_count  uint64
)

func walk_subtree(page_no cache.Page_no_t, file *os.File, tabledef *rsql.Tabledef, depth_max int, current_depth int, verbose bool) error {
	var (
		err            error
		target_page_no cache.Page_no_t
		scratch_page   cache.Page
	)

	// read page

	err = read_page(&scratch_page, file, page_no)
	if err != nil {
		return err
	}

	// print current page_no

	depth := scratch_page.Pg_page_depth()

	if verbose {
		indenting := 4*(depth_max-depth) + 4
		fmt.Printf("%.*s%d\n", indenting, INDENTING_STRING, scratch_page.Pg_no())
	}

	// check current page is coherent tabledef

	rsql.Assert(scratch_page.Pg_page_depth() == current_depth)
	rsql.Assert(scratch_page.Pg_td_type() == tabledef.Td_type)
	rsql.Assert(scratch_page.Pg_dbid() == cache.Dbid_t(tabledef.Td_dbid))
	rsql.Assert(scratch_page.Pg_schid() == cache.Schid_t(tabledef.Td_schid))
	rsql.Assert(scratch_page.Pg_base_gtblid() == cache.Tblid_t(tabledef.Td_base_gtblid))
	rsql.Assert(scratch_page.Pg_tblid() == cache.Tblid_t(tabledef.Td_tblid))

	// feed Page_link into channel

	g_page_links_channel <- New_page_link(&scratch_page)

	// if node page, walk children. If leaf page, count tuples.

	switch scratch_page.Pg_type() {
	case cache.PAGE_TYPE_NODE:
		rsql.Assert(scratch_page.Pg_page_depth() > 0)

		g_total_node_pages_count += 1
		g_total_node_consumed_room += uint64(scratch_page.Consumed_room())

		tuple_count := scratch_page.Pg_tuple_count()

		for i := -1; i < tuple_count; i++ { // -1 is the bottom target page
			target_page_no = btree.Target_page_no_in_node(&scratch_page, i)

			if i >= 0 {
				g_tuples_channel <- Tuple_descr{cache.PAGE_TYPE_NODE, scratch_page.Pg_no(), i, scratch_page.Pg_tuple(i)}
			}

			if err := walk_subtree(target_page_no, file, tabledef, depth_max, depth-1, verbose); err != nil {
				return err
			}
		}

	case cache.PAGE_TYPE_LEAF:
		rsql.Assert(scratch_page.Pg_page_depth() == 0)
		rsql.Assert(scratch_page.Pg_tuple_count() > 0)

		g_total_leaf_pages_count += 1
		g_total_leaf_consumed_room += uint64(scratch_page.Consumed_room())
		g_total_leaf_tuples_count += uint64(scratch_page.Pg_tuple_count())

		// tuples

		tuple_count := scratch_page.Pg_tuple_count()

		for i := 0; i < tuple_count; i++ {
			g_tuples_channel <- Tuple_descr{cache.PAGE_TYPE_LEAF, scratch_page.Pg_no(), i, scratch_page.Pg_tuple(i)}
		}

	default:
		panic("file corrupted")
	}

	return nil
}

//------------------

// checks that at each depth, pages are correctly double-linked to the previous page.
// Also checks that LEAF pages must have tuple count > 0.
//
func read_MODE_CHECK(page_info *cache.Page, file *os.File, width_limit int, separator byte, option_ms bool, tabledef *rsql.Tabledef, leaf_row rsql.Row, node_row rsql.Row, verbose bool) error {
	var (
		err                       error
		root_page_no              cache.Page_no_t
		depth_max                 int
		warning_leaf_tuples_count string
		warning_leaf_pages_count  string
	)

	//======== print page_info ========

	root_page_no = page_info.Pinf_root_page_no()

	g_pinf_total_leaf_tuples_count = page_info.Pinf_total_leaf_tuples_count()
	g_pinf_total_leaf_pages_count = page_info.Pinf_total_leaf_pages_count()

	print_page(page_info, tabledef, nil, nil, 0, 0, false, 0)

	if root_page_no == 0 { // if table is empty, just return
		fmt.Println("       ### THE TABLE IS EMPTY ###")
		fmt.Println()

		return nil
	}

	// read root page

	var root_page = &cache.Page{}

	err = read_page(root_page, file, root_page_no)
	if err != nil {
		return err
	}

	depth_max = root_page.Pg_page_depth()

	root_page = nil

	// make channels

	g_tuples_channel = make(chan Tuple_descr)
	g_page_links_channel = make(chan Page_link)

	//======== perform checking ========

	g_btree_depth = depth_max
	g_total_leaf_tuples_count = 0
	g_total_leaf_pages_count = 0
	g_total_leaf_consumed_room = 0
	g_total_node_pages_count = 0
	g_total_node_consumed_room = 0
	g_total_distance = 0

	if verbose {
		fmt.Println("Node tree:")
	}

	go check_tuples_are_ordered(g_tuples_channel, tabledef) // check that all tuples are correctly ordered

	go check_pages_linking(g_page_links_channel, depth_max) // check that pages in btree structure are correctly linked

	if err := walk_subtree(root_page_no, file, tabledef, depth_max, depth_max, verbose); err != nil { // walk the btree and feed the channels
		return err
	}

	close(g_tuples_channel)
	close(g_page_links_channel)

	wg.Wait()
	rsql.Assert(g_total_leaf_tuples_count_by_ordering_checker == g_total_leaf_tuples_count)
	rsql.Assert(g_total_leaf_pages_count_by_link_checker == g_total_leaf_pages_count)

	// check zone allocator

	check_zone_allocators(page_info, file)

	check_rootbitmap(page_info, file)

	//======== global information ========

	if verbose {
		fmt.Println()
	}

	if g_total_leaf_tuples_count != g_pinf_total_leaf_tuples_count {
		warning_leaf_tuples_count = fmt.Sprintf("COUNT MISMATCH !  pinf_total_leaf_tuples_count == %d", g_pinf_total_leaf_tuples_count)
	}

	if g_total_leaf_pages_count != g_pinf_total_leaf_pages_count {
		warning_leaf_pages_count = fmt.Sprintf("COUNT MISMATCH !  pinf_total_leaf_pages_count == %d", g_pinf_total_leaf_pages_count)
	}

	fmt.Println("----- stats -----")

	fmt.Printf("Btree depth                              : %d\n", g_btree_depth)
	fmt.Println()
	fmt.Printf("Total number of tuples read in leaf pages: %d%s\n", g_total_leaf_tuples_count, warning_leaf_tuples_count)
	var mean_tuple_count_per_page float64 = 0
	if g_total_leaf_pages_count > 0 {
		mean_tuple_count_per_page = float64(g_total_leaf_tuples_count) / float64(g_total_leaf_pages_count)
	}
	fmt.Printf("Mean tuple count per page                : %.1f\n", mean_tuple_count_per_page)
	fmt.Println()

	fmt.Printf("Total number of leaf pages               : %-15d%s\n", g_total_leaf_pages_count, warning_leaf_pages_count)
	fmt.Printf("Total number of node pages               : %-15d\n", g_total_node_pages_count)

	var mean_leaf_page_consumed_room uint64 = 0
	if g_total_leaf_pages_count > 0 {
		mean_leaf_page_consumed_room = (g_total_leaf_consumed_room / g_total_leaf_pages_count * 100) / cache.PAGE_CANVAS_LENGTH
	}
	fmt.Println()
	fmt.Printf("Mean leaf page consumed room             : %d %%\n", mean_leaf_page_consumed_room)

	var mean_node_page_consumed_room uint64 = 0
	if g_total_node_pages_count > 0 {
		mean_node_page_consumed_room = (g_total_node_consumed_room / g_total_node_pages_count * 100) / cache.PAGE_CANVAS_LENGTH
	}
	fmt.Printf("Mean node page consumed room             : %d %%\n", mean_node_page_consumed_room)
	fmt.Println()

	var g_mean_leaf_distance uint64 = 0
	if g_total_leaf_pages_count > 1 {
		g_mean_leaf_distance = g_total_leaf_distance / (g_total_leaf_pages_count - 1)
	}
	fmt.Printf("Mean distance between leaf pages         : %d\n", g_mean_leaf_distance)

	return nil
}

// check_tuples_are_ordered is run by a separate goroutine, to check all tuples in g_tuples_channel.
//
func check_tuples_are_ordered(tuples_channel <-chan Tuple_descr, tabledef *rsql.Tabledef) {
	var (
		rsql_err            *rsql.Error
		tuple_previous      rsql.Tuple
		tupledescr_previous Tuple_descr
		workpad             *Workpad
	)

	workpad = New_workpad(tabledef)

	// create a node tuple with all fields NULL, as starting tuple

	tuple_previous, rsql_err = data.Write_serialized_row(tuple_previous[:0], data.New_node_row(rsql.KIND_COL_LEAF, tabledef))
	rsql.Assert(rsql_err == nil)
	tupledescr_previous = Tuple_descr{page_type: cache.PAGE_TYPE_NODE, tuple: tuple_previous}

	// check all tuples in channel

	wg.Add(1)

	for tupledescr := range tuples_channel {
		if tupledescr.page_type == cache.PAGE_TYPE_LEAF {
			g_total_leaf_tuples_count_by_ordering_checker++
		}

		res := workpad.compare_tuples(tupledescr, tupledescr_previous)

		switch {
		case tupledescr_previous.page_type == cache.PAGE_TYPE_NODE && tupledescr.page_type == cache.PAGE_TYPE_LEAF:
			if !(res >= 0) {
				log.Fatalf("CHECK for tuple ordering failed. page %s %d, tuple_no %d,   previous page %s %d, tuple_no %d", tupledescr.page_type, tupledescr.page_no, tupledescr.tuple_no, tupledescr_previous.page_type, tupledescr_previous.page_no, tupledescr_previous.tuple_no)
			}

		default:
			if !(res > 0) {
				log.Fatalf("CHECK for tuple ordering failed. page %s %d, tuple_no %d,   previous page %s %d, tuple_no %d", tupledescr.page_type, tupledescr.page_no, tupledescr.tuple_no, tupledescr_previous.page_type, tupledescr_previous.page_no, tupledescr_previous.tuple_no)
			}
		}

		tupledescr_previous.Copy_from(tupledescr)
	}

	wg.Done()
}

// check_pages_linking is run by a separate goroutine, to check that all pages in g_page_links_channel.
//
func check_pages_linking(page_links_channel <-chan Page_link, depth_max int) {
	var (
		page_link_previous Page_link
	)

	array_link_previous := make([]Page_link, depth_max+1)

	for i, _ := range array_link_previous {
		page_type := cache.PAGE_TYPE_NODE
		if i == 0 {
			page_type = cache.PAGE_TYPE_LEAF
		}
		array_link_previous[i] = Page_link{pg_type: page_type, pg_no: cache.PAGE_BOT, pg_prev_no: cache.PAGE_BOT, pg_next_no: cache.PAGE_EOT}
	}

	wg.Add(1)

	for page_link := range page_links_channel {
		page_link_previous = array_link_previous[page_link.pg_depth]
		rsql.Assert(page_link.pg_type == page_link_previous.pg_type)

		if page_link.pg_type == cache.PAGE_TYPE_LEAF {
			g_total_leaf_pages_count_by_link_checker++
		}

		// compute distance

		if page_link_previous.pg_no != cache.PAGE_BOT {
			if page_link.pg_no > page_link_previous.pg_no {
				g_total_leaf_distance += uint64(page_link.pg_no - page_link_previous.pg_no)
			} else {
				g_total_leaf_distance += uint64(page_link_previous.pg_no - page_link.pg_no)
			}
		}

		// check links

		if page_link_previous.pg_no == cache.PAGE_BOT { // adjustment just for starting dummy Page_link
			page_link_previous.pg_next_no = page_link.pg_no
		}

		if !(page_link.pg_prev_no == page_link_previous.pg_no && page_link.pg_no == page_link_previous.pg_next_no) {
			log.Fatalf("CHECK for page linking failed. page %d,   previous page %d", page_link.pg_no, page_link_previous.pg_no)
		}

		array_link_previous[page_link.pg_depth] = page_link
	}

	for _, page_link := range array_link_previous {
		rsql.Assert(page_link.pg_next_no == cache.PAGE_EOT)
	}

	wg.Done()
}

func check_zone_allocators(page_info *cache.Page, file *os.File) {

	allocator_page := &cache.Page{}

	allocator_count := page_info.Pinf_zone_bitmaps_count()

	for zone_no := 0; zone_no < int(allocator_count); zone_no++ { // for each zone
		err := read_page(allocator_page, file, cache.Page_no_t(zone_no*cache.PZONE_NUMBER_OF_PAGES_IN_ZONE))
		if err != nil {
			log.Fatal(err)
		}

		free_page_count := allocator_page.Pzone_free_page_count()
		free_page_count_in_bitmap, _ := count_of_bit_0_1(allocator_page.Pzone_bitmap())

		if free_page_count != free_page_count_in_bitmap {
			log.Fatalf("zone_no %d, page_no %d: allocator page count mismatch: Pzone_free_page_count %d != free page count in bitmap %d.", zone_no, allocator_page.Pg_no(), free_page_count, free_page_count_in_bitmap)
		}
	}
}

func check_rootbitmap(page_info *cache.Page, file *os.File) {

	allocator_count := page_info.Pinf_zone_bitmaps_count()
	rootbitmap := page_info.Pinf_zone_rootbitmap_array()

	for i := allocator_count; i < cache.PZONE_ROOTBITMAP_ARRAY_SIZE*8; i++ { // check that all bits in zone_no >= Pinf_zone_bitmaps_count() are set to 1
		if cache.Zone_bitmap_get_bit(rootbitmap, uint64(i)) != 1 {
			log.Fatalf("rootbitmap corrupted: bit is 0 for zone %d, but Pinf_zone_bitmaps_count() = %d.", i, allocator_count)
		}
	}
}

func count_of_bit_0_1(s []byte) (n_0, n_1 uint32) {

	n := 0

	for _, b := range s {
		for i := 0; i < 8; i++ {
			if b&(0x01<<uint(i)) != 0 {
				n++
			}
		}
	}

	return uint32(8*len(s) - n), uint32(n)
}

//====================================================================================
//                               MAIN FUNCTION
//====================================================================================

// global counters
var (
	g_btree_depth              int
	g_total_leaf_tuples_count  uint64
	g_total_leaf_pages_count   uint64
	g_total_leaf_consumed_room uint64
	g_total_node_pages_count   uint64
	g_total_node_consumed_room uint64
	g_total_distance           uint64
)

func main() {
	var (
		mode                           string
		width_limit                    int
		separator_string               string
		separator                      byte
		option_ms                      bool
		start_page_no                  uint64
		read_count_bak_if_status_clear bool
		verbose                        bool
		args                           []string

		filename  string
		file      *os.File
		err       error
		tabledef  *rsql.Tabledef
		page_info *cache.Page
	)

	flag.StringVar(&mode, "m", MODE_TREE, "allowed modes are \"tree\", \"series\", \"full\", \"check\", \"log\"")
	flag.IntVar(&width_limit, "w", 10, "max witdh for varbinary and varchar")
	flag.StringVar(&separator_string, "sep", "|", "column separator")
	flag.BoolVar(&option_ms, "ms", false, "displays milliseconds for TIME and DATETIME")
	flag.Uint64Var(&start_page_no, "start_page_no", uint64(cache.PAGE_BOT), "start page_no for \"full\", \"series\", or \"tree\" modes")
	flag.BoolVar(&read_count_bak_if_status_clear, "log_count_bak", false, "if mode \"log\" and logfile status is LOG_STATUS_CLEAR, read log pages of last transaction")
	flag.BoolVar(&verbose, "v", false, "for mode \"check\", print node tree")

	flag.Parse()
	args = flag.Args()

	// check mode

	switch mode {
	case MODE_TREE,
		MODE_SERIES,
		MODE_FULL,
		MODE_CHECK,
		MODE_LOG:

	default:
		flag.Usage()
		os.Exit(-1)
	}

	// check separator

	if len(separator_string) != 1 {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", "separator must be an ascii character")
		os.Exit(-1)
	}

	separator = separator_string[0]

	// get file name

	if len(args) != 1 {
		fmt.Fprintf(os.Stderr, "%s\n", "ERROR: you must pass exactly one file name as argument")
		os.Exit(-1)
	}

	filename = args[0]

	file, err = os.Open(filename) // For read access.
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(-1)
	}

	defer file.Close()

	// if MODE_LOG, read logfile

	if mode == MODE_LOG {
		if err = read_MODE_LOG(file, read_count_bak_if_status_clear); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
			os.Exit(-1)
		}

		return //===> MODE_LOG exits here
	}

	// create Tabledef from page_info

	page_info = &cache.Page{} // scratch

	if err = read_page(page_info, file, cache.PAGE_INFO_PAGE_NO); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(-1)
	}

	tabledef = create_tabledef(page_info)

	// create global scratch variables

	g_leaf_row, _, _ = data.New_leaf_row(rsql.KIND_COL_LEAF, tabledef)
	g_node_row = data.New_node_row(rsql.KIND_COL_LEAF, tabledef)

	// display pages

	fmt.Printf("type: %s, dbid.schema.(gtblid)tblid: %d.%d.(%d)%d\n", tabledef.Td_type, tabledef.Td_dbid, tabledef.Td_schid, tabledef.Td_base_gtblid, tabledef.Td_tblid)
	fmt.Printf("highest allocated page no, excepted allocator pages, is page_no=%d\n", Find_highest_data_allocated_page(file))
	fmt.Printf("PAGE_SIZE=%d\n", cache.PAGE_SIZE)
	fmt.Printf("PZONE_NUMBER_OF_PAGES_IN_ZONE=%d\n", cache.PZONE_NUMBER_OF_PAGES_IN_ZONE)
	fmt.Println()

	switch mode {
	case MODE_TREE:
		if err = read_MODE_TREE(page_info, cache.Page_no_t(start_page_no), file, width_limit, separator, option_ms, tabledef, g_leaf_row, g_node_row); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
			os.Exit(-1)
		}

	case MODE_SERIES:
		if err = read_MODE_SERIES(page_info, cache.Page_no_t(start_page_no), file, width_limit, separator, option_ms, tabledef, g_leaf_row, g_node_row); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
			os.Exit(-1)
		}

	case MODE_FULL:
		if err = read_MODE_FULL(cache.Page_no_t(start_page_no), file, width_limit, separator, option_ms, tabledef, g_leaf_row, g_node_row); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
			os.Exit(-1)
		}

	case MODE_CHECK:
		if err = read_MODE_CHECK(page_info, file, width_limit, separator, option_ms, tabledef, g_leaf_row, g_node_row, verbose); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
			os.Exit(-1)
		}

	default:
		flag.Usage()
		os.Exit(-1)
	}

}
