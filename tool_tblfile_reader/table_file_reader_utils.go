package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/data"
)

const INDENTING_STRING = "                                                                                                    " // if more indenting is needed, just put more spaces in this string

func create_tabledef(page_info *cache.Page) *rsql.Tabledef {
	var (
		tabledef          *rsql.Tabledef
		collations        []string
		collations_exists bool
		col_count         int
		list_coldefs      []*rsql.Coldef
		nk_count          int
		list_nk           []*rsql.Coldef
		cluster_type      rsql.Td_cluster_type_t
	)

	// create list of Coldefs

	collations, collations_exists = page_info.Pinf_nk_collations()
	if collations_exists {
		rsql.Assert(len(collations) == int(page_info.Pinf_nk_count()))
	}

	col_count = int(page_info.Pinf_column_count())

	for i := 0; i < col_count; i++ {
		colname := fmt.Sprintf("c%d", i)
		datatype := page_info.Pinf_column_datatypes(i)
		precision := uint16(0)

		// to create Dataslot for these datatypes, we must set precision.

		switch datatype {
		case rsql.DATATYPE_VARBINARY:
			precision = rsql.DATATYPE_VARBINARY_PRECISION_MAX
		case rsql.DATATYPE_VARCHAR:
			precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX // fixlen_flag is always false. We don't create CHAR, as deserializing in VARCHAR is enough to see the string. We don't care about CHAR trailing blanks.
		case rsql.DATATYPE_NUMERIC:
			precision = uint16(page_info.Pinf_column_NUMERIC_precision(i)) // we deserialize in NUMERIC(Cd_precision, 0), but scale it is not important. As deserialization doesn't change the precision and scale of the tuple field, it will be displayed correctly. We only need the precision to have a suitable width to display NUMERIC values.
		}

		coldef := &rsql.Coldef{
			Cd_colname:          colname,
			Cd_colname_original: colname,
			Cd_datatype:         datatype,
			Cd_precision:        precision,
			Cd_base_seqno:       page_info.Pinf_column_base_seqno(i),
		}

		list_coldefs = append(list_coldefs, coldef)
	}

	// create list of native key columns

	nk_count = int(page_info.Pinf_nk_count())

	for i := 0; i < nk_count; i++ {
		seqno := page_info.Pinf_nk_base_seqno(i)

		zz := -1 // lookup coldef with same seqno as nk column
		for n, coldef := range list_coldefs {
			if coldef.Cd_base_seqno == seqno {
				zz = n
			}
		}

		rsql.Assert(zz != -1)

		list_nk = append(list_nk, list_coldefs[zz])
	}

	// create Tabledef

	switch page_info.Pg_td_type() {
	case rsql.TD_TYPE_BASE_TABLE:
		cluster_type = rsql.TD_CLUSTERED

	case rsql.TD_TYPE_INDEX_TABLE:
		cluster_type = rsql.TD_NONCLUSTERED

	default:
		panic("impossible")
	}

	tabledef = &rsql.Tabledef{
		Td_tblid: int64(page_info.Pg_tblid()),

		Td_dbid:                int64(page_info.Pg_dbid()),
		Td_schid:               int64(page_info.Pg_schid()),
		Td_base_gtblid:         int64(page_info.Pg_base_gtblid()),
		Td_index_name:          "table", // anything is ok for this dummy name
		Td_index_name_original: "table",

		//Td_file_path string

		Td_durability: rsql.DURABILITY_PERMANENT,
		Td_type:       page_info.Pg_td_type(), // TD_TYPE_BASE_TABLE, TD_TYPE_INDEX_TABLE
		//Td_index_type   Td_index_type_t   // TD_UNIQUE, TD_PRIMARY_KEY, TD_INDEX. Here, we cannot know the index type, so, we let this field blank.
		Td_cluster_type: cluster_type,

		Td_coldefs: list_coldefs, // list of all columns stored in physical storage, in physical order
		Td_nk:      list_nk,      // list of columns making up the native key of this table. If TD_INDEX, the column rowid is appended to make the native key unique.
		// Td_payload []*Coldef // only for TD_TYPE_INDEX_TABLE: list of columns making up the native key of the base table. Here, we cannot know, so, we let this field blank.

		//Td_status Td_status_t // TD_STATUS_BEING_CREATED, TD_STATUS_VALID, TD_STATUS_BEING_DROPPED, TD_STATUS_CORRUPTED
	}

	// put nk collations if exists

	if collations_exists {
		for i, coldef := range tabledef.Td_nk {
			switch coldef.Cd_datatype {
			case rsql.DATATYPE_VARCHAR:
				rsql.Assert(collations[i] != "")
				coldef.Cd_collation = collations[i]

			default:
				rsql.Assert(collations[i] == "")
			}
		}
	}

	return tabledef
}

func read_page(page_dest *cache.Page, file *os.File, page_no cache.Page_no_t) error {

	if _, err := file.ReadAt(page_dest.Pg_canvas[:], int64(page_no*cache.PAGE_SIZE)); err != nil {
		return err
	}

	return nil
}

func print_leaf_page_tuples(page *cache.Page, tabledef *rsql.Tabledef, leaf_row rsql.Row, width_limit int, separator byte, option_ms bool, indenting int) {

	tuple_count := page.Pg_tuple_count()

	// print tuples

	for i := 0; i < tuple_count; i++ {
		data.Read_row_from_leaf_page(leaf_row, tabledef, page, i)

		s := data.Debug_display_row(leaf_row, width_limit, separator, option_ms)

		fmt.Printf("%.*s%s\n", indenting, INDENTING_STRING, s)
	}
}

func print_node_page_tuples(page *cache.Page, tabledef *rsql.Tabledef, node_row rsql.Row, width_limit int, separator byte, option_ms bool, indenting int) {

	tuple_count := page.Pg_tuple_count()

	// print tuples

	for i := 0; i < tuple_count; i++ {
		data.Read_entry_from_node_page(node_row, tabledef, page, i)

		s := data.Debug_display_row(node_row, width_limit, separator, option_ms)

		fmt.Printf("%.*s%s\n", indenting, INDENTING_STRING, s)
	}
}

func pinf_column_base_seqnos_to_string(page *cache.Page) string {
	var (
		column_base_seqnos string
	)

	for i := 0; i < int(page.Pinf_column_count()); i++ {
		seqno := page.Pinf_column_base_seqno(i)

		column_base_seqnos += fmt.Sprintf("%d,", seqno)
	}

	column_base_seqnos = column_base_seqnos[:len(column_base_seqnos)-1] // discard trailing comma

	return column_base_seqnos
}

func pinf_column_datatypes_to_string(page *cache.Page) string {
	var (
		column_datatypes string
	)

	for i := 0; i < int(page.Pinf_column_count()); i++ {
		datatype := page.Pinf_column_datatypes(i)

		column_datatypes += fmt.Sprintf("%d:%s, ", i, datatype.String())
	}

	column_datatypes = column_datatypes[:len(column_datatypes)-2] // discard trailing comma and space

	return column_datatypes
}

func pinf_nk_base_seqnos_to_string(page *cache.Page) string {
	var (
		nk_base_seqnos string
	)

	for i := 0; i < int(page.Pinf_nk_count()); i++ {
		seqno := page.Pinf_nk_base_seqno(i)

		nk_base_seqnos += fmt.Sprintf("%d,", seqno)
	}

	nk_base_seqnos = nk_base_seqnos[:len(nk_base_seqnos)-1] // discard trailing comma

	return nk_base_seqnos
}

func print_page(page *cache.Page, tabledef *rsql.Tabledef, leaf_row rsql.Row, node_row rsql.Row, width_limit int, separator byte, option_ms bool, indenting int) {
	var (
		page_type         cache.Pg_type_t
		page_type_label   string
		collations        []string
		collations_string string
		ok                bool
	)

	// print page

	rsql.Assert(page.Pg_td_type() == tabledef.Td_type)

	rsql.Assert(page.Pg_dbid() == cache.Dbid_t(tabledef.Td_dbid))
	rsql.Assert(page.Pg_schid() == cache.Schid_t(tabledef.Td_schid))
	rsql.Assert(page.Pg_base_gtblid() == cache.Tblid_t(tabledef.Td_base_gtblid))
	rsql.Assert(page.Pg_tblid() == cache.Tblid_t(tabledef.Td_tblid))

	page_type = page.Pg_type()

	switch page_type {
	case cache.PAGE_TYPE_LEAF:
		page_type_label = "LEAF"
		fmt.Printf("%.*s---------- %s: %d ---- %d/%d %d-----\n", indenting, INDENTING_STRING, page_type_label, page.Pg_no(), page.Consumed_room(), page.Available_room(), page.Consumed_room()+page.Available_room())
		fmt.Printf("%.*spg_tuple_count: %d, pg_prev_no: %s, pg_next_no: %s\n", indenting, INDENTING_STRING, page.Pg_tuple_count(), page.Pg_prev_no(), page.Pg_next_no())

		print_leaf_page_tuples(page, tabledef, leaf_row, width_limit, separator, option_ms, indenting)

		rsql.Assert(page.Pg_page_depth() == 0)
		rsql.Assert(page.Pg_bottom_target_page_no() == 0)

	case cache.PAGE_TYPE_NODE:
		page_type_label = "NODE"
		fmt.Printf("%.*s---------- %s: %d ---- %d/%d %d-----\n", indenting, INDENTING_STRING, page_type_label, page.Pg_no(), page.Consumed_room(), page.Available_room(), page.Consumed_room()+page.Available_room())
		fmt.Printf("%.*spg_tuple_count: %d, pg_prev_no: %s, pg_next_no: %s\n", indenting, INDENTING_STRING, page.Pg_tuple_count(), page.Pg_prev_no(), page.Pg_next_no())
		fmt.Printf("%.*spg_page_depth           : %d\n", indenting, INDENTING_STRING, page.Pg_page_depth())
		fmt.Printf("%.*s|\n", indenting, INDENTING_STRING)

		fmt.Printf("%.*spg_bottom_target_page_no: %d\n", indenting, INDENTING_STRING, page.Pg_bottom_target_page_no())
		print_node_page_tuples(page, tabledef, node_row, width_limit, separator, option_ms, indenting)

		rsql.Assert(page.Pg_page_depth() != 0)
		rsql.Assert(page.Pg_bottom_target_page_no() != 0)

	case cache.PAGE_TYPE_TABLE_INFO:

		if collations, ok = page.Pinf_nk_collations(); ok == true {
			collations_string = "[" + strings.Join(collations, ";") + "]"
		} else {
			collations_string = cache.COLLATION_LISTINFO_INVALID
		}

		page_type_label = "PAGE_INFO"
		fmt.Printf("%.*s---------- %s: %d ---------\n", indenting, INDENTING_STRING, page_type_label, page.Pg_no())
		fmt.Printf("%.*spinf_column_array_capacity          : %d\n", indenting, INDENTING_STRING, page.Pinf_column_array_capacity())
		fmt.Printf("%.*spinf_nk_array_capacity              : %d\n", indenting, INDENTING_STRING, page.Pinf_nk_array_capacity())
		fmt.Printf("%.*spinf_nk_array_coll_capacity         : %d\n", indenting, INDENTING_STRING, page.Pinf_nk_array_coll_capacity())
		fmt.Printf("%.*s|\n", indenting, INDENTING_STRING)
		fmt.Printf("%.*spinf_column_count                   : %d\n", indenting, INDENTING_STRING, page.Pinf_column_count())
		fmt.Printf("%.*spinf_column_base_seqno              : %s\n", indenting, INDENTING_STRING, pinf_column_base_seqnos_to_string(page))
		fmt.Printf("%.*spinf_column_datatypes               : %s\n", indenting, INDENTING_STRING, pinf_column_datatypes_to_string(page))
		fmt.Printf("%.*spinf_nk_count                       : %d\n", indenting, INDENTING_STRING, page.Pinf_nk_count())
		fmt.Printf("%.*spinf_nk_base_seqno                  : %s\n", indenting, INDENTING_STRING, pinf_nk_base_seqnos_to_string(page))
		fmt.Printf("%.*spinf_nk_collations                  : %s\n", indenting, INDENTING_STRING, collations_string)
		fmt.Printf("%.*s|\n", indenting, INDENTING_STRING)
		fmt.Printf("%.*spinf_next_rowid                     : %d\n", indenting, INDENTING_STRING, page.Pinf_next_rowid())
		fmt.Printf("%.*spinf_next_identity                  : %d\n", indenting, INDENTING_STRING, page.Pinf_next_identity())
		fmt.Printf("%.*s|\n", indenting, INDENTING_STRING)
		fmt.Printf("%.*spinf_root_page_no                   : %d\n", indenting, INDENTING_STRING, page.Pinf_root_page_no())
		fmt.Printf("%.*s|\n", indenting, INDENTING_STRING)
		fmt.Printf("%.*spinf_total_leaf_tuples_count        : %d\n", indenting, INDENTING_STRING, page.Pinf_total_leaf_tuples_count())
		fmt.Printf("%.*spinf_total_leaf_pages _count        : %d\n", indenting, INDENTING_STRING, page.Pinf_total_leaf_pages_count())
		fmt.Printf("%.*s|\n", indenting, INDENTING_STRING)
		fmt.Printf("%.*spinf_zone_bitmaps_count             : %d\n", indenting, INDENTING_STRING, page.Pinf_zone_bitmaps_count())
		fmt.Printf("%.*spinf_zone_bitmap_array_size         : %d\n", indenting, INDENTING_STRING, page.Pinf_zone_bitmap_array_size())
		fmt.Printf("%.*s|\n", indenting, INDENTING_STRING)
		fmt.Printf("%.*spinf_zone_rootbitmap_array_size     : %d\n", indenting, INDENTING_STRING, page.Pinf_zone_rootbitmap_array_size())

		rsql.Assert(page.Pg_page_depth() == 0)
		rsql.Assert(page.Pg_tuple_count() == 0)
		rsql.Assert(page.Pg_no() == cache.PAGE_INFO_PAGE_NO)
		rsql.Assert(page.Pg_prev_no() == cache.PAGE_BOT)
		rsql.Assert(page.Pg_next_no() == cache.PAGE_EOT)
		rsql.Assert(page.Pg_bottom_target_page_no() == 0)

		g_pinf_total_leaf_tuples_count = page.Pinf_total_leaf_tuples_count()
		g_pinf_total_leaf_pages_count = page.Pinf_total_leaf_pages_count()

	case cache.PAGE_TYPE_ZONE_ALLOCATOR:
		page_type_label = "ZONE_ALLOCATOR"
		fmt.Printf("%.*s---------- %s: %d ---------\n", indenting, INDENTING_STRING, page_type_label, page.Pg_no())
		fmt.Printf("%.*spzone_free_page_count: %d, allocated: %d, total %d\n", indenting, INDENTING_STRING, page.Pzone_free_page_count(), cache.PZONE_NUMBER_OF_PAGES_IN_ZONE-page.Pzone_free_page_count(), cache.PZONE_NUMBER_OF_PAGES_IN_ZONE)

		rsql.Assert(page.Pg_page_depth() == 0)
		rsql.Assert(page.Pg_tuple_count() == 0)
		rsql.Assert(page.Pg_prev_no() == cache.PAGE_BOT)
		rsql.Assert(page.Pg_next_no() == cache.PAGE_EOT)
		rsql.Assert(page.Pg_bottom_target_page_no() == 0)

	default:
		fmt.Printf("%.*spg_type unknown\n", indenting, INDENTING_STRING)
		log.Fatal("EXIT FAILURE")
	}

	fmt.Println()
}

func compare_rows(col_list []int, tabledef *rsql.Tabledef, current_row rsql.Row, previous_row rsql.Row, syscollator *data.SYSCOLLATOR) data.Compsort_t {
	var (
		coldef      *rsql.Coldef
		i           int
		dataslot_a  rsql.IDataslot
		dataslot_b  rsql.IDataslot
		comp_result data.Compsort_t
	)

	rsql.Assert(len(col_list) == len(tabledef.Td_nk))

	for i, coldef = range tabledef.Td_nk { // for each column in native key

		//===== compare =====

		dataslot_a = current_row[col_list[i]]
		dataslot_b = previous_row[col_list[i]]

		rsql.Assert(coldef.Cd_datatype == dataslot_a.Datatype())

		if coldef.Cd_datatype != rsql.DATATYPE_VARCHAR {
			syscollator = nil
		}

		comp_result = btree.Comp_for_sort_XXX(dataslot_a, dataslot_b, syscollator) // for sort, a non-NULL value is greater than NULL

		if comp_result != 0 { // as soon as one field is greater or less, the whole record is also greater or less
			return comp_result //===> exit
		}

		// here, fields are equal. We must continue comparison with next field
	}

	rsql.Assert(comp_result == 0) // all native key fields are equal

	return comp_result
}

type Workpad struct {
	tabledef             *rsql.Tabledef
	nk_pos_in_leaf_tuple []int
	syscollator_list     []*data.SYSCOLLATOR

	row_scratch_a rsql.Row
	row_scratch_b rsql.Row
}

func New_workpad(tabledef *rsql.Tabledef) *Workpad {
	var (
		rsql_err             *rsql.Error
		workpad              *Workpad
		nk_pos_in_leaf_tuple []int
		syscollator          *data.SYSCOLLATOR
		syscollator_list     []*data.SYSCOLLATOR
	)

	workpad = &Workpad{}
	workpad.tabledef = tabledef

	// nk_pos_in_leaf_tuple

	base_seqno_map := make(map[uint16]int)

	for i, coldef := range tabledef.Td_coldefs {
		base_seqno_map[coldef.Cd_base_seqno] = i
	}

	for _, coldef := range tabledef.Td_nk {
		nk_pos_in_leaf_tuple = append(nk_pos_in_leaf_tuple, base_seqno_map[coldef.Cd_base_seqno])
	}

	workpad.nk_pos_in_leaf_tuple = nk_pos_in_leaf_tuple

	// syscollator_list

	for _, coldef := range tabledef.Td_nk {
		syscollator = nil

		switch {
		case coldef.Cd_collation != "":
			rsql.Assert(coldef.Cd_datatype == rsql.DATATYPE_VARCHAR)

			if syscollator, rsql_err = data.New_literal_SYSCOLLATOR_NO_LAZY(coldef.Cd_collation); rsql_err != nil { // collators are instanciated. If invalid collation, an error is returned.
				panic(rsql_err)
			}

		default:
			rsql.Assert(coldef.Cd_datatype != rsql.DATATYPE_VARCHAR)
		}

		syscollator_list = append(syscollator_list, syscollator)
	}

	workpad.syscollator_list = syscollator_list

	// row_scratch_a and _b

	workpad.row_scratch_a = data.New_node_row(rsql.KIND_COL_LEAF, tabledef)
	workpad.row_scratch_b = data.New_node_row(rsql.KIND_COL_LEAF, tabledef)

	return workpad
}

// result is comparison of tupledescr_a relative to tupledescr_b.
//
// row_scratch_a and _b are just working Rows, in which tuples are deserialized to perform the comparison.
//
func (workpad *Workpad) compare_tuples(tupledescr_a Tuple_descr, tupledescr_b Tuple_descr) data.Compsort_t {
	var (
		coldef      *rsql.Coldef
		i           int
		dataslot_a  rsql.IDataslot
		dataslot_b  rsql.IDataslot
		comp_result data.Compsort_t
	)

	tabledef := workpad.tabledef

	rsql.Assert(len(workpad.nk_pos_in_leaf_tuple) == len(tabledef.Td_nk))

	for i, coldef = range tabledef.Td_nk { // for each column in native key

		//===== fill in dataslots =====

		dataslot_a = workpad.row_scratch_a[i]
		rsql.Assert(coldef.Cd_datatype == dataslot_a.Datatype())

		switch tupledescr_a.page_type {
		case cache.PAGE_TYPE_NODE:
			data.Deserialize_tuple_field(dataslot_a, tupledescr_a.tuple, uint16(i))

		case cache.PAGE_TYPE_LEAF:
			data.Deserialize_tuple_field(dataslot_a, tupledescr_a.tuple, uint16(workpad.nk_pos_in_leaf_tuple[i]))

		default:
			panic("impossible")
		}

		dataslot_b = workpad.row_scratch_b[i]
		rsql.Assert(coldef.Cd_datatype == dataslot_b.Datatype())

		switch tupledescr_b.page_type {
		case cache.PAGE_TYPE_NODE:
			data.Deserialize_tuple_field(dataslot_b, tupledescr_b.tuple, uint16(i))

		case cache.PAGE_TYPE_LEAF:
			data.Deserialize_tuple_field(dataslot_b, tupledescr_b.tuple, uint16(workpad.nk_pos_in_leaf_tuple[i]))

		default:
			panic("impossible")
		}

		//===== compare =====

		syscollator := workpad.syscollator_list[i]
		if coldef.Cd_datatype != rsql.DATATYPE_VARCHAR {
			rsql.Assert(syscollator == nil)
		}

		comp_result = btree.Comp_for_sort_XXX(dataslot_a, dataslot_b, syscollator) // for sort, a non-NULL value is greater than NULL

		if comp_result != 0 { // as soon as one field is greater or less, the whole record is also greater or less
			return comp_result //===> exit
		}

		// here, fields are equal. We must continue comparison with next field
	}

	rsql.Assert(comp_result == 0) // all native key fields are equal

	return comp_result

}

func Find_highest_data_allocated_page(file *os.File) cache.Page_no_t {

	// page_info

	scratch := &cache.Page{} // scratch

	page_info := scratch

	if err := read_page(page_info, file, cache.PAGE_INFO_PAGE_NO); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
		os.Exit(-1)
	}

	zone_no := cache.Zone_no_t(page_info.Pinf_zone_bitmaps_count() - 1)

	// for all zones, from highest to 0, check if an allocated page is found in zone

	page_allocator := scratch // scratch

	for {
		zone_allocator_page_no := cache.Page_no_t(zone_no * cache.PZONE_NUMBER_OF_PAGES_IN_ZONE)

		if err := read_page(page_allocator, file, zone_allocator_page_no); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err)
			os.Exit(-1)
		}

		if delta_pages := page_allocator.Pzone_highest_allocated_delta_page_no(); delta_pages > 0 { // 0 is always allocator page
			return zone_allocator_page_no + delta_pages
		}

		if zone_no == 0 {
			break
		}

		zone_no--
	}

	panic("At least page_info must have been allocated.")
}
