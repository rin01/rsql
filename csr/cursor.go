package csr

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/data"
	"rsql/dict"
)

func init() {

	rsql.Check_column_not_null = check_column_not_null
}

func assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

// Set_row_to_NULL is used by Reset(), to NULLify the record dataslots.
//
func Set_row_to_NULL(row []rsql.IDataslot) {
	for _, dataslot := range row {
		if dataslot != nil {
			dataslot.Set_to_NULL()
		}
	}
}

type Join_type_t uint8

const (
	JT_INNER_JOIN Join_type_t = iota + 1
	JT_LEFT_JOIN
)

func (jtype Join_type_t) String() string {
	switch jtype {
	case JT_INNER_JOIN:
		return "JT_INNER_JOIN"
	case JT_LEFT_JOIN:
		return "JT_LEFT_JOIN"
	default:
		panic("impossible")
	}
}

type State_t uint8

const (
	CURS_RECORD_RESET State_t = iota + 1
	CURS_RECORD_AVAILABLE
)

func (state State_t) String() string {
	switch state {
	case CURS_RECORD_RESET:
		return "CURS_RECORD_RESET"
	case CURS_RECORD_AVAILABLE:
		return "CURS_RECORD_AVAILABLE"
	default:
		panic("impossible")
	}
}

type Cursor interface {
	Reset()
	Next(*rsql.Context) (State_t, *rsql.Error)
	State() State_t
}

type Cursor_table struct {
	Base_fields   []rsql.IDataslot // all fields of the table. Unused fields are nil.
	Gtabledef     *rsql.GTabledef
	Base_tabledef *rsql.Tabledef

	Lookup_type Sc_lookup_type_t

	// SC_LOOKUP_EQUALITY and SC_LOOKUP_RANGE

	Index_to_use                 *rsql.Tabledef     // the index to use to access the table
	Index_leaf_record            []rsql.IDataslot   // the leaf record of Index_to_use
	Index_is_covering            bool               // if false, the index must also access the base table to get missing column values
	Index_lookup                 []rsql.IDataslot   // same structure as Index_to_use leaf record, but only nk fields are filled with Sargs dataslots. The first Next call must walk the index btree native key to this position.
	Index_nk_sargs_expression_bb *rsql.Basicblock   // the first Next call must evaluate all computed expressions (right side of sarg: nk field <op> expression) for the Sargs in native key. May be nil, if expressions are all constants, variables, or columns; that is, if there is no operator nor function.
	Index_nk_sargs_expressions   []rsql.IDataslot   // Index_nk_sargs_expression_bb will put the computed expressions results in these dataslots
	Index_nk_sargs_bb            []*rsql.Basicblock // only bb for leading nk fields with Sargs. At each Next call, evaluate the comparison for each nk columns
	Index_nk_sargs_result        []*data.BOOLEAN    // result for above evaluation are stored here for each nk column

	Index_nk_fully_sarged bool // only for SC_LOOKUP_EQUALITY. Sargs are specified for all index nk fields. It means that at most one record can be returned.

	Index_nk_high_limit_bb           *rsql.Basicblock // only for SC_LOOKUP_RANGE.
	Index_nk_high_limit_col_dataslot rsql.IDataslot   // only for SC_LOOKUP_RANGE. This value is used only to check if it contains NULL.
	Index_nk_high_limit_result       *data.BOOLEAN    // only for SC_LOOKUP_RANGE. This value is checked only if Index_nk_high_limit_col_dataslot is not NULL.

	Index_other_conditions_bb     []*rsql.Basicblock // also used for SC_LOOKUP_BASE_TABLE_SCAN
	Index_other_conditions_result []*data.BOOLEAN    // also used for SC_LOOKUP_BASE_TABLE_SCAN

	// general attributes

	Index_selpad      *btree.Selpad
	Base_table_selpad *btree.Selpad

	current_page_no   cache.Page_no_t // page no, or PAGE_BOT or PAGE_EOT
	current_record_no uint64
	state             State_t
}

func New_cursor_table(gtabledef *rsql.GTabledef) *Cursor_table {

	curstable := &Cursor_table{
		Base_fields:   make([]rsql.IDataslot, len(gtabledef.Base.Td_coldefs)), // no dataslot is created here. They are created lazily when they are referenced by Lookup_name()
		Gtabledef:     gtabledef,
		Base_tabledef: gtabledef.Base,

		Lookup_type: SC_LOOKUP_BASE_TABLE_SCAN, // default

		current_page_no:   cache.PAGE_BOT,
		current_record_no: 0,
		state:             CURS_RECORD_RESET,
	}

	return curstable
}

// New_cursor_table_with_row is used to create cursor for flah tables (grouptables and sorttables).
//
func New_cursor_table_with_row(gtabledef *rsql.GTabledef, table_row rsql.Row) *Cursor_table {

	rsql.Assert(len(table_row) == len(gtabledef.Base.Td_coldefs))

	curstable := &Cursor_table{
		Base_fields:   table_row,
		Gtabledef:     gtabledef,
		Base_tabledef: gtabledef.Base,

		Lookup_type: SC_LOOKUP_BASE_TABLE_SCAN, // default

		current_page_no:   cache.PAGE_BOT,
		current_record_no: 0,
		state:             CURS_RECORD_RESET,
	}

	return curstable
}

func (curstable *Cursor_table) Reset() {
	curstable.current_page_no = cache.PAGE_BOT
	curstable.current_record_no = 0
	curstable.state = CURS_RECORD_RESET
	Set_row_to_NULL(curstable.Base_fields)
}

func (curstable *Cursor_table) State() State_t {
	return curstable.state
}

func (curstable *Cursor_table) Next(context *rsql.Context) (State_t, *rsql.Error) {

	switch curstable.Lookup_type {
	case SC_LOOKUP_EQUALITY:
		return curstable.Next_for_lookup_equality(context)

	case SC_LOOKUP_RANGE:
		return curstable.Next_for_lookup_range(context)

	case SC_LOOKUP_BASE_TABLE_SCAN:
		return curstable.Next_for_base_table_scan(context)

	default:
		panic("impossible")
	}
}

func (curstable *Cursor_table) Next_for_base_table_scan(context *rsql.Context) (State_t, *rsql.Error) {
	var (
		elem_page            *cache.Page_cache_element
		ppage                *cache.Page
		elem_page_no         cache.Page_no_t
		child_target_page_no cache.Page_no_t
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)

	defer func() {
		if elem_page != nil {
			wcache.Unpin(elem_page)
		}
	}()

LABEL_LOAD_RECORD:

	switch {
	case curstable.state == CURS_RECORD_RESET:
		page_info := wcache.Fetch_TRANSITORY_or_PUBLIC_page_info_and_pin_it(curstable.Base_tabledef) // read page info
		ppage_info := wcache.Ppage(page_info)
		root_page_no := ppage_info.Pinf_root_page_no()
		wcache.Unpin(page_info)
		page_info = nil
		ppage_info = nil

		if root_page_no == 0 { // table is empty, no leaf page exists
			curstable.current_page_no = cache.PAGE_BOT
			curstable.current_record_no = 0
			curstable.state = CURS_RECORD_RESET
			Set_row_to_NULL(curstable.Base_fields)
			return CURS_RECORD_RESET, nil
		}

		elem_page_no = root_page_no

		for { // start from root page, walk through node pages, to finally reach leaf page
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(curstable.Base_tabledef, elem_page_no)
			ppage = wcache.Ppage(elem_page)

			if ppage.Pg_type() == cache.PAGE_TYPE_LEAF {
				break
			}

			child_target_page_no = btree.Target_page_no_in_node(ppage, -1) // -1 is the bottom target page

			wcache.Unpin(elem_page)

			elem_page_no = child_target_page_no
		}

		assert(ppage.Pg_tuple_count() > 0)

		curstable.current_page_no = elem_page.Pce_page_no()
		curstable.current_record_no = 0

	case curstable.state == CURS_RECORD_AVAILABLE:
		if elem_page == nil {
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(curstable.Base_tabledef, curstable.current_page_no)
			ppage = wcache.Ppage(elem_page)
			assert(ppage.Pg_type() == cache.PAGE_TYPE_LEAF)
		}

		curstable.current_record_no++ // go to next record

		if int(curstable.current_record_no) >= ppage.Pg_tuple_count() { // go to next page, first record
			curstable.current_record_no = 0
			curstable.current_page_no = ppage.Pg_next_no()

			if curstable.current_page_no == cache.PAGE_EOT { // no more record
				curstable.current_page_no = cache.PAGE_BOT
				curstable.current_record_no = 0
				curstable.state = CURS_RECORD_RESET
				Set_row_to_NULL(curstable.Base_fields)
				return CURS_RECORD_RESET, nil
			}

			wcache.Unpin(elem_page)
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(curstable.Base_tabledef, curstable.current_page_no)
			ppage = wcache.Ppage(elem_page)
			assert(ppage.Pg_tuple_count() > 0)
		}

	default:
		panic("impossible")
	}

	// read record

	data.Read_row_from_leaf_page(curstable.Base_fields, curstable.Base_tabledef, ppage, int(curstable.current_record_no))

	curstable.state = CURS_RECORD_AVAILABLE

	// check periodically if client is alive

	if rsql_err := context.Check_keepalive(); rsql_err != nil {
		return 0, rsql_err
	}

	// check Condinfos

	for i, bb := range curstable.Index_other_conditions_bb {
		if _, rsql_err := rsql.Execute_basicblock(context, bb); rsql_err != nil {
			return 0, rsql_err
		}

		result := curstable.Index_other_conditions_result[i]

		if result.Error() != nil {
			return 0, result.Error()
		}

		if result.Is_true() == false { // if Cond is false, try with the next record
			goto LABEL_LOAD_RECORD
		}
	}

	return CURS_RECORD_AVAILABLE, nil
}

func (curstable *Cursor_table) Next_for_lookup_equality(context *rsql.Context) (State_t, *rsql.Error) {
	var (
		elem_page *cache.Page_cache_element
		ppage     *cache.Page
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)

	defer func() {
		if elem_page != nil {
			wcache.Unpin(elem_page)
		}
	}()

LABEL_LOAD_RECORD:

	switch {
	case curstable.state == CURS_RECORD_RESET:
		if curstable.Index_nk_sargs_expression_bb != nil { // Sarg values won't change during current index scan (until next RESET), as they depend on columns of previous tables
			if _, rsql_err := rsql.Execute_basicblock(context, curstable.Index_nk_sargs_expression_bb); rsql_err != nil {
				return 0, rsql_err
			}
		}

		for _, dataslot := range curstable.Index_nk_sargs_expressions { // check that expressions computed above are not error
			if dataslot.Error() != nil {
				return 0, dataslot.Error()
			}
		}

		count, index_page_no, index_tuple_no := curstable.Index_selpad.Lookup_nk_starting_row_and_load_record_equal_or_greater(wcache, curstable.Index_to_use, curstable.Index_lookup, curstable.Index_leaf_record) // returned record is equal or greater than Index_lookup, and Index_leaf_record has been loaded. If no record found, count == 0.

		assert(index_tuple_no >= 0)

		if count == 0 { // table is empty, or no equality record can be found
			curstable.current_page_no = cache.PAGE_BOT
			curstable.current_record_no = 0
			curstable.state = CURS_RECORD_RESET
			Set_row_to_NULL(curstable.Base_fields)
			return CURS_RECORD_RESET, nil
		}

		curstable.current_page_no = index_page_no
		curstable.current_record_no = uint64(index_tuple_no)

	case curstable.state == CURS_RECORD_AVAILABLE:
		if curstable.Index_nk_fully_sarged == true { // only one row is expected, corresponding to the full key, and it has previously been processed
			curstable.current_page_no = cache.PAGE_BOT
			curstable.current_record_no = 0
			curstable.state = CURS_RECORD_RESET
			Set_row_to_NULL(curstable.Base_fields)
			return CURS_RECORD_RESET, nil
		}

		if elem_page == nil {
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(curstable.Index_to_use, curstable.current_page_no)
			ppage = wcache.Ppage(elem_page)
			assert(ppage.Pg_type() == cache.PAGE_TYPE_LEAF)
		}

		curstable.current_record_no++ // go to next record

		if int(curstable.current_record_no) >= ppage.Pg_tuple_count() { // go to next page, first record
			curstable.current_record_no = 0
			curstable.current_page_no = ppage.Pg_next_no()

			if curstable.current_page_no == cache.PAGE_EOT { // no more record
				curstable.current_page_no = cache.PAGE_BOT
				curstable.current_record_no = 0
				curstable.state = CURS_RECORD_RESET
				Set_row_to_NULL(curstable.Base_fields)
				return CURS_RECORD_RESET, nil
			}

			wcache.Unpin(elem_page) // unpin previous page element
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(curstable.Index_to_use, curstable.current_page_no)
			ppage = wcache.Ppage(elem_page)
			assert(ppage.Pg_tuple_count() > 0)
		}

		data.Read_row_from_leaf_page(curstable.Index_leaf_record, curstable.Index_to_use, ppage, int(curstable.current_record_no))

	default:
		panic("impossible")
	}

	// check periodically if client is alive

	if rsql_err := context.Check_keepalive(); rsql_err != nil {
		return 0, rsql_err
	}

	//====== check if leading nk fields of the index comply with Sargs =======

	for i, bb := range curstable.Index_nk_sargs_bb {
		if _, rsql_err := rsql.Execute_basicblock(context, bb); rsql_err != nil {
			return 0, rsql_err
		}

		result := curstable.Index_nk_sargs_result[i]

		if result.Error() != nil {
			return 0, result.Error()
		}

		if result.Is_true() == false { // NULL or false. If leading nk fields don't match for current record, they won't match for subsequent records either
			curstable.current_page_no = cache.PAGE_BOT
			curstable.current_record_no = 0
			curstable.state = CURS_RECORD_RESET
			Set_row_to_NULL(curstable.Base_fields)
			return CURS_RECORD_RESET, nil
		}
	}

	curstable.state = CURS_RECORD_AVAILABLE // all comparison results are not error, and are true

	// read record from base table

	if curstable.Index_is_covering == false {
		count, _, _ := curstable.Base_table_selpad.Walk_base_table_for_native_key_and_load_record(wcache, curstable.Base_tabledef, curstable.Base_fields, curstable.Base_fields)

		if count != 1 { // base record not found
			return 0, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_INDEX_CORRUPTED, rsql.ERROR_BATCH_ABORT, curstable.Index_to_use.Td_index_name, dict.MASTER.Get_gtable_full_name(curstable.Index_to_use))
		}
	}

	// check Conds

	for i, bb := range curstable.Index_other_conditions_bb {
		if _, rsql_err := rsql.Execute_basicblock(context, bb); rsql_err != nil {
			return 0, rsql_err
		}

		result := curstable.Index_other_conditions_result[i]

		if result.Error() != nil {
			return 0, result.Error()
		}

		if result.Is_true() == false { // if Cond is false, try with the next record
			goto LABEL_LOAD_RECORD
		}
	}

	return CURS_RECORD_AVAILABLE, nil
}

func (curstable *Cursor_table) Next_for_lookup_range(context *rsql.Context) (State_t, *rsql.Error) {
	var (
		elem_page *cache.Page_cache_element
		ppage     *cache.Page
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)

	defer func() {
		if elem_page != nil {
			wcache.Unpin(elem_page)
		}
	}()

LABEL_LOAD_RECORD:

	switch {
	case curstable.state == CURS_RECORD_RESET:
		if curstable.Index_nk_sargs_expression_bb != nil { // Sarg values won't change during current index scan (until next RESET), as they depend on columns of previous tables
			if _, rsql_err := rsql.Execute_basicblock(context, curstable.Index_nk_sargs_expression_bb); rsql_err != nil {
				return 0, rsql_err
			}
		}

		for _, dataslot := range curstable.Index_nk_sargs_expressions { // check that expressions computed above are not error
			if dataslot.Error() != nil {
				return 0, dataslot.Error()
			}
		}

		count, index_page_no, index_tuple_no := curstable.Index_selpad.Lookup_nk_starting_row_and_load_record_equal_or_greater(wcache, curstable.Index_to_use, curstable.Index_lookup, curstable.Index_leaf_record) // returned record is equal or greater than Index_lookup, and Index_leaf_record has been loaded. If no record found, count == 0.

		assert(index_tuple_no >= 0)

		if count == 0 { // table is empty, or no equality record can be found
			curstable.current_page_no = cache.PAGE_BOT
			curstable.current_record_no = 0
			curstable.state = CURS_RECORD_RESET
			Set_row_to_NULL(curstable.Base_fields)
			return CURS_RECORD_RESET, nil
		}

		curstable.current_page_no = index_page_no
		curstable.current_record_no = uint64(index_tuple_no)

	case curstable.state == CURS_RECORD_AVAILABLE:
		if elem_page == nil {
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(curstable.Index_to_use, curstable.current_page_no)
			ppage = wcache.Ppage(elem_page)
			assert(ppage.Pg_type() == cache.PAGE_TYPE_LEAF)
		}

		curstable.current_record_no++ // go to next record

		if int(curstable.current_record_no) >= ppage.Pg_tuple_count() { // go to next page, first record
			curstable.current_record_no = 0
			curstable.current_page_no = ppage.Pg_next_no()

			if curstable.current_page_no == cache.PAGE_EOT { // no more record
				curstable.current_page_no = cache.PAGE_BOT
				curstable.current_record_no = 0
				curstable.state = CURS_RECORD_RESET
				Set_row_to_NULL(curstable.Base_fields)
				return CURS_RECORD_RESET, nil
			}

			wcache.Unpin(elem_page) // unpin previous page element
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(curstable.Index_to_use, curstable.current_page_no)
			ppage = wcache.Ppage(elem_page)
			assert(ppage.Pg_tuple_count() > 0)
		}

		data.Read_row_from_leaf_page(curstable.Index_leaf_record, curstable.Index_to_use, ppage, int(curstable.current_record_no))

	default:
		panic("impossible")
	}

	// check periodically if client is alive

	if rsql_err := context.Check_keepalive(); rsql_err != nil {
		return 0, rsql_err
	}

	//====== check if leading nk fields of the index comply with Sargs =======

	for i, bb := range curstable.Index_nk_sargs_bb {
		if _, rsql_err := rsql.Execute_basicblock(context, bb); rsql_err != nil {
			return 0, rsql_err
		}

		result := curstable.Index_nk_sargs_result[i]

		if result.Error() != nil {
			return 0, result.Error()
		}

		if result.Is_true() == false {
			curstable.state = CURS_RECORD_AVAILABLE
			goto LABEL_LOAD_RECORD
		}
	}

	// check high limit

	if curstable.Index_nk_high_limit_result != nil {
		if _, rsql_err := rsql.Execute_basicblock(context, curstable.Index_nk_high_limit_bb); rsql_err != nil {
			return 0, rsql_err
		}

		result := curstable.Index_nk_high_limit_result

		if result.Error() != nil {
			return 0, result.Error()
		}

		if curstable.Index_nk_high_limit_col_dataslot.NULL_flag() == false && result.Is_true() == false { // end of range lookup. Result is checked only if nk field is not null. Else, the result would be false and would stop the processing prematurely when there is no '>' or '>=' sarg. E.g.  SELECT * FROM t WHERE a < 100
			curstable.current_page_no = cache.PAGE_BOT
			curstable.current_record_no = 0
			curstable.state = CURS_RECORD_RESET
			Set_row_to_NULL(curstable.Base_fields)
			return CURS_RECORD_RESET, nil
		}
	}

	curstable.state = CURS_RECORD_AVAILABLE // all comparison results are not error, and are true

	// read record from base table

	if curstable.Index_is_covering == false {
		count, _, _ := curstable.Base_table_selpad.Walk_base_table_for_native_key_and_load_record(wcache, curstable.Base_tabledef, curstable.Base_fields, curstable.Base_fields)

		if count != 1 { // base record not found
			return 0, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_INDEX_CORRUPTED, rsql.ERROR_BATCH_ABORT, curstable.Index_to_use.Td_index_name, dict.MASTER.Get_gtable_full_name(curstable.Index_to_use))
		}
	}

	// check Conds

	for i, bb := range curstable.Index_other_conditions_bb {
		if _, rsql_err := rsql.Execute_basicblock(context, bb); rsql_err != nil {
			return 0, rsql_err
		}

		result := curstable.Index_other_conditions_result[i]

		if result.Error() != nil {
			return 0, result.Error()
		}

		if result.Is_true() == false { // if Cond is false, try with the next record
			goto LABEL_LOAD_RECORD
		}
	}

	return CURS_RECORD_AVAILABLE, nil
}

type Cursor_join struct {
	Jtype Join_type_t

	Left  Cursor
	Right Cursor

	On_basicblock *rsql.Basicblock
	On_result     *data.BOOLEAN

	state State_t
}

func New_cursor_join(join_type Join_type_t, curs_left Cursor, curs_right Cursor) *Cursor_join {

	cursjoin := &Cursor_join{
		Jtype: join_type,

		Left:  curs_left,
		Right: curs_right,

		state: CURS_RECORD_RESET,
	}

	return cursjoin
}

func (cursjoin *Cursor_join) Reset() {

	cursjoin.Left.Reset()
	cursjoin.Right.Reset()

	//cursjoin.On_result should not be set to NULL, because if it is KIND_RO_LEAF or KIND_VAR_LEAF, it would overwrite these values

	cursjoin.state = CURS_RECORD_RESET
}

func (cursjoin *Cursor_join) State() State_t {
	return cursjoin.state
}

func (cursjoin *Cursor_join) Next(context *rsql.Context) (State_t, *rsql.Error) {
	var (
		rsql_err   *rsql.Error
		rc         State_t
		must_print bool // used for LEFT JOIN
	)

	for {
		if cursjoin.Right.State() == CURS_RECORD_RESET {
			if must_print == true {
				cursjoin.state = CURS_RECORD_AVAILABLE // all fields of right row have been set to NULL
				return CURS_RECORD_AVAILABLE, nil
			}

			if rc, rsql_err = cursjoin.Left.Next(context); rsql_err != nil {
				return 0, rsql_err
			}

			if rc == CURS_RECORD_RESET {
				cursjoin.state = CURS_RECORD_RESET
				return CURS_RECORD_RESET, nil
			}

			if cursjoin.Jtype == JT_LEFT_JOIN {
				must_print = true
			}
		}

		assert(cursjoin.Left.State() == CURS_RECORD_AVAILABLE)

		if rc, rsql_err = cursjoin.Right.Next(context); rsql_err != nil {
			return 0, rsql_err
		}

		if rc == CURS_RECORD_RESET {
			continue
		}

		// process ON basicblock

		if cursjoin.On_basicblock != nil {
			if _, rsql_err := rsql.Execute_basicblock(context, cursjoin.On_basicblock); rsql_err != nil {
				return 0, rsql_err
			}

			if rsql_err := cursjoin.On_result.Error(); rsql_err != nil {
				return 0, rsql_err
			}

			if cursjoin.On_result.Is_true() {
				cursjoin.state = CURS_RECORD_AVAILABLE
				return CURS_RECORD_AVAILABLE, nil
			}
		} else {
			cursjoin.state = CURS_RECORD_AVAILABLE
			return CURS_RECORD_AVAILABLE, nil
		}
	}
}

type Cursor_single struct {
	state State_t
}

func New_cursor_single() *Cursor_single {

	curssingle := &Cursor_single{
		state: CURS_RECORD_RESET,
	}

	return curssingle
}

func (curssingle *Cursor_single) Reset() {

	curssingle.state = CURS_RECORD_RESET
}

func (curssingle *Cursor_single) State() State_t {
	return curssingle.state
}

func (curssingle *Cursor_single) Next(context *rsql.Context) (State_t, *rsql.Error) {

	if curssingle.state == CURS_RECORD_RESET {
		curssingle.state = CURS_RECORD_AVAILABLE
		return CURS_RECORD_AVAILABLE, nil
	}

	curssingle.state = CURS_RECORD_RESET
	return CURS_RECORD_RESET, nil
}

type Cursor_from struct {
	Root_curs        Cursor
	Where_basicblock *rsql.Basicblock
	Where_result     *data.BOOLEAN

	state State_t
}

func New_cursor_from(root_curs Cursor) *Cursor_from {

	cursfrom := &Cursor_from{}

	cursfrom.Root_curs = root_curs

	cursfrom.state = CURS_RECORD_RESET

	return cursfrom
}

func (cursfrom *Cursor_from) Reset() {

	cursfrom.Root_curs.Reset()

	//cursfrom.Where_result should not be set to NULL, because if it is KIND_RO_LEAF or KIND_VAR_LEAF, it would overwrite these values

	cursfrom.state = CURS_RECORD_RESET
}

func (cursfrom *Cursor_from) State() State_t {
	return cursfrom.state
}

func (cursfrom *Cursor_from) Next(context *rsql.Context) (State_t, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		rc       State_t
	)

	for {
		if rc, rsql_err = cursfrom.Root_curs.Next(context); rsql_err != nil {
			return 0, rsql_err
		}

		if rc == CURS_RECORD_RESET {
			cursfrom.state = CURS_RECORD_RESET
			return CURS_RECORD_RESET, nil
		}

		// process WHERE basicblock

		if cursfrom.Where_result != nil {
			if _, rsql_err := rsql.Execute_basicblock(context, cursfrom.Where_basicblock); rsql_err != nil {
				return 0, rsql_err
			}

			if rsql_err := cursfrom.Where_result.Error(); rsql_err != nil {
				return 0, rsql_err
			}

			if cursfrom.Where_result.Is_true() {
				cursfrom.state = CURS_RECORD_AVAILABLE
				return CURS_RECORD_AVAILABLE, nil
			}
		} else {
			cursfrom.state = CURS_RECORD_AVAILABLE
			return CURS_RECORD_AVAILABLE, nil
		}
	}
}

// Cursor for SELECT
//
// Cols_facade contains all the columns as KIND_COL_LEAF, and are copies of Cols_dataslots.
// When Cursor_select is Reset(), these copies can be safely set to NULL, without fear of overwriting a @variable or constant (KIND_VAR_LEAF or KIND_RO_LEAF).
//
type Cursor_select struct {
	Cols_facade []rsql.IDataslot // for external use. Exact copies of Cols_dataslots, but with type KIND_COL_LEAF. Each iteration just copy values from Cols_dataslots into Cols_facade.

	Cols_dataslots  []rsql.IDataslot // all SELECT columns. Dataslots can be KIND_RO_LEAF, KIND_VAR_LEAF, KIND_COL_LEAF or KIND_TMP. They are copied to Cols_facade.
	Cols_basicblock *rsql.Basicblock

	From *Cursor_from

	state State_t

	// grouptable for GROUP BY

	Grouping_flag                   bool             // true if grouping SELECT
	Grouping_explicit_GROUP_BY      bool             // SELECT has an explicit GROUP BY clause
	Grouptable_gtabledef            *rsql.GTabledef  // GTabledef of grouptable
	Grouptable_feeding_cursor       *Cursor_from     // Cursor_from from the original SELECT, which feeds grouptable
	Grouptable_nk_basicblock        *rsql.Basicblock // computes the nk fields (fields of the GROUP BY clause) of Grouptable_insertion_row
	Grouptable_insertion_row        rsql.Row         // row to insert in grouptable if not exists. Used for row lookup.
	Grouptable_upsert_row           rsql.Row         // upsert row
	Grouptable_injection_basicblock *rsql.Basicblock // injects values into aggregate fields of Grouptable_upsert_row
	Grouptable_is_created           bool             // during execution, if false, grouptable has not been created
}

func New_cursor_select(curs_from *Cursor_from, dataslot_list []rsql.IDataslot) *Cursor_select {

	cursselect := &Cursor_select{}

	cursselect.From = curs_from
	cursselect.Cols_dataslots = dataslot_list

	cursselect.Cols_facade = make([]rsql.IDataslot, len(dataslot_list))
	for i, d := range cursselect.Cols_dataslots {
		cursselect.Cols_facade[i] = data.Dataslot_XXX_clone(rsql.KIND_COL_LEAF, d)
	}

	cursselect.state = CURS_RECORD_RESET

	return cursselect
}

func (cursselect *Cursor_select) Reset() {

	Set_row_to_NULL(cursselect.Cols_facade)

	cursselect.From.Reset()

	cursselect.state = CURS_RECORD_RESET
}

func (cursselect *Cursor_select) State() State_t {
	return cursselect.state
}

func (cursselect *Cursor_select) Next(context *rsql.Context) (State_t, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		rc       State_t
	)

	if cursselect.Grouping_flag == true && cursselect.state == CURS_RECORD_RESET {
		wcache := context.Ctx_wcache.(*cache.Wcache)
		assert(cursselect.Grouptable_is_created == false)
		if rsql_err := wcache.Create_info_page_and_first_zone_allocator(cursselect.Grouptable_gtabledef.Base); rsql_err != nil {
			return 0, rsql_err
		}
		cursselect.Grouptable_is_created = true

		// fill grouptable

		if rsql_err := Upsert_grouptable(context,
			rsql.Object_qname_t{Object_name: cursselect.Grouptable_gtabledef.Gtable_name},
			cursselect.Grouptable_gtabledef,
			cursselect.Grouptable_feeding_cursor,
			cursselect.Grouptable_nk_basicblock,
			cursselect.Grouptable_injection_basicblock,
			cursselect.Grouptable_insertion_row,
			cursselect.Grouptable_upsert_row,
			cursselect.Grouping_explicit_GROUP_BY); rsql_err != nil {
			return 0, rsql_err
		}

		tblid := cache.Tblid_t(cursselect.Grouptable_gtabledef.Base.Td_tblid)
		wcache.Transform_all_DRAFT_elements_to_TRANSITORY_for_group_or_sort_table(tblid)
	}

	if rc, rsql_err = cursselect.From.Next(context); rsql_err != nil {
		return 0, rsql_err
	}

	if rc == CURS_RECORD_RESET {
		if cursselect.Grouping_flag == true { // discard all Page_cache_elements of grouptable from wcache
			assert(cursselect.Grouptable_is_created == true)
			wcache := context.Ctx_wcache.(*cache.Wcache)
			tblid := cache.Tblid_t(cursselect.Grouptable_gtabledef.Base.Td_tblid)
			wcache.Discard_all_TRANSITORY_elements_for_group_or_sort_table(tblid)
			cursselect.Grouptable_is_created = false
		}

		Set_row_to_NULL(cursselect.Cols_facade)

		cursselect.state = CURS_RECORD_RESET
		return CURS_RECORD_RESET, nil
	}

	if _, rsql_err := rsql.Execute_basicblock(context, cursselect.Cols_basicblock); rsql_err != nil { // updates cursselect.Cols_dataslots
		return 0, rsql_err
	}

	for _, dataslot := range cursselect.Cols_dataslots {
		if rsql_err := dataslot.Error(); rsql_err != nil {
			return 0, rsql_err
		}
	}

	for i, dest := range cursselect.Cols_facade {
		data.Dataslot_XXX_copy(dest, cursselect.Cols_dataslots[i])
	}

	cursselect.state = CURS_RECORD_AVAILABLE
	return CURS_RECORD_AVAILABLE, nil
}

type Cursor_union struct {
	Equalized_dataslots  []rsql.IDataslot   // equalized row for each cursor. Always KIND_COL_LEAF. Columns of SELECT or UNION members are copied here at each Next() iteration.
	Equalize_basicblocks []*rsql.Basicblock // CAST or copy instructions

	List_cursors   []Cursor // SELECT and UNION cursors
	Current_member int

	state State_t
}

func New_cursor_union(curs_list []Cursor, equalized_dataslot_list []rsql.IDataslot) *Cursor_union {

	cursunion := &Cursor_union{}

	cursunion.Equalized_dataslots = equalized_dataslot_list

	cursunion.List_cursors = curs_list

	cursunion.state = CURS_RECORD_RESET

	return cursunion
}

func (cursunion *Cursor_union) Reset() {

	Set_row_to_NULL(cursunion.Equalized_dataslots)

	for _, curs := range cursunion.List_cursors {
		curs.Reset()
	}

	cursunion.Current_member = 0

	cursunion.state = CURS_RECORD_RESET
}

func (cursunion *Cursor_union) State() State_t {
	return cursunion.state
}

func (cursunion *Cursor_union) Next(context *rsql.Context) (State_t, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		rc       State_t
	)

	for {
		curs := cursunion.List_cursors[cursunion.Current_member]

		if rc, rsql_err = curs.Next(context); rsql_err != nil {
			return 0, rsql_err
		}

		if rc == CURS_RECORD_AVAILABLE {
			break
		}

		cursunion.Current_member++
		if cursunion.Current_member >= len(cursunion.List_cursors) {
			Set_row_to_NULL(cursunion.Equalized_dataslots)
			cursunion.Current_member = 0
			cursunion.state = CURS_RECORD_RESET
			return CURS_RECORD_RESET, nil
		}
	}

	if _, rsql_err := rsql.Execute_basicblock(context, cursunion.Equalize_basicblocks[cursunion.Current_member]); rsql_err != nil {
		return 0, rsql_err
	}

	for _, dataslot := range cursunion.Equalized_dataslots {
		if rsql_err := dataslot.Error(); rsql_err != nil {
			return 0, rsql_err
		}
	}

	cursunion.state = CURS_RECORD_AVAILABLE
	return CURS_RECORD_AVAILABLE, nil
}
