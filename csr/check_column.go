package csr

import (
	"rsql"
	"rsql/data"
)

// check_column_not_null checks that column_name contains no NULL value.
//
// It is used by the statement    ALTER TABLE t ALTER COLUMN a NOT NULL.
//
func check_column_not_null(context *rsql.Context, gtabledef *rsql.GTabledef, column_name string) *rsql.Error {
	var (
		rsql_err  *rsql.Error
		curstable *Cursor_table
		dataslot  rsql.IDataslot
	)

	curstable = New_cursor_table(gtabledef)

	if dataslot, rsql_err = lookup_name(curstable, column_name); rsql_err != nil {
		return rsql_err
	}

	curstable.Reset()

	for {
		state, rsql_err := curstable.Next(context)
		if rsql_err != nil {
			return rsql_err
		}

		switch state {
		case CURS_RECORD_RESET:
			return nil

		case CURS_RECORD_AVAILABLE:
			rsql.Assert(dataslot.Error() == nil)

			if dataslot.NULL_flag() {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COLUMN_CONTAINS_NULL, rsql.ERROR_BATCH_ABORT, column_name)
			}

		default:
			panic("impossible")
		}
	}

	return nil
}

func lookup_name(curstable *Cursor_table, column_name string) (dataslot rsql.IDataslot, rsql_err *rsql.Error) {
	var (
		coldef         *rsql.Coldef
		ok             bool
		col_base_seqno uint16
	)

	if coldef, ok = curstable.Gtabledef.Colmap[column_name]; ok == false {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_NOT_FOUND, rsql.ERROR_BATCH_ABORT, column_name)
	}

	col_base_seqno = coldef.Cd_base_seqno

	rsql.Assert(curstable.Base_fields[col_base_seqno] == nil)

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
	curstable.Base_fields[col_base_seqno] = dataslot

	rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)

	return dataslot, nil
}
