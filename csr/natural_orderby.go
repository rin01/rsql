package csr

import (
	"rsql"
)

type Natural_order []rsql.IDataslot // for the leftmost table, list of natural order dataslots

// Cursor_natural_order returns the natural order dataslots of the leftmost table if the FROM clause.
//
// Only the leftmost table is significant, because other joined table are most often accessed by index, and cannot be taken into account for natural order.
//
func Cursor_natural_order(curs_from *Cursor_from) []rsql.IDataslot {
	var (
		nt         Natural_order
		curs       Cursor
		curs_table *Cursor_table
		ok         bool
	)

	if curs_from == nil {
		return nil
	}

	nt = make([]rsql.IDataslot, 0, 4)

	curs = curs_from.Root_curs

	for { // walk join tree on the left side
		if curs_join, ok := curs.(*Cursor_join); ok == true {
			curs = curs_join.Left
			continue
		}

		break
	}

	if curs_table, ok = curs.(*Cursor_table); ok == true { // a table has been found
		var native_order_key []*rsql.Coldef

		// base table native key order

		switch curs_table.Lookup_type {
		case SC_LOOKUP_BASE_TABLE_SCAN:
			native_order_key = curs_table.Base_tabledef.Td_nk

		default:
			native_order_key = curs_table.Index_to_use.Td_nk
		}

		// index order

		nt = make([]rsql.IDataslot, 0, len(native_order_key))

		for _, coldef := range native_order_key {
			dataslot := curs_table.Base_fields[coldef.Cd_base_seqno]
			if dataslot == nil {
				break
			}

			nt = append(nt, dataslot)
		}
	}

	return nt
}
