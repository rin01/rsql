package csr

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
)

func Upsert_grouptable(context *rsql.Context, table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef, feeding_cursor *Cursor_from, nk_basicblock *rsql.Basicblock, injection_basicblock *rsql.Basicblock, insertion_row rsql.Row, upsert_row rsql.Row, explicit_GROUP_BY_exists bool) *rsql.Error {
	var (
		rsql_err          *rsql.Error
		selpad            *btree.Selpad
		inspad            *btree.Inspad
		delpad            *btree.Delpad
		upsert_row_status uint8
		page_no           cache.Page_no_t
		tuple_index       int
	)

	const (
		UPSERT_ROW_STATUS_INVALID uint8 = iota
		UPSERT_ROW_STATUS_DIRTY
	)

	upsert_row_status = UPSERT_ROW_STATUS_INVALID

	wcache := context.Ctx_wcache.(*cache.Wcache)

	if selpad, rsql_err = btree.New_selpad(gtabledef.Base); rsql_err != nil {
		return rsql_err
	}

	if inspad, rsql_err = btree.New_inspad(gtabledef.Base, table_qname); rsql_err != nil {
		return rsql_err
	}

	if delpad, rsql_err = btree.New_delpad(gtabledef.Base); rsql_err != nil {
		return rsql_err
	}

	// ======= read feeding records and insert/update grouptable records  =======

	feeding_cursor.Reset()

	for {
		// read feeding record

		var rsql_err *rsql.Error
		var rc State_t

		if rc, rsql_err = feeding_cursor.Next(context); rsql_err != nil {
			return rsql_err
		}

		if rc == CURS_RECORD_RESET {
			break
		}

		// compute nk fields of insertion_row

		if _, rsql_err := rsql.Execute_basicblock(context, nk_basicblock); rsql_err != nil {
			return rsql_err
		}

		for _, dataslot := range insertion_row {
			if rsql_err := dataslot.Error(); rsql_err != nil {
				return rsql_err
			}
		}

		// look up for grouptable record and process the aggregation

		switch upsert_row_status {
		case UPSERT_ROW_STATUS_INVALID:
			// insert insertion_row if not exists, and load record into upsert_row

			if page_no, tuple_index, rsql_err = Grouptable_lookup_equality_or_insert(wcache, gtabledef, insertion_row, upsert_row, selpad, inspad); rsql_err != nil {
				return rsql_err
			}

			upsert_row_status = UPSERT_ROW_STATUS_DIRTY

		case UPSERT_ROW_STATUS_DIRTY: // check if current upsert_row has same native key as insertion_row
			if selpad.Equality_leaf_rows_native_key_for_sort(gtabledef.Base, insertion_row, upsert_row) == false { // if not, save it to page and refresh upsert_row
				// update tuple in page for upsert_row

				if rsql_err := grouptable_tuple_update(wcache, gtabledef.Base, page_no, tuple_index, upsert_row, delpad, inspad); rsql_err != nil {
					return rsql_err
				}

				// insert insertion_row if not exists, and refresh upsert_row by loading proper record into it

				if page_no, tuple_index, rsql_err = Grouptable_lookup_equality_or_insert(wcache, gtabledef, insertion_row, upsert_row, selpad, inspad); rsql_err != nil {
					return rsql_err
				}
			}
		}

		// apply aggregate function to upsert_row

		if _, rsql_err := rsql.Execute_basicblock(context, injection_basicblock); rsql_err != nil {
			return rsql_err
		}

		for _, dataslot := range upsert_row {
			if rsql_err := dataslot.Error(); rsql_err != nil {
				return rsql_err
			}
		}

		// check periodically if client is alive

		if rsql_err := context.Check_keepalive(); rsql_err != nil {
			return rsql_err
		}
	} // end loop

	switch upsert_row_status {
	case UPSERT_ROW_STATUS_INVALID: // grouptable is empty. Insert a row with 0 or NULL for SELECT with no explicit GROUP BY clause.
		if explicit_GROUP_BY_exists == false {
			if _, _, rsql_err = Grouptable_lookup_equality_or_insert(wcache, gtabledef, insertion_row, upsert_row, selpad, inspad); rsql_err != nil {
				return rsql_err
			}
		}

	case UPSERT_ROW_STATUS_DIRTY:
		// update tuple in page

		if rsql_err := grouptable_tuple_update(wcache, gtabledef.Base, page_no, tuple_index, upsert_row, delpad, inspad); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

func grouptable_tuple_update(wcache *cache.Wcache, tabledef *rsql.Tabledef, page_no cache.Page_no_t, tuple_index int, upsert_row rsql.Row, delpad *btree.Delpad, inspad *btree.Inspad) *rsql.Error {
	var (
		rsql_err *rsql.Error
		count    int
	)

	// try to update in-place

	if count, rsql_err = inspad.Replace_one_row_in_page_in_place(wcache, tabledef, page_no, tuple_index, upsert_row); rsql_err != nil {
		return rsql_err
	}

	// if update in-place failed because no room available in page, delete and insert upsert_row

	if count != 1 {
		assert(count == 0)

		count = delpad.Delete_row_from_table(wcache, tabledef, upsert_row) // only fields belonging to a native key (of base table or index) contain a Dataslot, others are nil
		assert(count == 1)

		for _, dataslot := range upsert_row {
			if rsql_err := dataslot.Error(); rsql_err != nil {
				return rsql_err
			}
		}

		if rsql_err := inspad.Insert_row_into_table(wcache, tabledef, upsert_row); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

// Grouptable_lookup_equality_or_insert looks up the row which native key is same as base_insertion_row native key.
// If not found, base_insertion_row is inserted.
// Tuple is loaded into base_row.
//
func Grouptable_lookup_equality_or_insert(wcache *cache.Wcache, gtabledef *rsql.GTabledef, base_insertion_row rsql.Row, base_row rsql.Row, selpad *btree.Selpad, inspad *btree.Inspad) (page_no cache.Page_no_t, tuple_index int, rsql_err *rsql.Error) {
	var (
		count int
	)

	// lookup and read record from grouptable

	base_tabledef := gtabledef.Base

	count, page_no, tuple_index = selpad.Walk_base_table_for_native_key_and_load_record_for_grouptable(wcache, base_tabledef, base_insertion_row, base_row)

	if count == 0 { // if base record not found, insert base_insertion_row
		if rsql_err := btree.Check_dataslot_error_and_NOT_NULL_and_update_ROWID_and_IDENTITY_columns(wcache, gtabledef, base_insertion_row, false); rsql_err != nil { // check that no dataslot contains error. Update value of ROWID.
			return 0, 0, rsql_err
		}

		// insert row into base table

		if rsql_err := inspad.Insert_row_into_table(wcache, base_tabledef, base_insertion_row); rsql_err != nil {
			return 0, 0, rsql_err
		}

		// lookup and read record from grouptable

		count, page_no, tuple_index = selpad.Walk_base_table_for_native_key_and_load_record_for_grouptable(wcache, base_tabledef, base_insertion_row, base_row)
	}

	assert(count == 1)

	return page_no, tuple_index, nil
}
