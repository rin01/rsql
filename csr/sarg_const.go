package csr

import (
	"fmt"

	"rsql"
	"rsql/data"
)

type Sc_lookup_type_t uint8

// the value of these constants are used for sort purpose. Don't change their order !
const (
	SC_LOOKUP_EQUALITY        Sc_lookup_type_t = iota + 1 // score can be used for index lookup
	SC_LOOKUP_RANGE                                       // score can be used for index lookup
	SC_LOOKUP_BASE_TABLE_SCAN                             // score CANNOT BE USED for index lookup, but these Condinfos can be checked during base table scan
)

func (st Sc_lookup_type_t) String() string {

	switch st {
	case SC_LOOKUP_EQUALITY:
		return "SC_LOOKUP_EQUALITY"
	case SC_LOOKUP_RANGE:
		return "SC_LOOKUP_RANGE"
	case SC_LOOKUP_BASE_TABLE_SCAN:
		return "SC_LOOKUP_BASE_TABLE_SCAN"
	default:
		panic("impossible")
	}
}

type Sarg_comp_t uint8

const (
	SARG_GREATER Sarg_comp_t = iota + 1
	SARG_GREATER_EQUAL
	SARG_EQUAL
	SARG_LESS_EQUAL
	SARG_LESS
)

func (s Sarg_comp_t) String() string {

	switch s {
	case SARG_GREATER:
		return ">"

	case SARG_GREATER_EQUAL:
		return ">="

	case SARG_EQUAL:
		return "="

	case SARG_LESS_EQUAL:
		return "<="

	case SARG_LESS:
		return "<"

	default:
		panic("impossible")
	}
}

type Sarg struct {
	Table_qname        rsql.Object_qname_t
	Table_cursor       *Cursor_table
	Col_no             uint16
	Comp               Sarg_comp_t
	Comp_args_reversed bool // if true, column is on the right side of the comparison operator
	Comp_dataslot      *data.BOOLEAN
}

func (s *Sarg) String() string {

	return fmt.Sprintf("%s(%d) %s", s.Table_qname, s.Col_no, s.Comp.String())

}

type Cond struct {
	Cond_seqno int

	Cond_dataslot *data.BOOLEAN
}
