package cache

import (
	"fmt"
	"log"
	"os"

	"rsql"
)

// Init initializes the global read cache.
// Arrys of Page_cache_elements and arrays of Pages are created.
// All Page_cache_elements are put in free list.
//
// gpage_cache_memory argument is cache memory, in bytes. If it is too small, a suitable minimal value is used instead.
//
func (gpage_cache *Gpage_cache) Init(gpage_cache_memory uint64) {
	var (
		elem *Page_cache_element
		i    uint64
		j    uint64
	)

	// compute GPC_NUMBER_OF_PAGES_PER_CHUNK

	GPC_NUMBER_OF_PAGES_PER_CHUNK = gpage_cache_memory / PAGE_SIZE / GPC_ELEMENT_ARRAY_NUMBER
	GPC_TOTAL_NUMBER_OF_PAGES = GPC_NUMBER_OF_PAGES_PER_CHUNK * GPC_ELEMENT_ARRAY_NUMBER

	if GPC_TOTAL_NUMBER_OF_PAGES < GPC_TOTAL_NUMBER_OF_PAGES_MIN { // if total number of pages is too small
		GPC_NUMBER_OF_PAGES_PER_CHUNK = GPC_TOTAL_NUMBER_OF_PAGES_MIN / GPC_ELEMENT_ARRAY_NUMBER
		GPC_TOTAL_NUMBER_OF_PAGES = GPC_NUMBER_OF_PAGES_PER_CHUNK * GPC_ELEMENT_ARRAY_NUMBER

		if GPC_TOTAL_NUMBER_OF_PAGES < GPC_TOTAL_NUMBER_OF_PAGES_MIN { // GPC_TOTAL_NUMBER_OF_PAGES_MIN is not a multiple of GPC_ELEMENT_ARRAY_NUMBER
			GPC_NUMBER_OF_PAGES_PER_CHUNK++
			GPC_TOTAL_NUMBER_OF_PAGES = GPC_NUMBER_OF_PAGES_PER_CHUNK * GPC_ELEMENT_ARRAY_NUMBER
		}

		log.Printf("Init: global read cache size requested (%.1f MB) is too small. Resized to %.1f MB.", float64(gpage_cache_memory)/1024/1024, float64(GPC_TOTAL_NUMBER_OF_PAGES*PAGE_SIZE)/1024/1024)
	}

	// create ppage array

	for i = 0; i < GPC_ELEMENT_ARRAY_NUMBER; i++ {
		gpage_cache.gpc_ppage_array[i] = make([]Page, GPC_NUMBER_OF_PAGES_PER_CHUNK)
	}

	// create element array

	for i = 0; i < GPC_ELEMENT_ARRAY_NUMBER; i++ {
		gpage_cache.gpc_element_array[i] = make([]Page_cache_element, GPC_NUMBER_OF_PAGES_PER_CHUNK)

		for j = 0; j < GPC_NUMBER_OF_PAGES_PER_CHUNK; j++ {
			elem = &gpage_cache.gpc_element_array[i][j]
			elem.pce_bpos = bpos(i, j)
			elem.pce_version = PCE_FREED
			elem.pce_listinfo = PCE_ILLEGAL_LIST

			elem.pce_chain.Prev = BPOS_ILLEGAL
			elem.pce_chain.Next = BPOS_ILLEGAL
			elem.pce_enlistment.Prev = BPOS_ILLEGAL
			elem.pce_enlistment.Next = BPOS_ILLEGAL

			elem.pce_ppage_bpos = elem.pce_bpos // will never change
			elem.pce_logpage_no = -1            // global read cache pages are never logged

			//fmt.Printf("<%d, %d>\n", i, j)
			gpage_cache.insert_first(PCE_FREE_LIST, elem)
		}
	}

	assert(gpage_cache.gpc_free_list.Count == GPC_TOTAL_NUMBER_OF_PAGES)

	// create hashtable

	hashtable_size := rsql.Prime_for_hashtable(uint32(GPC_TOTAL_NUMBER_OF_PAGES + GPC_TOTAL_NUMBER_OF_PAGES/4)) // hashtable size is 25 % larger than total number of elements

	gpage_cache.gpc_hashtable = make([]Hlist, hashtable_size)
	gpage_cache.gpc_hashtable_size = uint64(len(gpage_cache.gpc_hashtable))

	assert(uint64(hashtable_size) == gpage_cache.gpc_hashtable_size)

	log.Printf("Init: global read cache size == %d pages of %d bytes (%.1f MB)\n", GPC_TOTAL_NUMBER_OF_PAGES, PAGE_SIZE, float64(GPC_TOTAL_NUMBER_OF_PAGES*PAGE_SIZE)/1024/1024)
}

// for debugging only.
//
//    WARNING: this function is not goroutine-safe, as it doesn't lock the cache.
//
func (gpage_cache *Gpage_cache) Debug_print(listinfo Dlist_type_t) {
	var (
		anchor    *Dlist_anchor
		elem_bpos bpos_t
		elem      *Page_cache_element
	)

	switch listinfo {
	case PCE_PIN_LIST:
		anchor = &gpage_cache.gpc_pin_list
		fmt.Printf("---Read cache: Pin list (count: %d) ---\n", anchor.Count)

	case PCE_LRU_LIST:
		anchor = &gpage_cache.gpc_lru_list
		fmt.Printf("---Read cache: Lru list (count: %d) ---\n", anchor.Count)

	case PCE_FREE_LIST:
		anchor = &gpage_cache.gpc_free_list
		fmt.Printf("---Read cache: Free list (count: %d) ---\n", anchor.Count)
	}

	for elem_bpos = anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_enlistment.Next {
		elem = gpage_cache.g_elem(elem_bpos)

		if listinfo != PCE_FREE_LIST {
			fmt.Printf("pce: %d     tblid: %d   page_no:%d\n", elem.pce_bpos, elem.pce_tblid, elem.pce_page_no)
		} else {
			fmt.Printf("pce: %d\n", elem.pce_bpos)
		}
	}

}

// for debugging only.
//
//    WARNING: this function is not goroutine-safe, as it doesn't lock the cache.
//
func (gpage_cache *Gpage_cache) Lists_count() (pin_count uint64, lru_count uint64, free_count uint64) {

	pin_count = gpage_cache.gpc_pin_list.Count
	lru_count = gpage_cache.gpc_lru_list.Count
	free_count = gpage_cache.gpc_free_list.Count

	return pin_count, lru_count, free_count
}

// gpc_discard_lru_element removes element from lru list and puts it into free list.
//
// The element is removed from bucket chain and lru list, put in free list, and elem fields are cleared.
// It is used, e.g. when there is no more free pages, and we must remove a few lru pages to put them in free page list.
//
//     WARNING: elem MUST EXISTS IN LRU LIST
//
func (gpage_cache *Gpage_cache) discard_lru_element(elem *Page_cache_element) {

	// remove element from lru list and bucket chain

	gpage_cache.remove(PCE_LRU_LIST, elem)  // remove from lru list
	gpage_cache.remove_from_hashtable(elem) // remove also from hashtable

	// insert it in free list

	gpage_cache.insert_first(PCE_FREE_LIST, elem)
}

// discard_lru_element_if_exists removes element from lru list and puts it into free list.
//
// The element is removed from hashtable and lru list, put in free list, and elem fields are cleared.
//
// This function removes a PUBLIC page from the global read cache, in case a TRANSITORY page with same <tblid,page_no> exists in write cache.
// It is used at COMMIT time, when the TRANSITORY page is written to the table file on disk, and thus making the PUBLIC page holding old data obsolete.
//
//          The page MUST BELONG EXCLUSIVELY TO THE CURRENT SESSION that issues the COMMIT, because it should not be used (pinned) by another session.
//          The current session must be holding an exclusive lock on the table being updated.
//
func (gpage_cache *Gpage_cache) discard_lru_element_if_exists(tblid Tblid_t, page_no Page_no_t) {
	var (
		hashval             uint64
		idx                 uint64
		bucket_chain_anchor *Hlist
		elem_bpos           bpos_t
		elem                *Page_cache_element
	)

	hashval = compute_hashval_tblid_page_no(tblid, page_no)
	idx = hashval % gpage_cache.gpc_hashtable_size

	bucket_chain_anchor = &gpage_cache.gpc_hashtable[idx] // address of the bucket Hlist

	for elem_bpos = bucket_chain_anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_chain.Next {
		elem = gpage_cache.g_elem(elem_bpos)

		if elem.pce_hashval == hashval &&
			elem.pce_tblid == tblid &&
			elem.pce_page_no == page_no {
			// page is found. It must be in lru list.
			//      It cannot be in pin list, because the current session must have unpinned all its pages when this function is called. This page belongs exclusively to the current session, as the table must have an exclusive lock.
			assert(elem.pce_listinfo == PCE_LRU_LIST)
			assert(elem.pce_version == PCE_PUBLIC)
			gpage_cache.discard_lru_element(elem) // remove elem from hashtable and lru list, put it in free list, and clear elem fields

			return // ====> return
		}
	}

	// here, page has not been found in the cache.
}

// fetch_blank_PUBLIC_page_from_free_list_and_pin_it retrieves a pointer to a blank pinned PCE_PUBLIC Page_cache_element, taken out of the free list.
//   - If no element is available, this function frees a handful of the oldest elements from the lru list, and put them into the free list.
//
//  This function retrieves an element from the free list, and puts it into the pin list as PCE_PUBLIC.
//  Then, it returns this element.
//
//  The element and page must then be initialized by the caller with the proper information (tblid, page_no, etc) and data.
//
//       IMPORTANT:      the returned element and ppage are "blank" and are not in any hashtable bucket.
//
//  This function is a helper function for fetch_PUBLIC_page_and_pin_it(), which must call it with mutex gpage_cache.gpc_mutex locked.
//
func (gpage_cache *Gpage_cache) fetch_blank_PUBLIC_page_from_free_list_and_pin_it() *Page_cache_element {
	var (
		lru_list_anchor  *Dlist_anchor // anchor of the lru list
		free_list_anchor *Dlist_anchor // anchor of the free list
		elem_bpos        bpos_t
		elem             *Page_cache_element
		i                int
	)

	lru_list_anchor = &gpage_cache.gpc_lru_list
	free_list_anchor = &gpage_cache.gpc_free_list

	//=== if free list is empty, fill it with a handful of elements from lru list ===

	if free_list_anchor.Count == 0 {
		//   Free a handful (GPAGE_CACHE_NUMBER_OF_ELEMENTS_TO_FREE) of oldest elements from the lru list and put them into the free list.

		for i = 0; i < GPC_NUMBER_OF_ELEMENTS_TO_FREE; i++ { // we want to free exactly GPAGE_CACHE_NUMBER_OF_ELEMENTS_TO_FREE elements
			elem_bpos = lru_list_anchor.Prev // oldest element of lru list

			if elem_bpos == 0 { // means that all elements are in the pin list, which is impossible, as only a few pages are pinned at a one moment.
				if i == 0 { // In the really worst case with many worker processes, only several hundreds of pages may be in pin list, and the cache contains several thousands pages.
					panic("no cache element available in lru list.") // but in our test, we can have many goroutines trying to pin more pages than the cache contains
				}
				break // less than GPC_NUMBER_OF_ELEMENTS_TO_FREE elements have been freed
			}

			// remove elem from lru list and put it into free list

			elem = gpage_cache.g_elem(elem_bpos)
			gpage_cache.discard_lru_element(elem) // remove elem from hashtable and lru list, put it in free list, and clear elem fields
		}
	}

	assert(free_list_anchor.Count > 0) // here, free list should contain at least one element.

	//=== Remove one element from the free list, pin it and return it. ===

	elem = gpage_cache.pop_last(PCE_FREE_LIST) // pop an element from the free list

	gpage_cache.insert_first(PCE_PIN_LIST, elem)

	// elem is pinned, but not in the hashtable yet

	return elem
}

// used only for debugging.
// It puts all the pages of the read cache into free list.
//
//    WARNING: this function is not goroutine-safe, as it doesn't lock the cache.
//
func (gpage_cache *Gpage_cache) debug_purge_all_elements() {
	var (
		i               uint64
		elem_bpos       bpos_t
		elem            *Page_cache_element
		lru_list_anchor *Dlist_anchor
		lru_list_count  uint64
	)

	assert(gpage_cache.gpc_pin_list.Count == 0) // purge only when there is no pinned page

	if gpage_cache.gpc_lru_list.Count == 0 { // no element to free
		assert(gpage_cache.gpc_free_list.Count == GPC_TOTAL_NUMBER_OF_PAGES)
		return
	}

	// free all lru elements

	lru_list_anchor = &gpage_cache.gpc_lru_list
	lru_list_count = gpage_cache.gpc_lru_list.Count

	for i = 0; i < lru_list_count; i++ { // we want to free all elements in lru list
		elem_bpos = lru_list_anchor.Prev // oldest element of lru list

		if elem_bpos == 0 {
			panic("count mismatch : no cache element available in lru list.")
		}

		// remove elem from lru list and put it into free list

		elem = gpage_cache.g_elem(elem_bpos)
		assert(elem.pce_listinfo == PCE_LRU_LIST)

		gpage_cache.discard_lru_element(elem) // remove elem from hashtable and lru list, put it in free list, and clear elem fields
	}

	assert(gpage_cache.gpc_free_list.Count == GPC_TOTAL_NUMBER_OF_PAGES)
	assert(gpage_cache.gpc_lru_list.Count == 0)
	assert(gpage_cache.gpc_pin_list.Count == 0)
}

// fetch_PUBLIC_page_and_pin_it returns a page from the public cache.
//
//      THIS IS THE MAIN FUNCTION FOR THE GLOBAL READ CACHE.
//
//          - If page is in pin list, return it.
//          - If page is in lru list, pin it and return it.
//          - Else, load the page from disk into a free page, pin it and return it.
//
func (gpage_cache *Gpage_cache) fetch_PUBLIC_page_and_pin_it(filebag *Filebag, tabledef *rsql.Tabledef, page_no Page_no_t) *Page_cache_element {
	var (
		tblid               Tblid_t
		hashval             uint64
		idx                 uint64
		bucket_chain_anchor *Hlist
		elem_bpos           bpos_t
		elem                *Page_cache_element
		pinned_elem         *Page_cache_element
		second_try          bool
	)

	// get tblid

	tblid = Tblid_t(tabledef.Td_tblid)

	// compute hashval

	hashval = compute_hashval_tblid_page_no(tblid, page_no)

	gpage_cache.gpc_mutex.Lock() // === lock cache ===

	idx = hashval % gpage_cache.gpc_hashtable_size

	//=== check if page <tblid, page_no> already exists in cache, in pin list or lru list ===

	bucket_chain_anchor = &gpage_cache.gpc_hashtable[idx] // address of the bucket Dlist

	second_try = false

LABEL_LOOKUP_HASHTABLE:

	for elem_bpos = bucket_chain_anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_chain.Next {
		elem = gpage_cache.g_elem(elem_bpos)

		if elem.pce_hashval == hashval &&
			elem.pce_tblid == tblid &&
			elem.pce_page_no == page_no {

			switch elem.pce_listinfo {
			case PCE_LRU_LIST: // if page found is in lru list
				gpage_cache.remove(PCE_LRU_LIST, elem) // remove it from lru list

				gpage_cache.insert_first(PCE_PIN_LIST, elem) // put it in pin list

			case PCE_PIN_LIST:
				elem.pce_ref_count++ // if page was already in pin list, increment ref_count
			}

			assert(elem.pce_version == PCE_PUBLIC && elem.pce_listinfo == PCE_PIN_LIST)

			if XDEBUG_PIN {
				if elem.pce_listinfo == PCE_PIN_LIST {
					rsql.Printf("*** PIN ***    %s\n", (*Wcache)(nil).Debug_elem_string(elem))
				}
			}

			gpage_cache.gpc_mutex.Unlock() // === unlock cache ===
			return elem                    // return pointer to Page_cache_element
		}
	}

	// here, page has not been found in the cache.

	//======================== read page from disk and put in into cache ========================

	// note that the cache is unlocked, so that other worker processes can use the cache now.

	if second_try == false {
		gpage_cache.gpc_mutex.Unlock() // === unlock cache ===

		// read page from disk into private buffer of the session

		read_ppage_from_disk(filebag.private_ppage_buffer, tabledef, page_no, filebag) // note that many sessions can read the same page from disk at the same time, here, but it is a very rare occurence

		second_try = true

		gpage_cache.gpc_mutex.Lock() // === relock cache ===

		goto LABEL_LOOKUP_HASHTABLE // again, check if page <tblid, page_no> already exists in cache, in pin list or lru list, in case it has been put in cache in the meantime by another session (unlikely, but possible)
	}

	// here, page has really not been found in the cache.

	//=== copy the page we have loaded in our private memory into the cache ===

	pinned_elem = gpage_cache.fetch_blank_PUBLIC_page_from_free_list_and_pin_it() // always succeeds. Most fields of pinned_elem have been initialized. pinned_elem is in pin list.

	pinned_elem.pce_hashval = hashval
	pinned_elem.pce_tblid = tblid
	pinned_elem.pce_page_no = page_no

	assert(pinned_elem.pce_ref_count == 1 &&
		pinned_elem.pce_version == PCE_PUBLIC &&
		pinned_elem.pce_dirty == false &&
		pinned_elem.pce_listinfo == PCE_PIN_LIST)

	gpage_cache.insert_first_in_hashtable(pinned_elem) // put it in proper bucket chain

	copy(gpage_cache.g_ppage(pinned_elem).Pg_canvas[:], filebag.private_ppage_buffer.Pg_canvas[:]) // copy data from private buffer into cached page

	assert(gpage_cache.gpc_pin_list.Count+gpage_cache.gpc_lru_list.Count+gpage_cache.gpc_free_list.Count == GPC_TOTAL_NUMBER_OF_PAGES)

	if XDEBUG_PIN {
		if pinned_elem.pce_listinfo == PCE_PIN_LIST {
			rsql.Printf("*** PIN ***    %s\n", (*Wcache)(nil).Debug_elem_string(pinned_elem))
		}
	}

	gpage_cache.gpc_mutex.Unlock() // === unlock cache ===
	return pinned_elem
}

// unpin0 unpins a page from global read cache.
//
// Auxiliary function.
//
//      - if ref count > 1, page is kept in pin list
//      - if ref count == 1, page is removed from pin list and put into lru list
//
//      As you shouldn't use elem after it has been unpinned, this function always returns nil, so that you can write:
//
//                        elem = wcache.unpin0() // elem is set to nil
//
func (gpage_cache *Gpage_cache) unpin0(elem *Page_cache_element) *Page_cache_element {

	assert(elem.pce_listinfo == PCE_PIN_LIST && elem.pce_version == PCE_PUBLIC)

	// lock the cache

	gpage_cache.gpc_mutex.Lock() // === lock cache ===

	// decrement reference count

	if elem.pce_ref_count > 1 {
		elem.pce_ref_count--

		gpage_cache.gpc_mutex.Unlock()
		return nil // ----> return
	}

	// move from pin list to lru list

	if XDEBUG_NO_LRU_FOR_GCACHE {
		GPAGE_CACHE.g_ppage(elem).Clear()
	}

	gpage_cache.remove(PCE_PIN_LIST, elem)

	gpage_cache.insert_first(PCE_LRU_LIST, elem)

	if XDEBUG_NO_LRU_FOR_GCACHE {
		gpage_cache.remove(PCE_LRU_LIST, elem)
		gpage_cache.remove_from_hashtable(elem) // remove also from hashtable
		gpage_cache.insert_first(PCE_FREE_LIST, elem)
	}

	// note : elem.pce_hashval, tblid, page_no, and pce_chain are not modified.
	//        elem remains in the same bucket when it swaps between pin list and lru list.

	gpage_cache.gpc_mutex.Unlock()
	return nil
}

// read_ppage_from_disk reads a page from disk into the private ppage buffer.
//
// If page not found because file size is corrupt, or if another unexpected error occurs, log.Panicf() is called. But this should never happen.
//
// This function is the mandatory and unique path to load data from disk into the cache system.
//
func read_ppage_from_disk(ppage_buffer *Page, tabledef *rsql.Tabledef, page_no Page_no_t, filebag *Filebag) {
	var (
		err    error
		file   *os.File
		offset int64
	)

	// get file (file is opened if necessary)

	file = filebag.get_file(tabledef, FB_FILE_FOR_READ)

	// read page into buffer

	offset = int64(page_no) * PAGE_SIZE

	if _, err = file.ReadAt(ppage_buffer.Pg_canvas[:], offset); err != nil { // in godoc: "ReadAt() always returns a non-nil error when n < len(b)"
		log.Panicf("FATAL ERROR, ReadAt() for \"%s\" %d %s: %v", tabledef.Td_file_path, tabledef.Td_tblid, tabledef.Td_index_name, err)
	}
}
