package cache

import ()

//=========================================================================
//
//                  pin list, lru list and free list functions
//
//=========================================================================

// insert_first inserts element at beginning of list.
//
func (gpage_cache *Gpage_cache) insert_first(listinfo Dlist_type_t, elem *Page_cache_element) {
	var (
		anchor     *Dlist_anchor
		a_bpos     bpos_t
		a_dlist    *Dlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Dlist
		elem_bpos  bpos_t
		elem_dlist *Dlist
	)

	assert(elem.pce_listinfo == PCE_ILLEGAL_LIST)

	switch listinfo {
	case PCE_PIN_LIST:
		anchor = &gpage_cache.gpc_pin_list
		assert(elem.pce_ref_count == 0)
		elem.pce_ref_count = 1
		elem.pce_version = PCE_PUBLIC

	case PCE_LRU_LIST:
		anchor = &gpage_cache.gpc_lru_list
		assert(elem.pce_ref_count == 0)
		elem.pce_version = PCE_PUBLIC

	case PCE_FREE_LIST:
		anchor = &gpage_cache.gpc_free_list

		// nullify element fields

		elem.pce_hashval = 0
		elem.pce_tblid = 0
		elem.pce_page_no = 0
		elem.pce_version = PCE_FREED
		// assert(elem.pce_dirty == false)   // never dirty

		// assert(elem.pce_ppage_bpos == elem.pce_bpos) // in global read cache, this field never changes

		if XDEBUG {
			elem.pce_version = PCE_PUBLIC    // because gpage_cache.g_ppage(elem) below contains such assert()
			elem.pce_listinfo = PCE_PIN_LIST // same as above
			ppage := gpage_cache.g_ppage(elem)
			ppage.Clear()
			elem.pce_version = PCE_FREED         // restore original value
			elem.pce_listinfo = PCE_ILLEGAL_LIST // same as above
		}

		// assert(elem.pce_logpage_no == -1) // in global read cache, pages are never logged

		assert(elem.pce_ref_count == 0 && elem.pce_dirty == false && elem.pce_ppage_bpos == elem.pce_bpos && elem.pce_logpage_no == -1)
	}

	anchor.Count++
	a_bpos = 0
	a_dlist = &anchor.Dlist

	elem_bpos = elem.pce_bpos
	elem_dlist = &elem.pce_enlistment
	assert(elem_dlist.Prev == BPOS_ILLEGAL && elem_dlist.Next == BPOS_ILLEGAL)

	b_bpos = a_dlist.Next
	if b = gpage_cache.g_elem(b_bpos); b != nil {
		b_dlist = &b.pce_enlistment
	} else {
		b_dlist = a_dlist
	}

	elem_dlist.Prev = a_bpos
	a_dlist.Next = elem_bpos

	elem_dlist.Next = b_bpos
	b_dlist.Prev = elem_bpos

	elem.pce_listinfo = listinfo
}

// remove removes element from list.
//
func (gpage_cache *Gpage_cache) remove(listinfo Dlist_type_t, elem *Page_cache_element) {
	var (
		anchor     *Dlist_anchor
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Dlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Dlist
		elem_dlist *Dlist
	)

	switch listinfo {
	case PCE_PIN_LIST:
		anchor = &gpage_cache.gpc_pin_list
		assert(elem.pce_ref_count == 1)
		elem.pce_ref_count = 0

	case PCE_LRU_LIST:
		anchor = &gpage_cache.gpc_lru_list
		assert(elem.pce_ref_count == 0)

	case PCE_FREE_LIST:
		anchor = &gpage_cache.gpc_free_list
		assert(elem.pce_ref_count == 0)
	}

	anchor.Count--
	elem_dlist = &elem.pce_enlistment

	a_bpos = elem_dlist.Prev
	if a = gpage_cache.g_elem(a_bpos); a != nil {
		a_dlist = &a.pce_enlistment
	} else {
		a_dlist = &anchor.Dlist
	}

	b_bpos = elem_dlist.Next
	if b = gpage_cache.g_elem(b_bpos); b != nil {
		b_dlist = &b.pce_enlistment
	} else {
		b_dlist = &anchor.Dlist
	}

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	assert(elem.pce_listinfo == listinfo)
	elem.pce_listinfo = PCE_ILLEGAL_LIST
}

// pop_last removes last element from list.
// Returns nil if list is empty.
//
func (gpage_cache *Gpage_cache) pop_last(listinfo Dlist_type_t) *Page_cache_element {
	var (
		anchor     *Dlist_anchor
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Dlist
		b_bpos     bpos_t
		b_dlist    *Dlist
		elem       *Page_cache_element
		elem_bpos  bpos_t
		elem_dlist *Dlist
	)

	switch listinfo {
	case PCE_LRU_LIST:
		anchor = &gpage_cache.gpc_lru_list

	case PCE_FREE_LIST:
		anchor = &gpage_cache.gpc_free_list

	default:
		panic("impossible") // Pop_last is never used for pin list
	}

	anchor.Count--
	elem_bpos = anchor.Dlist.Prev
	if elem = gpage_cache.g_elem(elem_bpos); elem == nil {
		return nil
	}
	elem_dlist = &elem.pce_enlistment

	a_bpos = elem.pce_enlistment.Prev
	if a = gpage_cache.g_elem(a_bpos); a != nil {
		a_dlist = &a.pce_enlistment
	} else {
		a_dlist = &anchor.Dlist
	}

	b_bpos = 0
	b_dlist = &anchor.Dlist

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	assert(elem.pce_ref_count == 0)

	assert(elem.pce_listinfo == listinfo)
	elem.pce_listinfo = PCE_ILLEGAL_LIST

	return elem
}

//=========================================================================
//
//                         hashtable functions
//
//=========================================================================

func (gpage_cache *Gpage_cache) insert_first_in_hashtable(elem *Page_cache_element) {
	var (
		a_bpos     bpos_t
		a_dlist    *Hlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Hlist
		elem_bpos  bpos_t
		elem_dlist *Hlist
	)

	a_bpos = 0
	a_dlist = &gpage_cache.gpc_hashtable[elem.pce_hashval%gpage_cache.gpc_hashtable_size]

	elem_bpos = elem.pce_bpos
	elem_dlist = &elem.pce_chain
	assert(elem_dlist.Prev == BPOS_ILLEGAL && elem_dlist.Next == BPOS_ILLEGAL)

	b_bpos = a_dlist.Next
	if b = gpage_cache.g_elem(b_bpos); b != nil {
		b_dlist = &b.pce_chain
	} else {
		b_dlist = a_dlist
	}

	elem_dlist.Prev = a_bpos
	a_dlist.Next = elem_bpos

	elem_dlist.Next = b_bpos
	b_dlist.Prev = elem_bpos

	gpage_cache.gpc_hashtable_count++
}

func (gpage_cache *Gpage_cache) remove_from_hashtable(elem *Page_cache_element) {
	var (
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Hlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Hlist
		elem_dlist *Hlist
	)

	elem_dlist = &elem.pce_chain

	a_bpos = elem_dlist.Prev
	if a_bpos != 0 {
		a = gpage_cache.g_elem(a_bpos)
		a_dlist = &a.pce_chain
	} else {
		a_dlist = &gpage_cache.gpc_hashtable[elem.pce_hashval%gpage_cache.gpc_hashtable_size]
	}

	b_bpos = elem_dlist.Next
	if b_bpos != 0 {
		b = gpage_cache.g_elem(b_bpos)
		b_dlist = &b.pce_chain
	} else {
		b_dlist = a_dlist
		if a_bpos != 0 {
			b_dlist = &gpage_cache.gpc_hashtable[elem.pce_hashval%gpage_cache.gpc_hashtable_size]
		}
	}

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	gpage_cache.gpc_hashtable_count--
}

// only for debugging.
//
//    WARNING: this function is not goroutine-safe, as it doesn't lock the cache.
//
func (gpage_cache *Gpage_cache) Debug_check_hashtable() {
	var (
		n    int
		elem *Page_cache_element
	)

	for _, anchor := range gpage_cache.gpc_hashtable {
		if anchor.Next == 0 {
			assert(anchor.Prev == 0)
			continue
		}

		for bpos := anchor.Next; bpos != 0; bpos = elem.pce_chain.Next {
			elem = gpage_cache.g_elem(bpos)
			n++
		}
	}

	assert(n == gpage_cache.gpc_hashtable_count)
}

//=========================================================================
//
//                             misc functions
//
//=========================================================================

// g_elem returns element identified by elem_bpos, from global read cache.
//
// If elem_bpos == 0, returns nil.
//
func (gpage_cache *Gpage_cache) g_elem(elem_bpos bpos_t) *Page_cache_element {
	var (
		i    uint64
		j    uint64
		elem *Page_cache_element
	)

	if elem_bpos == 0 {
		return nil
	}

	elem_bpos -= BPOS_BASE

	i = uint64(elem_bpos) >> 32
	j = uint64(elem_bpos) & 0x00000000ffffffff

	elem = &gpage_cache.gpc_element_array[i][j]

	return elem
}

// g_ppage returns ppage identified by elem.pce_ppage_bpos, from global read cache.
//
// elem must have a ppage attached, that is: elem.pce_ppage_bpos != 0.
//
func (gpage_cache *Gpage_cache) g_ppage(elem *Page_cache_element) *Page {
	var (
		i          uint64
		j          uint64
		ppage_bpos bpos_t
		ppage      *Page
	)

	assert(elem.pce_version == PCE_PUBLIC && elem.pce_listinfo == PCE_PIN_LIST && elem.pce_ppage_bpos != 0)

	ppage_bpos = elem.pce_ppage_bpos - BPOS_BASE

	i = uint64(ppage_bpos) >> 32
	j = uint64(ppage_bpos) & 0x00000000ffffffff

	ppage = &gpage_cache.gpc_ppage_array[i][j]

	return ppage
}
