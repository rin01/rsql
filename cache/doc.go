/* Package cache is the implementation of the page cache of the database server.

   It is made of:
       - a public read cache, filled with shared pages, read-only, that can be accessed by multiple sessions at the same time.
       - a session private write cache, used to cache modifications of the pages during INSERT, UPDATE, DELETE, TRUCATE, etc sql statements.
             These modified pages will be flushed to log, and copied to data files, at end of transaction.

   The steps to modify a table pages are as follow:

       1) lock manager has put an exclusive lock on the table that will be modified, so that no other session can access this table.
       2) session modifies PCE_DRAFT pages in private write cache.
       3) when a statement terminates, all PCE_DRAFT pages are converted into PCE_TRANSITORY pages.
                                       Subsequent SQL statement in the same transaction will read these PCE_TRANSITORY pages, which are the modified state of the database data.
       4) when transaction terminates, all PCE_TRANSITORY pages are flushed to log, and a sync() is performed on file to make sure that all data are actually written on disk, in the log file.
                                       If a crash occurs during this copy step, the log is just disarded. The data in the table files have not been modified yet.
                                       If ROLLBACK occurs, or if an error aborts the transaction, the log is just discarded too.
       5)                              On implicit or explicit COMMIT, pages on log are copied into table files, and a sync() is performed.
                                       If a crash occurs during this copy step, the next start of the server will just redo the whole copy again.
       6) all pages in the global read cache that contains old data are discarded, so that the next read of the page will be fetched from disk, with the updated data.
       7) lock manager releases the locks.


   No error is returned by the functions
   ======================================

   To make the API simple, no error is returned by the functions.
   If the number of elements created by the write cache exceeds element_array_size_total_max_soft_limit given at wcache creation, an error is put into wcache.wpc_error.
   The same occurs if a table file becomes too large and the last available zone is allocated.

   The program continues working normally and can complete the pending operations, e.g. insertion of a record.
   But the program must call wcache.Check_error(), and if a non-nil value is returned, it should abort the transaction.
   This "lazy" error detection makes the cache API easier to use.


   API functions
   =============

   miscellaneous:
   --------------
      func New_wcache(journal *Journal, element_array_size_first_chunk uint64, element_array_size_total_max_soft_limit uint64, ppage_array_size_first_chunk uint64, ppage_array_size_total_max_limit uint64) *Wcache

      func (wcache *Wcache) Check_error() *rsql.Error

      func (wcache *Wcache) Ppage(elem *Page_cache_element) *Page                                                                                                        get ppage for read
      func (wcache *Wcache) Ppage_DRAFT_for_write(elem *Page_cache_element) *Page                                                                                        get ppage for write

      func (elem *Page_cache_element) Is_DRAFT_for_write() bool
      func (elem *Page_cache_element) Pce_page_no()        Page_no_t

      func (wcache *Wcache) Unpin(elem *Page_cache_element) *Page_cache_element

      func (wcache *Wcache) Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_info_and_pin_it(tabledef *rsql.Tabledef) *Page_cache_element
      func (wcache *Wcache) Require_DRAFT_page_info_and_pin_it(tabledef *rsql.Tabledef) *Page_cache_element                                                              returned DRAFT page is always dirty, so you can write in it, until you unpin it


   get page info, when creating a new table:
   -----------------------------------------
      func (wcache *Wcache) Get_clean_new_DRAFT_info_page(tabledef *rsql.Tabledef) (elem_page_info *Page_cache_element)                                                  returned DRAFT page is always dirty, so you can write in it, until you unpin it

      func (wcache *Wcache) Create_info_page_and_first_zone_allocator(tabledef *rsql.Tabledef)

   get a page, having its page_no:
   -------------------------------
      func (wcache *Wcache) Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef *rsql.Tabledef, page_no Page_no_t) *Page_cache_element
      func (wcache *Wcache) Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef *rsql.Tabledef, page_no Page_no_t) *Page_cache_element

      func (wcache *Wcache) Require_DRAFT_page_and_pin_it(tabledef *rsql.Tabledef, page_no Page_no_t) *Page_cache_element                                                returned DRAFT page is always dirty, so you can write in it, until you unpin it
      func (wcache *Wcache) DRAFTIFY_pinned_page(tabledef *rsql.Tabledef, elem *Page_cache_element) *Page_cache_element                                                  returned DRAFT page is always dirty, so you can write in it, until you unpin it


   get blank new page:
   -------------------
      func (wcache *Wcache) Fetch_BLANK_DRAFT_page_and_pin_it(tabledef *rsql.Tabledef, page_type Pg_type_t, hint_page_no Page_no_t) *Page_cache_element                  returned DRAFT page is always dirty, so you can write in it, until you unpin it


   discard page from cache (used by DELETE and TRUNCATE):
   ------------------------------------------------------
      func (wcache *Wcache) Delete_DRAFT_element(tabledef *rsql.Tabledef, page_no Page_no_t)
      func (wcache *Wcache) Delete_DRAFT_and_TRANSITORY_element_and_page(tabledef *rsql.Tabledef, page_no Page_no_t)


   validate changes made by a SQL statement (INSERT, UPDATE, DELETE):
   ------------------------------------------------------------------
      func (wcache *Wcache) Transform_all_DRAFT_elements_to_TRANSITORY()


   commit changes to log file, then to data files on disk:
   -------------------------------------------------------
      func (wcache *Wcache) COMMIT()


   INTERNAL USE ONLY. You should not use this function, as it is used only internally to create a zone allocator page, or the page info.
   -------------------------------------------------------------------------------------------------------------------------------------
      func (wcache *Wcache) Fetch_BLANK_DRAFT_page_with_page_no_and_pin_it(tabledef *rsql.Tabledef, page_type Pg_type_t, page_no Page_no_t) *Page_cache_element          returned DRAFT page is always dirty, so you can write in it, until you unpin it



   Here are some examples of how to use the cache API:
   ===================================================


   func example_create_table_file() {
       var (
           err       error
           file      *os.File
           page_info *cache.Page_cache_element
       )

       //=== init cache ===

       journal := cache.WORKERS_JOURNAL.Get_journal(0)
       defer cache.WORKERS_JOURNAL.Release_journal(journal) // release journal when session terminates

       wcache := cache.New_wcache(journal, 100, 10000, 10, 100)

       //=== sample Tabledef ===

       tabledef := rsql.New_Tabledef(rsql.TD_TYPE_BASE_TABLE)
       tabledef.Td_tblid = 12345
       tabledef.Td_dbid = 100
       tabledef.Td_schid = 0
       tabledef.Td_base_gtblid = 12345
       tabledef.Td_index_name = "hello"
       tabledef.Td_index_name_original = "hello"
       tabledef.Td_file_path = "data/d100/s0/t12345"

       //=== create a new table file ===

       if file, err = os.Create(tabledef.Td_file_path); err != nil {
           panic(err)
       }

       if err := file.Close(); err != nil {
           panic(err)
       }

       //=== create page_info and first zone allocator ===

       page_info = wcache.Get_clean_new_DRAFT_info_page(tabledef) // this function also created first zone allocator page, and unpinned it before returning

       page_info = wcache.Unpin(page_info)

       wcache.Transform_all_DRAFT_elements_to_TRANSITORY() // validate statement

       wcache = wcache.COMMIT() // write modifications to disk. The table file has been created.
   }


   // IMOORTANT: when new elements are created in the write cache, when a record is inserted, you must call wcache.Check_error() after each record has been inserted to check if write cache capacity is exceeded, and abort the transaction.
   //
   func example_create_pages() {
       var (
           page_draft_A *cache.Page_cache_element
           page_draft_B *cache.Page_cache_element
           page_draft_C *cache.Page_cache_element
       )

       //=== init cache ===

       wcache := cache.New_wcache(journal, 100, 10000, 10, 100)

       //=== sample Tabledef ===

       tabledef := rsql.New_Tabledef(rsql.TD_TYPE_BASE_TABLE)
       tabledef.Td_tblid = 12345
       tabledef.Td_dbid = 100
       tabledef.Td_schid = 0
       tabledef.Td_base_gtblid = 12345
       tabledef.Td_index_name = "hello"
       tabledef.Td_index_name_original = "hello"
       tabledef.Td_file_path = "data/d100/s0/t12345"

       // statement 1: create two new blank pages

       page_draft_A= wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_LEAF, 0) // create one page

       ppage_draft_A := wcache.Ppage_DRAFT_for_write(page_draft_A)

       ppage_draft_A.Pg_canvas[0] = 22 // write something in ppage

       page_draft_A = wcache.Unpin(page_draft_A)

       page_draft_B = wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_LEAF, 0) // create a second page

       ppage_draft_B := wcache.Ppage_DRAFT_for_write(page_draft_B)

       ppage_draft_B.Pg_canvas[0] = 33 // write something in ppage

       page_draft_B = wcache.Unpin(page_draft_B)

       wcache.Transform_all_DRAFT_elements_to_TRANSITORY() // validate statement (all pages used in the statement must have been unpinned)

       // statement 2: get one new blank page

       page_draft_C = wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_LEAF, 0) // create one page

       ppage_draft_C := wcache.Ppage_DRAFT_for_write(page_draft_C)

       ppage_draft_C.Pg_canvas[0] = 44 // write something in ppage

       page_draft_C = wcache.Unpin(page_draft_C)

       wcache.Transform_all_DRAFT_elements_to_TRANSITORY() // validate statement (all pages used in the statement must have been unpinned)

       //=== COMMIT ===

       wcache = wcache.COMMIT() // write modifications of all statements to disk
   }


   func example_read_pages() {
       var (
           page_no   cache.Page_no_t
           page_A    *cache.Page_cache_element
           page_B    *cache.Page_cache_element
       )

       //=== init cache ===

       wcache := cache.New_wcache(journal, 100, 10000, 10, 100)

       //=== sample Tabledef ===

       tabledef := rsql.New_Tabledef(rsql.TD_TYPE_BASE_TABLE)
       tabledef.Td_tblid = 12345
       tabledef.Td_dbid = 100
       tabledef.Td_schid = 0
       tabledef.Td_base_gtblid = 12345
       tabledef.Td_index_name = "hello"
       tabledef.Td_index_name_original = "hello"
       tabledef.Td_file_path = "data/d100/s0/t12345"

       //=== get a page already existing   page_no = 3 ===

       page_no = 3 // read page_no 3

       page_A = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, page_no)

       ppage_A := wcache.Ppage(page_A)

       fmt.Println(ppage_A.Pg_canvas[0])

       // if we want to return here, just unpin page_A, and return.

       //=== modify this page ===

       page_A = wcache.DRAFTIFY_pinned_page(tabledef, page_A)

       ppage_A = wcache.Ppage_DRAFT_for_write(page_A)

       ppage_A.Pg_canvas[0] = 34

       page_A = wcache.Unpin(page_A)

       wcache.Transform_all_DRAFT_elements_to_TRANSITORY() // validate statement (all pages used in the statement must have been unpinned)

       //=== read the modified page ===

       page_A = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, page_no)

       ppage_A = wcache.Ppage(page_A)

       fmt.Println(ppage_A.Pg_canvas[0])

       page_A = wcache.Unpin(page_A)

       //=== get a page already existing   page_no = 4   and modify this page ===

       page_no = 4 // read page_no 4

       page_B = wcache.Require_DRAFT_page_and_pin_it(tabledef, page_no)

       ppage_B := wcache.Ppage_DRAFT_for_write(page_B)

       fmt.Println(ppage_B.Pg_canvas[0])

       ppage_B.Pg_canvas[0] = 45

       page_B = wcache.Unpin(page_B)

       wcache.Transform_all_DRAFT_elements_to_TRANSITORY() // validate statement (all pages used in the statement must have been unpinned)

       //=== read the modified page ===

       page_B = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, page_no)

       ppage_B = wcache.Ppage(page_B)

       fmt.Println(ppage_B.Pg_canvas[0])

       page_B = wcache.Unpin(page_B)

       //=== COMMIT ===

       wcache = wcache.COMMIT() // write modifications of all statements to disk

       }
*/
package cache
