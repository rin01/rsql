// +build trial

package cache

const (
	XDEBUG                              bool = true
	XDEBUG_PIN                          bool = false
	XDEBUG_UNPIN_TO_SLIM_LIST           bool = true  // WHEN true, DETECTS A LOT OF BUGS, e.g. when we write in a draft page, but forget to put pce_dirty to true
	XDEBUG_NO_LRU_FOR_GCACHE            bool = false // when true, pages of global cache never go to lru list, but are cleared and go to free list
	XDEBUG_CHECK_COMMIT_LOGPAGES_UNIQUE bool = true  // for COMMIT, checks that each page_no is unique in logfile.
)

const (
	PAGE_SIZE = 16384 // TODO remettre 1024

	PZONE_BITMAP_ARRAY_SIZE       = PAGE_SIZE / 2               // TODO remettre 4                           // PAGE_SIZE / 2
	PZONE_NUMBER_OF_PAGES_IN_ZONE = PZONE_BITMAP_ARRAY_SIZE * 8 // if PAGE_SIZE is 16 KB, a single bitmap tracks 65536 pages (that is, one zone size == 1 GB)
	PZONE_ROOTBITMAP_ARRAY_SIZE   = 2048                        // TODO remettre 20                          // 2048                        // rootbitmap tracks 16384 zones
)

const (
	GPC_ELEMENT_ARRAY_NUMBER             = 8  // any number is ok, but 8 is good. If we had only one big array, the GC could not run concurrently on it. (in fact, as the elements don't contain pointers, the GC should not process them, but it is just to be sure)
	GPC_TOTAL_NUMBER_OF_PAGES_MIN uint64 = 32 // the number of pages in the cache is always a multiple of GPC_ELEMENT_ARRAY_NUMBER. Else, a few more pages will just be allocated to make it a multiple.

	GPC_NUMBER_OF_ELEMENTS_TO_FREE = 10 // number of pages that the cache will free when the cache free list is empty
)

const (
	WCACHE_ELEMENT_ARRAY_SIZE_FIRST_CHUNK                 = 1000
	WCACHE_ELEMENT_CHUNK_SIZE_MAX                         = 30 // 100000     // 6
	WCACHE_ELEMENT_ARRAY_SIZE_TOTAL_MAX_SOFT_LIMIT_LOWEST = WCACHE_ELEMENT_ARRAY_SIZE_FIRST_CHUNK

	WCACHE_PPAGE_ARRAY_SIZE_FIRST_CHUNK            = 20
	WCACHE_PPAGE_CHUNK_SIZE_MAX                    = 1000 // 10
	WCACHE_PPAGE_ARRAY_SIZE_TOTAL_MAX_LIMIT_LOWEST = WCACHE_PPAGE_ARRAY_SIZE_FIRST_CHUNK
	WCACHE_NUMBER_OF_PPAGES_TO_FREE                = 20                              // 2
	WCACHE_HANGERS_CHUNK_SIZE_MAX                  = WCACHE_PPAGE_CHUNK_SIZE_MAX / 4 // any number <= WCACHE_PPAGE_CHUNK_SIZE_MAX is good
)

const JOURNAL_ID_BASE = 1000 // suffix of first log file
const LOGFILE_FREED_PAGES_DEFAULT_CAPACITY = 4
const LOGFILE_MAX_PAGE_COUNT_BEFORE_TRUNCATION = 10000 // journal log file can grow up to this limit, before it is truncated
const LOGFILE_PAGE_COUNT_AFTER_TRUNCATION = 1000       // journal log file size after truncation triggered by LOGFILE_MAX_PAGE_COUNT_BEFORE_TRUNCATION limit

const PAGE_INFO_PAGE_NO = 1 // page_no of page info. It cannot be 0, because the first zone allocator page is at page_no==0

const TRANCOUNT_MAX = 40 // max depth of nested transaction
