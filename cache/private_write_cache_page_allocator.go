package cache

import (
	"rsql"
)

//=====================================================================================================================
//
//   Rootbitmap:
//       ppage_info.Pinf_zone_bitmaps_count() is the number of zone allocators currently existing in table file.
//       By default, rootbitmap bits are 1.
//       In an empty table, rootbitmap bit 0 is 0, meaning that zone 0 contains free pages.
//       Bit in rootbitmap is:
//            0: zone contains at least one free page.
//            1: zone contains no free page.
//
//   Zone allocator:
//       in each zone, the zone allocator page is always allocated.
//       Bit in bitmap is:
//            0: page is free
//            1: page is allocated (it is used in the btree)
//
//=====================================================================================================================

type Zone_fullness_info_t int8

const (
	ZONE_FULLNESS_ORDINARY        Zone_fullness_info_t = 1 << iota
	ZONE_FULLNESS_BECAME_FULL                          // when a page has been allocated, making the zone full
	ZONE_FULLNESS_BECAME_NON_FULL                      // when in a zone, a page has been freed from a zone that was full
)

const ZONE_CLEAR_BIT_NOT_FOUND uint64 = 0xFFFFFFFFFFFFFFFF

const FREE_PAGE_NOT_FOUND Page_no_t = 0xFFFFFFFFFFFFFFFF

const ZONE_WITH_FREE_PAGE_NOT_FOUND Zone_no_t = 0xFFFFFFFFFFFFFFFF

// return bit value (1 or 0) at requested position in bitmap byte array.
//
//     WARNING:   The caller must ensure that bit_pos does not exceed the bitmap capacity, as no check is done here.
//
func Zone_bitmap_get_bit(bitmap []uint8, bit_pos uint64) (bit_value uint8) {
	var (
		byte_index uint64
		byte_delta uint64
		bbyte      uint8
	)

	byte_index = bit_pos >> 3     // divided by 8
	byte_delta = bit_pos & 0x0007 // last 3 bits

	bbyte = bitmap[byte_index]

	bit_value = (bbyte >> byte_delta) & 0x01

	return bit_value
}

// set bit (1 or 0) at requested position in bitmap byte array.
//
//     WARNING:   The caller must ensure that bit_pos does not exceed the bitmap capacity, as no check is done here.
//
func Zone_bitmap_set_bit(bitmap []uint8, bit_pos uint64, bit_value uint8) {
	var (
		byte_index uint64
		byte_delta uint64
		p_bbyte    *uint8
		bbyte_bak  uint8
	)

	byte_index = bit_pos >> 3     // divided by 8
	byte_delta = bit_pos & 0x0007 // last 3 bits

	p_bbyte = &bitmap[byte_index]
	bbyte_bak = *p_bbyte

	if bit_value != 0 {
		*p_bbyte |= 0x01 << byte_delta // set bit
	} else {
		*p_bbyte &^= 0x01 << byte_delta // clear bit
	}

	assert(*p_bbyte != bbyte_bak) // bit that is set was unset, or bit that is unset was set
}

// find lowest clear bit in byte b, starting from start_offset_in_b position, scanning forwards.
//
// This function scans bits of a single byte.
//
//      b                      byte to scan.
//      start_delta_in_b       start position for scanning bits in byte. It must be 0 ... 7. Scanning is done from bit start_delta_in_b to bit 7.
//
// Return offset of lowest bit in byte. 0 ... 7. If no clear bit, return ZONE_CLEAR_BIT_NOT_FOUND.
//
func zone_scan_forwards_for_clear_bit_in_single_byte(b uint8, start_delta_in_b uint64) uint64 {

	assert(start_delta_in_b < 8)

	// if no clear bit in byte, return.

	if b == 0xFF {
		return ZONE_CLEAR_BIT_NOT_FOUND
	}

	// find lowest 0 bit in byte

	for i := start_delta_in_b; i < 8; i++ {
		if b&(0x0001<<i) == 0 {
			return i
		}
	}

	return ZONE_CLEAR_BIT_NOT_FOUND
}

// find highest clear bit in byte b, starting from start_offset_in_b position, scanning backwards.
//
// This function scans bits of a single byte.
//
//      b                      byte to scan.
//      start_delta_in_b       start position for scanning bits in byte. It must be 0 ... 7. Scanning is done from bit start_delta_in_b to bit 0.
//
// Return offset of highest bit in byte. 0 ... 7. If no clear bit, return ZONE_CLEAR_BIT_NOT_FOUND.
//
func zone_scan_backwards_for_clear_bit_in_single_byte(b uint8, start_delta_in_b uint64) uint64 {

	assert(start_delta_in_b < 8)

	// if no clear bit in byte, return.

	if b == 0xFF {
		return ZONE_CLEAR_BIT_NOT_FOUND
	}

	// find lowest 0 bit in byte

	for i := start_delta_in_b; i >= 0; i-- {
		if b&(0x0001<<i) == 0 {
			return i
		}
	}

	return ZONE_CLEAR_BIT_NOT_FOUND
}

// find the lowest clear bit in byte array, scanning forwards, starting at starting_bit_offset_in_bitmap (bit count).
//
//        starting_bit_offset_in_bitmap       the scan starts at this position, in bit count. It must be in the range [0 ... len(bitmap)*8-1]
//
// return position of the lowest clear bit, at or after starting_bit_offset_in_bitmap. If no clear bit, return ZONE_CLEAR_BIT_NOT_FOUND.
//
func zone_bitmap_scan_forwards_for_clear_bit(bitmap []uint8, starting_bit_offset_in_bitmap uint64) uint64 {
	var (
		byte_start_index uint64
		byte_start_delta uint64
		i                int
		clear_bit_delta  uint64
		clear_bit_offset uint64
		b                uint8
	)

	assert(starting_bit_offset_in_bitmap < uint64(len(bitmap)*8))

	byte_start_index = starting_bit_offset_in_bitmap / 8                  // byte that contains bit at starting_bit_offset_in_bitmap
	byte_start_delta = starting_bit_offset_in_bitmap - byte_start_index*8 // delta in byte for bit at starting_bit_offset_in_bitmap

	// partial search for starting byte if needed

	i = int(byte_start_index) // index of first byte to scan

	if byte_start_delta > 0 {
		b = bitmap[i]
		clear_bit_delta = zone_scan_forwards_for_clear_bit_in_single_byte(b, byte_start_delta) // try to find clear bit in first byte

		if clear_bit_delta != ZONE_CLEAR_BIT_NOT_FOUND {
			clear_bit_offset = uint64(i)*8 + clear_bit_delta

			return clear_bit_offset
		}

		i++ // no clear bit in starting byte. Go to next byte.
	}

	// search following bytes

	for ; i < len(bitmap); i++ {
		b = bitmap[i]
		if b != 0xff { // clear bit exists in byte
			clear_bit_delta = zone_scan_forwards_for_clear_bit_in_single_byte(b, 0) // scan byte from bit 0 to 7

			assert(clear_bit_delta != ZONE_CLEAR_BIT_NOT_FOUND)

			clear_bit_offset = uint64(i)*8 + clear_bit_delta

			return clear_bit_offset
		}
	}

	return ZONE_CLEAR_BIT_NOT_FOUND
}

// find the highest clear bit in byte array, scanning backwards, starting at starting_bit_offset_in_bitmap (bit count).
//
//        starting_bit_offset_in_bitmap       the scan starts at this position, in bit count. It must be in the range [0 ... len(bitmap)*8-1]
//
// return position of the highest clear bit, at or before starting_bit_offset_in_bitmap. If no clear bit, return ZONE_CLEAR_BIT_NOT_FOUND.
//
func zone_bitmap_scan_backwards_for_clear_bit(bitmap []uint8, starting_bit_offset_in_bitmap uint64) uint64 {
	var (
		byte_start_index uint64
		byte_start_delta uint64
		i                int
		clear_bit_delta  uint64
		clear_bit_offset uint64
		b                uint8
	)

	assert(starting_bit_offset_in_bitmap < uint64(len(bitmap)*8))

	byte_start_index = starting_bit_offset_in_bitmap / 8                  // byte that contains bit at starting_bit_offset_in_bitmap
	byte_start_delta = starting_bit_offset_in_bitmap - byte_start_index*8 // delta in byte for bit at starting_bit_offset_in_bitmap

	// partial search for starting byte if needed

	i = int(byte_start_index) // index of first byte to scan

	if byte_start_delta < 7 {
		b = bitmap[i]
		clear_bit_delta = zone_scan_backwards_for_clear_bit_in_single_byte(b, byte_start_delta) // try to find clear bit in first byte

		if clear_bit_delta != ZONE_CLEAR_BIT_NOT_FOUND {
			clear_bit_offset = uint64(i)*8 + clear_bit_delta

			return clear_bit_offset
		}

		i-- // no clear bit in starting byte. Go to next byte.
	}

	// search preceding bytes

	for ; i >= 0; i-- {
		b = bitmap[i]
		if b != 0xff { // clear bit exists in byte
			clear_bit_delta = zone_scan_backwards_for_clear_bit_in_single_byte(b, 7) // scan byte from bit 0 to 7

			assert(clear_bit_delta != ZONE_CLEAR_BIT_NOT_FOUND)

			clear_bit_offset = uint64(i)*8 + clear_bit_delta

			return clear_bit_offset
		}
	}

	return ZONE_CLEAR_BIT_NOT_FOUND
}

// return the free page_no which is nearest to hint_page_no forwards.
//
// hint_page_no may be outside the zone covered by page_bitmap.
//
// If no free page found, returns FREE_PAGE_NOT_FOUND.
//
func (page_allocator *Page) zone_search_for_free_page_scan_forwards(hint_page_no Page_no_t) Page_no_t {
	var (
		bitmap_array                 []uint8
		bitmap_allocation_zone_no    Zone_no_t // bitmap manages the allocation of pages in this zone. The bitmap is in a page that belongs to this zone.
		bitmap_first_page_no         Page_no_t
		bitmap_last_page_no          Page_no_t
		scan_starting_page_no        Page_no_t
		scan_starting_page_no_offset uint64
		clear_bit_offset             uint64
		free_page_no                 Page_no_t
	)

	assert(page_allocator.Pg_type() == PAGE_TYPE_ZONE_ALLOCATOR)

	bitmap_array = page_allocator.Pzone_bitmap()
	bitmap_allocation_zone_no = Zone_no_t(page_allocator.Pg_no() / PZONE_NUMBER_OF_PAGES_IN_ZONE)

	bitmap_first_page_no = Page_no_t(bitmap_allocation_zone_no * PZONE_NUMBER_OF_PAGES_IN_ZONE)
	bitmap_last_page_no = bitmap_first_page_no + PZONE_NUMBER_OF_PAGES_IN_ZONE - 1 // inclusive

	if hint_page_no > bitmap_last_page_no { // if hint_page_no is beyond the page_allocator zone, return FREE_PAGE_NOT_FOUND
		return FREE_PAGE_NOT_FOUND // ====> return
	}

	// search bitmap for nearest clear bit (== free page)

	scan_starting_page_no = hint_page_no // adjust scan_starting_page_no if hint_page_no is before the page_allocator zone
	if scan_starting_page_no < bitmap_first_page_no {
		scan_starting_page_no = bitmap_first_page_no
	}

	scan_starting_page_no_offset = uint64(scan_starting_page_no - bitmap_first_page_no) // starting bit offset in bitmap

	clear_bit_offset = zone_bitmap_scan_forwards_for_clear_bit(bitmap_array, scan_starting_page_no_offset) // may return ZONE_CLEAR_BIT_NOT_FOUND

	if clear_bit_offset == ZONE_CLEAR_BIT_NOT_FOUND {
		return FREE_PAGE_NOT_FOUND
	}

	free_page_no = bitmap_first_page_no + Page_no_t(clear_bit_offset)

	assert(free_page_no >= hint_page_no)

	return free_page_no
}

// return the free page_no which is nearest to hint_page_no backwards.
//
// hint_page_no may be outside the zone covered by page_bitmap.
//
// If no free page found, returns FREE_PAGE_NOT_FOUND.
//
func (page_allocator *Page) zone_search_for_free_page_scan_backwards(hint_page_no Page_no_t) Page_no_t {
	var (
		bitmap_array                 []uint8
		bitmap_allocation_zone_no    Zone_no_t // bitmap manages the allocation of pages in this zone. The bitmap is in a page that belongs to this zone.
		bitmap_first_page_no         Page_no_t
		bitmap_last_page_no          Page_no_t
		scan_starting_page_no        Page_no_t
		scan_starting_page_no_offset uint64
		clear_bit_offset             uint64
		free_page_no                 Page_no_t
	)

	assert(page_allocator.Pg_type() == PAGE_TYPE_ZONE_ALLOCATOR)

	bitmap_array = page_allocator.Pzone_bitmap()
	bitmap_allocation_zone_no = Zone_no_t(page_allocator.Pg_no() / PZONE_NUMBER_OF_PAGES_IN_ZONE)

	bitmap_first_page_no = Page_no_t(bitmap_allocation_zone_no * PZONE_NUMBER_OF_PAGES_IN_ZONE)
	bitmap_last_page_no = bitmap_first_page_no + PZONE_NUMBER_OF_PAGES_IN_ZONE - 1 // inclusive

	if hint_page_no < bitmap_first_page_no { // if hint_page_no is before the page_allocator zone, return FREE_PAGE_NOT_FOUND
		return FREE_PAGE_NOT_FOUND // ====> return
	}

	// search bitmap for nearest clear bit (== free page)

	scan_starting_page_no = hint_page_no // adjust scan_starting_page_no if hint_page_no is beyond the page_allocator zone
	if scan_starting_page_no > bitmap_last_page_no {
		scan_starting_page_no = bitmap_last_page_no
	}

	scan_starting_page_no_offset = uint64(scan_starting_page_no - bitmap_first_page_no) // starting bit offset in bitmap

	clear_bit_offset = zone_bitmap_scan_backwards_for_clear_bit(bitmap_array, scan_starting_page_no_offset) // may return ZONE_CLEAR_BIT_NOT_FOUND

	if clear_bit_offset == ZONE_CLEAR_BIT_NOT_FOUND {
		return FREE_PAGE_NOT_FOUND
	}

	free_page_no = bitmap_first_page_no + Page_no_t(clear_bit_offset)

	assert(free_page_no <= hint_page_no)

	return free_page_no
}

// For the bitmap of allocator page, set bit (0 to free the page, 1 to allocate the page) for the page at page_no.
//
//    return value is :
//        - if zone becomes full, return ZONE_FULLNESS_BECAME_FULL
//        - if a page is freed from a full zone, return ZONE_FULLNESS_BECAME_NON_FULL
//        - else, return ZONE_FULLNESS_ORDINARY
//
func (page_allocator *Page) zone_bitmap_set_bit_for_page_no(page_no Page_no_t, bit_value uint8) Zone_fullness_info_t {
	var (
		bitmap_array              []uint8
		bitmap_allocation_zone_no Zone_no_t // bitmap manages the allocation of pages in this zone. The bitmap is in a page that belongs to this zone.
		bitmap_first_page_no      Page_no_t
		bit_pos                   uint64
		free_page_count           uint32
		zone_fullness_info        Zone_fullness_info_t
	)

	assert(page_allocator.Pg_type() == PAGE_TYPE_ZONE_ALLOCATOR)

	bitmap_array = page_allocator.Pzone_bitmap()
	bitmap_allocation_zone_no = Zone_no_t(page_allocator.Pg_no() / PZONE_NUMBER_OF_PAGES_IN_ZONE)

	bitmap_first_page_no = Page_no_t(bitmap_allocation_zone_no * PZONE_NUMBER_OF_PAGES_IN_ZONE)

	assert(page_no >= bitmap_first_page_no)
	assert(page_no < bitmap_first_page_no+PZONE_NUMBER_OF_PAGES_IN_ZONE)

	// set bit in bitmap

	bit_pos = uint64(page_no - bitmap_first_page_no)

	Zone_bitmap_set_bit(bitmap_array, bit_pos, bit_value)

	// update pzone_bitmap_free_page_count

	zone_fullness_info = ZONE_FULLNESS_ORDINARY

	switch {
	case bit_value != 0: // allocate a page
		free_page_count = page_allocator.Pzone_free_page_count()
		assert(free_page_count > 0 && free_page_count <= PZONE_NUMBER_OF_PAGES_IN_ZONE-1) // -1 because in each zone, the zone allocator page is always allocated

		free_page_count--
		page_allocator.Set_pzone_free_page_count(free_page_count)

		if free_page_count == 0 { // if zone became full, return ZONE_HAS_BECOME_FULL
			zone_fullness_info = ZONE_FULLNESS_BECAME_FULL
		}

	default: // free a page
		free_page_count = page_allocator.Pzone_free_page_count()
		assert(free_page_count >= 0 && free_page_count < PZONE_NUMBER_OF_PAGES_IN_ZONE-1) // -1 because in each zone, the zone allocator page is always allocated

		free_page_count++
		page_allocator.Set_pzone_free_page_count(free_page_count)

		if free_page_count == 1 { // if zone became non-full, return ZONE_FULLNESS_BECAME_NON_FULL
			zone_fullness_info = ZONE_FULLNESS_BECAME_NON_FULL
		}
	}

	return zone_fullness_info
}

func (ppage_info *Page) zone_rootbitmap_get_bit_for_zone_no(zone_no Zone_no_t) (bit_value uint8) {
	var (
		rootbitmap_array      []uint8
		rootbitmap_array_size uint32
	)

	rootbitmap_array = ppage_info.Pinf_zone_rootbitmap_array()
	rootbitmap_array_size = ppage_info.Pinf_zone_rootbitmap_array_size()

	assert(uint32(zone_no) < rootbitmap_array_size*8) // else, table file is too big. If it happens, I must just increase PZONE_ROOTBITMAP_ARRAY_SIZE.

	// get bit in rootbitmap

	bit_value = Zone_bitmap_get_bit(rootbitmap_array, uint64(zone_no))

	return bit_value
}

// For the rootbitmap of page_info, set bit for the zone at zone_no.
//
func (ppage_info *Page) zone_rootbitmap_set_bit_for_zone_no(zone_no Zone_no_t, bit_value uint8) {
	var (
		rootbitmap_array      []uint8
		rootbitmap_array_size uint32
	)

	rootbitmap_array = ppage_info.Pinf_zone_rootbitmap_array()
	rootbitmap_array_size = ppage_info.Pinf_zone_rootbitmap_array_size()

	assert(uint32(zone_no) < rootbitmap_array_size*8) // else, table file is too big. If it happens, I must just increase PZONE_ROOTBITMAP_ARRAY_SIZE.

	// set bit in rootbitmap

	Zone_bitmap_set_bit(rootbitmap_array, uint64(zone_no), bit_value)
	//rsql.Printf("set rootbitmap[%d] = %d\n", zone_no, bit_value)
}

// zone_rootbitmap_scan_backwards_for_zone_with_free_page_before returns a zone BEFORE hint_zone_no, containing a free page.
// Returned value is always < hint_zone_no.
// If no zone with free page is found, returns ZONE_WITH_FREE_PAGE_NOT_FOUND.
//
func (ppage_info *Page) zone_rootbitmap_scan_backwards_for_zone_with_free_page_before(hint_zone_no Zone_no_t) Zone_no_t {
	var (
		rootbitmap_array []uint8
		clear_bit_offset uint64
	)

	if hint_zone_no == 0 {
		return ZONE_WITH_FREE_PAGE_NOT_FOUND
	}

	rootbitmap_array = ppage_info.Pinf_zone_rootbitmap_array()

	clear_bit_offset = zone_bitmap_scan_backwards_for_clear_bit(rootbitmap_array, uint64(hint_zone_no-1)) // search starting from previous zone

	if clear_bit_offset == ZONE_CLEAR_BIT_NOT_FOUND {
		return ZONE_WITH_FREE_PAGE_NOT_FOUND
	}

	return Zone_no_t(clear_bit_offset)
}

// zone_rootbitmap_scan_forwards_for_zone_with_free_page_after returns a zone AFTER hint_zone_no, containing a free page.
// Returned value is always > hint_zone_no.
// If no zone with free page is found, returns ZONE_WITH_FREE_PAGE_NOT_FOUND.
//
func (ppage_info *Page) zone_rootbitmap_scan_forwards_for_zone_with_free_page_after(hint_zone_no Zone_no_t) Zone_no_t {
	var (
		rootbitmap_array []uint8
		clear_bit_offset uint64
	)

	rootbitmap_array = ppage_info.Pinf_zone_rootbitmap_array()

	if hint_zone_no >= Zone_no_t(len(rootbitmap_array)*8-1) {
		return ZONE_WITH_FREE_PAGE_NOT_FOUND
	}

	clear_bit_offset = zone_bitmap_scan_forwards_for_clear_bit(rootbitmap_array, uint64(hint_zone_no+1)) // search starting from next zone

	if clear_bit_offset == ZONE_CLEAR_BIT_NOT_FOUND {
		return ZONE_WITH_FREE_PAGE_NOT_FOUND
	}

	return Zone_no_t(clear_bit_offset)
}

// PUBLIC API: Get_clean_new_DRAFT_info_page returns a clean info page.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Get_clean_new_DRAFT_info_page(tabledef *rsql.Tabledef) (elem_page_info *Page_cache_element) {
	var (
		rootbitmap                []uint8
		ppage_info                *Page
		collation_listinfo        []byte // list of collations, separated by semicolons, between brackets
		collation_listinfo_length int
	)

	//==== get a new page_cache_element ====

	elem_page_info = wcache.Fetch_BLANK_DRAFT_page_with_page_no_and_pin_it(tabledef, PAGE_TYPE_TABLE_INFO, PAGE_INFO_PAGE_NO)
	assert(elem_page_info.pce_dirty == true) // ensure we can write in this page

	//==== init ppage info ====

	ppage_info = wcache.Ppage_DRAFT_for_write(elem_page_info)

	ppage_info.Set_pinf_column_array_capacity(PINF_COLUMN_ARRAY_CAPACITY)
	ppage_info.Set_pinf_nk_array_capacity(PINF_NK_ARRAY_CAPACITY)
	ppage_info.Set_pinf_nk_array_coll_capacity(PINF_NK_ARRAY_COLL_CAPACITY)

	// columns info

	ppage_info.Set_pinf_column_count(uint16(len(tabledef.Td_coldefs))) // coldef count, and list of seqnos and datatypes

	for i, coldef := range tabledef.Td_coldefs {
		ppage_info.Set_pinf_column_base_seqno(i, coldef.Cd_base_seqno)
		ppage_info.Set_pinf_column_datatypes(i, coldef.Cd_datatype)

		NUMERIC_precision := uint8(0)
		if coldef.Cd_datatype == rsql.DATATYPE_NUMERIC { // we are only interested in precision for NUMERIC, so that tools directly reading table file can display NUMERIC values in suitable width
			NUMERIC_precision = uint8(coldef.Cd_precision)
		}
		ppage_info.Set_pinf_column_NUMERIC_precision(i, NUMERIC_precision)
	}

	// native key info

	ppage_info.Set_pinf_nk_count(uint16(len(tabledef.Td_nk))) // native key coldef count, and list of seqnos

	collation_listinfo = append(collation_listinfo, '[')

	for i, coldef := range tabledef.Td_nk {
		ppage_info.Set_pinf_nk_base_seqno(i, coldef.Cd_base_seqno)

		if coldef.Cd_datatype == rsql.DATATYPE_VARCHAR { // we need collations only for nk, so that tools can check if node entries and leaf tuples are properly ordered
			collation_listinfo = append(collation_listinfo, coldef.Cd_collation...)
		}
		collation_listinfo = append(collation_listinfo, ';')
	}

	collation_listinfo_length = len(collation_listinfo)

	if collation_listinfo[collation_listinfo_length-1] == ';' {
		collation_listinfo[collation_listinfo_length-1] = ']'
	}

	ppage_info.Set_pinf_nk_collations(collation_listinfo) // if not capacity enough in info page, collation_listinfo is not stored

	// other info

	if tabledef.Td_type == rsql.TD_TYPE_BASE_TABLE {
		ppage_info.Set_pinf_next_rowid(0)
		ppage_info.Set_pinf_next_identity(NEXT_IDENTITY_IS_SEED)
	}

	ppage_info.Set_pinf_root_page_no(0) // pointer to root page. 0 means that no page contains data.

	ppage_info.Set_pinf_zone_bitmap_array_size(PZONE_BITMAP_ARRAY_SIZE)

	ppage_info.Set_pinf_zone_rootbitmap_array_size(PZONE_ROOTBITMAP_ARRAY_SIZE)
	rootbitmap = ppage_info.Pinf_zone_rootbitmap_array()
	for i, _ := range rootbitmap {
		rootbitmap[i] = 0xFF
	}

	//==== get a DRAFT element from write cache, for new zone allocator page ====

	elem_page_info = wcache.allocate_BLANK_DRAFT_ZONE_ALLOCATOR_page(elem_page_info, tabledef) // a DRAFT page has been allocated for zone allocator 0, at page_no 0, initialized, and unpinned

	assert(wcache.w_ppage(elem_page_info).Pinf_zone_bitmaps_count() == 1) // first zone allocator has been created

	assert(elem_page_info.pce_dirty == true)
	return elem_page_info
}

// PUBLIC API: Create_info_page_and_first_zone_allocator creates info page and first zone allocator.
//
// It is used when a new table is created.
//
func (wcache *Wcache) Create_info_page_and_first_zone_allocator(tabledef *rsql.Tabledef) *rsql.Error {

	elem_page_info := wcache.Get_clean_new_DRAFT_info_page(tabledef)

	wcache.Unpin(elem_page_info)

	return wcache.Check_error()
}

// return page_no of the allocation page for zone_no
//
func zone_allocation_page_no(zone_no Zone_no_t) Page_no_t {
	var (
		page_no_of_allocation_page Page_no_t
	)

	page_no_of_allocation_page = Page_no_t(zone_no) * PZONE_NUMBER_OF_PAGES_IN_ZONE

	return page_no_of_allocation_page
}

// return free page_no nearest hint_page_no.
//
// This function returns the nearest free page before or at hint_page_no, backwards.
// If not found, it returns the nearest free page after hint_page_no, forwards.
// Else, if no free page is available, it returns ZONE_WITH_FREE_PAGE_NOT_FOUND.
//
// This function needs page_info for the root allocation bitmap.
// It also pins and unpins one or two zone allocation pages for zone bitmaps, looking up for the free page nearest hint_page_no.
//
// hint_page_no can be any number, even greater than largest page_no that allocation bitmaps can manage, as it is clipped by the function in this case.
//
//          This function updates the bitmap of the zone where the free page is located, and also the rootbitmap in info page if this zone gets full.
//
//          elem_page_info must be pinned.
//          It can also be modified by the function (a modified DRAFT version of the page can be returned). So, you must call it like this:
//
//               free_page_no, elem_page_info = zone_get_nearest_free_page_no(elem_page_info, tabledef, hint_page_no)
//
//          *** THE USER MUST UNPIN the received elem_page_info when he has finished using it ***
//
func (wcache *Wcache) zone_get_nearest_free_page_no(elem_page_info *Page_cache_element, tabledef *rsql.Tabledef, hint_page_no Page_no_t) (free_page_no Page_no_t, elem_page_info_modified *Page_cache_element) {
	var (
		ppage_info                *Page
		zone_no_ultimate_sentinel Zone_no_t // one past max zone_no in rootbitmap
		page_no_ultimate_sentinel Page_no_t // one past max page_no managed by last zone bitmap

		zone_rootbitmap_array_size uint64

		hint_zone_no  Zone_no_t // zone containing hint_page_no
		hint_zone_bit uint8     // bit of hint zone in rootbitmap. If 0, hint zone contains at least one free page

		zone_no          Zone_no_t
		elem_allocator_A *Page_cache_element // allocator before elem_allocator_B, containing a free page
		elem_allocator_B *Page_cache_element // allocator containing hint_page_no
		elem_allocator_C *Page_cache_element // allocator after elem_allocator_B, containing a free page
		elem_allocator   *Page_cache_element // points to elem_allocator_B, elem_allocator_A, or elem_allocator_C, which contains a free page

		free_page_no_forwards_bak Page_no_t

		zone_fullness_info Zone_fullness_info_t
	)

	// defer Unpin

	defer func() {
		if elem_allocator_A != nil {
			wcache.Unpin(elem_allocator_A)
		}

		if elem_allocator_B != nil {
			wcache.Unpin(elem_allocator_B)
		}

		if elem_allocator_C != nil {
			wcache.Unpin(elem_allocator_C)
		}
	}()

	// fetch info page from cache

	assert(elem_page_info.pce_listinfo == PCE_PIN_LIST) // elem_page_info must be pinned

	ppage_info = wcache.Ppage(elem_page_info) // info page, may be in global read cache or in write cache
	assert(ppage_info.Pg_type() == PAGE_TYPE_TABLE_INFO)
	assert(ppage_info.Pinf_zone_bitmaps_count() > 0)

	zone_rootbitmap_array_size = uint64(ppage_info.Pinf_zone_rootbitmap_array_size()) // PZONE_ROOTBITMAP_ARRAY_SIZE
	assert(ppage_info.Pinf_zone_bitmap_array_size() == PZONE_BITMAP_ARRAY_SIZE)

	zone_no_ultimate_sentinel = Zone_no_t(zone_rootbitmap_array_size * 8)                                 // one past greatest zone_no that rootbitmap may track
	page_no_ultimate_sentinel = Page_no_t(zone_rootbitmap_array_size * 8 * PZONE_NUMBER_OF_PAGES_IN_ZONE) // one past greatest page_no that the allocation bitmaps may track

	if hint_page_no >= page_no_ultimate_sentinel { // if hint_page_no too large, clip it
		hint_page_no = page_no_ultimate_sentinel - 1
	}

	hint_zone_no = Zone_no_t(uint64(hint_page_no) / PZONE_NUMBER_OF_PAGES_IN_ZONE)
	assert(hint_zone_no < zone_no_ultimate_sentinel)

	//===== search hint zone, backwards or forwards =====

	free_page_no = FREE_PAGE_NOT_FOUND
	free_page_no_forwards_bak = FREE_PAGE_NOT_FOUND

	hint_zone_bit = ppage_info.zone_rootbitmap_get_bit_for_zone_no(hint_zone_no) // check if hint zone contains at least a free page

	if hint_zone_bit == 0 { // a free page exists in hint zone, perhaps backwards or forwards
		elem_allocator_B = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, zone_allocation_page_no(hint_zone_no))
		//println("qqqqqqqqqqqq", hint_zone_no, zone_allocation_page_no(hint_zone_no))
		ppage_allocator_B := wcache.Ppage(elem_allocator_B)

		free_page_no = ppage_allocator_B.zone_search_for_free_page_scan_backwards(hint_page_no)
		free_page_no_forwards_bak = ppage_allocator_B.zone_search_for_free_page_scan_forwards(hint_page_no)

		assert(free_page_no == FREE_PAGE_NOT_FOUND || free_page_no <= hint_page_no)
		assert(free_page_no_forwards_bak == FREE_PAGE_NOT_FOUND || free_page_no_forwards_bak >= hint_page_no)
		assert(free_page_no != FREE_PAGE_NOT_FOUND || free_page_no_forwards_bak != FREE_PAGE_NOT_FOUND)

		if free_page_no != FREE_PAGE_NOT_FOUND {
			elem_allocator_B = wcache.DRAFTIFY_pinned_page(tabledef, elem_allocator_B) // because we must update allocation bitmap
			elem_allocator = elem_allocator_B
			goto LABEL_FREE_PAGE_FOUND
		}
	}

	//===== search a zone with free page, backwards =====

	zone_no = ppage_info.zone_rootbitmap_scan_backwards_for_zone_with_free_page_before(hint_zone_no) // find a zone_no < hint_zone_no, backwards, which has free page. Result may be ZONE_WITH_FREE_PAGE_NOT_FOUND

	if zone_no != ZONE_WITH_FREE_PAGE_NOT_FOUND { // search in zone_no if any
		assert(zone_no < hint_zone_no)
		elem_allocator_A = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, zone_allocation_page_no(zone_no))
		ppage_allocator_A := wcache.Ppage(elem_allocator_A)

		free_page_no = ppage_allocator_A.zone_search_for_free_page_scan_backwards(hint_page_no) // is never FREE_PAGE_NOT_FOUND
		assert(free_page_no != FREE_PAGE_NOT_FOUND)
		assert(free_page_no < hint_page_no)

		elem_allocator_A = wcache.DRAFTIFY_pinned_page(tabledef, elem_allocator_A) // because we must update allocation bitmap
		elem_allocator = elem_allocator_A
		goto LABEL_FREE_PAGE_FOUND
	}

	//===== search hint zone, forwards =====

	if free_page_no_forwards_bak != FREE_PAGE_NOT_FOUND {
		free_page_no = free_page_no_forwards_bak
		elem_allocator_B = wcache.DRAFTIFY_pinned_page(tabledef, elem_allocator_B) // because we must update allocation bitmap
		elem_allocator = elem_allocator_B
		goto LABEL_FREE_PAGE_FOUND
	}

	//===== search a zone with free page, forwards =====

	zone_no = ppage_info.zone_rootbitmap_scan_forwards_for_zone_with_free_page_after(hint_zone_no) // find a zone_no > hint_zone_no, forwards, which has free page. Result may be ZONE_WITH_FREE_PAGE_NOT_FOUND

	if zone_no != ZONE_WITH_FREE_PAGE_NOT_FOUND { // search in zone_no if any
		assert(zone_no > hint_zone_no)
		elem_allocator_C = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, zone_allocation_page_no(zone_no))
		ppage_allocator_C := wcache.Ppage(elem_allocator_C)

		free_page_no = ppage_allocator_C.zone_search_for_free_page_scan_forwards(hint_page_no) // is never FREE_PAGE_NOT_FOUND
		assert(free_page_no != FREE_PAGE_NOT_FOUND)
		assert(free_page_no > hint_page_no)

		elem_allocator_C = wcache.DRAFTIFY_pinned_page(tabledef, elem_allocator_C) // because we must update allocation bitmap
		elem_allocator = elem_allocator_C
		goto LABEL_FREE_PAGE_FOUND
	}

	//===== no free page in zone =====

	return FREE_PAGE_NOT_FOUND, elem_page_info // ===> return no free page found

	//==== update zone allocators and page_info: set bit for free_page_no in bitmap and rootbitmap ====
LABEL_FREE_PAGE_FOUND:

	zone_fullness_info = ZONE_FULLNESS_ORDINARY

	ppage_allocator := wcache.Ppage_DRAFT_for_write(elem_allocator)

	zone_fullness_info = ppage_allocator.zone_bitmap_set_bit_for_page_no(free_page_no, 1)
	assert(zone_fullness_info&(ZONE_FULLNESS_ORDINARY|ZONE_FULLNESS_BECAME_FULL) != 0)

	if zone_fullness_info == ZONE_FULLNESS_BECAME_FULL { // if zone is full, set bit to 1 for the zone in rootbitmap, in page_info
		elem_page_info = wcache.DRAFTIFY_pinned_page(tabledef, elem_page_info) // we can modify elem_page_info, because pce_dirty is set to true
		ppage_info = wcache.Ppage_DRAFT_for_write(elem_page_info)

		ppage_info.zone_rootbitmap_set_bit_for_zone_no(Zone_no_t(free_page_no/PZONE_NUMBER_OF_PAGES_IN_ZONE), 1)
	}

	return free_page_no, elem_page_info
}

// allocate_BLANK_DRAFT_ZONE_ALLOCATOR_page creates a new blank zone allocator page.
//
// So far, all zones in zone_rootbitmap are full. We must create a new free zone.
//
// The bitmap is initialized with all bits set to 0, except the bit 0 set to 1, because it is the zone allocator page position.
//
// This function reads table info page, and create a new zone allocator in the table, full of free pages.
//
//        *** YOU MUST ALWAYS UNPIN THE elem_page_info THAT IS RETURNED BY THIS FUNCTION ***
//
func (wcache *Wcache) allocate_BLANK_DRAFT_ZONE_ALLOCATOR_page(elem_page_info *Page_cache_element, tabledef *rsql.Tabledef) (elem_page_info_modified *Page_cache_element) {
	var (
		ppage_info                *Page
		zone_no_ultimate_sentinel Zone_no_t // one past max zone_no in rootbitmap

		zone_rootbitmap_array_size uint64

		zone_bitmaps_count uint64
		page_no            Page_no_t

		za_blank_allocator_draft_elem  *Page_cache_element
		za_ppage                       *Page
		za_zone_bitmap_array           []uint8
		za_zone_bitmap_free_page_count uint64
	)

	// fetch info page from cache

	assert(elem_page_info.pce_listinfo == PCE_PIN_LIST) // elem_page_info must be pinned

	elem_page_info = wcache.DRAFTIFY_pinned_page(tabledef, elem_page_info) // WE DRAFTIFY HERE, as we will modify the page with the pointers we get (zone_rootbitmap_array, etc)

	ppage_info = wcache.Ppage_DRAFT_for_write(elem_page_info) // info page

	zone_rootbitmap_array_size = uint64(ppage_info.Pinf_zone_rootbitmap_array_size()) // PZONE_ROOTBITMAP_ARRAY_SIZE
	assert(ppage_info.Pinf_zone_bitmap_array_size() == PZONE_BITMAP_ARRAY_SIZE)

	zone_no_ultimate_sentinel = Zone_no_t(zone_rootbitmap_array_size * 8) // one past greatest zone_no that rootbitmap may track

	// get zone no

	zone_bitmaps_count = uint64(ppage_info.Pinf_zone_bitmaps_count()) // number of zone allocators currently existing in table file

	if zone_bitmaps_count >= uint64(zone_no_ultimate_sentinel-1) { // when last available zone is used, we signal an error in wcache.wpc_error to warn the user
		if zone_bitmaps_count >= uint64(zone_no_ultimate_sentinel) { // no more zone is available
			panic("table file max size has been reached. The code should have checked wcache.wpc_error earlier.")
		}

		wcache.wpc_error = rsql.New_Error(rsql.ERROR_RESOURCE, rsql.ERROR_WCACHE_TABLEFILE_MAX_SIZE_REACHED, rsql.ERROR_BATCH_ABORT, zone_bitmaps_count, PZONE_NUMBER_OF_PAGES_IN_ZONE) // but we can continue, using the last zone available
	}

	// get page_no of a free page for new zone allocator

	page_no = Page_no_t(zone_bitmaps_count * PZONE_NUMBER_OF_PAGES_IN_ZONE) // zone allocator page

	//=== get a DRAFT element from write cache, for new zone allocator page ====

	za_blank_allocator_draft_elem = wcache.Fetch_BLANK_DRAFT_page_with_page_no_and_pin_it(tabledef, PAGE_TYPE_ZONE_ALLOCATOR, page_no)
	//rsql.Println("zone allocator", zone_bitmaps_count, page_no)

	// initialize bitmap

	za_ppage = wcache.Ppage_DRAFT_for_write(za_blank_allocator_draft_elem)

	za_zone_bitmap_array = za_ppage.Pzone_bitmap()
	za_zone_bitmap_array[0] = 0x01                                     // first page is used by this allocator page
	za_zone_bitmap_free_page_count = PZONE_NUMBER_OF_PAGES_IN_ZONE - 1 // 1 pages not free

	if zone_bitmaps_count == 0 { // for zone 0, which contains info_page
		assert(PAGE_INFO_PAGE_NO < 8)
		za_zone_bitmap_array[0] = 0x01 | (1 << PAGE_INFO_PAGE_NO)          // page 0 is used by this allocator page, and page 1 is for info_page
		za_zone_bitmap_free_page_count = PZONE_NUMBER_OF_PAGES_IN_ZONE - 2 // 2 pages not free
	}

	za_ppage.Set_pzone_free_page_count(uint32(za_zone_bitmap_free_page_count))

	wcache.Unpin(za_blank_allocator_draft_elem) // unpin zone allocator

	//=== in rootbitmap, set bit of the corresponding new free zone to 0 ===

	ppage_info.zone_rootbitmap_set_bit_for_zone_no(Zone_no_t(zone_bitmaps_count), 0) // a new free zone is born

	zone_bitmaps_count++
	ppage_info.Set_pinf_zone_bitmaps_count(uint32(zone_bitmaps_count))

	assert(elem_page_info.pce_version == PCE_DRAFT)
	return elem_page_info
}

// PUBLIC API: Fetch_BLANK_DRAFT_page_and_pin_it returns a pinned blank DRAFT page from write cache.
//
//      Note:  This element may already have a logpage attached, or not.
//
func (wcache *Wcache) Fetch_BLANK_DRAFT_page_and_pin_it(tabledef *rsql.Tabledef, page_type Pg_type_t, hint_page_no Page_no_t) *Page_cache_element {
	var (
		elem_page_info   *Page_cache_element
		page_no          Page_no_t
		blank_draft_elem *Page_cache_element
	)

	// get page_no of a free page

	elem_page_info = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, PAGE_INFO_PAGE_NO) // get page info

	page_no, elem_page_info = wcache.zone_get_nearest_free_page_no(elem_page_info, tabledef, hint_page_no) // lookup and updates zone allocation page and possibly page info for a free page

	if page_no == FREE_PAGE_NOT_FOUND { // no free page found. We must create a new zone allocator bitmap page.
		elem_page_info = wcache.allocate_BLANK_DRAFT_ZONE_ALLOCATOR_page(elem_page_info, tabledef) // returned elem_page_info is always a dirty draft page

		page_no, elem_page_info = wcache.zone_get_nearest_free_page_no(elem_page_info, tabledef, hint_page_no) // second try. Lookup and updates zone allocation page and possibly page info for a free page. It always succeeds.

		assert(page_no != FREE_PAGE_NOT_FOUND)
	}

	elem_page_info = wcache.Unpin(elem_page_info) // page info can be in global read cache or write cache

	// fetch free page

	blank_draft_elem = wcache.Fetch_BLANK_DRAFT_page_with_page_no_and_pin_it(tabledef, page_type, page_no) // always DRAFT and dirty

	return blank_draft_elem
}

// PUBLIC API: Delete_DRAFT_element_and_page removes PCE_DRAFT <tblid,page_no> element from write cache if it exists, and marks the page as free in zone allocator.
// It is used for sql DELETE statement, when we want to free a page.
//
//                 Note: there is no need to remove also the <tblid,page_no> from the global read cache, or TRANSITORY elements in write cache, because they become "zombie" elements,
//                       because the page_no is no more refeferenced anywhere in the other pages, making them unreachable.
//                       They are marked as "free" in the zone allocator.
//                       They will be ejected naturally from the read cache, because they are unused. Or discarded because overwritten if a new draft page with same page_no is created.
//                       TRANSITORY pages in the write cache will be written to the log file, even if they become unreachable.
//                       Deleted DRAFT pages must also be written to the log file, as page full of 0s so that there are no two pages in log file with same page_no.
//                       So, the performance hit is no big deal. The little inconvenience is just that as TRANSITORY elements are not freed, they cannot be reused by another statement in the same transaction.
//
//                       ***IMPORTANT*** As DELETE can be reading the TRANSITORY version of the pages if exists, we cannot remove them and must keep these TRANSITORY pages intact.
//
//                 The elements to remove must have been unpinned.
//
func (wcache *Wcache) Delete_DRAFT_element_and_page(tabledef *rsql.Tabledef, page_no Page_no_t) {
	var (
		tblid      Tblid_t
		elem_draft *Page_cache_element

		elem_page_info     *Page_cache_element
		ppage_info         *Page
		zone_no            Zone_no_t
		elem_allocator     *Page_cache_element
		ppage_allocator    *Page
		zone_fullness_info Zone_fullness_info_t
	)

	tblid = Tblid_t(tabledef.Td_tblid)

	// delete DRAFT page

	elem_draft = wcache.element_version_exists_in_wcache(tblid, page_no, PCE_DRAFT)

	assert(elem_draft.pce_listinfo&(PCE_LRU_LIST|PCE_SLIM_LIST) != 0) // DRAFT element must exist

	if elem_draft.pce_logpage_no != -1 { // if a page in log is reserved, discard this page in log, by writing a page with the invalid tblid==0, meaning the log page must be ignored
		wcache.erase_logpage_in_logfile(elem_draft.pce_tblid, elem_draft.pce_logpage_no)
	}

	elem_draft.pce_dirty = false             // force dirty flag to false. There is no need to flush, as the page is just discarded and the log page has been filled with 0s.
	wcache.discard_DRAFT_element(elem_draft) // release ppage, and put element into the free list. This function doesn't flush ppage and must be called with flag dirty == false.

	//===== mark page as "free" in zone allocator =====

	zone_no = Zone_no_t(uint64(page_no) / PZONE_NUMBER_OF_PAGES_IN_ZONE)

	elem_allocator = wcache.Require_DRAFT_page_and_pin_it(tabledef, zone_allocation_page_no(zone_no)) // get DRAFT zone allocator

	ppage_allocator = wcache.Ppage_DRAFT_for_write(elem_allocator)
	assert(ppage_allocator.Pg_type() == PAGE_TYPE_ZONE_ALLOCATOR)

	zone_fullness_info = ppage_allocator.zone_bitmap_set_bit_for_page_no(page_no, 0)

	elem_allocator = wcache.unpin0(elem_allocator)

	//===== change rootbitmap of ppage_info if one page has been freed from a full zone =====

	assert(zone_fullness_info&(ZONE_FULLNESS_ORDINARY|ZONE_FULLNESS_BECAME_NON_FULL) != 0)

	if zone_fullness_info == ZONE_FULLNESS_BECAME_NON_FULL { // if zone became not full, set bit to 0 for the zone in rootbitmap
		elem_page_info = wcache.Require_DRAFT_page_and_pin_it(tabledef, PAGE_INFO_PAGE_NO)

		ppage_info = wcache.Ppage_DRAFT_for_write(elem_page_info)
		ppage_info.zone_rootbitmap_set_bit_for_zone_no(Zone_no_t(page_no/PZONE_NUMBER_OF_PAGES_IN_ZONE), 0)

		assert(ppage_info.Pg_type() == PAGE_TYPE_TABLE_INFO)
		assert(ppage_info.Pinf_zone_bitmaps_count() > 0)

		wcache.unpin0(elem_page_info)
	}
}
