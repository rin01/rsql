package cache

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"

	"rsql"
)

// PUBLIC API: New_wcache initializes private write cache.
//
//     The arguments are:
//        - element_array_size_first_chunk               size of first chunk of elements that will be created
//        - element_array_size_total_max_soft_limit      max number of elements that can be created before signaling it in wcache.wpc_error. It is a soft limit and more elements are created. The program must rollback when it sees wcache.wpc_error non-nil.
//        - ppage_array_size_first_chunk                 size of first chunk of ppages that will be created
//        - ppage_array_size_total_max_limit             max number of ppages that can be created
//
func New_wcache(journal *Journal, element_array_size_first_chunk uint64, element_array_size_total_max_soft_limit uint64, ppage_array_size_first_chunk uint64, ppage_array_size_total_max_limit uint64) *Wcache {
	var (
		wcache *Wcache
	)

	//=== create new cache ===

	wcache = &Wcache{}

	//==== initialize cache parameters ====

	wcache.wpc_element_array_size_first_chunk = element_array_size_first_chunk
	wcache.wpc_element_array_size_total_max_soft_limit = element_array_size_total_max_soft_limit // max number of elements that can be created before signaling it in wcache.wpc_error (soft limit)

	wcache.wpc_hangers_array_size_first_chunk = ppage_array_size_first_chunk // there is no reason to choose a different number

	wcache.wpc_ppage_array_size_first_chunk = ppage_array_size_first_chunk
	wcache.wpc_ppage_array_size_total_max_limit = ppage_array_size_total_max_limit // max number of ppages that can be created

	//=== init wpc_same_tblid_map map ===

	wcache.wpc_same_tblid_map = make(map[Tblid_t]*Tlist_anchor)

	//==== create filebag ====

	wcache.wpc_filebag = New_Filebag()

	//==== set journal ====

	wcache.wpc_journal = journal

	wcache.wpc_flashjournal = nil

	return wcache
}

// PUBLIC API: returns an error if it has been signaled by a cache operation (too many elements have been created, of table file too large).
//
// If not nil, the program must abort the current transaction.
// The wcache cannot be used any more.
//
func (wcache *Wcache) Check_error() *rsql.Error {
	return wcache.wpc_error
}

// PUBLIC API: Unpin unpins a page.
//
//       You should never use elem after calling this function.
//       This is because an unpinned elem and its attached ppage are put in lru list and can be modified by the cache manager at any time.
//       Unpin always returns nil.
//
//                                So, you should always write:    elem = wcache.Unpin(elem) // elem is nil
//
func (wcache *Wcache) Unpin(elem *Page_cache_element) *Page_cache_element {

	assert(elem.pce_listinfo == PCE_PIN_LIST) // elem cannot be nil

	if XDEBUG_PIN {
		rsql.Printf("*** UNPIN ***  %s\n", wcache.Debug_elem_string(elem))
	}

	if elem.pce_version == PCE_PUBLIC { // if PUBLIC, move elem from pin list to lru list of global read cache
		GPAGE_CACHE.unpin0(elem)
	} else {
		wcache.unpin0(elem) // if TRANSITORY or DRAFT, move elem from pin list to lru list of write cache
	}

	return nil
}

// PUBLIC API: Ppage returns the page attached to the element.
//
//         *** The user can only call this function on a pinned element ***
//
func (wcache *Wcache) Ppage(elem *Page_cache_element) *Page {
	var (
		ppage *Page
	)

	assert(elem.pce_listinfo == PCE_PIN_LIST)

	// check if elem is in global read cache

	if elem.pce_version == PCE_PUBLIC {
		ppage = GPAGE_CACHE.g_ppage(elem)
		return ppage
	}

	// elem is in write cache

	ppage = wcache.w_ppage(elem)
	return ppage
}

// ppage_for_debug returns the page attached to the element.
//
// Exactly the same as Ppage(), but element can be in lru list.
//
//         *** The user can only call this function on element in pin list or lru list ***
//
func (wcache *Wcache) ppage_for_debug(elem *Page_cache_element) *Page {
	var (
		ppage *Page
	)

	assert(elem.pce_listinfo&(PCE_PIN_LIST|PCE_LRU_LIST) != 0) // code of our package can call Ppage_for_debug() also on element in lru list

	// check if elem is in global read cache

	if elem.pce_version == PCE_PUBLIC {
		ppage = GPAGE_CACHE.g_ppage(elem)
		return ppage
	}

	// elem is in write cache

	ppage = wcache.w_ppage(elem)
	return ppage
}

// element_version_exists_in_wcache indicates if element <tblid, page_no> with given version exists in write cache.
//
// It is used when a SQL statement terminates, and all DRAFT elements are converted to TRANSITORY. If a TRANSITORY element already exists (created by a previous SQL statement in same transaction), it must be discarded.
//
func (wcache *Wcache) element_version_exists_in_wcache(tblid Tblid_t, page_no Page_no_t, version Version_t) *Page_cache_element {
	var (
		hashval             uint64
		idx                 uint64
		bucket_chain_anchor *Hlist
		elem_bpos           bpos_t
		elem                *Page_cache_element
		elem_result         *Page_cache_element
		found               bool
	)

	// compute hashval

	hashval = compute_hashval_tblid_page_no(tblid, page_no)
	idx = hashval % wcache.wpc_hashtable_size

	// check if page <tblid, page_no> exists in write cache

	bucket_chain_anchor = &wcache.wpc_hashtable[idx] // address of the bucket Dlist

	for elem_bpos = bucket_chain_anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_chain.Next {
		elem = wcache.w_elem(elem_bpos)

		if elem.pce_hashval == hashval &&
			elem.pce_version == version &&
			elem.pce_tblid == tblid &&
			elem.pce_page_no == page_no { // page is found. We don't care in which list (pin_list, lru_list, slim_list) it is.

			if XDEBUG == false {
				return elem
			}

			// if XDEBUG, scan whole bucket chain for checking

			if found == true {
				panic("second element found in wcache with same tblid, page_no, version")
			}

			elem_result = elem
			found = true
		}
	}

	return elem_result
}

// element_exists_in_wcache indicates if element <tblid, page_no> exists in write cache.
//
// At COMMIT, we must discard all PUBLIC pages in global read cache that are overriden by a TRANSITORY page.
// As the TRANSITORY page will be written into the table storage file, the PUBLIC page will become obsolete and so must be discarded.
//
// So, we will scan all cached pages in global read cache. If this function indicates that same <tblid, page_no> page also exists as TRANSITORY page in write cache, the page in global read cache must be discarded.
//
func (wcache *Wcache) element_exists_in_wcache(tblid Tblid_t, page_no Page_no_t) bool {
	var (
		hashval             uint64
		idx                 uint64
		bucket_chain_anchor *Hlist
		elem_bpos           bpos_t
		elem                *Page_cache_element
	)

	// compute hashval

	hashval = compute_hashval_tblid_page_no(tblid, page_no)
	idx = hashval % wcache.wpc_hashtable_size

	// check if page <tblid, page_no> exists in write cache

	bucket_chain_anchor = &wcache.wpc_hashtable[idx] // address of the bucket Dlist

	for elem_bpos = bucket_chain_anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_chain.Next {
		elem = wcache.w_elem(elem_bpos)

		if elem.pce_hashval == hashval &&
			elem.pce_tblid == tblid &&
			elem.pce_page_no == page_no { // page is found. We don't care in which list it is.

			assert(elem.pce_version == PCE_TRANSITORY) // page is found. It must be TRANSITORY, because this function is called when there is no more draft pages.

			return true
		}
	}

	return false
}

// PUBLIC API: Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it returns a pinned TRANSITORY page if exists in write cache, or the PUBLIC page from global read cache.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef *rsql.Tabledef, page_no Page_no_t) *Page_cache_element {
	var (
		tblid               Tblid_t
		hashval             uint64
		idx                 uint64
		bucket_chain_anchor *Hlist
		elem_bpos           bpos_t
		elem                *Page_cache_element
		ppage_bpos          bpos_t
	)

	tblid = Tblid_t(tabledef.Td_tblid)

	//===== for permanent table, if write cache not in use, just lookup for PUBLIC page =====

	if tblid >= 0 && wcache.wpc_permanent_tables_count == 0 {
		elem = GPAGE_CACHE.fetch_PUBLIC_page_and_pin_it(wcache.wpc_filebag, tabledef, page_no)

		return elem // ====>    return pinned PUBLIC page
	}

	//===== lookup TRANSITORY page in write cache =====

	// compute hashval

	hashval = compute_hashval_tblid_page_no(tblid, page_no)
	idx = hashval % wcache.wpc_hashtable_size

	// check if page <tblid, page_no> exists in write cache

	bucket_chain_anchor = &wcache.wpc_hashtable[idx] // address of the bucket Dlist

	for elem_bpos = bucket_chain_anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_chain.Next {
		elem = wcache.w_elem(elem_bpos)

		if elem.pce_hashval == hashval &&
			elem.pce_version == PCE_TRANSITORY &&
			elem.pce_tblid == tblid &&
			elem.pce_page_no == page_no {
			break // page is found. It is in pin list or lru list or slim list.
		}
	}

	// if no TRANSITORY page found in write cache, return PUBLIC page in global read cache

	if elem_bpos == 0 {
		elem = GPAGE_CACHE.fetch_PUBLIC_page_and_pin_it(wcache.wpc_filebag, tabledef, page_no)

		return elem // ====>    return pinned PUBLIC page
	}

	// TRANSITORY page has been found in write cache

	switch elem.pce_listinfo {
	case PCE_PIN_LIST: // if page found is in pin list
		assert(elem.pce_ref_count > 0)
		elem.pce_ref_count++

	case PCE_LRU_LIST: // if page found is in lru list
		assert(elem.pce_ref_count == 0)
		wcache.remove(PCE_LRU_LIST, elem) // move it to pin list
		wcache.insert_first(PCE_PIN_LIST, elem)

	case PCE_SLIM_LIST: // if page found is in slim list
		assert(elem.pce_ref_count == 0)
		wcache.remove(PCE_SLIM_LIST, elem) // move it to pin list
		wcache.insert_first(PCE_PIN_LIST, elem)

		assert(elem.pce_ppage_bpos == 0)
		ppage_bpos = wcache.get_blank_ppage() // get a blank ppage
		elem.pce_ppage_bpos = ppage_bpos      // attach it to elem

		wcache.read_ppage_from_logfile(elem) // fill blank ppage with data from logfile

	default:
		panic("unknown page element type")
	}

	assert(elem.pce_version == PCE_TRANSITORY)
	assert(elem.pce_listinfo == PCE_PIN_LIST)

	if XDEBUG_PIN {
		if elem.pce_listinfo == PCE_PIN_LIST {
			rsql.Printf("*** PIN ***    %s\n", wcache.Debug_elem_string(elem))
		}
	}

	return elem
}

// PUBLIC API: Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it returns a pinned DRAFT page if exists in write cache, or a pinned TRANSITORY page if exists in write cache, or the pinned PUBLIC page from global read cache.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef *rsql.Tabledef, page_no Page_no_t) *Page_cache_element {
	var (
		tblid               Tblid_t
		hashval             uint64
		idx                 uint64
		bucket_chain_anchor *Hlist
		elem_bpos           bpos_t
		elem                *Page_cache_element
		elem_transitory     *Page_cache_element
		elem_draft          *Page_cache_element
		ppage_bpos          bpos_t
	)

	tblid = Tblid_t(tabledef.Td_tblid)

	//===== for permanent table, if write cache not in use, just lookup for PUBLIC page =====

	if tblid >= 0 && wcache.wpc_permanent_tables_count == 0 {
		elem = GPAGE_CACHE.fetch_PUBLIC_page_and_pin_it(wcache.wpc_filebag, tabledef, page_no)

		return elem // ====>    return pinned PUBLIC page
	}

	//===== lookup DRAFT or TRANSITORY page in write cache =====

	// compute hashval

	hashval = compute_hashval_tblid_page_no(tblid, page_no)
	idx = hashval % wcache.wpc_hashtable_size

	// check if page <tblid, page_no> exists in write cache

	elem_transitory = nil // will point to TRANSITORY page if found
	elem_draft = nil      // will point to DRAFT page if found

	bucket_chain_anchor = &wcache.wpc_hashtable[idx] // address of the bucket Dlist

	for elem_bpos = bucket_chain_anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_chain.Next {
		elem = wcache.w_elem(elem_bpos)

		if elem.pce_hashval == hashval &&
			elem.pce_tblid == tblid &&
			elem.pce_page_no == page_no { // page is found. It is in pin list or lru list or slim list.

			if elem.pce_version == PCE_DRAFT { // DRAFT page found. Break bucket lookup.
				elem_draft = elem
				break
			}

			assert(elem.pce_version == PCE_TRANSITORY) // TRANSITORY page found. Memorize it and continue bucket lookup, in case DRAFT element exists further in chain.
			assert(elem_transitory == nil)

			elem_transitory = elem
		}
	}

	// if no DRAFT or TRANSITORY page found in write cache, return PUBLIC page in global read cache

	if elem_draft == nil && elem_transitory == nil {
		elem = GPAGE_CACHE.fetch_PUBLIC_page_and_pin_it(wcache.wpc_filebag, tabledef, page_no)

		return elem // ====>    return pinned PUBLIC page
	}

	// DRAFT or TRANSITORY page has been found in write cache

	elem = elem_transitory
	if elem_draft != nil {
		elem = elem_draft
	}

	switch elem.pce_listinfo {
	case PCE_PIN_LIST: // if page found is in pin list
		assert(elem.pce_ref_count > 0)
		elem.pce_ref_count++

	case PCE_LRU_LIST: // if page found is in lru list
		assert(elem.pce_ref_count == 0)
		wcache.remove(PCE_LRU_LIST, elem) // move it to pin list
		wcache.insert_first(PCE_PIN_LIST, elem)

	case PCE_SLIM_LIST: // if page found is in slim list
		assert(elem.pce_ref_count == 0)
		wcache.remove(PCE_SLIM_LIST, elem) // move it to pin list
		wcache.insert_first(PCE_PIN_LIST, elem)

		assert(elem.pce_ppage_bpos == 0)
		ppage_bpos = wcache.get_blank_ppage() // get a blank ppage
		elem.pce_ppage_bpos = ppage_bpos      // attach it to elem

		wcache.read_ppage_from_logfile(elem) // fill blank ppage with data from logfile

	default:
		panic("unknown page element type")
	}

	assert(elem.pce_version&(PCE_DRAFT|PCE_TRANSITORY) != 0)
	assert(elem.pce_listinfo == PCE_PIN_LIST)

	if XDEBUG_PIN {
		if elem.pce_listinfo == PCE_PIN_LIST {
			rsql.Printf("*** PIN ***    %s\n", wcache.Debug_elem_string(elem))
		}
	}

	return elem
}

// PUBLIC API: Fetch_TRANSITORY_or_PUBLIC_page_info_and_pin_it returns page_info element.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Fetch_TRANSITORY_or_PUBLIC_page_info_and_pin_it(tabledef *rsql.Tabledef) *Page_cache_element {

	page_info := wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, PAGE_INFO_PAGE_NO)

	return page_info
}

// PUBLIC API: Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_info_and_pin_it returns page_info element.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_info_and_pin_it(tabledef *rsql.Tabledef) *Page_cache_element {

	page_info := wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, PAGE_INFO_PAGE_NO)

	return page_info
}

// PUBLIC API: Require_DRAFT_page_and_pin_it returns a pinned DRAFT version of the page if exists, or create a DRAFT version.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Require_DRAFT_page_and_pin_it(tabledef *rsql.Tabledef, page_no Page_no_t) *Page_cache_element {
	var (
		elem       *Page_cache_element
		elem_draft *Page_cache_element
		dest       *Page
		src        *Page
	)

	//==== get elem

	elem = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, page_no)

	assert(elem.pce_listinfo == PCE_PIN_LIST)

	if elem.pce_version == PCE_DRAFT { // if elem is already DRAFT, just return
		elem.pce_dirty = true // because when we require a DRAFT page, we certainly want to write in it
		return elem           // ====> return
	}

	//==== elem is TRANSITORY or PUBLIC, create a DRAFT copy

	assert(elem.pce_version&(PCE_TRANSITORY|PCE_PUBLIC) != 0)

	elem_draft = wcache.retrieve_new_blank_DRAFT_element_from_free_list_and_pin_it(elem.pce_tblid) // the returned elem_draft is already in pin list and draft list, with dirty flag set to true. The argument tblid is used to look for the proper journal for flashtables.

	elem_draft.pce_hashval = elem.pce_hashval // elem_draft has same hashval as elem
	elem_draft.pce_tblid = elem.pce_tblid
	elem_draft.pce_page_no = elem.pce_page_no
	assert(elem_draft.pce_ref_count == 1)
	assert(elem_draft.pce_version == PCE_DRAFT)
	assert(elem_draft.pce_dirty == true) // page is dirty, as it is a fresh copy of the TRANSITORY or PUBLIC page, and we certainly want to write in it
	assert(elem_draft.pce_listinfo == PCE_PIN_LIST)

	wcache.insert_first_in_hashtable(elem_draft) // insert elem_draft in hashtable bucket

	wcache.insert_first_in_same_tblid_maplist(elem_draft) // insert elem_draft in same_tblid_maplist

	dest = wcache.w_ppage(elem_draft)
	switch elem.pce_version {
	case PCE_PUBLIC:
		src = GPAGE_CACHE.g_ppage(elem)
	case PCE_TRANSITORY:
		src = wcache.w_ppage(elem)
	}

	copy(dest.Pg_canvas[:], src.Pg_canvas[:]) // copy ppage of TRANSITORY or PUBLIC page

	// note : pce_logpage_no may already point to a page reserved in the logfile

	if XDEBUG_PIN {
		if elem_draft.pce_listinfo == PCE_PIN_LIST {
			rsql.Printf("*** PIN ***    %s\n", wcache.Debug_elem_string(elem_draft))
		}
	}

	// TRANSITORY or PUBLIC page is no more needed. Unpin it.

	wcache.Unpin(elem)

	return elem_draft
}

// PUBLIC API: Require_DRAFT_page_info_and_pin_it returns DRAFT pinned page_info element.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Require_DRAFT_page_info_and_pin_it(tabledef *rsql.Tabledef) *Page_cache_element {

	page_info := wcache.Require_DRAFT_page_and_pin_it(tabledef, PAGE_INFO_PAGE_NO)

	return page_info
}

// PUBLIC API: DRAFTIFY_pinned_page returns a pinned DRAFT page, based on pinned elem argument.
//
//     If elem is already DRAFT, the function returns it immediately.
//
//     Else, if elem is TRANSITORY or PUBLIC :
//          - a DRAFT copy is made.
//          - then, the original TRANSITORY or PUBLIC elem is unpinned.
//          - the new DRAFT elem is returned.
//
//     You must not use the old value of the pointer elem, as this function always unpins it.
//
//     You must call this function like this :  elem = wcache.DRAFTIFY_pinned_page(elem, e)
//
//           *** THE USER MUST ALWAYS UNPIN the received element when he has finished using it                   ***
//           *** but DON'T UNPIN the original elem passed as argument, because this function ALWAYS unpins it    ***
//
func (wcache *Wcache) DRAFTIFY_pinned_page(tabledef *rsql.Tabledef, elem *Page_cache_element) *Page_cache_element {
	var (
		elem_draft *Page_cache_element
	)

	assert(elem.pce_listinfo == PCE_PIN_LIST)

	if elem.pce_version == PCE_DRAFT {
		elem.pce_dirty = true // we certainly want to write in the page
		return elem
	}

	elem_draft = wcache.Require_DRAFT_page_and_pin_it(tabledef, elem.pce_page_no)
	assert(elem_draft.pce_dirty == true) // we certainly want to write in the page

	wcache.Unpin(elem)

	return elem_draft
}

// PUBLIC API: Fetch_BLANK_DRAFT_page_with_page_no_and_pin_it returns a blank DRAFT page for a given page_no, pinned.
// Attached ppage has been filled with zeroes.
//
// We don't care if a PUBLIC or TRANSITORY version of the page already exists, as we don't care about the old page content.
// The page is not read from table file, it is just a new page in memory, initialized with requested tblid, page_no, etc. But the content is filled with zeroes.
//
// We use it when we need to write a new page at a specific page_no, e.g. when we write a new zone allocator page, or fetch a new blank draft page.
//
// Another DRAFT version of the page should not already exist in the write cache. If XDEBUG, a panic occurs in this case.
//
//           *** THE USER MUST UNPIN the received element when he has finished using it ***
//
func (wcache *Wcache) Fetch_BLANK_DRAFT_page_with_page_no_and_pin_it(tabledef *rsql.Tabledef, page_type Pg_type_t, page_no Page_no_t) *Page_cache_element {
	var (
		hashval          uint64
		tblid            Tblid_t
		blank_draft_elem *Page_cache_element
		ppage            *Page
	)

	// get tblid

	tblid = Tblid_t(tabledef.Td_tblid)

	// get a DRAFT element from write cache

	blank_draft_elem = wcache.retrieve_new_blank_DRAFT_element_from_free_list_and_pin_it(tblid) // the returned elem_draft is already in pin list and draft list, with dirty flag set to true. The argument tblid is used to look for the proper journal for flashtables.

	if XDEBUG {
		if wcache.element_version_exists_in_wcache(tblid, page_no, PCE_DRAFT) != nil { // check that a DRAFT version of the requested blank page does not already exist
			panic("another DRAFT page should not exist when you request a BLANK DRAFT page") //   Note that TRANSITORY or PUBLIC version of the page are allowed to exist, but we ignore them as we don't care about their content.
		}
	}

	// fill in DRAFT element

	hashval = compute_hashval_tblid_page_no(tblid, page_no)

	blank_draft_elem.pce_hashval = hashval
	blank_draft_elem.pce_tblid = tblid
	blank_draft_elem.pce_page_no = page_no
	assert(blank_draft_elem.pce_ref_count == 1)
	assert(blank_draft_elem.pce_version == PCE_DRAFT)
	assert(blank_draft_elem.pce_dirty == true)
	assert(blank_draft_elem.pce_listinfo == PCE_PIN_LIST)

	wcache.insert_first_in_hashtable(blank_draft_elem) // insert blank_draft_elem in hashtable bucket

	wcache.insert_first_in_same_tblid_maplist(blank_draft_elem) // insert blank_draft_elem in tblid_maplist

	// initialize ppage

	ppage = wcache.w_ppage(blank_draft_elem)

	ppage.Set_pg_type(page_type)
	ppage.Set_pg_td_type(tabledef.Td_type)
	ppage.Set_pg_dbid(Dbid_t(tabledef.Td_dbid))
	ppage.Set_pg_schid(Schid_t(tabledef.Td_schid))
	ppage.Set_pg_base_gtblid(Tblid_t(tabledef.Td_base_gtblid))
	ppage.Set_pg_tblid(Tblid_t(tabledef.Td_tblid))
	ppage.Set_pg_no(page_no)
	ppage.Set_pg_prev_no(PAGE_BOT)
	ppage.Set_pg_next_no(PAGE_EOT)
	ppage.Set_pg_bottom_target_page_no(0) // we put an invalid value here (page_no 0 is first zone allocator page, which no node entry never points to)

	if XDEBUG_PIN {
		if blank_draft_elem.pce_listinfo == PCE_PIN_LIST {
			rsql.Printf("*** PIN ***    %s\n", wcache.Debug_elem_string(blank_draft_elem))
		}
	}

	return blank_draft_elem
}

// PUBLIC API: Transform_all_DRAFT_elements_to_TRANSITORY transforms all DRAFT pages into TRANSITORY pages.
//
// If a TRANSITORY page already exists, it is discarded as it is overriden by the DRAFT version.
//
// This function is called when a SQL statement that has modified data terminates (INSERT, UPDATE, DELETE, etc).
//
//         WARNING:  Before calling this function, no page must be in pin list any more.
//
func (wcache *Wcache) Transform_all_DRAFT_elements_to_TRANSITORY() {
	var (
		elem            *Page_cache_element
		elem_transitory *Page_cache_element
	)

	assert(wcache.wpc_pin_list.Count == 0) // assert pin list is empty

	for wcache.wpc_draft_list.Count > 0 { // for all draft pages
		elem = wcache.pop_last_from_draft_list() // remove elem from draft list

		assert(elem.pce_listinfo&(PCE_LRU_LIST|PCE_SLIM_LIST) != 0) // pin list must be empty. So, elem is in lru list or slim list.
		assert(elem.pce_ref_count == 0)                             // page must not be referenced any more

		//=== lookup if TRANSITORY page already exists, and discard it

		elem_transitory = wcache.element_version_exists_in_wcache(elem.pce_tblid, elem.pce_page_no, PCE_TRANSITORY)

		if elem_transitory != nil { // if elem TRANSITORY found in lru list or slim list
			assert(elem_transitory.pce_listinfo&(PCE_LRU_LIST|PCE_SLIM_LIST) != 0)

			if elem_transitory.pce_logpage_no != -1 { // if a page in log is reserved, discard this page in log, by writing a page with the invalid tblid==0, meaning the log page must be ignored
				wcache.erase_logpage_in_logfile(elem_transitory.pce_tblid, elem_transitory.pce_logpage_no) // if tblid < 0, do nothing
			}

			elem_transitory.pce_dirty = false                  // force dirty flag to false. There is no need to flush, as the page is just discarded and overriden by the DRAFT page.
			wcache.discard_TRANSITORY_element(elem_transitory) // release ppage, and put element into the free list. This function doesn't flush ppage and must be called with flag dirty == false.
		}

		//=== convert DRAFT to TRANSITORY

		elem.pce_version = PCE_TRANSITORY // convert to TRANSITORY
		// elem.pce_dirty                                                   // page may be dirty
	}

	assert(wcache.wpc_draft_list.Count == 0) // assert draft list is empty
}

// PUBLIC API: Transform_all_DRAFT_elements_to_TRANSITORY_for_group_or_sort_table transforms DRAFT pages of table tblid into TRANSITORY pages.
//
// All elements of tblid must be unpinned, and DRAFT (no TRANSITORY element should exist for tblid).
//
// This function is used for group tables and sorting tables, created for SELECT.
//
// These tables never override existing TRANSITORY pages as they are created, filled once and then completely discarded.
//
func (wcache *Wcache) Transform_all_DRAFT_elements_to_TRANSITORY_for_group_or_sort_table(tblid Tblid_t) {
	var (
		tlist_anchor *Tlist_anchor
		elem         *Page_cache_element
	)

	tlist_anchor = wcache.wpc_same_tblid_map[tblid]
	assert(tlist_anchor != nil) // at least first allocator page and info page exist as DRAFT

	for bpos := tlist_anchor.Next; bpos != 0; bpos = elem.pce_same_tblid.Next {
		elem = wcache.w_elem(bpos)

		assert(elem.pce_version == PCE_DRAFT)

		wcache.remove_from_draft_list(elem) // remove elem from draft list

		assert(elem.pce_listinfo&(PCE_LRU_LIST|PCE_SLIM_LIST) != 0) // elem cannot be pinned. So, elem is in lru list or slim list.
		assert(elem.pce_ref_count == 0)                             // page must not be referenced any more

		elem.pce_version = PCE_TRANSITORY // convert to TRANSITORY
		// elem.pce_dirty                 // page is dirty for PCE_LRU_LIST and not dirty for PCE_SLIM_LIST
	}
}

// PUBLIC API: Discard_all_TRANSITORY_elements_for_group_or_sort_table discards all TRANSITORY pages of table tblid from wcache.
//
// All elements of tblid must be unpinned and TRANSITORY.
//
// This function is used for group tables and sorting tables, created for SELECT.
//
// There is no need to clear logpages for discarded pages, as the content of these logpages will never be read.
//
func (wcache *Wcache) Discard_all_TRANSITORY_elements_for_group_or_sort_table(tblid Tblid_t) {
	var (
		tlist_anchor *Tlist_anchor
		elem         *Page_cache_element
	)

	assert(tblid < 0)

	tlist_anchor = wcache.wpc_same_tblid_map[tblid]
	assert(tlist_anchor != nil) // first allocator page and info page always exist

	for {
		bpos := tlist_anchor.Next // wcache.discard_TRANSITORY_element(() below removes elem from tlist_anchor.
		if bpos == 0 {            // after all elements have been removed, tlist_anchor is empty, and has been deleted from wpc_same_tblid_map.
			return
		}

		elem = wcache.w_elem(bpos)

		assert(elem.pce_version == PCE_TRANSITORY)
		assert(elem.pce_listinfo&(PCE_LRU_LIST|PCE_SLIM_LIST) != 0) // elem cannot be pinned. So, elem is in lru list or slim list.
		assert(elem.pce_ref_count == 0)                             // page must not be referenced any more

		//if elem.pce_logpage_no != -1 { // no need to wipe out the logpage, as it the flashlogfile is never used by rollforward
		//	wcache.erase_logpage_in_logfile(elem.pce_tblid, elem.pce_logpage_no) // if tblid < 0, do nothing
		//}

		elem.pce_dirty = false                  // page is dirty, but we don't want to save it to table file as flashtables don't have it
		wcache.discard_TRANSITORY_element(elem) // removes elem from tlist_anchor
	}
}

// PUBLIC API: Discard_all_TRANSITORY_elements_and_pages_from_wcache deletes all elements (except zone allocator 0 and info page) of table tblid from write cache.
// All pages with page_no >= used_pages_count will be deleted.
//
// If used_pages_count == 2, zone allocator page 0 and info page 1 are not deleted.
//
// ***IMPORTANT*** IT DOES NOT MARK THE PAGE AS FREE IN ZONE ALLOCATOR. The caller must properly update the allocation bitmaps.
//
// It is used for sql TRUNCATE statement, when we want to free a page.
//
//                 Note: there is no need to remove also the page from the global read cache, because they become "zombie" elements,
//                       because the page_no is no more refeferenced anywhere in the other pages, making them unreachable.
//                       They will be ejected naturally from the read cache, because they are unused. Or discarded because overwritten if a new draft page with same page_no is committed.
//
//                 The elements to remove must have been unpinned.
//
func (wcache *Wcache) Discard_all_TRANSITORY_elements_and_pages_from_wcache(tblid Tblid_t, used_pages_count uint64) {
	var (
		tlist_anchor   *Tlist_anchor
		elem_bpos      bpos_t
		elem_bpos_next bpos_t
		elem           *Page_cache_element
		ok             bool
	)

	assert(used_pages_count >= 2) // for TRUNCATE, don't delete zone allocator page 0, and info page

	if tlist_anchor, ok = wcache.wpc_same_tblid_map[tblid]; ok == false {
		return // no page for tblid in wcache
	}

	for elem_bpos = tlist_anchor.Next; elem_bpos != 0; elem_bpos = elem_bpos_next {
		elem = wcache.w_elem(elem_bpos)
		elem_bpos_next = elem.pce_same_tblid.Next

		if elem.pce_page_no < Page_no_t(used_pages_count) {
			continue
		}

		assert(elem.pce_version == PCE_TRANSITORY)
		assert(elem.pce_listinfo&(PCE_LRU_LIST|PCE_SLIM_LIST) != 0) // elem cannot be pinned. So, elem is in lru list or slim list.
		assert(elem.pce_ref_count == 0)                             // page must not be referenced any more

		if elem.pce_logpage_no != -1 { // if a page in log is reserved, discard this page in log, by writing a page with the invalid tblid==0, meaning the log page must be ignored
			wcache.erase_logpage_in_logfile(elem.pce_tblid, elem.pce_logpage_no)
		}

		elem.pce_dirty = false
		wcache.discard_TRANSITORY_element(elem)
	}
}

func (wcache *Wcache) Remove_all_temporary_tables_elements(tablebag *rsql.Temptable_bag) {

	for tblname, gtabledef := range tablebag.Bag() {
		wcache.Discard_all_TRANSITORY_elements_for_group_or_sort_table(Tblid_t(gtabledef.Gtblid))

		for _, tabledef := range gtabledef.Indexmap {
			wcache.Discard_all_TRANSITORY_elements_for_group_or_sort_table(Tblid_t(tabledef.Td_tblid))
		}

		if XDEBUG {
			fmt.Printf("    REMOVE FROM Temptable_bag %s\n", tblname)
		}

		tablebag.Remove(tblname)
	}

	assert(wcache.wpc_temporary_tables_count == 0)
}

// PUBLIC API: COMMIT commits all changes in database.
//
// All elements in write cache are TRANSITORY, in lru list or slim list.
//
//       First, this function:
//              - discard obsolete elements in global read cache, if they also exist in lru list or slim list of write cache.
//
//       Then, commit occurs as follows:
//
//              - flush all elements in lru list to logfile (but not elements of flashtables)
//              - sync logfile
//              - ---------
//              - write LOG_STATUS_READY_FOR_TRANSFER in logpage 0
//              - sync logfile
//              - ---------
//              - for all elements of permanent tables in wcache.wpc_same_tblid_map, copy ppage into table files. PAGES FROM LOGFILE ARE ONLY READ IF NEEDED.
//              - discard these elements and put them into free list
//              - put these elements in slim list into free list
//              - sync affected data files
//              - ---------
//              - write LOG_STATUS_CLEAR in logpage 0
//              - sync logfile
//              - reset the logfile
//
func (wcache *Wcache) COMMIT() {
	var (
		bpos                                     bpos_t
		bpos_next                                bpos_t
		elem                                     *Page_cache_element
		logfile_ppage_buffer                     *Page
		number_of_modified_pages                 uint64
		number_of_modified_pages_for_flashtables uint64
		same_tblid_map_pages_count               uint64
		journal                                  *Journal
		flashtables_count                        int
	)

	assert(wcache.wpc_pin_list.Count == 0)   // assert pin list is empty
	assert(wcache.wpc_draft_list.Count == 0) // assert draft list is empty. We have only TRANSITORY pages now.
	assert(wcache.wpc_lru_list.Count+wcache.wpc_hangers_with_blank_ppage.Count == wcache.wpc_ppage_array_size_total)
	if XDEBUG {
		wcache.Debug_check_lists_count()
	}

	journal = wcache.wpc_journal

	//==== if no permanent table in write cache, just return ====

	if wcache.wpc_permanent_tables_count == 0 { // no permanent table
		assert(journal.jr_logfile_ppage_count == 1) // this counter never decreases, and no statement can remove all modified pages of a permanent table from wcache.
		assert(len(journal.jr_logfile_freed_pages) == 0)

		if XDEBUG {
			check_journal_is_STATUS_CLEAR(journal)
		}

		return //====> return
	}

	//==== discard obsolete page in global read cache ====

	// In global read cache, we only need to check the pages in lru list.
	// A page that we have modified cannot being pinned by another session in global read cache, because we have exclusive access to the table since the beginning of the batch.

	GPAGE_CACHE.gpc_mutex.Lock() // === lock cache ===

	switch {
	case wcache.wpc_lru_list.Count+wcache.wpc_slim_list.Count <= GPAGE_CACHE.gpc_lru_list.Count: //==== modified pages are less numerous than pages cached in global read cache

		for tblid, tlist_anchor := range wcache.wpc_same_tblid_map { // for each permanent table
			if tblid < 0 {
				continue
			}

			for bpos := tlist_anchor.Next; bpos != 0; bpos = elem.pce_same_tblid.Next {
				elem = wcache.w_elem(bpos)
				assert(elem.pce_version == PCE_TRANSITORY)

				GPAGE_CACHE.discard_lru_element_if_exists(elem.pce_tblid, elem.pce_page_no) // if PUBLIC page with same <tblid,page_no> exists in global read cache, discard it
			}
		}

	default: //==== modified pages are more numerous than pages cached in global read cache

		for bpos = GPAGE_CACHE.gpc_lru_list.Next; bpos != 0; bpos = bpos_next { // for all elements in lru list of global read cache
			elem = GPAGE_CACHE.g_elem(bpos)
			bpos_next = elem.pce_enlistment.Next

			if wcache.element_exists_in_wcache(elem.pce_tblid, elem.pce_page_no) == true { // if page with same <tblid,page_no> exists in write cache
				GPAGE_CACHE.discard_lru_element(elem) // remove elem from bucket chain and lru list, put it in free list, and clear elem fields
			}
		}
	}

	GPAGE_CACHE.gpc_mutex.Unlock() // === unlock cache ===

	//=== check number of modified pages ===

	number_of_modified_pages = wcache.wpc_lru_list.Count + wcache.wpc_slim_list.Count // total count of modified pages

	flashtables_count = 0
	number_of_modified_pages_for_flashtables = 0
	same_tblid_map_pages_count = 0
	for tblid, tlist_anchor := range wcache.wpc_same_tblid_map {
		if tblid < 0 {
			flashtables_count++
			number_of_modified_pages_for_flashtables += tlist_anchor.Count
		}
		same_tblid_map_pages_count += tlist_anchor.Count
	}

	assert(number_of_modified_pages == same_tblid_map_pages_count)
	assert(wcache.wpc_temporary_tables_count == flashtables_count)
	assert(wcache.wpc_permanent_tables_count+wcache.wpc_temporary_tables_count == len(wcache.wpc_same_tblid_map))

	//***************** COMMIT METHOD: elements in wcache.wpc_same_tblid_map are written into table files. If element is in slim list, ppage is read from log file ******************

	//==== flush all elements in lru list to logfile, but only for permanent tables ====

	for tblid, tlist_anchor := range wcache.wpc_same_tblid_map { // for each permanent table
		if tblid < 0 {
			continue
		}

		for bpos := tlist_anchor.Prev; bpos != 0; bpos = elem.pce_same_tblid.Prev {
			elem = wcache.w_elem(bpos)
			assert(elem.pce_version == PCE_TRANSITORY)

			if elem.pce_tblid < 0 {
				continue
			}

			if elem.pce_listinfo == PCE_LRU_LIST && elem.pce_dirty {
				wcache.flush_ppage_to_logfile(elem) // if dirty, flush ppage to logfile. dirty flag is set to false
			}
		}
	}

	if journal.jr_logfile_ppage_count > journal.jr_logfile_ppage_count_highest { // save highest value of logfile page count
		journal.jr_logfile_ppage_count_highest = journal.jr_logfile_ppage_count
	}

	//==== synchronize logfile ====

	journal.logfile_sync() // synchronize logfile

	//==== write LOG_STATUS_READY_FOR_TRANSFER in logfile and synchronize ====

	journal.logfile_status_and_page_count(LOG_STATUS_READY_FOR_TRANSFER) // write log status
	journal.logfile_sync()                                               // synchronize logfile

	if XDEBUG_CHECK_COMMIT_LOGPAGES_UNIQUE {
		_ = check_for_duplicate_pages_in_logfile(journal.jr_file, journal.jr_logfile_ppage_count) // panic if duplicate page found
	}

	//==== for all elements in wcache.wpc_same_tblid_map, copy ppage into table files ====

	logfile_ppage_buffer = wcache.wpc_filebag.private_ppage_buffer

	for tblid, tlist_anchor := range wcache.wpc_same_tblid_map { // for each permanent table
		if tblid < 0 {
			continue
		}

		for bpos := tlist_anchor.Prev; bpos != 0; bpos = elem.pce_same_tblid.Prev { // list is walked backwards, so that ppage are written more or less in increasing page_no order, as it is prettier
			elem = wcache.w_elem(bpos)

			assert(elem.pce_version == PCE_TRANSITORY)

			switch elem.pce_listinfo {
			case PCE_LRU_LIST:
				assert(elem.pce_dirty == false)
				ppage := wcache.w_ppage(elem)
				assert(elem.pce_page_no == ppage.Pg_no())
				assert(elem.pce_tblid == ppage.Pg_tblid())

				//rsql.Printf("write log page -------- %10d %10d -----lru------ %s\n", elem.pce_tblid, elem.pce_page_no, ppage.Pg_type())

				wcache.wpc_filebag.write_ppage_to_datafile(ppage)

			case PCE_SLIM_LIST:
				wcache.read_ppage_from_logfile_into_buffer(elem, logfile_ppage_buffer)
				assert(elem.pce_page_no == logfile_ppage_buffer.Pg_no())
				assert(elem.pce_tblid == logfile_ppage_buffer.Pg_tblid())

				//rsql.Printf("write log page -------- %10d %10d -----slim----- %s\n", elem.pce_tblid, elem.pce_page_no, logfile_ppage_buffer.Pg_type())

				wcache.wpc_filebag.write_ppage_to_datafile(logfile_ppage_buffer)

			default:
				panic("impossible")
			}
		}
	}

	//==== discard elements of all permanent tables ====

	for tblid, tlist_anchor := range wcache.wpc_same_tblid_map {
		if tblid < 0 {
			continue
		}

		for bpos := tlist_anchor.Next; bpos != 0; bpos = tlist_anchor.Next { // until tlist_anchor is empty
			elem = wcache.w_elem(bpos)
			assert(elem.pce_version == PCE_TRANSITORY)

			wcache.discard_TRANSITORY_element(elem)
		}
	}

	if wcache.wpc_lru_list.Count == 0 && wcache.wpc_slim_list.Count == 0 { // if wcache has no flashtables
		assert(wcache.wpc_hashtable_count == 0)
		assert(len(wcache.wpc_same_tblid_map) == 0)
	}

	assert(wcache.wpc_lru_list.Count+wcache.wpc_slim_list.Count == number_of_modified_pages_for_flashtables)
	assert(len(wcache.wpc_same_tblid_map) == wcache.wpc_temporary_tables_count)
	assert(wcache.wpc_lru_list.Count+wcache.wpc_hangers_with_blank_ppage.Count == wcache.wpc_ppage_array_size_total)
	assert(wcache.wpc_free_list.Count+number_of_modified_pages_for_flashtables == wcache.wpc_element_array_size_total)
	assert(wcache.wpc_permanent_tables_count == 0)

	assert(journal.jr_logfile_ppage_count == 1+uint64(len(journal.jr_logfile_freed_pages)))

	//==== sync all these table files and clear fbe_need_sync_flag ====

	wcache.wpc_filebag.Sync_modified_files()

	//==== write LOG_STATUS_CLEAR and page count in logfile, and synchronize ====

	journal.logfile_status_and_page_count(LOG_STATUS_CLEAR) // write log status, and reset log pages count to 1
	journal.logfile_sync()                                  // synchronize logfile

	assert(journal.jr_logfile_ppage_count == 1)
	assert(len(journal.jr_logfile_freed_pages) == 0)
}

func (wcache *Wcache) ROLLBACK() {
	var (
		elem                                     *Page_cache_element
		number_of_modified_pages                 uint64
		number_of_modified_pages_for_flashtables uint64
		same_tblid_map_pages_count               uint64
		journal                                  *Journal
		flashtables_count                        int
	)

	assert(wcache.wpc_pin_list.Count == 0)   // assert pin list is empty
	assert(wcache.wpc_draft_list.Count == 0) // assert draft list is empty. We have only TRANSITORY pages now.
	assert(wcache.wpc_lru_list.Count+wcache.wpc_hangers_with_blank_ppage.Count == wcache.wpc_ppage_array_size_total)
	if XDEBUG {
		wcache.Debug_check_lists_count()
	}

	journal = wcache.wpc_journal

	//=== check number of modified pages ===

	number_of_modified_pages = wcache.wpc_lru_list.Count + wcache.wpc_slim_list.Count // total count of modified pages

	flashtables_count = 0
	number_of_modified_pages_for_flashtables = 0
	same_tblid_map_pages_count = 0
	for tblid, tlist_anchor := range wcache.wpc_same_tblid_map {
		if tblid < 0 {
			flashtables_count++
			number_of_modified_pages_for_flashtables += tlist_anchor.Count
		}
		same_tblid_map_pages_count += tlist_anchor.Count
	}

	assert(number_of_modified_pages == same_tblid_map_pages_count)
	assert(wcache.wpc_temporary_tables_count == flashtables_count)
	assert(wcache.wpc_permanent_tables_count+wcache.wpc_temporary_tables_count == len(wcache.wpc_same_tblid_map))

	//==== discard elements of all permanent tables ====

	for tblid, tlist_anchor := range wcache.wpc_same_tblid_map {
		if tblid < 0 {
			continue
		}

		for bpos := tlist_anchor.Next; bpos != 0; bpos = tlist_anchor.Next { // until tlist_anchor is empty
			elem = wcache.w_elem(bpos)
			assert(elem.pce_version == PCE_TRANSITORY)

			elem.pce_dirty = false // force dirty flag to false
			wcache.discard_TRANSITORY_element(elem)
		}
	}

	if wcache.wpc_lru_list.Count == 0 && wcache.wpc_slim_list.Count == 0 { // if wcache has no flashtables
		assert(wcache.wpc_hashtable_count == 0)
		assert(len(wcache.wpc_same_tblid_map) == 0)
	}

	assert(wcache.wpc_lru_list.Count+wcache.wpc_slim_list.Count == number_of_modified_pages_for_flashtables)
	assert(len(wcache.wpc_same_tblid_map) == wcache.wpc_temporary_tables_count)
	assert(wcache.wpc_lru_list.Count+wcache.wpc_hangers_with_blank_ppage.Count == wcache.wpc_ppage_array_size_total)
	assert(wcache.wpc_free_list.Count+number_of_modified_pages_for_flashtables == wcache.wpc_element_array_size_total)
	assert(wcache.wpc_permanent_tables_count == 0)

	assert(journal.jr_logfile_ppage_count == 1+uint64(len(journal.jr_logfile_freed_pages)))

	if journal.jr_logfile_ppage_count > journal.jr_logfile_ppage_count_highest { // save highest value of logfile page count
		journal.jr_logfile_ppage_count_highest = journal.jr_logfile_ppage_count
	}

	//==== check fbe_need_sync_flag == false for all these table files ====

	wcache.wpc_filebag.check_sync_flag_is_false_for_ordinary_table_files()

	if XDEBUG {
		wcache.Debug_check_lists_count()
		check_journal_is_STATUS_CLEAR(journal)
	}

	journal.jr_logfile_ppage_count = 1 // reset logfile
	journal.jr_logfile_freed_pages = journal.jr_logfile_freed_pages[:0]
}

// rollforward_logfile_at_startup copies the pages from log file to data files.
// If log file status is LOG_STATUS_CLEAR, this function just returns.
//
func rollforward_logfile_at_startup(journal_id uint64) {
	var (
		err                  error
		journal_path         string
		logfile              *os.File
		status               Log_status_t
		logfile_sector       Logfile_sector
		logfile_ppage_buffer *Page
		logfile_ppage_count  uint64
		log_reader           *bufio.Reader
		n                    int
		filebag              *Filebag
	)

	logfile_ppage_buffer = &Page{}

	filebag = New_Filebag()

	//==== open logfile ====

	journal_path = rsql.Journal_path(rsql.DIRECTORY_JOURNALS, int64(journal_id))

	if logfile, err = os.OpenFile(journal_path, os.O_RDWR, rsql.JOURNALFILE_PERM); err != nil { // must open logfile
		log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
	}
	defer logfile.Close()

	//==== read logfile sector 0 ====

	if err := logfile_sector.Load(logfile); err != nil {
		log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
	}

	status = logfile_sector.Plog_status()
	logfile_ppage_count = logfile_sector.Plog_count()

	if status != LOG_STATUS_READY_FOR_TRANSFER { // for this logfile, no rollforward is needed
		if status != LOG_STATUS_CLEAR {
			log.Fatalf("FATAL ERROR, log file %s is corrupted. Status is %d, instead of LOG_STATUS_CLEAR.", journal_path, status)
		}

		if logfile_ppage_count != 1 {
			log.Fatalf("FATAL ERROR, log file %s is corrupted. Active log page count is %d, instead of 1.", journal_path, logfile_ppage_count)
		}

		assert(logfile_ppage_count == 1)

		if err := logfile.Truncate(PAGE_SIZE); err != nil { // truncate logfile, so that it contains only the status page
			log.Panicf("FATAL ERROR, log file \"%s\" truncate failed: %v", journal_path, err)
		}

		return //====> no rollforward is needed, just return
	}

	log.Printf("recovering from log file %s (length: %d pages)", journal_path, logfile_ppage_count)

	//==== check for duplicate pages in logfile ===

	_ = check_for_duplicate_pages_in_logfile(logfile, logfile_ppage_count) // panic if duplicate page found

	//==== copy logfile to multiple table files ====

	var rollforward_page_count int

	if _, err := logfile.Seek(PAGE_SIZE, 0); err != nil {
		log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
	}

	log_reader = bufio.NewReader(logfile)

	for i := uint64(1); i < logfile_ppage_count; i++ {
		if n, err = io.ReadFull(log_reader, logfile_ppage_buffer.Pg_canvas[:]); err != nil {
			log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
		}
		assert(n == PAGE_SIZE)

		if logfile_ppage_buffer.Pg_tblid() == 0 { // in logfile, pages with tblid==0 must be ignored
			continue
		}

		//rsql.Printf("roll forward: write log page --- %10d %10d --- %s\n", logfile_ppage_buffer.Pg_tblid(), logfile_ppage_buffer.Pg_no(), logfile_ppage_buffer.Pg_type())

		filebag.write_ppage_to_datafile(logfile_ppage_buffer)
		rollforward_page_count++
	}

	//==== sync all these table files and clear fbe_need_sync_flag, and close the files ====

	filebag.Sync_modified_files() // sync table files

	filebag.Close_all() // close table files

	//==== write LOG_STATUS_CLEAR in logfile and synchronize ====

	logfile_sector.Set_plog_status(LOG_STATUS_CLEAR)
	logfile_sector.Set_plog_count(1)
	logfile_sector.Set_plog_count_bak(logfile_ppage_count)

	if err = logfile_sector.Save(logfile); err != nil {
		log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
	}

	if err = logfile.Sync(); err != nil { // because logfile.Close() doesn't guarantee fsync(). See man page "man close"
		log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
	}

	if err := logfile.Truncate(PAGE_SIZE); err != nil { // truncate logfile, so that it contains only the status page
		log.Panicf("FATAL ERROR, log file \"%s\" truncate failed: %v", journal_path, err)
	}

	if err = logfile.Sync(); err != nil { // because logfile.Close() doesn't guarantee fsync(). See man page "man close"
		log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
	}

	log.Printf("recovered  from log file %s: %d active pages (success)", journal_path, rollforward_page_count)
}

func check_for_duplicate_pages_in_logfile(logfile *os.File, logfile_ppage_count uint64) (rollforward_page_count int) {
	var (
		err                  error
		log_reader           *bufio.Reader
		n                    int
		logfile_ppage_buffer *Page
	)

	type page_entry_t struct {
		Tblid_t
		Page_no_t
	}

	var debug_map = make(map[page_entry_t]struct{})

	logfile_ppage_buffer = &Page{}

	// go to first logpage

	if _, err := logfile.Seek(PAGE_SIZE, 0); err != nil {
		log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
	}

	// check each page in logfile

	log_reader = bufio.NewReader(logfile)

	for i := uint64(1); i < logfile_ppage_count; i++ {
		if n, err = io.ReadFull(log_reader, logfile_ppage_buffer.Pg_canvas[:]); err != nil {
			log.Panicf("FATAL ERROR, log file rollforward failed: %v", err)
		}
		assert(n == PAGE_SIZE)

		if logfile_ppage_buffer.Pg_tblid() == 0 { // in logfile, pages with tblid==0 must be ignored
			continue
		}

		// check that logpage <tblid, page_no> is unique

		entry := page_entry_t{logfile_ppage_buffer.Pg_tblid(), logfile_ppage_buffer.Pg_no()}

		if _, ok := debug_map[entry]; ok == true { // check if already exists in map
			panic(fmt.Sprintf("duplicate tblid/page_no %d:%d found in logfile", logfile_ppage_buffer.Pg_tblid(), logfile_ppage_buffer.Pg_no()))
		}

		debug_map[entry] = struct{}{} // put in map

		//rsql.Printf("roll forward: write log page --- %10d %10d --- %s\n", logfile_ppage_buffer.Pg_tblid(), logfile_ppage_buffer.Pg_no(), logfile_ppage_buffer.Pg_type())

		rollforward_page_count++
	}

	assert(len(debug_map) == rollforward_page_count)

	return rollforward_page_count
}

func (wcache *Wcache) Create_flashtables(tablebag *rsql.Temptable_bag) *rsql.Error {
	var flashtable_count int

	for _, gtabledef := range tablebag.Bag() {
		if rsql_err := wcache.Create_info_page_and_first_zone_allocator(gtabledef.Base); rsql_err != nil {
			return rsql_err
		}

		flashtable_count++

		for _, indexdef := range gtabledef.Indexmap {
			if rsql_err := wcache.Create_info_page_and_first_zone_allocator(indexdef); rsql_err != nil {
				return rsql_err
			}

			flashtable_count++
		}
	}

	// transform DRAFT elements to TRANSITORY

	//assert(wcache.wpc_permanent_tables_count == 0) // this transformation must be performed before the batch is run. Uncommented because it may happen if batch starts with a pending transaction.
	assert(wcache.wpc_temporary_tables_count == flashtable_count)

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	return nil
}
