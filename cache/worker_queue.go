package cache

import (
	"container/heap"
	"fmt"
	"sort"
	"sync"
	"time"

	"rsql"
)

var G_WORKER_QUEUE Worker_queue

type Worker struct {
	sync.Mutex
	Wk_id uint32

	Wk_remote_address string    // just for info
	Wk_start_time     time.Time // just for info
	Wk_login_id       int64     // just for info
	Wk_login_name     string    // just for info
}

type Worker_queue struct {
	mu          sync.Mutex
	not_empty   sync.Cond
	worker_heap wk_heap // we want workers with smaller id to be taken first, just because it is nicer

	workers_in_use map[uint32]*Worker
}

func (wqueue *Worker_queue) Initialize(workers_number_max int) {
	rsql.Assert(wqueue.not_empty.L == nil)

	wqueue.not_empty.L = &wqueue.mu

	for i := 0; i < workers_number_max; i++ {
		wqueue.worker_heap = append(wqueue.worker_heap, &Worker{Wk_id: uint32(i)})
	}

	heap.Init(&wqueue.worker_heap)

	wqueue.workers_in_use = make(map[uint32]*Worker, 64) // for the map initial capacity, any reasonable number is good
}

func (wqueue *Worker_queue) Push(x *Worker) {
	wqueue.mu.Lock()

	// clean worker

	x.Wk_remote_address = "<unknown>"
	x.Wk_start_time = time.Time{}
	x.Wk_login_id = -1
	x.Wk_login_name = "<unknown>"

	// move worker in proper lists

	if len(wqueue.worker_heap) == 0 {
		wqueue.not_empty.Broadcast()
	}

	heap.Push(&wqueue.worker_heap, x)

	delete(wqueue.workers_in_use, x.Wk_id)

	wqueue.mu.Unlock()
}

func (wqueue *Worker_queue) Pop() *Worker {
	wqueue.mu.Lock()

	for len(wqueue.worker_heap) == 0 {
		wqueue.not_empty.Wait()
	}

	worker := heap.Pop(&wqueue.worker_heap).(*Worker)

	wqueue.workers_in_use[worker.Wk_id] = worker

	wqueue.mu.Unlock()
	return worker
}

func (wqueue *Worker_queue) List_workers() []string {
	wqueue.mu.Lock()

	lst := make([]*Worker, 0, len(wqueue.workers_in_use))

	for _, worker := range wqueue.workers_in_use {
		lst = append(lst, worker)
	}

	sort.Sort(ByWorkerId(lst))

	ss := make([]string, 0, len(lst))

	for _, worker := range lst {
		worker.Lock()
		ss = append(ss, fmt.Sprintf("%4d  %30s  %-26s  %10d  %-s", worker.Wk_id, worker.Wk_remote_address, worker.Wk_start_time.Format(time.RFC3339), worker.Wk_login_id, worker.Wk_login_name))
		worker.Unlock()
	}

	wqueue.mu.Unlock()
	return ss
}

func (wqueue *Worker_queue) Workers_in_use() int {
	wqueue.mu.Lock()

	count := len(wqueue.workers_in_use)

	wqueue.mu.Unlock()

	return count
}

//--- Sort interface ---

type ByWorkerId []*Worker

func (a ByWorkerId) Len() int           { return len(a) }
func (a ByWorkerId) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByWorkerId) Less(i, j int) bool { return a[i].Wk_id < a[j].Wk_id }

//--- implementation of heap.Interface ---

type wk_heap []*Worker // available workers are stored here, in heap order

func (wh wk_heap) Len() int { return len(wh) }

func (wh wk_heap) Less(i, j int) bool {
	return wh[i].Wk_id < wh[j].Wk_id
}

func (wh wk_heap) Swap(i, j int) {
	wh[i], wh[j] = wh[j], wh[i]
}

func (wh *wk_heap) Push(x interface{}) {
	*wh = append(*wh, x.(*Worker))
}

func (wh *wk_heap) Pop() interface{} {
	n := len(*wh)
	worker := (*wh)[n-1]
	*wh = (*wh)[:n-1]
	return worker
}
