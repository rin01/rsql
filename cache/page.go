package cache

import (
	"fmt"
	"io"
	"log"
	"os"
	"strconv"

	"rsql"
)

// static page full of zero. It is used when we need to write a page full of zero into a file.
var G_PPAGE_CLEAR = [PAGE_SIZE]byte{}

// if you change any of these definitions, you must also change PAGE_CANVAS_LENGTH and PG_OFFSET_xxx constants, and also getter and setter code, below in this page.
type (
	Pg_type_t uint16
	Dbid_t    int64
	Schid_t   int64
	Tblid_t   int64
	Page_no_t uint64
	Zone_no_t uint64
)

// power of two
const (
	PAGE_TYPE_LEAF           Pg_type_t = 1
	PAGE_TYPE_NODE           Pg_type_t = 2
	PAGE_TYPE_ZONE_ALLOCATOR Pg_type_t = 4 // an allocator page manages allocation of pages in its zone, using a bitmap. One allocator page is needed for each zone. The first allocator page is at Pg_no == 0.
	PAGE_TYPE_TABLE_INFO     Pg_type_t = 8 // a table file contains exactly one info page, Pg_no == 1, containing critical info about the table, such as root page's Pg_no, and a bitmap showing zone allocator pages still containing free pages.
)

func (t Pg_type_t) String() string {
	var (
		s string
	)

	switch t {
	case PAGE_TYPE_LEAF:
		s = "PAGE_TYPE_LEAF"
	case PAGE_TYPE_NODE:
		s = "PAGE_TYPE_NODE"
	case PAGE_TYPE_ZONE_ALLOCATOR:
		s = "PAGE_TYPE_ZONE_ALLOCATOR"
	case PAGE_TYPE_TABLE_INFO:
		s = "PAGE_TYPE_TABLE_INFO"
	default:
		s = fmt.Sprintf("unknown %d", t)
	}

	return s
}

const (
	PAGE_BOT Page_no_t = 0xfffffffffffffffe //  beginning of table
	PAGE_EOT Page_no_t = 0xffffffffffffffff //  end of table
)

func (page_no Page_no_t) String() string {

	switch page_no {
	case PAGE_BOT:
		return "BOT"
	case PAGE_EOT:
		return "EOT"
	default:
		return strconv.FormatUint(uint64(page_no), 10)
	}
}

const (
	NEXT_IDENTITY_OVERFLOW = rsql.MIN_INT64     // next value will overflow
	NEXT_IDENTITY_IS_SEED  = rsql.MIN_INT64 + 1 // next IDENTITY value must be seed value

	NEXT_IDENTITY_SPECIAL_LIMIT = NEXT_IDENTITY_IS_SEED // all values <= NEXT_IDENTITY_SPECIAL_LIMIT are special values
)

// WARNING: You must update these lines if you add or delete struct Page members, or change their type.
const (
	PAGE_CANVAS_LENGTH = PAGE_SIZE - 5*2 - 8*8 // 5*sizeof(uint16) + 8*sizeof(uint64). See Page struct members.

	PG_OFFSET_TUPLE_OFFSET_0        = PAGE_CANVAS_LENGTH
	PG_OFFSET_PAGE_DEPTH            = PG_OFFSET_TUPLE_OFFSET_0 + 2
	PG_OFFSET_TUPLE_COUNT           = PG_OFFSET_PAGE_DEPTH + 2
	PG_OFFSET_PG_TYPE               = PG_OFFSET_TUPLE_COUNT + 2
	PG_OFFSET_TD_TYPE               = PG_OFFSET_PG_TYPE + 2
	PG_OFFSET_DBID                  = PG_OFFSET_TD_TYPE + 2
	PG_OFFSET_SCHID                 = PG_OFFSET_DBID + 8
	PG_OFFSET_BASE_GTBLID           = PG_OFFSET_SCHID + 8
	PG_OFFSET_TBLID                 = PG_OFFSET_BASE_GTBLID + 8
	PG_OFFSET_PG_NO                 = PG_OFFSET_TBLID + 8
	PG_OFFSET_PREV_NO               = PG_OFFSET_PG_NO + 8
	PG_OFFSET_NEXT_NO               = PG_OFFSET_PREV_NO + 8
	PG_OFFSET_BOTTOM_TARGET_PAGE_NO = PG_OFFSET_NEXT_NO + 8
	// PAGE_SIZE = PG_OFFSET_BOTTOM_TARGET_PAGE_NO + 8       this is checked in init()
)

const TUPLE_OFFSET_SIZE = 2 // tuple offsets in pg_tuple_offsets[] array are uint16 values

func init() {

	if PG_OFFSET_BOTTOM_TARGET_PAGE_NO+8 != PAGE_SIZE {
		log.Fatalf("PG_OFFSET_BOTTOM_TARGET_PAGE_NO + 8 != PAGE_SIZE: %d != %d", PG_OFFSET_BOTTOM_TARGET_PAGE_NO+8, PAGE_SIZE)
	}

	if PINF_OFFSET_SENTINEL > PAGE_CANVAS_LENGTH {
		log.Fatalf("PINF_OFFSET_SENTINEL > PAGE_CANVAS_LENGTH: %d > %d", PINF_OFFSET_SENTINEL, PAGE_CANVAS_LENGTH)
	}

	// just to be sure, sum of uint8, uint16, uint32 and uint64 PINF_ fields

	if PINF_OFFSET_SENTINEL != 1*(PINF_COLUMN_ARRAY_CAPACITY+PINF_NK_ARRAY_COLL_CAPACITY+PZONE_ROOTBITMAP_ARRAY_SIZE)+2*(4+PINF_COLUMN_ARRAY_CAPACITY*2+1+PINF_NK_ARRAY_CAPACITY)+4*(3)+8*(5) {
		log.Fatalf("PINF_OFFSET_SENTINEL value %d is bad.", PINF_OFFSET_SENTINEL)
	}
}

// part of PAGE_CANVAS_LENGTH available for tuples and offsets.
//
func (page *Page) Available_room() int {
	var (
		tuple_count   int
		consumed_room int
	)

	tuple_count = page.Pg_tuple_count()

	consumed_room = page.Pg_tuple_offset(tuple_count) + tuple_count*TUPLE_OFFSET_SIZE

	assert(consumed_room >= 0 && consumed_room <= PAGE_CANVAS_LENGTH)

	return PAGE_CANVAS_LENGTH - consumed_room
}

// part of PAGE_CANVAS_LENGTH consumed by tuples and offsets.
//
func (page *Page) Consumed_room() int {
	var (
		tuple_count   int
		consumed_room int
	)

	tuple_count = page.Pg_tuple_count()

	consumed_room = page.Pg_tuple_offset(tuple_count) + tuple_count*TUPLE_OFFSET_SIZE

	assert(consumed_room >= 0 && consumed_room <= PAGE_CANVAS_LENGTH)

	return consumed_room
}

// part of PAGE_CANVAS_LENGTH consumed by tuples and offsets, up to and including tuple at tuple_index.
//
func (page *Page) Consumed_room_at_tuple_index(tuple_index int) int {
	var (
		consumed_room int
	)

	assert(tuple_index < page.Pg_tuple_count())
	assert(tuple_index >= -1)

	consumed_room = page.Pg_tuple_offset(tuple_index+1) + (tuple_index+1)*TUPLE_OFFSET_SIZE

	assert(consumed_room >= 0 && consumed_room <= PAGE_CANVAS_LENGTH)

	return consumed_room
}

// clear unused part of the canvas, for debugging.
//
func (page *Page) Debug_clear_unused_canvas_part() {
	var (
		tuple_count     int
		sentinel_offset int
	)

	tuple_count = page.Pg_tuple_count()

	sentinel_offset = page.Pg_tuple_offset(tuple_count)

	// set unused space in the page to 0

	unused_space := page.Pg_canvas[sentinel_offset : PAGE_CANVAS_LENGTH-tuple_count*TUPLE_OFFSET_SIZE]

	for i, _ := range unused_space {
		unused_space[i] = 88 // any value is good for filling
	}
}

// Page structure.
//
// A page is just an array of bytes, in field Pg_canvas.
// The top of this array of bytes contains common information, such as pg_no, etc.
// The payload is stored at beginning of this byte array, and contains different information, depending on the type of the page, as described below.
//
//
//     Pg_canvas's payload:
//     ====================
//
//        PAGE_TYPE_LEAF and PAGE_TYPE_NODE
//        ---------------------------------
//        Tuple storage in the page is done this way :
//            - tuples are put in Pg_canvas[] byte array.
//                The space consumed by tuple data grows upwards.
//            - tuple offsets are put in the array starting at pg_tuple_offset_0
//                E.g. pg_tuple_offset[-3] is offset of tuple 3. pg_tuple_offset[0] is always 0 and is offset of tuple 0.
//                pg_tuple_offset[pg_tuple_index] is offset of the first byte of unused space.
//                The space consumed by offsets grows downwards.
//            - when empty, the available space for storing tuples is PAGE_CANVAS_LENGTH.
//            - each tuple consumes the size of the tuple in bytes, plus two bytes for offset.
//
//        PAGE_TYPE_ZONE_ALLOCATOR
//        ------------------------
//        a zone allocator page's Pg_canvas[] has a layout which mimics this structure:
//            struct {
//                      pzone_free_page_count   uint32
//                      pzone_bitmap            [PZONE_BITMAP_ARRAY_SIZE]uint8             a bit value of 0 indicates a free page
//            }
//
//
//        PAGE_TYPE_INFO
//        --------------
//        a page info's Pg_canvas[] has a layout which mimics this structure:
//            struct {
//                      pinf_column_array_capacity                     uint16         // contains PINF_COLUMN_ARRAY_CAPACITY for info
//                      pinf_nk_array_capacity                         uint16         // contains PINF_NK_ARRAY_CAPACITY for info
//                      pinf_nk_array_coll_capacity                    uint16         // contains PINF_NK_ARRAY_COLL_CAPACITY for info
//
//                      pinf_column_count                              uint16
//                      pinf_column_base_seqno        [PINF_COLUMN_ARRAY_CAPACITY]uint16     // array of column seqnos
//                      pinf_column_datatypes         [PINF_COLUMN_ARRAY_CAPACITY]uint16     // array of rsql.Datatype_t. We don't care about precision for VARBINARY and VARCHAR, and we don't care about fixlen_flag.
//                      pinf_column_NUMERIC_precision [PINF_COLUMN_ARRAY_CAPACITY]uint8      // array of NUMERIC precision. We don't care about numeric scale. It is just for tools to display NUMERIC values with suitable width.
//
//                      pinf_nk_count                                  uint16
//                      pinf_nk_base_seqno     [PINF_NK_ARRAY_CAPACITY]uint16         // native key seqnos
//                      pinf_nk_collations [PINF_NK_ARRAY_COLL_CAPACITY]uint8         // native key collations, separated by semicolons
//
//                      pinf_next_rowid                                 int64         // next value for ROWID    column
//                      pinf_next_identity                              int64         // next value for IDENTITY column. It can contains the special value NEXT_IDENTITY_IS_SEED or NEXT_IDENTITY_OVERFLOW.
//
//                      pinf_root_page_no                              uint64         // Page_no_t. If 0, table is empty (0 is invalid value, as it points to the first allocator page).
//
//                      pinf_total_leaf_tuples_count                   uint64         // total number of leaf tuples in the table
//                      pinf_total_leaf_pages_count                    uint64         // total number of leaf pages  in the table
//
//                      pinf_zone_bitmaps_count                        uint32         // count of bitmap zone allocation pages in use. When a table is just created, it is 1, as page 0 is first zone allocation page.
//                      pinf_zone_bitmap_array_size                    uint32         // PZONE_BITMAP_ARRAY_SIZE
//
//                      pinf_zone_rootbitmap_array_size                uint32         // SPEC_TABLE_ZONE_ROOTBITMAP_ARRAY_SIZE
//                      pinf_zone_rootbitmap_array[PZONE_ROOTBITMAP_ARRAY_SIZE]uint8  // by default, all bits in array are 1. When a new zone allocator page is created, the corresponding bit is set to 0, meaning that free pages are available.
//            }
//
//
//     Common fields:
//     ==============
//
//            Fields like pg_page_depth, etc, are not real fields of the Page struct, a Page is just an array of bytes. Access to these pseudo-fields is made by methods like Pg_page_depth(), etc.
//
//            IMPORTANT: Update PAGE_CANVAS_LENGTH if you add or delete struct Page members, or change their type !!!
//
type Page struct {
	Pg_canvas [PAGE_SIZE]byte

	//      pg_storage_space   [PAGE_CANVAS_LENGTH]byte        // space for tuples, plus 2 bytes for offset per tuple
	//
	//      pg_tuple_offset[]  uint16    // pg_tuple_offset[0]==0 is offset of tuple 0. It is the bottom of an array of offers, growing DOWNWARDS. pg_tuple_offset[-1] for tuple 1 is just before this position, consuming two bytes from pg_storage_space, etc.
	//                                                                                  A sentinel offset also exists, which points one byte pas the end of the tuple. For n tuples, there are n+1 offsets (tuple offsets and sentinel offset).
	//      pg_page_depth      uint16                          // 0 for leaf page, 1, 2, etc for node pages at deeper level.
	//      pg_tuple_count     uint16                          // number of tuples stored in page. page.Pg_tuple_offset(pg_tuple_index) points at the first byte of non-used space
	//      pg_type            Pg_type_t (uint16)              // PAGE_TYPE_LEAF or PAGE_TYPE_NODE, or PAGE_TYPE_ZONE_ALLOCATOR or PAGE_TYPE_TABLE_INFO
	//      pg_td_type         rsql.Td_type_t (uint16)         // in fact, Td_type_t is uint8, but I prefer to store it in two bytes, because I think it is prettier
	//      pg_dbid            Dbid_t    (int64)               // <td_type, pg_dbid, pg_schid, pg_tblid> gives the path of the data file
	//      pg_schid           Schid_t   (int64)
	//      pg_base_gtblid     Tblid_t   (int64)
	//      pg_tblid           Tblid_t   (int64)
	//      pg_no              Page_no_t (uint64)
	//      pg_prev_no         Page_no_t (uint64)              // PAGE_BOT for beginning of table
	//      pg_next_no         Page_no_t (uint64)              // PAGE_EOT for end of table
	//      pg_bottom_target_page_no      Page_no_t (uint64)   // only for node pages
}

func (page *Page) Clear() {

	for i := range page.Pg_canvas {
		page.Pg_canvas[i] = 0
	}
}

// for debuging.
//
func (page *Page) Fill_0xff() {

	for i := range page.Pg_canvas {
		page.Pg_canvas[i] = 0xFF
	}
}

func (page *Page) Has_no_prev() bool {

	if page.Pg_prev_no() == PAGE_BOT {
		return true
	}

	return false
}

func (page *Page) Has_no_next() bool {

	if page.Pg_next_no() == PAGE_EOT {
		return true
	}

	return false
}

func get_uint64(canvas *[PAGE_SIZE]byte, field_offset int) uint64 {
	return uint64(canvas[field_offset]) | uint64(canvas[field_offset+1])<<8 | uint64(canvas[field_offset+2])<<16 | uint64(canvas[field_offset+3])<<24 |
		uint64(canvas[field_offset+4])<<32 | uint64(canvas[field_offset+5])<<40 | uint64(canvas[field_offset+6])<<48 | uint64(canvas[field_offset+7])<<56
}

func set_uint64(canvas *[PAGE_SIZE]byte, field_offset int, v uint64) {
	canvas[field_offset+0] = byte(v)
	canvas[field_offset+1] = byte(v >> 8)
	canvas[field_offset+2] = byte(v >> 16)
	canvas[field_offset+3] = byte(v >> 24)
	canvas[field_offset+4] = byte(v >> 32)
	canvas[field_offset+5] = byte(v >> 40)
	canvas[field_offset+6] = byte(v >> 48)
	canvas[field_offset+7] = byte(v >> 56)
}

func get_int64(canvas *[PAGE_SIZE]byte, field_offset int) int64 {
	return int64(get_uint64(canvas, field_offset))
}

func set_int64(canvas *[PAGE_SIZE]byte, field_offset int, v int64) {
	set_uint64(canvas, field_offset, uint64(v))
}

func get_uint32(canvas *[PAGE_SIZE]byte, field_offset int) uint32 {
	return uint32(canvas[field_offset]) | uint32(canvas[field_offset+1])<<8 | uint32(canvas[field_offset+2])<<16 | uint32(canvas[field_offset+3])<<24
}

func set_uint32(canvas *[PAGE_SIZE]byte, field_offset int, v uint32) {
	canvas[field_offset+0] = byte(v)
	canvas[field_offset+1] = byte(v >> 8)
	canvas[field_offset+2] = byte(v >> 16)
	canvas[field_offset+3] = byte(v >> 24)
}

func get_uint16(canvas *[PAGE_SIZE]byte, field_offset int) uint16 {
	return uint16(canvas[field_offset]) | uint16(canvas[field_offset+1])<<8
}

func set_uint16(canvas *[PAGE_SIZE]byte, field_offset int, v uint16) {
	canvas[field_offset+0] = byte(v)
	canvas[field_offset+1] = byte(v >> 8)
}

func get_uint8(canvas *[PAGE_SIZE]byte, field_offset int) uint8 {
	return canvas[field_offset]
}

func set_uint8(canvas *[PAGE_SIZE]byte, field_offset int, v uint8) {
	canvas[field_offset] = v
}

func (page *Page) Pg_tuple_offset(index int) int {

	return int(get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_OFFSET_0-2*index))
}

func (page *Page) Pg_tuple_length(index int) int {

	assert(index < int(get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_COUNT)))

	tuple_length := int(get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_OFFSET_0-2*(index+1))) - int(get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_OFFSET_0-2*index))

	return tuple_length
}

func (page *Page) Pg_tuple(index int) rsql.Tuple {

	assert(index < int(get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_COUNT)))

	tuple_offset := get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_OFFSET_0-2*index)
	tuple_endx := get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_OFFSET_0-2*(index+1))

	return page.Pg_canvas[tuple_offset:tuple_endx]
}

func (page *Page) Pg_page_depth() int {
	return int(get_uint16(&page.Pg_canvas, PG_OFFSET_PAGE_DEPTH))
}

func (page *Page) Pg_tuple_count() int {
	return int(get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_COUNT))
}

func (page *Page) Pg_type() Pg_type_t {
	return Pg_type_t(get_uint16(&page.Pg_canvas, PG_OFFSET_PG_TYPE))
}

func (page *Page) Pg_td_type() rsql.Td_type_t {
	return rsql.Td_type_t(get_uint16(&page.Pg_canvas, PG_OFFSET_TD_TYPE))
}

func (page *Page) Pg_dbid() Dbid_t {
	return Dbid_t(get_uint64(&page.Pg_canvas, PG_OFFSET_DBID))
}

func (page *Page) Pg_schid() Schid_t {
	return Schid_t(get_uint64(&page.Pg_canvas, PG_OFFSET_SCHID))
}

func (page *Page) Pg_base_gtblid() Tblid_t {
	return Tblid_t(get_uint64(&page.Pg_canvas, PG_OFFSET_BASE_GTBLID))
}

func (page *Page) Pg_tblid() Tblid_t {
	return Tblid_t(get_uint64(&page.Pg_canvas, PG_OFFSET_TBLID))
}

func (page *Page) Pg_no() Page_no_t {
	return Page_no_t(get_uint64(&page.Pg_canvas, PG_OFFSET_PG_NO))
}

func (page *Page) Pg_prev_no() Page_no_t {
	return Page_no_t(get_uint64(&page.Pg_canvas, PG_OFFSET_PREV_NO))
}

func (page *Page) Pg_next_no() Page_no_t {
	return Page_no_t(get_uint64(&page.Pg_canvas, PG_OFFSET_NEXT_NO))
}

func (page *Page) Pg_bottom_target_page_no() Page_no_t {
	return Page_no_t(get_uint64(&page.Pg_canvas, PG_OFFSET_BOTTOM_TARGET_PAGE_NO))
}

func (page *Page) Set_pg_tuple_offset(index int, v uint16) {
	assert(index > 0 || (index == 0 && v == 0))

	set_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_OFFSET_0-2*index, v)
}

func (page *Page) Set_pg_page_depth(v uint16) {
	set_uint16(&page.Pg_canvas, PG_OFFSET_PAGE_DEPTH, v)
}

func (page *Page) Set_pg_tuple_count(v uint16) {
	set_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_COUNT, v)
}

func (page *Page) Add_pg_tuple_count(v int) {
	var (
		count     int
		count_new int
	)

	count = int(get_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_COUNT))
	count_new = count + v

	assert(count_new >= 0 && count_new <= 0xFFFF)

	set_uint16(&page.Pg_canvas, PG_OFFSET_TUPLE_COUNT, uint16(count_new))
}

func (page *Page) Set_pg_type(v Pg_type_t) {
	set_uint16(&page.Pg_canvas, PG_OFFSET_PG_TYPE, uint16(v))
}

func (page *Page) Set_pg_td_type(v rsql.Td_type_t) {
	set_uint16(&page.Pg_canvas, PG_OFFSET_TD_TYPE, uint16(v))
}

func (page *Page) Set_pg_dbid(v Dbid_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_DBID, uint64(v)) // Dbid_t is in fact int64
}

func (page *Page) Set_pg_schid(v Schid_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_SCHID, uint64(v)) // Schid_t is in fact int64
}

func (page *Page) Set_pg_base_gtblid(v Tblid_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_BASE_GTBLID, uint64(v)) // Tblid_t is in fact int64
}

func (page *Page) Set_pg_tblid(v Tblid_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_TBLID, uint64(v)) // Tblid_t is in fact int64
}

func (page *Page) Set_pg_no(v Page_no_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_PG_NO, uint64(v))
}

func (page *Page) Set_pg_prev_no(v Page_no_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_PREV_NO, uint64(v))
}

func (page *Page) Set_pg_next_no(v Page_no_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_NEXT_NO, uint64(v))
}

func (page *Page) Set_pg_bottom_target_page_no(v Page_no_t) {
	set_uint64(&page.Pg_canvas, PG_OFFSET_BOTTOM_TARGET_PAGE_NO, uint64(v))
}

//====================================================
//                zone allocator page
//====================================================

//        a zone allocator page's Pg_canvas[] has a layout which mimics this structure:
//            struct {
//                      pzone_free_page_count   uint32
//                      pzone_bitmap            [PZONE_BITMAP_ARRAY_SIZE]uint8             a bit value of 0 indicates a free page
//            }
const (
	PZONE_OFFSET_FREE_PAGE_COUNT = 0
	PZONE_OFFSET_BITMAP          = 4
)

func (page_allocator *Page) Pzone_free_page_count() uint32 {
	return get_uint32(&page_allocator.Pg_canvas, PZONE_OFFSET_FREE_PAGE_COUNT)
}

func (page_allocator *Page) Set_pzone_free_page_count(v uint32) {
	set_uint32(&page_allocator.Pg_canvas, PZONE_OFFSET_FREE_PAGE_COUNT, v)
}

func (page_allocator *Page) Pzone_bitmap() []uint8 {
	return page_allocator.Pg_canvas[PZONE_OFFSET_BITMAP : PZONE_OFFSET_BITMAP+PZONE_BITMAP_ARRAY_SIZE]
}

func (page_allocator *Page) Pzone_is_empty() bool {

	if PZONE_NUMBER_OF_PAGES_IN_ZONE-page_allocator.Pzone_free_page_count() > 1 {
		//println("           page_allocator.Pzone_free_page_count() =", page_allocator.Pzone_free_page_count(), page_allocator.Pg_no())
		return false
	}

	// zone is empty, only allocator page is allocated

	bitmap := page_allocator.Pzone_bitmap()

	for i, b := range bitmap {
		if i != 0 && b == 0 {
			continue
		}

		if i == 0 && b == 0x01 { // first bit is zone allocator
			continue
		}

		log.Printf("table Tblid %d Gtblid %d is corrupted. Allocator bitmap for zone %d is not empty.", page_allocator.Pg_tblid(), page_allocator.Pg_base_gtblid(), i)
	}

	return true
}

func (page_allocator *Page) Pzone_highest_allocated_delta_page_no() Page_no_t {
	var (
		bitmap []uint8
	)

	bitmap = page_allocator.Pg_canvas[PZONE_OFFSET_BITMAP : PZONE_OFFSET_BITMAP+PZONE_BITMAP_ARRAY_SIZE]

	for i := len(bitmap) - 1; i >= 0; i-- {
		b := bitmap[i]
		if b == 0 {
			continue
		}

		for k := 0; k < 8; k++ {
			if (b<<uint(k))&0x80 != 0 {
				return Page_no_t(i)*8 + Page_no_t(7-k)
			}
		}

		panic("impossible")
	}

	panic("impossible") // because page_allocator is always allocated
}

//====================================================
//                   info page
//====================================================

//        a page info's Pg_canvas[] has a layout which mimics this structure:
//            struct {
//                      pinf_column_array_capacity                     uint16         // contains PINF_COLUMN_ARRAY_CAPACITY for info
//                      pinf_nk_array_capacity                         uint16         // contains PINF_NK_ARRAY_CAPACITY for info
//                      pinf_nk_array_coll_capacity                    uint16         // contains PINF_NK_ARRAY_COLL_CAPACITY for info
//
//                      pinf_column_count                              uint16
//                      pinf_column_base_seqno        [PINF_COLUMN_ARRAY_CAPACITY]uint16     // array of column seqnos
//                      pinf_column_datatypes         [PINF_COLUMN_ARRAY_CAPACITY]uint16     // array of rsql.Datatype_t. We don't care about precision for VARBINARY and VARCHAR, and we don't care about fixlen_flag.
//                      pinf_column_NUMERIC_precision [PINF_COLUMN_ARRAY_CAPACITY]uint8      // array of NUMERIC precision. We don't care about numeric scale. It is just for tools to display NUMERIC values with suitable width.
//
//                      pinf_nk_count                                  uint16
//                      pinf_nk_base_seqno     [PINF_NK_ARRAY_CAPACITY]uint16         // native key seqnos
//                      pinf_nk_collations [PINF_NK_ARRAY_COLL_CAPACITY]uint8         // native key collations, separated by semicolons
//
//                      pinf_next_rowid                                 int64         // next value for ROWID    column
//                      pinf_next_identity                              int64         // next value for IDENTITY column. It can contains the special value NEXT_IDENTITY_IS_SEED or NEXT_IDENTITY_OVERFLOW.
//
//                      pinf_root_page_no                              uint64         // Page_no_t. Root of the btree. If 0, table is empty (0 is invalid value, as it points to the first allocator page).
//
//                      pinf_total_leaf_tuples_count                   uint64         // total number of leaf tuples in the table
//                      pinf_total_leaf_pages_count                    uint64         // total number of leaf pages  in the table
//
//                      pinf_zone_bitmaps_count                        uint32         // count of bitmap zone allocation pages in use. When a table is just created, it is 1, as page 0 is first zone allocation page.
//                      pinf_zone_bitmap_array_size                    uint32         // PZONE_BITMAP_ARRAY_SIZE
//
//                      pinf_zone_rootbitmap_array_size                uint32         // SPEC_TABLE_ZONE_ROOTBITMAP_ARRAY_SIZE
//                      pinf_zone_rootbitmap_array[PZONE_ROOTBITMAP_ARRAY_SIZE]uint8  // by default, all bits in array are 1. When a new zone allocator page is created, the corresponding bit is set to 0, meaning that free pages are available.
//            }
const (
	PINF_COLUMN_ARRAY_CAPACITY  = rsql.SPEC_TABLE_NUMBER_OF_COLUMNS_MAX          // max number of columns, including ROWID
	PINF_NK_ARRAY_CAPACITY      = rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX + 1 // max number of native key columns. +1 because ROWID can have been appended to make native key unique.
	PINF_NK_ARRAY_COLL_CAPACITY = PINF_NK_ARRAY_CAPACITY * 30                    // 29 characters per collation, plus semicolon. It is largely enough.

	PINF_OFFSET_COLUMN_ARRAY_CAPACITY      = 0
	PINF_OFFSET_NK_ARRAY_CAPACITY          = PINF_OFFSET_COLUMN_ARRAY_CAPACITY + 2
	PINF_OFFSET_NK_ARRAY_COLL_CAPACITY     = PINF_OFFSET_NK_ARRAY_CAPACITY + 2
	PINF_OFFSET_COLUMN_COUNT               = PINF_OFFSET_NK_ARRAY_COLL_CAPACITY + 2
	PINF_OFFSET_COLUMN_BASE_SEQNO          = PINF_OFFSET_COLUMN_COUNT + 2
	PINF_OFFSET_COLUMN_DATATYPES           = PINF_OFFSET_COLUMN_BASE_SEQNO + 2*PINF_COLUMN_ARRAY_CAPACITY
	PINF_OFFSET_COLUMN_NUMERIC_PRECISION   = PINF_OFFSET_COLUMN_DATATYPES + 2*PINF_COLUMN_ARRAY_CAPACITY
	PINF_OFFSET_NK_COUNT                   = PINF_OFFSET_COLUMN_NUMERIC_PRECISION + PINF_COLUMN_ARRAY_CAPACITY
	PINF_OFFSET_NK_BASE_SEQNO              = PINF_OFFSET_NK_COUNT + 2
	PINF_OFFSET_NK_COLLATIONS              = PINF_OFFSET_NK_BASE_SEQNO + 2*PINF_NK_ARRAY_CAPACITY
	PINF_OFFSET_NEXT_ROWID                 = PINF_OFFSET_NK_COLLATIONS + PINF_NK_ARRAY_COLL_CAPACITY
	PINF_OFFSET_NEXT_IDENTITY              = PINF_OFFSET_NEXT_ROWID + 8
	PINF_OFFSET_ROOT_PAGE_NO               = PINF_OFFSET_NEXT_IDENTITY + 8
	PINF_OFFSET_TOTAL_LEAF_TUPLES_COUNT    = PINF_OFFSET_ROOT_PAGE_NO + 8
	PINF_OFFSET_TOTAL_LEAF_PAGES_COUNT     = PINF_OFFSET_TOTAL_LEAF_TUPLES_COUNT + 8
	PINF_OFFSET_ZONE_BITMAPS_COUNT         = PINF_OFFSET_TOTAL_LEAF_PAGES_COUNT + 8
	PINF_OFFSET_ZONE_BITMAP_ARRAY_SIZE     = PINF_OFFSET_ZONE_BITMAPS_COUNT + 4
	PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY_SIZE = PINF_OFFSET_ZONE_BITMAP_ARRAY_SIZE + 4
	PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY      = PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY_SIZE + 4
	PINF_OFFSET_SENTINEL                   = PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY + 1*PZONE_ROOTBITMAP_ARRAY_SIZE
)

func (page_info *Page) Pinf_column_array_capacity() uint16 {
	return get_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_ARRAY_CAPACITY)
}

func (page_info *Page) Pinf_nk_array_capacity() uint16 {
	return get_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_ARRAY_CAPACITY)
}

func (page_info *Page) Pinf_nk_array_coll_capacity() uint16 {
	return get_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_ARRAY_COLL_CAPACITY)
}

func (page_info *Page) Pinf_column_count() uint16 {
	return get_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_COUNT)
}

func (page_info *Page) Pinf_column_base_seqno(i int) uint16 {
	if i >= PINF_COLUMN_ARRAY_CAPACITY {
		panic("impossible")
	}

	return get_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_BASE_SEQNO+i*2)
}

func (page_info *Page) Pinf_column_datatypes(i int) rsql.Datatype_t {
	if i >= PINF_COLUMN_ARRAY_CAPACITY {
		panic("impossible")
	}

	return rsql.Datatype_t(get_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_DATATYPES+i*2))
}

func (page_info *Page) Pinf_column_NUMERIC_precision(i int) uint8 {
	if i >= PINF_COLUMN_ARRAY_CAPACITY {
		panic("impossible")
	}

	return get_uint8(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_NUMERIC_PRECISION+i)
}

func (page_info *Page) Pinf_nk_count() uint16 {
	return get_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_COUNT)
}

func (page_info *Page) Pinf_nk_base_seqno(i int) uint16 {
	if i >= PINF_NK_ARRAY_CAPACITY {
		panic("impossible")
	}

	return get_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_BASE_SEQNO+i*2)
}

const COLLATION_LISTINFO_INVALID = "<na>"

func (page_info *Page) Pinf_nk_collations() (collations []string, ok bool) {
	var (
		collation_listinfo string
		start              int
	)

	collation_listinfo = string(page_info.Pg_canvas[PINF_OFFSET_NK_COLLATIONS : PINF_OFFSET_NK_COLLATIONS+PINF_NK_ARRAY_COLL_CAPACITY])

	if collation_listinfo == COLLATION_LISTINFO_INVALID {
		return nil, false
	}

	assert(collation_listinfo[0] == '[')
	collation_listinfo = collation_listinfo[1:]

	start = 0

	for i, c := range collation_listinfo {
		if c == ';' || c == ']' {
			collations = append(collations, collation_listinfo[start:i])
			start = i + 1

			if c == ']' {
				break
			}
		}
	}

	return collations, true
}

func (page_info *Page) Pinf_next_rowid() int64 {
	return get_int64(&page_info.Pg_canvas, PINF_OFFSET_NEXT_ROWID)
}

func (page_info *Page) Pinf_next_identity() int64 {
	return get_int64(&page_info.Pg_canvas, PINF_OFFSET_NEXT_IDENTITY)
}

// returns root page no, which can be a node or a leaf page.
// If 0, table is empty (0 is invalid value, as it points to the first allocator page).
//
func (page_info *Page) Pinf_root_page_no() Page_no_t {
	return Page_no_t(get_uint64(&page_info.Pg_canvas, PINF_OFFSET_ROOT_PAGE_NO))
}

func (page_info *Page) Pinf_total_leaf_tuples_count() uint64 {
	return get_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_TUPLES_COUNT)
}

func (page_info *Page) Add_pinf_total_leaf_tuples_count(v int) {
	var (
		count     int
		count_new int
	)

	count = int(get_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_TUPLES_COUNT))
	count_new = count + v

	assert(count_new >= 0)

	set_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_TUPLES_COUNT, uint64(count_new))
}

func (page_info *Page) Pinf_total_leaf_pages_count() uint64 {
	return get_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_PAGES_COUNT)
}

func (page_info *Page) Add_pinf_total_leaf_pages_count(v int) {
	var (
		count     int
		count_new int
	)

	count = int(get_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_PAGES_COUNT))
	count_new = count + v

	assert(count_new >= 0)

	set_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_PAGES_COUNT, uint64(count_new))
}

func (page_info *Page) Pinf_zone_bitmaps_count() uint32 {
	return get_uint32(&page_info.Pg_canvas, PINF_OFFSET_ZONE_BITMAPS_COUNT)
}

func (page_info *Page) Pinf_zone_bitmap_array_size() uint32 {
	return get_uint32(&page_info.Pg_canvas, PINF_OFFSET_ZONE_BITMAP_ARRAY_SIZE)
}

func (page_info *Page) Pinf_zone_rootbitmap_array_size() uint32 {
	return get_uint32(&page_info.Pg_canvas, PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY_SIZE)
}

func (page_info *Page) Pinf_zone_rootbitmap_array() []uint8 {
	return page_info.Pg_canvas[PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY : PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY+PZONE_ROOTBITMAP_ARRAY_SIZE]
}

func (page_info *Page) Set_pinf_column_array_capacity(v uint16) {
	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_ARRAY_CAPACITY, v)
}

func (page_info *Page) Set_pinf_nk_array_capacity(v uint16) {
	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_ARRAY_CAPACITY, v)
}

func (page_info *Page) Set_pinf_nk_array_coll_capacity(v uint16) {
	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_ARRAY_COLL_CAPACITY, v)
}

func (page_info *Page) Set_pinf_column_count(v uint16) {
	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_COUNT, v)
}

func (page_info *Page) Set_pinf_column_base_seqno(i int, v uint16) {
	if i >= PINF_COLUMN_ARRAY_CAPACITY {
		panic("impossible")
	}

	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_BASE_SEQNO+i*2, v)
}

func (page_info *Page) Set_pinf_column_datatypes(i int, v rsql.Datatype_t) {
	if i >= PINF_COLUMN_ARRAY_CAPACITY {
		panic("impossible")
	}

	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_DATATYPES+i*2, uint16(v))
}

func (page_info *Page) Set_pinf_column_NUMERIC_precision(i int, v uint8) {
	if i >= PINF_COLUMN_ARRAY_CAPACITY {
		panic("impossible")
	}

	set_uint8(&page_info.Pg_canvas, PINF_OFFSET_COLUMN_NUMERIC_PRECISION+i, v)
}

func (page_info *Page) Set_pinf_nk_count(v uint16) {
	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_COUNT, v)
}

func (page_info *Page) Set_pinf_nk_base_seqno(i int, v uint16) {
	if i >= PINF_NK_ARRAY_CAPACITY {
		panic("impossible")
	}

	set_uint16(&page_info.Pg_canvas, PINF_OFFSET_NK_BASE_SEQNO+i*2, v)
}

func (page_info *Page) Set_pinf_nk_collations(collation_listinfo []byte) {

	collation_listinfo_length := len(collation_listinfo)

	if collation_listinfo_length > PINF_NK_ARRAY_COLL_CAPACITY {
		collation_listinfo = append(collation_listinfo[:0], COLLATION_LISTINFO_INVALID...)
	}

	copy(page_info.Pg_canvas[PINF_OFFSET_NK_COLLATIONS:PINF_OFFSET_NK_COLLATIONS+collation_listinfo_length], collation_listinfo)
}

func (page_info *Page) Set_pinf_next_rowid(v int64) {
	set_int64(&page_info.Pg_canvas, PINF_OFFSET_NEXT_ROWID, v)
}

func (page_info *Page) Set_pinf_next_identity(v int64) {
	set_int64(&page_info.Pg_canvas, PINF_OFFSET_NEXT_IDENTITY, v)
}

func (page_info *Page) Set_pinf_root_page_no(v Page_no_t) {
	set_uint64(&page_info.Pg_canvas, PINF_OFFSET_ROOT_PAGE_NO, uint64(v))
}

func (page_info *Page) Set_pinf_total_leaf_tuples_count(v uint64) {
	set_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_TUPLES_COUNT, v)
}

func (page_info *Page) Set_pinf_total_leaf_pages_count(v uint64) {
	set_uint64(&page_info.Pg_canvas, PINF_OFFSET_TOTAL_LEAF_PAGES_COUNT, v)
}

func (page_info *Page) Set_pinf_zone_bitmaps_count(v uint32) {
	set_uint32(&page_info.Pg_canvas, PINF_OFFSET_ZONE_BITMAPS_COUNT, v)
}

func (page_info *Page) Set_pinf_zone_bitmap_array_size(v uint32) {
	set_uint32(&page_info.Pg_canvas, PINF_OFFSET_ZONE_BITMAP_ARRAY_SIZE, v)
}

func (page_info *Page) Set_pinf_zone_rootbitmap_array_size(v uint32) {
	set_uint32(&page_info.Pg_canvas, PINF_OFFSET_ZONE_ROOTBITMAP_ARRAY_SIZE, v)
}

//====================================================
//                   logfile status sector
//====================================================

// These functions operates on the page 0 of logfile, which contains status and other information.
// The full logpage 0 is not used, but only a small part of it, smaller than a disk sector, so that changes are atomic.

type Logfile_sector [LOGFILE_SECTOR_SIZE]byte

const (
	PLOG_OFFSET_STATUS    = 0
	PLOG_OFFSET_COUNT     = 8
	PLOG_OFFSET_COUNT_BAK = 16
	LOGFILE_SECTOR_SIZE   = 24 // if we need more fields in Logfile_sector, we can increase LOGFILE_SECTOR_SIZE, but is must remain <= sector size on disk (512 bytes), so that changes of log status are atomic.
)

func (sect *Logfile_sector) Load(logfile *os.File) error {
	var (
		err error
		n   int
	)

	if _, err := logfile.Seek(0, 0); err != nil {
		return err
	}

	if n, err = io.ReadFull(logfile, sect[:]); err != nil {
		return err
	}
	assert(n == LOGFILE_SECTOR_SIZE)

	return nil
}

func (sect *Logfile_sector) Save(logfile *os.File) error {
	var (
		err error
		n   int
	)

	if n, err = logfile.WriteAt(sect[:], 0); err != nil { // writes log status and logfile_ppage_count at beginning of log file.
		return err
	}
	assert(n == LOGFILE_SECTOR_SIZE)

	return nil
}

func (sect *Logfile_sector) Plog_status() Log_status_t {
	return Log_status_t(rsql.Get_uint64(sect[PLOG_OFFSET_STATUS : PLOG_OFFSET_STATUS+8]))
}

func (sect *Logfile_sector) Plog_count() uint64 {
	return rsql.Get_uint64(sect[PLOG_OFFSET_COUNT : PLOG_OFFSET_COUNT+8])
}

func (sect *Logfile_sector) Plog_count_bak() uint64 {
	return rsql.Get_uint64(sect[PLOG_OFFSET_COUNT_BAK : PLOG_OFFSET_COUNT_BAK+8])
}

func (sect *Logfile_sector) Set_plog_status(v Log_status_t) {
	rsql.Set_uint64(sect[PLOG_OFFSET_STATUS:PLOG_OFFSET_STATUS+8], uint64(v))
}

func (sect *Logfile_sector) Set_plog_count(v uint64) {
	rsql.Set_uint64(sect[PLOG_OFFSET_COUNT:PLOG_OFFSET_COUNT+8], v)
}

func (sect *Logfile_sector) Set_plog_count_bak(v uint64) {
	rsql.Set_uint64(sect[PLOG_OFFSET_COUNT_BAK:PLOG_OFFSET_COUNT_BAK+8], v)
}
