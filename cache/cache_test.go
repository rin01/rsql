package cache

import (
	"fmt"
	"math/rand"
	"strconv"
	"testing"

	"rsql"
)

func Test_page_set_get(t *testing.T) {

	p := &Page{}

	const (
		ra  = 0x537b
		rb  = 0x7829
		rc  = 0x57bd
		r1  = 0xdf3d
		r2  = 0x9bf5
		r3  = 0x210c
		r4  = 0x0073
		r5  = 0x4b977ccef9ac1ec5
		r6  = 0x20ec3f5f185096ed
		r7  = 0x38cb73480cc80267
		r8  = 0x27118c0ab0b1d5bc
		r9  = 0x1877226663666663
		r10 = 0x267777aabbbbbcdd
		r11 = 0x2477003002338ab1
	)

	p.Set_pg_tuple_offset(1, ra)
	p.Set_pg_tuple_offset(3, rb)
	p.Set_pg_tuple_offset(2, rc)
	p.Set_pg_page_depth(r1)
	p.Set_pg_tuple_count(r2)
	p.Set_pg_type(r3)
	p.Set_pg_td_type(r4)
	p.Set_pg_dbid(r5)
	p.Set_pg_schid(r6)
	p.Set_pg_base_gtblid(r7)
	p.Set_pg_tblid(r8)
	p.Set_pg_no(r9)
	p.Set_pg_prev_no(r10)
	p.Set_pg_next_no(r11)

	if aa := p.Pg_tuple_offset(1); aa != ra {
		t.Fatalf("Pg_tuple_offset is %d, should be %d", aa, ra)
	}
	if ab := p.Pg_tuple_offset(3); ab != rb {
		t.Fatalf("Pg_tuple_offset is %d, should be %d", ab, rb)
	}
	if ac := p.Pg_tuple_offset(2); ac != rc {
		t.Fatalf("Pg_tuple_offset is %d, should be %d", ac, rc)
	}

	if a_0 := p.Pg_tuple_offset(2); a_0 != rc {
		t.Fatalf("Pg_tuple_offset is %d, should be %d", a_0, 0)
	}

	if a1 := p.Pg_page_depth(); a1 != r1 {
		t.Fatalf("Pg_page_depth is %d, should be %d", a1, r1)
	}

	if a2 := p.Pg_tuple_count(); a2 != r2 {
		t.Fatalf("Pg_tuple_count is %d, should be %d", a2, r2)
	}

	if a3 := p.Pg_type(); a3 != r3 {
		t.Fatalf("Pg_type is %d, should be %d", a3, r3)
	}

	if a4 := p.Pg_td_type(); a4 != r4 {
		t.Fatalf("Pg_td_type is %d, should be %d", a4, r4)
	}

	if a5 := p.Pg_dbid(); a5 != r5 {
		t.Fatalf("Pg_dbid is %d, should be %d", a5, r5)
	}

	if a6 := p.Pg_schid(); a6 != r6 {
		t.Fatalf("Pg_schid is %d, should be %d", a6, r6)
	}

	if a7 := p.Pg_base_gtblid(); a7 != r7 {
		t.Fatalf("Pg_tblid is %d, should be %d", a7, r7)
	}

	if a8 := p.Pg_tblid(); a8 != r8 {
		t.Fatalf("Pg_tblid is %d, should be %d", a8, r8)
	}

	if a9 := p.Pg_no(); a9 != r9 {
		t.Fatalf("Pg_no is %d, should be %d", a9, r9)
	}

	if a10 := p.Pg_prev_no(); a10 != r10 {
		t.Fatalf("Pg_prev_no is %d, should be %d", a10, r10)
	}

	if a11 := p.Pg_next_no(); a11 != r11 {
		t.Fatalf("Pg_next_no is %d, should be %d", a11, r11)
	}

}

// just some insert and remove.
//
func Test_gpage_cache(t *testing.T) {

	gpage_cache := &Gpage_cache{}

	gpage_cache.Init(2) // all pages are in free list

	pce2 := gpage_cache.pop_last(PCE_FREE_LIST)
	gpage_cache.insert_first(PCE_PIN_LIST, pce2)
	pce0 := gpage_cache.pop_last(PCE_FREE_LIST)
	gpage_cache.insert_first(PCE_PIN_LIST, pce0)
	pce1 := gpage_cache.pop_last(PCE_FREE_LIST)
	gpage_cache.insert_first(PCE_PIN_LIST, pce1)

	gpage_cache.remove(PCE_PIN_LIST, pce0)
	gpage_cache.insert_first(PCE_FREE_LIST, pce0)
	gpage_cache.remove(PCE_PIN_LIST, pce1)
	gpage_cache.insert_first(PCE_FREE_LIST, pce1)
	gpage_cache.remove(PCE_PIN_LIST, pce2)
	gpage_cache.insert_first(PCE_FREE_LIST, pce2)

	//	gpage_cache.Print(PCE_PIN_LIST)
	//	gpage_cache.Print(PCE_LRU_LIST)
	//	gpage_cache.Print(PCE_FREE_LIST)
}

func Test_wcache(t *testing.T) {

	WORKERS_JOURNALS.Initialize()

	journal := WORKERS_JOURNAL.Get_journal(0)
	defer WORKERS_JOURNAL.Release_journal(journal) // release journal when session terminates
	wcache := New_wcache(0, 2, 400, 0, 0)

	for i := 0; i < 10; i++ {
		rsql.Printf("--- %d ---\n", i)
		wcache.enlarge_element_array_and_hashtable()
	}

	for i := 0; i < 10; i++ {
		elem := wcache.pop_last(PCE_FREE_LIST)
		if elem == nil {
			t.Fatal("elem nil")
		}

		elem.pce_page_no = Page_no_t(i)
		elem.pce_hashval = uint64(i)
		elem.pce_version = PCE_DRAFT

		wcache.insert_first(PCE_SLIM_LIST, elem)
		wcache.insert_first_in_hashtable(elem)
		wcache.insert_first_in_draft_list(elem)
	}

	// rsql.Printf("hashtable: %d %v\n", len(wcache.wpc_hashtable), wcache.wpc_hashtable)

	wcache.debug_print_draft_list()

	for i := 0; i < 10; i++ {
		elem := wcache.pop_last_from_draft_list()
		wcache.remove(PCE_SLIM_LIST, elem)
		wcache.remove_from_hashtable(elem)

		wcache.insert_first(PCE_FREE_LIST, elem)
	}

	// rsql.Printf("hashtable: %d %v\n", len(wcache.wpc_hashtable), wcache.wpc_hashtable)

	//wcache.Print(PCE_PIN_LIST)
	//wcache.Print(PCE_LRU_LIST)
	//wcache.Print(PCE_SLIM_LIST)
	//wcache.Print(PCE_FREE_LIST)

	wcache.debug_print_draft_list()

}

func Test_allocator_zone_bitmap_get_bit(t *testing.T) {
	var result []byte

	a := []uint8{1<<3 + 1<<7, 128, 1, 0, 255, 131, 6}

	for i := uint64(0); i < uint64(len(a))*8; i++ {
		r := Zone_bitmap_get_bit(a, i)
		switch r {
		case 0:
			result = append(result, '0')
		case 1:
			result = append(result, '1')
		}
	}

	if string(result) != "00010001"+"00000001"+"10000000"+"00000000"+"11111111"+"11000001"+"01100000" {
		t.Fatalf("bad result: %s", result)
	}
}

func Test_allocator_zone_bitmap_set_bit(t *testing.T) {

	s := "00010001" + "00000001" + "10000000" + "00000000" + "11111111" + "11000001" + "01100000"

	a := make([]byte, len(s)/8)

	for i, b := range s {
		if b != '0' {
			Zone_bitmap_set_bit(a, uint64(i), 1)
			if Zone_bitmap_get_bit(a, uint64(i)) == 0 {
				t.Fatalf("failure set get bit")
			}
		}
	}

	// check a

	var result []byte

	for i := uint64(0); i < uint64(len(a))*8; i++ {
		r := Zone_bitmap_get_bit(a, i)
		switch r {
		case 0:
			result = append(result, '0')
		case 1:
			result = append(result, '1')
		}
	}

	if string(result) != s {
		t.Fatalf("bad result: %s %s", result, s)
	}

	// modify some bits

	if Zone_bitmap_get_bit(a, 0) == 0 {
		Zone_bitmap_set_bit(a, 0, 1)
	}
	Zone_bitmap_set_bit(a, 0, 0)
	if Zone_bitmap_get_bit(a, 3) == 0 {
		Zone_bitmap_set_bit(a, 3, 1)
	}
	Zone_bitmap_set_bit(a, 3, 0)
	if Zone_bitmap_get_bit(a, 4) == 1 {
		Zone_bitmap_set_bit(a, 4, 0)
	}
	Zone_bitmap_set_bit(a, 4, 1)
	if Zone_bitmap_get_bit(a, 8) == 1 {
		Zone_bitmap_set_bit(a, 8, 0)
	}
	Zone_bitmap_set_bit(a, 8, 1)
	if Zone_bitmap_get_bit(a, 15) == 0 {
		Zone_bitmap_set_bit(a, 15, 1)
	}
	Zone_bitmap_set_bit(a, 15, 0)
	if Zone_bitmap_get_bit(a, 16) == 0 {
		Zone_bitmap_set_bit(a, 16, 1)
	}
	Zone_bitmap_set_bit(a, 16, 0)

	if Zone_bitmap_get_bit(a, 40) == 0 {
		Zone_bitmap_set_bit(a, 40, 1)
	}
	Zone_bitmap_set_bit(a, 40, 0)
	if Zone_bitmap_get_bit(a, 41) == 1 {
		Zone_bitmap_set_bit(a, 41, 0)
	}
	Zone_bitmap_set_bit(a, 41, 1)
	if Zone_bitmap_get_bit(a, 42) == 0 {
		Zone_bitmap_set_bit(a, 42, 1)
	}
	Zone_bitmap_set_bit(a, 42, 0)
	if Zone_bitmap_get_bit(a, 42) == 1 {
		Zone_bitmap_set_bit(a, 42, 0)
	}
	Zone_bitmap_set_bit(a, 42, 1)
	if Zone_bitmap_get_bit(a, 42) == 0 {
		Zone_bitmap_set_bit(a, 42, 1)
	}
	Zone_bitmap_set_bit(a, 42, 0)
	if Zone_bitmap_get_bit(a, 43) == 0 {
		Zone_bitmap_set_bit(a, 43, 1)
	}
	Zone_bitmap_set_bit(a, 43, 0)
	if Zone_bitmap_get_bit(a, 44) == 0 {
		Zone_bitmap_set_bit(a, 44, 1)
	}
	Zone_bitmap_set_bit(a, 44, 0)
	if Zone_bitmap_get_bit(a, 45) == 0 {
		Zone_bitmap_set_bit(a, 45, 1)
	}
	Zone_bitmap_set_bit(a, 45, 0)
	if Zone_bitmap_get_bit(a, 46) == 1 {
		Zone_bitmap_set_bit(a, 46, 0)
	}
	Zone_bitmap_set_bit(a, 46, 1)
	if Zone_bitmap_get_bit(a, 47) == 1 {
		Zone_bitmap_set_bit(a, 47, 0)
	}
	Zone_bitmap_set_bit(a, 47, 1)
	if Zone_bitmap_get_bit(a, 47) == 0 {
		Zone_bitmap_set_bit(a, 47, 1)
	}
	Zone_bitmap_set_bit(a, 47, 0)
	if Zone_bitmap_get_bit(a, 47) == 1 {
		Zone_bitmap_set_bit(a, 47, 0)
	}
	Zone_bitmap_set_bit(a, 47, 1)

	s = "00001001" + "10000000" + "00000000" + "00000000" + "11111111" + "01000011" + "01100000"

	// check a

	result = result[:0]

	for i := uint64(0); i < uint64(len(a))*8; i++ {
		r := Zone_bitmap_get_bit(a, i)
		switch r {
		case 0:
			result = append(result, '0')
		case 1:
			result = append(result, '1')
		}
	}

	if string(result) != s {
		t.Fatalf("bad result: %s %s", result, s)
	}

}

func Test_allocator_zone_scan_forwards_for_clear_bit_in_single_byte(t *testing.T) {
	var (
		err error
		n   uint64
		res uint64
	)

	samples := []struct {
		s                string
		start_delta_in_b uint64
		expected_res     uint64
	}{
		{"00000001", 0, 1},
		{"00000001", 1, 1},
		{"00000001", 2, 2},
		{"00000001", 3, 3},
		{"00000001", 4, 4},
		{"00000001", 5, 5},
		{"00000001", 6, 6},
		{"00000001", 7, 7},

		{"00000010", 0, 0},
		{"00000010", 1, 2},
		{"00000010", 2, 2},
		{"00000010", 7, 7},

		{"11101110", 0, 0},
		{"11101110", 1, 4},
		{"11101110", 2, 4},
		{"11101110", 3, 4},
		{"11101110", 4, 4},
		{"11101110", 5, ZONE_CLEAR_BIT_NOT_FOUND},
		{"11101110", 6, ZONE_CLEAR_BIT_NOT_FOUND},
		{"11101110", 7, ZONE_CLEAR_BIT_NOT_FOUND},

		{"10010011", 0, 2},
		{"10010011", 1, 2},
		{"10010011", 2, 2},
		{"10010011", 3, 3},
		{"10010011", 4, 5},
		{"10010011", 5, 5},
		{"10010011", 6, 6},
		{"10010011", 7, ZONE_CLEAR_BIT_NOT_FOUND},

		{"00000101", 0, 1},
		{"00000101", 1, 1},
		{"00000101", 2, 3},
		{"00000101", 3, 3},
		{"00000101", 4, 4},
		{"00000101", 5, 5},
		{"00000101", 6, 6},
		{"00000101", 7, 7},

		{"01111111", 0, 7},
		{"01111111", 6, 7},
		{"01111111", 7, 7},

		{"11111111", 0, ZONE_CLEAR_BIT_NOT_FOUND},
		{"11111111", 7, ZONE_CLEAR_BIT_NOT_FOUND},

		{"00000000", 0, 0},
		{"00000000", 7, 7},
	}

	for _, sample := range samples {

		if n, err = strconv.ParseUint(sample.s, 2, 8); err != nil {
			panic("impossible")
		}

		res = zone_scan_forwards_for_clear_bit_in_single_byte(uint8(n), sample.start_delta_in_b)

		if res != sample.expected_res {
			t.Fatalf("failed: sample=%v, res=%d", sample, res)
		}

	}
}

func Test_allocator_zone_scan_backwards_for_clear_bit_in_single_byte(t *testing.T) {
	var (
		err error
		n   uint64
		res uint64
	)

	samples := []struct {
		s                string
		start_delta_in_b uint64
		expected_res     uint64
	}{
		{"00000001", 0, ZONE_CLEAR_BIT_NOT_FOUND},
		{"00000001", 1, 1},
		{"00000001", 2, 2},
		{"00000001", 3, 3},
		{"00000001", 4, 4},
		{"00000001", 5, 5},
		{"00000001", 6, 6},
		{"00000001", 7, 7},

		{"00000010", 0, 0},
		{"00000010", 1, 0},
		{"00000010", 2, 2},
		{"00000010", 7, 7},

		{"11101110", 0, 0},
		{"11101110", 1, 0},
		{"11101110", 2, 0},
		{"11101110", 3, 0},
		{"11101110", 4, 4},
		{"11101110", 5, 4},
		{"11101110", 6, 4},
		{"11101110", 7, 4},

		{"10010011", 0, ZONE_CLEAR_BIT_NOT_FOUND},
		{"10010011", 1, ZONE_CLEAR_BIT_NOT_FOUND},
		{"10010011", 2, 2},
		{"10010011", 3, 3},
		{"10010011", 4, 3},
		{"10010011", 5, 5},
		{"10010011", 6, 6},
		{"10010011", 7, 6},

		{"00000101", 0, ZONE_CLEAR_BIT_NOT_FOUND},
		{"00000101", 1, 1},
		{"00000101", 2, 1},
		{"00000101", 3, 3},
		{"00000101", 4, 4},
		{"00000101", 5, 5},
		{"00000101", 6, 6},
		{"00000101", 7, 7},

		{"01111111", 0, ZONE_CLEAR_BIT_NOT_FOUND},
		{"01111111", 6, ZONE_CLEAR_BIT_NOT_FOUND},
		{"01111111", 7, 7},

		{"11111111", 0, ZONE_CLEAR_BIT_NOT_FOUND},
		{"11111111", 7, ZONE_CLEAR_BIT_NOT_FOUND},

		{"00000000", 0, 0},
		{"00000000", 7, 7},
	}

	for _, sample := range samples {

		if n, err = strconv.ParseUint(sample.s, 2, 8); err != nil {
			panic("impossible")
		}

		res = zone_scan_backwards_for_clear_bit_in_single_byte(uint8(n), sample.start_delta_in_b)

		if res != sample.expected_res {
			t.Fatalf("failed: sample=%v, res=%d", sample, res)
		}

	}
}

func Test_allocator_zone_bitmap_scan_forwards_for_clear_bit(t *testing.T) {
	var (
		bitmap                        []uint8
		bitmap_string                 string
		starting_bit_offset_in_bitmap uint64
		res                           uint64
		expected_res                  uint64
	)

	for k := 0; k < 40; k++ {
		for i := 0; i < 10; i++ {
			n := rand.Intn(256)
			bitmap = append(bitmap, uint8(n))

			bitmap_string = fmt.Sprintf("%08b%s", uint64(n), bitmap_string) // MSB is on left, LSB is on right
		}

		bitmap = append(bitmap, 255)
		bitmap_string = fmt.Sprintf("%08b%s", 255, bitmap_string)
		bitmap = append(bitmap, 255)
		bitmap_string = fmt.Sprintf("%08b%s", 255, bitmap_string)
		bitmap = append(bitmap, 255)
		bitmap_string = fmt.Sprintf("%08b%s", 255, bitmap_string)

		for i := 0; i < 10; i++ {
			n := rand.Intn(256)
			bitmap = append(bitmap, uint8(n))

			bitmap_string = fmt.Sprintf("%08b%s", uint64(n), bitmap_string) // MSB is on left, LSB is on right
		}

		for starting_bit_offset_in_bitmap = 0; starting_bit_offset_in_bitmap < 8*uint64(len(bitmap)); starting_bit_offset_in_bitmap++ {
			res = zone_bitmap_scan_forwards_for_clear_bit(bitmap, starting_bit_offset_in_bitmap)

			for i := starting_bit_offset_in_bitmap; i < uint64(len(bitmap_string)); i++ {
				expected_res = ZONE_CLEAR_BIT_NOT_FOUND
				ii := uint64(len(bitmap_string)) - i - 1
				if bitmap_string[ii] == '0' {
					expected_res = i
					break
				}
			}

			if res != expected_res {
				t.Fatalf("failed: %d %d", res, expected_res)
			}
		}

		//rsql.Println(bitmap)
		//rsql.Println(bitmap_string)
	}
}

func Test_allocator_zone_bitmap_scan_forwards_for_clear_bit_2(t *testing.T) {
	var (
		pos uint64
	)

	bitmap := make([]uint8, 20)

	for i, _ := range bitmap {
		bitmap[i] = 0xff
	}

	pos = 73

	Zone_bitmap_set_bit(bitmap, pos, 0)

	for i := uint64(0); i < uint64(len(bitmap)*8); i++ {
		res := zone_bitmap_scan_forwards_for_clear_bit(bitmap, i)

		switch {
		case i <= pos:
			if res != pos {
				t.Fatalf("fail: %d %d", res, pos)
			}
		case i > pos:
			if res != ZONE_CLEAR_BIT_NOT_FOUND {
				t.Fatalf("fail: %d %d", res, pos)
			}
		}
		//fmt.Println(res, pos)
	}

}

func Test_allocator_zone_bitmap_scan_backwards_for_clear_bit(t *testing.T) {
	var (
		bitmap                        []uint8
		bitmap_string                 string
		starting_bit_offset_in_bitmap uint64
		res                           uint64
		expected_res                  uint64
	)

	for k := 0; k < 40; k++ {
		for i := 0; i < 10; i++ {
			n := rand.Intn(256)
			bitmap = append(bitmap, uint8(n))

			bitmap_string = fmt.Sprintf("%08b%s", uint64(n), bitmap_string) // MSB is on left, LSB is on right
		}

		bitmap = append(bitmap, 255)
		bitmap_string = fmt.Sprintf("%08b%s", 255, bitmap_string)
		bitmap = append(bitmap, 255)
		bitmap_string = fmt.Sprintf("%08b%s", 255, bitmap_string)
		bitmap = append(bitmap, 255)
		bitmap_string = fmt.Sprintf("%08b%s", 255, bitmap_string)

		for i := 0; i < 10; i++ {
			n := rand.Intn(256)
			bitmap = append(bitmap, uint8(n))

			bitmap_string = fmt.Sprintf("%08b%s", uint64(n), bitmap_string) // MSB is on left, LSB is on right
		}

		for starting_bit_offset_in_bitmap = 0; starting_bit_offset_in_bitmap < 8*uint64(len(bitmap)); starting_bit_offset_in_bitmap++ {
			res = zone_bitmap_scan_backwards_for_clear_bit(bitmap, starting_bit_offset_in_bitmap)

			for i := int64(starting_bit_offset_in_bitmap); i >= 0; i-- {
				expected_res = ZONE_CLEAR_BIT_NOT_FOUND
				ii := uint64(len(bitmap_string) - int(i) - 1)
				if bitmap_string[ii] == '0' {
					expected_res = uint64(i)
					break
				}
			}

			if res != expected_res {
				t.Fatalf("failed: %d %d", res, expected_res)
			}
		}

		//rsql.Println(bitmap)
		//rsql.Println(bitmap_string)
	}
}

func Test_allocator_zone_bitmap_scan_backwards_for_clear_bit_2(t *testing.T) {
	var (
		pos uint64
	)

	bitmap := make([]uint8, 20)

	for i, _ := range bitmap {
		bitmap[i] = 0xff
	}

	pos = 73

	Zone_bitmap_set_bit(bitmap, pos, 0)

	for i := uint64(0); i < uint64(len(bitmap)*8); i++ {
		res := zone_bitmap_scan_backwards_for_clear_bit(bitmap, i)

		switch {
		case i < pos:
			if res != ZONE_CLEAR_BIT_NOT_FOUND {
				t.Fatalf("fail: %d %d", res, pos)
			}
		case i >= pos:
			if res != pos {
				t.Fatalf("fail: %d %d", res, pos)
			}
		}
		//fmt.Println(res, pos)
	}

}

func Test_allocator_zone_bitmap_scan_forwards_backwards_for_clear_bit(t *testing.T) {
	var (
		bitmap                        []uint8
		bitmap_size                   int
		starting_bit_offset_in_bitmap uint64
		res                           uint64
		delta                         uint64
		delta_bak                     uint64
	)

	bitmap_size = 7

	samples := []uint64{0, uint64(bitmap_size*8) - 1, 27, 28, 55, 0, uint64(bitmap_size*8) - 1, 27, 28, 55, 0, uint64(bitmap_size*8) - 1, 27, 28, 55, 0, uint64(bitmap_size*8) - 1, 27, 28, 55, 0, uint64(bitmap_size*8) - 1, 27, 28, 55}

	for _, starting_bit_offset_in_bitmap = range samples {
		delta_bak = 0
		bitmap = bitmap[:0]

		for i := 0; i < bitmap_size; i++ {
			n := rand.Intn(256)
			bitmap = append(bitmap, uint8(n))
		}

		//fmt.Printf("start=%d, bitmap=%v\n", starting_bit_offset_in_bitmap, bitmap)

		for {
			res_f := zone_bitmap_scan_forwards_for_clear_bit(bitmap, starting_bit_offset_in_bitmap)
			res_b := zone_bitmap_scan_backwards_for_clear_bit(bitmap, starting_bit_offset_in_bitmap)

			switch {
			case res_f == ZONE_CLEAR_BIT_NOT_FOUND && res_b == ZONE_CLEAR_BIT_NOT_FOUND:
				res = ZONE_CLEAR_BIT_NOT_FOUND
				delta = 0
			case res_f == ZONE_CLEAR_BIT_NOT_FOUND:
				res = res_b
				delta = starting_bit_offset_in_bitmap - res_b
			case res_b == ZONE_CLEAR_BIT_NOT_FOUND:
				res = res_f
				delta = res_f - starting_bit_offset_in_bitmap
			default:
				res = res_f
				delta = res_f - starting_bit_offset_in_bitmap
				if starting_bit_offset_in_bitmap-res_b < res_f-starting_bit_offset_in_bitmap {
					res = res_b
					delta = starting_bit_offset_in_bitmap - res_b
				}
			}

			if res == ZONE_CLEAR_BIT_NOT_FOUND {
				break
			}

			if delta < delta_bak {
				t.Fatalf("delta %d %d", delta, delta_bak)
			}
			delta_bak = delta

			//fmt.Printf("delta: % d\n", int64(res)-int64(starting_bit_offset_in_bitmap))

			Zone_bitmap_set_bit(bitmap, res, 1)
		}

		for _, b := range bitmap {
			if b != 255 {
				t.Fatalf("byte != 0xff: %d", b)
			}
		}
	}
}

func Test_allocator_page_getter_setter(t *testing.T) {
	const (
		pinf_column_array_capacity uint16 = 6253
		pinf_nk_array_capacity     uint16 = 2984

		pinf_column_count          uint16          = 7722
		pinf_column_datatypes_0    rsql.Datatype_t = 9946
		pinf_column_datatypes_1    rsql.Datatype_t = 7744
		pinf_column_datatypes_last rsql.Datatype_t = 15267

		pinf_nk_count           uint16 = 523
		pinf_nk_base_seqno_0    uint16 = 9191
		pinf_nk_base_seqno_1    uint16 = 3322
		pinf_nk_base_seqno_last uint16 = 6354

		pinf_next_rowid    int64 = -14253647589069547
		pinf_next_identity int64 = -83647362574635245

		pinf_root_page_no Page_no_t = 77335244226644784

		pinf_zone_bitmaps_count     uint32 = 35463728
		pinf_zone_bitmap_array_size uint32 = 73664758

		pinf_zone_rootbitmap_array_size uint32 = 64738893
		pinf_zone_rootbitmap_array_0    uint8  = 121
		pinf_zone_rootbitmap_array_1    uint8  = 63
		pinf_zone_rootbitmap_array_last uint8  = 34
	)

	page := &Page{}

	page.Set_pinf_column_array_capacity(pinf_column_array_capacity)
	page.Set_pinf_nk_array_capacity(pinf_nk_array_capacity)
	page.Set_pinf_column_count(pinf_column_count)
	page.Set_pinf_column_datatypes(0, pinf_column_datatypes_0)
	page.Set_pinf_column_datatypes(1, pinf_column_datatypes_1)
	page.Set_pinf_column_datatypes(rsql.SPEC_TABLE_NUMBER_OF_COLUMNS_MAX-1, pinf_column_datatypes_last)
	page.Set_pinf_nk_count(pinf_nk_count)
	page.Set_pinf_nk_base_seqno(0, pinf_nk_base_seqno_0)
	page.Set_pinf_nk_base_seqno(1, pinf_nk_base_seqno_1)
	page.Set_pinf_nk_base_seqno(rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX-1, pinf_nk_base_seqno_last)
	page.Set_pinf_next_rowid(pinf_next_rowid)
	page.Set_pinf_next_identity(pinf_next_identity)
	page.Set_pinf_root_page_no(pinf_root_page_no)
	page.Set_pinf_zone_bitmaps_count(pinf_zone_bitmaps_count)
	page.Set_pinf_zone_bitmap_array_size(pinf_zone_bitmap_array_size)
	page.Set_pinf_zone_rootbitmap_array_size(pinf_zone_rootbitmap_array_size)
	page.Pinf_zone_rootbitmap_array()[0] = pinf_zone_rootbitmap_array_0
	page.Pinf_zone_rootbitmap_array()[1] = pinf_zone_rootbitmap_array_1
	page.Pinf_zone_rootbitmap_array()[PZONE_ROOTBITMAP_ARRAY_SIZE-1] = pinf_zone_rootbitmap_array_last

	if page.Pinf_column_array_capacity() != pinf_column_array_capacity {
		t.Fatal("failed")
	}

	if page.Pinf_nk_array_capacity() != pinf_nk_array_capacity {
		t.Fatal("failed")
	}

	if page.Pinf_column_count() != pinf_column_count {
		t.Fatal("failed")
	}

	if page.Pinf_column_datatypes(0) != pinf_column_datatypes_0 {
		t.Fatal("failed")
	}

	if page.Pinf_column_datatypes(1) != pinf_column_datatypes_1 {
		t.Fatal("failed")
	}

	if page.Pinf_column_datatypes(rsql.SPEC_TABLE_NUMBER_OF_COLUMNS_MAX-1) != pinf_column_datatypes_last {
		t.Fatal("failed")
	}

	if page.Pinf_nk_count() != pinf_nk_count {
		t.Fatal("failed")
	}

	if page.Pinf_nk_base_seqno(0) != pinf_nk_base_seqno_0 {
		t.Fatal("failed")
	}

	if page.Pinf_nk_base_seqno(1) != pinf_nk_base_seqno_1 {
		t.Fatal("failed")
	}

	if page.Pinf_nk_base_seqno(rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX-1) != pinf_nk_base_seqno_last {
		t.Fatal("failed")
	}

	if page.Pinf_next_rowid() != pinf_next_rowid {
		t.Fatal("failed")
	}

	if page.Pinf_next_identity() != pinf_next_identity {
		t.Fatal("failed")
	}

	if page.Pinf_root_page_no() != pinf_root_page_no {
		t.Fatal("failed")
	}

	if page.Pinf_zone_bitmaps_count() != pinf_zone_bitmaps_count {
		t.Fatal("failed")
	}

	if page.Pinf_zone_bitmap_array_size() != pinf_zone_bitmap_array_size {
		t.Fatal("failed")
	}

	if page.Pinf_zone_rootbitmap_array_size() != pinf_zone_rootbitmap_array_size {
		t.Fatal("failed")
	}

	if page.Pinf_zone_rootbitmap_array()[0] != pinf_zone_rootbitmap_array_0 {
		t.Fatal("failed")
	}

	if page.Pinf_zone_rootbitmap_array()[1] != pinf_zone_rootbitmap_array_1 {
		t.Fatal("failed")
	}

	if page.Pinf_zone_rootbitmap_array()[PZONE_ROOTBITMAP_ARRAY_SIZE-1] != pinf_zone_rootbitmap_array_last {
		t.Fatal("failed")
	}

}
