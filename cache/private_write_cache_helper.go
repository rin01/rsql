package cache

import (
	"fmt"
	"log"
	"os"

	"rsql"
)

//=========================================================================
//
//            pin list, lru list, slim list and free list functions
//
//=========================================================================

// insert_first inserts element at beginning of list.
//
func (wcache *Wcache) insert_first(listinfo Dlist_type_t, elem *Page_cache_element) {
	var (
		anchor     *Dlist_anchor
		a_bpos     bpos_t
		a_dlist    *Dlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Dlist
		elem_bpos  bpos_t
		elem_dlist *Dlist
	)

	assert(elem.pce_listinfo == PCE_ILLEGAL_LIST)

	switch listinfo { // if listinfo is not listed below, the function will panic
	case PCE_PIN_LIST:
		anchor = &wcache.wpc_pin_list
		assert(elem.pce_ref_count == 0)
		elem.pce_ref_count = 1

	case PCE_LRU_LIST:
		anchor = &wcache.wpc_lru_list
		assert(elem.pce_ref_count == 0)

	case PCE_SLIM_LIST:
		anchor = &wcache.wpc_slim_list
		assert(elem.pce_ref_count == 0 && elem.pce_dirty == false && elem.pce_ppage_bpos == 0)

	case PCE_FREE_LIST: // when element is put into free list, it must be in a clean state, that is, we fill here all fields with default values.
		anchor = &wcache.wpc_free_list

		// nullify element fields

		elem.pce_hashval = 0
		elem.pce_tblid = 0
		elem.pce_page_no = 0
		// assert(elem.pce_ref_count == 0)
		elem.pce_version = PCE_FREED
		// assert(elem.pce_dirty == false)   // never dirty

		assert(elem.pce_chain.Prev == BPOS_ILLEGAL && elem.pce_chain.Next == BPOS_ILLEGAL)
		assert(elem.pce_draft.Prev == BPOS_ILLEGAL && elem.pce_draft.Next == BPOS_ILLEGAL)
		assert(elem.pce_same_tblid.Prev == BPOS_ILLEGAL && elem.pce_same_tblid.Next == BPOS_ILLEGAL)

		// assert(elem.pce_ppage_bpos == 0)  // no ppage is attached, when in free list
		// assert(elem.pce_logpage_no == -1)

		assert(elem.pce_ref_count == 0 && elem.pce_dirty == false && elem.pce_ppage_bpos == 0 && elem.pce_logpage_no == -1)
	}

	anchor.Count++
	a_bpos = 0
	a_dlist = &anchor.Dlist

	elem_bpos = elem.pce_bpos
	elem_dlist = &elem.pce_enlistment
	assert(elem_dlist.Prev == BPOS_ILLEGAL && elem_dlist.Next == BPOS_ILLEGAL)

	b_bpos = a_dlist.Next
	if b = wcache.w_elem(b_bpos); b != nil {
		b_dlist = &b.pce_enlistment
	} else {
		b_dlist = a_dlist
	}

	elem_dlist.Prev = a_bpos
	a_dlist.Next = elem_bpos

	elem_dlist.Next = b_bpos
	b_dlist.Prev = elem_bpos

	elem.pce_listinfo = listinfo
}

// insert_last inserts element at end of list.
//
func (wcache *Wcache) insert_last(listinfo Dlist_type_t, elem *Page_cache_element) {
	var (
		anchor     *Dlist_anchor
		b_bpos     bpos_t
		b_dlist    *Dlist
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Dlist
		elem_bpos  bpos_t
		elem_dlist *Dlist
	)

	assert(elem.pce_listinfo == PCE_ILLEGAL_LIST)

	switch listinfo { // if listinfo is not listed below, the function will panic
	case PCE_FREE_LIST: // when element is put into free list, it must be in a clean state, that is, we fill here all fields with default values.
		anchor = &wcache.wpc_free_list

		// nullify element fields

		elem.pce_hashval = 0
		elem.pce_tblid = 0
		elem.pce_page_no = 0
		// assert(elem.pce_ref_count == 0)
		elem.pce_version = PCE_FREED
		// assert(elem.pce_dirty == false)   // never dirty

		assert(elem.pce_chain.Prev == BPOS_ILLEGAL && elem.pce_chain.Next == BPOS_ILLEGAL)
		assert(elem.pce_draft.Prev == BPOS_ILLEGAL && elem.pce_draft.Next == BPOS_ILLEGAL)
		assert(elem.pce_same_tblid.Prev == BPOS_ILLEGAL && elem.pce_same_tblid.Next == BPOS_ILLEGAL)

		// assert(elem.pce_ppage_bpos == 0)  // no ppage is attached, when in free list
		// assert(elem.pce_logpage_no == -1)

		assert(elem.pce_ref_count == 0 && elem.pce_dirty == false && elem.pce_ppage_bpos == 0 && elem.pce_logpage_no == -1)
	}

	anchor.Count++
	b_bpos = 0
	b_dlist = &anchor.Dlist

	elem_bpos = elem.pce_bpos
	elem_dlist = &elem.pce_enlistment
	assert(elem_dlist.Prev == BPOS_ILLEGAL && elem_dlist.Next == BPOS_ILLEGAL)

	a_bpos = b_dlist.Prev
	if a = wcache.w_elem(a_bpos); a != nil {
		a_dlist = &a.pce_enlistment
	} else {
		a_dlist = b_dlist
	}

	elem_dlist.Prev = a_bpos
	a_dlist.Next = elem_bpos

	elem_dlist.Next = b_bpos
	b_dlist.Prev = elem_bpos

	elem.pce_listinfo = listinfo
}

// remove removes element from list.
//
func (wcache *Wcache) remove(listinfo Dlist_type_t, elem *Page_cache_element) {
	var (
		anchor     *Dlist_anchor
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Dlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Dlist
		elem_dlist *Dlist
	)

	switch listinfo { // if listinfo is not listed below, the function will panic
	case PCE_PIN_LIST:
		anchor = &wcache.wpc_pin_list
		assert(elem.pce_ref_count == 1)
		elem.pce_ref_count = 0

	case PCE_LRU_LIST:
		anchor = &wcache.wpc_lru_list
		assert(elem.pce_ref_count == 0)

	case PCE_SLIM_LIST:
		anchor = &wcache.wpc_slim_list
		assert(elem.pce_ref_count == 0)
	}

	anchor.Count--
	elem_dlist = &elem.pce_enlistment

	a_bpos = elem_dlist.Prev
	if a = wcache.w_elem(a_bpos); a != nil {
		a_dlist = &a.pce_enlistment
	} else {
		a_dlist = &anchor.Dlist
	}

	b_bpos = elem_dlist.Next
	if b = wcache.w_elem(b_bpos); b != nil {
		b_dlist = &b.pce_enlistment
	} else {
		b_dlist = &anchor.Dlist
	}

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	assert(elem.pce_listinfo == listinfo)
	elem.pce_listinfo = PCE_ILLEGAL_LIST
}

// pop_last removes last element from list.
// Returns nil if list is empty.
//
func (wcache *Wcache) pop_last(listinfo Dlist_type_t) *Page_cache_element {
	var (
		anchor     *Dlist_anchor
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Dlist
		b_bpos     bpos_t
		b_dlist    *Dlist
		elem       *Page_cache_element
		elem_bpos  bpos_t
		elem_dlist *Dlist
	)

	switch listinfo { // if listinfo is not listed below, the function will panic
	case PCE_LRU_LIST:
		anchor = &wcache.wpc_lru_list

	case PCE_SLIM_LIST:
		anchor = &wcache.wpc_slim_list

	case PCE_FREE_LIST:
		anchor = &wcache.wpc_free_list
	}

	anchor.Count--
	elem_bpos = anchor.Dlist.Prev
	if elem = wcache.w_elem(elem_bpos); elem == nil {
		return nil
	}
	elem_dlist = &elem.pce_enlistment

	a_bpos = elem.pce_enlistment.Prev
	if a = wcache.w_elem(a_bpos); a != nil {
		a_dlist = &a.pce_enlistment
	} else {
		a_dlist = &anchor.Dlist
	}

	b_bpos = 0
	b_dlist = &anchor.Dlist

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	assert(elem.pce_ref_count == 0)

	assert(elem.pce_listinfo == listinfo)
	elem.pce_listinfo = PCE_ILLEGAL_LIST

	return elem
}

//=========================================================================
//
//                         hashtable functions
//
//=========================================================================

func (wcache *Wcache) insert_first_in_hashtable(elem *Page_cache_element) {
	var (
		a_bpos     bpos_t
		a_dlist    *Hlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Hlist
		elem_bpos  bpos_t
		elem_dlist *Hlist
	)

	a_bpos = 0
	a_dlist = &wcache.wpc_hashtable[elem.pce_hashval%wcache.wpc_hashtable_size]

	elem_bpos = elem.pce_bpos
	elem_dlist = &elem.pce_chain
	assert(elem_dlist.Prev == BPOS_ILLEGAL && elem_dlist.Next == BPOS_ILLEGAL)

	b_bpos = a_dlist.Next
	if b = wcache.w_elem(b_bpos); b != nil {
		b_dlist = &b.pce_chain
	} else {
		b_dlist = a_dlist
	}

	elem_dlist.Prev = a_bpos
	a_dlist.Next = elem_bpos

	elem_dlist.Next = b_bpos
	b_dlist.Prev = elem_bpos

	wcache.wpc_hashtable_count++
}

func (wcache *Wcache) remove_from_hashtable(elem *Page_cache_element) {
	var (
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Hlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Hlist
		elem_dlist *Hlist
	)

	elem_dlist = &elem.pce_chain

	a_bpos = elem_dlist.Prev
	if a_bpos != 0 {
		a = wcache.w_elem(a_bpos)
		a_dlist = &a.pce_chain
	} else {
		a_dlist = &wcache.wpc_hashtable[elem.pce_hashval%wcache.wpc_hashtable_size]
	}

	b_bpos = elem_dlist.Next
	if b_bpos != 0 {
		b = wcache.w_elem(b_bpos)
		b_dlist = &b.pce_chain
	} else {
		b_dlist = a_dlist
		if a_bpos != 0 {
			b_dlist = &wcache.wpc_hashtable[elem.pce_hashval%wcache.wpc_hashtable_size]
		}
	}

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	wcache.wpc_hashtable_count--
}

// only for debugging.
//
func (wcache *Wcache) Debug_check_hashtable() {
	var (
		n    int
		elem *Page_cache_element
	)

	for _, anchor := range wcache.wpc_hashtable {
		if anchor.Next == 0 {
			assert(anchor.Prev == 0)
			continue
		}

		for bpos := anchor.Next; bpos != 0; bpos = elem.pce_chain.Next {
			elem = wcache.w_elem(bpos)
			n++
		}
	}

	assert(n == wcache.wpc_hashtable_count)
}

//=========================================================================
//
//                     elem.pce_same_tblid list functions
//
//=========================================================================

// insert_first_in_same_tblid_maplist inserts element at beginning of list.
//
func (wcache *Wcache) insert_first_in_same_tblid_maplist(elem *Page_cache_element) {
	var (
		anchor     *Tlist_anchor
		a_bpos     bpos_t
		a_dlist    *Tlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Tlist
		elem_bpos  bpos_t
		elem_dlist *Tlist
		ok         bool
	)

	assert(elem.pce_version == PCE_DRAFT) // only PCE_DRAFT elements are inserted, but they will become PCE_TRANSITORY when statement terminates

	if anchor, ok = wcache.wpc_same_tblid_map[elem.pce_tblid]; ok == false { // create anchor if not exists
		anchor = &Tlist_anchor{}
		wcache.wpc_same_tblid_map[elem.pce_tblid] = anchor

		if elem.pce_tblid >= 0 { // update table count
			wcache.wpc_permanent_tables_count++
		} else {
			wcache.wpc_temporary_tables_count++
		}
	}

	anchor.Count++
	a_bpos = 0
	a_dlist = &anchor.Tlist

	elem_bpos = elem.pce_bpos
	elem_dlist = &elem.pce_same_tblid
	assert(elem_dlist.Prev == BPOS_ILLEGAL && elem_dlist.Next == BPOS_ILLEGAL)

	b_bpos = a_dlist.Next
	if b = wcache.w_elem(b_bpos); b != nil {
		b_dlist = &b.pce_same_tblid
	} else {
		b_dlist = a_dlist
	}

	elem_dlist.Prev = a_bpos
	a_dlist.Next = elem_bpos

	elem_dlist.Next = b_bpos
	b_dlist.Prev = elem_bpos
}

// remove_from_same_tblid_maplist removes element from list.
//
func (wcache *Wcache) remove_from_same_tblid_maplist(elem *Page_cache_element) {
	var (
		anchor     *Tlist_anchor
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Tlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Tlist
		elem_dlist *Tlist
	)

	anchor = wcache.wpc_same_tblid_map[elem.pce_tblid]
	assert(anchor != nil)

	anchor.Count--
	elem_dlist = &elem.pce_same_tblid

	a_bpos = elem_dlist.Prev
	if a = wcache.w_elem(a_bpos); a != nil {
		a_dlist = &a.pce_same_tblid
	} else {
		a_dlist = &anchor.Tlist
	}

	b_bpos = elem_dlist.Next
	if b = wcache.w_elem(b_bpos); b != nil {
		b_dlist = &b.pce_same_tblid
	} else {
		b_dlist = &anchor.Tlist
	}

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	if anchor.Count == 0 { // if anchor is empty, remove it
		assert(anchor.Prev == 0 && anchor.Next == 0)
		delete(wcache.wpc_same_tblid_map, elem.pce_tblid)

		if elem.pce_tblid >= 0 { // update table count
			wcache.wpc_permanent_tables_count--
		} else {
			wcache.wpc_temporary_tables_count--
		}
	}
}

//=========================================================================
//
//                          draft list functions
//
//=========================================================================

// insert_first_in_draft_list inserts element at beginning of list.
//
func (wcache *Wcache) insert_first_in_draft_list(elem *Page_cache_element) {
	var (
		anchor     *Rlist_anchor
		a_bpos     bpos_t
		a_dlist    *Rlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Rlist
		elem_bpos  bpos_t
		elem_dlist *Rlist
	)

	assert(elem.pce_version == PCE_DRAFT)
	anchor = &wcache.wpc_draft_list

	anchor.Count++
	a_bpos = 0
	a_dlist = &anchor.Rlist

	elem_bpos = elem.pce_bpos
	elem_dlist = &elem.pce_draft
	assert(elem_dlist.Prev == BPOS_ILLEGAL && elem_dlist.Next == BPOS_ILLEGAL)

	b_bpos = a_dlist.Next
	if b = wcache.w_elem(b_bpos); b != nil {
		b_dlist = &b.pce_draft
	} else {
		b_dlist = a_dlist
	}

	elem_dlist.Prev = a_bpos
	a_dlist.Next = elem_bpos

	elem_dlist.Next = b_bpos
	b_dlist.Prev = elem_bpos
}

// remove_from_draft_list removes element from list.
//
func (wcache *Wcache) remove_from_draft_list(elem *Page_cache_element) {
	var (
		anchor     *Rlist_anchor
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Rlist
		b          *Page_cache_element
		b_bpos     bpos_t
		b_dlist    *Rlist
		elem_dlist *Rlist
	)

	assert(elem.pce_version == PCE_DRAFT)

	anchor = &wcache.wpc_draft_list

	anchor.Count--
	elem_dlist = &elem.pce_draft

	a_bpos = elem_dlist.Prev
	if a = wcache.w_elem(a_bpos); a != nil {
		a_dlist = &a.pce_draft
	} else {
		a_dlist = &anchor.Rlist
	}

	b_bpos = elem_dlist.Next
	if b = wcache.w_elem(b_bpos); b != nil {
		b_dlist = &b.pce_draft
	} else {
		b_dlist = &anchor.Rlist
	}

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL
}

// pop_last_from_draft_list removes last element from list.
// Returns nil if list is empty.
//
func (wcache *Wcache) pop_last_from_draft_list() *Page_cache_element {
	var (
		anchor     *Rlist_anchor
		a          *Page_cache_element
		a_bpos     bpos_t
		a_dlist    *Rlist
		b_bpos     bpos_t
		b_dlist    *Rlist
		elem       *Page_cache_element
		elem_bpos  bpos_t
		elem_dlist *Rlist
	)

	anchor = &wcache.wpc_draft_list

	anchor.Count--
	elem_bpos = anchor.Rlist.Prev
	if elem = wcache.w_elem(elem_bpos); elem == nil {
		return nil
	}
	elem_dlist = &elem.pce_draft

	a_bpos = elem.pce_draft.Prev
	if a = wcache.w_elem(a_bpos); a != nil {
		a_dlist = &a.pce_draft
	} else {
		a_dlist = &anchor.Rlist
	}

	b_bpos = 0
	b_dlist = &anchor.Rlist

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	elem_dlist.Prev = BPOS_ILLEGAL // nullify element pointers
	elem_dlist.Next = BPOS_ILLEGAL

	assert(elem.pce_version == PCE_DRAFT)

	return elem
}

//=========================================================================
//
//                           hanger functions
//
//=========================================================================

// hanger_insert_first inserts hanger at beginning of list.
//
func (wcache *Wcache) hanger_insert_first(listinfo Hanger_dlist_type_t, hanger *Ppage_hanger) {
	var (
		anchor       *Dlist_anchor
		a_bpos       bpos_t
		a_dlist      *Dlist
		b            *Ppage_hanger
		b_bpos       bpos_t
		b_dlist      *Dlist
		hanger_bpos  bpos_t
		hanger_dlist *Dlist
		ppage        *Page
	)

	switch listinfo {
	case HANGERS_WITH_BLANK_PPAGE_LIST:
		anchor = &wcache.wpc_hangers_with_blank_ppage
		assert(hanger.pph_ppage_bpos != 0)

		if XDEBUG {
			ppage = wcache.hanger_ppage(hanger)
			ppage.Fill_0xff() // fill with 0xff
		}

	case HANGERS_WITH_NO_PPAGE_LIST:
		anchor = &wcache.wpc_hangers_with_no_ppage
		assert(hanger.pph_ppage_bpos == 0)
	}

	anchor.Count++
	a_bpos = 0
	a_dlist = &anchor.Dlist

	hanger_bpos = hanger.pph_bpos
	hanger_dlist = &hanger.pph_dlist

	b_bpos = a_dlist.Next
	if b = wcache.w_hanger(b_bpos); b != nil {
		b_dlist = &b.pph_dlist
	} else {
		b_dlist = a_dlist
	}

	hanger_dlist.Prev = a_bpos
	a_dlist.Next = hanger_bpos

	hanger_dlist.Next = b_bpos
	b_dlist.Prev = hanger_bpos
}

// hanger_pop_last removes last hanger from list.
// Returns nil if list is empty.
//
// If listinfo == HANGERS_WITH_BLANK_PPAGE_LIST, the attached ppage has been cleared.
//
func (wcache *Wcache) hanger_pop_last(listinfo Hanger_dlist_type_t) *Ppage_hanger {
	var (
		anchor       *Dlist_anchor
		a            *Ppage_hanger
		a_bpos       bpos_t
		a_dlist      *Dlist
		b_bpos       bpos_t
		b_dlist      *Dlist
		hanger       *Ppage_hanger
		hanger_bpos  bpos_t
		hanger_dlist *Dlist
		ppage        *Page
	)

	switch listinfo {
	case HANGERS_WITH_BLANK_PPAGE_LIST:
		anchor = &wcache.wpc_hangers_with_blank_ppage

	case HANGERS_WITH_NO_PPAGE_LIST:
		anchor = &wcache.wpc_hangers_with_no_ppage
	}

	anchor.Count--
	hanger_bpos = anchor.Dlist.Prev
	if hanger = wcache.w_hanger(hanger_bpos); hanger == nil {
		return nil
	}
	hanger_dlist = &hanger.pph_dlist

	a_bpos = hanger.pph_dlist.Prev
	if a = wcache.w_hanger(a_bpos); a != nil {
		a_dlist = &a.pph_dlist
	} else {
		a_dlist = &anchor.Dlist
	}

	b_bpos = 0
	b_dlist = &anchor.Dlist

	a_dlist.Next = b_bpos // fusion a and b together
	b_dlist.Prev = a_bpos

	hanger_dlist.Prev = BPOS_ILLEGAL
	hanger_dlist.Next = BPOS_ILLEGAL

	if listinfo == HANGERS_WITH_BLANK_PPAGE_LIST {
		ppage = wcache.hanger_ppage(hanger) // ppage is cleared
		ppage.Clear()
	}

	return hanger
}

//=========================================================================
//
//                             misc functions
//
//=========================================================================

// w_elem returns element identified by elem_bpos, from wcache.
//
// If elem_bpos == 0, returns nil.
//
func (wcache *Wcache) w_elem(elem_bpos bpos_t) *Page_cache_element {
	var (
		i    uint64
		j    uint64
		elem *Page_cache_element
	)

	if elem_bpos == 0 {
		return nil
	}

	elem_bpos -= BPOS_BASE

	i = uint64(elem_bpos) >> 32
	j = uint64(elem_bpos) & 0x00000000ffffffff

	elem = &wcache.wpc_element_array[i][j]

	return elem
}

// w_ppage returns ppage identified by elem.pce_ppage_bpos, from wcache.
//
// elem must have a ppage attached, that is: elem.pce_ppage_bpos != 0.
//
func (wcache *Wcache) w_ppage(elem *Page_cache_element) *Page {
	var (
		i          uint64
		j          uint64
		ppage_bpos bpos_t
		ppage      *Page
	)

	assert(elem.pce_version&(PCE_DRAFT|PCE_TRANSITORY) != 0 && elem.pce_listinfo&(PCE_PIN_LIST|PCE_LRU_LIST|PCE_ILLEGAL_LIST) != 0 && elem.pce_ppage_bpos != 0)

	ppage_bpos = elem.pce_ppage_bpos - BPOS_BASE

	i = uint64(ppage_bpos) >> 32
	j = uint64(ppage_bpos) & 0x00000000ffffffff

	ppage = &wcache.wpc_ppage_array[i][j]

	return ppage
}

// PUBLIC API: ensure that the returned ppage is DRAFT and dirty, so that it can be written to.
//
// It is exactly the same as w_ppage(), except that it assert the element is DRAFT and dirty.
// This function just ensure that you can write to the page and the changes won't be lost.
//
// You will use it after calling a function that returns a DRAFT page:
//           Get_clean_new_DRAFT_info_page()
//           Require_DRAFT_page_and_pin_it()
//           DRAFTIFY_pinned_page()
//           Fetch_BLANK_DRAFT_page_and_pin_it()
//
// You could call Ppage() instead, if you want to only read the page.
// But if you call one of the functions above, you certainly want to write in the page.
//
func (wcache *Wcache) Ppage_DRAFT_for_write(elem *Page_cache_element) *Page {

	if elem.pce_listinfo != PCE_PIN_LIST || elem.pce_version != PCE_DRAFT || elem.pce_dirty == false {
		panic(fmt.Sprintf("if you write in a page, it must be dirty. Page_no=%d", elem.pce_page_no))
	}

	return wcache.w_ppage(elem)
}

// w_hanger returns hanger identified by hanger_bpos, from wcache.
//
// If hanger_bpos == 0, returns nil.
//
func (wcache *Wcache) w_hanger(hanger_bpos bpos_t) *Ppage_hanger {
	var (
		i      uint64
		j      uint64
		hanger *Ppage_hanger
	)

	if hanger_bpos == 0 {
		return nil
	}

	hanger_bpos -= BPOS_BASE

	i = uint64(hanger_bpos) >> 32
	j = uint64(hanger_bpos) & 0x00000000ffffffff

	hanger = &wcache.wpc_hangers_array[i][j]

	return hanger
}

// hanger_ppage returns ppage identified by hanger.pph_ppage_bpos, from wcache.
//
// hanger must have a ppage attached, that is: hanger.pph_ppage_bpos != 0.
//
func (wcache *Wcache) hanger_ppage(hanger *Ppage_hanger) *Page {
	var (
		i          uint64
		j          uint64
		ppage_bpos bpos_t
		ppage      *Page
	)

	assert(hanger.pph_ppage_bpos != 0)

	ppage_bpos = hanger.pph_ppage_bpos - BPOS_BASE

	i = uint64(ppage_bpos) >> 32
	j = uint64(ppage_bpos) & 0x00000000ffffffff

	ppage = &wcache.wpc_ppage_array[i][j]

	return ppage
}

// for debugging only.
// Prints list of wcache.
//
func (wcache *Wcache) debug_print(listinfo Dlist_type_t) {
	var (
		anchor    *Dlist_anchor
		elem_bpos bpos_t
		elem      *Page_cache_element
	)

	switch listinfo {
	case PCE_PIN_LIST:
		anchor = &wcache.wpc_pin_list
		fmt.Printf("---Pin list (count: %d) ---\n", anchor.Count)

	case PCE_LRU_LIST:
		anchor = &wcache.wpc_lru_list
		fmt.Printf("---Lru list (count: %d) ---\n", anchor.Count)

	case PCE_SLIM_LIST:
		anchor = &wcache.wpc_slim_list
		fmt.Printf("---Slim list (count: %d) ---\n", anchor.Count)

	case PCE_FREE_LIST:
		anchor = &wcache.wpc_free_list
		fmt.Printf("---Free list (count: %d) ---\n", anchor.Count)
	}

	for elem_bpos = anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_enlistment.Next {
		elem = wcache.w_elem(elem_bpos)
		assert(elem.pce_version != PCE_PUBLIC)

		fmt.Printf("pce bpos=%d  %d/%d  ref_count=%d\n", elem.pce_bpos, elem.pce_tblid, elem.pce_page_no, elem.pce_ref_count)
		if listinfo&(PCE_PIN_LIST|PCE_LRU_LIST) != 0 {
			pp := wcache.ppage_for_debug(elem)
			Print_ppage(pp)
		}
	}

}

// for debugging only.
//
func Print_ppage(ppage *Page) {
	var sum uint64

	for _, b := range ppage.Pg_canvas {
		sum += uint64(b)
	}

	rsql.Printf("%d     %d.%d.[%d sum:%d].%d %3d  <%d,%d>\n", get_int64(&ppage.Pg_canvas, 0), sum, ppage.Pg_dbid(), ppage.Pg_schid(), ppage.Pg_base_gtblid(), ppage.Pg_tblid(), ppage.Pg_no(), ppage.Pg_prev_no(), ppage.Pg_next_no())

}

// for debugging only.
//
func (wcache *Wcache) debug_print_draft_list() {
	var (
		anchor    *Rlist_anchor
		elem_bpos bpos_t
		elem      *Page_cache_element
	)

	anchor = &wcache.wpc_draft_list
	fmt.Printf("---Draft list (count: %d) ---\n", anchor.Count)

	for elem_bpos = anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_enlistment.Next {
		elem = wcache.w_elem(elem_bpos)
		fmt.Printf("pce: %d\n", elem.pce_bpos)
	}

}

//=========================================================================
//
//                             log functions
//
//=========================================================================

// read_ppage_from_logfile_into_buffer reads a ppage from logfile corresponding to elem in slim list, into a buffer.
//
func (wcache *Wcache) read_ppage_from_logfile_into_buffer(elem *Page_cache_element, ppage_buffer *Page) {
	var (
		err            error
		journal        *Journal
		logfile        *os.File
		logpage_no     Logpage_no_t
		logfile_offset int64
		n              int
	)

	assert(elem.pce_tblid >= 0) // This function is only used during COMMIT, and flashtables pages are never stored in main logfile.
	assert(elem.pce_version != PCE_PUBLIC)
	assert(elem.pce_listinfo == PCE_SLIM_LIST) // this function is only used with elem in slim list, that's why PCE_PIN_LIST and PCE_LRU_LIST are not listed here

	journal = wcache.wpc_journal
	logfile = journal.jr_file
	assert(logfile != nil) // if we read from logfile, it must have been written to before, and so must have been opened

	logpage_no = elem.pce_logpage_no
	assert(logpage_no > 0) // log page 0 is only used to store status data for the logfile. It is never used to store an element ppage.
	logfile_offset = int64(logpage_no * PAGE_SIZE)

	assert(ppage_buffer != nil)

	if n, err = logfile.ReadAt(ppage_buffer.Pg_canvas[:], logfile_offset); err != nil {
		log.Panicf("FATAL ERROR, ReadAt() failed for logfile \"%s\": %v", journal.jr_path, err)
	}

	assert(n == PAGE_SIZE)
}

// read_ppage_from_logfile reads a ppage from logfile into elem, which must be a pinned Page_cache_element.
//
func (wcache *Wcache) read_ppage_from_logfile(elem *Page_cache_element) {
	var (
		err            error
		journal        *Journal
		logfile        *os.File
		logpage_no     Logpage_no_t
		logfile_offset int64
		ppage          *Page
		n              int
	)

	assert(elem.pce_version != PCE_PUBLIC)
	assert(elem.pce_listinfo == PCE_PIN_LIST)

	journal = wcache.wpc_journal
	if elem.pce_tblid < 0 {
		journal = wcache.get_journal_for(elem.pce_tblid)
	}
	logfile = journal.jr_file
	assert(logfile != nil) // if we read from logfile, it must have been written to before, and so must have been opened

	logpage_no = elem.pce_logpage_no
	assert(logpage_no > 0) // log page 0 is only used to store status data for the logfile. It is never used to store an element ppage.
	logfile_offset = int64(logpage_no * PAGE_SIZE)

	ppage = wcache.w_ppage(elem)

	if n, err = logfile.ReadAt(ppage.Pg_canvas[:], logfile_offset); err != nil {
		log.Panicf("FATAL ERROR, ReadAt() failed for logfile \"%s\": %v", journal.jr_path, err)
	}
	assert(n == PAGE_SIZE)
}

// flush_ppage_to_logfile flushes a ppage content to log file.
// This function is used when the page cache element is moved e.g. from lru list to slim list, before discarding the ppage.
//
// elem must be in lru list, or in no list (PCE_ILLEGAL_LIST)   (note: elements in pin list are never flushed).
//
// elem dirty flag will be set to false.
//
func (wcache *Wcache) flush_ppage_to_logfile(elem *Page_cache_element) {
	var (
		err            error
		journal        *Journal
		logfile        *os.File
		logpage_no     Logpage_no_t
		logfile_offset int64
		ppage          *Page
		n              int
	)

	assert(elem.pce_version != PCE_PUBLIC)
	assert(elem.pce_listinfo&(PCE_LRU_LIST|PCE_ILLEGAL_LIST) != 0)

	//==== logfile must already exist ====

	journal = wcache.wpc_journal
	if elem.pce_tblid < 0 {
		journal = wcache.get_journal_for(elem.pce_tblid)
	}
	logfile = journal.jr_file

	//==== if elem.pce_logpage_no < 0, assign a logpage to the element ====

	if elem.pce_logpage_no < 0 {
		elem.pce_logpage_no = Logpage_no_t(journal.jr_logfile_ppage_count)

		journal.jr_logfile_ppage_count++
	}

	//==== write ppage into logfile at file_offset ====

	logpage_no = elem.pce_logpage_no
	assert(logpage_no > 0) // no elem ppage is at logpage_no==0, as it is for logfile status.
	logfile_offset = int64(logpage_no * PAGE_SIZE)

	ppage = wcache.w_ppage(elem)

	if n, err = logfile.WriteAt(ppage.Pg_canvas[:], logfile_offset); err != nil {
		log.Panicf("FATAL ERROR, WriteAt() failed for logfile \"%s\": %v", journal.jr_path, err)
	}
	assert(n == PAGE_SIZE)

	//==== set dirty flag to false ====

	elem.pce_dirty = false // page is no more dirty
}

// erase_logpage_in_logfile discards a page in log file.
//
// This function is used by wcache.Transform_all_DRAFT_elements_to_TRANSITORY().
//
// A TRANSITORY page must be discarded if overriden by a DRAFT page, but if the element has a log page reserved, we must also discard this log page.
//
// To do this, we write 0 at tblid position in file page, because tblid == 0 is an invalid tblid.
// In fact, we write a page full of 0 in log file. TODO I can also only write 0 at tblid position instead of a full page of zero, but I am not sure there is a gain in performance. I must test it.
//
// This way, if the log must be replayed on server start because a crash occured, the function that transfers all log pages to table files will just ignore such log pages.
//
func (wcache *Wcache) erase_logpage_in_logfile(tblid Tblid_t, logpage_no Logpage_no_t) {
	var (
		err            error
		journal        *Journal
		logfile        *os.File
		logfile_offset int64
		n              int
	)

	journal = wcache.wpc_journal
	if tblid < 0 { // in fact, for flashtables, we could just do nothing and return, as their discarded logpages are never read again.
		return // journal = wcache.get_journal_for(tblid) // flashtables don't need freed logpages to be filled with 0s
	}
	logfile = journal.jr_file
	assert(logfile != nil) // logfile must be open

	assert(logpage_no > 0) // logpage 0 is never discarded, as it contains logfile status
	logfile_offset = int64(logpage_no * PAGE_SIZE)

	//==== write G_PPAGE_CLEAR into logfile at file_offset ====

	if n, err = logfile.WriteAt(G_PPAGE_CLEAR[:], logfile_offset); err != nil { // writes PAGE_SIZE bytes. G_PPAGE_CLEAR is a static page full of 0.
		log.Panicf("FATAL ERROR, WriteAt() failed for logfile \"%s\": %v", journal.jr_path, err)
	}
	assert(n == PAGE_SIZE)
}

// logfile_status writes log status in first bytes of first page in logfile, as well as journal.jr_logfile_ppage_count.
//
//    log_status may be :
//         - LOG_STATUS_CLEAR
//         - LOG_STATUS_READY_FOR_TRANSFER
//
func (journal *Journal) logfile_status_and_page_count(log_status Log_status_t) {
	var (
		err                 error
		logfile             *os.File
		logfile_ppage_count uint64
		logfile_sector      Logfile_sector // array of LOGFILE_SECTOR_SIZE bytes. Contains status, page count, page count of last committed transaction.
	)

	logfile = journal.jr_file
	assert(logfile != nil) // logfile must be open

	//==== write log status into logfile ====

	logfile_ppage_count = journal.jr_logfile_ppage_count // number of log pages used in logfile by the transaction. But some of them can have been discarded.
	logfile_sector.Set_plog_status(log_status)

	switch log_status {
	case LOG_STATUS_READY_FOR_TRANSFER:
		logfile_sector.Set_plog_count_bak(0)
		logfile_sector.Set_plog_count(logfile_ppage_count)

	case LOG_STATUS_CLEAR:
		logfile_sector.Set_plog_count_bak(logfile_ppage_count)
		logfile_sector.Set_plog_count(1)

		journal.jr_logfile_ppage_count = 1 // reinit logfile
		journal.jr_logfile_freed_pages = journal.jr_logfile_freed_pages[:0]
	}

	if err = logfile_sector.Save(logfile); err != nil { // writes log status and logfile_ppage_count at beginning of log file.
		log.Panicf("FATAL ERROR, WriteAt() failed for logfile \"%s\": %v", journal.jr_path, err)
	}
}

func (journal *Journal) logfile_sync() {
	var (
		err     error
		logfile *os.File
	)

	logfile = journal.jr_file
	assert(logfile != nil) // logfile must be open

	//==== sync logfile ====

	if err = logfile.Sync(); err != nil {
		log.Panicf("FATAL ERROR, Sync() failed for logfile \"%s\": %v", journal.jr_path, err)
	}
}

func (journal *Journal) logfile_seek(offset int64) {
	var (
		err     error
		logfile *os.File
	)

	logfile = journal.jr_file
	assert(logfile != nil) // logfile must be open

	//==== seek logfile ====

	if _, err = logfile.Seek(offset, os.SEEK_SET); err != nil {
		log.Panicf("FATAL ERROR, Seek() failed for logfile \"%s\": %v", journal.jr_path, err)
	}
}

func (journal *Journal) logfile_read_exact(b []uint8) {
	var (
		err            error
		logfile        *os.File
		count          int
		n              int
		len_b_original int
	)

	logfile = journal.jr_file
	assert(logfile != nil) // logfile must be open

	//==== read logfile ====

	len_b_original = len(b)

	for len(b) > 0 { // loop in case partial read occurs
		if count, err = logfile.Read(b); err != nil {
			log.Panicf("FATAL ERROR, Read() failed for logfile \"%s\": %v", journal.jr_path, err)
		}

		n += count
		b = b[count:]
	}

	assert(n == len_b_original)
}

//=========================================================================
//
//             element array and hashtable resizing functions
//
//=========================================================================

// resize_hashtable resizes write cache hashtable.
//
// As more and more pages are modified in a transaction, more Page_cache_elems elements are created to track these pages.
// And the hashtable must be enlarged to track these elements.
//
func (wcache *Wcache) resize_hashtable(new_hashtable_size uint64) {
	var (
		i                  uint64
		new_hashtable      []Hlist
		old_hashtable      []Hlist
		old_hashtable_size uint64
		elem               *Page_cache_element
		old_bucket         *Hlist
		elem_bpos          bpos_t
		elem_bpos_next     bpos_t
		elem_count         int
	)

	// create new hashtable

	if (new_hashtable_size & 0x0001) == 0 {
		new_hashtable_size++ // better if it is odd
	}

	new_hashtable = make([]Hlist, new_hashtable_size)

	// put all elements from the old hashtable into the new hashtable

	old_hashtable = wcache.wpc_hashtable
	old_hashtable_size = wcache.wpc_hashtable_size

	wcache.wpc_hashtable = new_hashtable // the wcache has a new hashtable. We must fill it, now.
	wcache.wpc_hashtable_size = new_hashtable_size
	wcache.wpc_hashtable_count = 0

	for i = 0; i < old_hashtable_size; i++ {
		old_bucket = &old_hashtable[i] // for each bucket of the old hashtable.

		elem_bpos = old_bucket.Next

		for elem_bpos != 0 { // for each Page_cache_element in this bucket chain
			elem = wcache.w_elem(elem_bpos)
			elem_bpos_next = elem.pce_chain.Next

			elem.pce_chain.Prev = BPOS_ILLEGAL
			elem.pce_chain.Next = BPOS_ILLEGAL
			wcache.insert_first_in_hashtable(elem)

			elem_count++

			elem_bpos = elem_bpos_next // next element in bucket chain
		}
	}

	assert(elem_count == wcache.wpc_hashtable_count)
}

// enlarge_element_array_and_hashtable enlarges Page_cache_element array and hashtable.
//
// In a transaction, every modified page needs one or two Page_cache_element (PCE_TRANSITORY and/or PCE_DRAFT versions of a page).
//
// As more and more pages are modified in a transaction, more Page_cache_elements are created to track these pages.
//
// If wcache.wpc_element_array_size_total_max_soft_limit is reached, an error is just signaled in wcache.wpc_error. The program must check it when appropriate and rollback.
//
func (wcache *Wcache) enlarge_element_array_and_hashtable() {
	var (
		new_element_array      []Page_cache_element
		new_element_array_size uint64
		new_hashtable_size     uint64
		elem                   *Page_cache_element
	)

	//==== check if we must signal that element max limit is reached ====

	if wcache.wpc_element_array_size_total >= wcache.wpc_element_array_size_total_max_soft_limit {
		wcache.wpc_error = rsql.New_Error(rsql.ERROR_RESOURCE, rsql.ERROR_WCACHE_ELEMENT_ARRAY_MAX_SIZE_REACHED, rsql.ERROR_BATCH_ABORT, wcache.wpc_element_array_size_total)
	}

	//==== determine number of elements to create ( == size of new memory chunk ) ====

	new_element_array_size = wcache.wpc_element_array_size_first_chunk // size for first call

	if len(wcache.wpc_element_array) > 0 {
		new_element_array_size = wcache.wpc_element_array_size_total // if not first call, double the size of element array
	}

	if new_element_array_size > WCACHE_ELEMENT_CHUNK_SIZE_MAX { // max size
		new_element_array_size = WCACHE_ELEMENT_CHUNK_SIZE_MAX
	}

	assert(new_element_array_size > 0)

	//==== create new element array ====

	new_element_array = make([]Page_cache_element, new_element_array_size)

	wcache.wpc_element_array = append(wcache.wpc_element_array, new_element_array)

	wcache.wpc_element_array_size_total += new_element_array_size
	//println("enlarge Page_cache_elements array to:", wcache.wpc_element_array_size_total)

	//==== initialize all Page_cache_elements of the new element array ====

	i := len(wcache.wpc_element_array) - 1 // index new chunk of elements

	for j, _ := range new_element_array {
		elem = &new_element_array[j]

		elem.pce_bpos = bpos(uint64(i), uint64(j))
		elem.pce_listinfo = PCE_ILLEGAL_LIST
		elem.pce_ppage_bpos = 0 // in free list, no ppage is attached
		elem.pce_logpage_no = -1

		elem.pce_chain.Prev, elem.pce_chain.Next = BPOS_ILLEGAL, BPOS_ILLEGAL
		elem.pce_enlistment.Prev, elem.pce_enlistment.Next = BPOS_ILLEGAL, BPOS_ILLEGAL
		elem.pce_draft.Prev, elem.pce_draft.Next = BPOS_ILLEGAL, BPOS_ILLEGAL
		elem.pce_same_tblid.Prev, elem.pce_same_tblid.Next = BPOS_ILLEGAL, BPOS_ILLEGAL
		wcache.insert_first(PCE_FREE_LIST, elem) // put it in free list
	}

	//==== enlarge hashtable ====

	new_hashtable_size = wcache.wpc_element_array_size_total + wcache.wpc_element_array_size_total/4 // new size is 25 % larger than total number of elements

	wcache.resize_hashtable(new_hashtable_size)

	assert(wcache.wpc_pin_list.Count+wcache.wpc_lru_list.Count+wcache.wpc_slim_list.Count+wcache.wpc_free_list.Count == wcache.wpc_element_array_size_total)
}

//=========================================================================
//
//              hangers and ppage array resizing functions
//
//=========================================================================

// enlarge_hangers_array enlarges hangers array by appending a new memory chunk of hangers with no attached ppage.
//
// Hangers are created only when needed.
//
func (wcache *Wcache) enlarge_hangers_array() {
	var (
		new_hangers_array      []Ppage_hanger
		new_hangers_array_size uint64
		hanger                 *Ppage_hanger
	)

	//==== determine number of hangers to create ( == size of new memory chunk ) ====

	new_hangers_array_size = wcache.wpc_hangers_array_size_first_chunk // size for first call

	if len(wcache.wpc_hangers_array) > 0 {
		new_hangers_array_size = uint64(2 * len(wcache.wpc_hangers_array[len(wcache.wpc_hangers_array)-1])) // if not first call, new chunk of hangers size is double of last one
	}

	if new_hangers_array_size > WCACHE_HANGERS_CHUNK_SIZE_MAX { // max size
		new_hangers_array_size = WCACHE_HANGERS_CHUNK_SIZE_MAX
	}

	assert(new_hangers_array_size > 0)

	// allocate new memory chunk for hangers_array

	new_hangers_array = make([]Ppage_hanger, new_hangers_array_size)
	wcache.wpc_hangers_array = append(wcache.wpc_hangers_array, new_hangers_array) // array of hangers

	wcache.wpc_hangers_array_size_total += new_hangers_array_size // total count of hangers available for the write cache

	//==== put all hangers_array items in the wpc_hangers_with_no_ppage list ====

	i := uint64(len(wcache.wpc_hangers_array) - 1)

	for j := uint64(0); j < new_hangers_array_size; j++ {
		hanger = &new_hangers_array[j]
		hanger.pph_bpos = bpos(i, j)

		wcache.hanger_insert_first(HANGERS_WITH_NO_PPAGE_LIST, hanger) // put it in no ppages list
	}

	assert(wcache.wpc_hangers_with_blank_ppage.Count+wcache.wpc_hangers_with_no_ppage.Count == wcache.wpc_hangers_array_size_total)
}

// enlarge_ppage_array enlarges ppage array by appending a new memory chunk of blank ppages.
//
//        WARNING:  before calling this function, the caller must ensure that at least one more ppage will be allocated and available for caching :
//                      - wcache.wpc_ppage_array_size_total < wcache.wpc_ppage_array_size_total_max_limit
//
func (wcache *Wcache) enlarge_ppage_array() {
	var (
		new_ppage_array      []Page
		new_ppage_array_size uint64
		hanger               *Ppage_hanger
	)

	assert(wcache.wpc_ppage_array_size_total < wcache.wpc_ppage_array_size_total_max_limit) // ensure that it is allowed to create more ppages

	//==== determine number of ppages array to create ( == size of new memory chunk ) ====

	new_ppage_array_size = wcache.wpc_ppage_array_size_first_chunk // size for first call

	if len(wcache.wpc_ppage_array) > 0 {
		new_ppage_array_size = wcache.wpc_ppage_array_size_total // if not first call, double the size of ppage array
	}

	if new_ppage_array_size > WCACHE_PPAGE_CHUNK_SIZE_MAX {
		new_ppage_array_size = WCACHE_PPAGE_CHUNK_SIZE_MAX
	}

	if wcache.wpc_ppage_array_size_total+new_ppage_array_size > wcache.wpc_ppage_array_size_total_max_limit { // crop size if limit is exceeded
		new_ppage_array_size = wcache.wpc_ppage_array_size_total_max_limit - wcache.wpc_ppage_array_size_total
	}

	assert(new_ppage_array_size > 0)

	// allocate new memory chunk for ppage_array

	new_ppage_array = make([]Page, new_ppage_array_size)
	wcache.wpc_ppage_array = append(wcache.wpc_ppage_array, new_ppage_array) // array of ppages

	wcache.wpc_ppage_array_size_total += new_ppage_array_size // total count of ppages available for the write cache

	//==== hang all ppages on hangers, and put them all into the wpc_hangers_with_blank_ppage list ====

	i := uint64(len(wcache.wpc_ppage_array) - 1)

	for j := uint64(0); j < new_ppage_array_size; j++ {
		hanger = wcache.get_hanger_with_no_ppage()
		hanger.pph_ppage_bpos = bpos(i, j) // the attachement of a ppage to a hanger is not permanent and changes often, as ppages are hanged and unhanged

		wcache.hanger_insert_first(HANGERS_WITH_BLANK_PPAGE_LIST, hanger) // put hanger in blank ppages list
	}

	//assert(wcache.wpc_pin_list.Count+wcache.wpc_lru_list.Count+wcache.wpc_hangers_with_blank_ppage.Count == wcache.wpc_ppage_array_size_total) // fails because Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it moves element from slim list to pin list, before calling get_blank_ppage(), which calls in the end enlarge_ppage_array().
}

// get_hanger_with_no_ppage gets a hanger with no attached ppage.
//
func (wcache *Wcache) get_hanger_with_no_ppage() *Ppage_hanger {
	var (
		hanger *Ppage_hanger
	)

	// if no more available hangers with no ppage, create some

	if wcache.wpc_hangers_with_no_ppage.Count == 0 {
		assert(wcache.wpc_hangers_with_no_ppage.Prev == 0 && wcache.wpc_hangers_with_no_ppage.Next == 0)

		wcache.enlarge_hangers_array() // new hangers have been put in wcache.wpc_hangers_with_no_ppage list
	}

	// pop unused hanger from wpc_hangers_with_no_ppage list

	hanger = wcache.hanger_pop_last(HANGERS_WITH_NO_PPAGE_LIST)
	assert(hanger.pph_ppage_bpos == 0)

	return hanger
}

// release_ppage_from_element releases ppage from elem.
//
// The caller must have just removed elem from lru list, and dirty flag must be false.
//
//       WARNING: this function only removes ppage from elem, but doesn't change anything else.
//
func (wcache *Wcache) release_ppage_from_element(elem *Page_cache_element) {
	var (
		hanger     *Ppage_hanger
		ppage_bpos bpos_t
	)

	assert(elem.pce_listinfo == PCE_ILLEGAL_LIST)
	assert(elem.pce_dirty == false)

	ppage_bpos = elem.pce_ppage_bpos
	elem.pce_ppage_bpos = 0 // detach ppage from elem

	hanger = wcache.get_hanger_with_no_ppage() // get empty hanger
	hanger.pph_ppage_bpos = ppage_bpos         // hang ppage on hanger

	wcache.hanger_insert_first(HANGERS_WITH_BLANK_PPAGE_LIST, hanger) // put it into wpc_hangers_with_blank_ppage list
}

// refill_blank_ppages_list refills blank ppages list.
//
// This function must be called only if wcache.wpc_hangers_with_blank_ppage list is empty.
//
//      These blank ppages are generated as follows :
//          - if the max number of ppages has not been reached yet, create a new memory chunk with new blank ppages
//          - else, remove a handful of ppages from elems in lru list, and put these elems into slim list.
//
//      At least one ppage is put in blank ppages list, but often many more.
//
func (wcache *Wcache) refill_blank_ppages_list() {
	var (
		ppage_array_size_total_max_limit uint64
		elem                             *Page_cache_element
	)

	assert(wcache.wpc_hangers_with_blank_ppage.Count == 0) // assert blank ppage list is empty

	ppage_array_size_total_max_limit = wcache.wpc_ppage_array_size_total_max_limit

	//===== if we can create a new blank ppages array, just do it =====

	if wcache.wpc_ppage_array_size_total < ppage_array_size_total_max_limit { // if we can create more ppages
		wcache.enlarge_ppage_array() // create more ppages (it is guaranteed that at least one more ppage is created)

		return // ===> return           now, wpc_hangers_with_blank_ppage list is not empty
	}

	//===== else, we must free some ppages from lru list, by saving their content into the log =====

	for i := 0; i < WCACHE_NUMBER_OF_PPAGES_TO_FREE; i++ { // we want to move this number of elems from lru list to slim list. elem remains in the same hashtable bucket, and in the same draft_list if any.
		//=== remove elem from lru list ===

		elem = wcache.pop_last(PCE_LRU_LIST) // pop last elem from the lru list
		if elem == nil {                     // if no elem in lru list, it means they are all in pin list, which is impossible.
			if i == 0 { // In the really worst case with many worker processes, only several hundreds of pages may be in pin list, and the cache contains several thousands pages.
				panic("no cache element available in lru list.") // but in our test, we can have many goroutines trying to pin more pages than the cache contains
			}
			break // less than WCACHE_NUMBER_OF_PPAGES_TO_FREE elements have been freed
		}

		//=== flush elem to logfile if dirty ===

		if elem.pce_dirty {
			wcache.flush_ppage_to_logfile(elem) // if dirty, flush ppage to logfile. dirty flag is set to false.
		}

		assert(elem.pce_ref_count == 0 && // ref count is > 0  only in pin list
			elem.pce_version&(PCE_TRANSITORY|PCE_DRAFT) != 0 &&
			elem.pce_dirty == false &&
			elem.pce_listinfo == PCE_ILLEGAL_LIST && // wcache.release_ppage_from_element() just below asserts it to be PCE_ILLEGAL_LIST
			elem.pce_logpage_no >= 0) // ppage has been saved to this log page
		assert(elem.pce_chain.Prev != BPOS_ILLEGAL && elem.pce_chain.Next != BPOS_ILLEGAL)           // elem remains in hashtable bucket
		assert(elem.pce_same_tblid.Prev != BPOS_ILLEGAL && elem.pce_same_tblid.Next != BPOS_ILLEGAL) // elem also remains in this list

		//=== remove blank ppage from elem, and put this ppage into blank ppage list ===

		wcache.release_ppage_from_element(elem)

		//=== put elem into slim list ===

		wcache.insert_first(PCE_SLIM_LIST, elem)
	}
}

// get_blank_ppage gets pointer to a blank ppage.
//
//     If wpc_hangers_with_blank_ppage list is empty, we must refill it:
//          - by allocating some memory for a bunch of new ppages
//          - by taking away a handful of ppages from elements in lru list, which are transfered to slim list
//
//     Then, we just pop a ppage from wpc_hangers_with_blank_ppage list and return it.
//     The returned ppage is filled with zeros.
//
func (wcache *Wcache) get_blank_ppage() (ppage_bpos bpos_t) {
	var (
		hanger *Ppage_hanger
	)

	//===== if blank ppages list is empty, refill it =====

	if wcache.wpc_hangers_with_blank_ppage.Count == 0 { // if list of available blank pages is empty
		wcache.refill_blank_ppages_list() // refill blank ppage list, by creating a new ppage array if possible, or by taking a few ppages away from lru list elem
	}

	// here, blank ppages list is never empty

	//===== get pointer to a blank ppage =====

	hanger = wcache.hanger_pop_last(HANGERS_WITH_BLANK_PPAGE_LIST) // pop hanger from wpc_hangers_with_blank_ppage list. ppage has been filled with zeroes.
	assert(hanger != nil)                                          // a blank ppage was always available

	ppage_bpos = hanger.pph_ppage_bpos // unhang blank ppage from the hanger
	assert(ppage_bpos != 0)
	hanger.pph_ppage_bpos = 0

	wcache.hanger_insert_first(HANGERS_WITH_NO_PPAGE_LIST, hanger) // put it into wpc_hangers_with_no_ppage list

	return ppage_bpos // ===> and return the pppage bpos
}

// get_page_cache_element_from_free_list_with_blank_ppage_attached returns a Page_cache_element with attached ppage filled with zeroes.
//
func (wcache *Wcache) get_page_cache_element_from_free_list_with_blank_ppage_attached() *Page_cache_element {
	var (
		elem             *Page_cache_element
		ppage_blank_bpos bpos_t
	)

	if wcache.wpc_free_list.Count == 0 {
		// no free element exists.

		wcache.enlarge_element_array_and_hashtable() // create more new elements (at least one more element). Signals an error if max number of elements is reached.
	}

	//=== remove one element from the free list ===

	elem = wcache.pop_last(PCE_FREE_LIST) // pop an element from the free list
	assert(elem != nil)                   // available free element always exists here

	//=== attach a ppage to elem ===

	assert(elem.pce_ppage_bpos == 0)            // elem in free list never has a ppage attached
	ppage_blank_bpos = wcache.get_blank_ppage() // ppage has been filled with zeroes
	elem.pce_ppage_bpos = ppage_blank_bpos

	return elem
}

// retrieve_new_blank_DRAFT_element_from_free_list_and_pin_it retrieves a pointer to a blank pinned PCE_DRAFT Page_cache_element, taken out from the free list.
//
// If max number of element has been reached, an error is signaled in wcache.wpc_error.
//
// This function retrieves an element from the free list, and puts it in the pin list and draft list.
//
// Then, it returns this blank element. Attached ppage has been filled with zeroes.
//
//        The caller must then fill in the fields :
//            - pce_hashval
//            - pce_tblid
//            - pce_page_no
//            - pce_chain
//            - pce_same_tblid
//            - content of the ppage at pce_ppage_bpos
//
// The other fields are filled by the function.
//
func (wcache *Wcache) retrieve_new_blank_DRAFT_element_from_free_list_and_pin_it(tblid Tblid_t) *Page_cache_element {
	var (
		elem    *Page_cache_element
		journal *Journal
	)

	//=== get a Page_cache_element  ===

	// note : once initialized to track a page, an element is almost never freed. Only when it is overridden by a DRAFT element, is the TRANSITORY element freed. DELETE also free elements.

	elem = wcache.get_page_cache_element_from_free_list_with_blank_ppage_attached() // signals an error in wcache.wpc_error if max number of elements is reached. Attached page has been filled with zeroes.

	assert(elem.pce_ref_count == 0) // when inserted in pin list, ref_count will be set to 1
	elem.pce_version = PCE_DRAFT    // PCE_DRAFT

	wcache.insert_first(PCE_PIN_LIST, elem) // put it in pin list
	wcache.insert_first_in_draft_list(elem) // put it in draft list

	elem.pce_dirty = true // new DRAFT page is always dirty

	assert(elem.pce_hashval == 0 &&
		elem.pce_tblid == 0 &&
		elem.pce_page_no == 0 &&
		elem.pce_ref_count == 1 &&
		elem.pce_version == PCE_DRAFT &&
		elem.pce_dirty == true &&
		elem.pce_listinfo == PCE_PIN_LIST &&

		elem.pce_ppage_bpos != 0 &&
		elem.pce_logpage_no == -1)

	assert(elem.pce_chain.Prev == BPOS_ILLEGAL && elem.pce_chain.Next == BPOS_ILLEGAL) // ;Print_ppage(wcache.w_ppage(elem))

	journal = wcache.wpc_journal
	if tblid < 0 {
		journal = wcache.get_journal_for(tblid)
	}

	list_length := len(journal.jr_logfile_freed_pages) // reuse logpage if possible
	if list_length > 0 {
		elem.pce_logpage_no = journal.jr_logfile_freed_pages[list_length-1]
		assert(elem.pce_logpage_no != -1)
		journal.jr_logfile_freed_pages[list_length-1] = -1
		journal.jr_logfile_freed_pages = journal.jr_logfile_freed_pages[:list_length-1]
	}

	return elem
}

// discard_TRANSITORY_element removes TRANSITORY element from lru list or slim list, releasing ppage, and putting it into free list.
//
//      WARNING: elem.pce_dirty flag must be set to false before calling this function, even if you haven't flushed the ppage to log.
//
//      TRANSITORY elements can be discarded in two cases:
//           - when a TRANSITORY ppage of a previous SQL statement is overriden and made obsolete by DRAFT pages of the current SQL statement. It must be discarded when current statement terminates.
//           - at COMMIT, when all TRANSITORY ppages have been flushed to logfile. After logfile has been synced, TRANSITORY elements are no more needed and are discarded.
//
func (wcache *Wcache) discard_TRANSITORY_element(elem *Page_cache_element) {
	assert(elem.pce_version == PCE_TRANSITORY)
	assert(elem.pce_dirty == false)

	wcache.remove_from_hashtable(elem) // remove it from bucket chain
	// elem.pce_draft                           // TRANSITORY are not in elem.pce_draft list
	wcache.remove_from_same_tblid_maplist(elem) // remove it from same_tblid_maplist which contains all element with corresponding tblid

	switch elem.pce_listinfo {
	case PCE_LRU_LIST:
		assert(elem.pce_ppage_bpos != 0)
		wcache.remove(PCE_LRU_LIST, elem)
		wcache.release_ppage_from_element(elem) // release ppage

	case PCE_SLIM_LIST:
		assert(elem.pce_ppage_bpos == 0)
		wcache.remove(PCE_SLIM_LIST, elem)

	default:
		panic("impossible")
	}

	if elem.pce_logpage_no != -1 { // a logpage is attached. Put it in free list so that it can be reused.
		journal := wcache.wpc_journal
		if elem.pce_tblid < 0 {
			journal = wcache.get_journal_for(elem.pce_tblid)
		}
		journal.jr_logfile_freed_pages = append(journal.jr_logfile_freed_pages, elem.pce_logpage_no)
		//println("    discard logpage", elem.pce_logpage_no, "len", len(journal.jr_logfile_freed_pages))
		elem.pce_logpage_no = -1
	}

	wcache.insert_last(PCE_FREE_LIST, elem)
}

// discard_DRAFT_element removes DRAFT element from lru list or slim list, from draft list, releasing ppage, and putting it into free list.
//
//      WARNING: elem.pce_dirty flag must be set to false before calling this function, even if you haven't flushed the ppage to log.
//
// It is used for sql DELETE statement, when a page is deleted.
//
func (wcache *Wcache) discard_DRAFT_element(elem *Page_cache_element) {
	assert(elem.pce_version == PCE_DRAFT)
	assert(elem.pce_dirty == false)

	wcache.remove_from_hashtable(elem) // remove it from bucket chain

	wcache.remove_from_draft_list(elem) // remove from draft list

	wcache.remove_from_same_tblid_maplist(elem) // remove it from same_tblid_maplist which contains all element with corresponding tblid

	switch elem.pce_listinfo {
	case PCE_LRU_LIST:
		assert(elem.pce_ppage_bpos != 0)
		wcache.remove(PCE_LRU_LIST, elem)
		wcache.release_ppage_from_element(elem) // release ppage

	case PCE_SLIM_LIST:
		assert(elem.pce_ppage_bpos == 0)
		wcache.remove(PCE_SLIM_LIST, elem)

	default:
		panic("impossible")
	}

	if elem.pce_logpage_no != -1 { // a logpage is attached. Put it in free list so that it can be reused.
		journal := wcache.wpc_journal
		if elem.pce_tblid < 0 {
			journal = wcache.get_journal_for(elem.pce_tblid)
		}
		journal.jr_logfile_freed_pages = append(journal.jr_logfile_freed_pages, elem.pce_logpage_no)
		//println("    discard logpage DRAFT", elem.pce_logpage_no, "len", len(journal.jr_logfile_freed_pages))
		elem.pce_logpage_no = -1
	}

	wcache.insert_last(PCE_FREE_LIST, elem)
}

// unpin0 unpins a page from cache.
//
//      The page reference count is decremented.
//           - if ref count != 0, page is kept in pin list
//           - if ref count == 0, page is removed from pin list and put into lru list
//
//      As you shouldn't use elem after it has been unpinned, this function always returns nil, so that you can write:
//
//                        elem = wcache.unpin0() // elem is set to nil
//
func (wcache *Wcache) unpin0(elem *Page_cache_element) *Page_cache_element {

	assert(elem.pce_listinfo == PCE_PIN_LIST && elem.pce_version&(PCE_DRAFT|PCE_TRANSITORY) != 0)

	// decrement reference count

	if elem.pce_ref_count > 1 {
		elem.pce_ref_count--
		return nil // ----> return
	}

	// move from pin list to lru list

	wcache.remove(PCE_PIN_LIST, elem) // remove from pin list

	wcache.insert_first(PCE_LRU_LIST, elem) // put into lru list

	// note : elem.pce_hashval, tblid, page_no, pce_chain and pce_draft are not modified. elem remains in the same bucket and same draft list when it swaps between pin list and lru list.

	if XDEBUG_UNPIN_TO_SLIM_LIST {
		wcache.debug_move_from_LRU_to_SLIM_list(elem)
	}

	return nil
}

// for debuging only
//
func (wcache *Wcache) Debug_elem_string(elem *Page_cache_element) string {
	var (
		s string
	)

	pce_tblid := elem.pce_tblid
	pce_page_no := elem.pce_page_no
	pce_ref_count := elem.pce_ref_count
	pce_version := elem.pce_version // PCE_FREED, PCE_PUBLIC, PCE_TRANSITORY, PCE_DRAFT
	pce_dirty := elem.pce_dirty
	pce_listinfo := elem.pce_listinfo

	switch elem.pce_listinfo {
	case PCE_PIN_LIST, PCE_LRU_LIST:
		ppage := wcache.ppage_for_debug(elem)
		assert(elem.pce_tblid == ppage.Pg_tblid())
		assert(elem.pce_page_no == ppage.Pg_no())

		pg_type := ppage.Pg_type() // PAGE_TYPE_LEAF or PAGE_TYPE_NODE, or PAGE_TYPE_ZONE_ALLOCATOR or PAGE_TYPE_TABLE_INFO
		pg_td_type := ppage.Pg_td_type()
		pg_dbid := ppage.Pg_dbid()
		pg_schid := ppage.Pg_schid()
		pg_base_gtblid := ppage.Pg_base_gtblid()
		pg_tblid := ppage.Pg_tblid()
		pg_no := ppage.Pg_no()

		s = fmt.Sprintf("%s  %d/%d ref_count=%d version=%s dirty=%t        %s  %s  [%d.%d.(%d)%d]  page_no=%d", pce_listinfo, pce_tblid, pce_page_no, pce_ref_count, pce_version, pce_dirty, pg_type, pg_td_type, pg_dbid, pg_schid, pg_base_gtblid, pg_tblid, pg_no)

	case PCE_SLIM_LIST:
		assert(pce_dirty == false)

		s = fmt.Sprintf("%s  %d/%d ref_count=%d version=%s dirty=%t", pce_listinfo, pce_tblid, pce_page_no, pce_ref_count, pce_version, pce_dirty)

	case PCE_FREE_LIST:
		assert(pce_tblid == 0)
		assert(pce_page_no == 0)
		assert(pce_ref_count == 0)
		assert(pce_version == PCE_FREED)
		assert(pce_dirty == false)

		s = fmt.Sprintf("%s  %d/%d ref_count=%d version=%s dirty=%t", pce_listinfo, pce_tblid, pce_page_no, pce_ref_count, pce_version, pce_dirty)

	default:
		panic("impossible")
	}

	return s
}

// for debugging only.
// This code is from refill_blank_ppages_list(). It is used to move elem from lru to slim list, when unpinned.
// This way, we can detect writes to unpinned pages.
//
func (wcache *Wcache) debug_move_from_LRU_to_SLIM_list(elem *Page_cache_element) {

	wcache.remove(PCE_LRU_LIST, elem) // remove elem from the lru list

	//=== flush elem to logfile if dirty ===

	if elem.pce_dirty {
		wcache.flush_ppage_to_logfile(elem) // if dirty, flush ppage to logfile. dirty flag is set to false.
	}

	assert(elem.pce_ref_count == 0 && // ref count is > 0  only in pin list
		elem.pce_version&(PCE_TRANSITORY|PCE_DRAFT) != 0 &&
		elem.pce_dirty == false &&
		elem.pce_listinfo == PCE_ILLEGAL_LIST && // wcache.release_ppage_from_element() just below asserts it to be PCE_ILLEGAL_LIST
		elem.pce_logpage_no >= 0) // ppage has been saved to this log page
	assert(elem.pce_chain.Prev != BPOS_ILLEGAL && elem.pce_chain.Next != BPOS_ILLEGAL)           // elem remains in hashtable bucket
	assert(elem.pce_same_tblid.Prev != BPOS_ILLEGAL && elem.pce_same_tblid.Next != BPOS_ILLEGAL) // elem also remains in this list

	//=== remove blank ppage from elem, and put this ppage into blank ppage list ===

	wcache.release_ppage_from_element(elem)

	//=== put elem into slim list ===

	wcache.insert_first(PCE_SLIM_LIST, elem)
}

func (wcache *Wcache) Debug_print(listinfo Dlist_type_t) {

	wcache.debug_print(listinfo)
}

func (wcache *Wcache) Debug_check_lists_count() {
	var (
		anchor                            *Dlist_anchor
		elem_bpos                         bpos_t
		elem                              *Page_cache_element
		hanger_bpos                       bpos_t
		hanger                            *Ppage_hanger
		cc_pin                            uint64
		cc_lru                            uint64
		cc_slim                           uint64
		cc_free                           uint64
		cc_hangers_with_blank_ppage_count uint64
		cc_hangers_with_no_ppage_count    uint64
	)

	// pin list count

	anchor = &wcache.wpc_pin_list
	for elem_bpos = anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_enlistment.Next {
		elem = wcache.w_elem(elem_bpos)
		assert(elem.pce_version != PCE_PUBLIC)
		cc_pin++
	}

	assert(cc_pin == wcache.wpc_pin_list.Count)

	// lru list count

	anchor = &wcache.wpc_lru_list
	for elem_bpos = anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_enlistment.Next {
		elem = wcache.w_elem(elem_bpos)
		assert(elem.pce_version != PCE_PUBLIC)
		cc_lru++
	}

	assert(cc_lru == wcache.wpc_lru_list.Count)

	// slim list count

	anchor = &wcache.wpc_slim_list
	for elem_bpos = anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_enlistment.Next {
		elem = wcache.w_elem(elem_bpos)
		assert(elem.pce_version != PCE_PUBLIC)
		cc_slim++
	}

	assert(cc_slim == wcache.wpc_slim_list.Count)

	// free list count

	anchor = &wcache.wpc_free_list
	for elem_bpos = anchor.Next; elem_bpos != 0; elem_bpos = elem.pce_enlistment.Next {
		elem = wcache.w_elem(elem_bpos)
		assert(elem.pce_version != PCE_PUBLIC)
		cc_free++
	}

	assert(cc_free == wcache.wpc_free_list.Count)

	// hangers_with_blank_ppage list count

	anchor = &wcache.wpc_hangers_with_blank_ppage
	for hanger_bpos = anchor.Next; hanger_bpos != 0; hanger_bpos = hanger.pph_dlist.Next {
		hanger = wcache.w_hanger(hanger_bpos)
		cc_hangers_with_blank_ppage_count++
	}

	assert(cc_hangers_with_blank_ppage_count == wcache.wpc_hangers_with_blank_ppage.Count)

	// hangers_with_no_ppage list count

	anchor = &wcache.wpc_hangers_with_no_ppage
	for hanger_bpos = anchor.Next; hanger_bpos != 0; hanger_bpos = hanger.pph_dlist.Next {
		hanger = wcache.w_hanger(hanger_bpos)
		cc_hangers_with_no_ppage_count++
	}

	assert(cc_hangers_with_no_ppage_count == wcache.wpc_hangers_with_no_ppage.Count)
}
