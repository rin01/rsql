package cache

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"

	"rsql"
)

var GPAGE_CACHE Gpage_cache // global page cache

var WORKERS_JOURNALS Workers_journals // collection of journals for workers

func assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

// global read cache.
//
//      ***IMPORTANT***     VOCABULARY : we call "ppage" an object of type "Page" which contains the rows of a table or index.
//                                       The term "page" is for general use and may be ambiguous, as it may stand for a "Page_cache_element" or a "Page" or both together.
//
//      In this cache, gpc_element_array is a fixed size array.
//
//      Each element in gpc_element_array[] points to the corresponding ppage in gpc_ppage_array[], and this pointer never changes.
//
//      Hashtable is also fixed size.
//
//      Page_cache_elements in this gpc_element_array continuously change their <tblid, page_no>, and they are always PCE_PUBLIC.
//
//           All Page_cache_elements belong to one of these lists :
//               - pin list :
//                     - when the caller requests a page from the database, the data is copied into a page fetched from the free list, and put into the pin list.
//                     - the Page_cache_element and Page content are "pinned" in memory, that is, they don't change until they are "unpinned" by the caller.
//               - lru list :
//                     - least recently used list. When a page is unpinned, it is put into the lru list.
//                     - if the caller requests a page, and it is found in the lru list, it is "pinned" by being put back into the pin list. It is the "cache" effect.
//                     - if a free page is needed to "pin" it in the pin list, it is taken out of the free list. If free list is empty, a handful of pages from the lru list are discarded and put beforehand into the free list.
//               - free list :
//                     - when the caller requests a page from the database, and it is not in the pin list or lru list, it is copied into a page from this list, and it is "pinned" by being put in the pin list.
//
//      Multiple processes can access this global cache at the same time, but the access is controlled by the gpc_mutex.
//
type Gpage_cache struct {
	gpc_mutex sync.Mutex // synchronize access to this global shared read cache by multiple worker goroutines. The cache is only locked during the page lookup, which is a very short time.

	gpc_hashtable       []Hlist // fixed size hashtable. Lookup is done by using pce_hashval field of Page_cache_element
	gpc_hashtable_size  uint64  // size of hashtable. It never changes.
	gpc_hashtable_count int     // number of elements in hashtable

	gpc_pin_list  Dlist_anchor // double linked list of pages which must stay in memory because one or more processes are currently using them
	gpc_lru_list  Dlist_anchor // double linked list of unpinned pages, which may be pinned again later or freed
	gpc_free_list Dlist_anchor // double linked list of free pages

	gpc_element_array [GPC_ELEMENT_ARRAY_NUMBER][]Page_cache_element // fixed size element array, which always point to the same corresponding ppage in gpc_page_array[]
	gpc_ppage_array   [GPC_ELEMENT_ARRAY_NUMBER][]Page               // this is the real page cache, a few big chunks of memory. Same size as gpc_element_array[]
}

type Logpage_no_t int64

// When the worker process needs a page from cache, it receives a pointer to a Page_cache_element, which is pinned in memory.
//
//    In global read cache, a pinned element :
//      - cannot be removed from memory, until it is unpinned
//      - the worker can read the content of the element and page, and be sure it won't be modified at the same time by another worker, thanks to the locking manager.
//      - only the pce_ref_count field may be modified, but it is a field used only internally by the cache manager.
//          WHEN THE WORKER HAS RECEIVED A POINTER TO AN ELEMENT, IT MUST NOT READ OR MODIFIY pce_ref_count ! Besides, it is not 'volatile', and thus not reliable, as other goroutines may update it.
//          Same remark can be done for pce_chain and pce_enlistment field, as next or previous element in these lists may constantly change.
//
//    The write cache also returns Page_cache_elements, but as write cache is private to the worker, there is no problem of concurrency or locking.
//
type Page_cache_element struct {
	pce_bpos      bpos_t // Never changes. Unique bpos of a Page_cache element. It can be used to find the element location in the element array.
	pce_hashval   uint64
	pce_tblid     Tblid_t
	pce_page_no   Page_no_t
	pce_ref_count uint32       // number of uses of the page. If > 0, the cached page is pinned in memory and must stay in pin list. In all other lists, ref_count == 0.
	pce_version   Version_t    // PCE_FREED, PCE_PUBLIC, PCE_TRANSITORY, PCE_DRAFT
	pce_dirty     bool         // ONLY USED BY THE WRITE CACHE
	pce_listinfo  Dlist_type_t // PCE_PIN_LIST, PCE_LRU_LIST, PCE_SLIM_LIST, PCE_FREE_LIST. See pce_enlistment. An element is always in one of these lists.

	pce_chain      Hlist // bucket chain (double linked list). Only pages in pin list, lru list and slim list are chained in hashtable buckets, not the pages in free list.
	pce_enlistment Dlist // pin list, lru list, slim list and free list (the list is mentioned in pce_listinfo)
	pce_draft      Rlist // ONLY USED BY THE WRITE CACHE, draft page list.
	pce_same_tblid Tlist // ONLY USED BY THE WRITE CACHE, list of elements with same tblid.

	pce_ppage_bpos bpos_t       // FOR GLOBAL READ CACHE, points to its permanently attached ppage. FOR WRITE CACHE, it points to the currently attached ppage. It can be 0 if no ppage is attached.
	pce_logpage_no Logpage_no_t // ONLY USED BY THE WRITE CACHE. -1 if no page in log. If element is in PCE_FREE_LIST, it is always -1.
}

// PUBLIC API: returns true if elem is DRAFT and pce_dirty == true, which means you can write in the page.
//
func (elem *Page_cache_element) Is_DRAFT_for_write() bool {

	return elem.pce_listinfo == PCE_PIN_LIST && elem.pce_version == PCE_DRAFT && elem.pce_dirty == true
}

// PUBLIC API: returns element's pce_page_no field.
//
func (elem *Page_cache_element) Pce_page_no() Page_no_t {
	return elem.pce_page_no
}

// PUBLIC API: returns element's pce_version field.
//
func (elem *Page_cache_element) Pce_version() Version_t {
	return elem.pce_version
}

var (
	GPC_NUMBER_OF_PAGES_PER_CHUNK uint64 // computed when server is initializing, and then never changes.
	GPC_TOTAL_NUMBER_OF_PAGES     uint64 // computed when server is initializing, and then never changes.
)

// The write cache use hangers to manage free ppages.
//
// When a lot of pages are modified, there exists at least the same count of Page_cache_elements, or a little more.
//
// In order not to use too much memory, the count of ppage buffers is small, and are likely to be freed shortly after an element goes to lru list.
//
// The freed ppage ready to be reused are "hanged" on these hangers, ready to be picked up when another element needs a ppage before it can be pinned.
//
type Ppage_hanger struct {
	pph_bpos       bpos_t // Location of hanger. Never changes.
	pph_dlist      Dlist
	pph_ppage_bpos bpos_t // points to the attached ppage, or 0 if no ppage is attached
}

// private write cache.
//
//     This cache contains only pages that have been modified by the current transaction.
//     Non-modified pages are managed by the global read cache.
//
//     Page_cache_elements in this cache are all PCE_TRANSITORY or PCE_DRAFT.
//           - PCE_TRANSITORY pages are the pages modified by all previous SQL statements so far since the current transaction began.
//           - PCE_DRAFT pages are the pages being modified by the current SQL statement. When this statement terminates, PCE_DRAFT pages are converted to PCE_TRANSITORY, possibly making overriden PCE_TRANSITORY pages obsolete.
//
//     wpc_element_array is enlarged as more different database pages are modified.
//           - Each modified database page requires one Page_cache_element (PCE_TRANSITORY). If a lot of modifications are done, this memory may become very large.
//           - Some more Page_cache_element are also temporarily needed for all pages being modified by the current SQL statement (PCE_DRAFT)
//
//     Page_cache_elements PCE_TRANSITORY have all a fixed <tblid, page_no> which normally never changes for the duration of a transaction.
//           - however, they can return to the free list, in case a PCE_DRAFT with same <tblid, page_no> exists, which will be converted to PCE_TRANSITORY when the current SQL statement ends, making the old PCE_TRANSITORY Page_cache_element obsolete. This happens if a page is updated again by another SQL statement in the same transaction.
//           - else, they just stay in the cache. They don't go to the free list.
//
//     Page_cache_elements PCE_DRAFT are converted to PCE_TRANSITORY when the current SQL statement terminates. They never go to the free list.
//
//
//     The number of Page_cache_elements in memory may become very big, but the real page content may be found :
//        - in a page in memory, in wpc_ppage_array.
//        - in the log file, if there is no room left in wpc_page_array. The log file may be considered as a swap file for the wpc_ppage_array.
//
//     In a transaction, when a SQL statement terminates,
//        - all PCE_DRAFT pages are converted to PCE_TRANSITORY, possibly setting free some PCE_TRANSITORY pages if overriden.
//        - the SQL statement must free all elements that belongs to the systemptables it has created.
//
//     When the transaction ends, all dirty PCE_TRANSITORY pages are flushed to the log and the log file is synchronized.
//     Then, all pages from the log are copied to the table files, and synchronized.
//
type Wcache struct {
	//==== write cache: element array ====

	wpc_hashtable       []Hlist // size increases to cope with number of Page_cache_elements
	wpc_hashtable_size  uint64  // size of hashtable. The size increases to cope with the number of elements.
	wpc_hashtable_count int     // number of elements in hashtable

	wpc_element_array_size_first_chunk          uint64 // this limit is set at initialization. Size of first chunk.
	wpc_element_array_size_total_max_soft_limit uint64 // this soft limit is set at initialization. When this number of elements have been created, an error is signaled in wcache.wpc_error. The program must check it and rollback when possible.

	wpc_element_array            [][]Page_cache_element // Each modified page in a transaction needs a Page_cache_element. As more pages are modified, a new memory chunk is created, with size double of the previous one.
	wpc_element_array_size_total uint64                 // total count of Page_cache_elements created so far in all wpc_element_array[].

	wpc_pin_list  Dlist_anchor // (double linked list)             always has Ppage        may be dirty      may have logpage or not       PCE_DRAFT or PCE_TRANSITORY
	wpc_lru_list  Dlist_anchor // (double linked list)             always has Ppage        may be dirty      may have logpage or not       PCE_DRAFT or PCE_TRANSITORY
	wpc_slim_list Dlist_anchor // (double linked list)             has no Ppage            never dirty       always has logpage            PCE_DRAFT or PCE_TRANSITORY
	wpc_free_list Dlist_anchor // (double linked list)             has no Ppage            never dirty       never has logpage             PCE_FREED

	wpc_draft_list Rlist_anchor // (double linked list)

	wpc_same_tblid_map map[Tblid_t]*Tlist_anchor // (map of double linked lists)

	//==== write cache: hangers and ppage array ====

	wpc_hangers_array_size_first_chunk uint64 // this limit is set at initialization. Size of first chunk.

	wpc_hangers_array            [][]Ppage_hanger // array of Ppage_hangers, tracking blank ppages that are available for use
	wpc_hangers_array_size_total uint64           // total count of hangers created so far in all wpc_hangers_array[]

	wpc_hangers_with_blank_ppage Dlist_anchor // list of Ppage_hangers that point to blank ppages
	wpc_hangers_with_no_ppage    Dlist_anchor // list of empty Ppage_hangers that have no blank hanging ppage

	//---

	wpc_ppage_array_size_first_chunk     uint64 // this limit is set at initialization. Size of first chunk.
	wpc_ppage_array_size_total_max_limit uint64 // this limit is set at initialization. It is the max number of ppages that can be created.

	wpc_ppage_array            [][]Page // this is the real page cache, chunks of memory which are allocated if needed. I call it ppage just to make the code more readable.
	wpc_ppage_array_size_total uint64   // total count of ppages created so far in all wpc_ppage_array[]

	//==== write cache: filebag ====

	wpc_filebag *Filebag

	//==== write cache: logfiles ====

	wpc_journal *Journal // logfile for ordinary tables. Journal are permanent and stored in global variable WORKERS_JOURNALS collection.

	wpc_flashjournal *Journal // logfile for flashtables. Status page 0 is never use, as flashtables are never committed, but only live in the logfile. Journal is created lazily when needed.

	//==== write cache: status ====

	wpc_permanent_tables_count int // number of permanent tables in wcache
	wpc_temporary_tables_count int // number of temporary tables in wcache (that is, flashtables)

	wpc_error *rsql.Error // the cache functions don't return directly an error, but put it in this field. The program must check it when appropriate, e.g. after each record insertion, and abort the transaction.

	Wpc_last_identity_exists bool  // for @@IDENTITY, true if target table of last INSERT has an IDENTITY column; else, false.
	Wpc_last_identity        int64 // keep value for @@IDENTITY
	Wpc_last_rowcount        int64 // keep value for @@ROWCOUNT
}

func (wcache *Wcache) Set_last_rowcount(val int64) {

	wcache.Wpc_last_rowcount = val
}

func (wcache *Wcache) Filebag() *Filebag {

	return wcache.wpc_filebag
}

func (wcache *Wcache) Permanent_tables_count() int {

	return wcache.wpc_permanent_tables_count
}

func (wcache *Wcache) get_journal_for(tblid Tblid_t) *Journal {

	// permanent table

	if tblid >= 0 {
		return wcache.wpc_journal
	}

	// flashtable

	if wcache.wpc_flashjournal == nil {
		wcache.wpc_flashjournal = must_create_new_flashjournal_and_open_it()
	}

	return wcache.wpc_flashjournal
}

func (wcache *Wcache) Close_flashjournal() {

	if wcache.wpc_flashjournal != nil && wcache.wpc_flashjournal.jr_file != nil {
		if err := wcache.wpc_flashjournal.jr_file.Close(); err != nil { // the file has been unlinked just after its creation. So, it will disappear now.
			log.Fatalf("FATAL ERROR during file.Close() for flashjournal: %v", err)
		}
		wcache.wpc_flashjournal.jr_file = nil
	}
}

// bpos_t is an index, from which it is possible to locate a page in gpc_element_array and gpc_ppage_array arrays.
//
//     The value 0 means that the page doesn't exist. It means that the Dlist belongs to the anchor of the list.
//     Indexes for existing pages are > 0.
//     BPOS_ILLEGAL means that the element has been removed from a list, but not inserted into another list yet.
//
// Using bpos_t instead of pointers to lookup pages, GC will not scan gpc_element_array and gpc_ppage_array, which leads to better performance.
//
type bpos_t uint64

const (
	BPOS_BASE    bpos_t = 1            // offset, so that first page's bpos_t is not 0. This way, if bpos_t value == 0, we want it to mean it points to an anchor, or head of the list. It is also used if the element has no attached ppage.
	BPOS_ILLEGAL bpos_t = ^(bpos_t(0)) // 0xFFFFFFFFFFFFFFFF      This value is used if an element is not in the list
)

func bpos(i uint64, j uint64) bpos_t {

	return bpos_t(i<<32+j) + BPOS_BASE // i is index of array, j is index of element in the chunk
}

type Dlist_type_t uint8

// power of two
const (
	PCE_ILLEGAL_LIST Dlist_type_t = 1 << iota // just used when an element is removed from a list
	PCE_PIN_LIST
	PCE_LRU_LIST
	PCE_SLIM_LIST // only for write cache. Page is stored on log file.
	PCE_FREE_LIST
)

func (t Dlist_type_t) String() string {
	var (
		s string
	)

	switch t {
	case PCE_ILLEGAL_LIST:
		s = "PCE_ILLEGAL_LIST"
	case PCE_PIN_LIST:
		s = "PCE_PIN_LIST"
	case PCE_LRU_LIST:
		s = "PCE_LRU_LIST"
	case PCE_SLIM_LIST:
		s = "PCE_SLIM_LIST"
	case PCE_FREE_LIST:
		s = "PCE_FREE_LIST"
	default:
		panic("impossible")
	}

	return s
}

type Version_t uint8

// power of two
const (
	PCE_FREED      Version_t = 0 // cache element that is free for reuse.
	PCE_PUBLIC     Version_t = 1 // only for global read cache. Page loaded with original table data. All PUBLIC pages have been commited. You must NEVER WRITE directly in a PUBLIC page.
	PCE_TRANSITORY Version_t = 2 // only for write cache. Non commited page that has been modified by a previous SQL statement in the current transation. You must NEVER WRITE directly in a TRANSITORY page.
	PCE_DRAFT      Version_t = 4 // only for write cache. Non commited page that is beeing modified by the current SQL statement. You are allowed to write in a DRAFT page.
)

func (v Version_t) String() string {
	var (
		s string
	)

	switch v {
	case PCE_FREED:
		s = "PCE_FREED"
	case PCE_PUBLIC:
		s = "PCE_PUBLIC"
	case PCE_TRANSITORY:
		s = "PCE_TRANSITORY"
	case PCE_DRAFT:
		s = "PCE_DRAFT"
	default:
		panic("impossible")
	}

	return s
}

type Hanger_dlist_type_t uint8

// power of two
const (
	HANGERS_WITH_BLANK_PPAGE_LIST Hanger_dlist_type_t = 1 // only used for Hangers
	HANGERS_WITH_NO_PPAGE_LIST    Hanger_dlist_type_t = 2 // only used for Hangers
)

// Circular list
//     Value 0 for Prev or Next means that it points to the anchor of the circular list (an anchor is just an isolated Dlist, not a member of Page_cache_element or Page struct).
//     Value BPOS_ILLEGAL means that it doesn't belong to a list.
//
type Dlist struct {
	Prev bpos_t
	Next bpos_t
}

type Dlist_anchor struct {
	Dlist
	Count uint64
}

func (anchor *Dlist_anchor) Is_empty() bool {
	if anchor.Count == 0 {
		assert(anchor.Dlist.Prev == 0 && anchor.Dlist.Next == 0)
		return true
	}

	return false
}

type Hlist Dlist // Hlist is same as Dlist, but for the buckets in hashtable. This way, we are sure not to inadvertently mix the lists.

type Rlist Dlist // Rlist is same as Dlist, but for chaining the DRAFT list. This way, we are sure not to inadvertently mix the lists.

type Rlist_anchor struct {
	Rlist
	Count uint64
}

type Tlist Dlist // Tlist is same as Dlist, but for chaining the pce_same_tblid list. This way, we are sure not to inadvertently mix the lists.

type Tlist_anchor struct {
	Tlist
	Count uint64
}

type Filebag_mode_t uint8

const (
	FB_FILE_FOR_WRITE Filebag_mode_t = iota + 1 // passed as argument for filebag.get_file. Sets the fbe_dirty flag to true.
	FB_FILE_FOR_READ
)

// each session must have its own filebag.
// Each ordinary table that is accessed has an entry pointing to the *os.File, which is the file containing the records of the table (these records are the PUBLIC version).
type Filebag struct {
	private_ppage_buffer *Page // buffer into which a page is read from disk, before being copied into global read cache. It is also used by write cache.

	// data files for ordinary tables

	bag map[Tblid_t]*Fb_entry

	minicache_tblid    Tblid_t   // last tblid accessed
	minicache_fb_entry *Fb_entry // entry for the tblid hereabove
}

type Fb_entry struct {
	fbe_file           *os.File
	fbe_need_sync_flag bool // true if the file has been written to, and has not been fsynced yet
}

func New_Filebag() *Filebag {

	filebag := &Filebag{}

	filebag.private_ppage_buffer = &Page{}

	filebag.bag = make(map[Tblid_t]*Fb_entry)

	return filebag
}

func (wcache *Wcache) Debug_print_filebag() {

	rsql.Println("------ Filebag ------")

	for tblid, _ := range wcache.wpc_filebag.bag {
		rsql.Printf("   %d\n", tblid)
	}
}

func (filebag *Filebag) get_file(tabledef *rsql.Tabledef, mode Filebag_mode_t) *os.File {
	var (
		err      error
		fb_entry *Fb_entry
		file     *os.File
		tblid    Tblid_t
		ok       bool
	)

	tblid = Tblid_t(tabledef.Td_tblid)

	// get fb_entry

	if filebag.minicache_tblid == tblid {
		fb_entry = filebag.minicache_fb_entry
	} else {
		if fb_entry, ok = filebag.bag[tblid]; ok == false {
			fb_entry = &Fb_entry{}
			filebag.bag[tblid] = fb_entry
		}

		filebag.minicache_tblid = tblid // refresh minicache
		filebag.minicache_fb_entry = fb_entry
	}

	// if file is closed, open it

	if fb_entry.fbe_file == nil {
		assert(fb_entry.fbe_need_sync_flag == false)

		if file, err = os.OpenFile(tabledef.Td_file_path, os.O_RDWR, 0); err != nil {
			log.Panicf("FATAL ERROR, OpenFile(\"%s\") %d %s: %v", tabledef.Td_file_path, tabledef.Td_tblid, tabledef.Td_index_name, err)
		}
		fb_entry.fbe_file = file
	}

	// file will be written to, so it will need fsync()

	if mode == FB_FILE_FOR_WRITE {
		if fb_entry.fbe_need_sync_flag == false {
			fb_entry.fbe_need_sync_flag = true
		}
	}

	return fb_entry.fbe_file
}

func (filebag *Filebag) get_file_for_ppage_write(td_type rsql.Td_type_t, dbid Dbid_t, schid Schid_t, tblid Tblid_t) *os.File {
	var (
		err         error
		fb_entry    *Fb_entry
		file        *os.File
		prefix_path string
		file_path   string
		ok          bool
	)

	// get fb_entry

	if filebag.minicache_tblid == tblid {
		fb_entry = filebag.minicache_fb_entry
	} else {
		if fb_entry, ok = filebag.bag[tblid]; ok == false {
			fb_entry = &Fb_entry{}
			filebag.bag[tblid] = fb_entry
		}

		filebag.minicache_tblid = tblid // refresh minicache
		filebag.minicache_fb_entry = fb_entry
	}

	// if file is closed, open it

	if fb_entry.fbe_file == nil {
		assert(fb_entry.fbe_need_sync_flag == false)

		switch td_type {
		case rsql.TD_TYPE_BASE_TABLE:
			prefix_path = rsql.DIRECTORY_DATA
		case rsql.TD_TYPE_INDEX_TABLE:
			prefix_path = rsql.DIRECTORY_INDEX
		default:
			panic("impossible")
		}

		file_path = rsql.Tabledef_path(prefix_path, int64(dbid), int64(schid), int64(tblid))

		if file, err = os.OpenFile(file_path, os.O_RDWR, 0); err != nil {
			log.Panicf("FATAL ERROR, OpenFile(\"%s\") %d: %v", file_path, tblid, err)
		}

		fb_entry.fbe_file = file
	}

	// file will be written to, so it will need fsync()

	if fb_entry.fbe_need_sync_flag == false {
		fb_entry.fbe_need_sync_flag = true
	}

	return fb_entry.fbe_file
}

// write_ppage_to_datafile writes ppage in tblid's table file, at page_no offset.
//
func (filebag *Filebag) write_ppage_to_datafile(ppage *Page) {
	var (
		err         error
		file        *os.File
		prefix_path string
		file_path   string
		td_type     rsql.Td_type_t
		dbid        Dbid_t
		schid       Schid_t
		base_gtblid Tblid_t
		tblid       Tblid_t
		page_no     Page_no_t
		offset      int64
	)

	td_type = ppage.Pg_td_type()
	dbid = ppage.Pg_dbid()
	schid = ppage.Pg_schid()
	base_gtblid = ppage.Pg_base_gtblid()
	tblid = ppage.Pg_tblid()
	page_no = ppage.Pg_no()
	offset = int64(page_no * PAGE_SIZE)

	file = filebag.get_file_for_ppage_write(td_type, dbid, schid, tblid) // file is opened if needed

	//println("------------------------------------------------------------------------------------------------------- WRITE ---------", page_no)

	if _, err = file.WriteAt(ppage.Pg_canvas[:], offset); err != nil { // WriteAt returns a non-nil error when n != len(b)
		switch td_type {
		case rsql.TD_TYPE_BASE_TABLE:
			prefix_path = rsql.DIRECTORY_DATA
		case rsql.TD_TYPE_INDEX_TABLE:
			prefix_path = rsql.DIRECTORY_INDEX
		default:
			panic("impossible")
		}
		file_path = rsql.Tabledef_path(prefix_path, int64(dbid), int64(schid), int64(tblid))
		log.Panicf("FATAL ERROR, WriteAt() for \"%s\" table %d, base table %d: %v", file_path, tblid, base_gtblid, err)
	}
}

func (filebag *Filebag) Shrink_file(tabledef *rsql.Tabledef, size_in_pages int64) {
	var (
		file *os.File
	)

	file = filebag.get_file(tabledef, FB_FILE_FOR_WRITE)

	if err := file.Truncate(size_in_pages * PAGE_SIZE); err != nil {
		log.Panicf("FATAL ERROR, file.Truncate(\"%s\") %d %s: %v", tabledef.Td_file_path, tabledef.Td_tblid, tabledef.Td_index_name, err)
	}
}

func (filebag *Filebag) Sync_modified_files() {

	for tblid, fb_entry := range filebag.bag {
		if fb_entry.fbe_need_sync_flag == false {
			continue
		}

		file := fb_entry.fbe_file
		assert(file != nil)

		if err := file.Sync(); err != nil {
			log.Fatalf("FATAL ERROR during file.Sync() for table %d: %v", tblid, err)
		}
		fb_entry.fbe_need_sync_flag = false
	}
}

func (filebag *Filebag) check_sync_flag_is_false_for_ordinary_table_files() {

	for tblid, fb_entry := range filebag.bag {
		if tblid < 0 || fb_entry.fbe_need_sync_flag == false {
			continue
		}

		panic("ROLLBACK: fbe_need_sync_flag should be false")
	}
}

func (filebag *Filebag) Close_all() {

	for tblid, fb_entry := range filebag.bag {
		file := fb_entry.fbe_file
		assert(file != nil)
		assert(fb_entry.fbe_need_sync_flag == false)

		if err := file.Close(); err != nil {
			log.Fatalf("FATAL ERROR during file.Close() for table %d: %v", tblid, err)
		}
		fb_entry.fbe_file = nil

		delete(filebag.bag, tblid)
	}

	assert(len(filebag.bag) == 0)

	// reset minicache

	filebag.minicache_tblid = 0 // invalid tblid
	filebag.minicache_fb_entry = nil
}

// Close_file closes a file before it is deleted by DROP TABLE, DROP INDEX, etc.
//
// On Linux, there is NO NEED to use this function. This function has only been written because of Windows.
// Indeed, on Windows, a file cannot be removed if it is still open.
//
func (filebag *Filebag) Close_file(tabledef *rsql.Tabledef) {
	var (
		fb_entry *Fb_entry
		tblid    Tblid_t
		ok       bool
	)

	if filebag == nil { // during server startup, half created tables and indexes are removed. A dummy Context is passed with no Filebag.
		return
	}

	tblid = Tblid_t(tabledef.Td_tblid)

	// get fb_entry

	if fb_entry, ok = filebag.bag[tblid]; ok == false {
		return
	}

	// sync and close file

	if fb_entry.fbe_file != nil {
		file := fb_entry.fbe_file

		// sync

		if fb_entry.fbe_need_sync_flag {
			if err := file.Sync(); err != nil {
				log.Fatalf("FATAL ERROR during file.Sync() for table %d: %v", tblid, err)
			}
			fb_entry.fbe_need_sync_flag = false
		}

		// close

		if err := file.Close(); err != nil {
			log.Fatalf("FATAL ERROR during file.Close() for table %d: %v", tblid, err)
		}
		fb_entry.fbe_file = nil
	}

	// delete entry from map

	delete(filebag.bag, tblid)

	// reset minicache

	filebag.minicache_tblid = 0 // invalid tblid
	filebag.minicache_fb_entry = nil
}

type Log_status_t uint8

const (
	LOG_STATUS_CLEAR              Log_status_t = 0
	LOG_STATUS_READY_FOR_TRANSFER Log_status_t = 1 // data modification are now durable, as they have been written to logfile. They are being copied from logfile to table files, and when this is done, status is set to LOG_STATUS_CLEAR again.
)

func (log_status Log_status_t) String() string {

	switch log_status {
	case LOG_STATUS_CLEAR:
		return "LOG_STATUS_CLEAR"
	case LOG_STATUS_READY_FOR_TRANSFER:
		return "LOG_STATUS_READY_FOR_TRANSFER"
	default:
		panic("impossible")
	}
}

type Journal_id_t int64 // journals for workers have jr_id == worker_id >= 0, and journals for temporary tables have jr_id == -1.

type Journal struct {
	jr_id   Journal_id_t // journals for workers have jr_id == worker_id >= 0, and journals for temporary tables have jr_id == -1.
	jr_path string
	jr_file *os.File // nil if file is closed. For flashjournals, it is a temporary file.

	jr_logfile_ppage_count         uint64         // size of used portion of logfile, in page count. When not inside a transaction, the value is 1. Note: logpage 0 is the log status page. The logfile capacity (real size of the file) may be larger.
	jr_logfile_ppage_count_highest uint64         // updated before jr_logfile_ppage_count is reset to 1, to keep its highest value. It is used to truncate logfile if too large.
	jr_logfile_freed_pages         []Logpage_no_t // pages in logfile that have been freed. They can be reused when a new Page_cache_element is requested. Outside a transaction, list is empty.
	jr_in_use                      bool           // if true, the worker worker_id is using this journal. It is used to check that a journal is only used by one worker. Not used for flashjournals.
}

// check_journal_is_STATUS_CLEAR checks if status is LOG_STATUS_CLEAR and log file used page count is 1.
// Else, it panics.
//
func check_journal_is_STATUS_CLEAR(journal *Journal) {
	var (
		err            error
		logfile_sector Logfile_sector
		n              int
	)

	if _, err := journal.jr_file.Seek(0, 0); err != nil {
		log.Panicf("FATAL ERROR, read log file: %v", err)
	}

	if n, err = io.ReadFull(journal.jr_file, logfile_sector[:]); err != nil {
		log.Panicf("FATAL ERROR, read log file: %v", err)
	}
	assert(n == LOGFILE_SECTOR_SIZE)

	if _, err := journal.jr_file.Seek(0, 0); err != nil {
		log.Panicf("FATAL ERROR, read log file: %v", err)
	}

	if logfile_sector.Plog_status() != LOG_STATUS_CLEAR {
		log.Panicf("FATAL ERROR, log file status is: %d, it should be LOG_STATUS_CLEAR", logfile_sector.Plog_status())
	}

	if logfile_sector.Plog_count() != 1 {
		log.Panicf("FATAL ERROR, log file used page count is: %d, it should be 1", logfile_sector.Plog_count())
	}
}

//==================== collection of journals for workers ========================

// Workers_journals is the collection of journals for the workers.
//
type Workers_journals struct {
	wj_mutex sync.Mutex
	wj_map   map[Journal_id_t]*Journal
}

// Initialize puts all the existing journal files into the Workers_journals collection.
// Note that all journal files have already been checked for data restoration, and status MUST have been set to LOG_STATUS_CLEAR and count of log pages in use is 1.
//
func (w *Workers_journals) Initialize() {
	var (
		err        error
		fileinfos  []os.FileInfo
		file_names []string
		journal_id uint64
	)

	// create map

	w.wj_map = make(map[Journal_id_t]*Journal)

	// read all entries in journal directory

	if fileinfos, err = ioutil.ReadDir(rsql.DIRECTORY_JOURNALS); err != nil {
		log.Fatalf("Init: cannot read journals: %s", err)
	}

	// check they are all regular files

	for _, finfo := range fileinfos {
		if finfo.Mode().IsRegular() == false {
			log.Fatalf("Init: file \"%s\" is not a regular file", finfo.Name())
		}

		file_names = append(file_names, finfo.Name())
	}

	// fill in collection of journals

	for _, filename := range file_names {
		if strings.HasPrefix(filename, rsql.JOURNAL_FILENAME_PREFIX) == false {
			log.Fatalf("Init: file \"%s\" is not a journal file. Prefix should be \"%s\".", filename, rsql.JOURNAL_FILENAME_PREFIX)
		}

		if strings.HasSuffix(filename, rsql.JOURNAL_FILENAME_EXTENSION) == false {
			log.Fatalf("Init: file \"%s\" is not a journal file. Extension should be \"%s\".", filename, rsql.JOURNAL_FILENAME_EXTENSION)
		}

		journal_id_string := filename[len(rsql.JOURNAL_FILENAME_PREFIX) : len(filename)-len(rsql.JOURNAL_FILENAME_EXTENSION)]

		if len(journal_id_string) == 0 {
			log.Fatalf("Init: file \"%s\" is not a journal file. Its filename has no journal id.", filename)
		}

		if journal_id, err = strconv.ParseUint(journal_id_string, 10, 64); err != nil {
			log.Fatalf("Init: file \"%s\" is not a journal file. Journal id is not numeric: %v)", filename, err)
		}

		journal_path := rsql.Journal_path(rsql.DIRECTORY_JOURNALS, int64(journal_id))

		// create entries in map

		journal := &Journal{jr_id: Journal_id_t(journal_id), jr_path: journal_path, jr_logfile_ppage_count: 1, jr_logfile_ppage_count_highest: 1, jr_logfile_freed_pages: make([]Logpage_no_t, 0, LOGFILE_FREED_PAGES_DEFAULT_CAPACITY), jr_in_use: false}

		if _, ok := w.wj_map[Journal_id_t(journal_id)]; ok == true {
			log.Fatalf("Init: journal file \"%s\" has duplicate filename", filename)
		}

		w.wj_map[Journal_id_t(journal_id)] = journal
	}

	// rollforward LOG_STATUS_READY_FOR_TRANSFER, which pages must be copied to table files

	list_journal_id := make([]int, 0, 20) // 20 is good enough

	for journal_id, _ := range w.wj_map {
		list_journal_id = append(list_journal_id, int(journal_id))
		sort.Ints(list_journal_id)
	}

	for _, journal_id := range list_journal_id {
		rollforward_logfile_at_startup(uint64(journal_id)) // also checks whether journal file is corrupted. Logfiles are truncated to 1 page (status page).
	}
}

// Get_journal returns the requested journal.
// It creates a new one if not exists.
//
func (w *Workers_journals) Get_journal(journal_id Journal_id_t) *Journal {
	var (
		err     error
		journal *Journal
		ok      bool
	)

	w.wj_mutex.Lock() //=== lock journal collection ===
	defer w.wj_mutex.Unlock()

	journal, ok = w.wj_map[journal_id] // Workers_journals has been populated at initialization with all existing journals
	if ok == true {
		assert(journal.jr_id == journal_id)
		assert(journal.jr_logfile_ppage_count == 1)
		assert(len(journal.jr_logfile_freed_pages) == 0)
		assert(journal.jr_in_use == false)
		journal.jr_in_use = true

		if journal.jr_file == nil { // if file is closed, open it
			var logfile *os.File
			if logfile, err = os.OpenFile(journal.jr_path, os.O_RDWR, 0); err != nil {
				log.Panicf("FATAL ERROR, log file \"%s\" open failed: %v", journal.jr_path, err)
			}

			journal.jr_file = logfile
		}

		delete(w.wj_map, journal_id)

		return journal // ----> journal found, return it
	}

	// journal not found. Create a new journal and logfile.

	journal = must_create_new_journal_and_open_it(journal_id)

	return journal // ----> return new journal created
}

// Release_journal puts the journal back in map, when the worker terminates.
//
func (w *Workers_journals) Release_journal(journal *Journal) {

	// truncate logfile if too big

	assert(journal.jr_file != nil)

	if journal.jr_logfile_ppage_count == 1 { // can be != 1 if an error has been raised or occurred in the session
		if journal.jr_logfile_ppage_count_highest > LOGFILE_MAX_PAGE_COUNT_BEFORE_TRUNCATION {
			if err := journal.jr_file.Truncate(LOGFILE_PAGE_COUNT_AFTER_TRUNCATION * PAGE_SIZE); err != nil {
				log.Panicf("FATAL ERROR, log file \"%s\" truncate failed: %v", journal.jr_path, err)
			}

			journal.jr_logfile_ppage_count_highest = 1
		}
	}

	// reinit journal (COMMIT and ROLLBACK have already set jr_logfile_ppage_count = 1 and reset jr_logfile_freed_pages. But this has not been done if an error occurred in the session.)

	journal.jr_logfile_ppage_count = 1
	journal.jr_logfile_freed_pages = make([]Logpage_no_t, 0, LOGFILE_FREED_PAGES_DEFAULT_CAPACITY)
	journal.jr_in_use = false

	// close the journal. In fact, IT IS NOT NEEDED, but closing it makes no difference in performance.

	journal.jr_file.Close()
	journal.jr_file = nil

	//=== put journal back in journal collection ===

	w.wj_mutex.Lock() //=== lock journal collection ===

	if _, ok := w.wj_map[journal.jr_id]; ok == true {
		panic(fmt.Sprintf("Release_journal failed for %s. Already exists in map.", journal.jr_path))
	}

	w.wj_map[Journal_id_t(journal.jr_id)] = journal

	w.wj_mutex.Unlock() //=== unlock journal collection ===
}

// Reset_journal resets the journal, so that it can be reused by the next batch in the same session.
//
func Reset_journal_for_resuse_in_same_session(journal *Journal) {

	// truncate logfile if too big

	assert(journal.jr_file != nil)

	if journal.jr_logfile_ppage_count == 1 { // can be != 1 if an error has been raised or occurred in the session
		if journal.jr_logfile_ppage_count_highest > LOGFILE_MAX_PAGE_COUNT_BEFORE_TRUNCATION {
			if err := journal.jr_file.Truncate(LOGFILE_PAGE_COUNT_AFTER_TRUNCATION * PAGE_SIZE); err != nil {
				log.Panicf("FATAL ERROR, log file \"%s\" truncate failed: %v", journal.jr_path, err)
			}

			journal.jr_logfile_ppage_count_highest = 1
		}
	}

	// reinit journal (COMMIT and ROLLBACK have already set jr_logfile_ppage_count = 1 and reset jr_logfile_freed_pages. But this has not been done if an error occurred in the session.)

	journal.jr_logfile_ppage_count = 1
	journal.jr_logfile_freed_pages = make([]Logpage_no_t, 0, LOGFILE_FREED_PAGES_DEFAULT_CAPACITY)
}

// must_create_new_journal_and_open_it creates a new journal with new open logfile.
//
func must_create_new_journal_and_open_it(journal_id Journal_id_t) *Journal {
	var (
		err            error
		journal        *Journal
		logfile        *os.File
		journal_path   string
		page_0         *Page
		logfile_sector Logfile_sector
		n              int
	)

	// create logfile

	page_0 = &Page{}

	logfile_sector.Set_plog_status(LOG_STATUS_CLEAR)
	logfile_sector.Set_plog_count(1)
	logfile_sector.Set_plog_count_bak(0)
	copy(page_0.Pg_canvas[:], logfile_sector[:])

	journal_path = rsql.Journal_path(rsql.DIRECTORY_JOURNALS, int64(journal_id))

	if logfile, err = os.OpenFile(journal_path, os.O_RDWR|os.O_CREATE|os.O_EXCL, rsql.JOURNALFILE_PERM); err != nil { // must create logfile
		log.Panicf("FATAL ERROR, log file creation failed: %v", err)
	}

	if n, err = logfile.WriteAt(page_0.Pg_canvas[:], 0); err != nil { // writes log status and logfile_ppage_count at beginning of log file.
		log.Panicf("FATAL ERROR, log file creation failed: %v", err)
	}
	assert(n == PAGE_SIZE)

	if err = logfile.Sync(); err != nil { // because logfile.Close() doesn't guarantee fsync(). See man page "man close"
		log.Panicf("FATAL ERROR, log file creation failed: %v", err)
	}

	// create Journal

	journal = &Journal{jr_id: journal_id, jr_path: journal_path}
	journal.jr_file = logfile
	journal.jr_logfile_ppage_count = 1 // no log page is used yet. The first page available is no 1. Log page 0 is for logfile status.
	journal.jr_logfile_ppage_count_highest = 1
	journal.jr_logfile_freed_pages = make([]Logpage_no_t, 0, LOGFILE_FREED_PAGES_DEFAULT_CAPACITY)
	journal.jr_in_use = true

	return journal
}

// must_create_new_flashjournal_and_open_it creates a new journal for the flashtables, and new open logfile.
//
func must_create_new_flashjournal_and_open_it() *Journal {
	var (
		journal *Journal
		logfile *os.File
	)

	// create logfile

	// NOTE: flashtables don't use logfile first sector
	//       because first sector is used for 2-phase commit. This is not necessary for flashtables, as Sync() is not called for flash logfile or datafile.

	logfile = rsql.Must_create_and_open_flashfile("flashlog_")

	// create Journal

	journal = &Journal{jr_id: -1, jr_path: logfile.Name()}
	journal.jr_file = logfile
	journal.jr_logfile_ppage_count = 1 // no log page is used yet. The first page available is no 1. Log page 0 is for logfile status.
	journal.jr_logfile_ppage_count_highest = 1
	journal.jr_logfile_freed_pages = make([]Logpage_no_t, 0, LOGFILE_FREED_PAGES_DEFAULT_CAPACITY)
	journal.jr_in_use = true

	return journal
}

//==================== misc ========================

// compute_hashval_tblid_page_no is the hashing function for cached pages, to put them in the proper hashtable bucket.
//
// For write cache, PCE_DRAFT and PCE_TRANSITORY versions of the same <tblid,page_no> page are put in the same bucket.
//
func compute_hashval_tblid_page_no(tblid Tblid_t, page_no Page_no_t) uint64 {
	var (
		hash uint64
	)

	hash = uint64(tblid)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += uint64(page_no)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}
