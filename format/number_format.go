package format

import (
	"fmt"
	"strconv"

	"rsql/lang"
	"rsql/quad"
)

/************************************************************************/
/*                                                                      */
/*                         number formatting                            */
/*                                                                      */
/************************************************************************/

// Numberfmt is the object that is used to output a number with a format string.
// It contains information from format string, like digit placeholders count.
// It also contains the number as string, before formatting.
//
//     Number formatting is made in two passes.
//       - First pass: Get_format_info() parses the format string to count the number of placeholders. Result is put in Numberfmt object.
//       - Fill_int64(), Fill_float64(), or Fill_MyQuad() puts the number to format in Numberfmt as plain string.
//       - Set_langinfo() puts decimal point symbol and group separator symbol in Numberfmt.
//       - Second pass: Append_formatted_number() makes a second parsing to replace placeholders by digits, writing result into target byte slice.
//
// Normally, errors occur only when number is in exponent notation, or Nan or Infinity. But this should not happen with the numbers being in SQL INT, BIGINT, FLOAT or NUMERIC range.
//
type Numberfmt struct {
	nbuff_default_storage           [50]byte // default nbuff storage. Size is arbitrary.
	state_sign_written              bool     // state value, used during creation of the formatted string by Append_formatted_number(). Default is false. When first digit is written, sign is output and this flag is set to true.
	state_inside_integral_part      bool     // same. Default is true, placeholder '.' sets it to false.
	state_must_output_digit_flag    bool     // same. Default is false, first '0' in integral part sets it to true.
	state_next_integral_digit_index int      // same. Index in part_integral of next digit to output.
	state_next_placeholder_index    int      // same. Index of next '0' or '#' placeholder in format string. When in integral part, value decreases from nbfmt.part_integral_digit_placeholder_count-1 to -1. When in fractional part, value is 0, 1, 2, etc.
	err                             error    // non-nil if anything goes wrong during number formatting

	// these fields are filled in by Get_format_info() and Set_langinfo()
	part_integral_digit_placeholder_count   int            // number of digit placeholders in integral part
	part_fractional_digit_placeholder_count int            // number of digit placeholders in fractional part
	fractional_point_pos                    int            // position of fractional point in format string. Default is -1
	langinfo                                *lang.Langinfo // langinfo contains grouping symbol, group widths, decimal point symbol.
	decimal_point_symbol                    rune           // decimal point symbol
	group_separator_symbol                  rune           // group separator symbol
	grouping_flag                           bool           // true if grouping symbol must be output
	grouping_array                          [40]bool       // contains true at each grouping position for the chosen langinfo. Else, contains false. Size is arbitrary. No group separator are printed for digits with position beyond this array.

	// these fields are filled in by Fill_int64(), Fill_float64(), and Fill_MyQuad().
	sign            bool   // true if < 0
	nbuff           []byte // absolute value of the number to format
	part_integral   []byte // integral part of the number to format, without sign, without leading zeroes. It is a subslice of nbuff.
	part_fractional []byte // fractional part of the number to format, without trailing zeroes. It is a subslice of nbuff. Length is less or equal to 'part_fractional_digit_placeholder_count'.
}

func (nbfmt *Numberfmt) String() string {

	return fmt.Sprintf("part_integral_digit_placeholder_count: %d, part_fractional_digit_placeholder_count: %d, sign %t, part_integral [%s], part_fractional [%s],", nbfmt.part_integral_digit_placeholder_count, nbfmt.part_fractional_digit_placeholder_count, nbfmt.sign, nbfmt.part_integral, nbfmt.part_fractional)
}

// Get_format_info counts the number of digit placeholders.
//
func (nbfmt *Numberfmt) Get_format_info(format_string []byte) {
	var (
		part_integral_digit_placeholder_count   int  // count of '0' and '#' in integral part
		part_fractional_digit_placeholder_count int  // count of '0' and '#' in fractional part
		fractional_point_pos                    int  // position in format string of the fractional point. If none, -1.
		last_digit_placeholder_pos              int  // when parsing integral part, contains position of last 0 or #
		grouping_flag                           bool // true if ',' is found.

		quoted_string_mode bool // true if current char is inside quote ' or "
		escape_flag        bool // true if last character was backslash
		escape_quote       byte // last opening single-quote or double-quote
	)

	fractional_point_pos = -1
	last_digit_placeholder_pos = -1
	s := format_string

	// scan the format string to get the formatting specifications

	for i, c := range s {
		if quoted_string_mode == true {
			if c == escape_quote && escape_flag == false { // end of quoted string
				quoted_string_mode = false
				continue
			}
			if c == '\\' { // if  escape backslash, just skip it
				escape_flag = true
				continue
			}
			escape_flag = false
			continue
		}

		if escape_flag == true { // skip escaped character
			escape_flag = false
			continue
		}

		if c == '"' || c == '\'' { // start of quoted string
			quoted_string_mode = true
			escape_quote = c
			continue
		}

		if c == '\\' { // escape character
			escape_flag = true
			continue
		}

		// here, we are not in quoted string mode, and current character is not escaped

		// if scanning integral part of the number formatting

		if fractional_point_pos < 0 {
			switch c {
			case '0', '#': // digit placeholder
				last_digit_placeholder_pos = i
				part_integral_digit_placeholder_count++

			case ',': // grouping symbol placeholder
				if i > 0 && i < len(s)-1 {
					if i-1 == last_digit_placeholder_pos && (s[i+1] == '0' || s[i+1] == '#') { // grouping symbol must be between two digit placeholders
						grouping_flag = true
					}
				}

			case '.': // fractional point placeholder
				if (i > 0 && i-1 == last_digit_placeholder_pos) || (i < len(s)-1 && (s[i+1] == '0' || s[i+1] == '#')) { // a digit must exist just before or after the fractional point
					fractional_point_pos = i
				}
			}
			continue
		}

		// if scanning fractional part of the number formatting

		switch c {
		case '0', '#':
			part_fractional_digit_placeholder_count++
		}

	} // end loop

	nbfmt.part_integral_digit_placeholder_count = part_integral_digit_placeholder_count

	if part_fractional_digit_placeholder_count > 50 { // we arbitrarily set a maximum for part_fractional_digit_placeholder_count. It can be any value, and 50 seems large enough.
		part_fractional_digit_placeholder_count = 50
	}
	nbfmt.part_fractional_digit_placeholder_count = part_fractional_digit_placeholder_count

	nbfmt.fractional_point_pos = fractional_point_pos
	nbfmt.grouping_flag = grouping_flag
}

func (nbfmt *Numberfmt) Fill_int64(n int64) {
	var (
		sign          bool
		nbuff         []byte
		part_integral []byte
	)

	nbuff = nbfmt.nbuff_default_storage[:0]

	nbuff = strconv.AppendInt(nbuff[0:], n, 10)
	if nbuff[0] == '-' {
		sign = true
		nbuff = nbuff[1:] // skip sign
	}

	part_integral = nbuff

	for len(part_integral) > 0 { // discard leading zeroes
		if part_integral[0] != '0' {
			break
		}
		part_integral = part_integral[1:]
	}

	for _, c := range part_integral { // error should not happen
		if c < '0' || c > '9' {
			nbfmt.err = fmt.Errorf("Number formatting: non-digit character found <%s>", nbuff)
			return
		}
	}

	nbfmt.sign = sign
	nbfmt.nbuff = nbuff
	nbfmt.part_integral = part_integral
	nbfmt.part_fractional = nil
}

func (nbfmt *Numberfmt) Fill_float64(n float64) {
	var (
		sign            bool
		nbuff           []byte
		part_integral   []byte
		part_fractional []byte
	)

	nbuff = nbfmt.nbuff_default_storage[:0]

	nbuff = strconv.AppendFloat(nbuff[0:], n, 'f', nbfmt.part_fractional_digit_placeholder_count, 64) // round-half-to-even (unlike MS SQL Server, which rounds round-half-up)
	if nbuff[0] == '-' {
		sign = true
		nbuff = nbuff[1:] // skip sign
	}

	part_integral = nbuff
	part_fractional = nil

	for i := len(nbuff) - 1; i >= 0; i-- { // find fractional point if any
		if nbuff[i] == '.' {
			part_integral = nbuff[:i]
			part_fractional = nbuff[i+1:]
			break
		}
	}

	for len(part_integral) > 0 { // discard leading zeroes
		if part_integral[0] != '0' {
			break
		}
		part_integral = part_integral[1:]
	}

	for { // discard trailing zeroes
		last_elem_pos := len(part_fractional) - 1
		if last_elem_pos >= 0 && part_fractional[last_elem_pos] == '0' {
			part_fractional = part_fractional[:last_elem_pos]
			continue
		}
		break
	}

	for _, c := range part_integral { // error should not happen
		if c < '0' || c > '9' {
			nbfmt.err = fmt.Errorf("Number formatting: non-digit character found <%s>", nbuff)
			return
		}
	}

	for _, c := range part_fractional { // error should not happen
		if c < '0' || c > '9' {
			nbfmt.err = fmt.Errorf("Number formatting: non-digit character found <%s>", nbuff)
			return
		}
	}

	nbfmt.sign = sign
	nbfmt.nbuff = nbuff
	nbfmt.part_integral = part_integral
	nbfmt.part_fractional = part_fractional
}

func (nbfmt *Numberfmt) Fill_MyQuad(n *quad.MyQuad) {
	var (
		sign            bool
		nbuff           []byte
		part_integral   []byte
		part_fractional []byte
		r               quad.MyQuad
	)

	nbuff = nbfmt.nbuff_default_storage[:0]

	if err := quad.Round_for_formatting(&r, n, int32(nbfmt.part_fractional_digit_placeholder_count)); err != nil { // round MyQuad
		nbfmt.err = fmt.Errorf("Number formatting: error occurred <%s>", nbuff)
		return
	}

	nbuff = quad.AppendQuad(nbuff[0:], &r) // convert MyQuad to string. In the NUMERIC range, exponent notation is not used
	if nbuff[0] == '-' {
		sign = true
		nbuff = nbuff[1:] // skip sign
	}

	part_integral = nbuff
	part_fractional = nil

	for i := len(nbuff) - 1; i >= 0; i-- { // find fractional point if any
		if nbuff[i] == '.' {
			part_integral = nbuff[:i]
			part_fractional = nbuff[i+1:]
			break
		}
	}

	for len(part_integral) > 0 { // discard leading zeroes
		if part_integral[0] != '0' {
			break
		}
		part_integral = part_integral[1:]
	}

	for { // discard trailing zeroes
		last_elem_pos := len(part_fractional) - 1
		if last_elem_pos >= 0 && part_fractional[last_elem_pos] == '0' {
			part_fractional = part_fractional[:last_elem_pos]
			continue
		}
		break
	}

	for _, c := range part_integral { // error should not happen for MyQuad in NUMERIC range
		if c < '0' || c > '9' {
			nbfmt.err = fmt.Errorf("Number formatting: non-digit character found <%s>", nbuff)
			return
		}
	}

	for _, c := range part_fractional { // error should not happen for MyQuad in NUMERIC range
		if c < '0' || c > '9' {
			nbfmt.err = fmt.Errorf("Number formatting: non-digit character found <%s>", nbuff)
			return
		}
	}

	nbfmt.sign = sign
	nbfmt.nbuff = nbuff
	nbfmt.part_integral = part_integral
	nbfmt.part_fractional = part_fractional
}

func (nbfmt *Numberfmt) Set_langinfo(langinfo *lang.Langinfo) {
	var (
		i                 int
		grouping_width    int
		grouping_pos      int
		number_grouping   []int
		max_separator_pos int
	)

	if nbfmt.err != nil { // if error detected, do nothing
		return
	}

	nbfmt.langinfo = langinfo
	nbfmt.decimal_point_symbol = langinfo.Lng_number_decimal_point
	nbfmt.group_separator_symbol = langinfo.Lng_number_group_separator

	nbfmt.state_sign_written = false
	nbfmt.state_inside_integral_part = true
	nbfmt.state_must_output_digit_flag = false
	nbfmt.state_next_integral_digit_index = len(nbfmt.part_integral) - 1
	nbfmt.state_next_placeholder_index = nbfmt.part_integral_digit_placeholder_count - 1

	// if grouping_flag, fill in nbfmt.grouping_array

	if nbfmt.grouping_flag {
		number_grouping = langinfo.Lng_number_grouping

		if len(number_grouping) == 0 { // if no grouping info, return.
			return
		}

		max_separator_pos = len(nbfmt.part_integral) - 1
		if max_separator_pos < nbfmt.part_integral_digit_placeholder_count-1 {
			max_separator_pos = nbfmt.part_integral_digit_placeholder_count - 1
		}
		if max_separator_pos > len(nbfmt.grouping_array)-1 {
			max_separator_pos = len(nbfmt.grouping_array) - 1
		}

		for {
			grouping_width = number_grouping[i]
			if grouping_width == 0 { // if 0, no more grouping for the remaining. Return.
				return
			}
			grouping_pos += grouping_width
			if grouping_pos > max_separator_pos {
				return
			}
			nbfmt.grouping_array[grouping_pos] = true
			i++
			if i >= len(number_grouping) { // repeat last group width
				i--
			}
		}
	}
}

func (nbfmt *Numberfmt) has_group_separator(pos int) bool {

	if pos >= len(nbfmt.grouping_array) {
		return false
	}

	return nbfmt.grouping_array[pos]
}

func (nbfmt *Numberfmt) append_placeholder(dest []byte, placeholder byte) []byte {
	var (
		digit    byte
		revindex int
	)

	// write sign '-' at first call
	if nbfmt.state_sign_written == false {
		if nbfmt.sign {
			dest = append(dest, '-')
		}
		nbfmt.state_sign_written = true
	}

	// fractional point

	if placeholder == '.' {
		// write remaining integral digits, if any
		for nbfmt.state_next_integral_digit_index >= 0 {
			revindex = len(nbfmt.part_integral) - 1 - nbfmt.state_next_integral_digit_index
			digit = nbfmt.part_integral[revindex]
			dest = append(dest, digit)
			if nbfmt.has_group_separator(nbfmt.state_next_integral_digit_index) {
				dest = append_rune(dest, nbfmt.group_separator_symbol)
			}
			nbfmt.state_next_integral_digit_index--
		}
		// write fractional point
		dest = append_rune(dest, nbfmt.decimal_point_symbol)
		nbfmt.state_inside_integral_part = false
		nbfmt.state_must_output_digit_flag = true
		nbfmt.state_next_integral_digit_index = -1
		nbfmt.state_next_placeholder_index = 0
		return dest
	}

	// integral part digit

	if nbfmt.state_inside_integral_part {
		if nbfmt.state_next_placeholder_index > nbfmt.state_next_integral_digit_index { // no digit at placeholder position. '0' or nothing is written
			if placeholder == '0' || nbfmt.state_must_output_digit_flag == true {
				// write '0', and group separator if needed
				dest = append(dest, '0')
				if nbfmt.has_group_separator(nbfmt.state_next_placeholder_index) {
					dest = append_rune(dest, nbfmt.group_separator_symbol)
				}
				nbfmt.state_must_output_digit_flag = true
			}
			nbfmt.state_next_placeholder_index--
			return dest
		}

		if nbfmt.state_next_integral_digit_index < 0 { // if no more digits exist, do nothing (should never happen)
			return dest
		}

		for nbfmt.state_next_integral_digit_index >= nbfmt.state_next_placeholder_index { // first call may loop
			// write digit, and group separator if needed
			revindex = len(nbfmt.part_integral) - 1 - nbfmt.state_next_integral_digit_index
			digit = nbfmt.part_integral[revindex]
			dest = append(dest, digit)
			if nbfmt.has_group_separator(nbfmt.state_next_integral_digit_index) {
				dest = append_rune(dest, nbfmt.group_separator_symbol)
			}
			nbfmt.state_next_integral_digit_index--
		}
		nbfmt.state_next_placeholder_index--
		return dest
	}

	// fractional part digit

	if placeholder == '#' {
		nbfmt.state_must_output_digit_flag = false
	}

	digit_available := nbfmt.state_next_placeholder_index < len(nbfmt.part_fractional)
	if nbfmt.state_must_output_digit_flag || digit_available { // if '0' placeholder, or '#' placeholder with digit available in number to format
		digit = '0'          // default
		if digit_available { // digit is available in number to format
			digit = nbfmt.part_fractional[nbfmt.state_next_placeholder_index]
		}
		dest = append(dest, digit)
		nbfmt.state_next_placeholder_index++
		return dest
	}

	return dest
}

func (nbfmt *Numberfmt) Append_formatted_number(dest []byte, format_string []byte) (dst []byte, err error) {
	var (
		quoted_string_mode   bool
		escape_flag          bool
		escape_quote         byte // last opening single-quote or double-quote
		fractional_point_pos int
	)

	if nbfmt.err != nil {
		return nil, nbfmt.err
	}

	fractional_point_pos = -1
	s := format_string

	// rescan the format string and write formatted output to dest
	//    The parsing code is almost the same as in Get_format_info()

	for i, c := range s {
		if quoted_string_mode == true {
			if c == escape_quote && escape_flag == false { // end of quoted string
				quoted_string_mode = false
				continue
			}
			if c == '\\' { // if  escape backslash, just skip it
				escape_flag = true
				continue
			}
			dest = append(dest, c) // append character contained in the quoted string
			escape_flag = false
			continue
		}

		if escape_flag == true { // output escaped character
			dest = append(dest, c)
			escape_flag = false
			continue
		}

		if c == '"' || c == '\'' { // start of quoted string
			quoted_string_mode = true
			escape_quote = c
			continue
		}

		if c == '\\' { // escape character
			escape_flag = true
			continue
		}

		// here, we are not in quoted string mode, and current character is not escaped

		// if scanning integral part of the number formatting

		if fractional_point_pos < 0 {
			switch c {
			case '0', '#': // digit placeholder
				dest = nbfmt.append_placeholder(dest, c)

			case '.': // fractional point placeholder
				if i == nbfmt.fractional_point_pos {
					dest = nbfmt.append_placeholder(dest, c)
					fractional_point_pos = i
				}

			case ',':
				// don't print group separator

			default:
				dest = append(dest, c)
			}
			continue
		}

		// if scanning fractional part of the number formatting

		switch c {
		case '0', '#':
			dest = nbfmt.append_placeholder(dest, c)

		default:
			if c != '.' && c != ',' {
				dest = append(dest, c)
			}
		}

	} // end loop

	if nbfmt.state_next_integral_digit_index >= 0 && nbfmt.state_next_placeholder_index != nbfmt.part_integral_digit_placeholder_count-1 { // should never happen
		return nil, fmt.Errorf("Number formatting: non-printed digits remain <%s>", nbfmt.nbuff)
	}

	return dest, nil
}
