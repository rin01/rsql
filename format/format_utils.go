package format

import (
	"unicode/utf8"
)

func append_rune(dest []byte, c rune) []byte {
	var buff [4]byte

	if c < utf8.RuneSelf { // if < 128, rune is in ascii range
		return append(dest, byte(c))
	}

	n := utf8.EncodeRune(buff[:], c)

	return append(dest, buff[:n]...)
}
