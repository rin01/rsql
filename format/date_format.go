package format

import (
	"time"

	"rsql/lang"
)

/************************************************************************/
/*                                                                      */
/*                           date formatting                            */
/*                                                                      */
/************************************************************************/

const (
	STATE_DEFAULT int = iota
	STATE_QUOTED_STRING
	STATE_YEAR
	STATE_MONTH
	STATE_DAY
	STATE_HOUR_h // 0-12
	STATE_HOUR_H // 0-23
	STATE_MINUTE
	STATE_SECOND
	STATE_FRACTION_SECOND_f
	STATE_FRACTION_SECOND_F
	STATE_AM_PM
	STATE_DATE_SEPARATOR
	STATE_TIME_SEPARATOR
)

// Append_formatted_date appends date dt, formatted by format_string.
//
func Append_formatted_date(dest []byte, dt time.Time, format_string []byte, langinfo *lang.Langinfo) []byte {
	var (
		c            byte
		state        int
		substate     int // count of format symbol
		escape_flag  bool
		escape_quote byte // last opening single-quote or double-quote
	)

	// if one ascii character, it is a "standard" format

	if len(format_string) == 1 {
		switch format_string[0] {
		case 'f':
			format_string = []byte(langinfo.Lng_date_pattern_long + " " + langinfo.Lng_time_pattern_short)
		case 'F':
			format_string = []byte(langinfo.Lng_datetime_pattern_full)
		case 'd':
			format_string = []byte(langinfo.Lng_date_pattern_short)
		case 'D':
			format_string = []byte(langinfo.Lng_date_pattern_long)
		case 't':
			format_string = []byte(langinfo.Lng_time_pattern_short)
		case 'T':
			format_string = []byte(langinfo.Lng_time_pattern_long)
		}
	}

	if len(format_string) == 2 && format_string[0] == '%' { // % as first character is used to force custom format instead of "standard" format
		format_string = format_string[1:] // discard prefix %
	}

	// parse and format

	for _, c = range format_string {

		if state == STATE_QUOTED_STRING {
			if c == escape_quote && escape_flag == false { // end of quoted string
				state = STATE_DEFAULT
				continue
			}
			if c == '\\' { // if  escape backslash, just skip it
				escape_flag = true
				continue
			}
			escape_flag = false
			dest = append(dest, c)
			continue
		}

		if state == STATE_DEFAULT && escape_flag == true {
			dest = append(dest, c)
			escape_flag = false
			continue
		}

		switch c {
		case 'y':
			if state != STATE_YEAR {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_YEAR
				substate = 0
			}
			if substate < 5 {
				substate++
			}
			continue

		case 'M':
			if state != STATE_MONTH {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_MONTH
				substate = 0
			}
			if substate < 4 {
				substate++
			}
			continue

		case 'd':
			if state != STATE_DAY {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_DAY
				substate = 0
			}
			if substate < 4 {
				substate++
			}
			continue

		case 'h':
			if state != STATE_HOUR_h {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_HOUR_h
				substate = 0
			}
			if substate < 2 {
				substate++
			}
			continue

		case 'H':
			if state != STATE_HOUR_H {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_HOUR_H
				substate = 0
			}
			if substate < 2 {
				substate++
			}
			continue

		case 'm':
			if state != STATE_MINUTE {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_MINUTE
				substate = 0
			}
			if substate < 2 {
				substate++
			}
			continue

		case 's':
			if state != STATE_SECOND {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_SECOND
				substate = 0
			}
			if substate < 2 {
				substate++
			}
			continue

		case 'f':
			if state != STATE_FRACTION_SECOND_f {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_FRACTION_SECOND_f
				substate = 0
			}
			if substate < 9 {
				substate++
			}
			continue

		case 'F':
			if state != STATE_FRACTION_SECOND_F {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_FRACTION_SECOND_F
				substate = 0
			}
			if substate < 9 {
				substate++
			}
			continue

		case 't':
			if state != STATE_AM_PM {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_AM_PM
				substate = 0
			}
			if substate < 2 {
				substate++
			}
			continue

		case '/':
			dest = append_date_part(dest, dt, state, substate, langinfo)
			state = STATE_DATE_SEPARATOR
			substate = 1
			continue

		case ':':
			dest = append_date_part(dest, dt, state, substate, langinfo)
			state = STATE_TIME_SEPARATOR
			substate = 1
			continue

		case '"', '\'': // start of quoted string
			dest = append_date_part(dest, dt, state, substate, langinfo)
			state = STATE_QUOTED_STRING
			substate = 0
			escape_quote = c
			continue

		default:
			if state != STATE_DEFAULT {
				dest = append_date_part(dest, dt, state, substate, langinfo)
				state = STATE_DEFAULT
				substate = 0
				escape_flag = false
			}
			if c == '\\' {
				escape_flag = true
				continue
			}
			dest = append(dest, c)
			escape_flag = false
			continue
		}

	}

	if !(state == STATE_DEFAULT || state == STATE_QUOTED_STRING) { // for STATE_DEFAULT and STATE_QUOTED_STRING, bytes have already been written to dest on the fly
		dest = append_date_part(dest, dt, state, substate, langinfo)
	}

	return dest
}

// append_date_part appends the date part to dest, for the part specified by state, with format symbol count given by substate.
// E.g., for format "yyyy", state is STATE_YEAR and substate if 4, for the four 'f'.
//
func append_date_part(dest []byte, dt time.Time, state int, substate int, langinfo *lang.Langinfo) []byte {

	if state == STATE_DEFAULT || state == STATE_QUOTED_STRING { // for STATE_DEFAULT and STATE_QUOTED_STRING, there is nothing to do
		return dest
	}

	switch state {
	case STATE_YEAR:
		var year_buff = []byte{0, 0, 0, 0, 0}
		year_buff = append_int64(year_buff[:0], int64(dt.Year()), 5) // write 5 digits in year_buff
		dest = append(dest, year_buff[len(year_buff)-substate:]...)  // only substate count digits are written

	case STATE_MONTH:
		switch substate {
		case 1,
			2:
			dest = append_int64(dest, int64(dt.Month()), substate)
		case 3:
			dest = append(dest, langinfo.Lng_monthnames_genitive_abbr[dt.Month()-1]...)
		case 4:
			dest = append(dest, langinfo.Lng_monthnames_genitive[dt.Month()-1]...)
		default:
			panic("impossible")
		}

	case STATE_DAY:
		switch substate {
		case 1,
			2:
			dest = append_int64(dest, int64(dt.Day()), substate)
		case 3:
			dest = append(dest, langinfo.Lng_daynames_abbr[dt.Weekday()]...) // 0: Sunday   1: Monday  ...  6: Saturday
		case 4:
			dest = append(dest, langinfo.Lng_daynames[dt.Weekday()]...) // 0: Sunday   1: Monday  ...  6: Saturday
		default:
			panic("impossible")
		}

	case STATE_HOUR_h:
		h := int64(dt.Hour())
		if h > 12 {
			h = h - 12
		} else if h == 0 {
			h = 12
		}
		dest = append_int64(dest, h, substate)

	case STATE_HOUR_H:
		dest = append_int64(dest, int64(dt.Hour()), substate)

	case STATE_MINUTE:
		dest = append_int64(dest, int64(dt.Minute()), substate)

	case STATE_SECOND:
		dest = append_int64(dest, int64(dt.Second()), substate)

	case STATE_FRACTION_SECOND_f, STATE_FRACTION_SECOND_F:
		fracsec := int64(dt.Nanosecond())
		switch substate {
		case 1:
			fracsec /= 1e8 // 10th of second
		case 2:
			fracsec /= 1e7 // 100th of second
		case 3:
			fracsec /= 1e6 // millisecond
		case 4:
			fracsec /= 1e5 // 10th of millisecond
		case 5:
			fracsec /= 1e4 // 100th of millisecond
		case 6:
			fracsec /= 1e3 // microsecond
		case 7:
			fracsec /= 1e2 // 10th of microsecond
		case 8:
			fracsec /= 1e1 // 100th of microsecond
		case 9:
			// nanosecond
		default:
			panic("impossible")
		}

		switch {
		case state == STATE_FRACTION_SECOND_f:
			dest = append_int64(dest, fracsec, substate)

		case fracsec == 0: // STATE_FRACTION_SECOND_F
			if len(dest) > 0 && dest[len(dest)-1] == '.' { // discard dot before fracsec, if any
				dest = dest[:len(dest)-1]
			}

		default: // STATE_FRACTION_SECOND_F. Discard trailing '0's
			var i int

			dest = append_int64(dest, fracsec, substate)

			for i = 0; i < substate; i++ {
				if dest[len(dest)-1] != '0' { // stop discarding '0's
					break
				}

				dest = dest[:len(dest)-1] // discard last '0'
			}

			if i == substate && len(dest) > 0 && dest[len(dest)-1] == '.' { // discard dot before fracsec, if any
				dest = dest[:len(dest)-1]
			}
		}

	case STATE_AM_PM:
		var am_pm string
		h := int64(dt.Hour())
		if h < 12 {
			am_pm = langinfo.Lng_AM_symbol
			if am_pm == "" {
				am_pm = "AM"
			}
		} else {
			am_pm = langinfo.Lng_PM_symbol
			if am_pm == "" {
				am_pm = "PM"
			}
		}
		dest = append(dest, am_pm...)

	case STATE_DATE_SEPARATOR:
		dest = append_rune(dest, langinfo.Lng_date_separator)

	case STATE_TIME_SEPARATOR:
		dest = append_rune(dest, langinfo.Lng_time_separator)

	default:
		panic("impossible")
	}

	return dest
}

// append_int64 appends an int to dest, with leading zeroes to fill the requested length.
// THIS FUNCTION DOESN'T WORK WITH n = math.MinInt64.
// requested_length should be at least 1, to print '0' for 0. If requested_length = 0, it will print an empty string for 0.
// If number size is larger than requested_length, requested_length is ignored.
//
func append_int64(dest []byte, n int64, requested_length int) []byte {

	const BUFF_SIZE int = 20 // max output length, including '-' sign if any, is large enough for any int64
	var (
		sign int // 1 if negative, else 0
		i    int
		q    int64
		buff [BUFF_SIZE]byte
	)

	if n < 0 {
		n = -n
		sign = 1
	}

	for n != 0 {
		q = n / 10
		buff[BUFF_SIZE-1-i] = byte(n - q*10) // panic if buff array capacity is exceeded. Never happens.
		n = q
		i++
	}

	// adjust length if too long or too short

	if requested_length > BUFF_SIZE {
		requested_length = BUFF_SIZE
	}

	if requested_length < i+sign {
		requested_length = i + sign
	}

	res := buff[BUFF_SIZE-requested_length:]

	for i := range res {
		res[i] += '0'
	}

	if sign > 0 {
		res[0] = '-'
	}

	dest = append(dest, res...)
	return dest
}
