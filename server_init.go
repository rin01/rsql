package rsql

import (
	"math/rand"
	"time"
)

var G_SERVERNAME string
var G_SERVER_DEFAULT_COLLATION string
var G_LISTENER_ADDRESS string

var G_DEBUG_FLAG bool // set by the -debug option

// If too large, there is a risk that too much memory will be used by the batch for all the Token_primaries, which size is more than 300 bytes.
// This value is sent to the client, which stores it in session.server_batch_text_max_size when connection is established.
var G_BATCH_TEXT_MAX_SIZE int

var G_BATCH_INSERTS_MAX_COUNT int

func init() {

	// seed global random source with random number

	rand.Seed(time.Now().UnixNano())
}
