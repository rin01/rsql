package mstr

import (
	"bytes"
	"testing"
	"unicode"
)

func Test_Map(t *testing.T) {
	var (
		a Mystr
	)

	var samples = []struct {
		in  string
		out string
	}{
		{"", ""},
		{" ", " "},
		{"a", "a"},
		{"A", "a"},
		{"BLabLa", "blabla"},
		{"世界A", "世界a"},
	}

	for _, sample := range samples {
		a = make([]byte, 1)
		in := Mystr(sample.in)
		out := Mystr(sample.out)

		res := Map(a, in, unicode.ToLower)

		if bytes.Compare(res, out) != 0 {
			t.Fatalf("error during Map of \"%s\". Got \"%s\", expected \"%s\"", in, res, out)
		}

	}

}

func Test_Truncate_to_precision(t *testing.T) {
	var (
		a Mystr
	)

	var samples = []struct {
		in   string
		in_p uint16
		out  string
	}{
		{"世界A", 0, ""},
		{"世界A", 1, "世"},
		{"世界A", 2, "世界"},
		{"世界A", 3, "世界A"},
		{"世界A", 4, "世界A"},
		{"世界A", 100, "世界A"},
		{"abc", 0, ""},
		{"abc", 1, "a"},
		{"abc", 2, "ab"},
		{"abc", 3, "abc"},
		{"abc", 4, "abc"},
		{"abc", 100, "abc"},
	}

	for _, sample := range samples {
		in := Mystr(sample.in)
		in_p := sample.in_p
		out := Mystr(sample.out)

		a = append(a[:0], in...)

		a.Truncate_to_precision(in_p)

		if bytes.Compare(a, out) != 0 {
			t.Fatalf("error during Truncate_to_precision of \"%s\". Got \"%s\", expected \"%s\"", in, a, out)
		}

	}

}

func Test_Pad_or_truncate_to_precision(t *testing.T) {
	var (
		a Mystr
	)

	var samples = []struct {
		in   string
		in_p uint16
		out  string
	}{
		{"世界A", 0, ""},
		{"世界A", 1, "世"},
		{"世界A", 2, "世界"},
		{"世界A", 3, "世界A"},
		{"世界A", 4, "世界A "},
		{"世界A", 100, "世界A                                                                                                 "},
		{"abc", 0, ""},
		{"abc", 1, "a"},
		{"abc", 2, "ab"},
		{"abc", 3, "abc"},
		{"abc", 4, "abc "},
		{"abc", 100, "abc                                                                                                 "},
	}

	for _, sample := range samples {
		in := Mystr(sample.in)
		in_p := sample.in_p
		out := Mystr(sample.out)

		a = append(a[:0], in...)

		a.Pad_or_truncate_to_precision(in_p)

		if bytes.Compare(a, out) != 0 {
			t.Fatalf("error during Pad_or_truncate_to_precision of \"%s\". Got \"%s\", expected \"%s\"", in, a, out)
		}

	}

}
