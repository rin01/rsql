package mstr

import (
	"bytes"
	"unicode/utf8"

	"golang.org/x/text/collate"
	"golang.org/x/text/unicode/norm"

	"rsql"
)

type Mystr []byte

// Equal_strict compares two strings for equality, byte per byte.
// It is a strict comparison, because trailing blanks are not discarded.
//
func Equal_strict(a Mystr, b Mystr) bool {

	if bytes.Compare(a, b) == 0 {
		return true
	}

	return false
}

// Compare compares string a and b.
// The trailing blanks of a and b are skipped.
// The result will be 0 if a==b, -1 if a < b, and +1 if a > b.
//
func Compare(a Mystr, b Mystr, collator *collate.Collator) int {
	var (
		str_a             Mystr
		str_b             Mystr
		i                 int
		comparison_result int
	)

	for i = len(a) - 1; i >= 0; i-- { // adjust the length, so that the trailing spaces are skipped if any.
		if a[i] != ' ' {
			break
		}
	}
	str_a = a[:i+1]

	for i = len(b) - 1; i >= 0; i-- { // adjust the length, so that the trailing spaces are skipped if any.
		if b[i] != ' ' {
			break
		}
	}
	str_b = b[:i+1]

	comparison_result = collator.Compare(str_a, str_b)

	return comparison_result
}

// Equal compares string a and b for equality.
//
//     It is the same code as Compare(), but with a quick path (byte comparison).
//     THIS FUNCTION IS NOT USED BECAUSE IN A SQL QUERY, MOST RESULT OF '=' RETURN FALSE, AND IN THIS CASE, THE SHORTCUT PATH IS JUST USELESS WORK.
//
// The trailing blanks of a and b are skipped.
// The result will be 0 if a==b, -1 if a < b, and +1 if a > b.
//
func Equal(a Mystr, b Mystr, collator *collate.Collator) bool {
	var (
		str_a             Mystr
		str_b             Mystr
		i                 int
		comparison_result int
	)

	for i = len(a) - 1; i >= 0; i-- { // adjust the length, so that the trailing spaces are skipped if any.
		if a[i] != ' ' {
			break
		}
	}
	str_a = a[:i+1]

	for i = len(b) - 1; i >= 0; i-- { // adjust the length, so that the trailing spaces are skipped if any.
		if b[i] != ' ' {
			break
		}
	}
	str_b = b[:i+1]

	if bytes.Compare(a, b) == 0 { // quick path
		return true
	}

	comparison_result = collator.Compare(str_a, str_b)

	if comparison_result == 0 {
		return true
	}

	return false
}

// append_rune appends one encoded rune to dest.
//
func append_rune(dest Mystr, uc rune) (res Mystr) {
	var (
		bytebuff [utf8.UTFMax]byte // one character can take up to 4 bytes in utf8
	)

	n := utf8.EncodeRune(bytebuff[:], uc)

	dest = append(dest, bytebuff[:n]...)

	return dest
}

// rune_advance tries to eat rune_count runes in string s, starting at pos_start.
// It returns pos_endx (0-based byte position, in the range [0 ... len(s)]), which is the position just after the last rune eaten, and number of runes eaten (may be less than rune_count).
// pos_start must be in the range [0 ... len(s)], and it is a 0-based byte position.
//
func rune_advance(s Mystr, pos_start int, rune_count int) (pos_endx int, runes_eaten int) {
	var (
		i     int
		count int
	)

	if rune_count == 0 {
		return pos_start, 0
	}

	i = pos_start

	for i < len(s) {
		// read one rune from s

		r, width := rune(s[i]), 1
		if r >= utf8.RuneSelf {
			r, width = utf8.DecodeRune(s[i:]) // returns (RuneError, 1) if invalid sequence
		}

		// go to next rune position in s

		count++
		i += width

		if count == rune_count { // if requested number of runes have been eaten, return
			return i, count
		}
	}

	// there are not enough runes in s

	rsql.Assert(i == len(s))

	return i, count
}

// Map writes all runes of s, mapped with function 'mapping', into dest slice.
// If capacity of dest is not enough, a new storage array is allocated.
// The slice containing result is returned.
//
func Map(dest Mystr, s Mystr, mapping func(r rune) rune) (res Mystr) {

	// if empty string, return

	if len(s) == 0 {
		return dest[:0]
	}

	// make dest as large as s, to avoid memory reallocation

	if cap(dest) < len(s) {
		dest = make([]byte, len(s)) // enough for most cases
	}

	// append mapped character into dest

	dest = dest[:0]

	for i := 0; i < len(s); {
		// read each rune from s

		r, width := rune(s[i]), 1
		if r >= utf8.RuneSelf {
			r, width = utf8.DecodeRune(s[i:]) // returns (RuneError, 1) if invalid sequence
		}

		// compute new rune

		r = mapping(r)

		// write r into dest

		if r >= 0 { // if mapping has returned a negative value, the rune is just dropped.
			dest = append_rune(dest, r)
		}

		// go to next rune posiiton in s

		i += width
	}

	return dest
}

// Left writes rune_count runes from s, starting from left, into dest slice.
// If capacity of dest is not enough, a new storage array is allocated.
// The slice containing result is returned.
//
// If not enough characters are available, a copy of s is returned.
//
func Left(dest Mystr, s Mystr, rune_count int) (res Mystr) {
	var (
		pos_endx int
	)

	// if empty string, return

	if len(s) == 0 || rune_count <= 0 {
		return dest[:0]
	}

	// if s contains rune_count bytes or less, copy the whole string

	if rune_count >= len(s) {
		dest = append(dest[:0], s...)
		return dest
	}

	// copy rune_count runes to dest

	pos_endx, _ = rune_advance(s, 0, rune_count) // if runes in s is less or equal to rune_count, pos_endx == len(s)

	dest = append(dest[:0], s[:pos_endx]...)

	return dest
}

// Right writes rune_count runes from s, starting from right, into dest slice.
// If capacity of dest is not enough, a new storage array is allocated.
// The slice containing result is returned.
//
// If not enough characters are available, a copy of s is returned.
//
func Right(dest Mystr, s Mystr, rune_count int) (res Mystr) {
	var (
		i     int
		count int
	)

	// if empty string, return

	if len(s) == 0 || rune_count <= 0 {
		return dest[:0]
	}

	// if s contains rune_count bytes or less, copy the whole string

	if rune_count >= len(s) {
		dest = append(dest[:0], s...)
		return dest
	}

	// copy rune_count runes to dest. The following code requires that rune_count > 0.

	i = len(s)
	for i > 0 {
		// read each rune from s

		r, width := rune(s[i-1]), 1 // last byte of rune before
		if r >= utf8.RuneSelf {
			r, width = utf8.DecodeLastRune(s[:i]) // returns (RuneError, 1) if invalid sequence
		}

		// go to previous rune posiiton in s

		count++
		i -= width // s[i:] contains 'count' runes

		if count == rune_count { // if requested number of runes have been read, copy them and return
			dest = append(dest[:0], s[i:]...)
			return dest
		}
	}

	rsql.Assert(i == 0)

	dest = append(dest[:0], s...) // copy the whole string
	return dest
}

// Stuff deletes section of string s which starts at rune_section_start (1-based) of length rune_section_length (in runes), and replaces it by replacement.
// If argument invalid, returned res_NULL_flag is true, meaning that the SQL result of the function must be NULL.
//
func Stuff(dest Mystr, s Mystr, rune_section_start int, rune_section_length int, replacement Mystr) (res_NULL_flag bool, res Mystr) {
	var (
		pos_section_start int // byte index (0-based) of the section to delete
		pos_section_endx  int // byte index (0-based) of one-past last byte of the section to delete
	)

	// if empty string or invalid argument, return NULL string

	if len(s) == 0 || rune_section_start <= 0 || rune_section_length < 0 {
		return true, nil
	}

	// skip rune_section_start-1 runes, to find start of section to remove

	if rune_section_start > 1 {
		pos_section_start, _ = rune_advance(s, 0, rune_section_start-1)
	}

	if pos_section_start == len(s) { // if rune_section_start is beyond string s, return NULL string
		return true, nil
	}

	// count rune_section_length runes

	pos_section_endx = pos_section_start

	if rune_section_length > 0 {
		pos_section_endx, _ = rune_advance(s, pos_section_start, rune_section_length)
	}

	// concatenate string parts

	dest = dest[:0]

	if pos_section_start > 0 { // write first part of s
		dest = append(dest, s[:pos_section_start]...)
	}

	if len(replacement) > 0 { // write replacement string
		dest = norm.NFC.Append(dest, replacement...)
	}

	if pos_section_endx < len(s) { // write last part of s
		dest = norm.NFC.Append(dest, s[pos_section_endx:]...)
	}

	return false, dest
}

func Substring(dest Mystr, s Mystr, rune_section_start int, rune_section_length int) (res Mystr) {
	var (
		pos_section_start int // byte index (0-based) of the section to copy
		pos_section_endx  int // byte index (0-based) of one-past last byte of the section to copy
		corrected_length  int64
	)

	// if rune_section_length <= 0, return empty string

	if rune_section_length <= 0 {
		return dest[:0]
	}

	// if rune_section_start <= 0, correction of start and length

	if rune_section_start <= 0 {
		corrected_length = int64(rune_section_length) + int64(rune_section_start) - 1
		if corrected_length < 0 {
			corrected_length = 0
		}

		rune_section_start = 1
		rune_section_length = int(corrected_length)
	}

	// if empty string or rune_section_length == 0, return empty string

	if len(s) == 0 || rune_section_length == 0 {
		return dest[:0]
	}

	// skip rune_section_start-1 runes, to find start of substring

	if rune_section_start > 1 {
		pos_section_start, _ = rune_advance(s, 0, rune_section_start-1)
	}

	if pos_section_start == len(s) { // if rune_section_start is beyond string s, return empty string
		return dest[:0]
	}

	// count rune_section_length runes

	pos_section_endx = pos_section_start

	pos_section_endx, _ = rune_advance(s, pos_section_start, rune_section_length)

	// copy substring

	dest = append(dest[:0], s[pos_section_start:pos_section_endx]...)

	return dest
}

// Charindex return the rune index (returned found_rune_index is 1-based) of findstr.
// Returned values pos_start and pos_endx are 0-based.
// If findstr is not found, returns found_rune_index == 0, and pos_start == -1.
//
// If you know the length in runes of findstr, pass it in findstr_rune_count_hint. Else, pass -1.
//
// rune_section_start is 1-based. To start the search from the first character, pass 1 for this argument.
//
// We could use package golang.org/x/text/search. TODO I must think about it.
//
func Charindex(s Mystr, rune_section_start int, findstr Mystr, findstr_rune_count_hint int, collator *collate.Collator) (pos_start int, pos_endx int, found_rune_index int) {
	var (
		runes_eaten int
		runes_count int
		r           rune
		width       int
	)

	// if empty string to search or find, return not found

	if len(s) == 0 || len(findstr) == 0 {
		return -1, -1, 0
	}

	// correction of rune_section_start

	if rune_section_start <= 0 {
		rune_section_start = 1
	}

	findstr_rune_count := findstr_rune_count_hint
	if findstr_rune_count_hint < 0 {
		findstr_rune_count = utf8.RuneCount(findstr)
	}

	// skip rune_section_start-1 runes, to find start position for search

	if rune_section_start > 1 {
		pos_start, runes_eaten = rune_advance(s, 0, rune_section_start-1)
	}

	if pos_start == len(s) { // if rune_section_start is beyond string s, return not found
		return -1, -1, 0
	}

	// advance findstr_rune_count runes, to find endx position for comparison

	pos_endx, runes_count = rune_advance(s, pos_start, findstr_rune_count)

	if runes_count != findstr_rune_count { // if string to search is smaller than string to find, return not found
		return -1, -1, 0
	}

	// search

	if collator.Compare(findstr, s[pos_start:pos_endx]) == 0 {
		return pos_start, pos_endx, runes_eaten + 1
	}

	for pos_endx < len(s) { // move the comparison sliding window one rune forwards

		r, width = rune(s[pos_start]), 1
		if r >= utf8.RuneSelf {
			r, width = utf8.DecodeRune(s[pos_start:]) // returns (RuneError, 1) if invalid sequence
		}
		pos_start += width

		r, width = rune(s[pos_endx]), 1
		if r >= utf8.RuneSelf {
			r, width = utf8.DecodeRune(s[pos_endx:]) // returns (RuneError, 1) if invalid sequence
		}
		pos_endx += width

		runes_eaten++

		if collator.Compare(findstr, s[pos_start:pos_endx]) == 0 {
			return pos_start, pos_endx, runes_eaten + 1
		}
	}

	return -1, -1, 0 // not found
}

func Replace(dest Mystr, s Mystr, findstr Mystr, replacement Mystr, collator *collate.Collator) (res Mystr) {
	var (
		pos_start         int
		pos_endx          int
		section_to_copy   Mystr
		section_to_search Mystr
	)

	findstr_rune_count := utf8.RuneCount(findstr)

	section_to_search = s[:]
	dest = dest[:0]

	for {
		pos_start, pos_endx, _ = Charindex(section_to_search, 1, findstr, findstr_rune_count, collator)
		if pos_start < 0 { // no more findstr found
			if len(section_to_search) > 0 {
				dest = norm.NFC.Append(dest, section_to_search...)
			}
			break
		}

		section_to_copy = section_to_search[:pos_start]

		if len(section_to_copy) > 0 {
			dest = norm.NFC.Append(dest, section_to_copy...)
		}

		if len(replacement) > 0 {
			dest = norm.NFC.Append(dest, replacement...)
		}

		section_to_search = section_to_search[pos_endx:]

		if len(dest) > rsql.DATATYPE_VARCHAR_PRECISION_MAX*utf8.UTFMax { // result string is getting really too long, stop replacing
			break
		}
	}

	return dest
}

// Starts_with returns true if string starts with prefix.
//
// If you know the rune count in prefix, pass it in prefix_rune_count_hint. Else, pass -1.
//
func Starts_with(s Mystr, prefix Mystr, prefix_rune_count_hint int, collator *collate.Collator) bool {
	var (
		pos_endx    int
		runes_count int
	)

	// if prefix is empty string, return true. It is for the case        'Hello' LIKE '%'    which is true

	if len(prefix) == 0 {
		return true
	}

	// if string to search is empty string, return false

	if len(s) == 0 {
		return false
	}

	// correction of prefix_rune_count

	prefix_rune_count := prefix_rune_count_hint
	if prefix_rune_count_hint < 0 {
		prefix_rune_count = utf8.RuneCount(prefix)
	}

	// advance prefix_rune_count runes, to find endx position for search

	pos_endx, runes_count = rune_advance(s, 0, prefix_rune_count)

	if runes_count != prefix_rune_count { // string to search is smaller than string to find
		return false
	}

	// compare

	if collator.Compare(prefix, s[:pos_endx]) == 0 {
		return true
	}

	return false
}

func (mystr *Mystr) Truncate_to_precision(precision uint16) {
	var (
		pos_endx   int
		rune_count int
		ss         []byte
	)

	// if string length (in bytes) <= precision (in runes), just return

	ss = *mystr

	if len(ss) <= int(precision) {
		return
	}

	// optimistic try. Most of the times, there will be no truncation.

	rune_count = utf8.RuneCount(ss)

	if rune_count <= int(precision) {
		return
	}

	// more runes than precision, we must truncate

	pos_endx, _ = rune_advance(ss, 0, int(precision))

	*mystr = ss[:pos_endx]
}

func (mystr *Mystr) Pad_or_truncate_to_precision(precision uint16) {
	var (
		padding_count int
		rune_count    int
		string_length int
		ss            []byte
	)

	rune_count = utf8.RuneCount(*mystr)

	if rune_count == int(precision) { // if string is proper length, return
		return
	}

	if rune_count > int(precision) { // if string too long, truncate
		ss = *mystr
		pos_endx, _ := rune_advance(ss, 0, int(precision))
		*mystr = ss[:pos_endx]
		return
	}

	// string too short. Append padding

	padding_count = int(precision) - rune_count // number of spaces to append

	string_length = len(*mystr) + padding_count

	ss = *mystr
	if cap(ss) < string_length {
		ss = make([]byte, string_length)
		copy(ss, *mystr)
	}
	ss = ss[:string_length]

	for i := len(*mystr); i < string_length; i++ {
		ss[i] = ' '
	}

	*mystr = ss
}

// Fit_to_precision_no_truncate removes trailing spaces in excess, if rune count in mystr is larger than precision.
// If trailing characters in excess are not all spaces, an error is returned.
// Else, the function just returns.
//
func (mystr *Mystr) Fit_to_precision_no_truncate(precision uint16) *rsql.Error {
	var (
		rune_count     int
		spaces_to_skip int
		ss             []byte
		part_to_skip   []byte
	)

	// if string length (in bytes) <= precision (in runes), just return

	ss = *mystr

	if len(ss) <= int(precision) {
		return nil
	}

	// optimistic try. Most of the times, there will be no truncation.

	rune_count = utf8.RuneCount(ss)

	if rune_count <= int(precision) {
		return nil
	}

	// more runes than precision, we try to ignore trailing spaces

	spaces_to_skip = rune_count - int(precision)

	part_to_skip = ss[len(ss)-spaces_to_skip:]

	for _, c := range part_to_skip { // check that part to skip contains only spaces
		if c != ' ' {
			return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, rune_count, precision)
		}
	}

	*mystr = ss[:len(ss)-spaces_to_skip]

	return nil
}
