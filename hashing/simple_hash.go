package hashing

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"hash"
)

const Salt_size = 16

// Generate_hash_from_password generates a simple SHA1 hash, from random salt and password.
//
func Generate_hash_from_password(password string) (hashed_password_base64 string, err error) {
	var (
		buff []byte
		h    hash.Hash
	)

	buff = make([]byte, Salt_size, Salt_size+sha1.Size)

	if _, err := rand.Read(buff); err != nil { // write random salt in buff
		return "", err
	}

	h = sha1.New()
	h.Write(buff)             // hash salt
	h.Write([]byte(password)) // hash password

	hashed_password_base64 = base64.StdEncoding.EncodeToString(h.Sum(buff)) // the first Salt_size bytes contain the salt

	return hashed_password_base64, nil
}

func Compare_hash_and_password(hashed_password_base64 string, password string) bool {
	var (
		err         error
		h           hash.Hash
		hashed_data []byte
	)

	if hashed_data, err = base64.StdEncoding.DecodeString(hashed_password_base64); err != nil {
		return false
	}

	if len(hashed_data) != Salt_size+sha1.Size {
		return false
	}

	h = sha1.New()
	h.Write(hashed_data[:Salt_size])
	h.Write([]byte(password))

	res := bytes.Equal(h.Sum(nil), hashed_data[Salt_size:])

	return res
}
