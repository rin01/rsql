package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
	"rsql/emit"
)

// walk_Token_primary_and_simplify Walks the ast tree recursively, starting from tok.
//
// In fact, despite the word 'simplify' in the function name, only CAST tokens are simplified.
//
//      'Simplify' means that when the current token is TOK_PRIM_CAST, the following may happen if possible :
//          - TOK_PRIM_CAST_COPY token is collapsed into by its child token.
//          - TOK_PRIM_CAST token is linked directly to its grandchild token ( the child token is discarded ), with a new proper cast instruction code.
//                  - the grandchild may also collapse into current token.
//
//      The other token types are not simplified.
//
func (decor *Decorator) walk_Token_primary_and_simplify(tok *ast.Token_primary) *rsql.Error {
	var (
		rsql_err *rsql.Error
		dataslot rsql.IDataslot
		//		outer_namespace  *Xtable
	)

	// if dataslot already exists, it is an aggregate function ( for SELECT with regroupment )

	if tok.Prim_dataslot != nil {
		rsql.Assert(tok.Prim_type == ast.TOK_PRIM_SYSFUNC && tok.Prim_sysfunc_descr.Sd_id == ast.SYSFUNC_AGGR_SUM)

		return nil //  ===> return
	}

	// walk the token and children

	decor.Info_line = tok.Tok_batch_line // update line/pos for error message

	switch tok.Prim_type {
	case ast.TOK_PRIM_OPERATOR:
		if rsql_err = decor.walk_Token_primary_operator(tok); rsql_err != nil {
			return rsql_err
		}

	case ast.TOK_PRIM_CAST:
		if rsql_err = decor.walk_Token_primary_EXPLICIT_CAST_and_simplify(tok); rsql_err != nil {
			return rsql_err
		}

	case ast.TOK_PRIM_LITERAL_NUMBER:
		switch tok.Prim_subtype {
		case ast.TOK_PRIM_LITERAL_NUMBER_INTEGRAL: // may be INT, BIGINT or NUMERIC
			if dataslot, rsql_err = data.New_literal_INT_BIGINT_or_NUMERIC(tok.Prim_name); rsql_err != nil {
				return rsql_err
			}
		case ast.TOK_PRIM_LITERAL_NUMBER_MONEY:
			if dataslot, rsql_err = data.New_literal_MONEY(tok.Prim_name); rsql_err != nil {
				return rsql_err
			}
		case ast.TOK_PRIM_LITERAL_NUMBER_NUMERIC:
			if dataslot, rsql_err = data.New_literal_NUMERIC(tok.Prim_name); rsql_err != nil {
				return rsql_err
			}
		case ast.TOK_PRIM_LITERAL_NUMBER_FLOAT:
			if dataslot, rsql_err = data.New_literal_FLOAT(tok.Prim_name); rsql_err != nil {
				return rsql_err
			}
		default:
			panic("impossible")
		}
		tok.Prim_dataslot = dataslot

	case ast.TOK_PRIM_LITERAL_STRING:
		if dataslot, rsql_err = data.New_literal_VARCHAR(tok.Prim_name); rsql_err != nil {
			return rsql_err
		}
		tok.Prim_dataslot = dataslot
		tok.Prim_collation_strength = rsql.COLLSTRENGTH_0
		tok.Prim_collation = decor.Server_default_collation

	case ast.TOK_PRIM_LITERAL_HEXASTRING:
		if dataslot, rsql_err = data.New_literal_VARBINARY(tok.Prim_name); rsql_err != nil {
			return rsql_err
		}
		tok.Prim_dataslot = dataslot

	case ast.TOK_PRIM_LITERAL_NULL:
		dataslot = data.New_literal_VOID_NULL()
		tok.Prim_dataslot = dataslot

	case ast.TOK_PRIM_LITERAL_BOOLEAN:
		dataslot = data.New_literal_BOOLEAN(tok.Prim_name)
		tok.Prim_dataslot = dataslot

	case ast.TOK_PRIM_VARIABLE:
		if dataslot, rsql_err = decor.VARIABLES.Get_symbol_entry(tok.Prim_name); rsql_err != nil { // if variable doesn't exist, return error
			return rsql_err
		}

		tok.Prim_dataslot = dataslot

		if tok.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR { // tok is a TOK_PRIM_VARIABLE
			tok.Prim_collation_strength = rsql.COLLSTRENGTH_0 // IMPORTANT: a VARCHAR variable has always the default collation
			tok.Prim_collation = decor.Server_default_collation
		}

	case ast.TOK_PRIM_IDENTIFIER:
		// search in local namespace

		var xtable_ds ast.Xtable
		var col_no uint16

		xtable_namespace := decor.Xtable_object_stack.Top()
		collation := ""

		if xtable_namespace == nil {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_IDENTIFIER_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		if xtable_ds, col_no, dataslot, collation, rsql_err = xtable_namespace.Lookup_name(tok.Prim_database_name, tok.Prim_schema_name, tok.Prim_table_name, tok.Prim_name); rsql_err != nil {
			return rsql_err
		}

		if dataslot == nil {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_NOT_FOUND, rsql.ERROR_BATCH_ABORT, ast.Coalesce_colname(tok.Prim_database_name, tok.Prim_schema_name, tok.Prim_table_name, tok.Prim_name))
		}

		/*

			// search in outer namespaces

			for dataslot == nil {
				outer_namespace = tok.Prim_xtable_base.Xt_outer_namespace

				if outer_namespace == NULL {
					throw(S_ERROR_SQL_SYNTAX, S_ERROR_SQL_INVALID_SYNTAX_COLUMN_NAME_NOT_FOUND, S_ERROR_BATCH_ABORT, tok.Prim_lexeme.Lex_word.data)
				}

				dataslot = search_column_name_in_xnamespace(outer_namespace, tok.Prim_lexeme_server_name, tok.Prim_lexeme_database_name, tok.Prim_lexeme_schema_name, tok.Prim_lexeme_table_name, tok.Prim_lexeme, e)

				tok.Prim_identifier_is_external_flag = true
			}
		*/

		tok.Prim_xtable_ds = xtable_ds
		tok.Prim_col_no = col_no
		tok.Prim_dataslot = dataslot

		if tok.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
			rsql.Assert(collation != "")
			tok.Prim_collation_strength = rsql.COLLSTRENGTH_1 // column collation is always rsql.COLLSTRENGTH_1
			tok.Prim_collation = collation
		}

	case ast.TOK_PRIM_SYSFUNC:
		if rsql_err = decor.walk_Token_primary_sysfunc(tok); rsql_err != nil {
			return rsql_err
		}

	case ast.TOK_PRIM_CASE:
		if rsql_err = decor.walk_Token_primary_CASE(tok); rsql_err != nil {
			return rsql_err
		}

		/*	case ast.TOK_PRIM_SUBQUERY:
			if rsql_err = decor.walk_Token_primary_subquery(tok); rsql_err != nil {
				return rsql_err
			}
		*/
	default:
		panic("todo: token with prim_type yet to be processed " + tok.Prim_type.String())
	}

	// if COLLATE clause in script, force collation of VARCHAR result. (note: variable occurences also can have a COLLATE clause)

	if tok.Prim_sql_collate != "" {
		if tok.Prim_dataslot.Datatype() != rsql.DATATYPE_VARCHAR { // if result is not a VARCHAR, return error
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLLATE_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		// note: if operator or sysfunc, force collation. Note: "(a+b) collate en_cs_as" applies collation to the '+' result, but "a+b collate en_cs_as" applies collation to 'b' operand.

		tok.Prim_collation_strength = rsql.COLLSTRENGTH_2
		tok.Prim_collation = tok.Prim_sql_collate
	}

	// check that tok.Prim_syscollator is a literal value, and tok.Prim_syslanguage is the variable _@current_language

	if tok.Prim_syscollator != nil {
		rsql.Assert(tok.Prim_syslanguage == nil && tok.Prim_syscollator.Prim_dataslot.Datatype() == rsql.DATATYPE_SYSCOLLATOR &&
			tok.Prim_syscollator.Prim_subtype == ast.TOK_PRIM_LITERAL_SYSCOLLATOR_SUB && tok.Prim_syscollator.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF) // literal
	} else if tok.Prim_syslanguage != nil {
		rsql.Assert(tok.Prim_syslanguage.Prim_dataslot.Datatype() == rsql.DATATYPE_SYSLANGUAGE &&
			(tok.Prim_syslanguage.Prim_type == ast.TOK_PRIM_VARIABLE && tok.Prim_syslanguage.Prim_dataslot.Kind() == rsql.KIND_VAR_LEAF)) // TOK_PRIM_VARIABLE _@current_language
	}

	return nil
}

// Wacf_expression is the main function that will walk/constfold an expression.
// Use it each time a statement token needs to process an expression.
//
// If you want the expression to be a requested datatype, you can use the function Wacf_expression_with_promotion instead.
//
func (decor *Decorator) Wacf_expression(tok_expression *ast.Token_primary) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	if rsql_err = decor.walk_Token_primary_and_simplify(tok_expression); rsql_err != nil { // decorate the expression
		return rsql_err
	}

	if decor.Debug_disable_constant_folding_flag == false {
		emit.Constfold_walk_expression_tree(tok_expression) // constant fold the tree
	}

	decor.CONSTANTS.Const_walk_Token_primary(tok_expression) // fill CONSTANTS table with all constants dataslots

	return nil
}

// Wacf_expression_with_promotion is the main function that will walk/constfold an expression.
// Use it each time a statement token needs to process an expression.
//
// A promotion token is inserted if needed using Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(), and will be returned.
//
func (decor *Decorator) Wacf_expression_with_promotion(tok_expression *ast.Token_primary, requested_datatype rsql.Datatype_t) (*ast.Token_primary, *rsql.Error) {
	var (
		rsql_err            *rsql.Error
		value_datatype      rsql.Datatype_t
		tok_promotion_token *ast.Token_primary
	)

	if rsql_err = decor.walk_Token_primary_and_simplify(tok_expression); rsql_err != nil { // decorate the expression
		return nil, rsql_err
	}

	value_datatype = tok_expression.Prim_dataslot.Datatype()

	rsql.Assert(requested_datatype > 0)

	if value_datatype != requested_datatype { // create conversion token if requested and necessary
		if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok_expression, requested_datatype); rsql_err != nil { // token has been simplified
			return nil, rsql_err
		}
		tok_expression = tok_promotion_token
	}

	// constant folding of expression

	if decor.Debug_disable_constant_folding_flag == false {
		emit.Constfold_walk_expression_tree(tok_expression) // constant fold the tree
	}

	decor.CONSTANTS.Const_walk_Token_primary(tok_expression) // fill CONSTANTS table with all constants dataslots

	return tok_expression, nil
}

// Wacf_expression_with_promotion_exact_for_variable_assignment is like Wacf_expression_with_promotion, but target datatype is more precise.
// It is used for right-hand expression of variable assignment.
//
// A promotion token is inserted if needed using Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot(), and will be returned.
//
//     ********* WE WANT DATAYPE, BUT ALSO PRECISION, SCALE, AND FIXLEN_FLAG TO MATCH THOSE OF THE SAMPLE DATASLOT *********
//               (FOR VARCHAR DATASLOT, COLLATION_STRENGTH AND COLLATION ARE FORCED TO DEFAULT SERVER COLLATION, WHICH IS THE COLLATION FOR ALL VARCHAR VARIABLES)
//
func (decor *Decorator) Wacf_expression_with_promotion_exact_for_variable_assignment(tok_expression *ast.Token_primary, sample_Data_XXX_to rsql.IDataslot) (*ast.Token_primary, *rsql.Error) {
	var (
		rsql_err            *rsql.Error
		tok_promotion_token *ast.Token_primary
	)

	// walk column expression

	if rsql_err = decor.walk_Token_primary_and_simplify(tok_expression); rsql_err != nil {
		return nil, rsql_err
	}

	// put promotion token

	if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot(tok_expression, sample_Data_XXX_to); rsql_err != nil {
		return nil, rsql_err
	}
	tok_expression = tok_promotion_token

	// force collation to server collation

	if tok_expression.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
		tok_expression.Prim_collation_strength = rsql.COLLSTRENGTH_0 // IMPORTANT: force result to be default collation (like collation of all VARCHAR variables)
		tok_expression.Prim_collation = decor.Server_default_collation
	}

	// constant folding of expression

	if decor.Debug_disable_constant_folding_flag == false {
		emit.Constfold_walk_expression_tree(tok_expression) // constant fold the tree
	}

	decor.CONSTANTS.Const_walk_Token_primary(tok_expression) // fill CONSTANTS table with all constants dataslots

	return tok_expression, nil
}

// Wacf_expression_FITTING_for_INSERT_column is like Wacf_expression_with_promotion, but used only for expression to INSERT INTO table.
//
// For VARBINARY and VARCHAR, precision of the returned token CAN BE SMALLER than precision of target column !
//
// A promotion token is inserted if needed using Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot(), and will be returned.
// If expression and table column are VARBINARY, Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY_new_with_Dataslot() is used instead.
// If expression and table column are VARCHAR, Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR_new_with_Dataslot() is used instead.
//
func (decor *Decorator) Wacf_expression_FITTING_for_INSERT_column(tok_expression *ast.Token_primary, sample_Data_XXX_to rsql.IDataslot) (*ast.Token_primary, *rsql.Error) {
	var (
		rsql_err      *rsql.Error
		datatype_from rsql.Datatype_t
		datatype_to   rsql.Datatype_t
	)

	// walk column expression

	if rsql_err = decor.walk_Token_primary_and_simplify(tok_expression); rsql_err != nil {
		return nil, rsql_err
	}

	// put promotion token

	datatype_from = tok_expression.Prim_dataslot.Datatype()
	datatype_to = sample_Data_XXX_to.Datatype()

	switch {
	case datatype_from == rsql.DATATYPE_VARBINARY && datatype_to == rsql.DATATYPE_VARBINARY: // VARBINARY ---> VARBINARY column
		dataslot_to := sample_Data_XXX_to.(*data.VARBINARY)
		precision_to := dataslot_to.Data_precision

		tok_expression = decor.Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY_new_with_Dataslot(tok_expression, precision_to) // this function may return tok_expression, and precision may be smaller than precision_to

	case datatype_from == rsql.DATATYPE_VARCHAR && datatype_to == rsql.DATATYPE_VARCHAR: // VARCHAR ---> VARCHAR column
		dataslot_to := sample_Data_XXX_to.(*data.VARCHAR)
		fixlen_flag_to := dataslot_to.Data_fixlen_flag
		precision_to := dataslot_to.Data_precision

		tok_expression = decor.Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR_new_with_Dataslot(tok_expression, precision_to, fixlen_flag_to) // collation is inherited from tok_expression. This function may return tok_expression, and precision may be smaller than precision_to.

	default:
		if tok_expression, rsql_err = decor.Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot(tok_expression, sample_Data_XXX_to); rsql_err != nil { // collation is inherited from tok_expression. This function may return tok_expression.
			return nil, rsql_err
		}
	}

	// constant folding of expression

	if decor.Debug_disable_constant_folding_flag == false {
		emit.Constfold_walk_expression_tree(tok_expression) // constant fold the tree
	}

	decor.CONSTANTS.Const_walk_Token_primary(tok_expression) // fill CONSTANTS table with all constants dataslots

	return tok_expression, nil
}
