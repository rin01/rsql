package deco

import (
	"fmt"

	"rsql"
	"rsql/ast"
	"rsql/csr"
	"rsql/data"
	"rsql/dict"
)

const GROUPTABLE_NAME = "$grouptable$"
const SORTTABLE_NAME = "$sorttable$"

type Gf_entry_t struct {
	col_no   uint16
	dataslot rsql.IDataslot
}

// NOTE: the grouptable row layout is as follows:
//
// <$rowid$> <group by col1> ... <group by coln> <aggr func1> ... <aggr funcm>
//

type Group_splitup struct {
	grouptable_xtable              *ast.Xtable_table             // grouptable. Contains cursor for grouptable.
	grouptable_coldefs             []*rsql.Coldef                // coldefs for grouptable_gtabledef
	grouptable_gtabledef           *rsql.GTabledef               // grouptable_insertion_row will be inserted into grouptable for each group. Then, grouptable_aggrfuncs_to_inject will update grouptable_upsert_row.
	grouptable_insertion_row       []rsql.IDataslot              // For grouping fields, dataslots from xtable_select.Sel_groupby_expressions. For aggregate functions, dataslots are new, with NULL or 0 value, and are never modified.
	grouptable_upsert_row          []rsql.IDataslot              // this row is read from page, updated by grouptable_aggrfuncs_to_inject, and written back to page. It contains KIND_COL_LEAF and KIND_TMP dataslots.
	grouptable_row_expressions     []*ast.Token_primary          // expressions for grouptable row (all GROUP BY expressions, and simple aggregate functions listed in SELECT clause), used to look up for terms in HAVING clause.
	map_to_output_fields           map[rsql.IDataslot]Gf_entry_t // token original dataslot of simple column in GROUP BY --> grouptable_output_row dataslot
	grouptable_aggrfuncs_to_inject []*ast.Token_primary          // COUNT, SUM, etc expressions. They are all KIND_TMP dataslots.
	grouptable_output_row          []rsql.IDataslot              // grouptable row with KIND_COL_LEAF dataslots, used to output the row of grouptable

	colname_generator int        // generates names for columns without name
	decor             *Decorator // used only to update the line/pos for error message
}

func (gs *Group_splitup) generate_colname(prefix string) string {

	new_colname := fmt.Sprintf("%s%d", prefix, gs.colname_generator)

	gs.colname_generator++

	return new_colname
}

// Process_GROUPING_SELECT is the main function to process a grouping SELECT.
//
// It creates the grouptable, default insertion row, break up SELECT expressions into parts to inject into grouptable row, and parts that read grouptable, etc.
// The processing result is put in Group_splitup receiver.
//
func (gs *Group_splitup) Process_GROUPING_SELECT(xtable_select *ast.Xtable_select, grouptable_gtblid int64) (grouptable_xtable_from *ast.Xtable_from, rsql_err *rsql.Error) {
	var (
		grouptable_nk_width int
	)

	gs.grouptable_xtable = &ast.Xtable_table{Tbl_qname: rsql.Object_qname_t{Object_name: GROUPTABLE_NAME, Object_name_original: GROUPTABLE_NAME}}
	gs.grouptable_coldefs = make([]*rsql.Coldef, 0, 10)
	gs.grouptable_insertion_row = make([]rsql.IDataslot, 0, 10)
	gs.grouptable_upsert_row = make([]rsql.IDataslot, 0, 10)
	gs.grouptable_row_expressions = make([]*ast.Token_primary, 0, 10)
	gs.map_to_output_fields = make(map[rsql.IDataslot]Gf_entry_t, 20)
	gs.grouptable_aggrfuncs_to_inject = make([]*ast.Token_primary, 0, 10)
	gs.grouptable_output_row = make([]rsql.IDataslot, 0, 10)

	// process GROUP BY clause, aggregate functions, HAVING clause, etc

	if grouptable_nk_width, rsql_err = gs.process_GROUP_BY_clause(xtable_select.Sel_groupby_expressions); rsql_err != nil { // fills in gs.map_to_output_fields
		return nil, rsql_err
	}

	if rsql_err = gs.process_SELECT_clause(xtable_select.Sel_columns, xtable_select.Sel_groupby_expressions); rsql_err != nil {
		return nil, rsql_err
	}

	if xtable_select.Sel_grouptable_having != nil {
		if rsql_err := gs.process_HAVING_tree(xtable_select.Sel_grouptable_having); rsql_err != nil {
			return nil, rsql_err
		}
	}

	// create Tabledef

	tabledef := &rsql.Tabledef{
		Td_tblid: grouptable_gtblid,

		Td_dbid:                dict.TRASHDB_DBID, // just a dummy dbid, in the case an error message looks up for the name of the database. I think it doesn't happen, but just to be sure.
		Td_schid:               dict.SCHID_DBO,    // see comment above
		Td_base_gtblid:         grouptable_gtblid, // negative number
		Td_index_name:          "unique",
		Td_index_name_original: "unique",

		Td_file_path: "",

		Td_durability:   rsql.DURABILITY_FLASH,
		Td_type:         rsql.TD_TYPE_BASE_TABLE,
		Td_index_type:   rsql.TD_UNIQUE, // not primary key because nk fields can contain NULL values
		Td_cluster_type: rsql.TD_CLUSTERED,

		Td_coldefs: gs.grouptable_coldefs,
		Td_nk:      append([]*rsql.Coldef{}, gs.grouptable_coldefs[1:grouptable_nk_width+1]...), // can be empty list if no GROUP BY clause

		Td_status: rsql.TD_STATUS_VALID,
	}

	gtabledef := &rsql.GTabledef{
		Gtblid: grouptable_gtblid, // negative number. gtblid for grouptable is not global. It is unique only in the batch.

		Gdbid:                dict.TRASHDB_DBID, // same comment as for Tabledef above
		Gschid:               dict.SCHID_DBO,    // see comment above
		Gtable_name:          GROUPTABLE_NAME,
		Gtable_name_original: GROUPTABLE_NAME,

		Base:     tabledef,
		Indexmap: nil,
		Colmap:   make(map[string]*rsql.Coldef, len(tabledef.Td_coldefs)),
	}

	gs.grouptable_gtabledef = gtabledef
	xtable_select.Sel_grouptable_gtabledef = gtabledef

	for _, coldef := range tabledef.Td_coldefs {
		if coldef.Cd_colname != "" {
			gtabledef.Colmap[coldef.Cd_colname] = coldef
		}
	}

	// create cursor and xtable_from

	cursor_grouptable := csr.New_cursor_table_with_row(gtabledef, gs.grouptable_output_row)
	gs.grouptable_xtable.Tbl_cursor = cursor_grouptable

	xtable_from := &ast.Xtable_from{
		Fr_list:   []ast.Xtable{gs.grouptable_xtable},
		Fr_where:  xtable_select.Sel_grouptable_having,
		Fr_cursor: csr.New_cursor_from(cursor_grouptable),
	}

	return xtable_from, nil
}

// process_GROUP_BY_clause creates the native key of the grouptable, and the native key of the insertion row.
// If no GROUP BY clause, nk_width == 0.
//
func (gs *Group_splitup) process_GROUP_BY_clause(groupby_clause []*ast.Token_primary) (nk_width int, rsql_err *rsql.Error) {

	// add ROWID column

	coldef_rowid := &rsql.Coldef{ // rowid is the first column. It doesn't belong to the native key.
		Cd_colname:            rsql.ROWID_INTERNAL, // colname is $rowid$
		Cd_colname_original:   rsql.ROWID_INTERNAL,
		Cd_datatype:           rsql.DATATYPE_BIGINT,
		Cd_precision:          0,
		Cd_scale:              0,
		Cd_fixlen_flag:        false,
		Cd_collation_strength: 0,
		Cd_collation:          "",
		Cd_autonum:            rsql.CD_AUTONUM_ROWID,
		Cd_NOT_NULL_flag:      true,
		Cd_base_seqno:         0,
	}

	dataslot_rowid := data.New_BIGINT_NULL(rsql.KIND_COL_LEAF)
	dataslot_rowid_clone_for_upsert := data.New_BIGINT_NULL(rsql.KIND_COL_LEAF)
	dataslot_rowid_clone_for_output := data.New_BIGINT_NULL(rsql.KIND_COL_LEAF)

	gs.grouptable_coldefs = append(gs.grouptable_coldefs, coldef_rowid)
	gs.grouptable_insertion_row = append(gs.grouptable_insertion_row, dataslot_rowid)
	gs.grouptable_upsert_row = append(gs.grouptable_upsert_row, dataslot_rowid_clone_for_upsert)
	gs.grouptable_output_row = append(gs.grouptable_output_row, dataslot_rowid_clone_for_output)

	// add GROUP BY columns, which make up the native key

	rsql.Assert(len(groupby_clause) > 0)

	if len(groupby_clause) > rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_GROUP_BY_IN_SELECT, rsql.ERROR_BATCH_ABORT, rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX)
	}

	for _, tok := range groupby_clause {
		if kind := tok.Prim_dataslot.Kind(); kind&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF) != 0 { // constant or @variable not allowed (there is no technical reason for this, though)
			if !(len(groupby_clause) == 1 && kind == rsql.KIND_RO_LEAF) { // because when no explicit GROUP BY exists, GROUP BY 1   has been created by the parser
				return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_GROUPBY_CONST_VAR_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT) // to allow const or @var, comment
			}
		}

		gs.grouptable_insertion_row = append(gs.grouptable_insertion_row, tok.Prim_dataslot) // grouptable_insertion_row will be used for row lookup

		dataslot_clone_for_upsert := data.Dataslot_XXX_clone(rsql.KIND_COL_LEAF, tok.Prim_dataslot) // append new dataslot for grouptable_upsert_row
		dataslot_clone_for_output := data.Dataslot_XXX_clone(rsql.KIND_COL_LEAF, tok.Prim_dataslot) // append new dataslot for grouptable_output_row
		colname := ""
		if tok.Prim_dataslot.Kind() == rsql.KIND_COL_LEAF {
			colname = tok.Prim_name

			gs.map_to_output_fields[tok.Prim_dataslot] = Gf_entry_t{col_no: uint16(len(gs.grouptable_coldefs)), dataslot: dataslot_clone_for_output} // GROUP BY a, a, a, a   is allowed, but useless
		}
		coldef := New_coldef_from_dataslot(colname, len(gs.grouptable_coldefs), tok.Prim_collation, dataslot_clone_for_output) // Coldef for grouptable
		gs.grouptable_coldefs = append(gs.grouptable_coldefs, coldef)

		gs.grouptable_upsert_row = append(gs.grouptable_upsert_row, dataslot_clone_for_upsert) // used to read tuple from grouptable, modify aggregate cols, and write it back to page
		gs.grouptable_output_row = append(gs.grouptable_output_row, dataslot_clone_for_output)
		gs.grouptable_row_expressions = append(gs.grouptable_row_expressions, tok)
	}

	return len(groupby_clause), nil
}

func (gs *Group_splitup) process_SELECT_clause(columns_list []*ast.Column, groupby_clause []*ast.Token_primary) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	aggrfunc_list := make([]*ast.Token_primary, 0, 10)

	for _, column := range columns_list {
		gs.decor.Info_line = column.Col_expression.Batch_line() // update line/pos for error message

		if n := check_KIND_TMP_tree_is_in_list(column.Col_expression, groupby_clause); n >= 0 {
			tok_output := ast.Token_primary{
				Token: column.Col_expression.Token,

				Prim_type:    ast.TOK_PRIM_IDENTIFIER,
				Prim_subtype: ast.TOK_PRIM_IDENTIFIER_SUB,

				Prim_table_name: GROUPTABLE_NAME,
				Prim_name:       gs.generate_colname("$g$"), // name is not mandatory, but it is easier to debug when displaying AST tree (rcli -t option)

				Prim_status: ast.TOK_PRIM_STATUS_COMPLETE,

				Prim_collation_strength: 0,
				Prim_collation:          "",

				Prim_xtable_ds: gs.grouptable_xtable,
				Prim_col_no:    uint16(n + 1),
				Prim_dataslot:  gs.grouptable_output_row[n+1], // points to dataslot in the grouptable output row. +1 because first column is always $rowid$.
			}

			if tok_output.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
				tok_output.Prim_collation_strength = rsql.COLLSTRENGTH_1
				tok_output.Prim_collation = column.Col_expression.Prim_collation
			}

			*column.Col_expression = tok_output

			rsql.Assert(column.Col_expression.Prim_dataslot.Kind() == rsql.KIND_COL_LEAF)

			continue
		}

		// else, column is an expression made of aggregate functions and simple columns listed in GROUP BY (that is, not complex expressions listed in GROUP BY)

		aggrfunc_list = aggrfunc_list[:0]
		if aggrfunc_list, rsql_err = gs.splitup_expression(aggrfunc_list, column.Col_expression); rsql_err != nil { // all column identifiers (but not those belonging to the arguments of aggregate functions) are modified, so that their dataslots point to the native key dataslots of grouptable_output_row.
			return rsql_err
		}

		if len(aggrfunc_list) > 0 { // each aggregate function in expression will become a column in grouptable
			for _, tok := range aggrfunc_list {
				// clone aggregate function token

				tok_clone := &ast.Token_primary{} // original tok will be transformed later in TOK_PRIM_IDENTIFIER, pointing to group_output_row dataslot
				*tok_clone = *tok

				gs.grouptable_aggrfuncs_to_inject = append(gs.grouptable_aggrfuncs_to_inject, tok_clone) // COUNT, SUM, MIN, MAX, etc. Dataslot belongs to grouptable_upsert_row.

				// append new dataslot for grouptable_insertion_row

				dataslot_0 := data.Dataslot_XXX_clone(rsql.KIND_COL_LEAF, tok_clone.Prim_dataslot)
				switch tok_clone.Prim_sysfunc_descr.Sd_id {
				case ast.SYSFUNC_AGGR_COUNT:
					rsql.Assert(dataslot_0.Datatype() == rsql.DATATYPE_INT)
					rsql_err := data.Set_int32_value(dataslot_0, 0)
					rsql.Assert(rsql_err == nil)

				case ast.SYSFUNC_AGGR_COUNT_BIG:
					rsql.Assert(dataslot_0.Datatype() == rsql.DATATYPE_BIGINT)
					rsql_err := data.Set_int32_value(dataslot_0, 0)
					rsql.Assert(rsql_err == nil)
				}

				gs.grouptable_insertion_row = append(gs.grouptable_insertion_row, dataslot_0)
				gs.grouptable_row_expressions = append(gs.grouptable_row_expressions, tok_clone)
				gs.grouptable_upsert_row = append(gs.grouptable_upsert_row, tok.Prim_dataslot)

				// transform tok into a normal identifier token

				prefix := ""

				switch tok_clone.Prim_sysfunc_descr.Sd_id {
				case ast.SYSFUNC_AGGR_COUNT,
					ast.SYSFUNC_AGGR_COUNT_BIG:
					prefix = "$count$"
				case ast.SYSFUNC_AGGR_SUM:
					prefix = "$sum$"
				case ast.SYSFUNC_AGGR_MIN:
					prefix = "$min$"
				case ast.SYSFUNC_AGGR_MAX:
					prefix = "$max$"
				default:
					panic("impossible")
				}

				output_dataslot := data.Dataslot_XXX_clone(rsql.KIND_COL_LEAF, tok.Prim_dataslot) // dataslot belongs to grouptable_output_row

				tok_output := ast.Token_primary{
					Token: tok.Token,

					Prim_type:    ast.TOK_PRIM_IDENTIFIER,
					Prim_subtype: ast.TOK_PRIM_IDENTIFIER_SUB,

					Prim_table_name: GROUPTABLE_NAME,
					Prim_name:       gs.generate_colname(prefix), // name is not mandatory, but it is easier to debug when displaying AST tree (rcli -t option)

					Prim_status: ast.TOK_PRIM_STATUS_COMPLETE,

					Prim_collation_strength: 0,
					Prim_collation:          "",

					Prim_xtable_ds: gs.grouptable_xtable,
					Prim_col_no:    uint16(len(gs.grouptable_coldefs)),
					Prim_dataslot:  output_dataslot, // dataslot belongs to grouptable_output_row
				}

				if tok_output.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
					tok_output.Prim_collation_strength = rsql.COLLSTRENGTH_1
					tok_output.Prim_collation = tok.Prim_collation
				}

				*tok = tok_output

				coldef := New_coldef_from_dataslot(tok.Prim_name, len(gs.grouptable_coldefs), tok.Prim_collation, output_dataslot) // Coldef for grouptable
				gs.grouptable_coldefs = append(gs.grouptable_coldefs, coldef)
				gs.grouptable_output_row = append(gs.grouptable_output_row, output_dataslot)
			}
		}
	}

	return nil
}

// splitup_expression walk the tok expression tree, and search for aggregate functions.
//
// Aggregate functions are appended to aggrlist and returned in aggrlist2.
//
// All column identifiers (but not those belonging to the arguments of aggregate functions) are modified, so that their dataslots point to dataslots of the grouptable output row.
//
func (gs *Group_splitup) splitup_expression(aggrlist []*ast.Token_primary, tok *ast.Token_primary) (aggrlist2 []*ast.Token_primary, rsql_err *rsql.Error) {
	var (
		sysoperand *ast.Token_primary
	)

	gs.decor.Info_line = tok.Batch_line() // update line/pos for error message

	// if KIND_RO_LEAF, KIND_VAR_LEAF or KIND_COL_LEAF, it is not an operator nor a function, but a simple data. It may be a literal, a variable or a column.

	if tok.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF) != 0 { // in this case, just return.
		return aggrlist, nil
	}

	if tok.Prim_dataslot.Kind() == rsql.KIND_COL_LEAF {
		var entry Gf_entry_t
		var ok bool

		if entry, ok = gs.map_to_output_fields[tok.Prim_dataslot]; ok == false {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_NOT_IN_GROUP_BY, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		tok.Prim_xtable_ds = gs.grouptable_xtable
		tok.Prim_col_no = entry.col_no
		tok.Prim_dataslot = entry.dataslot // make tok.Prim_dataslot point to a dataslot of grouptable_output_row

		return aggrlist, nil
	}

	// current token is aggregate function

	if tok.Prim_type == ast.TOK_PRIM_SYSFUNC && tok.Prim_sysfunc_descr.Is_aggregate() == true {
		aggrlist = append(aggrlist, tok)

		return aggrlist, nil // don't walk the operand
	}

	// SYSCOLLATOR and SYSLANGUAGE argument (for the moment, no instruction has both arguments at the same time)
	// note: tok.Prim_syscollator and tok.Prim_syslanguage are KIND_RO_LEAF or KIND_VAR_LEAF, so they don't need to be walked.

	sysoperand = nil
	if tok.Prim_syscollator != nil {
		sysoperand = tok.Prim_syscollator // always a literal or a variable
		rsql.Assert(tok.Prim_syslanguage == nil)
	} else if tok.Prim_syslanguage != nil {
		sysoperand = tok.Prim_syslanguage // always a literal or a variable
	}

	// walk left, right, listexpr operands and sysoperand

	if tok.Prim_left != nil {
		if aggrlist, rsql_err = gs.splitup_expression(aggrlist, tok.Prim_left); rsql_err != nil { // left operand
			return nil, rsql_err
		}
	}

	if tok.Prim_right != nil {
		if aggrlist, rsql_err = gs.splitup_expression(aggrlist, tok.Prim_right); rsql_err != nil { // left operand
			return nil, rsql_err
		}
	}

	for _, tok_arg := range tok.Prim_listexpr {
		if aggrlist, rsql_err = gs.splitup_expression(aggrlist, tok_arg); rsql_err != nil { // list of operands
			return nil, rsql_err
		}
	}

	if sysoperand != nil {
		if aggrlist, rsql_err = gs.splitup_expression(aggrlist, sysoperand); rsql_err != nil { // sysoperand
			return nil, rsql_err
		}
	}

	return aggrlist, nil
}

func check_KIND_TMP_tree_is_in_list(tok *ast.Token_primary, list []*ast.Token_primary) int {

	if tok.Prim_dataslot.Kind() != rsql.KIND_TMP {
		return -1
	}

	if len(list) == 0 {
		return -1
	}

	for i, tok0 := range list {
		if tok0.Prim_dataslot.Kind() != rsql.KIND_TMP {
			continue
		}

		if Compare_tok_trees(tok, tok0) == true {
			return i
		}
	}

	return -1
}

func check_KIND_TMP_tree_contains_only_variables_and_constants(tok *ast.Token_primary) bool {

	switch tok.Prim_dataslot.Kind() {
	case rsql.KIND_RO_LEAF,
		rsql.KIND_VAR_LEAF: // constant or variable
		return true

	case rsql.KIND_COL_LEAF:
		return false

	case rsql.KIND_TMP:
		if tok.Prim_left != nil && check_KIND_TMP_tree_contains_only_variables_and_constants(tok.Prim_left) == false {
			return false
		}
		if tok.Prim_right != nil && check_KIND_TMP_tree_contains_only_variables_and_constants(tok.Prim_right) == false {
			return false
		}
		for _, expr := range tok.Prim_listexpr {
			if expr != nil && check_KIND_TMP_tree_contains_only_variables_and_constants(expr) == false {
				return false
			}
		}
		return true

	default:
		panic("impossible")
	}
}

func (gs *Group_splitup) process_HAVING_tree(tok *ast.Token_primary) *rsql.Error {

	gs.decor.Info_line = tok.Batch_line() // update line/pos for error message

	// process AND, OR, NOT

	if tok.Prim_dataslot.Kind() == rsql.KIND_TMP {
		switch {
		case tok.Prim_instruction_code == rsql.INSTR_LOGICAL_UNARY_NOT_BOOLEAN:
			if rsql_err := gs.process_HAVING_tree(tok.Prim_left); rsql_err != nil {
				return rsql_err
			}
			return nil

		case tok.Prim_instruction_code == rsql.INSTR_LOGICAL_AND_BOOLEAN ||
			tok.Prim_instruction_code == rsql.INSTR_LOGICAL_OR_BOOLEAN:
			if rsql_err := gs.process_HAVING_tree(tok.Prim_left); rsql_err != nil {
				return rsql_err
			}
			if rsql_err := gs.process_HAVING_tree(tok.Prim_right); rsql_err != nil {
				return rsql_err
			}
			return nil

		case tok.Prim_subtype == ast.TOK_PRIM_OPERATOR_COMP_GREATER ||
			tok.Prim_subtype == ast.TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL ||
			tok.Prim_subtype == ast.TOK_PRIM_OPERATOR_COMP_EQUAL ||
			tok.Prim_subtype == ast.TOK_PRIM_OPERATOR_COMP_LESS_EQUAL ||
			tok.Prim_subtype == ast.TOK_PRIM_OPERATOR_COMP_LESS ||
			tok.Prim_subtype == ast.TOK_PRIM_OPERATOR_COMP_NOT_EQUAL:
			if rsql_err := gs.process_HAVING_tree(tok.Prim_left); rsql_err != nil {
				return rsql_err
			}
			if rsql_err := gs.process_HAVING_tree(tok.Prim_right); rsql_err != nil {
				return rsql_err
			}
			return nil

		default:
			// pass
		}
	}

	// process constant and @variable

	if tok.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF) != 0 {
		return nil
	}

	// process column

	if tok.Prim_dataslot.Kind() == rsql.KIND_COL_LEAF {
		var entry Gf_entry_t
		var ok bool

		if entry, ok = gs.map_to_output_fields[tok.Prim_dataslot]; ok == false {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_NOT_IN_GROUP_BY, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		tok.Prim_xtable_ds = gs.grouptable_xtable
		tok.Prim_col_no = entry.col_no
		tok.Prim_dataslot = entry.dataslot // make tok.Prim_dataslot point to a dataslot of grouptable_output_row

		return nil
	}

	// process operator or function

	rsql.Assert(tok.Prim_dataslot.Kind() == rsql.KIND_TMP)

	if check_KIND_TMP_tree_contains_only_variables_and_constants(tok) {
		return nil
	}

	if n := check_KIND_TMP_tree_is_in_list(tok, gs.grouptable_row_expressions); n >= 0 {
		tok_output := ast.Token_primary{
			Token: tok.Token,

			Prim_type:    ast.TOK_PRIM_IDENTIFIER,
			Prim_subtype: ast.TOK_PRIM_IDENTIFIER_SUB,

			Prim_table_name: GROUPTABLE_NAME,
			Prim_name:       gs.generate_colname("$x$"), // name is not mandatory, but it is easier to debug when displaying AST tree (rcli -t option)

			Prim_status: ast.TOK_PRIM_STATUS_COMPLETE,

			Prim_collation_strength: 0,
			Prim_collation:          "",

			Prim_xtable_ds: gs.grouptable_xtable,
			Prim_col_no:    uint16(n + 1),
			Prim_dataslot:  gs.grouptable_output_row[n+1], // points to dataslot in the grouptable_output_row. +1 because first column is always rowid.
		}

		if tok_output.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
			tok_output.Prim_collation_strength = rsql.COLLSTRENGTH_1
			tok_output.Prim_collation = tok.Prim_collation
		}

		*tok = tok_output

		return nil
	}

	return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_HAVING_TERM, rsql.ERROR_BATCH_ABORT)
}

//===============================================================

func create_leaf_row_orderby(xtable_select *ast.Xtable_select) rsql.Row {
	var leaf_row_orderby rsql.Row

	// create sorttable row

	leaf_row_orderby = make([]rsql.IDataslot, len(xtable_select.Sel_columns)+1)

	leaf_row_orderby[0] = data.New_BIGINT_NULL(rsql.KIND_COL_LEAF) // $rowid$

	for i, column := range xtable_select.Sel_columns {
		leaf_row_orderby[i+1] = data.Dataslot_XXX_clone(rsql.KIND_COL_LEAF, column.Col_expression.Prim_dataslot)
	}

	return leaf_row_orderby
}

type Orderby_attr struct {
	Col_no          int
	Force_collation string // collation of the ORDER BY item, but only if COLLSTRENGTH_2. Else, empty string.
}

// columns_orderby is only used for comparison with natural order of the result set, so that ORDER BY can be discarded if useless.
//
func (decor *Decorator) process_ORDER_BY(xtable_select *ast.Xtable_select, leaf_row_orderby rsql.Row, orderby []*ast.Orderby_part) (gtabledef_orderby *rsql.GTabledef, columns_orderby []rsql.IDataslot, rsql_err *rsql.Error) {

	nk_list := make([]Orderby_attr, 0, len(orderby))

	// create map for column identifier names

	colident_map := make(map[string]*ast.Column, len(xtable_select.Sel_columns))

	for _, column := range xtable_select.Sel_columns {
		if column.Col_expression.Prim_type == ast.TOK_PRIM_IDENTIFIER { // if column is a plain table column (that is, not an expression)
			col_identname := column.Col_expression.Prim_name
			if _, ok := colident_map[col_identname]; ok == true { // column name already exists. It is ambiguous.
				colident_map[col_identname] = nil
			} else {
				colident_map[col_identname] = column
			}
		}
	}

	// lookup identifier column name

	coltok_list := make([]*ast.Token_primary, 0, len(xtable_select.Sel_columns))

	for _, column := range xtable_select.Sel_columns {
		coltok_list = append(coltok_list, column.Col_expression)
	}

	xtable_orderby := &ast.Xtable_table{Tbl_qname: rsql.Object_qname_t{Object_name: SORTTABLE_NAME, Object_name_original: SORTTABLE_NAME}}

	for i, odpart := range orderby {
		tok := odpart.Odr_expression
		decor.Info_line = tok.Batch_line() // update line/pos for error message

		n := -1
		is_colalias_name := false

		if tok.Prim_dataslot == nil {
			is_colalias_name = true
		}

		// process constant and @variable

		if is_colalias_name == false && tok.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF) != 0 { // dataslot is nil if tok is an alias name
			continue
		}

		// process column

		switch {
		case tok.Prim_dataslot == nil || tok.Prim_dataslot.Kind() == rsql.KIND_COL_LEAF:
			var ok bool
			var column *ast.Column

			//for name, _ := range xtable_select.Sel_columns_map {
			//	println("       ----", name)
			//}

			if column, ok = colident_map[tok.Prim_name]; ok == false {
				if column, ok = xtable_select.Sel_columns_map[tok.Prim_name]; ok == false { // also find if corresponding alias name exists
					return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_COLUMN_NAME_NOT_FOUND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
				}
			}

			if column == nil {
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_COLUMN_NAME, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
			}

			n = column.Col_no

		default:
			rsql.Assert(tok.Prim_dataslot.Kind() == rsql.KIND_TMP)

			if n = check_KIND_TMP_tree_is_in_list(tok, coltok_list); n < 0 {
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_EXPRESSION_NOT_FOUND, rsql.ERROR_BATCH_ABORT)
			}
		}

		tok_new := ast.Token_primary{
			Token: tok.Token,

			Prim_type:    ast.TOK_PRIM_IDENTIFIER,
			Prim_subtype: ast.TOK_PRIM_IDENTIFIER_SUB,

			Prim_table_name: SORTTABLE_NAME,
			Prim_name:       fmt.Sprintf("$x$%d", i), // name is not mandatory, but it is easier to debug when displaying AST tree (rcli -t option)

			Prim_status: ast.TOK_PRIM_STATUS_COMPLETE,

			Prim_collation_strength: 0,
			Prim_collation:          "",

			Prim_xtable_ds: xtable_orderby,
			Prim_col_no:    uint16(n + 1),
			Prim_dataslot:  leaf_row_orderby[n+1], // points to dataslot in the grouptable row. +1 because first column is always rowid.
		}

		collation_to_force := ""
		if tok_new.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
			tok_new.Prim_collation_strength = rsql.COLLSTRENGTH_1
			tok_new.Prim_collation = tok.Prim_collation

			if tok.Prim_collation_strength == rsql.COLLSTRENGTH_2 {
				collation_to_force = tok.Prim_collation
			}
		}

		if is_colalias_name == true && tok.Prim_sql_collate != "" {
			if tok_new.Prim_dataslot.Datatype() != rsql.DATATYPE_VARCHAR {
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLLATE_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
			}

			tok.Prim_collation = tok.Prim_sql_collate // as walk of column alias resulted in error, no dataslot has been attached and tok.Prim_collation has not been set
			collation_to_force = tok.Prim_collation
		}

		*tok = tok_new // ORDER BY expression is replaced by TOK_PRIM_IDENTIFIER referencing a column in sorttable

		nk_list = append(nk_list, Orderby_attr{Col_no: n, Force_collation: collation_to_force})

		columns_orderby = append(columns_orderby, xtable_select.Sel_columns[n].Col_expression.Prim_dataslot)
	}

	// create gtabledef for sorttable

	sorttable_gtblid := decor.Get_flashtable_new_tblid()
	gtabledef_orderby = create_tabledef_for_SELECT_sort(xtable_select, leaf_row_orderby, sorttable_gtblid, nk_list)

	return gtabledef_orderby, columns_orderby, nil
}

//===============================================================

func New_coldef_from_dataslot(colname string, base_seqno int, collation string, dataslot rsql.IDataslot) *rsql.Coldef {

	coldef := &rsql.Coldef{
		Cd_colname:          colname,
		Cd_colname_original: colname,

		Cd_datatype:           dataslot.Datatype(),
		Cd_precision:          0,
		Cd_scale:              0,
		Cd_fixlen_flag:        false,
		Cd_collation_strength: 0,
		Cd_collation:          "",

		Cd_autonum:       rsql.CD_AUTONUM_NONE,
		Cd_NOT_NULL_flag: false, // NULL always allowed

		Cd_base_seqno: uint16(base_seqno), // seqno of the base table
	}

	switch dataslot := dataslot.(type) {
	case *data.VARBINARY:
		coldef.Cd_precision = dataslot.Data_precision

	case *data.VARCHAR:
		coldef.Cd_precision = dataslot.Data_precision
		coldef.Cd_fixlen_flag = dataslot.Data_fixlen_flag
		coldef.Cd_collation_strength = rsql.COLLSTRENGTH_1
		coldef.Cd_collation = collation

	case *data.NUMERIC:
		coldef.Cd_precision = dataslot.Data_precision
		coldef.Cd_scale = dataslot.Data_scale
	}

	return coldef
}

// Argument xtable_select is used only for column names and collations.
// Argument leaf_row_orderby is used for datatype definition (precision, scale, etc).
//
func create_tabledef_for_SELECT_sort(xtable_select *ast.Xtable_select, leaf_row_orderby rsql.Row, sorttable_gtblid int64, nk_list []Orderby_attr) *rsql.GTabledef {
	var (
		tabledef        *rsql.Tabledef
		gtabledef       *rsql.GTabledef
		list_coldefs_nk []*rsql.Coldef
	)

	rsql.Assert(len(leaf_row_orderby) == len(xtable_select.Sel_columns)+1)

	// create list of Coldefs

	list_coldefs := make([]*rsql.Coldef, 0, len(xtable_select.Sel_columns)+1)

	coldef_rowid := &rsql.Coldef{ // rowid is the first column. It doesn't belong to the native key.
		Cd_colname:            rsql.ROWID_INTERNAL, // colname is $rowid$
		Cd_colname_original:   rsql.ROWID_INTERNAL,
		Cd_datatype:           rsql.DATATYPE_BIGINT,
		Cd_precision:          0,
		Cd_scale:              0,
		Cd_fixlen_flag:        false,
		Cd_collation_strength: 0,
		Cd_collation:          "",
		Cd_autonum:            rsql.CD_AUTONUM_ROWID,
		Cd_NOT_NULL_flag:      true,
		Cd_base_seqno:         0,
	}

	list_coldefs = append(list_coldefs, coldef_rowid)

	for i, column := range xtable_select.Sel_columns {
		dataslot := leaf_row_orderby[i+1]

		colname := column.Col_label
		if colname == "" && column.Col_expression.Prim_type == ast.TOK_PRIM_IDENTIFIER {
			colname = column.Col_expression.Prim_name
		}

		coldef := New_coldef_from_dataslot(colname, i+1, column.Col_expression.Prim_collation, dataslot)

		list_coldefs = append(list_coldefs, coldef)
	}

	for _, orderby_attr := range nk_list { // create list of nk fields
		col_no := orderby_attr.Col_no
		coldef := list_coldefs[col_no+1]
		if orderby_attr.Force_collation != "" { // if ORDER BY item collation was COLLSTRENGTH_2
			rsql.Assert(coldef.Cd_datatype == rsql.DATATYPE_VARCHAR)
			coldef.Cd_collation = orderby_attr.Force_collation // force column collation
		}

		list_coldefs_nk = append(list_coldefs_nk, coldef)
	}
	list_coldefs_nk = append(list_coldefs_nk, list_coldefs[0]) // append $rowid$ to make index unique

	// create Tabledef

	tabledef = &rsql.Tabledef{
		Td_tblid: sorttable_gtblid,

		Td_dbid:                dict.TRASHDB_DBID, // just a dummy dbid, in the case an error message looks up for the name of the database. I think it down't happen, but just to be sure.
		Td_schid:               dict.SCHID_DBO,    // see comment above
		Td_base_gtblid:         sorttable_gtblid,  // negative number
		Td_index_name:          "unique",
		Td_index_name_original: "unique",

		Td_file_path: "",

		Td_durability:   rsql.DURABILITY_FLASH,
		Td_type:         rsql.TD_TYPE_BASE_TABLE,
		Td_index_type:   rsql.TD_UNIQUE, // not primary key because nk fields can contain NULL values
		Td_cluster_type: rsql.TD_CLUSTERED,

		Td_coldefs: list_coldefs,    // list of all columns stored in sorttable, in physical order
		Td_nk:      list_coldefs_nk, // list of columns making up the native key of this table. The column rowid is appended to be sure that the native key is unique.
		Td_payload: nil,

		Td_status: rsql.TD_STATUS_VALID,
	}

	gtabledef = &rsql.GTabledef{
		Gtblid: sorttable_gtblid, // negative number. gtblid for sorttable is not global. It is unique only in the batch.

		Gdbid:                dict.TRASHDB_DBID, // same comment as for Tabledef above
		Gschid:               dict.SCHID_DBO,    // see comment above
		Gtable_name:          SORTTABLE_NAME,
		Gtable_name_original: SORTTABLE_NAME,

		Base:     tabledef,
		Indexmap: nil,
		Colmap:   make(map[string]*rsql.Coldef, len(tabledef.Td_coldefs)),
	}

	for _, coldef := range tabledef.Td_coldefs {
		if coldef.Cd_colname != "" {
			gtabledef.Colmap[coldef.Cd_colname] = coldef
		}
	}

	return gtabledef
}

func Select_ORDER_BY_is_naturally_ordered(columns_orderby []rsql.IDataslot, natural_order []rsql.IDataslot) bool {

	if len(columns_orderby) > len(natural_order) {
		return false
	}

	for i, dataslot := range columns_orderby {
		if dataslot != natural_order[i] {
			return false
		}
	}

	return true
}

func orderby_is_same_as_grouping_order(grouptable_output_row []rsql.IDataslot, grouptable_gtabledef *rsql.GTabledef, select_columns []*ast.Column, sorttable_gtabledef *rsql.GTabledef) bool {

	grouptabledef := grouptable_gtabledef.Base
	sorttabledef := sorttable_gtabledef.Base

	if rsql.G_DEBUG_FLAG {
		println("-- grouptable nk --")

		for i, coldef := range grouptabledef.Td_nk {
			dataslot := grouptable_output_row[coldef.Cd_base_seqno]
			println(i, dataslot, dataslot.Datatype().String(), coldef.Cd_base_seqno)
		}

		println("-- sorttable dataslots --")

		for i, coldef := range sorttabledef.Td_nk[:len(sorttabledef.Td_nk)-1] {
			column := select_columns[coldef.Cd_base_seqno-1]
			tok := column.Col_expression
			dataslot := tok.Prim_dataslot
			println(i, dataslot, dataslot.Datatype().String(), coldef.Cd_base_seqno, tok.Prim_name)
		}
	}

	// check if sorttable nk fields comply with grouptable nk fields

	if len(sorttabledef.Td_nk)-1 > len(grouptabledef.Td_nk) { // if number of ORDER BY items > nk fields in grouptable
		return false
	}

	for i, coldef := range sorttabledef.Td_nk[:len(sorttabledef.Td_nk)-1] { // for all sorttable nk fields, except last field $rowid$
		column := select_columns[coldef.Cd_base_seqno-1]
		tok := column.Col_expression
		dataslot := tok.Prim_dataslot

		coldef_gr := grouptabledef.Td_nk[i]
		dataslot_gr := grouptable_output_row[coldef_gr.Cd_base_seqno]

		if dataslot != dataslot_gr {
			return false
		}
	}

	return true
}
