package deco

import (
	"rsql"
	"rsql/ast"
)

// create_new_Basicblock creates new Basicblock.
//
func (decor *Decorator) create_new_Basicblock() *rsql.Basicblock {
	var (
		bb *rsql.Basicblock
	)

	// create new Basicblock

	bb = &rsql.Basicblock{}

	bb.Bb_instr3ac_list = make([]rsql.Instr3ac, 0, 10) // 10 is an arbitrary initial capacity. It is good enough.

	return bb
}

// create_and_register_new_Basicblock_current creates new Basicblock and makes it current.
//
func (decor *Decorator) create_and_register_new_Basicblock_current() *rsql.Basicblock {
	var (
		bb *rsql.Basicblock
	)

	// create new Basicblock

	bb = &rsql.Basicblock{}

	bb.Bb_instr3ac_list = make([]rsql.Instr3ac, 0, 10) // 10 is an arbitrary initial capacity. It is good enough.

	// register Basicblock

	decor.Basicblock_list = append(decor.Basicblock_list, bb)

	decor.Basicblock_current = bb

	return bb
}

// wacf_Token_stmt_IF walks the IF statement.
// It creates and references Basicblocks. We need to create Basicblocks now during decorator stage, because when we will emit bytecode in the next stage, jump instructions can refer to a Basicblock located farther.
//
func (decor *Decorator) wacf_Token_stmt_IF(tok *ast.Token_stmt_IF) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error

		basicblock_pre        *rsql.Basicblock
		basicblock_then_start *rsql.Basicblock
		basicblock_then_end   *rsql.Basicblock
		basicblock_else_start *rsql.Basicblock
		basicblock_else_end   *rsql.Basicblock
		basicblock_post       *rsql.Basicblock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_IF)

	decor.Info_line = tok.Batch_line() // update line/pos for error message

	//=== walk condition ===

	if rsql_err = decor.Wacf_expression(tok.If_cond); rsql_err != nil {
		return rsql_err
	}

	if tok.If_cond.Prim_dataslot.Datatype() != rsql.DATATYPE_BOOLEAN {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BOOLEAN_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	basicblock_pre = decor.Basicblock_current // save current basicblock

	//=== walk THEN branch ===

	rsql.Assert(tok.If_then != nil)

	basicblock_then_start = decor.create_and_register_new_Basicblock_current() // start a new basicblock for THEN

	if rsql_err = decor.wacf_sequence_of_Token(tok.If_then); rsql_err != nil {
		return rsql_err
	}

	basicblock_then_end = decor.Basicblock_current

	//=== walk ELSE branch ===

	if tok.If_else != nil {
		basicblock_else_start = decor.create_and_register_new_Basicblock_current() // start a new basicblock for ELSE

		if rsql_err = decor.wacf_sequence_of_Token(tok.If_else); rsql_err != nil {
			return rsql_err
		}

		basicblock_else_end = decor.Basicblock_current
	}

	//=== basicblock post ===

	basicblock_post = decor.create_and_register_new_Basicblock_current() // start a new basicblock

	//=== save basicblocks into IF stmt ===

	tok.If_basicblock_pre = basicblock_pre
	tok.If_basicblock_then_start = basicblock_then_start
	tok.If_basicblock_then_end = basicblock_then_end
	tok.If_basicblock_else_start = basicblock_else_start
	tok.If_basicblock_else_end = basicblock_else_end
	tok.If_basicblock_post = basicblock_post

	return nil
}

// wacf_Token_stmt_WHILE walks the WHILE statement.
// It creates and references Basicblocks. We need to create Basicblocks now during decorator stage, because when we will emit bytecode in the next stage, jump instructions can refer to a Basicblock located farther.
//
func (decor *Decorator) wacf_Token_stmt_WHILE(tok *ast.Token_stmt_WHILE) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error

		basicblock_pre        *rsql.Basicblock
		basicblock_cond_start *rsql.Basicblock
		basicblock_while_end  *rsql.Basicblock
		basicblock_post       *rsql.Basicblock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_WHILE)

	decor.Info_line = tok.Batch_line() // update line/pos for error message

	//=== basicblock pre ===

	basicblock_pre = decor.Basicblock_current // save current basicblock

	//=== walk condition ===

	basicblock_cond_start = decor.create_and_register_new_Basicblock_current() // start a new basicblock for WHILE condition and body

	if rsql_err = decor.Wacf_expression(tok.While_cond); rsql_err != nil {
		return rsql_err
	}

	if tok.While_cond.Prim_dataslot.Datatype() != rsql.DATATYPE_BOOLEAN {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BOOLEAN_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	//=== walk WHILE body ( in same basicblock as condition ) ===

	if rsql_err = decor.wacf_sequence_of_Token(tok.While_body); rsql_err != nil {
		return rsql_err
	}

	basicblock_while_end = decor.Basicblock_current

	//=== basicblock post ===

	basicblock_post = decor.create_and_register_new_Basicblock_current() // start a new basicblock

	//=== save basicblocks into WHILE stmt ===

	tok.While_basicblock_pre = basicblock_pre
	tok.While_basicblock_cond_start = basicblock_cond_start
	tok.While_basicblock_while_end = basicblock_while_end
	tok.While_basicblock_post = basicblock_post

	return nil
}

func (decor *Decorator) wacf_Token_stmt_RETURN(tok *ast.Token_stmt_RETURN) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error

		tok_promotion_token *ast.Token_primary
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_RETURN)
	rsql.Assert(tok.Ret_expression != nil)

	decor.Info_line = tok.Batch_line() // update line/pos for error message

	//=== walk return expression ===

	if tok_promotion_token, rsql_err = decor.Wacf_expression_with_promotion(tok.Ret_expression, rsql.DATATYPE_INT); rsql_err != nil {
		return rsql_err
	}

	tok.Ret_expression = tok_promotion_token

	return nil
}

func (decor *Decorator) walk_Token_stmt_GOTO(tok *ast.Token_stmt_GOTO) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_GOTO)

	decor.Info_line = tok.Batch_line() // update line/pos for error message

	//=== check that label exists ===

	if rsql_err = decor.LABELS.Check_exists(tok.Gt_label); rsql_err != nil { // error if label doesn't exist. LABELS dictionary has been filled during parsing stage.
		return rsql_err
	}

	return nil
}

func (decor *Decorator) walk_Token_stmt_LABEL(tok *ast.Token_stmt_LABEL) {
	var (
		basicblock_pre   *rsql.Basicblock
		basicblock_label *rsql.Basicblock
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_LABEL)

	decor.Info_line = tok.Batch_line() // update line/pos for error message

	//=== basicblock pre ===

	basicblock_pre = decor.Basicblock_current // save current basicblock

	//=== start of label block ===

	basicblock_label = decor.create_and_register_new_Basicblock_current() // start a new basicblock

	//=== save basicblocks into label stmt ===

	tok.Lab_basicblock_pre = basicblock_pre
	tok.Lab_basicblock_label = basicblock_label
}

func (decor *Decorator) wacf_sequence_of_Token(tok ast.Tokener) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	for tok != nil {
		decor.Info_line = tok.Batch_line() // update line/pos for error message

		switch tok.Category() {
		case ast.TOK_STMT_DECLARE_VARIABLE:
			rsql_err = decor.walk_Token_stmt_DECLARE_variable(tok.(*ast.Token_stmt_DECLARE_variable)) // just insert a new Symbol in VARIABLES symbol table, with a new Dataslot attached to it.

		case ast.TOK_STMT_ASSIGNMENT:
			rsql_err = decor.wacf_Token_stmt_ASSIGNMENT(tok.(*ast.Token_stmt_ASSIGNMENT))

		case ast.TOK_STMT_THROW:
			rsql_err = decor.wacf_Token_stmt_THROW(tok.(*ast.Token_stmt_THROW))

		case ast.TOK_STMT_SHOW_INFO:
			rsql_err = decor.wacf_Token_stmt_SHOW_INFO(tok.(*ast.Token_stmt_SHOW_INFO))

		case ast.TOK_STMT_PRINT:
			rsql_err = decor.wacf_Token_stmt_PRINT(tok.(*ast.Token_stmt_PRINT))

		case ast.TOK_STMT_ASSERT_:
			rsql_err = decor.wacf_Token_stmt_ASSERT_(tok.(*ast.Token_stmt_ASSERT_))

		case ast.TOK_STMT_ASSERT_NULL_:
			rsql_err = decor.wacf_Token_stmt_ASSERT_NULL_(tok.(*ast.Token_stmt_ASSERT_NULL_))

		case ast.TOK_STMT_ASSERT_ERROR_:
			rsql_err = decor.wacf_Token_stmt_ASSERT_ERROR_(tok.(*ast.Token_stmt_ASSERT_ERROR_))

		case ast.TOK_STMT_USE:
			rsql_err = decor.wacf_Token_stmt_USE(tok.(*ast.Token_stmt_USE))

		case ast.TOK_STMT_IF:
			rsql_err = decor.wacf_Token_stmt_IF(tok.(*ast.Token_stmt_IF))

		case ast.TOK_STMT_WHILE:
			rsql_err = decor.wacf_Token_stmt_WHILE(tok.(*ast.Token_stmt_WHILE))

		case ast.TOK_STMT_BREAK,
			ast.TOK_STMT_CONTINUE:
			// pass

		case ast.TOK_STMT_RETURN:
			rsql_err = decor.wacf_Token_stmt_RETURN(tok.(*ast.Token_stmt_RETURN))

		case ast.TOK_STMT_GOTO:
			rsql_err = decor.walk_Token_stmt_GOTO(tok.(*ast.Token_stmt_GOTO))

		case ast.TOK_STMT_LABEL:
			decor.walk_Token_stmt_LABEL(tok.(*ast.Token_stmt_LABEL))

		case ast.TOK_STMT_INSERT_INTO:
			rsql_err = decor.wacf_Token_stmt_INSERT_INTO(tok.(*ast.Token_stmt_INSERT_INTO))

		case ast.TOK_STMT_BULK_INSERT:
			rsql_err = decor.wacf_Token_stmt_BULK_INSERT(tok.(*ast.Token_stmt_BULK_INSERT))

		case ast.TOK_STMT_BULK_EXPORT:
			rsql_err = decor.wacf_Token_stmt_BULK_EXPORT(tok.(*ast.Token_stmt_BULK_EXPORT))

		case ast.TOK_STMT_SELECT_OR_UNION:
			rsql_err = decor.wacf_Token_stmt_SELECT_or_UNION(tok.(*ast.Token_stmt_SELECT_or_UNION))

		case ast.TOK_STMT_DELETE:
			rsql_err = decor.wacf_Token_stmt_DELETE(tok.(*ast.Token_stmt_DELETE))

		case ast.TOK_STMT_UPDATE:
			rsql_err = decor.wacf_Token_stmt_UPDATE(tok.(*ast.Token_stmt_UPDATE))

		case ast.TOK_STMT_TRUNCATE_TABLE:
			rsql_err = decor.wacf_Token_stmt_TRUNCATE_TABLE(tok.(*ast.Token_stmt_TRUNCATE_TABLE))

		case ast.TOK_STMT_SHRINK_TABLE:
			rsql_err = decor.wacf_Token_stmt_SHRINK_TABLE(tok.(*ast.Token_stmt_SHRINK_TABLE))

		case ast.TOK_STMT_DEBUG_TABLE_LOCK, // parser pragma       : enlist table for locking
			ast.TOK_STMT_SET_NOCOUNT,                // no argument to walk
			ast.TOK_STMT_SET_LEXER_OR_PARSER_OPTION, // lexer or parser pragma no-op : SET PARSEONLY ON or OFF, SET QUOTED_IDENTIFIER ON or OFF, etc
			ast.TOK_STMT_ALTER_SERVER_PARAMETER,

			ast.TOK_STMT_CREATE_LOGIN,        // no argument to walk
			ast.TOK_STMT_ALTER_LOGIN,         // no argument to walk
			ast.TOK_STMT_DROP_LOGIN,          // no argument to walk
			ast.TOK_STMT_CREATE_DATABASE,     // no argument to walk
			ast.TOK_STMT_ALTER_DATABASE,      // no argument to walk
			ast.TOK_STMT_DROP_DATABASE,       // no argument to walk
			ast.TOK_STMT_CREATE_USER,         // no argument to walk
			ast.TOK_STMT_ALTER_USER,          // no argument to walk
			ast.TOK_STMT_DROP_USER,           // no argument to walk
			ast.TOK_STMT_CREATE_ROLE,         // no argument to walk
			ast.TOK_STMT_ALTER_ROLE,          // no argument to walk
			ast.TOK_STMT_DROP_ROLE,           // no argument to walk
			ast.TOK_STMT_GRANT,               // no argument to walk
			ast.TOK_STMT_DENY,                // no argument to walk
			ast.TOK_STMT_REVOKE,              // no argument to walk
			ast.TOK_STMT_ALTER_AUTHORIZATION, // no argument to walk
			ast.TOK_STMT_SHOW_COLLATIONS,     // no argument to walk
			ast.TOK_STMT_SHOW_LANGUAGES,      // no argument to walk
			ast.TOK_STMT_SHOW_LOCKS,          // no argument to walk
			ast.TOK_STMT_SHOW_WORKERS,        // no argument to walk
			ast.TOK_STMT_SHOW,                // no argument to walk
			ast.TOK_STMT_SLEEP,               // no argument to walk
			ast.TOK_STMT_SHUTDOWN,            // no argument to walk
			ast.TOK_STMT_BACKUP,              // no argument to walk
			ast.TOK_STMT_RESTORE,             // no argument to walk
			ast.TOK_STMT_DUMP,                // no argument to walk

			ast.TOK_STMT_CREATE_TABLE, // no argument to walk
			ast.TOK_STMT_ALTER_TABLE,  // no argument to walk
			ast.TOK_STMT_DROP_TABLE,   // no argument to walk
			ast.TOK_STMT_CREATE_INDEX, // no argument to walk
			ast.TOK_STMT_ALTER_INDEX,  // no argument to walk
			ast.TOK_STMT_DROP_INDEX,   // no argument to walk

			ast.TOK_STMT_BEGIN_TRANSACTION,    // no argument to walk
			ast.TOK_STMT_COMMIT_TRANSACTION,   // no argument to walk
			ast.TOK_STMT_ROLLBACK_TRANSACTION: // no argument to walk
			// pass

			//

		default:
			panic("ast walk unknown tok_subcategory")
		}

		if rsql_err != nil { // syntax error
			return rsql_err
		}

		tok = tok.Next()
	}

	return nil
}

// Wacf_all_AST decorates the entire ast tree.
//
// walk-constfold all tokens, starting from tok.
//
func (decor *Decorator) Wacf_all_AST(tok ast.Tokener) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	bb := decor.create_and_register_new_Basicblock_current()

	decor.Basicblock_first = bb

	if rsql_err = decor.wacf_sequence_of_Token(tok); rsql_err != nil { // walk-constfold all tokens, starting from tok
		if rsql_err.Batch_line_no == 0 { // e.g. the error from opz.Optimize_all()
			rsql_err.Batch_line_no = decor.Info_line.No
			rsql_err.Batch_line_pos = uint32(decor.Info_line.Pos)
		}
		return rsql_err
	}

	return nil
}
