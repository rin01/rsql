package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
	"rsql/lex"
)

// walk_Token_stmt_DECLARE_variable creates a new symbol for a declared variable, and a new dataslot, initialized to NULL value.
//
// The new dataslot is entered in VARIABLES symbol table.
// If the variable already exists in symbol table, an error is returned.
//
// A DECLARE statement is not an instruction for the VM, it is only processed here by the decorator.
//
func (decor *Decorator) walk_Token_stmt_DECLARE_variable(tok *ast.Token_stmt_DECLARE_variable) *rsql.Error {
	var (
		rsql_err     *rsql.Error
		tok_variable *ast.Token_primary
		dataslot     rsql.IDataslot
	)

	rsql.Assert(tok != nil)
	rsql.Assert(tok.Tok_category == ast.TOK_STMT_DECLARE_VARIABLE)

	// create new Dataslot structures, and initialize them to NULL value

	tok_variable = tok.Decl_variable

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_VAR_LEAF, tok_variable.Prim_sql_datatype, tok_variable.Prim_sql_precision, tok_variable.Prim_sql_scale, tok_variable.Prim_sql_fixlen_flag)

	tok_variable.Prim_dataslot = dataslot

	// check if a variable @table with same name already exists

	if decor.Temptable_bag.Exists(tok_variable.Prim_name) == true {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_VARTABLE_WITH_SAME_NAME_EXISTS, rsql.ERROR_BATCH_ABORT, tok_variable.Prim_name)
	}

	// put new entry in VARIABLES symbol table

	if rsql_err = decor.VARIABLES.Put(tok_variable.Prim_name, dataslot); rsql_err != nil { // error if variable name already exists
		return rsql_err
	}

	// set collation if VARCHAR

	if tok_variable.Prim_sql_datatype == rsql.DATATYPE_VARCHAR { // a VARCHAR variable has always the default collation
		tok_variable.Prim_collation_strength = rsql.COLLSTRENGTH_0
		tok_variable.Prim_collation = decor.Server_default_collation
	}

	return nil
}

// wacf_Token_stmt_ASSIGNMENT processes the assignment of an expression to a variable.
//
// The left variable has a dataslot attached to it, already created in the VARIABLES symbol table by ast_walk_Token_stmt_DECLARE_variable().
//
func (decor *Decorator) wacf_Token_stmt_ASSIGNMENT(tok *ast.Token_stmt_ASSIGNMENT) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err            *rsql.Error
		tok_promotion_token *ast.Token_primary
		dataslot            rsql.IDataslot
		datatype_to         rsql.Datatype_t
		instruction_code    rsql.Instrcode_t
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_ASSIGNMENT)

	// get Dataslot for the variable

	rsql.Assert(tok.Set_lvalue.Prim_type == ast.TOK_PRIM_VARIABLE)

	if dataslot, rsql_err = decor.VARIABLES.Get_symbol_entry(tok.Set_lvalue.Prim_name); rsql_err != nil { // returns error if symbol not exists
		return rsql_err
	}

	tok.Set_lvalue.Prim_dataslot = dataslot

	// walk expression to assign, and put promotion token on right child if needed

	if tok_promotion_token, rsql_err = decor.Wacf_expression_with_promotion_exact_for_variable_assignment(tok.Set_rvalue, tok.Set_lvalue.Prim_dataslot); rsql_err != nil { // collation is set to server default collation
		return rsql_err
	}

	tok.Set_rvalue = tok_promotion_token

	// determine instruction_code

	datatype_to = tok.Set_lvalue.Prim_dataslot.Datatype()

	switch datatype_to {
	case rsql.DATATYPE_SYSLANGUAGE:
		instruction_code = rsql.INSTR_ASSIGN_SYSLANGUAGE

	case rsql.DATATYPE_VARBINARY:
		instruction_code = rsql.INSTR_ASSIGN_VARBINARY

	case rsql.DATATYPE_VARCHAR:
		instruction_code = rsql.INSTR_ASSIGN_VARCHAR

	case rsql.DATATYPE_BIT:
		instruction_code = rsql.INSTR_ASSIGN_BIT

	case rsql.DATATYPE_TINYINT:
		instruction_code = rsql.INSTR_ASSIGN_TINYINT

	case rsql.DATATYPE_SMALLINT:
		instruction_code = rsql.INSTR_ASSIGN_SMALLINT

	case rsql.DATATYPE_INT:
		instruction_code = rsql.INSTR_ASSIGN_INT

	case rsql.DATATYPE_BIGINT:
		instruction_code = rsql.INSTR_ASSIGN_BIGINT

	case rsql.DATATYPE_MONEY:
		instruction_code = rsql.INSTR_ASSIGN_MONEY

	case rsql.DATATYPE_NUMERIC:
		instruction_code = rsql.INSTR_ASSIGN_NUMERIC

	case rsql.DATATYPE_FLOAT:
		instruction_code = rsql.INSTR_ASSIGN_FLOAT

	case rsql.DATATYPE_DATE:
		instruction_code = rsql.INSTR_ASSIGN_DATE

	case rsql.DATATYPE_TIME:
		instruction_code = rsql.INSTR_ASSIGN_TIME

	case rsql.DATATYPE_DATETIME:
		instruction_code = rsql.INSTR_ASSIGN_DATETIME

	default:
		panic("unknown assignment datatype")
	}

	tok.Set_instruction_code = instruction_code

	return nil
}

// Token_primary_VARIABLE_CAST_COPY_new_with_Dataslot was used to insert a copy of @b in   SET @a = @b
//
// Works well, but NOT USED ANY MORE.
//
func (decor *Decorator) Token_primary_VARIABLE_CAST_COPY_new_with_Dataslot_NOT_USED_ANY_MORE(tok_from *ast.Token_primary) *ast.Token_primary {
	var (
		tok_cast                *ast.Token_primary
		dataslot                rsql.IDataslot
		instruction_code        rsql.Instrcode_t
		prim_subtype            ast.Prim_subtype_t
		datatype_to             rsql.Datatype_t
		datatype_to_precision   uint16
		datatype_to_scale       uint16
		datatype_to_fixlen_flag bool
	)

	rsql.Assert(tok_from.Prim_type == ast.TOK_PRIM_VARIABLE)

	// extract information for the CAST

	prim_subtype, instruction_code, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag = cast_analysis(tok_from.Prim_dataslot, tok_from.Prim_dataslot) // from and target are exactly same datatype and attributes

	rsql.Assert(prim_subtype == ast.TOK_PRIM_CAST_COPY)

	// create a new CAST Token_primary

	tok_cast = decor.New_token_primary(ast.TOK_PRIM_CAST, prim_subtype, lex.LEXEME_KEYWORD_CAST) // create new CAST Token_primary
	tok_cast.Tok_batch_line = tok_from.Tok_batch_line                                            // line/pos
	tok_cast.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_cast.Prim_instruction_code = instruction_code

	// create a dataslot of the proper datatype and attach it to the CAST token

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag)
	tok_cast.Prim_dataslot = dataslot // attach Dataslot to CAST token.
	tok_cast.Prim_left = tok_from     // argument of CAST token is the operand that need to be cast.

	if datatype_to == rsql.DATATYPE_VARCHAR {
		rsql.Assert(tok_from.Prim_collation_strength == rsql.COLLSTRENGTH_0)
		rsql.Assert(tok_from.Prim_collation == decor.Server_default_collation)
		tok_cast.Prim_collation_strength = rsql.COLLSTRENGTH_0 // IMPORTANT: force result to be default collation (like collation of all VARCHAR variables)
		tok_cast.Prim_collation = decor.Server_default_collation
	}

	return tok_cast
}

// Token_primary_CAST_COPY_new_with_Dataslot inserts a copy token.
// It is used when decorating SELECT statements.
//
// Same as Token_primary_VARIABLE_CAST_COPY_new_with_Dataslot, but collation is not changed.
//
// NOT USED ANY MORE
//
func (decor *Decorator) Token_primary_CAST_COPY_new_with_Dataslot_NOT_USED_ANY_MORE(tok_from *ast.Token_primary) *ast.Token_primary {
	var (
		tok_cast                *ast.Token_primary
		dataslot                rsql.IDataslot
		instruction_code        rsql.Instrcode_t
		prim_subtype            ast.Prim_subtype_t
		datatype_to             rsql.Datatype_t
		datatype_to_precision   uint16
		datatype_to_scale       uint16
		datatype_to_fixlen_flag bool
	)

	rsql.Assert(tok_from.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF) != 0)

	// extract information for the CAST

	prim_subtype, instruction_code, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag = cast_analysis(tok_from.Prim_dataslot, tok_from.Prim_dataslot) // from and target are exactly same datatype and attributes

	rsql.Assert(prim_subtype == ast.TOK_PRIM_CAST_COPY)

	// create a new CAST Token_primary

	tok_cast = decor.New_token_primary(ast.TOK_PRIM_CAST, prim_subtype, lex.LEXEME_KEYWORD_CAST) // create new CAST Token_primary
	tok_cast.Tok_batch_line = tok_from.Tok_batch_line                                            // line/pos
	tok_cast.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_cast.Prim_instruction_code = instruction_code

	// create a dataslot of the proper datatype and attach it to the CAST token

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag)
	tok_cast.Prim_dataslot = dataslot // attach Dataslot to CAST token.
	tok_cast.Prim_left = tok_from     // argument of CAST token is the operand that need to be cast.

	if datatype_to == rsql.DATATYPE_VARCHAR {
		tok_cast.Prim_collation_strength = tok_from.Prim_collation_strength
		tok_cast.Prim_collation = tok_from.Prim_collation
	}

	return tok_cast
}
