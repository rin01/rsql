package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
	"rsql/lex"
)

func (decor *Decorator) wacf_Token_stmt_UPDATE(tok *ast.Token_stmt_UPDATE) *rsql.Error { // wacf means walk-constfold
	var (
		target_table          *ast.Xtable_table
		gtabledef             *rsql.GTabledef
		base_tabledef         *rsql.Tabledef
		insertion_base_tokens []*ast.Token_primary
		SET_column_collection map[*rsql.Coldef]struct{} // collection of columns having SET clause
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_UPDATE)

	SET_column_collection = make(map[*rsql.Coldef]struct{})

	// walk FROM clause

	xtable_from := tok.Ud_from

	if rsql_err := decor.wacf_Xtable_from(xtable_from); rsql_err != nil {
		return rsql_err
	}

	// create Dataslots for all columns of target table

	target_table = tok.Ud_target_table
	gtabledef = target_table.Tbl_cursor.Gtabledef
	base_tabledef = gtabledef.Base

	for i, coldef := range base_tabledef.Td_coldefs { // for all columns
		dataslot := target_table.Tbl_cursor.Base_fields[i]

		if dataslot == nil { // can be != nil if column has been referenced e.g. in a ON or WHERE clause, during walking of FROM clause, just hereabove
			dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
			target_table.Tbl_cursor.Base_fields[i] = dataslot
		}

		rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)
	}

	// walk SET expressions

	decor.Xtable_object_stack.Push(xtable_from)
	defer func() { decor.Xtable_object_stack.Pop() }()

	insertion_base_tokens = make([]*ast.Token_primary, len(base_tabledef.Td_coldefs)) // create insertion base row

	for i, set_clause := range tok.Ud_set_list { // for all SET expressions (which always include ROWID column)
		var (
			rsql_err            *rsql.Error
			coldef              *rsql.Coldef
			ok                  bool
			tok_promotion_token *ast.Token_primary
		)

		if coldef, ok = gtabledef.Colmap[set_clause.St_colname]; ok == false {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_MASTER_COLUMN_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, set_clause.St_colname)
		}

		column_base_seqno := coldef.Cd_base_seqno
		sample_Data_XXX_to := target_table.Tbl_cursor.Base_fields[column_base_seqno]

		if tok_promotion_token, rsql_err = decor.Wacf_expression_FITTING_for_INSERT_column(set_clause.St_expr, sample_Data_XXX_to); rsql_err != nil { // set_clause.St_expr may be returned
			return rsql_err
		}

		insertion_base_tokens[column_base_seqno] = tok_promotion_token // for VARBINARY and VARCHAR, precision of tok_promotion_token may be smaller than precision specified in Coldef
		tok.Ud_set_list[i].St_expr = tok_promotion_token

		SET_column_collection[coldef] = struct{}{}
	}

	for i, token_col := range insertion_base_tokens { // for columns that have no SET expressions, just put pointers to target_table dataslots
		if token_col == nil {
			token_col = ast.New_TOK_PRIM_IDENTIFIER(base_tabledef.Td_coldefs[i].Cd_colname, lex.Coord_t{}) // no Coord_t for this token, it is useless
			token_col.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE

			token_col.Prim_dataslot = target_table.Tbl_cursor.Base_fields[i] // directly reference target_table dataslot

			insertion_base_tokens[i] = token_col
		}
	}

	tok.Ud_insertion_base_tokens = insertion_base_tokens // insertion_base_tokens is a shallow copy of target_table base row, except for columns that have a SET clause

	// create Basicblock for storing instructions of SET clauses

	tok.Ud_basicblock = decor.create_new_Basicblock()

	// create list of indexes to update

	tok.Ud_indexes_to_update = make(map[string]*rsql.Tabledef, len(gtabledef.Indexmap))

	for _, tabledef := range gtabledef.Indexmap {
		for _, coldef := range tabledef.Td_coldefs {
			if _, ok := SET_column_collection[coldef]; ok == true {
				tok.Ud_indexes_to_update[tabledef.Td_index_name] = tabledef
				break
			}
		}
	}

	// optimize FROM tables

	optimizer := New_optimizer()

	if rsql_err := optimizer.Optimize_all(tok.Ud_from); rsql_err != nil {
		return rsql_err
	}

	return nil
}
