package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
	"rsql/lex"
)

// cast_analysis extracts CAST information by analyzing source and target model dataslots, returning CAST instruction, subtype, and properties of the target.
//
// Source and target model dataslots can be any datatype.
// They are just "models", as it is not important whether they will be the operands used by the CAST operation or not.
//
// This function is used by Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot() and walk_Token_primary_EXPLICIT_CAST().
//
//      dataslot_from:      source model dataslot, *data.INT, *data.VARCHAR, etc.
//      dataslot_to:        target model dataslot, *data.INT, *data.VARCHAR, etc.
//
//
//      Returned values are:
//
//          prim_subtype is the target token subtype :
//               - TOK_PRIM_CAST_ILLEGAL  : illegal cast.
//                         instruction_code is INSTR_CAST_ILLEGAL
//
//               - TOK_PRIM_CAST_ENLARGE  : target datatype rank is higher than source datatype rank
//                         instruction_code is e.g. INSTR_CAST_INT_FLOAT, INSTR_CAST_VOID_DATETIME, INSTR_CAST_MONEY_NUMERIC, etc
//
//               - TOK_PRIM_CAST_COPY     : target and source datatypes are exactly the same. Operation is just a copy of the value.
//                         instruction_code is e.g. INSTR_CAST_INT_INT, INSTR_CAST_MONEY_MONEY, INSTR_CAST_DATETIME_DATETIME, etc
//                                               or INSTR_CAST_VARBINARY_VARBINARY, INSTR_CAST_VARCHAR_VARCHAR, INSTR_CAST_NUMERIC_NUMERIC with identical precision, scale, and fixlen_flag.
//
//               - TOK_PRIM_CAST_ADJUST   : target and source datatypes are exactly the same. Precision, scale, or fixlen_flag of source and target are different.
//                         instruction_code is INSTR_CAST_VARBINARY_VARBINARY, INSTR_CAST_VARCHAR_VARCHAR, INSTR_CAST_NUMERIC_NUMERIC
//
//               - TOK_PRIM_CAST_RESTRICT : target datatype rank is lower than source datatype rank
//                         instruction_code is e.g. INSTR_CAST_FLOAT_INT, INSTR_CAST_DATETIME_DATE, INSTR_CAST_NUMERIC_MONEY, etc
//
//          Datatype, precision, scale, fixlen_flag of target dataslot 'dataslot_to' are always returned, except for TOK_PRIM_CAST_ILLEGAL.
//
// The caller of this function wants to create a new CAST Token_primary with the same datatype, precision, scale, and fixlen_flag as 'dataslot_to'.
//
func cast_analysis(dataslot_from, dataslot_to rsql.IDataslot) (prim_subtype ast.Prim_subtype_t, instruction_code rsql.Instrcode_t, datatype rsql.Datatype_t, precision uint16, scale uint16, fixlen_flag bool) {
	var (
		datatype_from             rsql.Datatype_t
		datatype_from_precision   uint16
		datatype_from_scale       uint16
		datatype_from_fixlen_flag bool

		datatype_to             rsql.Datatype_t
		datatype_to_precision   uint16
		datatype_to_scale       uint16
		datatype_to_fixlen_flag bool
	)

	//=== initialization ===

	datatype_from = dataslot_from.Datatype() // datatype of the operand to cast, which rank can be higher, equal or lower than target datatype.
	datatype_to = dataslot_to.Datatype()

	switch {
	case datatype_from > datatype_to:
		prim_subtype = ast.TOK_PRIM_CAST_RESTRICT
	case datatype_from < datatype_to:
		prim_subtype = ast.TOK_PRIM_CAST_ENLARGE
	default:
		prim_subtype = ast.TOK_PRIM_CAST_ADJUST
	}

	//=== get instruction_code and check if the cast is allowed ===

	instruction_code = G_SQL_DATATYPE_CAST_INSTRUCTION_CODE[datatype_from][datatype_to] // get the instruction code from a static table.

	if instruction_code == rsql.INSTR_CAST_ILLEGAL { // return INSTR_CAST_ILLEGAL if illegal CAST
		return ast.TOK_PRIM_CAST_ILLEGAL, rsql.INSTR_CAST_ILLEGAL, 0, 0, 0, false
	}

	if Is_CAST_COPY(instruction_code) { // if source and target datatypes are the same ( e.g. INT, MONEY, FLOAT, DATETIME, etc ), just return.
		rsql.Assert(datatype_from == datatype_to) // the condition is always false for VARBINARY, VARCHAR and NUMERIC cases, which are processed later below.
		return ast.TOK_PRIM_CAST_COPY, instruction_code, datatype_to, 0, 0, false
	}

	// here, source and target datatypes are different, or are the same for VARBINARY, VARCHAR and NUMERIC which may need to be adjusted for p, s, fixlen_flag

	//=== if datatype_to is VARBINARY, VARCHAR, or NUMERIC, determine p, s, and fixlen_flag of the target by reading this information from 'dataslot_to' ===

	switch datatype_to {
	case rsql.DATATYPE_VARBINARY:
		datatype_to_precision = dataslot_to.(*data.VARBINARY).Data_precision

	case rsql.DATATYPE_VARCHAR:
		datatype_to_precision = dataslot_to.(*data.VARCHAR).Data_precision
		datatype_to_fixlen_flag = dataslot_to.(*data.VARCHAR).Data_fixlen_flag

	case rsql.DATATYPE_REGEXPLIKE:
		panic("assign or cast to REGEXPLIKE unexpected and not allowed")

	case rsql.DATATYPE_NUMERIC:
		datatype_to_precision = dataslot_to.(*data.NUMERIC).Data_precision
		datatype_to_scale = dataslot_to.(*data.NUMERIC).Data_scale
	}

	//=== if source and target are same datatype, one of VARBINARY, VARCHAR or NUMERIC, check if precision, scale, and fixlen_flag are the same ===

	if datatype_from == datatype_to {
		switch datatype_from {
		case rsql.DATATYPE_VARBINARY:
			datatype_from_precision = dataslot_from.(*data.VARBINARY).Data_precision

		case rsql.DATATYPE_VARCHAR:
			datatype_from_precision = dataslot_from.(*data.VARCHAR).Data_precision
			datatype_from_fixlen_flag = dataslot_from.(*data.VARCHAR).Data_fixlen_flag

		case rsql.DATATYPE_NUMERIC:
			datatype_from_precision = dataslot_from.(*data.NUMERIC).Data_precision
			datatype_from_scale = dataslot_from.(*data.NUMERIC).Data_scale

		default:
			panic("impossible") // INT, FLOAT, DATETIME etc have already been processed and we returned earlier.
		}

		// if source and target datatype and p, s, fixlen_flag are the same, return INSTR_CAST_COPY.

		if datatype_to_precision == datatype_from_precision &&
			datatype_to_scale == datatype_from_scale &&
			datatype_to_fixlen_flag == datatype_from_fixlen_flag {
			return ast.TOK_PRIM_CAST_COPY, instruction_code, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag // source and target datatypes are EXACTLY the same ===> exit
		}
	}

	// here, source and target are different.
	//  - It can be because datatypes are different.
	//  - It can be because datatypes are both VARBINARY, but precision is different.
	//  - It can be because datatypes are both VARCHAR,   but precision, or fixlen_flag is different.
	//  - It can be because datatypes are both NUMERIC,   but <p,s> is different.
	//
	// A CAST node is thus needed, and will be created by the caller of this function.

	// prim_subtype is TOK_PRIM_CAST_ENLARGE, TOK_PRIM_CAST_ADJUST, TOK_PRIM_CAST_RESTRICT

	return prim_subtype, instruction_code, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag
}

// walk_Token_primary_EXPLICIT_CAST_and_simplify walks EXPLICIT CAST and creates dataslot.
//
//      EXPLICIT CAST is a CAST(expression AS datatype) or CONVERT(datatype, expression, [style]) sql function.
//             After parsing, both CAST and CONVERT functions are TOK_PRIM_CAST subtype in the AST tree.
//             During decoration, this function will transform some INSTR_CAST to ordinary INSTR_SYSFUNC_CONVERT function, e.g. INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME for cast('20110101' as datetime).
//
// If a style parameter is passed which is not used, e.g. convert(int, 10, 123), this function discards it by setting the pointer tok.Prim_right to nil.
//
//     note : Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot() and Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot() never create a INSTR_CAST_COPY instruction.
//               Only walk_Token_primary_EXPLICIT_CAST_and_simplify() can produce a INSTR_CAST_COPY instruction, but it is simplified out and is not returned to the caller.
//
func (decor *Decorator) walk_Token_primary_EXPLICIT_CAST_and_simplify(tok *ast.Token_primary) *rsql.Error {
	var (
		rsql_err                *rsql.Error
		prim_subtype            ast.Prim_subtype_t
		datatype_from           rsql.Datatype_t
		datatype_to             rsql.Datatype_t
		datatype_to_precision   uint16
		datatype_to_scale       uint16
		datatype_to_fixlen_flag bool
		instruction_code        rsql.Instrcode_t
		dataslot                rsql.IDataslot
	)

	rsql.Assert(tok.Prim_left != nil)

	//=== walk the child, and the optional style parameter of the EXPLICIT CAST if any (in optional style exists, it has been written as CONVERT() in the SQL script) ===

	if rsql_err = decor.walk_Token_primary_and_simplify(tok.Prim_left); rsql_err != nil {
		return rsql_err
	}

	if tok.Prim_right != nil { // for style parameter of CONVERT if any
		if rsql_err = decor.walk_Token_primary_and_simplify(tok.Prim_right); rsql_err != nil {
			return rsql_err
		}
	}

	//=== datatype, precision, scale, and fixlen_flag of the target are those explicitly specified by the user in the SQL script ===

	datatype_from = tok.Prim_left.Prim_dataslot.Datatype()

	datatype_to = tok.Prim_sql_datatype
	datatype_to_precision = tok.Prim_sql_precision
	datatype_to_scale = tok.Prim_sql_scale
	datatype_to_fixlen_flag = tok.Prim_sql_fixlen_flag

	if datatype_to == rsql.DATATYPE_VARCHAR { // if target is VARCHAR, determine collation
		switch {
		case datatype_from == rsql.DATATYPE_VARCHAR: // if source is VARCHAR
			tok.Prim_collation_strength = tok.Prim_left.Prim_collation_strength // then the target inherits collation from source
			tok.Prim_collation = tok.Prim_left.Prim_collation

		default: // else, the target collation is the default collation.
			tok.Prim_collation_strength = rsql.COLLSTRENGTH_0
			tok.Prim_collation = decor.Server_default_collation
		}

	}

	//=== create Dataslot ===

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag)
	tok.Prim_dataslot = dataslot

	//=== get instruction code for the EXPLICIT CAST ===

	prim_subtype, instruction_code, _, _, _, _ = cast_analysis(tok.Prim_left.Prim_dataslot, tok.Prim_dataslot) // only instruction_code and prim_subtype are needed

	if prim_subtype == ast.TOK_PRIM_CAST_ILLEGAL { // if illegal CAST, return error
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, datatype_from, datatype_to)
	}

	tok.Prim_subtype = prim_subtype // TOK_PRIM_CAST_RESTRICT / _ENLARGE / _ADJUST / _COPY
	tok.Prim_instruction_code = instruction_code

	// for INSTR_CAST_VARCHAR_DATE/TIME/DATETIME, INSTR_CAST_DATE/TIME/DATETIME_VARCHAR, transform to ordinary 'CONVERT' function with the explicitly passed style parameter, of else with default style parameter, and syslanguage if needed

	if rsql_err = decor.transform_DATETIME_CAST_to_SYSFUNC_CONVERT(tok); rsql_err != nil { // useless style is discarded if any, e.g. convert(smallint, 10, 123), where style 123 is useless
		return rsql_err
	}

	//=== simplify CAST ===

	if tok.Prim_type == ast.TOK_PRIM_CAST {
		tok.Prim_right = nil // useless style is discarded if any, e.g. convert(int, 10, 123), with useless style '123'

		simplify_CAST(tok)
	}

	return nil
}

type rounding_mode_t uint16 // used by get_cast_rounding_mode()

const (
	S_CAST_MODE_NEUTRAL rounding_mode_t = 0 // must be 0
	S_CAST_MODE_ROUND   rounding_mode_t = 1
	S_CAST_MODE_TRUNC   rounding_mode_t = 2
)

// get_cast_rounding_mode returns rounding mode of CAST from an integer, money, numeric or float, to an integer, money, numeric or float.
//
// When datatype_from is integer datatypes, the rounding mode is S_CAST_MODE_NEUTRAL (=number is not modified).
//
// When MONEY, NUMERIC or FLOAT are involved, the fractional part may be modified.
// Rounding mode will be S_CAST_MODE_ROUND, S_CAST_MODE_TRUNC, or S_CAST_MODE_NEUTRAL, as follows :
//
//      cast(999 as int)           = 999        -- neutral
//      cast(999 as money)         = 999        -- neutral
//      cast(999 as numeric(4))    = 999        -- neutral
//      cast(999 as float)         = 999.       -- neutral
//
//      cast($999.9 as int)        = 1000       -- round
//      cast($999.9 as money)      = $999.9     -- neutral
//      cast($999.9 as numeric(4)) = 1000       -- round
//      cast($999.75 as float)     = 999.75e0   -- neutral
//
//      cast(999.9d as int)        =   999      -- trunc
//      cast(999.99999d as money)  = $1000      -- round
//      cast(999.9d as numeric(4)) =  1000      -- round
//      cast(999.75d as float)     = 999.75e0   -- neutral
//
//      cast(999.9f as int)        =   999      -- trunc
//      cast(999.99999f as money)  = $1000      -- round
//      cast(999.9f as numeric(4)) =  1000      -- round
//      cast(999.75f as float)     = 999.75e0   -- neutral
//
// datatype_from and datatype_to must be a number type ( DATATYPE_TINYINT ... DATATYPE_FLOAT ).
//
func get_cast_rounding_mode(datatype_from rsql.Datatype_t, datatype_to rsql.Datatype_t) rounding_mode_t {
	var rounding_mode rounding_mode_t

	rsql.Assert(datatype_from >= rsql.DATATYPE_TINYINT && datatype_from <= rsql.DATATYPE_FLOAT)
	rsql.Assert(datatype_to >= rsql.DATATYPE_TINYINT && datatype_to <= rsql.DATATYPE_FLOAT)

	rounding_mode = S_CAST_MODE_NEUTRAL // default rounding mode, when datatype_from is integer datatype

	switch datatype_from {
	case rsql.DATATYPE_MONEY: // === from MONEY ===
		rounding_mode = S_CAST_MODE_ROUND

		switch datatype_to {
		case rsql.DATATYPE_MONEY:
			rounding_mode = S_CAST_MODE_NEUTRAL
		case rsql.DATATYPE_NUMERIC:
			rounding_mode = S_CAST_MODE_ROUND
		case rsql.DATATYPE_FLOAT:
			rounding_mode = S_CAST_MODE_NEUTRAL
		}

	case rsql.DATATYPE_NUMERIC: // === from NUMERIC ===
		rounding_mode = S_CAST_MODE_TRUNC

		switch datatype_to {
		case rsql.DATATYPE_MONEY:
			rounding_mode = S_CAST_MODE_ROUND
		case rsql.DATATYPE_NUMERIC:
			rounding_mode = S_CAST_MODE_ROUND
		case rsql.DATATYPE_FLOAT:
			rounding_mode = S_CAST_MODE_NEUTRAL
		}

	case rsql.DATATYPE_FLOAT: // === from FLOAT ===
		rounding_mode = S_CAST_MODE_TRUNC

		switch datatype_to {
		case rsql.DATATYPE_MONEY:
			rounding_mode = S_CAST_MODE_ROUND
		case rsql.DATATYPE_NUMERIC:
			rounding_mode = S_CAST_MODE_ROUND
		case rsql.DATATYPE_FLOAT:
			rounding_mode = S_CAST_MODE_NEUTRAL
		}
	}

	return rounding_mode
}

// simplify_CAST simplifies the current token by collapsing child or grandchild into it, discarding original current token content.
//
//        If simplification is possible, it is one of the following :
//            - current token is TOK_PRIM_CAST_COPY and child token of any type:                                       child token is collapsed into current token.
//            - current token is TOK_PRIM_CAST      and child TOK_PRIM_CAST and grandchild token of any type:      child token is discarded
//                                                                                                                              - if resulting cast is TOK_PRIM_CAST_COPY, grandchild token is collapsed into current token,
//                                                                                                                              - else, grandchild becomes child of current token.
//
//        If child or grandchild token is collapsed into the current token position, the content of current token is clobbered.
//            - The pointer to dataslot is also copied.
//            - It is as if the child or grandchild token was moved into the current token position.
//
//        Notice that in all cases, the datatype and properties of the dataslot attached to the current token remain unchanged, even if the original dataslot has been substituted by child or grandchild's one.
//
//
//        declare @a int = 10
//
//        if cast(@a as int) between 5 and 15  -- example 1, TOK_PRIM_CAST_COPY is discarded.
//            print 'in'
//        else
//            print 'out'
//
//        print case cast(@a as int) when 1 then 'one' when 10 then 'ten' end  -- example 2, TOK_PRIM_CAST_COPY is discarded.
//
func simplify_CAST(tok *ast.Token_primary) {
	type group_type_t int
	const (
		GROUP_VARBINARY group_type_t = iota + 1
		GROUP_VARCHAR
		GROUP_NUMBER
		GROUP_DATE_TIME
	)

	var (
		sql_collate_bak        string
		collation_strength_bak rsql.Collstrength_t
		collation_bak          string

		tok_from         *ast.Token_primary
		datatype_from    rsql.Datatype_t
		integral_from    uint16
		precision_from   uint16
		scale_from       uint16
		fixlen_flag_from bool

		tok_middle           *ast.Token_primary
		rounding_mode_middle rounding_mode_t
		datatype_middle      rsql.Datatype_t
		integral_middle      uint16
		precision_middle     uint16
		scale_middle         uint16
		fixlen_flag_middle   bool

		rounding_mode_to rounding_mode_t
		datatype_to      rsql.Datatype_t
		integral_to      uint16
		precision_to     uint16
		scale_to         uint16
		fixlen_flag_to   bool

		group_from   group_type_t
		group_middle group_type_t
		group_to     group_type_t

		rounding_mode_simplified rounding_mode_t

		new_instruction_code rsql.Instrcode_t
		new_prim_subtype     ast.Prim_subtype_t
	)

	//=== current token must be TOK_PRIM_CAST. Else, just return ===

	if tok.Prim_type != ast.TOK_PRIM_CAST {
		return
	}

	tok_middle = tok.Prim_left

	//=== if current token is a CAST that changes nothing, collapse the child token content (which may be a CAST, a literal, function, variable, etc) into the current token address. ===

	if tok.Prim_subtype == ast.TOK_PRIM_CAST_COPY {
		// IMPORTANT : note that child may be a CAST or any other kind of token, like literal, function, variable, etc. E.g. cast(1234567890.12 as numeric(12,2)), cast(cos(10.e0) as float)
		//             note that we MUST also copy Prim_dataslot of the child, because if child is a variable, its dataslot is shared through the symbol table. And also if it is a literal constant, its dataslot contains a value.

		sql_collate_bak = tok.Prim_sql_collate
		collation_strength_bak = tok.Prim_collation_strength
		collation_bak = tok.Prim_collation
		*tok = *tok_middle
		tok.Prim_sql_collate = sql_collate_bak
		tok.Prim_collation_strength = collation_strength_bak
		tok.Prim_collation = collation_bak

		tok_middle.Prim_instruction_code = rsql.INSTR_CAST_ILLEGAL // put garbage in discarded tok_middle
		tok_middle.Prim_dataslot = nil

		return // === and return.
	}

	//=== current token is a CAST, and simplification needs its child to also be a CAST ===

	if tok_middle.Prim_type != ast.TOK_PRIM_CAST { // if child is not a CAST, just return.
		return
	}

	tok_from = tok_middle.Prim_left

	//=== collect some information about datatype_from ===

	datatype_from = tok_from.Prim_dataslot.Datatype() // may be not a CAST, but a token of any type

	switch datatype_from {
	case rsql.DATATYPE_VARBINARY:
		precision_from = tok_from.Prim_dataslot.(*data.VARBINARY).Data_precision
		group_from = GROUP_VARBINARY

	case rsql.DATATYPE_VARCHAR:
		precision_from = tok_from.Prim_dataslot.(*data.VARCHAR).Data_precision
		fixlen_flag_from = tok_from.Prim_dataslot.(*data.VARCHAR).Data_fixlen_flag
		group_from = GROUP_VARCHAR

	case rsql.DATATYPE_TINYINT:
		precision_from = rsql.DATATYPE_PRECISION_TINYINT
		group_from = GROUP_NUMBER

	case rsql.DATATYPE_SMALLINT:
		precision_from = rsql.DATATYPE_PRECISION_SMALLINT
		group_from = GROUP_NUMBER

	case rsql.DATATYPE_INT:
		precision_from = rsql.DATATYPE_PRECISION_INT
		group_from = GROUP_NUMBER

	case rsql.DATATYPE_BIGINT:
		precision_from = rsql.DATATYPE_PRECISION_BIGINT
		group_from = GROUP_NUMBER

	case rsql.DATATYPE_MONEY:
		precision_from = rsql.DATATYPE_PRECISION_MONEY
		scale_from = rsql.DATATYPE_SCALE_MONEY
		group_from = GROUP_NUMBER

	case rsql.DATATYPE_NUMERIC:
		precision_from = tok_from.Prim_dataslot.(*data.NUMERIC).Data_precision
		scale_from = tok_from.Prim_dataslot.(*data.NUMERIC).Data_scale
		group_from = GROUP_NUMBER

	case rsql.DATATYPE_FLOAT:
		precision_from = 0xffff // pretends this, so that MONEY and NUMERIC accept to fit in FLOAT
		scale_from = 0x00ff     // same
		group_from = GROUP_NUMBER

	case rsql.DATATYPE_DATE,
		rsql.DATATYPE_TIME,
		rsql.DATATYPE_DATETIME:
		group_from = GROUP_DATE_TIME

	default:
		return // if datatype_from is not listed here, just return.
	}

	// collect some information about datatype_middle

	datatype_middle = tok_middle.Prim_dataslot.Datatype()

	switch datatype_middle {
	case rsql.DATATYPE_VARBINARY:
		precision_middle = tok_middle.Prim_dataslot.(*data.VARBINARY).Data_precision
		group_middle = GROUP_VARBINARY

	case rsql.DATATYPE_VARCHAR:
		precision_middle = tok_middle.Prim_dataslot.(*data.VARCHAR).Data_precision
		fixlen_flag_middle = tok_middle.Prim_dataslot.(*data.VARCHAR).Data_fixlen_flag
		group_middle = GROUP_VARCHAR

	case rsql.DATATYPE_TINYINT:
		precision_middle = rsql.DATATYPE_PRECISION_TINYINT
		group_middle = GROUP_NUMBER

	case rsql.DATATYPE_SMALLINT:
		precision_middle = rsql.DATATYPE_PRECISION_SMALLINT
		group_middle = GROUP_NUMBER

	case rsql.DATATYPE_INT:
		precision_middle = rsql.DATATYPE_PRECISION_INT
		group_middle = GROUP_NUMBER

	case rsql.DATATYPE_BIGINT:
		precision_middle = rsql.DATATYPE_PRECISION_BIGINT
		group_middle = GROUP_NUMBER

	case rsql.DATATYPE_MONEY:
		precision_middle = rsql.DATATYPE_PRECISION_MONEY
		scale_middle = rsql.DATATYPE_SCALE_MONEY
		group_middle = GROUP_NUMBER

	case rsql.DATATYPE_NUMERIC:
		precision_middle = tok_middle.Prim_dataslot.(*data.NUMERIC).Data_precision
		scale_middle = tok_middle.Prim_dataslot.(*data.NUMERIC).Data_scale
		group_middle = GROUP_NUMBER

	case rsql.DATATYPE_FLOAT:
		precision_middle = 0xffff // pretends this, so that MONEY and NUMERIC accept to fit in FLOAT
		scale_middle = 0x00ff     // same
		group_middle = GROUP_NUMBER

	case rsql.DATATYPE_DATE,
		rsql.DATATYPE_TIME,
		rsql.DATATYPE_DATETIME:
		group_middle = GROUP_DATE_TIME

	default:
		return // if datatype_middle is not listed here, just return.
	}

	// collect some information about datatype_to

	datatype_to = tok.Prim_dataslot.Datatype()

	switch datatype_to {
	case rsql.DATATYPE_VARBINARY:
		precision_to = tok.Prim_dataslot.(*data.VARBINARY).Data_precision
		group_to = GROUP_VARBINARY

	case rsql.DATATYPE_VARCHAR:
		precision_to = tok.Prim_dataslot.(*data.VARCHAR).Data_precision
		fixlen_flag_to = tok.Prim_dataslot.(*data.VARCHAR).Data_fixlen_flag
		group_to = GROUP_VARCHAR

	case rsql.DATATYPE_TINYINT:
		precision_to = rsql.DATATYPE_PRECISION_TINYINT
		group_to = GROUP_NUMBER

	case rsql.DATATYPE_SMALLINT:
		precision_to = rsql.DATATYPE_PRECISION_SMALLINT
		group_to = GROUP_NUMBER

	case rsql.DATATYPE_INT:
		precision_to = rsql.DATATYPE_PRECISION_INT
		group_to = GROUP_NUMBER

	case rsql.DATATYPE_BIGINT:
		precision_to = rsql.DATATYPE_PRECISION_BIGINT
		group_to = GROUP_NUMBER

	case rsql.DATATYPE_MONEY:
		precision_to = rsql.DATATYPE_PRECISION_MONEY
		scale_to = rsql.DATATYPE_SCALE_MONEY
		group_to = GROUP_NUMBER

	case rsql.DATATYPE_NUMERIC:
		precision_to = tok.Prim_dataslot.(*data.NUMERIC).Data_precision
		scale_to = tok.Prim_dataslot.(*data.NUMERIC).Data_scale
		group_to = GROUP_NUMBER

	case rsql.DATATYPE_FLOAT:
		precision_to = 0xffff // pretends this, so that MONEY and NUMERIC accept to fit in FLOAT
		scale_to = 0x00ff     // same
		group_to = GROUP_NUMBER

	case rsql.DATATYPE_DATE,
		rsql.DATATYPE_TIME,
		rsql.DATATYPE_DATETIME:
		group_to = GROUP_DATE_TIME

	default:
		return // if datatype_to is not listed here, just return.
	}

	//=== CAST from NUMBER to NUMBER to NUMBER ===

	if group_from == GROUP_NUMBER && group_middle == GROUP_NUMBER && group_to == GROUP_NUMBER {

		// === check rounding modes

		// if scale_from == 0, no need to check rounding modes, because no rounding takes place
		// for example :   integer -> float -> integer, or numeric(8) -> numeric(12,4) -> numeric(5)

		if scale_from > 0 { // if fractional part exists, rounding mode of the result must be compatible with the rounding modes of the two CASTs
			// E.g. if MONEY -> NUMERIC -> integer    or    NUMERIC -> MONEY -> integer      or     NUMERIC -> NUMERIC -> integer    or     MONEY -> FLOAT -> integer     never simplify.
			//   because MONEY rounds to integer, but NUMERIC or FLOAT truncates to integer.
			//   Simplifying would lead to MONEY -> integer or NUMERIC -> integer, but the different rounding mode of datatype_middle would be lost.

			// Also, NUMERIC -> integer -> NUMERIC has the same rounding problem, which is 'trunc' + 'neutral', whereas the simplified CAST would be 'round'

			rounding_mode_middle = get_cast_rounding_mode(datatype_from, datatype_middle) // S_CAST_MODE_NEUTRAL, S_CAST_MODE_ROUND or S_CAST_MODE_TRUNC
			rounding_mode_to = get_cast_rounding_mode(datatype_middle, datatype_to)       //  same
			rounding_mode_simplified = get_cast_rounding_mode(datatype_from, datatype_to) //  same

			if rounding_mode_simplified != (rounding_mode_middle | rounding_mode_to) { // if rounding modes are not compatible, simplification is impossible. Just return.
				// println("##### INCOMPATIBLE ROUNDING MODE")
				return
			}
		}

		// === check precision and scale of tok_middle

		// We choose to be quite lenient with the simplification, as numeric(3)->tinyint->smallint is simplified into numeric(3)->smallint
		//   So, 900->900 succeeds, but would have raised overflow if simplification was not applied.

		integral_from = precision_from - scale_from
		integral_middle = precision_middle - scale_middle
		integral_to = precision_to - scale_to

		if (integral_middle < integral_from && integral_middle < integral_to) || // 'middle' integral part should not truncate the 'from' integral part more than 'to' would do.
			(scale_middle < scale_from && scale_middle < scale_to) { // 'middle' fractional part should not truncate the 'from' fractional part more than 'to' would do.
			// println("##### TOK MIDDLE CLIPPING")
			return
		}

		goto LABEL_SIMPLIFY
	}

	//=== CAST from VARCHAR to VARCHAR to any higher ===

	if group_from == GROUP_VARCHAR && group_middle == GROUP_VARCHAR && tok.Prim_subtype == ast.TOK_PRIM_CAST_ENLARGE {
		if precision_middle < precision_from {
			return
		}

		goto LABEL_SIMPLIFY
	}

	//=== CAST from VARCHAR to VARCHAR to VARCHAR ===

	if group_from == GROUP_VARCHAR && group_middle == GROUP_VARCHAR && group_to == GROUP_VARCHAR && fixlen_flag_from == fixlen_flag_middle && fixlen_flag_middle == fixlen_flag_to {
		if precision_middle < precision_from && precision_middle < precision_to {
			return
		}

		goto LABEL_SIMPLIFY
	}

	return // if we get here, it means that we haven't found a way to merge the tokens.

	//========= collapse grandchild into current token, or make it child of current token. tok_middle will always be discarded. =========

LABEL_SIMPLIFY:

	new_prim_subtype, new_instruction_code, _, _, _, _ = cast_analysis(tok_from.Prim_dataslot, tok.Prim_dataslot) // only instruction_code and subtype are needed

	if new_prim_subtype == ast.TOK_PRIM_CAST_ILLEGAL { // if simplified cast instruction does not exist, simplification is impossible. Just return.
		return
	}

	// if CAST does nothing, collapse grandchild 'tok_from' into current token 'tok', and discard child and grandchild.

	if new_prim_subtype == ast.TOK_PRIM_CAST_COPY {

		// IMPORTANT : note that tok_from may be a CAST or any other kind of token, like literal, function, variable, etc.
		//             note that we must also copy prim_dataslot of the grandchild, because if it is a variable, its dataslot is shared through the symbol table. And also if it is a literal constant, its dataslot contains a value.

		sql_collate_bak = tok.Prim_sql_collate
		collation_strength_bak = tok.Prim_collation_strength
		collation_bak = tok.Prim_collation
		*tok = *tok_from
		tok.Prim_sql_collate = sql_collate_bak
		tok.Prim_collation_strength = collation_strength_bak
		tok.Prim_collation = collation_bak

		tok_from.Prim_instruction_code = rsql.INSTR_CAST_ILLEGAL // put garbage in discarded tok_from
		tok_from.Prim_dataslot = nil
		tok_middle.Prim_instruction_code = rsql.INSTR_CAST_ILLEGAL // put garbage in discarded tok_middle
		tok_middle.Prim_dataslot = nil

		return // === and return.
	}

	// if normal CAST, change Prim_left and instruction_code, and discard child.
	// Its dataslot should be kept, as it is the target dataslot with proper datatype.
	//
	// Child tok_middle is a CAST, and not a variable or a column or a literal value. This means that its dataslot is not shared, but is just a storage for temporary result, so it can be discarded safely.

	tok.Prim_left = tok_from // argument points to grandchild.

	tok_middle.Prim_instruction_code = rsql.INSTR_CAST_ILLEGAL // discard tok_middle and its Dataslot
	tok_middle.Prim_dataslot = nil

	tok.Prim_subtype = new_prim_subtype              // update Prim_subtype
	tok.Prim_instruction_code = new_instruction_code // put new simplified CAST instruction

	tok.Prim_sql_datatype = 0  // clear these fields, so that when displaying the token, we know it has been simplified.
	tok.Prim_sql_precision = 0 // but the precision, scale, fixlen_flag, collate of the current token are preserved in its Dataslot.
	tok.Prim_sql_scale = 0
	tok.Prim_sql_fixlen_flag = false

}

// transform_DATETIME_CAST_to_SYSFUNC_CONVERT transforms an implicit or explicit CAST token into a INSTR_SYSFUNC_CONVERT_XXX token.
//
// In the AST tree, the CAST and CONVERT functions are all created as TOK_PRIM_CAST tokens by the parser.
// Here, in the decoration stage, some of these TOK_PRIM_CAST tokens will be transformed to TOK_PRIM_SYSFUNC.
// The token will not be a INSTR_CAST_XXX_XXX anymore, but becomes a normal CONVERT function.
//
//      This transformation is done only if the CAST is :
//          - INSTR_CAST_VARCHAR_DATE     : transformed to INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE       with '0'   as style if style is missing, and syslanguage argument.
//          - INSTR_CAST_VARCHAR_TIME     : transformed to INSTR_SYSFUNC_CONVERT_VARCHAR_TO_TIME       with '0'   as style if style is missing, and syslanguage argument.
//          - INSTR_CAST_VARCHAR_DATETIME : transformed to INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME   with '0'   as style if style is missing, and syslanguage argument.
//          - INSTR_CAST_DATE_VARCHAR     : transformed to INSTR_SYSFUNC_CONVERT_DATE_TO_VARCHAR       with '121' as style if style is missing, and syslanguage argument, like MS SQL Server.
//          - INSTR_CAST_TIME_VARCHAR     : transformed to INSTR_SYSFUNC_CONVERT_TIME_TO_VARCHAR       with '121' as style if style is missing, and syslanguage argument, like MS SQL Server.
//          - INSTR_CAST_DATETIME_VARCHAR : transformed to INSTR_SYSFUNC_CONVERT_DATETIME_TO_VARCHAR   with '121' as style if style is missing, and syslanguage argument, like MS SQL Server for datetime2.
//
//      The token is transformed to a sysfunc token and will have the following fields :
//          - Prim_type             : TOK_PRIM_SYSFUNC
//          - Prim_subtype          : TOK_PRIM_SYSFUNC_SUB
//          - Prim_name             : LEXEME_KEYWORD_CONVERT
//          - Prim_sysfunc_descr    : SYSFUNC_DESCR_CONVERT
//          - Prim_instruction_code : e.g. INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME etc
//          - Prim_listexpr         : argument in Prim_left is put here, and style argument in Prim_right too
//          - Prim_syslanguage      : syslanguage token
//          - Prim_left             : is set to nil
//          - Prim_right            : is set to nil
//
// If the CAST is a normal cast (if it is not in the list above), nothing is done and the function just returns. In this case, if a style parameter has been passed in an EXPLICIT CAST in Prim_right, it is just ignored and set to nil.
//
//      NOTE: target may be VARCHAR with fixlen, if SQL script was e.g CAST(@mydatetime as char(50)), which is transformed here into CONVERT.
//
func (decor *Decorator) transform_DATETIME_CAST_to_SYSFUNC_CONVERT(tok_cast *ast.Token_primary) *rsql.Error {
	var (
		rsql_err                *rsql.Error
		style_parameter_token   *ast.Token_primary
		style_value             int32
		append_syslanguage_flag bool
		result_is_varchar_flag  bool
		tok_syslanguage         *ast.Token_primary
		listexpr                []*ast.Token_primary
	)

	// convert CAST token to a CONVERT function token

	switch tok_cast.Prim_instruction_code {
	// for cast VARCHAR to DATETIME, transform to parse function.

	case rsql.INSTR_CAST_VARCHAR_DATE:
		tok_cast.Prim_instruction_code = rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATE
		style_value = 0 // add token with constant '0' as style parameter if it doesn't exist.
		append_syslanguage_flag = true

	case rsql.INSTR_CAST_VARCHAR_TIME:
		tok_cast.Prim_instruction_code = rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_TIME
		style_value = 0 // add token with constant '0' as style parameter if it doesn't exist.
		append_syslanguage_flag = true

	case rsql.INSTR_CAST_VARCHAR_DATETIME:
		tok_cast.Prim_instruction_code = rsql.INSTR_SYSFUNC_CONVERT_VARCHAR_TO_DATETIME
		style_value = 0 // add token with constant '0' as style parameter if it doesn't exist.
		append_syslanguage_flag = true

		// for cast DATETIME to VARCHAR, transform to format function.

	case rsql.INSTR_CAST_DATE_VARCHAR:
		tok_cast.Prim_instruction_code = rsql.INSTR_SYSFUNC_CONVERT_DATE_TO_VARCHAR
		style_value = 121 // add token with constant '121' as style parameter if it doesn't exist.
		append_syslanguage_flag = true
		result_is_varchar_flag = true

	case rsql.INSTR_CAST_TIME_VARCHAR:
		tok_cast.Prim_instruction_code = rsql.INSTR_SYSFUNC_CONVERT_TIME_TO_VARCHAR
		style_value = 121 // add token with constant '121' as style parameter if it doesn't exist.
		append_syslanguage_flag = true
		result_is_varchar_flag = true

	case rsql.INSTR_CAST_DATETIME_VARCHAR:
		tok_cast.Prim_instruction_code = rsql.INSTR_SYSFUNC_CONVERT_DATETIME_TO_VARCHAR
		style_value = 121 // add token with constant '121' as style parameter if it doesn't exist.
		append_syslanguage_flag = true
		result_is_varchar_flag = true

		// for other cast, discard useless style parameter if any. The cast is a normal CAST, and we just return.

	default:
		tok_cast.Prim_right = nil // discard useless style if any
		return nil
	}

	//==== transform CAST token to a sysfunc token ===

	tok_cast.Prim_type = ast.TOK_PRIM_SYSFUNC // the token becomes a normal builtin function
	tok_cast.Prim_subtype = ast.TOK_PRIM_SYSFUNC_SUB

	tok_cast.Prim_name = lex.LEXEME_KEYWORD_CONVERT.Lex_word
	tok_cast.Prim_sysfunc_descr = ast.SYSFUNC_DESCR_CONVERT

	if result_is_varchar_flag {
		tok_cast.Prim_collation_strength = rsql.COLLSTRENGTH_0 // setting collation here is not really useful, because the caller has already done it for the CAST token. These two lines can be deleted, but I prefer to keep them.
		tok_cast.Prim_collation = decor.Server_default_collation
	}

	//==== create optional style parameter token if not available ===

	style_parameter_token = tok_cast.Prim_right // style parameter may be not nil only for EXPLICIT CAST ( which must have been a CONVERT() SQL instruction ).

	switch {
	case style_parameter_token == nil: // if no style parameter, create a default style parameter token
		style_parameter_token = decor.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(style_value)
		style_parameter_token.Prim_dataslot = data.New_literal_INT_value(style_value)

	default: // if EXPLICIT CAST and style parameter is passed (=CONVERT), equalize style parameter.
		if style_parameter_token.Prim_dataslot.Datatype() != rsql.DATATYPE_INT {
			if style_parameter_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(style_parameter_token, rsql.DATATYPE_INT); rsql_err != nil {
				return rsql_err
			}
		}
	}

	//=== create listexpr with child, style parameter, and syslanguage if needed ===

	listexpr = []*ast.Token_primary{tok_cast.Prim_left, style_parameter_token}
	tok_cast.Prim_listexpr = listexpr

	if append_syslanguage_flag {
		tok_syslanguage = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE)
		if rsql_err = decor.walk_Token_primary_and_simplify(tok_syslanguage); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(tok_syslanguage.Prim_dataslot.Datatype() == rsql.DATATYPE_SYSLANGUAGE)
		tok_cast.Prim_syslanguage = tok_syslanguage // add syslanguage as argument
	}

	tok_cast.Prim_left = nil // arguments are in Prim_listexpr now.
	tok_cast.Prim_right = nil

	return nil
}

// Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot is used for "implicit equalizing cast".
//
// This kind of operation takes place with most operators, when one operand must be cast to the 'stronger' datatype of the other operand.
// But with other operation like ISNULL(a,b), the operand b must be cast to the datatype of a, which can be 'weaker' ( that is, of lesser rank ).
//
//             declare @a int
//             declare @b float
//             select @a + @b         -- the '+' operator result type is float. @a is implicitly cast from INT to FLOAT
//
//             select ISNULL(@a, @b)  -- @b is implicitly cast from FLOAT to INT
//
// If the two operands are not of the same datatype, a new CAST Token_primary is created and inserted in the AST between the '+' operator and the operand to convert 'tok_from'.
//
// This function is called during the walking of the ast, and all operands have already been processed and have their datatypes defined.
//
// The target datatype 'datatype_to' MUST BE DIFFERENT than datatype of 'tok_from'. Else, panics. The caller must check this.
//
// Returns pointer to the new CAST Token_primary
//
//            The important point is that the function should create a CAST token using only the 'datatype_to' argument.
//            This function doesn't need other info like p, s, collation, etc.
//
//
//            *** WE JUST GIVE THE PLAIN TARGET DATATYPE (without p, s, or whatever) AND THIS FUNCTION SHOULD CREATE THE PROPER CAST TOKEN ***
//
//            *** WE JUST WANT TO HAVE THE SAME DATATYPE FOR BOTH OPERANDS, BUT WE DON'T CARE ABOUT THE PRECISION, SCALE, OR FIXLEN_FLAG ***
//                     - an operator can have an operand which is a normal VARCHAR(p) and the other being a VARCHAR(q) with fixlen_flag==true, for instance.
//                     - an operator can have an operand of type NUMERIC(p,s) and the other of type NUMERIC(q,r)
//
//            We don't care about p, s, or fixlen_flag of the target. We see indeed that this information is not passed in the list of function arguments.
//
// For the moment, only VOID can be implicitly cast to VARCHAR.
//
func (decor *Decorator) Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok_from *ast.Token_primary, datatype_to rsql.Datatype_t) (tok_cast *ast.Token_primary, rsql_err *rsql.Error) {

	if tok_cast, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot_no_simplify(tok_from, datatype_to); rsql_err != nil {
		return nil, rsql_err
	}

	// simplify CAST

	if tok_cast.Prim_type == ast.TOK_PRIM_CAST {
		simplify_CAST(tok_cast)
	}

	return tok_cast, nil
}

// Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot_no_simplify creates a CAST Token for implicit equalization.
// See comments for Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot.
//
// The target datatype 'datatype_to' MUST BE DIFFERENT than datatype of 'tok_from'. Else, panics. The caller must check this.
//
// A new CAST Token is always returned, because no simplification is done.
//
// This function is used by wacf_Xtable_union() in walk_statements_SELECT.go.
//
func (decor *Decorator) Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot_no_simplify(tok_from *ast.Token_primary, datatype_to rsql.Datatype_t) (tok_cast *ast.Token_primary, rsql_err *rsql.Error) {
	var (
		dataslot         rsql.IDataslot
		instruction_code rsql.Instrcode_t
		prim_subtype     ast.Prim_subtype_t

		datatype_from             rsql.Datatype_t
		datatype_to_precision     uint16
		datatype_to_scale         uint16
		datatype_to_fixlen_flag   bool
		result_collation_strength rsql.Collstrength_t
		result_collation          string
	)

	/*
	   With MS SQL Server, an implicit conversion from CHAR or VARCHAR to NUMERIC is allowed for equalization cast.
	   In this case, more information is needed to define precisely the target datatype.
	   Indeed, MS SQL Server sets the precision and scale of the target numeric datatype to be the same as the other operand.

	   I think it is dangerous to allow that, so this kind of cast is illegal with RSQL. You should use an explicit CAST in your SQL script !

	   Example with MS SQL Server :
	     declare @a varchar(30) = '1'
	     print 2.34 + @a  --  3.34
	     set @a = '9'
	     print 2.34 + @a  -- 11.34
	     set @a = '10'
	     print 2.34 + @a  -- Arithmetic overflow error converting varchar to data type numeric.

	  If I really must do that one day, I would prefer to create a target NUMERIC with max precision and scale.
	*/

	datatype_from = tok_from.Prim_dataslot.Datatype() // datatype of the operand to cast

	rsql.Assert(datatype_from != datatype_to) // datatype_from and datatype_to must be different. This must have been checked by the caller.

	prim_subtype = ast.TOK_PRIM_CAST_ENLARGE // define prim_subtype = TOK_PRIM_CAST_ENLARGE or _RESTRICT. ( never _ADJUST or _COPY, because source and target datatypes are different )
	if datatype_from > datatype_to {
		prim_subtype = ast.TOK_PRIM_CAST_RESTRICT
	}

	// get instruction_code and check if the cast is allowed

	instruction_code = G_SQL_DATATYPE_CAST_INSTRUCTION_CODE[datatype_from][datatype_to] // get the instruction code from a static table.

	if instruction_code == rsql.INSTR_CAST_ILLEGAL {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, datatype_from, datatype_to)
	}

	rsql.Assert(Is_CAST_COPY(instruction_code) == false)

	// if cast to VARBINARY, VARCHAR, NUMERIC, determine precision, scale, fixlen_flag and collation of the result from the attributes of the source datatype

	switch datatype_to {
	case rsql.DATATYPE_VARBINARY: // === target VARBINARY
		switch datatype_from { // if we want to allow implicit equalization cast for other datatype_from, just add them as new case.
		case rsql.DATATYPE_VOID:
			datatype_to_precision = 1 // default.

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, datatype_from, datatype_to)
		}

	case rsql.DATATYPE_VARCHAR: // === target VARCHAR. We decide that fixlen_flag = false, with default collation.
		switch datatype_from { // if we want to allow implicit equalization cast for other datatype_from, just add them as new case.
		case rsql.DATATYPE_VOID:
			datatype_to_precision = 1       // default
			datatype_to_fixlen_flag = false // default
			result_collation_strength = rsql.COLLSTRENGTH_0
			result_collation = decor.Server_default_collation

		case rsql.DATATYPE_BIT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_BIT
			datatype_to_fixlen_flag = false // default
			result_collation_strength = rsql.COLLSTRENGTH_0
			result_collation = decor.Server_default_collation

		case rsql.DATATYPE_TINYINT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_TINYINT + 1 // +1 for sign
			datatype_to_fixlen_flag = false                             // default
			result_collation_strength = rsql.COLLSTRENGTH_0
			result_collation = decor.Server_default_collation

		case rsql.DATATYPE_SMALLINT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_SMALLINT + 1 // +1 for sign
			datatype_to_fixlen_flag = false                              // default
			result_collation_strength = rsql.COLLSTRENGTH_0
			result_collation = decor.Server_default_collation

		case rsql.DATATYPE_INT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_INT + 1 // +1 for sign
			datatype_to_fixlen_flag = false                         // default
			result_collation_strength = rsql.COLLSTRENGTH_0
			result_collation = decor.Server_default_collation

		case rsql.DATATYPE_BIGINT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_BIGINT + 1 // +1 for sign
			datatype_to_fixlen_flag = false                            // default
			result_collation_strength = rsql.COLLSTRENGTH_0
			result_collation = decor.Server_default_collation

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, datatype_from, datatype_to)
		}

	case rsql.DATATYPE_NUMERIC: // === target NUMERIC.
		switch datatype_from { // if we want to allow implicit equalization cast for other datatype_from, just add them as new case.
		case rsql.DATATYPE_VOID:
			datatype_to_precision = 1

			//          case rsql.DATATYPE_VARCHAR :  // implicit equalization cast from VARCHAR to NUMERIC is not allowed, because it is difficult to find the target <p,s>
			//            panic("cast from VARCHAR to NUMERIC is not allowed")

		case rsql.DATATYPE_BIT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_BIT

		case rsql.DATATYPE_TINYINT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_TINYINT

		case rsql.DATATYPE_SMALLINT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_SMALLINT

		case rsql.DATATYPE_INT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_INT

		case rsql.DATATYPE_BIGINT:
			datatype_to_precision = rsql.DATATYPE_PRECISION_BIGINT

		case rsql.DATATYPE_MONEY:
			datatype_to_precision = rsql.DATATYPE_PRECISION_MONEY
			datatype_to_scale = rsql.DATATYPE_SCALE_MONEY

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, datatype_from, datatype_to)
		}
	}

	// create a new CAST Token_primary

	tok_cast = decor.New_token_primary(ast.TOK_PRIM_CAST, prim_subtype, lex.LEXEME_KEYWORD_CAST) // create new CAST Token_primary
	tok_cast.Tok_batch_line = tok_from.Tok_batch_line                                            // line/pos
	tok_cast.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_cast.Prim_instruction_code = instruction_code

	tok_cast.Prim_collation_strength = result_collation_strength
	tok_cast.Prim_collation = result_collation
	rsql.Assert((datatype_to == rsql.DATATYPE_VARCHAR) != (tok_cast.Prim_collation == ""))

	// create a dataslot of the proper datatype and attach it to the CAST token

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag)

	tok_cast.Prim_dataslot = dataslot // attach Dataslot to CAST token.
	tok_cast.Prim_left = tok_from     // argument of CAST token is the operand that need to be cast.

	// for INSTR_CAST_VARCHAR_DATE/TIME/DATETIME, INSTR_CAST_DATE/TIME/DATETIME_VARCHAR, transform to ordinary 'CONVERT' function with default style parameter, and syslanguage if needed

	if rsql_err = decor.transform_DATETIME_CAST_to_SYSFUNC_CONVERT(tok_cast); rsql_err != nil {
		return nil, rsql_err
	}

	return tok_cast, nil
}

// Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot creates a CAST token, used to implicitly cast a dataslot to exactly the same datatype and attributes as sample dataslot passed as argument.
//
// This kind of operation takes place when a value is assigned to a variable, or to a column in an INSERT statement, or to a user function argument.
//
//      declare @a int
//      set @a = 123.45    -- NUMERIC(5,2) --> INT
//
// If rvalue and lvalue are not EXACTLY of the same datatype, a new CAST Token_primary is created and inserted in the AST between the '=' assignment operator and the operand 'tok_from' to convert.
//
// This function is called during the walking of the ast, and all operands have already been processed and have their datatypes defined.
//
//     ***IMPORTANT*** For variable assignment, the caller must force collation to be the collation of the variable, which is always default server collation.
//                     When a variable occurence will later point to this Token_primary, it will copy this collation, which is what we want.
//
//     ********* WE WANT DATAYPE, BUT ALSO PRECISION, SCALE, AND FIXLEN_FLAG TO MATCH THOSE OF THE TARGET *********
//               (FOR VARIABLE ASSIGNMENT, IF VARCHAR DATASLOT, THE CALLER MUST FORCE COLLATION_STRENGTH AND COLLATION TO DEFAULT SERVER COLLATION, WHICH IS THE COLLATION FOR ALL VARCHAR VARIABLES)
//
func (decor *Decorator) Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot(tok_from *ast.Token_primary, sample_Data_XXX_to rsql.IDataslot) (*ast.Token_primary, *rsql.Error) {
	var (
		rsql_err                *rsql.Error
		tok_cast                *ast.Token_primary
		dataslot                rsql.IDataslot
		instruction_code        rsql.Instrcode_t
		prim_subtype            ast.Prim_subtype_t
		datatype_to             rsql.Datatype_t
		datatype_to_precision   uint16
		datatype_to_scale       uint16
		datatype_to_fixlen_flag bool
	)

	// extract information for the CAST

	prim_subtype, instruction_code, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag = cast_analysis(tok_from.Prim_dataslot, sample_Data_XXX_to)

	if prim_subtype == ast.TOK_PRIM_CAST_ILLEGAL { // if cast instruction does not exist, just return.
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, tok_from.Prim_dataslot.Datatype(), sample_Data_XXX_to.Datatype())
	}

	if prim_subtype == ast.TOK_PRIM_CAST_COPY { // if 'tok_from' and 'sample_Data_XXX_to' token are EXACTLY the same datatype (also p,s,fixlen_flag), just return the 'tok_from' token.
		return tok_from, nil
	}

	// create a new CAST Token_primary

	tok_cast = decor.New_token_primary(ast.TOK_PRIM_CAST, prim_subtype, lex.LEXEME_KEYWORD_CAST) // create new CAST Token_primary
	tok_cast.Tok_batch_line = tok_from.Tok_batch_line                                            // line/pos
	tok_cast.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_cast.Prim_instruction_code = instruction_code

	// create a dataslot of the proper datatype and attach it to the CAST token

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, datatype_to, datatype_to_precision, datatype_to_scale, datatype_to_fixlen_flag)
	tok_cast.Prim_dataslot = dataslot // attach Dataslot to CAST token.
	tok_cast.Prim_left = tok_from     // argument of CAST token is the operand that need to be cast.

	if datatype_to == rsql.DATATYPE_VARCHAR {
		if tok_from.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
			rsql.Assert(tok_from.Prim_collation != "")
			tok_cast.Prim_collation_strength = tok_from.Prim_collation_strength
			tok_cast.Prim_collation = tok_from.Prim_collation
		} else {
			tok_cast.Prim_collation_strength = rsql.COLLSTRENGTH_0
			tok_cast.Prim_collation = decor.Server_default_collation
		}
	}

	// for INSTR_CAST_VARCHAR_DATE/TIME/DATETIME, INSTR_CAST_DATE/TIME/DATETIME_VARCHAR, transform to ordinary 'CONVERT' function with default style parameter, and syslanguage if needed

	if rsql_err = decor.transform_DATETIME_CAST_to_SYSFUNC_CONVERT(tok_cast); rsql_err != nil {
		return nil, rsql_err
	}

	// simplify CAST

	if tok_cast.Prim_type == ast.TOK_PRIM_CAST {
		simplify_CAST(tok_cast)
	}

	return tok_cast, nil
}

// Token_primary_COPY_or_CAST_EXACT_new_to_target_dataslot creates a CAST Token_primary, which can be a plain copy, e.g. INSTR_CAST_INT_INT, to cast tok_from to target_dataslot.
// The dataslot of the CAST Token_primary will be set to target_dataslot.
// This function is used when we need to cast or copy a dataslot to another target dataslot.
// This is used for UNION, when we want to cast or copy SELECT or UNION columns to the equalized_row dataslots.
//
// This function is exactly the same as Token_primary_CAST_EXACT_new_to_target_dataslot, but it can return a plain copy, e.g. INSTR_CAST_INT_INT.
//
// No simplification is done.
//
func (decor *Decorator) Token_primary_COPY_or_CAST_EXACT_new_to_target_dataslot(tok_from *ast.Token_primary, target_dataslot rsql.IDataslot) (*ast.Token_primary, *rsql.Error) {
	var (
		rsql_err         *rsql.Error
		tok_cast         *ast.Token_primary
		instruction_code rsql.Instrcode_t
		prim_subtype     ast.Prim_subtype_t
		datatype_to      rsql.Datatype_t
	)

	// extract information for the CAST

	prim_subtype, instruction_code, datatype_to, _, _, _ = cast_analysis(tok_from.Prim_dataslot, target_dataslot)

	if prim_subtype == ast.TOK_PRIM_CAST_ILLEGAL { // if cast instruction does not exist, just return.
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, tok_from.Prim_dataslot.Datatype(), target_dataslot.Datatype())
	}

	// create a new CAST Token_primary, which can be a plain copy instruction, e.g. INSTR_CAST_INT_INT

	tok_cast = decor.New_token_primary(ast.TOK_PRIM_CAST, prim_subtype, lex.LEXEME_KEYWORD_CAST) // create new CAST Token_primary
	tok_cast.Tok_batch_line = tok_from.Tok_batch_line                                            // line/pos
	tok_cast.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_cast.Prim_instruction_code = instruction_code

	// attach target_dataslot to the CAST token

	tok_cast.Prim_dataslot = target_dataslot // attach Dataslot to CAST token.
	tok_cast.Prim_left = tok_from            // argument of CAST token is the operand that need to be cast.

	if datatype_to == rsql.DATATYPE_VARCHAR {
		if tok_from.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
			rsql.Assert(tok_from.Prim_collation != "")
			tok_cast.Prim_collation_strength = tok_from.Prim_collation_strength
			tok_cast.Prim_collation = tok_from.Prim_collation
		} else {
			tok_cast.Prim_collation_strength = rsql.COLLSTRENGTH_0
			tok_cast.Prim_collation = decor.Server_default_collation
		}
	}

	// for INSTR_CAST_VARCHAR_DATE/TIME/DATETIME, INSTR_CAST_DATE/TIME/DATETIME_VARCHAR, transform to ordinary 'CONVERT' function with default style parameter, and syslanguage if needed

	if rsql_err = decor.transform_DATETIME_CAST_to_SYSFUNC_CONVERT(tok_cast); rsql_err != nil {
		return nil, rsql_err
	}

	return tok_cast, nil
}

// Token_primary_CAST_EXACT_new_to_target_dataslot creates a CAST Token_primary to cast tok_from to target_dataslot.
//
// It is NEVER a plain copy, e.g. INSTR_CAST_INT_INT. In this case, no conversion is needed and tok_from is returned.
//
// The dataslot of the CAST Token_primary will be set to target_dataslot.
// This function is used when we need to cast a dataslot to another target dataslot.
// This is used by INSERT, to cast columns from a SELECT into the proper datatypes of the target table.
//
// No simplification is done.
//
func (decor *Decorator) Token_primary_CAST_EXACT_new_to_target_dataslot(tok_from *ast.Token_primary, target_dataslot rsql.IDataslot) (*ast.Token_primary, *rsql.Error) {
	var (
		rsql_err         *rsql.Error
		tok_cast         *ast.Token_primary
		instruction_code rsql.Instrcode_t
		prim_subtype     ast.Prim_subtype_t
		datatype_to      rsql.Datatype_t
	)

	// extract information for the CAST

	prim_subtype, instruction_code, datatype_to, _, _, _ = cast_analysis(tok_from.Prim_dataslot, target_dataslot)

	if prim_subtype == ast.TOK_PRIM_CAST_ILLEGAL { // if cast instruction does not exist, just return.
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, tok_from.Prim_dataslot.Datatype(), target_dataslot.Datatype())
	}

	if prim_subtype == ast.TOK_PRIM_CAST_COPY { // if 'tok_from' and 'sample_Data_XXX_to' token are EXACTLY the same datatype (also p,s,fixlen_flag), just return the 'tok_from' token.
		return tok_from, nil
	}

	// create a new CAST Token_primary, which can be a plain copy instruction, e.g. INSTR_CAST_INT_INT

	tok_cast = decor.New_token_primary(ast.TOK_PRIM_CAST, prim_subtype, lex.LEXEME_KEYWORD_CAST) // create new CAST Token_primary
	tok_cast.Tok_batch_line = tok_from.Tok_batch_line                                            // line/pos
	tok_cast.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_cast.Prim_instruction_code = instruction_code

	// attach target_dataslot to the CAST token

	tok_cast.Prim_dataslot = target_dataslot // attach Dataslot to CAST token.
	tok_cast.Prim_left = tok_from            // argument of CAST token is the operand that need to be cast.

	if datatype_to == rsql.DATATYPE_VARCHAR {
		if tok_from.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
			rsql.Assert(tok_from.Prim_collation != "")
			tok_cast.Prim_collation_strength = tok_from.Prim_collation_strength
			tok_cast.Prim_collation = tok_from.Prim_collation
		} else {
			tok_cast.Prim_collation_strength = rsql.COLLSTRENGTH_0
			tok_cast.Prim_collation = decor.Server_default_collation
		}
	}

	// for INSTR_CAST_VARCHAR_DATE/TIME/DATETIME, INSTR_CAST_DATE/TIME/DATETIME_VARCHAR, transform to ordinary 'CONVERT' function with default style parameter, and syslanguage if needed

	if rsql_err = decor.transform_DATETIME_CAST_to_SYSFUNC_CONVERT(tok_cast); rsql_err != nil {
		return nil, rsql_err
	}

	return tok_cast, nil
}

// Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY_new_with_Dataslot inserts a verification token INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY, to check VARBINARY length before insertion in table.
// Argument tok_from must be *VARBINARY.
//
// It is a function, not a CAST, but as its purpose is much like a CAST, it is defined here.
//
// If no verification token is needed because target precision is larger than source, tok_from is returned.
//
func (decor *Decorator) Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY_new_with_Dataslot(tok_from *ast.Token_primary, precision_to uint16) *ast.Token_primary {
	var (
		tok_verif      *ast.Token_primary
		dataslot_from  *data.VARBINARY
		precision_from uint16
		dataslot       rsql.IDataslot
	)

	// check if verification node is needed

	dataslot_from = tok_from.Prim_dataslot.(*data.VARBINARY)
	precision_from = dataslot_from.Data_precision

	if precision_from <= precision_to { // source always fits in dest
		return tok_from
	}

	// create a new verification sysfunc token

	tok_verif = decor.New_token_primary(ast.TOK_PRIM_SYSFUNC, ast.TOK_PRIM_SYSFUNC_SUB, lex.LEXEME_DISPLAYWORD_FIT_NO_TRUNCATE_VARBINARY) // create new verif Token_primary
	tok_verif.Tok_batch_line = tok_from.Tok_batch_line                                                                                    // line/pos
	tok_verif.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_verif.Prim_sysfunc_descr = ast.SYSFUNC_DESCR_FIT_NO_TRUNCATE_VARBINARY
	tok_verif.Prim_instruction_code = rsql.INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY

	// create a dataslot of the proper datatype and attach it to the verif token

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, rsql.DATATYPE_VARBINARY, precision_to, 0, false)
	tok_verif.Prim_dataslot = dataslot // attach Dataslot to verif token.

	tok_verif.Prim_listexpr = []*ast.Token_primary{tok_from} // argument of verif token

	return tok_verif
}

// Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR_new_with_Dataslot inserts a verification token INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR, to check VARCHAR length before insertion in table.
// Argument tok_from must be *VARCHAR.
//
// It is a function, not a CAST, but as its purpose is much like a CAST, it is defined here.
//
// If no verification token is needed because target precision is larger than source, tok_from is returned.
//
func (decor *Decorator) Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR_new_with_Dataslot(tok_from *ast.Token_primary, precision_to uint16, fixlen_flag_to bool) *ast.Token_primary {
	var (
		tok_verif        *ast.Token_primary
		dataslot_from    *data.VARCHAR
		precision_from   uint16
		fixlen_flag_from bool
		dataslot         rsql.IDataslot
	)

	// check if verification node is needed

	dataslot_from = tok_from.Prim_dataslot.(*data.VARCHAR)
	precision_from = dataslot_from.Data_precision
	fixlen_flag_from = dataslot_from.Data_fixlen_flag

	if fixlen_flag_from == fixlen_flag_to && precision_from == precision_to { // source and dest are exactly same fixlen_flag and precision
		return tok_from
	}

	if fixlen_flag_from == false && fixlen_flag_to == false && precision_from <= precision_to { // source always fits in dest
		return tok_from
	}

	// create a new verification sysfunc token

	tok_verif = decor.New_token_primary(ast.TOK_PRIM_SYSFUNC, ast.TOK_PRIM_SYSFUNC_SUB, lex.LEXEME_DISPLAYWORD_FIT_NO_TRUNCATE_VARCHAR) // create new verif Token_primary
	tok_verif.Tok_batch_line = tok_from.Tok_batch_line                                                                                  // line/pos
	tok_verif.Prim_status = ast.TOK_PRIM_STATUS_COMPLETE
	tok_verif.Prim_sysfunc_descr = ast.SYSFUNC_DESCR_FIT_NO_TRUNCATE_VARCHAR
	tok_verif.Prim_instruction_code = rsql.INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR

	// create a dataslot of the proper datatype and attach it to the verif token

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, rsql.DATATYPE_VARCHAR, precision_to, 0, fixlen_flag_to)
	tok_verif.Prim_dataslot = dataslot // attach Dataslot to verif token.

	tok_verif.Prim_listexpr = []*ast.Token_primary{tok_from} // argument of verif token

	tok_verif.Prim_collation_strength = tok_from.Prim_collation_strength // inherit collation from tok_from
	tok_verif.Prim_collation = tok_from.Prim_collation

	return tok_verif
}
