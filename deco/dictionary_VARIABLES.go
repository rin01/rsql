package deco

import (
	"rsql"
	"rsql/data"
	"rsql/dict"
	"rsql/lex"
)

//======================================================
//                  Dictionary_VARIABLE               //
//======================================================

// Dictionary_VARIABLE is a map containing all the global variables of the batch.
//
type Dictionary_VARIABLE struct {
	Tbl map[string]rsql.IDataslot
}

func (dictvar *Dictionary_VARIABLE) Initialize(session_login_name string, session_login_id int64, language_string string, database_name string) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	// create map

	dictvar.Tbl = make(map[string]rsql.IDataslot, SPEC_DEFAULT_DICTIONARY_VARIABLE_CAPACITY)

	// put system variables in map

	if rsql_err = dictvar.Put_system_variables(session_login_name, session_login_id, language_string, database_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func (dictvar *Dictionary_VARIABLE) Get_symbol_entry(varname string) (rsql.IDataslot, *rsql.Error) {
	var (
		dataslot rsql.IDataslot
		ok       bool
	)

	if dataslot, ok = dictvar.Tbl[varname]; ok == false { // if variable name does not exist yet
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_VARIABLE_NOT_DECLARED, rsql.ERROR_BATCH_ABORT, varname)
	}

	return dataslot, nil
}

func (dictvar *Dictionary_VARIABLE) Put(varname string, dataslot rsql.IDataslot) *rsql.Error {
	var (
		ok bool
	)

	rsql.Assert(dataslot.Kind() == rsql.KIND_VAR_LEAF)

	if _, ok = dictvar.Tbl[varname]; ok == true { // if variable name already exists
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_VARIABLE_ALREADY_DECLARED, rsql.ERROR_BATCH_ABORT, varname)
	}

	dictvar.Tbl[varname] = dataslot

	return nil
}

// Put_system_variables creates system variables.
//
//         All arguments must be in lowercase.
//
func (dictvar *Dictionary_VARIABLE) Put_system_variables(session_login_name string, session_login_id int64, language_string string, database_name string) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		database_dbid int64
		schema_name   string
		schema_schid  int64
		user_name     string
		user_palid    int64
		dataslot      rsql.IDataslot
	)

	//=== put system variable _@current_language in VARIABLES map ===

	if dataslot, rsql_err = data.New_VARIABLE_SYSLANGUAGE_value(language_string); rsql_err != nil { // language is checked here
		return rsql_err
	}

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	//=== put system variable _@system_user_name and _@system_user_id in VARIABLES map ===

	if dataslot, rsql_err = data.New_VARIABLE_VARCHAR_value(session_login_name); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_SYSTEM_USER_NAME.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	// _@system_user_id

	dataslot = data.New_VARIABLE_BIGINT_value(session_login_id)

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_SYSTEM_USER_ID.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	//=== get login's user and schema ===

	if database_dbid, schema_name, schema_schid, user_name, user_palid, rsql_err = dict.MASTER.Use_sysdatabase(session_login_id, session_login_name, database_name); rsql_err != nil {
		return rsql_err
	}

	//=== put system variable _@current_db_name and _@current_db_id in VARIABLES map ===

	if dataslot, rsql_err = data.New_VARIABLE_VARCHAR_value(database_name); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_DB_NAME.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	// _@current_db_id

	dataslot = data.New_VARIABLE_BIGINT_value(database_dbid)

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_DB_ID.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	// put system variable _@current_schema in VARIABLES map

	if dataslot, rsql_err = data.New_VARIABLE_VARCHAR_value(schema_name); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_SCHEMA_NAME.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	// put system variable _@current_schema_id in VARIABLES map

	dataslot = data.New_VARIABLE_BIGINT_value(schema_schid)

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_SCHEMA_ID.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	// put system variable _@current_user_name in VARIABLES map

	if dataslot, rsql_err = data.New_VARIABLE_VARCHAR_value(user_name); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_USER_NAME.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	// put system variable _@current_user_id in VARIABLES map

	dataslot = data.New_VARIABLE_BIGINT_value(user_palid)

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_USER_ID.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	// put system variable _@current_timestamp in VARIABLES map

	dataslot = data.New_VARIABLE_DATETIME_NULL() // NULL. It must be initialized to the current datetime by the program before parsing is run.

	if rsql_err = dictvar.Put(lex.LEXEME_SYSVAR_CURRENT_TIMESTAMP.Lex_word, dataslot); rsql_err != nil { // put new entry in VARIABLES symbol table
		return rsql_err
	}

	return nil
}

func (dictvar *Dictionary_VARIABLE) Update_current_timestamp() {
	var (
		dataslot rsql.IDataslot
		ok       bool
	)

	if dataslot, ok = dictvar.Tbl["_@current_timestamp"]; ok == false { // _@current_timestamp variable must exist
		panic("impossible")
	}

	data.Sysfunc_getutcdate_DATETIME(dataslot.(*data.DATETIME))
}

func (dictvar *Dictionary_VARIABLE) Get_current_db_name() string {
	var (
		dataslot rsql.IDataslot
		ok       bool
	)

	if dataslot, ok = dictvar.Tbl["_@current_db_name"]; ok == false { // _@current_db_name variable should exist
		return "" // but this function can be used when the dictionary failed during creation.
	}

	return data.Get_value_from_VARCHAR(dataslot)
}
