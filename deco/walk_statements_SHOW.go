package deco

import (
	"rsql"
	"rsql/ast"
)

func (decor *Decorator) wacf_Token_stmt_SHOW_INFO(tok *ast.Token_stmt_SHOW_INFO) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHOW_INFO)

	if rsql_err = decor.Wacf_expression(tok.Shinf_login); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = decor.Wacf_expression(tok.Shinf_current_user); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = decor.Wacf_expression(tok.Shinf_current_database); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = decor.Wacf_expression(tok.Shinf_current_schema); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = decor.Wacf_expression(tok.Shinf_current_language); rsql_err != nil {
		return rsql_err
	}

	return nil
}
