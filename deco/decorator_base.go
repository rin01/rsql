package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/lk"
)

type Decorator struct {
	ast.Parser

	VARIABLES Dictionary_VARIABLE
	CONSTANTS Dictionary_CONSTANT

	Basicblock_first   *rsql.Basicblock // main entry point
	Basicblock_current *rsql.Basicblock // current basicblock during decoration stage and emit stage
	Basicblock_list    []*rsql.Basicblock

	Debug_disable_constant_folding_flag bool // should be false. You can set it to true only for debugging.
}

func (decor *Decorator) Initialize(session_login_name string, session_login_id int64, language_string string, database_name string, options ast.Options_t, registration *lk.Registration) *rsql.Error {
	var (
		rsql_err                        *rsql.Error
		server_default_collation_string string
	)

	server_default_collation_string = rsql.G_SERVER_DEFAULT_COLLATION

	if rsql_err = decor.Parser.Initialize(server_default_collation_string, database_name, options, registration); rsql_err != nil {
		return rsql_err
	}

	// initalize map of VARIABLES and CONSTANTS

	if rsql_err = decor.VARIABLES.Initialize(session_login_name, session_login_id, language_string, database_name); rsql_err != nil { // put system variables _@system_user_name, _@system_user_id, _@current_language, etc
		return rsql_err
	}

	decor.CONSTANTS.Initialize()

	return nil
}

// Set_disable_constant_folding_flag can disable constant folding.
// USE IT ONLY FOR DEBUGGING PURPOSE.
//
func (decor *Decorator) Set_disable_constant_folding_flag(flag bool) {
	decor.Debug_disable_constant_folding_flag = flag
}

func New_decorator(session_login_name string, session_login_id int64, language_string string, database_name string, options ast.Options_t, registration *lk.Registration) (*Decorator, *rsql.Error) {

	decor := &Decorator{}

	if rsql_err := decor.Initialize(session_login_name, session_login_id, language_string, database_name, options, registration); rsql_err != nil {
		return nil, rsql_err
	}

	return decor, nil
}
