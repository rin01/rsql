package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/dict"
	"rsql/lex"
)

func (decor *Decorator) wacf_Token_stmt_BULK_INSERT(tok *ast.Token_stmt_BULK_INSERT) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err        *rsql.Error
		gtabledef       *rsql.GTabledef
		tok_syslanguage *ast.Token_primary
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_BULK_INSERT)

	// get GTabledef from dict

	if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(tok.Bi_table_qname); rsql_err != nil {
		return rsql_err
	}

	tok.Bi_gtabledef = gtabledef

	// check if table has an IDENTITY column

	if gtabledef.Has_IDENTITY_column() == true {
		if tok.Bi_opt_keepidentity == tok.Bi_opt_newidentity {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BULK_INSERT_MISSING_IDENTITIY_OPTION, rsql.ERROR_BATCH_ABORT)
		}
	}

	// create current language token, needed to convert text field to DATE, TIME, DATETIME

	tok_syslanguage = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE) // ast.Create_TOK_PRIM_VARIABLE creates just a Token_primary
	tok_syslanguage.Tok_batch_line = tok.Tok_batch_line
	if rsql_err = decor.walk_Token_primary_and_simplify(tok_syslanguage); rsql_err != nil { // walk the operand
		return rsql_err
	}
	rsql.Assert(tok_syslanguage.Prim_dataslot.Datatype() == rsql.DATATYPE_SYSLANGUAGE)
	tok.Bi_syslanguage = tok_syslanguage

	return nil
}
