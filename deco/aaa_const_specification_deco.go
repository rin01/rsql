package deco

const (
	SPEC_DEFAULT_DICTIONARY_VARIABLE_CAPACITY = 10 // initial capacity 10 is good enough
	SPEC_DEFAULT_DICTIONARY_CONSTANT_CAPACITY = 50 // initial capacity 50 is good enough

	SPEC_FLASH_TBLID_START = -100 // first tblid for flash tables
)
