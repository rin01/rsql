package deco

import (
	"fmt"

	"rsql"
	"rsql/ast"
	"rsql/csr"
	"rsql/data"
)

func (decor *Decorator) wacf_Token_stmt_SELECT_or_UNION(tok *ast.Token_stmt_SELECT_or_UNION) *rsql.Error { // wacf means walk-constfold

	rsql.Assert(tok.Category() == ast.TOK_STMT_SELECT_OR_UNION)

	xtable := tok.Su_xtable
	rsql.Assert(xtable.Type() == ast.XT_SELECT || xtable.Type() == ast.XT_UNION)

	if rsql_err := decor.wacf_Xtable(xtable); rsql_err != nil {
		return rsql_err
	}

	// optimize SELECT tables

	optimizer := New_optimizer()

	if rsql_err := optimizer.Optimize_all(xtable); rsql_err != nil {
		return rsql_err
	}

	// discard ORDER BY if it is already the natural order

	switch xtable.Type() {
	case ast.XT_SELECT:
		var xtable_select *ast.Xtable_select
		var natural_order []rsql.IDataslot

		xtable_select = xtable.(*ast.Xtable_select)
		if xtable_select.Sel_grouping_flag == true { // for grouping SELECT, useless ORDER BY has already been discarded
			break
		}

		if len(xtable_select.Sel_orderby) == 0 { // if no ORDER BY, no check is necessary
			break
		}

		// discard useless ORDER BY for non-grouping SELECT

		natural_order = csr.Cursor_natural_order(xtable_select.Sel_cursor.From)

		if Select_ORDER_BY_is_naturally_ordered(xtable_select.Sel_orderby_dataslots, natural_order) == true {
			xtable_select.Sel_orderby = nil
			xtable_select.Sel_orderby_dataslots = nil
			xtable_select.Sel_cursor_orderby = nil

			if rsql.G_DEBUG_FLAG {
				fmt.Println("ORDER BY table is useless and has been discarded. Result is already ordered.")
			}
		}
	}

	return nil
}

func (decor *Decorator) wacf_Xtable_table(xtable_table *ast.Xtable_table) *rsql.Error {

	//println("------- START wacf_Xtable_table ------")
	//defer println("------- END wacf_Xtable_table ------")

	decor.Xtable_object_stack.Push(xtable_table)
	defer func() { decor.Xtable_object_stack.Pop() }()

	rsql.Assert(xtable_table.Tbl_gtabledef != nil)
	xtable_table.Tbl_cursor = csr.New_cursor_table(xtable_table.Tbl_gtabledef)

	return nil
}

func (decor *Decorator) wacf_Xtable_join(xtable_join *ast.Xtable_join) *rsql.Error {
	//println("------- START wacf_Xtable_join ------")
	//defer println("------- END wacf_Xtable_join ------")

	decor.Xtable_object_stack.Push(xtable_join)
	defer func() { decor.Xtable_object_stack.Pop() }()

	if rsql_err := decor.wacf_Xtable(xtable_join.Jn_right); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := decor.wacf_Xtable(xtable_join.Jn_left); rsql_err != nil {
		return rsql_err
	}

	// walk ON clause

	rsql.Assert(xtable_join.Jn_on != nil)

	if rsql_err := decor.Wacf_expression(xtable_join.Jn_on); rsql_err != nil {
		return rsql_err
	}

	if xtable_join.Jn_on.Prim_dataslot.Datatype() != rsql.DATATYPE_BOOLEAN {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BOOLEAN_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	// create cursor join

	xtable_join.Jn_cursor = csr.New_cursor_join(xtable_join.Jn_type, xtable_join.Jn_left.Cursor(), xtable_join.Jn_right.Cursor())
	xtable_join.Jn_cursor.On_basicblock = decor.create_new_Basicblock() // create a new basicblock for ON

	xtable_join.Jn_cursor.On_result = xtable_join.Jn_on.Prim_dataslot.(*data.BOOLEAN)

	return nil
}

func (decor *Decorator) wacf_Xtable_single(xtable_single *ast.Xtable_single) *rsql.Error {
	//println("------- START wacf_Xtable_single ------")
	//defer println("------- END wacf_Xtable_single ------")

	//decor.Xtable_object_stack.Push(xtable_single)
	//defer func() { decor.Xtable_object_stack.Pop() }()

	// create cursor single

	xtable_single.Sg_cursor = csr.New_cursor_single()

	return nil
}

func (decor *Decorator) wacf_Xtable_from(xtable_from *ast.Xtable_from) *rsql.Error {
	//println("------- START wacf_Xtable_from ------")
	//defer println("------- END wacf_Xtable_from ------")

	decor.Xtable_object_stack.Push(xtable_from)
	defer func() { decor.Xtable_object_stack.Pop() }()

	// walk FROM clause

	for _, xtable := range xtable_from.Fr_list {
		if rsql_err := decor.wacf_Xtable(xtable); rsql_err != nil {
			return rsql_err
		}
	}

	// walk WHERE clause

	if xtable_from.Fr_where != nil {
		if rsql_err := decor.Wacf_expression(xtable_from.Fr_where); rsql_err != nil {
			return rsql_err
		}

		if xtable_from.Fr_where.Prim_dataslot.Datatype() != rsql.DATATYPE_BOOLEAN {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BOOLEAN_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}
	}

	// rearrange series of xtables as JOIN tree

	var xtable_left ast.Xtable = xtable_from.Fr_list[0]

	for i := 1; i < len(xtable_from.Fr_list); i++ {
		xtable_right := xtable_from.Fr_list[i]
		xtable_join := ast.New_Xtable_join_with_cursor(csr.JT_INNER_JOIN, xtable_left, xtable_right, nil)
		xtable_join.Jn_cursor.On_basicblock = nil
		xtable_join.Jn_cursor.On_result = nil

		xtable_left = xtable_join
	}

	xtable_from.Fr_list = []ast.Xtable{xtable_left} // this list with one xtable replaces the old one

	// create cursor from

	xtable_from.Fr_cursor = csr.New_cursor_from(xtable_left.Cursor())
	xtable_from.Fr_cursor.Where_basicblock = decor.create_new_Basicblock() // create a new basicblock for WHERE

	if xtable_from.Fr_where != nil {
		xtable_from.Fr_cursor.Where_result = xtable_from.Fr_where.Prim_dataslot.(*data.BOOLEAN)
	}

	return nil
}

func (decor *Decorator) wacf_Xtable_select(xtable_select *ast.Xtable_select) *rsql.Error {
	var (
		rsql_err               *rsql.Error
		grouptable_xtable_from *ast.Xtable_from
		gtabledef_orderby      *rsql.GTabledef
		leaf_row_orderby       rsql.Row
		is_statement_level     bool
	)

	if decor.Xtable_object_stack.Count() == 0 {
		is_statement_level = true
	}

	//println("------- START wacf_Xtable_select ------")
	//defer println("------- END wacf_Xtable_select ------")

	decor.Xtable_object_stack.Push(xtable_select)
	defer func() { decor.Xtable_object_stack.Pop() }()

	if xtable_select.Sel_TOP_value >= 0 && len(decor.Xtable_object_stack) > 1 { // because ORDER BY is processed at statement level, and TOP must be processed after
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SELECT_TOP_NOT_AT_TOP_LEVEL, rsql.ERROR_BATCH_ABORT)
	}

	// walk FROM clause

	if rsql_err := decor.wacf_Xtable_from(xtable_select.Sel_from); rsql_err != nil {
		return rsql_err
	}

	// walk SELECT clause

	decor.Xtable_object_stack.Push(xtable_select.Sel_from)
	defer func() { decor.Xtable_object_stack.Pop() }()

	for _, column := range xtable_select.Sel_columns {
		if rsql_err := decor.Wacf_expression(column.Col_expression); rsql_err != nil {
			return rsql_err
		}

		if dt := column.Col_expression.Prim_dataslot.Datatype(); dt.Is_allowed_select_column() == false {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATATYPE_ILLEGAL_IN_SELECT, rsql.ERROR_BATCH_ABORT, dt.String())
		}
	}

	// if grouping SELECT, create a grouptable and modify the SELECT clause

	if xtable_select.Sel_grouping_flag == true {
		// walk GROUP BY clause

		for _, tok := range xtable_select.Sel_groupby_expressions { // dataslots belong to gs.insertion_row, which will be used for lookup and as insertion row into grouptable
			if rsql_err := decor.Wacf_expression(tok); rsql_err != nil {
				return rsql_err
			}

			if dt := tok.Prim_dataslot.Datatype(); dt.Is_allowed_select_column() == false {
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATATYPE_ILLEGAL_IN_GROUP_BY, rsql.ERROR_BATCH_ABORT, dt.String())
			}
		}

		// walk HAVING clause

		if xtable_select.Sel_grouptable_having != nil {
			if rsql_err := decor.Wacf_expression(xtable_select.Sel_grouptable_having); rsql_err != nil {
				return rsql_err
			}

			if xtable_select.Sel_grouptable_having.Prim_dataslot.Datatype() != rsql.DATATYPE_BOOLEAN {
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BOOLEAN_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}
		}

		// process ORDER BY (must be done before SELECT clause is modified and points to the grouptable)

		if len(xtable_select.Sel_orderby) > 0 {
			for _, odpart := range xtable_select.Sel_orderby {
				if rsql_err := decor.Wacf_expression(odpart.Odr_expression); rsql_err != nil {
					if !(odpart.Odr_expression.Prim_type == ast.TOK_PRIM_IDENTIFIER &&
						odpart.Odr_expression.Prim_database_name == "" &&
						odpart.Odr_expression.Prim_schema_name == "" &&
						rsql_err.Message_id == rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_NOT_FOUND) { // if column not found, it can be an alias name
						return rsql_err
					}
				}
			}

			leaf_row_orderby = create_leaf_row_orderby(xtable_select)

			if gtabledef_orderby, _, rsql_err = decor.process_ORDER_BY(xtable_select, leaf_row_orderby, xtable_select.Sel_orderby); rsql_err != nil {
				return rsql_err
			}

			xtable_select.Sel_cursor_orderby = csr.New_cursor_table_with_row(gtabledef_orderby, leaf_row_orderby)
		}

		// modify SELECT clause

		gs := &Group_splitup{decor: decor}

		if grouptable_xtable_from, rsql_err = gs.Process_GROUPING_SELECT(xtable_select, decor.Get_flashtable_new_tblid()); rsql_err != nil { // create Cursor_from on grouptable
			return rsql_err
		}

		grouptable_xtable_from.Fr_cursor.Where_basicblock = decor.create_new_Basicblock()
		if grouptable_xtable_from.Fr_where != nil {
			grouptable_xtable_from.Fr_cursor.Where_result = grouptable_xtable_from.Fr_where.Prim_dataslot.(*data.BOOLEAN)
		}

		// create FROM clause referencing grouptable

		xtable_select.Sel_grouptable_feeding_from = xtable_select.Sel_from
		xtable_select.Sel_from = grouptable_xtable_from

		xtable_select.Sel_grouptable_insertion_row = gs.grouptable_insertion_row
		xtable_select.Sel_grouptable_upsert_row = gs.grouptable_upsert_row
		xtable_select.Sel_grouptable_aggrfuncs_to_inject = gs.grouptable_aggrfuncs_to_inject
		xtable_select.Sel_grouptable_output_row = gs.grouptable_output_row
	}

	// create cursor

	dataslot_list := []rsql.IDataslot{}

	for _, column := range xtable_select.Sel_columns {
		dataslot_list = append(dataslot_list, column.Col_expression.Prim_dataslot)
	}

	xtable_select.Sel_cursor = csr.New_cursor_select(xtable_select.Sel_from.Fr_cursor, dataslot_list)
	xtable_select.Sel_cursor.Cols_basicblock = decor.create_new_Basicblock() // create a new basicblock for column expressions

	// cursor for grouping SELECT

	if xtable_select.Sel_grouping_flag == true {
		xtable_select.Sel_cursor.Grouping_flag = true
		xtable_select.Sel_cursor.Grouping_explicit_GROUP_BY = xtable_select.Sel_grouping_explicit_GROUP_BY
		xtable_select.Sel_cursor.Grouptable_gtabledef = xtable_select.Sel_grouptable_gtabledef

		xtable_select.Sel_cursor.Grouptable_nk_basicblock = decor.create_new_Basicblock()
		xtable_select.Sel_cursor.Grouptable_injection_basicblock = decor.create_new_Basicblock()

		xtable_select.Sel_grouptable_feeding_cursor_from = xtable_select.Sel_grouptable_feeding_from.Fr_cursor
		xtable_select.Sel_cursor.Grouptable_feeding_cursor = xtable_select.Sel_grouptable_feeding_from.Fr_cursor
		xtable_select.Sel_cursor.Grouptable_insertion_row = xtable_select.Sel_grouptable_insertion_row
		xtable_select.Sel_cursor.Grouptable_upsert_row = xtable_select.Sel_grouptable_upsert_row

		// discard ORDER BY if useless

		if len(xtable_select.Sel_orderby) > 0 {
			if orderby_is_same_as_grouping_order(xtable_select.Sel_grouptable_output_row, xtable_select.Sel_grouptable_gtabledef, xtable_select.Sel_columns, gtabledef_orderby) == true {
				xtable_select.Sel_orderby = nil
				xtable_select.Sel_orderby_dataslots = nil
				xtable_select.Sel_cursor_orderby = nil

				if rsql.G_DEBUG_FLAG {
					fmt.Println("ORDER BY table is useless and has been discarded. Grouping result is already ordered.")
				}
			}
		}
	}

	// process ORDER BY for non-grouping SELECT

	if xtable_select.Sel_grouping_flag == false && len(xtable_select.Sel_orderby) > 0 {
		for _, odpart := range xtable_select.Sel_orderby {
			if rsql_err := decor.Wacf_expression(odpart.Odr_expression); rsql_err != nil {
				if !(odpart.Odr_expression.Prim_type == ast.TOK_PRIM_IDENTIFIER &&
					odpart.Odr_expression.Prim_database_name == "" &&
					odpart.Odr_expression.Prim_schema_name == "" &&
					rsql_err.Message_id == rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_NOT_FOUND) { // if column not found, it can be an alias name
					return rsql_err
				}
			}
		}

		leaf_row_orderby = create_leaf_row_orderby(xtable_select)

		var columns_orderby []rsql.IDataslot
		if gtabledef_orderby, columns_orderby, rsql_err = decor.process_ORDER_BY(xtable_select, leaf_row_orderby, xtable_select.Sel_orderby); rsql_err != nil {
			return rsql_err
		}
		xtable_select.Sel_orderby_dataslots = columns_orderby // only used for comparison with natural order of the records, to discard ORDER BY if useless

		xtable_select.Sel_cursor_orderby = csr.New_cursor_table_with_row(gtabledef_orderby, leaf_row_orderby)
	}

	// walk @var = column in SELECT clause

	if len(xtable_select.Sel_varlist) > 0 {
		decor.Info_line = xtable_select.Sel_varlist[0].Tok_batch_line

		if is_statement_level == false {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SELECT_VARIABLE_NOT_STATEMENT_LEVEL, rsql.ERROR_BATCH_ABORT)
		}

		if len(xtable_select.Sel_varlist) != len(xtable_select.Sel_columns) {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SELECT_VARIABLE_COUNT_MISMATCH, rsql.ERROR_BATCH_ABORT)
		}

		list_cast_to_var := make([]*ast.Token_primary, 0, len(xtable_select.Sel_varlist))

		for i, tokvar := range xtable_select.Sel_varlist {
			var dataslot rsql.IDataslot
			var cast_token *ast.Token_primary
			var tok_dummy ast.Token_primary

			decor.Info_line = tokvar.Tok_batch_line
			rsql.Assert(tokvar.Prim_type == ast.TOK_PRIM_VARIABLE)

			if dataslot, rsql_err = decor.VARIABLES.Get_symbol_entry(tokvar.Prim_name); rsql_err != nil { // returns error if symbol not exists
				return rsql_err
			}

			tokvar.Prim_dataslot = dataslot

			// assignment of column value to variable

			column := xtable_select.Sel_columns[i]
			tok_dummy = *column.Col_expression
			switch len(xtable_select.Sel_orderby) {
			case 0:
				tok_dummy.Prim_dataslot = xtable_select.Sel_cursor.Cols_facade[column.Col_no]
			default: // ORDER BY exists
				tok_dummy.Prim_dataslot = leaf_row_orderby[column.Col_no+1]
			}

			if cast_token, rsql_err = decor.Token_primary_COPY_or_CAST_EXACT_new_to_target_dataslot(&tok_dummy, tokvar.Prim_dataslot); rsql_err != nil { // a CAST token is always returned, even if it is a plain copy
				return rsql_err
			}

			if cast_token.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
				cast_token.Prim_collation_strength = rsql.COLLSTRENGTH_0 // VARCHAR variables are always default collation
				cast_token.Prim_collation = decor.Server_default_collation
			}

			rsql.Assert(tok_dummy.Prim_dataslot != cast_token.Prim_dataslot) // target and source dataslots are always different
			rsql.Assert((cast_token.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR) != (cast_token.Prim_collation == ""))

			list_cast_to_var = append(list_cast_to_var, cast_token)
			xtable_select.Sel_list_cast_to_var = list_cast_to_var
		}

		xtable_select.Sel_cast_to_var_basicblock = decor.create_new_Basicblock()
	}

	return nil
}

func (decor *Decorator) wacf_Xtable_union(xtable_union *ast.Xtable_union) *rsql.Error {
	var (
		rsql_err     *rsql.Error
		col_count    int
		list_scratch []*ast.Token_primary
	)

	//println("------- START wacf_Xtable_union ------")
	//defer println("------- END wacf_Xtable_union ------")

	decor.Xtable_object_stack.Push(xtable_union)
	defer func() { decor.Xtable_object_stack.Pop() }()

	// walk all internal SELECTs and UNIONs

	for _, item := range xtable_union.Un_members {
		if rsql_err := decor.wacf_Xtable(item.Xtbl); rsql_err != nil {
			return rsql_err
		}

		xtable_col_count := item.Xtbl.Col_count()
		if col_count > 0 && xtable_col_count != col_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UNION_COL_COUNT_MISMATCH, rsql.ERROR_BATCH_ABORT)
		}

		col_count = xtable_col_count
	}

	// create equalized row

	for i := 0; i < col_count; i++ {
		var col_highest_datatype rsql.Datatype_t

		// compute target datatype for the column

		for _, item := range xtable_union.Un_members {
			xtable := item.Xtbl

			switch xtable.Type() {
			case ast.XT_SELECT:
				member_select := xtable.(*ast.Xtable_select)
				column := member_select.Sel_columns[i]

				dataslot := column.Col_expression.Prim_dataslot
				col_highest_datatype = compute_highest_datatype(col_highest_datatype, dataslot.Datatype())

			case ast.XT_UNION:
				member_union := xtable.(*ast.Xtable_union)
				tok_col := member_union.Un_equalized_row[i]

				dataslot := tok_col.Prim_dataslot
				col_highest_datatype = compute_highest_datatype(col_highest_datatype, dataslot.Datatype())

			default:
				panic("impossible")
			}
		}

		// equalize column to target datatype

		list_scratch = list_scratch[:0] // this list is only used to get dataslots properties (precision, scale, fixlen_flag), so that we can compute the target dataslot properties

		for _, item := range xtable_union.Un_members {
			var tok_target *ast.Token_primary

			xtable := item.Xtbl

			switch xtable.Type() {
			case ast.XT_SELECT:
				member_select := xtable.(*ast.Xtable_select)
				column := member_select.Sel_columns[i]
				tok_target = column.Col_expression

			case ast.XT_UNION:
				member_union := xtable.(*ast.Xtable_union)
				tok_target = member_union.Un_equalized_row[i]

			default:
				panic("impossible")
			}

			if tok_target.Prim_dataslot.Datatype() != col_highest_datatype {
				if tok_target, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot_no_simplify(tok_target, col_highest_datatype); rsql_err != nil {
					return rsql_err
				}
			}

			list_scratch = append(list_scratch, tok_target) // contains dataslots of type col_highest_datatype, for a given column, for all members. Precision, scale, fixlen_flag may be different
		}

		// find precision, scale, fixlen_flag for the column. Create the real dataslot in xtable_union.Un_equalized_row.

		var precision uint16
		var scale uint16
		var fixlen_flag bool

		switch col_highest_datatype {
		case rsql.DATATYPE_VARBINARY:
			precision = compute_varbinary_precision_largest(list_scratch...)

		case rsql.DATATYPE_VARCHAR:
			precision = compute_varchar_precision_largest(list_scratch...)
			fixlen_flag = compute_varchar_fixlen_flag(list_scratch...)

		case rsql.DATATYPE_NUMERIC:
			precision, scale = compute_numeric_precision_scale_encompassing(list_scratch...)
		}

		dataslot_equalized := data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, col_highest_datatype, precision, scale, fixlen_flag) // create dataslot for column i of UNION

		xtable_union.Un_equalized_row[i].Prim_dataslot = dataslot_equalized // Token_primary type is TOK_PRIM_IDENTIFIER
	}

	list_scratch = nil // we don't need it any more

	// create all CAST token to cast column datatypes to equalized_row datatypes

	for i := 0; i < col_count; i++ {
		for m, item := range xtable_union.Un_members {
			xtable := item.Xtbl

			var column_tok *ast.Token_primary
			var cast_token *ast.Token_primary

			switch xtable.Type() {
			case ast.XT_SELECT:
				member_select := xtable.(*ast.Xtable_select)
				column_tok = member_select.Sel_columns[i].Col_expression

			case ast.XT_UNION:
				member_union := xtable.(*ast.Xtable_union)
				column_tok = member_union.Un_equalized_row[i]

			default:
				panic("impossible")
			}

			if cast_token, rsql_err = decor.Token_primary_COPY_or_CAST_EXACT_new_to_target_dataslot(column_tok, xtable_union.Un_equalized_row[i].Prim_dataslot); rsql_err != nil { // a CAST token is always returned, even if it is a plain copy
				return rsql_err
			}

			rsql.Assert(column_tok.Prim_dataslot != cast_token.Prim_dataslot) // target and source dataslots are always different
			rsql.Assert((cast_token.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR) != (cast_token.Prim_collation == ""))

			item.Cast_to_equalized_row = append(item.Cast_to_equalized_row, cast_token)

			if m == 0 && xtable_union.Un_equalized_row[i].Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR { // if first member, set collation
				xtable_union.Un_equalized_row[i].Prim_collation_strength = rsql.COLLSTRENGTH_1
				switch cast_token.Prim_dataslot.Datatype() {
				case rsql.DATATYPE_VARCHAR:
					rsql.Assert(cast_token.Prim_collation != "")
					xtable_union.Un_equalized_row[i].Prim_collation = cast_token.Prim_collation

				default:
					xtable_union.Un_equalized_row[i].Prim_collation = decor.Server_default_collation
				}
			}
		}
	}

	// create cursor

	var list_cursors []csr.Cursor

	for _, item := range xtable_union.Un_members {
		xtable := item.Xtbl

		list_cursors = append(list_cursors, xtable.Cursor())
	}

	rsql.Assert(len(xtable_union.Un_equalized_row) == col_count)

	equalized_row_dataslots := make([]rsql.IDataslot, col_count)
	for k, tok := range xtable_union.Un_equalized_row {
		equalized_row_dataslots[k] = tok.Prim_dataslot
	}

	xtable_union.Un_cursor = csr.New_cursor_union(list_cursors, equalized_row_dataslots)

	xtable_union.Un_cursor.Equalize_basicblocks = make([]*rsql.Basicblock, len(xtable_union.Un_members))

	for i, _ := range xtable_union.Un_members {
		xtable_union.Un_cursor.Equalize_basicblocks[i] = decor.create_new_Basicblock() // create a new basicblock for each cursor, for equalizing code
	}

	// process ORDER BY

	if len(xtable_union.Un_orderby) > 0 {
		var gtabledef_orderby *rsql.GTabledef

		xtable_select_first := ast.Get_first_SELECT(xtable_union)
		decor.Xtable_object_stack.Push(xtable_select_first)          // push SELECT
		decor.Xtable_object_stack.Push(xtable_select_first.Sel_from) // push FROM

		for _, odpart := range xtable_union.Un_orderby {
			if rsql_err := decor.Wacf_expression(odpart.Odr_expression); rsql_err != nil {
				if !(odpart.Odr_expression.Prim_type == ast.TOK_PRIM_IDENTIFIER &&
					odpart.Odr_expression.Prim_database_name == "" &&
					odpart.Odr_expression.Prim_schema_name == "" &&
					rsql_err.Message_id == rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_NOT_FOUND) { // if column not found, it can be an alias name
					decor.Xtable_object_stack.Pop()
					decor.Xtable_object_stack.Pop()
					return rsql_err
				}
			}
		}

		decor.Xtable_object_stack.Pop() // pop FROM
		decor.Xtable_object_stack.Pop() // pop SELECT

		leaf_row_orderby := data.Clone_row_for_orderby(rsql.KIND_COL_LEAF, equalized_row_dataslots)

		if gtabledef_orderby, _, rsql_err = decor.process_ORDER_BY(xtable_select_first, leaf_row_orderby, xtable_union.Un_orderby); rsql_err != nil {
			return rsql_err
		}

		xtable_union.Un_cursor_orderby = csr.New_cursor_table_with_row(gtabledef_orderby, leaf_row_orderby)
	}

	return nil
}

func (decor *Decorator) wacf_Xtable(xtable ast.Xtable) *rsql.Error {

	switch xtable.Type() {
	case ast.XT_TABLE:
		return decor.wacf_Xtable_table(xtable.(*ast.Xtable_table))

	case ast.XT_JOIN:
		return decor.wacf_Xtable_join(xtable.(*ast.Xtable_join))

	case ast.XT_SINGLE:
		return decor.wacf_Xtable_single(xtable.(*ast.Xtable_single))

	case ast.XT_FROM:
		return decor.wacf_Xtable_from(xtable.(*ast.Xtable_from))

	case ast.XT_SELECT:
		return decor.wacf_Xtable_select(xtable.(*ast.Xtable_select))

	case ast.XT_UNION:
		return decor.wacf_Xtable_union(xtable.(*ast.Xtable_union))

	default:
		panic("impossible")
	}
}
