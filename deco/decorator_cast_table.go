package deco

import (
	"rsql"
)

// is_CAST_COPY returns true if instrcode is a CAST which only makes a copy of the dataslot value.
// This operation can be discarded by the decorator.
//
func Is_CAST_COPY(instrcode rsql.Instrcode_t) bool {

	if instrcode == rsql.INSTR_CAST_VOID_VOID ||
		instrcode == rsql.INSTR_CAST_SYSLANGUAGE_SYSLANGUAGE ||
		instrcode == rsql.INSTR_CAST_BIT_BIT ||
		instrcode == rsql.INSTR_CAST_TINYINT_TINYINT ||
		instrcode == rsql.INSTR_CAST_SMALLINT_SMALLINT ||
		instrcode == rsql.INSTR_CAST_INT_INT ||
		instrcode == rsql.INSTR_CAST_BIGINT_BIGINT ||
		instrcode == rsql.INSTR_CAST_MONEY_MONEY ||
		instrcode == rsql.INSTR_CAST_FLOAT_FLOAT ||
		instrcode == rsql.INSTR_CAST_DATE_DATE ||
		instrcode == rsql.INSTR_CAST_TIME_TIME ||
		instrcode == rsql.INSTR_CAST_DATETIME_DATETIME {
		return true
	}

	return false
}

// G_SQL_DATATYPE_CAST_INSTRUCTION_CODE is the CAST instruction table.
//
//    IMPLICIT CASTS are of two types :
//       - EQUALIZATION CAST :
//          - when an operator needs its operands to be of the same datatype, we often need to cast an operand to a higher rank datatype, to match the datatype of the other operand.
//          - when we need to cast arguments of a function to the datatype ( which can be higher or lower rank ) of the function parameters.
//          - we just care about the datatype, and ABSOLUTELY NOT about its properties ( precision, scale, fixlen_flag ).
//       - ASSIGNMENT CAST :
//          - when we need to cast rvalue to variable or column, e.g. SET @a = 123, or SET mycol = 123 in an UPDATE statement.
//          - the cast is done to a target which has the same datatype, but also EXACTLY THE SAME PROPERTIES ( precision, scale, fixlen_flag ) as the assignee.
//
//    EXPLICIT CASTS : corresponds to the instructions CAST(expression AS datatype), or CONVERT(datatype, expression) without any STYLE given.
//    If a style is given, the CAST instruction is transformed into a CONVERT(datatype, expression, style) instruction, which is a very ordinary builtin function for parsing or formatting.
//
//    The following casts are always illegal :
//       - any datatype to NULL ( rsql.DATATYPE_VOID ), as we cannot declare any variable of this type, nor is it the type of any operator arguments or function parameters.
//       - any datatype to rsql.DATATYPE_BOOLEAN. For instance, IF or CASE takes a boolean argument and we don't want expression such as 'IF 10' or 'CASE WHEN MYCOL THEN 'aaa' END'.
//       - rsql.DATATYPE_BOOLEAN to any datatype. We don't want to allow expressions like '(2=3) + 10'.
//
//    The cast of a datatype to same datatype, e.g. INT to INT, are allowed but the token will be discarded by the decorator from the AST as it is just a plain copy operation.
//
//    Some other same-datatype casts are real operation, such as NUMERIC to NUMERIC, or VARCHAR to VARCHAR, for instance, because precision, scale, fixlen_flag may be different.
//    This can only occur with assignment casts or explicit casts, not with equalization casts.
//    But if all these properties are the same, the token should be deleted from the ast as it is a no-operation.
//
var G_SQL_DATATYPE_CAST_INSTRUCTION_CODE = [rsql.DATATYPE_ARRAYSIZE][rsql.DATATYPE_ARRAYSIZE]rsql.Instrcode_t{

	rsql.DATATYPE_VOID: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_VOID_VOID, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_VOID_SYSLANGUAGE,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL, // e.g. NULL AND (1=1) is forbidden
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_VOID_VARBINARY,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_VOID_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_VOID_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_VOID_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_VOID_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_VOID_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_VOID_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_VOID_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_VOID_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_VOID_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_VOID_DATE,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_VOID_TIME,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_VOID_DATETIME,
	},

	rsql.DATATYPE_SYSLANGUAGE: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_SYSLANGUAGE_SYSLANGUAGE, // this instruction is never used, but needed in Token_primary_IMPLICIT_CAST_EXACT_new_with_Dataslot to allow assignment
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_BOOLEAN: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_VARBINARY: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_VARBINARY_VARBINARY, // used when precision of source and target is different.
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_VARBINARY_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_VARBINARY_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_VARBINARY_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_VARBINARY_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_VARBINARY_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_IMAGE: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_VARCHAR: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_VARCHAR_SYSLANGUAGE,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_VARCHAR_VARCHAR, // used when precision or fixlen_flag of source and target is different.
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL, // conversion from VARCHAR to REGEXPLIKE is made by the function INSTR_SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE, not by a cast.
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_VARCHAR_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_VARCHAR_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_VARCHAR_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_VARCHAR_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_VARCHAR_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_VARCHAR_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_VARCHAR_NUMERIC, // for implicit equalization cast, it is allowed by MS SQL Server but not by me. Too error prone for the user. See Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(). But it is allowed for implicit assignment cast and explicit cast.
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_VARCHAR_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_VARCHAR_DATE,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_VARCHAR_TIME,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_VARCHAR_DATETIME,
	},

	rsql.DATATYPE_TEXT: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_REGEXPLIKE: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_BIT: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_BIT_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_BIT_BIT, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_BIT_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_BIT_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_BIT_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_BIT_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_BIT_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_BIT_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_BIT_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_TINYINT: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_TINYINT_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_TINYINT_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_TINYINT_TINYINT, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_TINYINT_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_TINYINT_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_TINYINT_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_TINYINT_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_TINYINT_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_TINYINT_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_TINYINT_DATETIME,
	},

	rsql.DATATYPE_SMALLINT: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_SMALLINT_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_SMALLINT_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_SMALLINT_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_SMALLINT_SMALLINT, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_SMALLINT_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_SMALLINT_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_SMALLINT_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_SMALLINT_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_SMALLINT_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_SMALLINT_DATETIME,
	},

	rsql.DATATYPE_INT: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_INT_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_INT_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_INT_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_INT_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_INT_INT, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_INT_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_INT_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_INT_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_INT_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_INT_DATETIME,
	},

	rsql.DATATYPE_BIGINT: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_BIGINT_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_BIGINT_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_BIGINT_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_BIGINT_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_BIGINT_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_BIGINT_BIGINT, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_BIGINT_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_BIGINT_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_BIGINT_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_BIGINT_DATETIME,
	},

	rsql.DATATYPE_MONEY: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_MONEY_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_MONEY_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_MONEY_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_MONEY_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_MONEY_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_MONEY_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_MONEY_MONEY, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_MONEY_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_MONEY_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_NUMERIC: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_NUMERIC_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_NUMERIC_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_NUMERIC_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_NUMERIC_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_NUMERIC_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_NUMERIC_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_NUMERIC_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_NUMERIC_NUMERIC, // used when precision or scale of source and target are different.
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_NUMERIC_FLOAT,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_FLOAT: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_FLOAT_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_FLOAT_BIT,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_FLOAT_TINYINT,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_FLOAT_SMALLINT,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_FLOAT_INT,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_FLOAT_BIGINT,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_FLOAT_MONEY,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_FLOAT_NUMERIC,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_FLOAT_FLOAT, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_ILLEGAL,
	},

	rsql.DATATYPE_DATE: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_DATE_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_DATE_DATE, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_DATE_DATETIME,
	},

	rsql.DATATYPE_TIME: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_TIME_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_TIME_TIME, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_TIME_DATETIME,
	},

	rsql.DATATYPE_DATETIME: {
		rsql.DATATYPE_VOID:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SYSLANGUAGE: rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BOOLEAN:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARBINARY:   rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_IMAGE:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_VARCHAR:     rsql.INSTR_CAST_DATETIME_VARCHAR,
		rsql.DATATYPE_TEXT:        rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_REGEXPLIKE:  rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_TINYINT:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_SMALLINT:    rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_INT:         rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_BIGINT:      rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_MONEY:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_NUMERIC:     rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_FLOAT:       rsql.INSTR_CAST_ILLEGAL,
		rsql.DATATYPE_DATE:        rsql.INSTR_CAST_DATETIME_DATE,
		rsql.DATATYPE_TIME:        rsql.INSTR_CAST_DATETIME_TIME,
		rsql.DATATYPE_DATETIME:    rsql.INSTR_CAST_DATETIME_DATETIME, // can only be produced by an EXPLICIT CAST. It is just a copy operation.
	},
}
