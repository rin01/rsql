package deco

import (
	"rsql"
	"rsql/ast"
)

func (decor *Decorator) wacf_Token_stmt_ASSERT_(tok *ast.Token_stmt_ASSERT_) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_ASSERT_)

	if rsql_err = decor.Wacf_expression(tok.As_expr); rsql_err != nil {
		return rsql_err
	}

	if tok.As_expr.Prim_dataslot.Datatype() != rsql.DATATYPE_BOOLEAN { // expression must be BOOLEAN
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BOOLEAN_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	return nil
}

func (decor *Decorator) wacf_Token_stmt_ASSERT_NULL_(tok *ast.Token_stmt_ASSERT_NULL_) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_ASSERT_NULL_)

	// expression may be of any datatype

	if rsql_err = decor.Wacf_expression(tok.As_expr); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func (decor *Decorator) wacf_Token_stmt_ASSERT_ERROR_(tok *ast.Token_stmt_ASSERT_ERROR_) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_ASSERT_ERROR_)

	// expression may be of any datatype

	if rsql_err = decor.Wacf_expression(tok.As_expr); rsql_err != nil {
		return rsql_err
	}

	// error suffix

	if rsql_err = decor.Wacf_expression(tok.As_error_suffix); rsql_err != nil {
		return rsql_err
	}

	if tok.As_error_suffix.Prim_dataslot.Datatype() != rsql.DATATYPE_VARCHAR { // expression must be VARCHAR (fixlen_flag may be true of false)
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_VARCHAR_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	return nil
}
