package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
)

func (decor *Decorator) wacf_Token_stmt_DELETE(tok *ast.Token_stmt_DELETE) *rsql.Error { // wacf means walk-constfold
	var (
		target_table *ast.Xtable_table
		gtabledef    *rsql.GTabledef
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_DELETE)

	// walk FROM clause

	xtable_from := tok.De_from

	if rsql_err := decor.wacf_Xtable_from(xtable_from); rsql_err != nil {
		return rsql_err
	}

	// create Dataslots in target table

	target_table = tok.De_target_table
	gtabledef = target_table.Tbl_cursor.Gtabledef

	list_of_tabledefs := make([]*rsql.Tabledef, 0, 1+len(gtabledef.Indexmap))

	list_of_tabledefs = append(list_of_tabledefs, gtabledef.Base)
	for _, tabledef := range gtabledef.Indexmap {
		list_of_tabledefs = append(list_of_tabledefs, tabledef)
	}

	for _, tabledef := range list_of_tabledefs { // for base table and indexes
		for _, coldef := range tabledef.Td_nk { // add all columns making up native keys
			col_base_seqno := coldef.Cd_base_seqno

			dataslot := target_table.Tbl_cursor.Base_fields[col_base_seqno]

			if dataslot == nil {
				dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
				target_table.Tbl_cursor.Base_fields[col_base_seqno] = dataslot
			}

			rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)
		}
	}

	// optimize FROM tables

	optimizer := New_optimizer()

	if rsql_err := optimizer.Optimize_all(xtable_from); rsql_err != nil {
		return rsql_err
	}

	return nil
}
