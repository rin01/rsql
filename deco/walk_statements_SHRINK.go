package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/dict"
)

func (decor *Decorator) wacf_Token_stmt_SHRINK_TABLE(tok *ast.Token_stmt_SHRINK_TABLE) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err  *rsql.Error
		gtabledef *rsql.GTabledef
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_SHRINK_TABLE)

	// get GTabledef from dict

	if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(tok.Sk_table_qname); rsql_err != nil {
		return rsql_err
	}

	tok.Sk_gtabledef = gtabledef

	return nil
}
