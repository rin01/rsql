// Package deco contains the decorator of RSQL.
//
//     It walks the AST tree:
//         - it creates the dataslot for each Token_primary. See walk_Token_primary_and_simplify().
//         - it inserts promotion Token_primary (CAST token) for operator or function arguments, so that they have the proper datatype
//         - it inserts promotion Token_primary (CAST token) for assignment, to make expression exactly the same datatype and attributes as variable
//         - simplification for CAST(CAST ...) ...) is done. E.g. CAST(CAST(10.123 as INT) as INT) is simplified into CAST(10.123 as INT)
//         - constant folding is done
//
//     IMPORTANT THING TO UNDERSTAND:
//         - During decoration, lots of CAST tokens are usually inserted into the AST tree. Simplification may remove CAST tokens from the AST tree.
//         - During constant folding, the AST tree structure is absolutely static. Tokens are just transformed.
//         - Optimization is kept simple, to be made quickly. That's why there is no Common Subexpression Elimination (CSE). Besides, CSE would complicate the query optimizer too much.
//         - In the AST tree, when the VM will execute the SQL batch and fill the dataslots in the AST, all dataslots (except KIND_VAR_LEAF and KIND_COL_LEAF) are assigned a value once (they are immutable).
//         - The KIND_VAR_LEAF dataslots (in TOK_PRIM_VAR_NAMED tokens), represent variable, and their values are changed at each SET assignment (they are not immutable).
//         - The KIND_COL_LEAF dataslots are columns from table. They change at each cursor Next() call.
//         - The result dataslot never points to the same dataslot as an argument of an operator or function. This makes easier to write the package "data" which implements operators and functions.
//
package deco
