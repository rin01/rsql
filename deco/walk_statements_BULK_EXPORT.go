package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/csr"
	"rsql/data"
)

func (decor *Decorator) wacf_Token_stmt_BULK_EXPORT(tok *ast.Token_stmt_BULK_EXPORT) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_BULK_EXPORT)

	if rsql_err = decor.wacf_Xtable(tok.Be_xtable); rsql_err != nil { // XT_TABLE, XT_SELECT, or XT_UNION
		return rsql_err
	}

	if tok.Be_xtable.Type() == ast.XT_TABLE { // if XT_TABLE, we must create the dataslots
		cursor_table := tok.Be_xtable.Cursor().(*csr.Cursor_table)

		for i, coldef := range cursor_table.Base_tabledef.Td_coldefs {
			if coldef.Cd_autonum == rsql.CD_AUTONUM_ROWID { // don't write ROWID column to output file. The ROWID field has no dataslot.
				continue
			}

			dataslot := data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
			cursor_table.Base_fields[i] = dataslot

			rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)
		}
	}

	// optimize SELECT tables

	optimizer := New_optimizer()

	if rsql_err := optimizer.Optimize_all(tok.Be_xtable); rsql_err != nil {
		return rsql_err
	}

	return nil
}
