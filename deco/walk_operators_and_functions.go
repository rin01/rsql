package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
	"rsql/dict"
	"rsql/lex"
)

func MMAX16(a uint16, b uint16) uint16 {

	result := b

	if a > b {
		result = a
	}

	return result
}

// walk_Token_primary_operator walks an operator token and it children, in the AST tree.
//
// The following steps are processed :
//    - walk operands (it is recursive, so, all operand's subtree is walked)
//    - determine instruction code, and requested operand datype
//    - create promotion (CAST) node and insert it between current token and child operands if necessary
//    - if result datatype is NUMERIC, VARCHAR or VARBINARY, compute precision, scale, fixlen_flag
//    - for string operators like concatenation and string comparison, determine the collation for the VARCHAR result, and/or collation for the instruction
//    - add collator as argument if needed, for string comparison operators
//    - process current node and create Dataslot
//
func (decor *Decorator) walk_Token_primary_operator(tok *ast.Token_primary) *rsql.Error {

	type operand_normalization_mode_t uint // indicates how to cast operands, to make them of the type requested by the operator
	const (
		OPERAND_NORMALIZATION_ILLEGAL                           operand_normalization_mode_t = iota // illegal value
		OPERAND_NORMALIZATION_LEFT_OPERAND_DO_NOTHING                                               // only left operand is needed, as-is
		OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS                                          // most binary operators requires both operands to be of same datatype
		OPERAND_NORMALIZATION_EQUALIZE_IN_LIST                                                      // for IN operator
		OPERAND_NORMALIZATION_LIKE_VARCHAR                                                          // for LIKE operator
		OPERAND_NORMALIZATION_ADD_SUB_DATETIME_INT                                                  // for datetime '+' int operator, or datetime '-' int
		OPERAND_NORMALIZATION_ADD_DATE_TIME_DATETIME_DO_NOTHING                                     // for date '+' time operator
	)

	type properties_processing_t uint // if result is NUMERIC, VARCHAR or VARBINARY, we must determine precision, scale, fixlen_flag, collation of the result.
	const (
		DETERMINE_RESULT_NO_PROCESSING                       properties_processing_t = iota // default processing : do nothing
		DETERMINE_RESULT_NUMERIC_P_S_FOR_ONE_OPERAND                                        // for one operand operator
		DETERMINE_RESULT_NUMERIC_P_S_FOR_TWO_OPERANDS                                       // for two operands operator
		DETERMINE_RESULT_PRECISION_FOR_ADD_VARBINARY                                        // for VARBINARY + VARBINARY
		DETERMINE_RESULT_PRECISION_COLLATION_FOR_ADD_VARCHAR                                // for VARCHAR + VARCHAR
		DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR                                      // for =  >  < etc
		DETERMINE_OPERATION_COLLATION_FOR_LIKE_VARCHAR                                      // for LIKE, NOT LIKE
		DETERMINE_OPERATION_COLLATION_FOR_IN_LIST_VARCHAR                                   // for IN (...), NOT IN (...)
	)

	var (
		rsql_err            *rsql.Error
		tok_operand         *ast.Token_primary
		tok_promotion_token *ast.Token_primary
		i                   int

		left_datatype  rsql.Datatype_t
		right_datatype rsql.Datatype_t

		operand_datatype         rsql.Datatype_t
		operand_highest_datatype rsql.Datatype_t  // it is the highest rank datatype among the operands. Usually, it is this datatype that the operator wants to have for its operands.
		instruction_code         rsql.Instrcode_t // instruction code

		result_datatype           rsql.Datatype_t
		result_precision          uint16
		result_scale              uint16
		result_fixlen_flag        bool
		result_collation_strength rsql.Collstrength_t // collation of the VARCHAR result, which may be inherited from collation of operands.
		result_collation          string              //    same

		operand_normalization_mode operand_normalization_mode_t = OPERAND_NORMALIZATION_ILLEGAL  // how we want to put CAST token to have operands of the datatype requested by the operator.
		properties_processing      properties_processing_t      = DETERMINE_RESULT_NO_PROCESSING // default

		dataslot rsql.IDataslot

		instruction_collation string // collation used by string comparison operators
		tok_syscollator       *ast.Token_primary
	)

	/************* (1) walk left and right operands *************/

	// note: walk_Token_primary_and_simplify() may replace entirely the token with its child or grandchild, but the datatype remains exactly the same.

	switch tok.Prim_cardinality {
	case ast.TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND,
		ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL:
		rsql.Assert(tok.Prim_left != nil)
		rsql.Assert(tok.Prim_right == nil)

		if rsql_err = decor.walk_Token_primary_and_simplify(tok.Prim_left); rsql_err != nil {
			return rsql_err
		}
		left_datatype = tok.Prim_left.Prim_dataslot.Datatype()
		operand_highest_datatype = left_datatype

	case ast.TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS, // operators with two operands all need equalization of the operands
		ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE:
		rsql.Assert(tok.Prim_left != nil)
		rsql.Assert(tok.Prim_right != nil)

		if rsql_err = decor.walk_Token_primary_and_simplify(tok.Prim_left); rsql_err != nil {
			return rsql_err
		}
		if rsql_err = decor.walk_Token_primary_and_simplify(tok.Prim_right); rsql_err != nil {
			return rsql_err
		}
		left_datatype = tok.Prim_left.Prim_dataslot.Datatype()
		right_datatype = tok.Prim_right.Prim_dataslot.Datatype()
		operand_highest_datatype = compute_highest_datatype(left_datatype, right_datatype)

	case ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST:
		rsql.Assert(tok.Prim_left != nil)
		rsql.Assert(tok.Prim_right == nil)

		if rsql_err = decor.walk_Token_primary_and_simplify(tok.Prim_left); rsql_err != nil { // walk the left operand
			return rsql_err
		}
		left_datatype = tok.Prim_left.Prim_dataslot.Datatype()
		operand_highest_datatype = left_datatype // provisional default

		for _, tok_operand = range tok.Prim_listexpr { // list operands
			rsql.Assert(tok_operand != nil)

			if rsql_err = decor.walk_Token_primary_and_simplify(tok_operand); rsql_err != nil { // walk the list operands
				return rsql_err
			}
			operand_datatype = tok_operand.Prim_dataslot.Datatype()
			operand_highest_datatype = compute_highest_datatype(operand_highest_datatype, operand_datatype)
		}

	case ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL:
		// determiner le datatype du select TODO
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FEATURE_NOT_IMPLEMENTED_YET, rsql.ERROR_BATCH_ABORT)

	default:
		panic("Unknown prim_cardinality")
	}

	// Here, children have been walked and simplified.
	//
	// ***** Datatypes for operands have been resolved (also precision, scale, fixlen_flag, collation), but THEY HAVE NOT BEEN EQUALIZED YET (that is, cast to same datatype, or datatype requested by the operator) *****
	//
	// So, it is not possible yet to determine completely the properties of the result dataslot.
	// E.g. for numeric datatype target, it is not possible to compute <p,s> of the result, as it depends on the <p,s> of the operands. And this info is not available yet if an operand is INT, for instance.
	// Thus, we need to wait until the equalization CAST token is created and attached as operand.
	// But we can already determine the instruction code and the operator result datatype.

	/************* (2) determine instruction code *************/

	decor.Info_line = tok.Tok_batch_line // update line/pos for error message

	result_datatype = operand_highest_datatype // provisional default. For most operators, the result has the same datatype as its operands.

	switch tok.Prim_subtype {
	case ast.TOK_PRIM_OPERATOR_UNARY_PLUS:
		panic("unary plus should not exist any more in the ast tree")

	case ast.TOK_PRIM_OPERATOR_UNARY_MINUS:
		operand_normalization_mode = OPERAND_NORMALIZATION_LEFT_OPERAND_DO_NOTHING

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_UNARY_MINUS_VOID

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_UNARY_MINUS_TINYINT
			result_datatype = rsql.DATATYPE_SMALLINT // note that result is smallint.

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_UNARY_MINUS_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_UNARY_MINUS_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_UNARY_MINUS_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_UNARY_MINUS_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_UNARY_MINUS_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_ONE_OPERAND

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_UNARY_MINUS_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_PLUS:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_ADD_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_ADD_VARBINARY
			properties_processing = DETERMINE_RESULT_PRECISION_FOR_ADD_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_ADD_VARCHAR
			properties_processing = DETERMINE_RESULT_PRECISION_COLLATION_FOR_ADD_VARCHAR

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_ADD_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_ADD_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_ADD_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_ADD_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_ADD_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_ADD_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_TWO_OPERANDS

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_ADD_FLOAT

		case rsql.DATATYPE_TIME:
			if !(left_datatype == rsql.DATATYPE_DATE && right_datatype == rsql.DATATYPE_TIME) { // operands: (date, time)
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
			}
			operand_normalization_mode = OPERAND_NORMALIZATION_ADD_DATE_TIME_DATETIME_DO_NOTHING // operands are already of proper types
			instruction_code = rsql.INSTR_ADD_DATE_TIME_DATETIME
			result_datatype = rsql.DATATYPE_DATETIME

		case rsql.DATATYPE_DATETIME:
			operand_normalization_mode = OPERAND_NORMALIZATION_ADD_SUB_DATETIME_INT // operands: (datetime, int)
			instruction_code = rsql.INSTR_ADD_DATETIME_INT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_MINUS:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_SUBTRACT_VOID

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SUBTRACT_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SUBTRACT_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SUBTRACT_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SUBTRACT_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SUBTRACT_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SUBTRACT_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_TWO_OPERANDS

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SUBTRACT_FLOAT

		case rsql.DATATYPE_DATETIME:
			operand_normalization_mode = OPERAND_NORMALIZATION_ADD_SUB_DATETIME_INT // operands: (datetime, int)
			instruction_code = rsql.INSTR_SUBTRACT_DATETIME_INT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_MULT:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_MULT_VOID

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_MULT_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_MULT_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_MULT_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_MULT_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_MULT_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_MULT_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_TWO_OPERANDS

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_MULT_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_DIV:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_DIV_VOID

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_DIV_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_DIV_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_DIV_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_DIV_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_DIV_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_DIV_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_TWO_OPERANDS

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_DIV_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_MOD:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_MOD_VOID

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_MOD_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_MOD_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_MOD_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_MOD_BIGINT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_BIT_UNARY_NOT:
		operand_normalization_mode = OPERAND_NORMALIZATION_LEFT_OPERAND_DO_NOTHING

		switch operand_highest_datatype {
		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_BITWISE_UNARY_NOT_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_BITWISE_UNARY_NOT_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_BITWISE_UNARY_NOT_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_BITWISE_UNARY_NOT_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_BITWISE_UNARY_NOT_BIGINT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_BIT_AND:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_BITWISE_AND_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_BITWISE_AND_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_BITWISE_AND_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_BITWISE_AND_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_BITWISE_AND_BIGINT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_BIT_OR:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_BITWISE_OR_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_BITWISE_OR_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_BITWISE_OR_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_BITWISE_OR_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_BITWISE_OR_BIGINT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_BIT_XOR:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		switch operand_highest_datatype {
		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_BITWISE_XOR_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_BITWISE_XOR_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_BITWISE_XOR_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_BITWISE_XOR_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_BITWISE_XOR_BIGINT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_COMP_EQUAL:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_COMP_EQUAL_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_COMP_EQUAL_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_COMP_EQUAL_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_COMP_EQUAL_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_COMP_EQUAL_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_COMP_EQUAL_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_COMP_EQUAL_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_COMP_EQUAL_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_COMP_EQUAL_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_COMP_EQUAL_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_COMP_EQUAL_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_COMP_EQUAL_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_COMP_EQUAL_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_COMP_EQUAL_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_COMP_GREATER:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_COMP_GREATER_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_COMP_GREATER_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_COMP_GREATER_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_COMP_GREATER_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_COMP_GREATER_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_COMP_GREATER_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_COMP_GREATER_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_COMP_GREATER_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_COMP_GREATER_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_COMP_GREATER_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_COMP_GREATER_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_COMP_GREATER_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_COMP_GREATER_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_COMP_GREATER_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_COMP_LESS:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_COMP_LESS_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_COMP_LESS_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_COMP_LESS_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_COMP_LESS_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_COMP_LESS_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_COMP_LESS_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_COMP_LESS_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_COMP_LESS_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_COMP_LESS_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_COMP_LESS_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_COMP_LESS_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_COMP_LESS_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_COMP_LESS_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_COMP_LESS_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_COMP_GREATER_EQUAL_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_COMP_LESS_EQUAL:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_COMP_LESS_EQUAL_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_COMP_NOT_EQUAL:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_COMP_NOT_EQUAL_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_LOGICAL_UNARY_NOT:
		operand_normalization_mode = OPERAND_NORMALIZATION_LEFT_OPERAND_DO_NOTHING

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_BOOLEAN:
			instruction_code = rsql.INSTR_LOGICAL_UNARY_NOT_BOOLEAN

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_LOGICAL_AND:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_BOOLEAN:
			instruction_code = rsql.INSTR_LOGICAL_AND_BOOLEAN

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_LOGICAL_OR:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_BOOLEAN:
			instruction_code = rsql.INSTR_LOGICAL_OR_BOOLEAN

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_IS_NULL:
		operand_normalization_mode = OPERAND_NORMALIZATION_LEFT_OPERAND_DO_NOTHING

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_IS_NULL_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_IS_NULL_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_IS_NULL_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_IS_NULL_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_IS_NULL_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_IS_NULL_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_IS_NULL_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_IS_NULL_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_IS_NULL_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_IS_NULL_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_IS_NULL_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_IS_NULL_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_IS_NULL_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_IS_NULL_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_IS_NOT_NULL:
		operand_normalization_mode = OPERAND_NORMALIZATION_LEFT_OPERAND_DO_NOTHING

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_IS_NOT_NULL_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_IS_NOT_NULL_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_IS_NOT_NULL_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_IS_NOT_NULL_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_IS_NOT_NULL_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_IS_NOT_NULL_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_IS_NOT_NULL_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_IS_NOT_NULL_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_IS_NOT_NULL_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_IS_NOT_NULL_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_IS_NOT_NULL_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_IS_NOT_NULL_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_IS_NOT_NULL_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_IS_NOT_NULL_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_LIKE:
		operand_normalization_mode = OPERAND_NORMALIZATION_LIKE_VARCHAR // operands: (varchar, regexplike)

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_REGEXPLIKE:
			instruction_code = rsql.INSTR_LIKE_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_LIKE_VARCHAR

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_NOT_LIKE:
		operand_normalization_mode = OPERAND_NORMALIZATION_LIKE_VARCHAR // operands: (varchar, regexplike)

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_REGEXPLIKE:
			instruction_code = rsql.INSTR_NOT_LIKE_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_LIKE_VARCHAR

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_IN_LIST:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_IN_LIST

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_IN_LIST_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_IN_LIST_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_IN_LIST_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_IN_LIST_VARCHAR // collation from left operand

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_IN_LIST_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_IN_LIST_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_IN_LIST_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_IN_LIST_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_IN_LIST_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_IN_LIST_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_IN_LIST_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_IN_LIST_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_IN_LIST_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_IN_LIST_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_IN_LIST_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case ast.TOK_PRIM_OPERATOR_NOT_IN_LIST:
		operand_normalization_mode = OPERAND_NORMALIZATION_EQUALIZE_IN_LIST

		result_datatype = rsql.DATATYPE_BOOLEAN

		switch operand_highest_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_NOT_IN_LIST_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_NOT_IN_LIST_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_NOT_IN_LIST_VARCHAR
			properties_processing = DETERMINE_OPERATION_COLLATION_FOR_IN_LIST_VARCHAR // collation from left operand

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_NOT_IN_LIST_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_NOT_IN_LIST_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_NOT_IN_LIST_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_NOT_IN_LIST_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_NOT_IN_LIST_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_NOT_IN_LIST_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_NOT_IN_LIST_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_NOT_IN_LIST_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_NOT_IN_LIST_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_NOT_IN_LIST_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_NOT_IN_LIST_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	default:
		panic("operator or datatype unknown")
	}

	/************* (3) Create EQUALIZATION casting token and insert it between current token and children, if necessary *************/

	// here, operand_highest_datatype is a valid datatype that the operator needs for its operands. (there are exceptions, e.g. for LIKE or ADD datetime, where operand datatypes are different)
	//       result_datatype          is a valid datatype in which the operator will store the result.

	//   *** tok.Prim_left and tok.Prim_right HAVE ALREADY BEEN WALKED AND SIMPLIFIED ***

	switch operand_normalization_mode {
	case OPERAND_NORMALIZATION_LEFT_OPERAND_DO_NOTHING: // ====== unary minus, is null, etc. No CAST node is needed ======
		rsql.Assert(left_datatype == operand_highest_datatype)

	case OPERAND_NORMALIZATION_EQUALIZE_LEFT_RIGHT_OPERANDS: // ====== if left or right operand is not of the proper datatype, insert a CAST node ======
		switch {
		case left_datatype != operand_highest_datatype: // put CAST token on left child only
			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok.Prim_left, operand_highest_datatype); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}
			tok.Prim_left = tok_promotion_token

			rsql.Assert(right_datatype == operand_highest_datatype)

		case right_datatype != operand_highest_datatype: // put CAST token on right child only
			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok.Prim_right, operand_highest_datatype); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}
			tok.Prim_right = tok_promotion_token
		}

	case OPERAND_NORMALIZATION_EQUALIZE_IN_LIST: // ====== if left or listexpr operand is not of the proper datatype, insert a CAST node ======
		if left_datatype != operand_highest_datatype { // put CAST token on left child
			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok.Prim_left, operand_highest_datatype); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}
			tok.Prim_left = tok_promotion_token
		}

		for i, tok_operand = range tok.Prim_listexpr { // list operands
			if tok_operand.Prim_dataslot.Datatype() != operand_highest_datatype { // put CAST token on list arguments
				if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok_operand, operand_highest_datatype); rsql_err != nil { // returned token has been simplified
					return rsql_err
				}
				tok.Prim_listexpr[i] = tok_promotion_token
			}
		}

	case OPERAND_NORMALIZATION_LIKE_VARCHAR: // ====== for LIKE and NOT LIKE operator, first operand must be VARCHAR, second is REGEXPLIKE ======
		if left_datatype != rsql.DATATYPE_VARCHAR { // put CAST token on left child
			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok.Prim_left, rsql.DATATYPE_VARCHAR); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}
			tok.Prim_left = tok_promotion_token
		}

		if right_datatype != rsql.DATATYPE_REGEXPLIKE {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

	case OPERAND_NORMALIZATION_ADD_SUB_DATETIME_INT: // ====== for "datetime + integer" or "datetime - integer" operator, first operand must be DATETIME ======
		if left_datatype != rsql.DATATYPE_DATETIME {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_OPERATOR_OPERANDS, rsql.ERROR_BATCH_ABORT)
		}

		if right_datatype != rsql.DATATYPE_INT {
			if right_datatype != rsql.DATATYPE_TINYINT && right_datatype != rsql.DATATYPE_SMALLINT && right_datatype != rsql.DATATYPE_BIGINT { // second operand must be an integer
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATETIME_ADD_BAD_TYPE, rsql.ERROR_BATCH_ABORT)
			}

			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok.Prim_right, rsql.DATATYPE_INT); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}
			tok.Prim_right = tok_promotion_token
		}

	case OPERAND_NORMALIZATION_ADD_DATE_TIME_DATETIME_DO_NOTHING: // ====== for "date + time" operator, operators are already of proper types ======
		rsql.Assert(left_datatype == rsql.DATATYPE_DATE && right_datatype == rsql.DATATYPE_TIME && result_datatype == rsql.DATATYPE_DATETIME)

	default:
		panic("operand_normalization_mode unknown")
	}

	// here, operands have been equalized if the operator required it.

	/************* (4) If result datatype is NUMERIC, VARCHAR or VARBINARY, compute p, s, fixlen_flag, collation *************/

	result_precision = 0
	result_scale = 0
	result_fixlen_flag = false
	result_collation_strength = rsql.COLLSTRENGTH_0
	result_collation = ""
	instruction_collation = ""

	switch properties_processing { //  for NUMERIC, VARCHAR, VARBINARY result, determine precision, scale, collation. (fixlen_flag is always set to false)
	case DETERMINE_RESULT_NO_PROCESSING:
		// pass

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_ONE_OPERAND, // unary minus
		DETERMINE_RESULT_NUMERIC_P_S_FOR_TWO_OPERANDS: // addition, subtract, multiply, divide.
		result_precision, result_scale = compute_numeric_precision_scale(instruction_code, tok.Prim_left, tok.Prim_right) // also works if tok.Prim_right==nil (for unary minus)

	case DETERMINE_RESULT_PRECISION_FOR_ADD_VARBINARY:
		rsql.Assert(instruction_code == rsql.INSTR_ADD_VARBINARY)
		rsql.Assert(result_datatype == rsql.DATATYPE_VARBINARY)

		result_precision = compute_varbinary_precision_sum(tok.Prim_left, tok.Prim_right)

	case DETERMINE_RESULT_PRECISION_COLLATION_FOR_ADD_VARCHAR:
		rsql.Assert(instruction_code == rsql.INSTR_ADD_VARCHAR)
		rsql.Assert(result_datatype == rsql.DATATYPE_VARCHAR)

		result_fixlen_flag = false // even if two VARCHAR values with fixlen_flag==true (original datatype was CHAR) are concatenated, the result is VARCHAR with fixlen_flag==false

		result_precision = compute_varchar_precision_sum(tok.Prim_left, tok.Prim_right)

		if result_collation_strength, result_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_left, tok.Prim_right); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_OPERATION_COLLATION_FOR_COMP_VARCHAR: // =  >  <  >=  <=  <>
		rsql.Assert(result_datatype == rsql.DATATYPE_BOOLEAN)

		if _, instruction_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_left, tok.Prim_right); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_OPERATION_COLLATION_FOR_LIKE_VARCHAR: //  LIKE, NOT LIKE
		rsql.Assert(instruction_code == rsql.INSTR_LIKE_VARCHAR || instruction_code == rsql.INSTR_NOT_LIKE_VARCHAR)
		rsql.Assert(result_datatype == rsql.DATATYPE_BOOLEAN)

		if _, instruction_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_left, tok.Prim_right); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_OPERATION_COLLATION_FOR_IN_LIST_VARCHAR: // IN, NOT IN
		rsql.Assert(instruction_code == rsql.INSTR_IN_LIST_VARCHAR || instruction_code == rsql.INSTR_NOT_IN_LIST_VARCHAR)
		rsql.Assert(result_datatype == rsql.DATATYPE_BOOLEAN)

		instruction_collation = tok.Prim_left.Prim_collation

	default:
		panic("unknown numeric_ps_transform")
	}

	/************* (5) add collator as argument if needed, for string comparison operators *************/

	if instruction_collation != "" { // for string comparison operators, the collation is an operand for the instruction, not a property of the result.
		rsql.Assert(result_datatype == rsql.DATATYPE_BOOLEAN) // for the moment, all operators with instruction_collation have a boolean result

		tok_syscollator = decor.Create_TOK_PRIM_LITERAL_SYSCOLLATOR(instruction_collation) // ast.Create_TOK_PRIM_LITERAL_SYSCOLLATOR creates just a Token_primary
		tok_syscollator.Tok_batch_line = tok.Tok_batch_line
		tok_syscollator.Prim_dataslot = data.New_literal_SYSCOLLATOR(instruction_collation)

		tok.Prim_syscollator = tok_syscollator // add collator as argument
	}

	/************* (6) process current node and create Dataslot *************/

	// result_collation is used only if result is varchar, and must be non-nil. For other datatype, like int, result_collation must be nil (and result_collation_strength must be 0).

	rsql.Assert((result_datatype == rsql.DATATYPE_VARCHAR) != (result_collation == ""))

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, result_datatype, result_precision, result_scale, result_fixlen_flag)
	tok.Prim_dataslot = dataslot

	tok.Prim_collation_strength = result_collation_strength
	tok.Prim_collation = result_collation
	tok.Prim_instruction_code = instruction_code

	return nil
}

// walk_Token_primary_sysfunc walks sysfunc token and its arguments.
//
//     Sysfuncs can be grouped by the following families :
//        - functions that receive VARBINARY or VARCHAR.
//        - functions that receive INT, BIGINT, MONEY, FLOAT as argument.
//        - functions that receive NUMERIC.
//        - functions that receive DATE, TIME or DATETIME
//
func (decor *Decorator) walk_Token_primary_sysfunc(tok *ast.Token_primary) *rsql.Error {

	type properties_processing_t uint // if result is NUMERIC, VARCHAR or VARBINARY, we must define precision, scale, fixlen_flag, collation_strength, collation of the result.
	const (
		DETERMINE_RESULT_NO_PROCESSING properties_processing_t = iota // default processing : do nothing

		DETERMINE_RESULT_NUMERIC_P_S_FOR_ABS
		DETERMINE_RESULT_NUMERIC_P_S_FOR_CEILING_FLOOR
		DETERMINE_RESULT_NUMERIC_P_S_FOR_SIGN
		DETERMINE_RESULT_NUMERIC_P_S_FOR_POWER
		DETERMINE_RESULT_NUMERIC_P_S_FOR_ROUND
		DETERMINE_RESULT_NUMERIC_P_S_FOR_ISNULL
		DETERMINE_RESULT_NUMERIC_P_S_FOR_IIF
		DETERMINE_RESULT_NUMERIC_P_S_FOR_CHOOSE
		DETERMINE_RESULT_NUMERIC_P_S_FOR_COALESCE
		DETERMINE_RESULT_NUMERIC_P_S_FOR_SUM
		DETERMINE_RESULT_NUMERIC_P_S_FOR_MIN_MAX
		DETERMINE_RESULT_NUMERIC_P_S_FOR_RANDOM_NUMERIC

		DETERMINE_RESULT_REGEXPLIKE_COLLATION_FOR_COMPILE_VARCHAR_TO_REGEXPLIKE

		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatDATEFORMAT
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatLANGUAGE
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_STR
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CHAR_NCHAR
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TRIM
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_UPPER_LOWER
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SPACE
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_REPLICATE
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_LEFT_RIGHT
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_STUFF
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SUBSTRING
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CONCAT
		DETERMINE_RESOPER_VARCHAR_PRECISION_COLLATION_FOR_REPLACE // must define collation for operation and result
		DETERMINE_OPERATION_VARCHAR_COLLATION_FOR_CHARINDEX       // must define collation for operation
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_DATENAME
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_FORMAT
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_ISNULL
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_IIF
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CHOOSE
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_COALESCE
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TYPEOF
		DETERMINE_RESOPER_VARCHAR_PRECISION_COLLATION_FOR_MIN_MAX
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_RANDOM_VARCHAR

		DETERMINE_RESULT_VARBINARY_PRECISION_FOR_ISNULL
		DETERMINE_RESULT_VARBINARY_PRECISION_FOR_IIF
		DETERMINE_RESULT_VARBINARY_PRECISION_FOR_CHOOSE
		DETERMINE_RESULT_VARBINARY_PRECISION_FOR_COALESCE
		DETERMINE_RESULT_VARBINARY_PRECISION_FOR_MIN_MAX

		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SYSVAR_VARCHAR // for _@current_db, _@current_user, etc

		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatVERSION     // for @@version
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatSERVERNAME  // for @@servername
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatSERVICENAME // for @@servicename
	)

	var (
		rsql_err                     *rsql.Error
		tok_operand                  *ast.Token_primary // operand
		tok_promotion_token          *ast.Token_primary // equalization CAST node
		sysfunc_official_param_count int                // number of arguments that the function requires.
		i                            int

		original_operand_datatype  [ast.SPEC_FUNCTION_ARGUMENTS_COUNT_MAX]rsql.Datatype_t // datatypes of the operands before they have been equalized.
		requested_operand_datatype [ast.SPEC_FUNCTION_ARGUMENTS_COUNT_MAX]rsql.Datatype_t // datatypes that the function wants to have as operands.
		instruction_code           rsql.Instrcode_t

		result_datatype           rsql.Datatype_t
		result_precision          uint16
		result_scale              uint16
		result_fixlen_flag        bool
		result_collation_strength rsql.Collstrength_t // collation of the VARCHAR result, which may be inherited from operands, or be the server default collation.
		result_collation          string              //    same

		properties_processing properties_processing_t = DETERMINE_RESULT_NO_PROCESSING // default

		dataslot rsql.IDataslot // Dataslot of the function

		instruction_collation   string // collation used by functions doing string comparison
		append_syslanguage_flag bool
		tok_syscollator         *ast.Token_primary
		tok_syslanguage         *ast.Token_primary

		equalized_operands_datatype rsql.Datatype_t
	)

	/************* (1) walk all operands *************/

	// note: walk_Token_primary_and_simplify() may replace entirely the token with its child or grandchild, but the datatype remains exactly the same.

	if len(tok.Prim_listexpr) > ast.SPEC_FUNCTION_ARGUMENTS_COUNT_MAX { // should not happen as the ast parser has already checked this.
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FUNCTION_ARGUMENTS_COUNT_EXCEEDED, rsql.ERROR_BATCH_ABORT)
	}

	for i, tok_operand = range tok.Prim_listexpr { // tok.Prim_listexpr may increase later in this function, if optional argument are appended, like for ROUND()
		rsql.Assert(tok_operand != nil)

		if rsql_err = decor.walk_Token_primary_and_simplify(tok_operand); rsql_err != nil { // walk the operand
			return rsql_err
		}

		original_operand_datatype[i] = tok_operand.Prim_dataslot.Datatype() // get its datatype
	}

	// Here, children have been walked and simplified.
	//
	// ***** Datatypes for operands have been resolved (also precision, scale, fixlen_flag, collation), but THEY HAVE NOT BEEN EQUALIZED YET (cast to the datatype requested by the function) *****
	//
	// So, for numeric datatype for example, it is not possible to compute <p,s> of the result yet, as it depends on the <p,s> of the operands, which are not available if an operand is INT, for instance. E.g. for COALESCE.
	// Thus, we need to wait until the equalization casts are performed.
	// But we can already determine the instruction code and the result datatype.

	/************* (2) determine instruction code *************/

	switch tok.Prim_sysfunc_descr.Sd_id {
	case ast.SYSFUNC_atatDATEFIRST: // function @@DATEFIRST
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatDATEFIRST
		requested_operand_datatype[0] = rsql.DATATYPE_SYSLANGUAGE
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_atatDATEFORMAT: // function @@DATEFORMAT
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatDATEFORMAT
		requested_operand_datatype[0] = rsql.DATATYPE_SYSLANGUAGE
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatDATEFORMAT

	case ast.SYSFUNC_atatLANGUAGE: // function @@LANGUAGE
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatLANGUAGE
		requested_operand_datatype[0] = rsql.DATATYPE_SYSLANGUAGE
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatLANGUAGE

	case ast.SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE: // this internal function creates a SYSLANGUAGE with given first day of week
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE
		requested_operand_datatype[0] = rsql.DATATYPE_SYSLANGUAGE // SYSLANGUAGE value
		requested_operand_datatype[1] = rsql.DATATYPE_INT         // first day of week
		result_datatype = rsql.DATATYPE_SYSLANGUAGE

	case ast.SYSFUNC_DMY_SYSLANGUAGE: // this internal function creates a SYSLANGUAGE with given DMY
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_DMY_SYSLANGUAGE
		requested_operand_datatype[0] = rsql.DATATYPE_SYSLANGUAGE // SYSLANGUAGE value
		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR     // DMY
		result_datatype = rsql.DATATYPE_SYSLANGUAGE

	case ast.SYSFUNC_ABS:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID,
			rsql.DATATYPE_VARCHAR,
			rsql.DATATYPE_TINYINT,
			rsql.DATATYPE_SMALLINT,
			rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_ABS_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_ABS_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_ABS_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_ABS_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_ABS

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_ABS_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_CEILING:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID,
			rsql.DATATYPE_VARCHAR,
			rsql.DATATYPE_TINYINT,
			rsql.DATATYPE_SMALLINT,
			rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_CEILING_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_CEILING_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_CEILING_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_CEILING_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_CEILING_FLOOR

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_CEILING_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_FLOOR:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID,
			rsql.DATATYPE_VARCHAR,
			rsql.DATATYPE_TINYINT,
			rsql.DATATYPE_SMALLINT,
			rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_FLOOR_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_FLOOR_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_FLOOR_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_FLOOR_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_CEILING_FLOOR

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_FLOOR_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_SIGN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID,
			rsql.DATATYPE_VARCHAR,
			rsql.DATATYPE_TINYINT,
			rsql.DATATYPE_SMALLINT,
			rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_SIGN_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_SIGN_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_SIGN_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_SIGN_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_SIGN

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_SIGN_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_POWER:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		requested_operand_datatype[1] = rsql.DATATYPE_FLOAT // exponent is always FLOAT

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID,
			rsql.DATATYPE_VARCHAR,
			rsql.DATATYPE_TINYINT,
			rsql.DATATYPE_SMALLINT,
			rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_POWER_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_POWER_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_POWER_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_POWER_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_POWER

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_POWER_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_ROUND: // function with one optional parameter
		if len(tok.Prim_listexpr) == 2 {
			optional_parameter_token := decor.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(0)
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			optional_parameter_token.Prim_dataslot = data.New_literal_INT_value(0)

			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[2] = rsql.DATATYPE_INT
		}

		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_RANGE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 2, sysfunc_official_param_count)
		}

		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID,
			rsql.DATATYPE_VARCHAR,
			rsql.DATATYPE_TINYINT,
			rsql.DATATYPE_SMALLINT,
			rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_ROUND_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_ROUND_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_ROUND_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_ROUND_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_ROUND

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_ROUND_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_ACOS:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_ACOS_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_ASIN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_ASIN_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_ATAN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_ATAN_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_ATN2:
		sysfunc_official_param_count = 2 // 2 arguments

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_ATN2_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		requested_operand_datatype[1] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_COS:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_COS_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_SIN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SIN_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_TAN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_TAN_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_COT:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_COT_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_EXP:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_EXP_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_LOG:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_LOG_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_LOG10:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_LOG10_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_SQRT:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SQRT_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_SQUARE:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SQUARE_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_PI:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_PI_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_DEGREES:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_DEGREES_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_RADIANS:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RADIANS_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_RAND:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RAND_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE: // internal function
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_REGEXPLIKE

		properties_processing = DETERMINE_RESULT_REGEXPLIKE_COLLATION_FOR_COMPILE_VARCHAR_TO_REGEXPLIKE

	case ast.SYSFUNC_STR: // function with optional length parameter (default length is 10) and optional decimal parameter (default decimal is 0)
		if len(tok.Prim_listexpr) == 1 {
			optional_parameter_token := decor.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(10) // add token with constant '10' as length parameter.
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			optional_parameter_token.Prim_dataslot = data.New_literal_INT_value(10)

			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[1] = rsql.DATATYPE_INT
		}

		if len(tok.Prim_listexpr) == 2 {
			optional_parameter_token := decor.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(0) // add token with constant '0' as decimal parameter.
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			optional_parameter_token.Prim_dataslot = data.New_literal_INT_value(0)

			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[2] = rsql.DATATYPE_INT
		}

		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_RANGE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 1, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_STR_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_STR

	case ast.SYSFUNC_CHARLEN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_CHARLEN_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_LEN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_LEN_VARBINARY
			requested_operand_datatype[0] = rsql.DATATYPE_VARBINARY
			result_datatype = rsql.DATATYPE_INT

		default:
			instruction_code = rsql.INSTR_SYSFUNC_LEN_VARCHAR
			requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
			result_datatype = rsql.DATATYPE_INT
		}

	case ast.SYSFUNC_DATALENGTH:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_LEN_VARBINARY
			requested_operand_datatype[0] = rsql.DATATYPE_VARBINARY
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_CHARLEN_VARCHAR
			requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
			result_datatype = rsql.DATATYPE_INT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FUNCTION_DATALENGTH_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
		}

	case ast.SYSFUNC_ASCII:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_ASCII_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_UNICODE:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_UNICODE_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_CHAR:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_CHAR_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CHAR_NCHAR

	case ast.SYSFUNC_NCHAR:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_NCHAR_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CHAR_NCHAR

	case ast.SYSFUNC_LTRIM:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_LTRIM_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TRIM

	case ast.SYSFUNC_RTRIM:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RTRIM_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TRIM

	case ast.SYSFUNC_TRIM:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_TRIM_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TRIM

	case ast.SYSFUNC_LOWER:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_LOWER_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_UPPER_LOWER

	case ast.SYSFUNC_UPPER:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_UPPER_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_UPPER_LOWER

	case ast.SYSFUNC_SPACE:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SPACE_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SPACE

	case ast.SYSFUNC_REPLICATE:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_REPLICATE_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_REPLICATE

	case ast.SYSFUNC_LEFT:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_LEFT_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_LEFT_RIGHT

	case ast.SYSFUNC_RIGHT:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RIGHT_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_LEFT_RIGHT

	case ast.SYSFUNC_STUFF:
		sysfunc_official_param_count = 4

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_STUFF_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		requested_operand_datatype[3] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_STUFF

	case ast.SYSFUNC_SUBSTRING:
		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SUBSTRING_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SUBSTRING

	case ast.SYSFUNC_CONCAT:
		sysfunc_official_param_count = len(tok.Prim_listexpr)

		if len(tok.Prim_listexpr) < 2 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_MINIMAL, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 2)
		}

		instruction_code = rsql.INSTR_SYSFUNC_CONCAT_VARCHAR

		for i := range tok.Prim_listexpr {
			requested_operand_datatype[i] = rsql.DATATYPE_VARCHAR
		}

		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CONCAT

	case ast.SYSFUNC_REPLACE:
		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_REPLACE_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[2] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESOPER_VARCHAR_PRECISION_COLLATION_FOR_REPLACE

	case ast.SYSFUNC_CHARINDEX: // function with one optional parameter
		if len(tok.Prim_listexpr) == 2 {
			optional_parameter_token := decor.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(0)
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			optional_parameter_token.Prim_dataslot = data.New_literal_INT_value(0)

			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[2] = rsql.DATATYPE_INT
		}

		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_RANGE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 2, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_CHARINDEX_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_INT

		properties_processing = DETERMINE_OPERATION_VARCHAR_COLLATION_FOR_CHARINDEX

	case ast.SYSFUNC_DATEFROMPARTS:
		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_DATEFROMPARTS_DATE
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_DATE

	case ast.SYSFUNC_TIMEFROMPARTS:
		sysfunc_official_param_count = 5

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_TIMEFROMPARTS_TIME
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		requested_operand_datatype[3] = rsql.DATATYPE_INT
		requested_operand_datatype[4] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_TIME

	case ast.SYSFUNC_DATEADD: // DATEADD(datepart, number, date|time|datetime)
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[1] {
		case rsql.DATATYPE_DATE:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_YEAR_DATE

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_QUARTER_DATE

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MONTH_DATE

			case ast.DATEPARTFIELD_DAY, // day, dayofyear, weekday,
				ast.DATEPARTFIELD_DAYOFYEAR,
				ast.DATEPARTFIELD_WEEKDAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_DAY_DATE

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_WEEK_DATE

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATE")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_INT
			requested_operand_datatype[1] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_DATE

		case rsql.DATATYPE_TIME:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_HOUR_TIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MINUTE_TIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_SECOND_TIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MILLISECOND_TIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MICROSECOND_TIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_NANOSECOND_TIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "TIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_INT
			requested_operand_datatype[1] = rsql.DATATYPE_TIME
			result_datatype = rsql.DATATYPE_TIME

		default: // all other datatypes will be equalized to datetime.
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_YEAR_DATETIME

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_QUARTER_DATETIME

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MONTH_DATETIME

			case ast.DATEPARTFIELD_DAY, // day, dayofyear, weekday,
				ast.DATEPARTFIELD_DAYOFYEAR,
				ast.DATEPARTFIELD_WEEKDAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_DAY_DATETIME

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_WEEK_DATETIME

			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_HOUR_DATETIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MINUTE_DATETIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_SECOND_DATETIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MILLISECOND_DATETIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_MICROSECOND_DATETIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEADD_NANOSECOND_DATETIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATETIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_INT
			requested_operand_datatype[1] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_DATETIME
		}

	case ast.SYSFUNC_DATEDIFF:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch {
		case original_operand_datatype[0] == rsql.DATATYPE_DATE && original_operand_datatype[1] == rsql.DATATYPE_DATE:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_YEAR_DATE

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_QUARTER_DATE

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MONTH_DATE

			case ast.DATEPARTFIELD_DAY,
				ast.DATEPARTFIELD_DAYOFYEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_DAY_DATE

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_WEEK_DATE

			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_HOUR_DATE

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MINUTE_DATE

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_SECOND_DATE

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATE")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_DATE
			requested_operand_datatype[1] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_INT

		case original_operand_datatype[0] == rsql.DATATYPE_TIME && original_operand_datatype[1] == rsql.DATATYPE_TIME:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_HOUR_TIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MINUTE_TIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_SECOND_TIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MILLISECOND_TIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MICROSECOND_TIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_NANOSECOND_TIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "TIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_TIME
			requested_operand_datatype[1] = rsql.DATATYPE_TIME
			result_datatype = rsql.DATATYPE_INT

		default: // all other datatypes will be equalized to datetime.
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_YEAR_DATETIME

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_QUARTER_DATETIME

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MONTH_DATETIME

			case ast.DATEPARTFIELD_DAY,
				ast.DATEPARTFIELD_DAYOFYEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_DAY_DATETIME

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_WEEK_DATETIME

			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_HOUR_DATETIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MINUTE_DATETIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_SECOND_DATETIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MILLISECOND_DATETIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_MICROSECOND_DATETIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEDIFF_NANOSECOND_DATETIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATETIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
			requested_operand_datatype[1] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_INT
		}

	case ast.SYSFUNC_DATEPART:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_DATE:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_YEAR_DATE

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_QUARTER_DATE

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MONTH_DATE

			case ast.DATEPARTFIELD_DAYOFYEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_DAYOFYEAR_DATE

			case ast.DATEPARTFIELD_DAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_DAY_DATE

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_WEEK_DATE
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_WEEKDAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_WEEKDAY_DATE
				append_syslanguage_flag = true

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATE")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_TIME:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_HOUR_TIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MINUTE_TIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_SECOND_TIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MILLISECOND_TIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MICROSECOND_TIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_NANOSECOND_TIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "TIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_TIME
			result_datatype = rsql.DATATYPE_INT

		default: // all other datatypes will be equalized to datetime.
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_YEAR_DATETIME

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_QUARTER_DATETIME

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MONTH_DATETIME

			case ast.DATEPARTFIELD_DAYOFYEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_DAYOFYEAR_DATETIME

			case ast.DATEPARTFIELD_DAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_DAY_DATETIME

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_WEEK_DATETIME
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_WEEKDAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_WEEKDAY_DATETIME
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_HOUR_DATETIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MINUTE_DATETIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_SECOND_DATETIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MILLISECOND_DATETIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_MICROSECOND_DATETIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATEPART_NANOSECOND_DATETIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATETIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_INT
		}

	case ast.SYSFUNC_DATENAME:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_DATE:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_YEAR_DATE

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_QUARTER_DATE

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MONTH_DATE
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_DAYOFYEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_DAYOFYEAR_DATE

			case ast.DATEPARTFIELD_DAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_DAY_DATE

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_WEEK_DATE
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_WEEKDAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_WEEKDAY_DATE
				append_syslanguage_flag = true

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATE")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_VARCHAR

		case rsql.DATATYPE_TIME:
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_HOUR_TIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MINUTE_TIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_SECOND_TIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MILLISECOND_TIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MICROSECOND_TIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_NANOSECOND_TIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "TIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_TIME
			result_datatype = rsql.DATATYPE_VARCHAR

		default: // all other datatypes will be equalized to datetime.
			switch tok.Prim_sql_datepartfield {
			case ast.DATEPARTFIELD_YEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_YEAR_DATETIME

			case ast.DATEPARTFIELD_QUARTER:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_QUARTER_DATETIME

			case ast.DATEPARTFIELD_MONTH:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MONTH_DATETIME
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_DAYOFYEAR:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_DAYOFYEAR_DATETIME

			case ast.DATEPARTFIELD_DAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_DAY_DATETIME

			case ast.DATEPARTFIELD_WEEK:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_WEEK_DATETIME
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_WEEKDAY:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_WEEKDAY_DATETIME
				append_syslanguage_flag = true

			case ast.DATEPARTFIELD_HOUR:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_HOUR_DATETIME

			case ast.DATEPARTFIELD_MINUTE:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MINUTE_DATETIME

			case ast.DATEPARTFIELD_SECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_SECOND_DATETIME

			case ast.DATEPARTFIELD_MILLISECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MILLISECOND_DATETIME

			case ast.DATEPARTFIELD_MICROSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_MICROSECOND_DATETIME

			case ast.DATEPARTFIELD_NANOSECOND:
				instruction_code = rsql.INSTR_SYSFUNC_DATENAME_NANOSECOND_DATETIME

			default:
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATEPARTFIELD_NOT_SUPPORTED_FOR_TYPE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, tok.Prim_sql_datepartfield, "DATETIME")
			}

			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_VARCHAR
		}

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_DATENAME

	case ast.SYSFUNC_DATETIMEFROMPARTS:
		sysfunc_official_param_count = 7

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_DATETIMEFROMPARTS_DATETIME
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		requested_operand_datatype[3] = rsql.DATATYPE_INT
		requested_operand_datatype[4] = rsql.DATATYPE_INT
		requested_operand_datatype[5] = rsql.DATATYPE_INT
		requested_operand_datatype[6] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_DATETIME

	case ast.SYSFUNC_DATETIME2FROMPARTS:
		sysfunc_official_param_count = 8

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_DATETIME2FROMPARTS_DATETIME
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		requested_operand_datatype[2] = rsql.DATATYPE_INT
		requested_operand_datatype[3] = rsql.DATATYPE_INT
		requested_operand_datatype[4] = rsql.DATATYPE_INT
		requested_operand_datatype[5] = rsql.DATATYPE_INT
		requested_operand_datatype[6] = rsql.DATATYPE_INT
		requested_operand_datatype[7] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_DATETIME

	case ast.SYSFUNC_EOMONTH: // function with one optional parameter
		if len(tok.Prim_listexpr) == 1 {
			optional_parameter_token := decor.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(0)
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			optional_parameter_token.Prim_dataslot = data.New_literal_INT_value(0)

			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[1] = rsql.DATATYPE_INT
		}

		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_RANGE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 1, sysfunc_official_param_count)
		}

		requested_operand_datatype[1] = rsql.DATATYPE_INT

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_EOMONTH_DATE
			requested_operand_datatype[0] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_DATE

		default: // all other datatypes wil be equalized to datetime
			instruction_code = rsql.INSTR_SYSFUNC_EOMONTH_DATETIME
			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_DATETIME
		}

	case ast.SYSFUNC_BOMONTH: // function with one optional parameter
		if len(tok.Prim_listexpr) == 1 {
			optional_parameter_token := decor.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(0)
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			optional_parameter_token.Prim_dataslot = data.New_literal_INT_value(0)

			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[1] = rsql.DATATYPE_INT
		}

		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_RANGE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 1, sysfunc_official_param_count)
		}

		requested_operand_datatype[1] = rsql.DATATYPE_INT

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_BOMONTH_DATE
			requested_operand_datatype[0] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_DATE

		default: // all other datatypes wil be equalized to datetime
			instruction_code = rsql.INSTR_SYSFUNC_BOMONTH_DATETIME
			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_DATETIME
		}

	case ast.SYSFUNC_GETUTCDATE:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_GETUTCDATE_DATETIME
		result_datatype = rsql.DATATYPE_DATETIME

	case ast.SYSFUNC_UTC_TO_LOCAL:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		rsql.Assert(original_operand_datatype[0] == rsql.DATATYPE_DATETIME) // it is _@current_timestamp

		instruction_code = rsql.INSTR_SYSFUNC_UTC_TO_LOCAL_DATETIME
		requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
		result_datatype = rsql.DATATYPE_DATETIME

	case ast.SYSFUNC_FORMAT: // optional parameter SYSLANGUAGE
		if len(tok.Prim_listexpr) == 2 {
			optional_parameter_token := decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE)
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			if rsql_err = decor.walk_Token_primary_and_simplify(optional_parameter_token); rsql_err != nil { // walk the operand
				return rsql_err
			}
			rsql.Assert(optional_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_SYSLANGUAGE)
			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[2] = rsql.DATATYPE_SYSLANGUAGE
		}

		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_RANGE, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 2, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID,
			rsql.DATATYPE_VARCHAR,
			rsql.DATATYPE_TINYINT,
			rsql.DATATYPE_SMALLINT,
			rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_DATE
			requested_operand_datatype[0] = rsql.DATATYPE_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_TIME
			requested_operand_datatype[0] = rsql.DATATYPE_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_FORMAT_DATETIME
			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[2] = rsql.DATATYPE_SYSLANGUAGE
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_FORMAT

	case ast.SYSFUNC_ISNUMERIC:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_VARCHAR
			requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_BIT
			requested_operand_datatype[0] = rsql.DATATYPE_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_TINYINT
			requested_operand_datatype[0] = rsql.DATATYPE_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_SMALLINT
			requested_operand_datatype[0] = rsql.DATATYPE_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNUMERIC_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_ISDATE:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_ISDATE_VARCHAR
			requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
			append_syslanguage_flag = true

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_ISNULL: // the result datatype is same as first argument
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		equalized_operands_datatype = original_operand_datatype[0]
		if original_operand_datatype[0] == rsql.DATATYPE_VOID {
			equalized_operands_datatype = original_operand_datatype[1]
		}

		switch equalized_operands_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_VARBINARY
			properties_processing = DETERMINE_RESULT_VARBINARY_PRECISION_FOR_ISNULL

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_VARCHAR
			properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_ISNULL

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_ISNULL

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_ISNULL_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		requested_operand_datatype[0] = equalized_operands_datatype
		requested_operand_datatype[1] = equalized_operands_datatype
		result_datatype = equalized_operands_datatype

	case ast.SYSFUNC_IIF: // the result datatype is highest datatype from 2nd and 3rd arguments
		sysfunc_official_param_count = 3

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		equalized_operands_datatype = compute_highest_datatype(original_operand_datatype[1], original_operand_datatype[2]) // only for 2nd and 3rd operand, as 1st operand is always boolean

		switch equalized_operands_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_VARBINARY
			properties_processing = DETERMINE_RESULT_VARBINARY_PRECISION_FOR_IIF

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_VARCHAR
			properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_IIF

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_IIF

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_IIF_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		requested_operand_datatype[0] = rsql.DATATYPE_BOOLEAN
		requested_operand_datatype[1] = equalized_operands_datatype
		requested_operand_datatype[2] = equalized_operands_datatype
		result_datatype = equalized_operands_datatype

	case ast.SYSFUNC_CHOOSE:
		sysfunc_official_param_count = len(tok.Prim_listexpr) // note that first argument is an integer index

		if len(tok.Prim_listexpr) < 2 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_MINIMAL, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 2)
		}

		equalized_operands_datatype = 0 // find requested operand datatype, which is the datatype of highest rank of all the arguments, except the first which is the index
		for _, tok_operand = range tok.Prim_listexpr[1:] {
			equalized_operands_datatype = compute_highest_datatype(equalized_operands_datatype, tok_operand.Prim_dataslot.Datatype())
		}

		switch equalized_operands_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_VARBINARY
			properties_processing = DETERMINE_RESULT_VARBINARY_PRECISION_FOR_CHOOSE

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_VARCHAR
			properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CHOOSE

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_CHOOSE

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_CHOOSE_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		requested_operand_datatype[0] = rsql.DATATYPE_INT

		for i := 1; i < len(tok.Prim_listexpr); i++ {
			requested_operand_datatype[i] = equalized_operands_datatype
		}

		result_datatype = equalized_operands_datatype

	case ast.SYSFUNC_COALESCE:
		sysfunc_official_param_count = len(tok.Prim_listexpr)

		if len(tok.Prim_listexpr) < 1 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT_MINIMAL, rsql.ERROR_BATCH_ABORT, tok.Prim_name, 1)
		}

		equalized_operands_datatype = 0 // find requested operand datatype, which is the datatype of highest rank of all the arguments
		for _, tok_operand = range tok.Prim_listexpr {
			equalized_operands_datatype = compute_highest_datatype(equalized_operands_datatype, tok_operand.Prim_dataslot.Datatype())
		}

		switch equalized_operands_datatype {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_VOID

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_VARBINARY
			properties_processing = DETERMINE_RESULT_VARBINARY_PRECISION_FOR_COALESCE

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_VARCHAR
			properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_COALESCE

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_COALESCE

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_COALESCE_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		for i := range tok.Prim_listexpr {
			requested_operand_datatype[i] = equalized_operands_datatype
		}

		result_datatype = equalized_operands_datatype

	case ast.SYSFUNC_TYPEOF:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VOID:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_VOID

		case rsql.DATATYPE_BOOLEAN:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_BOOLEAN

		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_TYPEOF_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		requested_operand_datatype[0] = original_operand_datatype[0]
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TYPEOF

	case ast.SYSFUNC_RANDOM_VARCHAR:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RANDOM_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_RANDOM_VARCHAR

	case ast.SYSFUNC_RANDOM_INT:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RANDOM_INT
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_RANDOM_BIGINT:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RANDOM_BIGINT
		requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
		requested_operand_datatype[1] = rsql.DATATYPE_BIGINT
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_RANDOM_NUMERIC:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RANDOM_NUMERIC
		requested_operand_datatype[0] = rsql.DATATYPE_INT
		requested_operand_datatype[1] = rsql.DATATYPE_INT
		result_datatype = rsql.DATATYPE_NUMERIC

		properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_RANDOM_NUMERIC

	case ast.SYSFUNC_RANDOM_FLOAT:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RANDOM_FLOAT
		requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
		requested_operand_datatype[1] = rsql.DATATYPE_FLOAT
		result_datatype = rsql.DATATYPE_FLOAT

	case ast.SYSFUNC_RANDOM_DATE:
		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_RANDOM_DATE
		requested_operand_datatype[0] = rsql.DATATYPE_DATE
		requested_operand_datatype[1] = rsql.DATATYPE_DATE
		result_datatype = rsql.DATATYPE_DATE

	case ast.SYSFUNC_SUSER_NAME:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SUSER_NAME_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SYSVAR_VARCHAR

	case ast.SYSFUNC_SUSER_ID:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SUSER_ID_BIGINT
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_DB_NAME:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_DB_NAME_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SYSVAR_VARCHAR

	case ast.SYSFUNC_DB_ID:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_DB_ID_BIGINT
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_SCHEMA_NAME:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		db_id_parameter_token := decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_ID)
		db_id_parameter_token.Tok_batch_line = tok.Tok_batch_line
		if rsql_err = decor.walk_Token_primary_and_simplify(db_id_parameter_token); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(db_id_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_BIGINT)
		tok.Prim_listexpr = append(tok.Prim_listexpr, db_id_parameter_token)
		tok.Prim_listexpr[0], tok.Prim_listexpr[1] = tok.Prim_listexpr[1], tok.Prim_listexpr[0] // we want _@current_db_id to be the first operand, just because it looks nicer
		original_operand_datatype[0], original_operand_datatype[1] = rsql.DATATYPE_BIGINT, original_operand_datatype[0]
		sysfunc_official_param_count = 2

		instruction_code = rsql.INSTR_SYSFUNC_SCHEMA_NAME_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
		requested_operand_datatype[1] = rsql.DATATYPE_BIGINT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SYSVAR_VARCHAR

	case ast.SYSFUNC_SCHEMA_ID:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		db_id_parameter_token := decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_ID)
		db_id_parameter_token.Tok_batch_line = tok.Tok_batch_line
		if rsql_err = decor.walk_Token_primary_and_simplify(db_id_parameter_token); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(db_id_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_BIGINT)
		tok.Prim_listexpr = append(tok.Prim_listexpr, db_id_parameter_token)
		tok.Prim_listexpr[0], tok.Prim_listexpr[1] = tok.Prim_listexpr[1], tok.Prim_listexpr[0] // we want _@current_db_id to be the first operand, just because it looks nicer
		original_operand_datatype[0], original_operand_datatype[1] = rsql.DATATYPE_BIGINT, original_operand_datatype[0]
		sysfunc_official_param_count = 2

		instruction_code = rsql.INSTR_SYSFUNC_SCHEMA_ID_BIGINT
		requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_OBJECT_ID:
		if len(tok.Prim_listexpr) == 1 {
			optional_parameter_token := decor.Create_TOK_PRIM_LITERAL_STRING("U")
			optional_parameter_token.Tok_batch_line = tok.Tok_batch_line
			if rsql_err = decor.walk_Token_primary_and_simplify(optional_parameter_token); rsql_err != nil { // walk the operand
				return rsql_err
			}
			rsql.Assert(optional_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR)
			tok.Prim_listexpr = append(tok.Prim_listexpr, optional_parameter_token)
			original_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		}

		sysfunc_official_param_count = 2

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		db_name_parameter_token := decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_NAME)
		db_name_parameter_token.Tok_batch_line = tok.Tok_batch_line
		if rsql_err = decor.walk_Token_primary_and_simplify(db_name_parameter_token); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(db_name_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR)
		tok.Prim_listexpr = append(tok.Prim_listexpr, db_name_parameter_token)

		schema_name_parameter_token := decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_SCHEMA_NAME)
		schema_name_parameter_token.Tok_batch_line = tok.Tok_batch_line
		if rsql_err = decor.walk_Token_primary_and_simplify(schema_name_parameter_token); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(schema_name_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR)
		tok.Prim_listexpr = append(tok.Prim_listexpr, schema_name_parameter_token)

		tok.Prim_listexpr[0], tok.Prim_listexpr[1], tok.Prim_listexpr[2], tok.Prim_listexpr[3] = tok.Prim_listexpr[2], tok.Prim_listexpr[3], tok.Prim_listexpr[0], tok.Prim_listexpr[1] // we want _@current_db_id and _@current_schema_id to be the first operands, just because it looks nicer
		original_operand_datatype[0], original_operand_datatype[1], original_operand_datatype[2], original_operand_datatype[3] = rsql.DATATYPE_VARCHAR, rsql.DATATYPE_VARCHAR, original_operand_datatype[0], rsql.DATATYPE_VARCHAR
		sysfunc_official_param_count = 4

		instruction_code = rsql.INSTR_SYSFUNC_OBJECT_ID_BIGINT
		requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[2] = rsql.DATATYPE_VARCHAR
		requested_operand_datatype[3] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_USER_NAME:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		db_id_parameter_token := decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_ID)
		db_id_parameter_token.Tok_batch_line = tok.Tok_batch_line
		if rsql_err = decor.walk_Token_primary_and_simplify(db_id_parameter_token); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(db_id_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_BIGINT)
		tok.Prim_listexpr = append(tok.Prim_listexpr, db_id_parameter_token)
		tok.Prim_listexpr[0], tok.Prim_listexpr[1] = tok.Prim_listexpr[1], tok.Prim_listexpr[0] // we want _@current_db_id to be the first operand, just because it looks nicer
		original_operand_datatype[0], original_operand_datatype[1] = rsql.DATATYPE_BIGINT, original_operand_datatype[0]
		sysfunc_official_param_count = 2

		instruction_code = rsql.INSTR_SYSFUNC_USER_NAME_VARCHAR
		requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
		requested_operand_datatype[1] = rsql.DATATYPE_BIGINT
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SYSVAR_VARCHAR

	case ast.SYSFUNC_USER_ID:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		db_id_parameter_token := decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_ID)
		db_id_parameter_token.Tok_batch_line = tok.Tok_batch_line
		if rsql_err = decor.walk_Token_primary_and_simplify(db_id_parameter_token); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(db_id_parameter_token.Prim_dataslot.Datatype() == rsql.DATATYPE_BIGINT)
		tok.Prim_listexpr = append(tok.Prim_listexpr, db_id_parameter_token)
		tok.Prim_listexpr[0], tok.Prim_listexpr[1] = tok.Prim_listexpr[1], tok.Prim_listexpr[0] // we want _@current_db_id to be the first operand, just because it looks nicer
		original_operand_datatype[0], original_operand_datatype[1] = rsql.DATATYPE_BIGINT, original_operand_datatype[0]
		sysfunc_official_param_count = 2

		instruction_code = rsql.INSTR_SYSFUNC_USER_ID_BIGINT
		requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
		requested_operand_datatype[1] = rsql.DATATYPE_VARCHAR
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_atatVERSION:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatVERSION
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatVERSION

	case ast.SYSFUNC_atatSERVERNAME:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatSERVERNAME
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatSERVERNAME

	case ast.SYSFUNC_atatSERVICENAME:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatSERVICENAME
		result_datatype = rsql.DATATYPE_VARCHAR

		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatSERVICENAME

	case ast.SYSFUNC_atatIDENTITY:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatIDENTITY
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_atatROWCOUNT:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatROWCOUNT
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_atatERROR:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatERROR
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_atatTRANCOUNT:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_atatTRANCOUNT
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_SCOPE_IDENTITY:
		sysfunc_official_param_count = 0 // 0 argument

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		instruction_code = rsql.INSTR_SYSFUNC_SCOPE_IDENTITY
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_AGGR_COUNT:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		requested_operand_datatype[0] = original_operand_datatype[0]
		result_datatype = rsql.DATATYPE_INT

	case ast.SYSFUNC_AGGR_COUNT_BIG:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_VARBINARY

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_VARCHAR

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_NUMERIC

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_COUNT_BIG_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

		requested_operand_datatype[0] = original_operand_datatype[0]
		result_datatype = rsql.DATATYPE_BIGINT

	case ast.SYSFUNC_AGGR_SUM:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_BIT
			requested_operand_datatype[0] = rsql.DATATYPE_BIT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_TINYINT
			requested_operand_datatype[0] = rsql.DATATYPE_TINYINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_SMALLINT
			requested_operand_datatype[0] = rsql.DATATYPE_SMALLINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_SUM

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_SUM_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_AGGR_MIN:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_VARBINARY
			requested_operand_datatype[0] = rsql.DATATYPE_VARBINARY
			result_datatype = rsql.DATATYPE_VARBINARY
			properties_processing = DETERMINE_RESULT_VARBINARY_PRECISION_FOR_MIN_MAX

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_VARCHAR
			requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
			result_datatype = rsql.DATATYPE_VARCHAR
			properties_processing = DETERMINE_RESOPER_VARCHAR_PRECISION_COLLATION_FOR_MIN_MAX

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_BIT
			requested_operand_datatype[0] = rsql.DATATYPE_BIT
			result_datatype = rsql.DATATYPE_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_TINYINT
			requested_operand_datatype[0] = rsql.DATATYPE_TINYINT
			result_datatype = rsql.DATATYPE_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_SMALLINT
			requested_operand_datatype[0] = rsql.DATATYPE_SMALLINT
			result_datatype = rsql.DATATYPE_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_MIN_MAX

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_DATE
			requested_operand_datatype[0] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_TIME
			requested_operand_datatype[0] = rsql.DATATYPE_TIME
			result_datatype = rsql.DATATYPE_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_MIN_DATETIME
			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	case ast.SYSFUNC_AGGR_MAX:
		sysfunc_official_param_count = 1

		if len(tok.Prim_listexpr) != sysfunc_official_param_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok.Prim_name, sysfunc_official_param_count)
		}

		switch original_operand_datatype[0] {
		case rsql.DATATYPE_VARBINARY:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_VARBINARY
			requested_operand_datatype[0] = rsql.DATATYPE_VARBINARY
			result_datatype = rsql.DATATYPE_VARBINARY
			properties_processing = DETERMINE_RESULT_VARBINARY_PRECISION_FOR_MIN_MAX

		case rsql.DATATYPE_VARCHAR:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_VARCHAR
			requested_operand_datatype[0] = rsql.DATATYPE_VARCHAR
			result_datatype = rsql.DATATYPE_VARCHAR
			properties_processing = DETERMINE_RESOPER_VARCHAR_PRECISION_COLLATION_FOR_MIN_MAX

		case rsql.DATATYPE_BIT:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_BIT
			requested_operand_datatype[0] = rsql.DATATYPE_BIT
			result_datatype = rsql.DATATYPE_BIT

		case rsql.DATATYPE_TINYINT:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_TINYINT
			requested_operand_datatype[0] = rsql.DATATYPE_TINYINT
			result_datatype = rsql.DATATYPE_TINYINT

		case rsql.DATATYPE_SMALLINT:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_SMALLINT
			requested_operand_datatype[0] = rsql.DATATYPE_SMALLINT
			result_datatype = rsql.DATATYPE_SMALLINT

		case rsql.DATATYPE_INT:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_INT
			requested_operand_datatype[0] = rsql.DATATYPE_INT
			result_datatype = rsql.DATATYPE_INT

		case rsql.DATATYPE_BIGINT:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_BIGINT
			requested_operand_datatype[0] = rsql.DATATYPE_BIGINT
			result_datatype = rsql.DATATYPE_BIGINT

		case rsql.DATATYPE_MONEY:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_MONEY
			requested_operand_datatype[0] = rsql.DATATYPE_MONEY
			result_datatype = rsql.DATATYPE_MONEY

		case rsql.DATATYPE_NUMERIC:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_NUMERIC
			requested_operand_datatype[0] = rsql.DATATYPE_NUMERIC
			result_datatype = rsql.DATATYPE_NUMERIC
			properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_MIN_MAX

		case rsql.DATATYPE_FLOAT:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_FLOAT
			requested_operand_datatype[0] = rsql.DATATYPE_FLOAT
			result_datatype = rsql.DATATYPE_FLOAT

		case rsql.DATATYPE_DATE:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_DATE
			requested_operand_datatype[0] = rsql.DATATYPE_DATE
			result_datatype = rsql.DATATYPE_DATE

		case rsql.DATATYPE_TIME:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_TIME
			requested_operand_datatype[0] = rsql.DATATYPE_TIME
			result_datatype = rsql.DATATYPE_TIME

		case rsql.DATATYPE_DATETIME:
			instruction_code = rsql.INSTR_SYSFUNC_MAX_DATETIME
			requested_operand_datatype[0] = rsql.DATATYPE_DATETIME
			result_datatype = rsql.DATATYPE_DATETIME

		default:
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
		}

	default:
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_FUNCTION_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
	}

	/************* (3) Create promotion node and insert it between current token and children if necessary *************/

	// here, normalized_operand_datatype is a valid datatype
	//       result_datatype             is a valid datatype

	//   *** tok->prim_listexpr[i] HAVE ALREADY BEEN WALKED AND SIMPLIFIED ***

	for i := 0; i < len(tok.Prim_listexpr); i++ { // for all operands
		if original_operand_datatype[i] != requested_operand_datatype[i] { // create conversion token if necessary
			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(tok.Prim_listexpr[i], requested_operand_datatype[i]); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}

			tok.Prim_listexpr[i] = tok_promotion_token
		}
	}

	// here, operands have been equalized if the operator requires it.

	/************* (4) determine precision, scale, fixlen_flag, collation_strength and collation of result for NUMERIC, VARCHAR, VARBINARY datatypes *************/

	result_precision = 0
	result_scale = 0
	result_fixlen_flag = false
	result_collation_strength = rsql.COLLSTRENGTH_0
	result_collation = ""
	instruction_collation = ""

	switch properties_processing { //  for NUMERIC, VARCHAR, VARBINARY result, determine precision, scale, fixlen_flag, collation_strength, collation.
	case DETERMINE_RESULT_NO_PROCESSING:
		// do nothing

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_ABS: // for ABS(), we want the precision and scale of the result to be the same as the operand.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_precision
		result_scale = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_scale

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_CEILING_FLOOR: // for CEILING(), FLOOR(), we want the precision of the result to be integral part plus one more digit (as rounding may occur), and scale = 0.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_precision - tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_scale + 1
		if result_precision > rsql.DATATYPE_NUMERIC_PRECISION_MAX {
			result_precision = rsql.DATATYPE_NUMERIC_PRECISION_MAX
		}

		result_scale = 0

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_SIGN: // for SIGN(), we want <p,s> of the result to be <1,0>.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = 1
		result_scale = 0

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_POWER: // for POWER(), we want <p,s> of the result to be the maximum precision and scale.
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		result_precision = rsql.DATATYPE_NUMERIC_PRECISION_MAX
		result_scale = rsql.DATATYPE_NUMERIC_RESULT_SCALE_MAX

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_ROUND: // for ROUND(), we want the precision of the result to be one more digit if possible (as rounding occurs), and scale remains the same.
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_precision + 1
		if result_precision > rsql.DATATYPE_NUMERIC_PRECISION_MAX {
			result_precision = rsql.DATATYPE_NUMERIC_PRECISION_MAX
		}

		result_scale = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_scale

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_ISNULL: // for ISNULL(), we want <p,s> of the result to be able to receive any of the two operands. WARNING: THIS IS NOT MS SQL SERVER INSANE BEHAVIOUR (which forces result to be first operand <p,s>)
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		result_precision, result_scale = compute_numeric_precision_scale_encompassing(tok.Prim_listexpr[0], tok.Prim_listexpr[1])

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_IIF: // for IIF(), we want <p,s> of the result to be able to receive any of the two operands.
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision, result_scale = compute_numeric_precision_scale_encompassing(tok.Prim_listexpr[1], tok.Prim_listexpr[2])

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_CHOOSE: // for CHOOSE(), we want <p,s> of the result to encompass those of all operands, except first arg which is the index.

		result_precision, result_scale = compute_numeric_precision_scale_encompassing(tok.Prim_listexpr[1:]...)

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_COALESCE: // for COALESCE(), we want <p,s> of the result to encompass those of all operands.

		result_precision, result_scale = compute_numeric_precision_scale_encompassing(tok.Prim_listexpr...)

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_SUM:
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = rsql.DATATYPE_NUMERIC_PRECISION_MAX
		result_scale = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_scale

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_MIN_MAX: // for MAX() and MIN(), we want the precision and scale of the result to be the same as the operand.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_precision
		result_scale = tok.Prim_listexpr[0].Prim_dataslot.(*data.NUMERIC).Data_scale

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_RANDOM_NUMERIC:
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		var p = tok.Prim_listexpr[0].Prim_dataslot
		var s = tok.Prim_listexpr[1].Prim_dataslot

		if !(p.Kind() == rsql.KIND_RO_LEAF && p.Error() == nil && p.NULL_flag() == false) {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_RANDOM_NUMERIC_BAD_P_S, rsql.ERROR_BATCH_ABORT)
		}

		if !(s.Kind() == rsql.KIND_RO_LEAF && s.Error() == nil && s.NULL_flag() == false) {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_RANDOM_NUMERIC_BAD_P_S, rsql.ERROR_BATCH_ABORT)
		}

		var p64 = p.(*data.INT).Data_val()
		var s64 = s.(*data.INT).Data_val()

		if p64 > rsql.DATATYPE_NUMERIC_PRECISION_MAX { // 34 (note: in SQL Server, max precision is 38)
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION, rsql.ERROR_BATCH_ABORT, p64, rsql.DATATYPE_NUMERIC_PRECISION_MAX)
		}

		if !(p64 > 0 && s64 >= 0 && s64 <= p64) { // precision must be > 0, and scale must be <= precision
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION_OR_SCALE, rsql.ERROR_BATCH_ABORT)
		}

		result_precision = uint16(p64)
		result_scale = uint16(s64)

	case DETERMINE_RESULT_REGEXPLIKE_COLLATION_FOR_COMPILE_VARCHAR_TO_REGEXPLIKE:
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatDATEFORMAT: // for @@DATEFORMAT, precision = 3, collation is default.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = 3 // "DMY", "MDY", etc
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatLANGUAGE: // for @@LANGUAGE, precision = 12, collation is default.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = 12 // large enough for any language, which is of the form "fr-CH", "uz-Cyrl-UZ", etc
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_STR: // for STR(), precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is default
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX

		if val, ok := data.Get_value_from_literal_xxxINT(tok.Prim_listexpr[1].Prim_dataslot); ok == true { // if length argument is a valid integer literal, result precision may be more precisely defined.
			if val < int64(result_precision) && val >= 0 {
				result_precision = uint16(val)
				if result_precision == 0 {
					result_precision = 1
				}
			}
		}

		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CHAR_NCHAR: // for CHAR(), NCHAR(), precision = 1, collation is default.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = 1
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TRIM: // for LTRIM(), RTRIM(), TRIM(), precision and collation are same as argument.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.VARCHAR).Data_precision
		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_UPPER_LOWER: // for UPPER(), LOWER(), precision and collation are same as argument.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.VARCHAR).Data_precision
		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SPACE: // for SPACE(), precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is default
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX

		if val, ok := data.Get_value_from_literal_xxxINT(tok.Prim_listexpr[0].Prim_dataslot); ok == true { // if length argument is a integer literal, result precision may be more precisely defined.
			if val < int64(result_precision) && val >= 0 {
				result_precision = uint16(val)
				if result_precision == 0 {
					result_precision = 1
				}
			}
		}

		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_REPLICATE: // for REPLICATE(), precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is same as first argument.
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		result_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX

		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_LEFT_RIGHT: // for LEFT(), RIGHT(), precision and collation are same as first argument.
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.VARCHAR).Data_precision

		if val, ok := data.Get_value_from_literal_xxxINT(tok.Prim_listexpr[1].Prim_dataslot); ok == true { // if length argument is a integer literal, result precision may be more precisely defined.
			if val < int64(result_precision) && val >= 0 {
				result_precision = uint16(val)
				if result_precision == 0 {
					result_precision = 1
				}
			}
		}

		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_STUFF: // for STUFF(), precision is sum of first argument string and replacement string. Collation is collation of first argument.
		rsql.Assert(len(tok.Prim_listexpr) == 4)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.VARCHAR).Data_precision + tok.Prim_listexpr[3].Prim_dataslot.(*data.VARCHAR).Data_precision + 2 // + 2 just to be sure, in case normalization adds some more runes
		if result_precision > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
			result_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX
		}

		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SUBSTRING: // for SUBSTRING(), precision and collation are same as first argument.
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.VARCHAR).Data_precision

		if val, ok := data.Get_value_from_literal_xxxINT(tok.Prim_listexpr[2].Prim_dataslot); ok == true { // if length argument is a integer literal, result precision may be more precisely defined.
			if val < int64(result_precision) && val >= 0 {
				result_precision = uint16(val)
				if result_precision == 0 {
					result_precision = 1
				}
			}
		}

		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CONCAT: // for CONCAT(), precision is sum and collation of the result encompass those of all operands.

		result_precision = compute_varchar_precision_sum(tok.Prim_listexpr...) + 2 // + 2 just to be sure, in case normalization adds some more runes.

		if result_collation_strength, result_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_listexpr...); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_RESOPER_VARCHAR_PRECISION_COLLATION_FOR_REPLACE: // for REPLACE(), precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is same as first argument, instruction collation encompasses those of the two first arguments.
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX
		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

		if _, instruction_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_listexpr[0], tok.Prim_listexpr[1]); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_OPERATION_VARCHAR_COLLATION_FOR_CHARINDEX: // for CHARINDEX(), instruction collation encompasses those of both arguments.
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		if _, instruction_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_listexpr[0], tok.Prim_listexpr[1]); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_DATENAME: // for DATENAME(), precision = 60, collation is default
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = 60 // arbitrary value, large enough for any datename
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_FORMAT: // for FORMAT(), precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is default
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_ISNULL: // for ISNULL(), precision is the max of both argument, collation is same as first argument. WARNING: THIS IS NOT MS SQL SERVER BEHAVIOUR (which forces result to be first operand precision)
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		result_precision = compute_varchar_precision_largest(tok.Prim_listexpr[0], tok.Prim_listexpr[1])

		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength // collation comes from first argument only
		result_collation = tok.Prim_listexpr[0].Prim_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_IIF: // for IIF(), precision is the max of both argument, collation comes from both arguments
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision = compute_varchar_precision_largest(tok.Prim_listexpr[1], tok.Prim_listexpr[2])

		if result_collation_strength, result_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_listexpr[1], tok.Prim_listexpr[2]); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CHOOSE: // for CHOOSE(), precision and collation of the result encompass those of all operands.

		result_precision = compute_varchar_precision_largest(tok.Prim_listexpr[1:]...)

		if result_collation_strength, result_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_listexpr[1:]...); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_COALESCE: // for COALESCE(), precision and collation of the result encompass those of all operands.

		result_precision = compute_varchar_precision_largest(tok.Prim_listexpr...)

		if result_collation_strength, result_collation, rsql_err = decor.compute_varchar_collation_encompassing(tok.Prim_listexpr...); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_RANDOM_VARCHAR:
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		var len_min = tok.Prim_listexpr[0].Prim_dataslot
		var len_max = tok.Prim_listexpr[1].Prim_dataslot

		if !(len_min.Kind() == rsql.KIND_RO_LEAF && len_min.Error() == nil && len_min.NULL_flag() == false) {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_RANDOM_VARCHAR_BAD_LEN_MIN_MAX, rsql.ERROR_BATCH_ABORT)
		}

		if !(len_max.Kind() == rsql.KIND_RO_LEAF && len_max.Error() == nil && len_max.NULL_flag() == false) {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_RANDOM_VARCHAR_BAD_LEN_MIN_MAX, rsql.ERROR_BATCH_ABORT)
		}

		var len_min64 = len_min.(*data.INT).Data_val()
		var len_max64 = len_max.(*data.INT).Data_val()

		if len_max64 > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION, rsql.ERROR_BATCH_ABORT, len_max64, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
		}

		if !(len_max64 > 0 && len_min64 > 0 && len_min64 <= len_max64) {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_RANDOM_VARCHAR_BAD_LEN_MIN_MAX, rsql.ERROR_BATCH_ABORT)
		}

		result_precision = uint16(len_max64)
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARBINARY_PRECISION_FOR_ISNULL: // for ISNULL(), precision is the max of both arguments. WARNING: THIS IS NOT MS SQL SERVER BEHAVIOUR (which forces result to be first operand datatype)
		rsql.Assert(len(tok.Prim_listexpr) == 2)

		result_precision = compute_varbinary_precision_largest(tok.Prim_listexpr[0], tok.Prim_listexpr[1])

	case DETERMINE_RESULT_VARBINARY_PRECISION_FOR_IIF: // for IIF(), precision is the max of both arguments.
		rsql.Assert(len(tok.Prim_listexpr) == 3)

		result_precision = compute_varbinary_precision_largest(tok.Prim_listexpr[1], tok.Prim_listexpr[2])

	case DETERMINE_RESULT_VARBINARY_PRECISION_FOR_CHOOSE: // for CHOOSE(), precision is the max of all operands.

		result_precision = compute_varbinary_precision_largest(tok.Prim_listexpr[1:]...)

	case DETERMINE_RESULT_VARBINARY_PRECISION_FOR_COALESCE: // for COALESCE(), precision is the max of all operands.

		result_precision = compute_varbinary_precision_largest(tok.Prim_listexpr...)

	case DETERMINE_RESULT_VARBINARY_PRECISION_FOR_MIN_MAX: // for MIN() and MAX(), precision is same as operand.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.VARBINARY).Data_precision

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_SYSVAR_VARCHAR:

		result_precision = data.SYSVAR_VARCHAR_PRECISION_MAX
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatVERSION: // for @@VERSION, collation is default.
		rsql.Assert(len(tok.Prim_listexpr) == 0)

		result_precision = uint16(len(rsql.VERSION))
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatSERVERNAME: // for @@SERVERNAME, collation is default.
		rsql.Assert(len(tok.Prim_listexpr) == 0)

		result_precision = dict.MAX_SERVERNAME_RUNE_LENGTH
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_atatSERVICENAME: // for @@SERVICENAME, collation is default.
		rsql.Assert(len(tok.Prim_listexpr) == 0)

		result_precision = dict.MAX_SERVERNAME_RUNE_LENGTH // @@servicename is a substring of @@servername
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_TYPEOF:

		result_precision = 30 // longest strings are "varbinary(1000)" and "numeric(10, 10)". 30 is more than enough.
		result_collation_strength = rsql.COLLSTRENGTH_0
		result_collation = decor.Server_default_collation

	case DETERMINE_RESOPER_VARCHAR_PRECISION_COLLATION_FOR_MIN_MAX: // for MIN() and MAX(), precision is same as operand.
		rsql.Assert(len(tok.Prim_listexpr) == 1)

		result_precision = tok.Prim_listexpr[0].Prim_dataslot.(*data.VARCHAR).Data_precision
		result_collation_strength = tok.Prim_listexpr[0].Prim_collation_strength
		result_collation = tok.Prim_listexpr[0].Prim_collation

		instruction_collation = tok.Prim_listexpr[0].Prim_collation

	default:
		panic("unknown determine")
	}

	/************* (5) add syscollator or syslanguage as argument if needed *************/

	if instruction_collation != "" {
		tok_syscollator = decor.Create_TOK_PRIM_LITERAL_SYSCOLLATOR(instruction_collation) // ast.Create_TOK_PRIM_LITERAL_SYSCOLLATOR creates just a Token_primary
		tok_syscollator.Tok_batch_line = tok.Tok_batch_line
		tok_syscollator.Prim_dataslot = data.New_literal_SYSCOLLATOR(instruction_collation)

		tok.Prim_syscollator = tok_syscollator // add collator as argument
	}

	if append_syslanguage_flag {
		tok_syslanguage = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE) // ast.Create_TOK_PRIM_VARIABLE creates just a Token_primary
		tok_syslanguage.Tok_batch_line = tok.Tok_batch_line
		if rsql_err = decor.walk_Token_primary_and_simplify(tok_syslanguage); rsql_err != nil { // walk the operand
			return rsql_err
		}
		rsql.Assert(tok_syslanguage.Prim_dataslot.Datatype() == rsql.DATATYPE_SYSLANGUAGE)
		tok.Prim_syslanguage = tok_syslanguage // add syslanguage as argument
	}

	/************* (6) process current node and create Dataslot *************/

	// result_collation is used only if result is varchar or regexplike, and must be non-nil. For other datatype, like int, result_collation must be nil (and result_collation_strength must be 0).

	rsql.Assert((result_datatype == rsql.DATATYPE_VARCHAR || result_datatype == rsql.DATATYPE_REGEXPLIKE) != (result_collation == ""))

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, result_datatype, result_precision, result_scale, result_fixlen_flag)
	tok.Prim_dataslot = dataslot

	tok.Prim_collation_strength = result_collation_strength
	tok.Prim_collation = result_collation
	tok.Prim_instruction_code = instruction_code

	return nil
}

// walk_Token_primary_CASE walks CASE token and its arguments.
//
func (decor *Decorator) walk_Token_primary_CASE(tok *ast.Token_primary) *rsql.Error {

	type properties_processing_t uint // if result is NUMERIC, VARCHAR or VARBINARY, we must define precision, scale, fixlen_flag, collation_strength, collation of the result.
	const (
		DETERMINE_RESULT_NO_PROCESSING properties_processing_t = iota // default processing : do nothing

		DETERMINE_RESULT_NUMERIC_P_S_FOR_CASE
		DETERMINE_RESULT_VARBINARY_PRECISION_FOR_CASE
		DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CASE
	)

	var (
		rsql_err            *rsql.Error
		listexpr            []*ast.Token_primary
		tok_when            *ast.Token_primary
		tok_then            *ast.Token_primary
		tok_promotion_token *ast.Token_primary
		listexpr_when       []*ast.Token_primary
		listexpr_then       []*ast.Token_primary

		equalized_operands_datatype rsql.Datatype_t
		instruction_code            rsql.Instrcode_t

		result_datatype           rsql.Datatype_t
		result_precision          uint16
		result_scale              uint16
		result_fixlen_flag        bool
		result_collation_strength rsql.Collstrength_t // collation of the VARCHAR result, which may be inherited from operands, or be the server default collation.
		result_collation          string              //    same

		properties_processing properties_processing_t = DETERMINE_RESULT_NO_PROCESSING // default

		dataslot rsql.IDataslot // Dataslot of the function
	)

	listexpr = tok.Prim_listexpr

	listexpr_when = make([]*ast.Token_primary, len(listexpr)/2)
	listexpr_then = make([]*ast.Token_primary, len(listexpr)/2)

	/************* walk all WHEN and ThEN operands *************/

	i := 0
	for k := 0; k < len(listexpr)/2; k++ {
		// walk the WHEN operand

		tok_when = listexpr[i]
		if rsql_err = decor.walk_Token_primary_and_simplify(tok_when); rsql_err != nil {
			return rsql_err
		}
		listexpr_when[k] = tok_when

		// walk the THEN operand

		tok_then = listexpr[i+1]
		if rsql_err = decor.walk_Token_primary_and_simplify(tok_then); rsql_err != nil {
			return rsql_err
		}
		listexpr_then[k] = tok_then

		i += 2
	}

	/************* find requested operand datatype *************/

	equalized_operands_datatype = 0 // find requested operand datatype, which is the datatype of highest rank of all the arguments
	for _, tok_then = range listexpr_then {
		equalized_operands_datatype = compute_highest_datatype(equalized_operands_datatype, tok_then.Prim_dataslot.Datatype())
	}

	/************* determine instruction code *************/

	switch equalized_operands_datatype {
	case rsql.DATATYPE_VOID:
		instruction_code = rsql.INSTR_CASE_VOID

	case rsql.DATATYPE_VARBINARY:
		instruction_code = rsql.INSTR_CASE_VARBINARY
		properties_processing = DETERMINE_RESULT_VARBINARY_PRECISION_FOR_CASE

	case rsql.DATATYPE_VARCHAR:
		instruction_code = rsql.INSTR_CASE_VARCHAR
		properties_processing = DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CASE

	case rsql.DATATYPE_BIT:
		instruction_code = rsql.INSTR_CASE_BIT

	case rsql.DATATYPE_TINYINT:
		instruction_code = rsql.INSTR_CASE_TINYINT

	case rsql.DATATYPE_SMALLINT:
		instruction_code = rsql.INSTR_CASE_SMALLINT

	case rsql.DATATYPE_INT:
		instruction_code = rsql.INSTR_CASE_INT

	case rsql.DATATYPE_BIGINT:
		instruction_code = rsql.INSTR_CASE_BIGINT

	case rsql.DATATYPE_MONEY:
		instruction_code = rsql.INSTR_CASE_MONEY

	case rsql.DATATYPE_NUMERIC:
		instruction_code = rsql.INSTR_CASE_NUMERIC
		properties_processing = DETERMINE_RESULT_NUMERIC_P_S_FOR_CASE

	case rsql.DATATYPE_FLOAT:
		instruction_code = rsql.INSTR_CASE_FLOAT

	case rsql.DATATYPE_DATE:
		instruction_code = rsql.INSTR_CASE_DATE

	case rsql.DATATYPE_TIME:
		instruction_code = rsql.INSTR_CASE_TIME

	case rsql.DATATYPE_DATETIME:
		instruction_code = rsql.INSTR_CASE_DATETIME

	default:
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_SYSFUNC_OPERAND, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
	}

	result_datatype = equalized_operands_datatype

	/************* Create promotion node and insert it between current token and children if necessary *************/

	for i := 0; i < len(listexpr_when); i++ { // for all operands
		if listexpr_when[i].Prim_dataslot.Datatype() != rsql.DATATYPE_BOOLEAN { // create conversion token if necessary
			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(listexpr_when[i], rsql.DATATYPE_BOOLEAN); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}

			listexpr_when[i] = tok_promotion_token
		}
	}

	for i := 0; i < len(listexpr_then); i++ { // for all operands
		if listexpr_then[i].Prim_dataslot.Datatype() != equalized_operands_datatype { // create conversion token if necessary
			if tok_promotion_token, rsql_err = decor.Token_primary_IMPLICIT_CAST_EQUALIZE_new_with_Dataslot(listexpr_then[i], equalized_operands_datatype); rsql_err != nil { // returned token has been simplified
				return rsql_err
			}

			listexpr_then[i] = tok_promotion_token
		}
	}

	/************* determine precision, scale, fixlen_flag, collation_strength and collation of result for NUMERIC, VARCHAR, VARBINARY datatypes *************/

	result_precision = 0
	result_scale = 0
	result_fixlen_flag = false
	result_collation_strength = rsql.COLLSTRENGTH_0
	result_collation = ""

	switch properties_processing { //  for NUMERIC, VARCHAR, VARBINARY result, determine precision, scale, fixlen_flag, collation_strength, collation.
	case DETERMINE_RESULT_NO_PROCESSING:
		// do nothing

	case DETERMINE_RESULT_NUMERIC_P_S_FOR_CASE: // we want <p,s> of the result to encompass those of all operands.

		result_precision, result_scale = compute_numeric_precision_scale_encompassing(listexpr_then...)

	case DETERMINE_RESULT_VARCHAR_PRECISION_COLLATION_FOR_CASE: // precision and collation of the result encompass those of all operands.

		result_precision = compute_varchar_precision_largest(listexpr_then...)

		if result_collation_strength, result_collation, rsql_err = decor.compute_varchar_collation_encompassing(listexpr_then...); rsql_err != nil {
			return rsql_err
		}

	case DETERMINE_RESULT_VARBINARY_PRECISION_FOR_CASE: // precision is the max of all operands.

		result_precision = compute_varbinary_precision_largest(listexpr_then...)

	default:
		panic("unknown determine")
	}

	/************* copy listexpr_when and listexpr_then back into listexpr *************/

	for i, k := 0, 0; k < len(listexpr_when); i, k = i+2, k+1 {
		tok.Prim_listexpr[i] = listexpr_when[k]
		tok.Prim_listexpr[i+1] = listexpr_then[k]
	}

	/************* process current node and create Dataslot *************/

	// result_collation is used only if result is varchar, and must be non-nil. For other datatype, like int, result_collation must be "" (and result_collation_strength must be 0).

	rsql.Assert((result_datatype == rsql.DATATYPE_VARCHAR) != (result_collation == ""))

	dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_TMP, result_datatype, result_precision, result_scale, result_fixlen_flag)
	tok.Prim_dataslot = dataslot

	tok.Prim_collation_strength = result_collation_strength
	tok.Prim_collation = result_collation
	tok.Prim_instruction_code = instruction_code

	return nil
}

// compute_highest_datatype returns the datatype that has the highest rank.
//
func compute_highest_datatype(sql_datatype_a rsql.Datatype_t, sql_datatype_b rsql.Datatype_t) rsql.Datatype_t {

	sql_datatype_result := sql_datatype_b

	if sql_datatype_a > sql_datatype_b {
		sql_datatype_result = sql_datatype_a
	}

	return sql_datatype_result
}

// compute_numeric_precision_scale computes precision and scale of the NUMERIC result, for operators with NUMERIC operands and result.
//
//     The number of digits of the integral part of the result, as well as its scale, are limited to DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX (=28) and DATATYPE_NUMERIC_RESULT_SCALE_MAX (=6).
//     This means that if a result has more than 28 digits in its integral part, it will raise an overflow.
//
//     So, we have -1000. / 3 * 3 = -999.999999. For MS SQL Server, it is the same.
//
//     There is no best formula to compute precision and scale of the result. It is a range-scale tradeoff.
//     Originally, DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX was 22 and DATATYPE_NUMERIC_RESULT_SCALE_MAX was 12 and it worked well.
//     But we have changed that because we want this function to return integral part = precision-scale compatible with the dot Net decimal type precision (27-28).
//
func compute_numeric_precision_scale(instruction_code rsql.Instrcode_t, tok_left *ast.Token_primary, tok_right *ast.Token_primary) (result_precision uint16, result_scale uint16) {
	var (
		left_precision uint16
		left_integral  uint16 // integral part
		left_scale     uint16

		right_precision uint16
		right_integral  uint16 // integral part
		right_scale     uint16

		res_precision uint16
		res_integral  uint16 // integral part
		res_scale     uint16
	)

	rsql.Assert(tok_left != nil)

	left_precision = tok_left.Prim_dataslot.(*data.NUMERIC).Data_precision
	left_scale = tok_left.Prim_dataslot.(*data.NUMERIC).Data_scale
	left_integral = left_precision - left_scale

	if tok_right != nil {
		right_precision = tok_right.Prim_dataslot.(*data.NUMERIC).Data_precision
		right_scale = tok_right.Prim_dataslot.(*data.NUMERIC).Data_scale
		right_integral = right_precision - right_scale
	}

	switch instruction_code {
	case rsql.INSTR_UNARY_MINUS_NUMERIC:
		rsql.Assert(tok_right == nil)

		res_integral = left_integral
		res_scale = left_scale

	case rsql.INSTR_ADD_NUMERIC,
		rsql.INSTR_SUBTRACT_NUMERIC:
		rsql.Assert(tok_right != nil)

		res_integral = 1 + MMAX16(left_integral, right_integral) // e.g.   50.1 + 60.42 => 110.52
		res_scale = MMAX16(left_scale, right_scale)

	case rsql.INSTR_MULT_NUMERIC:
		rsql.Assert(tok_right != nil)

		res_integral = 1 + left_integral + right_integral // e.g.   9.9999999999999999 * 9.9999999999999999 => 100.000000000000
		res_scale = left_scale + right_scale

	case rsql.INSTR_DIV_NUMERIC:
		rsql.Assert(tok_right != nil)

		// with the formula below for res_scale, we obtain -1000. / 3 * 3 = -999.999999

		res_integral = left_integral + right_scale         // e.g. 987 / 0.0001 => 9870000
		res_scale = rsql.DATATYPE_NUMERIC_RESULT_SCALE_MAX // note: MS SQL Server has the formula MMAX16(6, left_scale + right_precision +1)

	default:
		panic("numeric operator unknown")
	}

	/* refit precision and scale of the result */

	if res_integral > rsql.DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX {
		res_integral = rsql.DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX
	}

	if res_scale > rsql.DATATYPE_NUMERIC_RESULT_SCALE_MAX {
		res_scale = rsql.DATATYPE_NUMERIC_RESULT_SCALE_MAX
	}

	res_precision = res_integral + res_scale // result precision

	return res_precision, res_scale
}

// compute_numeric_precision_scale_encompassing computes encompassing precision and scale from a list of NUMERIC operands.
//
func compute_numeric_precision_scale_encompassing(tok_list ...*ast.Token_primary) (result_precision uint16, result_scale uint16) {
	var (
		res_precision uint16
		res_integral  uint16
		res_scale     uint16

		tok_precision uint16
		tok_integral  uint16
		tok_scale     uint16
	)

	res_integral = 1 // default minimal result precision

	for _, tok := range tok_list {
		tok_precision = tok.Prim_dataslot.(*data.NUMERIC).Data_precision
		tok_scale = tok.Prim_dataslot.(*data.NUMERIC).Data_scale
		tok_integral = tok_precision - tok_scale

		if tok_integral > res_integral {
			res_integral = tok_integral
		}

		if tok_scale > res_scale {
			res_scale = tok_scale
		}
	}

	/* refit precision and scale of the result */

	if res_integral > rsql.DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX {
		res_integral = rsql.DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX
	}

	if res_scale > rsql.DATATYPE_NUMERIC_RESULT_SCALE_MAX {
		res_scale = rsql.DATATYPE_NUMERIC_RESULT_SCALE_MAX
	}

	res_precision = res_integral + res_scale // result precision

	return res_precision, res_scale
}

// compute_varchar_collation_encompassing computes encompassing collation from a list of VARCHAR operands.
//
func (decor *Decorator) compute_varchar_collation_encompassing(tok_list ...*ast.Token_primary) (collation_strength rsql.Collstrength_t, collation string, rsql_err *rsql.Error) {
	var (
		res_collation_strength rsql.Collstrength_t
		res_collation          string
		tok_collation_strength rsql.Collstrength_t
		tok_collation          string
	)

	res_collation_strength = rsql.COLLSTRENGTH_0
	res_collation = decor.Server_default_collation

	for _, tok := range tok_list {
		tok_collation_strength = tok.Prim_collation_strength
		tok_collation = tok.Prim_collation

		switch {
		case tok_collation_strength < res_collation_strength:
			continue

		case tok_collation_strength > res_collation_strength:
			res_collation_strength = tok_collation_strength
			res_collation = tok_collation

		default:
			if tok_collation == res_collation {
				continue
			}
			return 0, "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLLATIONS_CONFLICT, rsql.ERROR_BATCH_ABORT, tok_collation, res_collation)
		}
	}

	return res_collation_strength, res_collation, nil
}

// compute_varchar_precision_largest computes encompassing precision, by finding the largest precision in a list of VARCHAR operands.
//
func compute_varchar_precision_largest(tok_list ...*ast.Token_primary) uint16 {
	var (
		tok_precision uint16
		res_precision uint16
	)

	res_precision = 1 // minimal default precision

	for _, tok := range tok_list {
		tok_precision = tok.Prim_dataslot.(*data.VARCHAR).Data_precision
		if tok_precision > res_precision {
			res_precision = tok_precision
		}
	}

	if res_precision > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
		res_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX
	}

	return res_precision
}

// compute_varchar_precision_sum computes total precision, by adding up all precisions in a list of VARCHAR operands.
//
func compute_varchar_precision_sum(tok_list ...*ast.Token_primary) uint16 {
	var (
		tok_precision uint16
		res_precision uint64
	)

	for _, tok := range tok_list {
		tok_precision = tok.Prim_dataslot.(*data.VARCHAR).Data_precision
		res_precision += uint64(tok_precision)
	}

	if res_precision == 0 {
		res_precision = 1
	}

	if res_precision > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
		res_precision = rsql.DATATYPE_VARCHAR_PRECISION_MAX
	}

	return uint16(res_precision)
}

// compute_varchar_fixlen_flag returns true if all VARCHAR operands have fixlen_flag == true.
//
// This function is used when decorating UNION statements.
//
func compute_varchar_fixlen_flag(tok_list ...*ast.Token_primary) bool {

	for _, tok := range tok_list {
		if tok.Prim_dataslot.(*data.VARCHAR).Data_fixlen_flag == false {
			return false
		}
	}

	return true
}

// compute_varbinary_precision_largest computes encompassing precision, by finding the largest precision in a list of VARBINARY operands.
//
func compute_varbinary_precision_largest(tok_list ...*ast.Token_primary) uint16 {
	var (
		tok_precision uint16
		res_precision uint16
	)

	res_precision = 1 // minimal default precision

	for _, tok := range tok_list {
		tok_precision = tok.Prim_dataslot.(*data.VARBINARY).Data_precision
		if tok_precision > res_precision {
			res_precision = tok_precision
		}
	}

	if res_precision > rsql.DATATYPE_VARBINARY_PRECISION_MAX {
		res_precision = rsql.DATATYPE_VARBINARY_PRECISION_MAX
	}

	return res_precision
}

// compute_varbinary_precision_sum computes total precision, by adding up all precisions in a list of VARBINARY operands.
//
func compute_varbinary_precision_sum(tok_list ...*ast.Token_primary) uint16 {
	var (
		tok_precision uint16
		res_precision uint64
	)

	for _, tok := range tok_list {
		tok_precision = tok.Prim_dataslot.(*data.VARBINARY).Data_precision
		res_precision += uint64(tok_precision)
	}

	if res_precision == 0 {
		res_precision = 1
	}

	if res_precision > rsql.DATATYPE_VARBINARY_PRECISION_MAX {
		res_precision = rsql.DATATYPE_VARBINARY_PRECISION_MAX
	}

	return uint16(res_precision)
}
