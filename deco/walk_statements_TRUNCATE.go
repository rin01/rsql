package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/dict"
)

func (decor *Decorator) wacf_Token_stmt_TRUNCATE_TABLE(tok *ast.Token_stmt_TRUNCATE_TABLE) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err  *rsql.Error
		gtabledef *rsql.GTabledef
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_TRUNCATE_TABLE)

	// get GTabledef from dict

	if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(tok.Tt_table_qname); rsql_err != nil {
		return rsql_err
	}

	tok.Tt_gtabledef = gtabledef

	return nil
}
