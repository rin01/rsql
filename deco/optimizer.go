package deco

import (
	"fmt"
	"sort"

	"rsql"
	"rsql/ast"
	"rsql/btree"
	"rsql/csr"
	"rsql/data"
	"rsql/emit"
)

type Optimizer struct {
	Map_xtbl            map[ast.Xtable]*Xtbl_entry // filled-in when walking the JOIN tree, with real table, SELECT or UNION tables.
	Last_table_in_from  map[*ast.Xtable_from]ast.Xtable
	current_xtable_from *ast.Xtable_from

	current_sarginfos map[Sargkey]*ast.Sarginfo // during walking of Xtable tree, contains the current Sarginfos map. A new Sarginfos map is created at each LEFT JOIN.
	current_condinfos map[Condkey]*ast.Condinfo // same comment as above, but for Condinfos.
	current_condseqno int                       // counter for Condinfos

	volatile_function_detected *rsql.Error // not nil if a volatile function exists in ON or WHERE clause
}

type Xtbl_entry struct {
	xtbl      ast.Xtable       // real table, SELECT or UNION tables
	tseqno    int              // tables in tree are numbered sequentially 0, 1, ...
	xtbl_from *ast.Xtable_from // the FROM clause to which the table belongs

	sarginfos map[Sargkey]*ast.Sarginfo // collection of sargs that may apply to this xtable
	condinfos map[Condkey]*ast.Condinfo // collection of conditions that may apply to this xtable
}

// NOTE: if a Sarg is entered in opz.current_sarginfos map and a Sarg for the same table, column and comparison operator already exists in the map, the latter is replaced by the new one.
//
type Sargkey struct {
	Sarg_xtable         *ast.Xtable_table
	Sarg_col_base_seqno uint16
	Sarg_comp           csr.Sarg_comp_t
}

type Condkey struct {
	Cond_xtable ast.Xtable // Conditions may be applied to real table, and in the future also to SELECT or UNION table (not done yet)
	Cond_seqno  int        // a sequential numbering of all conditions encountered in ON and WHERE clause
}

func New_optimizer() *Optimizer {
	opz := &Optimizer{}

	opz.Map_xtbl = make(map[ast.Xtable]*Xtbl_entry, 10)
	opz.Last_table_in_from = make(map[*ast.Xtable_from]ast.Xtable, 6)

	opz.current_sarginfos = make(map[Sargkey]*ast.Sarginfo, 20)
	opz.current_condinfos = make(map[Condkey]*ast.Condinfo, 20)

	return opz
}

//--------- analyze_xxx functions walk the tree of Xtables:
//
//                  - fill in the list of Xtables in opz.Map_xtbl with real table, SELECT or UNION tables
//                  - search for Sargs in ON and WHERE clauses, and put them into opz.current_sarginfos
//                  - put all Conditions in opz.current_condinfos

func (opz *Optimizer) analyze_xtable_table(xtable_table *ast.Xtable_table) {

	// put table in map

	opz.Map_xtbl[xtable_table] = &Xtbl_entry{xtbl: xtable_table, tseqno: len(opz.Map_xtbl), xtbl_from: opz.current_xtable_from, sarginfos: opz.current_sarginfos, condinfos: opz.current_condinfos}

	if opz.current_xtable_from != nil {
		opz.Last_table_in_from[opz.current_xtable_from] = xtable_table
	}
}

func (opz *Optimizer) analyze_xtable_join(xtable_join *ast.Xtable_join) {
	var (
		bak_sarginfos map[Sargkey]*ast.Sarginfo
		bak_condinfos map[Condkey]*ast.Condinfo
	)

	opz.analyze_xtable(xtable_join.Jn_left)

	if xtable_join.Jn_type == csr.JT_LEFT_JOIN {
		bak_sarginfos = opz.current_sarginfos
		bak_condinfos = opz.current_condinfos

		opz.current_sarginfos = make(map[Sargkey]*ast.Sarginfo, 20)
		opz.current_condinfos = make(map[Condkey]*ast.Condinfo, 20)
	}

	opz.analyze_xtable(xtable_join.Jn_right)

	if xtable_join.Jn_on != nil {
		opz.search_for_sargs_and_conds(xtable_join.Jn_on)
	}

	if bak_sarginfos != nil {
		opz.current_sarginfos = bak_sarginfos
		opz.current_condinfos = bak_condinfos
	}
}

func (opz *Optimizer) analyze_xtable_from(xtable_from *ast.Xtable_from) {

	rsql.Assert(len(xtable_from.Fr_list) == 1)

	xtable_from_bak := opz.current_xtable_from
	opz.current_xtable_from = xtable_from
	opz.analyze_xtable(xtable_from.Fr_list[0]) // walk tree of tables in FROM clause
	opz.current_xtable_from = xtable_from_bak

	if xtable_from.Fr_where != nil {
		opz.search_for_sargs_and_conds(xtable_from.Fr_where)
	}
}

func (opz *Optimizer) analyze_xtable_select(xtable_select *ast.Xtable_select) {

	// analyze FROM

	bak_sarginfos := opz.current_sarginfos
	bak_condinfos := opz.current_condinfos

	opz.current_sarginfos = make(map[Sargkey]*ast.Sarginfo, 20)
	opz.current_condinfos = make(map[Condkey]*ast.Condinfo, 20)

	opz.analyze_xtable_from(xtable_select.Sel_from)

	opz.current_sarginfos = bak_sarginfos
	opz.current_condinfos = bak_condinfos

	// put table in map

	opz.Map_xtbl[xtable_select] = &Xtbl_entry{xtbl: xtable_select, tseqno: len(opz.Map_xtbl), xtbl_from: opz.current_xtable_from, sarginfos: nil, condinfos: nil} // sarginfos and condinfos are only relevant for real table

	if opz.current_xtable_from != nil {
		opz.Last_table_in_from[opz.current_xtable_from] = xtable_select
	}
}

func (opz *Optimizer) analyze_xtable_union(xtable_union *ast.Xtable_union) {

	// analyze each member of union

	for _, item := range xtable_union.Un_members { // for each member of UNION
		xtable := item.Xtbl

		opz.analyze_xtable(xtable) // each SELECT creates a new collection of Sarginfos
	}

	// put table in map

	opz.Map_xtbl[xtable_union] = &Xtbl_entry{xtbl: xtable_union, tseqno: len(opz.Map_xtbl), xtbl_from: opz.current_xtable_from, sarginfos: nil, condinfos: nil} // sarginfos and condinfos are only relevant for real table

	if opz.current_xtable_from != nil {
		opz.Last_table_in_from[opz.current_xtable_from] = xtable_union
	}
}

// main function to analyze tables in SELECT and search sargs.
//
func (opz *Optimizer) analyze_xtable(xtable ast.Xtable) {

	switch xtable.Type() {
	case ast.XT_TABLE:
		opz.analyze_xtable_table(xtable.(*ast.Xtable_table))

	case ast.XT_JOIN:
		opz.analyze_xtable_join(xtable.(*ast.Xtable_join))

	case ast.XT_SINGLE:
		// pass

	case ast.XT_FROM:
		opz.analyze_xtable_from(xtable.(*ast.Xtable_from))

	case ast.XT_SELECT:
		opz.analyze_xtable_select(xtable.(*ast.Xtable_select))

	case ast.XT_UNION:
		opz.analyze_xtable_union(xtable.(*ast.Xtable_union))

	default:
		panic("impossible")
	}
}

//=====

// search_for_sargs_and_conds takes a ON or WHERE clause as argument.
// The clause expression is splitted at each AND.
// If a subexpression is a comparison of the form     col = expression   or   cast(col as ...) = expression, it is registered as a SARG.
// Comparison can be any comparison operator.
//
// Each subexpression is also checked by inverting left and right operands, to detect possible SARG:
//           expression = col     or     expression = cast(col as ...)
//
// Conditions that are not SARGs but can be computed and applied on the table (that is, conditions that don't depend on subsequent tables) are also registered in a map.
//
func (opz *Optimizer) search_for_sargs_and_conds(tok *ast.Token_primary) {
	var (
		tok_a *ast.Token_primary // col  or   cast(col as ...)
		tok_b *ast.Token_primary // expression
		comp  csr.Sarg_comp_t
	)

	if tok.Prim_dataslot.Kind() == rsql.KIND_RO_LEAF { // boolean constant value
		return
	}

	if tok.Prim_subtype == ast.TOK_PRIM_OPERATOR_LOGICAL_AND {
		opz.search_for_sargs_and_conds(tok.Prim_left)
		opz.search_for_sargs_and_conds(tok.Prim_right)
		return
	}

	// put condition expression in map

	condinfo_highest_tseqno, condinfo_xtable := opz.get_highest_tseqno(-1, nil, tok) // look for the table to which the condition applies

	if condinfo_highest_tseqno >= 0 && condinfo_highest_tseqno != HIGHEST_TSEQNO_UNKNOWN {
		condinfo := &ast.Condinfo{}
		condinfo.Cond_seqno = opz.current_condseqno
		condinfo.Cond_dataslot = tok.Prim_dataslot.(*data.BOOLEAN)
		condinfo.Tok_cond = tok

		opz.current_condinfos[Condkey{Cond_xtable: condinfo_xtable, Cond_seqno: opz.current_condseqno}] = condinfo
		opz.current_condseqno++
	}

	// tok is a comparison operator. It is a computed (that is, not KIND_RO_LEAF) boolean value.

	switch tok.Prim_subtype {
	case ast.TOK_PRIM_OPERATOR_COMP_GREATER:
		comp = csr.SARG_GREATER

	case ast.TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL:
		comp = csr.SARG_GREATER_EQUAL

	case ast.TOK_PRIM_OPERATOR_COMP_EQUAL:
		comp = csr.SARG_EQUAL

	case ast.TOK_PRIM_OPERATOR_COMP_LESS_EQUAL:
		comp = csr.SARG_LESS_EQUAL

	case ast.TOK_PRIM_OPERATOR_COMP_LESS:
		comp = csr.SARG_LESS

	default: // if not a comparison operator, return
		return
	}

	tok_a = tok.Prim_left
	tok_b = tok.Prim_right
	second_pass := false

LABEL_START:

	var (
		tok_a_column              *ast.Token_primary
		xtable_table_a            *ast.Xtable_table
		entry                     *Xtbl_entry
		ok                        bool
		column_tseqno             int
		expression_highest_tseqno int
		sarg                      *ast.Sarginfo
	)

	if second_pass == true {
		// comparison is inverted

		switch tok.Prim_subtype {
		case ast.TOK_PRIM_OPERATOR_COMP_GREATER:
			comp = csr.SARG_LESS

		case ast.TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL:
			comp = csr.SARG_LESS_EQUAL

		case ast.TOK_PRIM_OPERATOR_COMP_EQUAL:
			comp = csr.SARG_EQUAL

		case ast.TOK_PRIM_OPERATOR_COMP_LESS_EQUAL:
			comp = csr.SARG_GREATER_EQUAL

		case ast.TOK_PRIM_OPERATOR_COMP_LESS:
			comp = csr.SARG_GREATER

		default:
			panic("impossible")
		}
	}

	switch {
	case tok_a.Prim_type == ast.TOK_PRIM_IDENTIFIER:
		rsql.Assert(tok_a.Prim_dataslot.Kind() == rsql.KIND_COL_LEAF)
		tok_a_column = tok_a

	case tok_a.Prim_type == ast.TOK_PRIM_CAST && tok_a.Prim_left.Prim_type == ast.TOK_PRIM_IDENTIFIER:
		rsql.Assert(tok_a.Prim_dataslot.Kind() == rsql.KIND_TMP && tok_a.Prim_left.Prim_dataslot.Kind() == rsql.KIND_COL_LEAF)
		tok_a_column = tok_a.Prim_left

	default:
		goto LABEL_END
	}

	// check if column dataslot belongs to a real table

	if xtable_table_a, ok = tok_a_column.Prim_xtable_ds.(*ast.Xtable_table); ok == false { // only columns of real table (not columns of SELECT or UNION tables) can use indexes
		goto LABEL_END
	}

	// if CAST(column), check that it is an allowed cast

	if tok_a.Prim_type == ast.TOK_PRIM_CAST {
		switch tok_a.Prim_instruction_code {
		case rsql.INSTR_CAST_VARCHAR_VARCHAR: // varchar or char -> varchar or char       Only the strings without trailing blanks are relevant, as comparisons skip trailing blanks.
			if tok_a.Prim_dataslot.(*data.VARCHAR).Data_precision < tok_a_column.Prim_dataslot.(*data.VARCHAR).Data_precision {
				goto LABEL_END
			}

		case rsql.INSTR_CAST_TINYINT_TINYINT,
			rsql.INSTR_CAST_TINYINT_SMALLINT,
			rsql.INSTR_CAST_TINYINT_INT,
			rsql.INSTR_CAST_TINYINT_BIGINT,

			rsql.INSTR_CAST_SMALLINT_TINYINT,
			rsql.INSTR_CAST_SMALLINT_SMALLINT,
			rsql.INSTR_CAST_SMALLINT_INT,
			rsql.INSTR_CAST_SMALLINT_BIGINT,

			rsql.INSTR_CAST_INT_TINYINT,
			rsql.INSTR_CAST_INT_SMALLINT,
			rsql.INSTR_CAST_INT_INT,
			rsql.INSTR_CAST_INT_BIGINT,

			rsql.INSTR_CAST_BIGINT_TINYINT,
			rsql.INSTR_CAST_BIGINT_SMALLINT,
			rsql.INSTR_CAST_BIGINT_INT,
			rsql.INSTR_CAST_BIGINT_BIGINT:
			//pass

		case rsql.INSTR_CAST_MONEY_MONEY:
			// pass

		case rsql.INSTR_CAST_MONEY_NUMERIC:
			if tok_a.Prim_dataslot.(*data.NUMERIC).Data_scale < tok_a_column.Prim_dataslot.(*data.MONEY).Data_scale {
				goto LABEL_END
			}

		case rsql.INSTR_CAST_NUMERIC_MONEY:
			if tok_a.Prim_dataslot.(*data.MONEY).Data_scale < tok_a_column.Prim_dataslot.(*data.NUMERIC).Data_scale {
				goto LABEL_END
			}

		case rsql.INSTR_CAST_NUMERIC_NUMERIC:
			if tok_a.Prim_dataslot.(*data.NUMERIC).Data_scale < tok_a_column.Prim_dataslot.(*data.NUMERIC).Data_scale {
				goto LABEL_END
			}

		case rsql.INSTR_CAST_DATE_DATETIME:
			// pass

		default:
			goto LABEL_END
		}
	}

	// check if instruction collation is same as column collation

	if tok_a_column.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR { // char or varchar
		instruction_collation := tok.Prim_syscollator.Prim_name
		collation_column_a := xtable_table_a.Tbl_cursor.Base_tabledef.Td_coldefs[tok_a_column.Prim_col_no].Cd_collation // physical collation of the column in table

		//println("VARCHAR comparison: " + instruction_collation + "   " + collation_column_a)

		if instruction_collation != collation_column_a {
			goto LABEL_END
		}
	}

	// check that all columns in expression tok_b are set before tok_a_column

	entry = opz.Map_xtbl[xtable_table_a] // info about the table containing tok_a_column
	rsql.Assert(entry != nil)
	column_tseqno = entry.tseqno

	expression_highest_tseqno, _ = opz.get_highest_tseqno(-1, nil, tok_b) // if tok_b is made of constants and @variables, returns -1

	if expression_highest_tseqno >= column_tseqno || expression_highest_tseqno == HIGHEST_TSEQNO_UNKNOWN {
		goto LABEL_END
	}

	// comparison expression is a SARG

	sarg = &ast.Sarginfo{
		Sarg: csr.Sarg{
			Table_qname:        xtable_table_a.Tbl_qname,
			Table_cursor:       xtable_table_a.Tbl_cursor,
			Col_no:             tok_a_column.Prim_col_no,
			Comp:               comp,
			Comp_args_reversed: second_pass,
			Comp_dataslot:      tok.Prim_dataslot.(*data.BOOLEAN),
		},

		Tok_a_column: tok_a_column,
		Tok_a:        tok_a,
		Tok_expr:     tok_b,
		Tok_comp:     tok,
	}

	opz.current_sarginfos[Sargkey{Sarg_xtable: xtable_table_a, Sarg_col_base_seqno: tok_a_column.Prim_col_no, Sarg_comp: comp}] = sarg

LABEL_END:
	if second_pass == true {
		return
	}

	tok_a = tok.Prim_right
	tok_b = tok.Prim_left
	second_pass = true

	goto LABEL_START
}

const HIGHEST_TSEQNO_UNKNOWN int = 1000000000 // any big value is good

// walk_check_Token_primary walks an expression tree 'tok', and returns the highest tseqno of the table (real table, SELECT or UNION) to which columns in the expression belong.
//
// NOTE: first call must be done with -1 as highest_tseqno and nil as xtable, which means that the expression value is computed before the first xtable is entered.
//
// The return value HIGHEST_TSEQNO_UNKNOWN is returned if the expression tree cannot be walked.
//
// As this function is used to walk ON and WHERE clauses, it is also used to detect volatile functions (e.g. rand()), which are not allowed in these clauses.
//
func (opz *Optimizer) get_highest_tseqno(highest_tseqno int, xtable ast.Xtable, tok *ast.Token_primary) (new_highest_tseqno int, new_xtable ast.Xtable) {

	// if KIND_RO_LEAF, KIND_VAR_LEAF or KIND_COL_LEAF, it is not an operator nor a function, but a simple data. It may be a literal, a variable or a column.

	switch tok.Prim_dataslot.Kind() {
	case rsql.KIND_RO_LEAF,
		rsql.KIND_VAR_LEAF:
		return highest_tseqno, xtable

	case rsql.KIND_COL_LEAF: // column of table, SELECT or UNION
		var entry *Xtbl_entry
		var ok bool

		rsql.Assert(tok.Prim_xtable_ds != nil)

		entry, ok = opz.Map_xtbl[tok.Prim_xtable_ds]
		rsql.Assert(ok == true)

		if entry.tseqno > highest_tseqno {
			return entry.tseqno, entry.xtbl
		}

		return highest_tseqno, xtable

	case rsql.KIND_TMP:
		// pass

	default:
		panic("impossible")
	}

	// walk left and right operands

	switch tok.Prim_type {
	case ast.TOK_PRIM_OPERATOR:
		switch tok.Prim_cardinality {
		case ast.TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL:

			highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, tok.Prim_left) // left operand

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE:

			highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, tok.Prim_left) // left operand

			highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, tok.Prim_right) // right operand

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST:

			highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, tok.Prim_left) // left operand

			for _, operand := range tok.Prim_listexpr {
				highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, operand) // walk each operand
			}

		default:
			panic("cardinality unknown")
		}

	case ast.TOK_PRIM_CAST:
		highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, tok.Prim_left) // left operand

	case ast.TOK_PRIM_SYSFUNC:
		for _, tok_arg := range tok.Prim_listexpr {
			highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, tok_arg)
		}

		if tok.Prim_sysfunc_descr.Is_volatile() { // check if volatile function exists in ON or WHERE clause
			rsql_err := rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_VOLATILE_FUNCTION_FORBIDDEN_IN_ON_WHERE, rsql.ERROR_BATCH_ABORT, tok.Prim_name)
			rsql_err.Batch_line_no = tok.Tok_batch_line.No // normally, we should use    decor.Info_line = tok.Batch_line(), but decor is not passed to this function
			rsql_err.Batch_line_pos = uint32(tok.Tok_batch_line.Pos)

			opz.volatile_function_detected = rsql_err
		}

	case ast.TOK_PRIM_CASE:
		rsql.Assert(tok.Prim_left == nil && tok.Prim_right == nil)

		for _, tok_arg := range tok.Prim_listexpr {
			highest_tseqno, xtable = opz.get_highest_tseqno(highest_tseqno, xtable, tok_arg)
		}

	default:
		return HIGHEST_TSEQNO_UNKNOWN, nil
	}

	return highest_tseqno, xtable
}

//---------------

// Optimize_all analyzes and optimizes all real tables in the tree of tables passed as argument.
//
func (opz *Optimizer) Optimize_all(xtable ast.Xtable) *rsql.Error {

	// optimization of table (may be grouptable). Create list of Sargs.

	opz.analyze_xtable(xtable)

	opz.current_sarginfos = nil
	opz.current_condinfos = nil

	if rsql_err := opz.volatile_function_detected; rsql_err != nil {
		return rsql_err
	}

	// for each real table, choose the best index

	list := make([]*Xtbl_entry, 0, len(opz.Map_xtbl))

	for _, xtbl_entry := range opz.Map_xtbl {
		list = append(list, xtbl_entry)
	}

	sort.Sort(Xe_sortable(list))

	if rsql.G_DEBUG_FLAG {
		fmt.Println("---- opz.Last_table_in_from")
		for _, last_table := range opz.Last_table_in_from {
			switch last_table := last_table.(type) {
			case *ast.Xtable_table:
				fmt.Println("    TABLE", last_table.Tbl_qname.String(), last_table.Tbl_alias)
			case *ast.Xtable_select:
				fmt.Println("    SELECT")
			case *ast.Xtable_union:
				fmt.Println("    UNION")
			default:
				panic("impossible")
			}
		}

		fmt.Println()
	}

	for _, xtbl_entry := range list {
		xtbl := xtbl_entry.xtbl

		if xtable_select, ok := xtbl.(*ast.Xtable_select); ok == true { // for each SELECT
			if xtable_select.Sel_grouping_flag == true { // for grouping SELECT, optimize the feeding FROM xtable
				optimizer := New_optimizer()

				if rsql_err := optimizer.Optimize_all(xtable_select.Sel_grouptable_feeding_from); rsql_err != nil { // optimize the feeding FROM xtable
					return rsql_err
				}
			}

			continue
		}

		// if plain table

		xtable_table, ok := xtbl.(*ast.Xtable_table) // for each real table
		if ok == false {
			continue
		}

		index_score := opz.optimize(xtable_table, xtbl_entry.sarginfos, xtbl_entry.condinfos)
		if index_score == nil {
			continue
		}

		switch index_score.Sc_lookup_type {
		case csr.SC_LOOKUP_EQUALITY:
			if rsql_err := optimize_table_cursor_for_lookup_equality(xtable_table.Tbl_cursor, index_score); rsql_err != nil { // fill cursor with optimization information
				return rsql_err
			}

		case csr.SC_LOOKUP_RANGE:
			if rsql_err := optimize_table_cursor_for_lookup_range(xtable_table.Tbl_cursor, index_score); rsql_err != nil { // fill cursor with optimization information
				return rsql_err
			}

		case csr.SC_LOOKUP_BASE_TABLE_SCAN: // it is not a real lookup, but just a scan which uses all Condinfos during the scan
			if rsql_err := optimize_table_cursor_for_lookup_base_table_scan(xtable_table.Tbl_cursor, index_score); rsql_err != nil { // fill cursor with optimization information
				return rsql_err
			}

		default:
			panic("impossible")
		}
	}

	return nil
}

type Xe_sortable []*Xtbl_entry

func (a Xe_sortable) Len() int           { return len(a) }
func (a Xe_sortable) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a Xe_sortable) Less(i, j int) bool { return a[i].tseqno < a[j].tseqno }

type Sc_sortkind_t uint8

// the value of these constants are used for sort purpose. Don't change their order !
const (
	SC_SORTKIND_CLUSTERED_KEY Sc_sortkind_t = iota + 1
	SC_SORTKIND_NONCLUSTERED_PRIMARY_KEY
	SC_SORTKIND_NONCLUSTERED_UNIQUE
	SC_SORTKIND_NONCLUSTERED_INDEX
)

func (sk Sc_sortkind_t) String() string {

	switch sk {
	case SC_SORTKIND_CLUSTERED_KEY:
		return "SC_SORTKIND_CLUSTERED_KEY"
	case SC_SORTKIND_NONCLUSTERED_PRIMARY_KEY:
		return "SC_SORTKIND_NONCLUSTERED_PRIMARY_KEY"
	case SC_SORTKIND_NONCLUSTERED_UNIQUE:
		return "SC_SORTKIND_NONCLUSTERED_UNIQUE"
	case SC_SORTKIND_NONCLUSTERED_INDEX:
		return "SC_SORTKIND_NONCLUSTERED_INDEX"
	default:
		panic("impossible")
	}
}

// Index_score contains for each index the information about its usability.
// Index_score for all indexes will be compared to find the more efficient.
//
// The algorithm is as follows:
//
//    Sort the list of scores as follows and take the first:
//
//           1) by Sc_lookup_type                        SC_LOOKUP_EQUALITY, SC_LOOKUP_RANGE
//           2) by Sc_significant_weight (desc):         the portion of index that is used for search
//           3) by Sc_sortkind:                          SC_SORTKIND_CLUSTERED_KEY, _NONCLUSTERED_PRIMARY_KEY, _NONCLUSTERED_UNIQUE, _NONCLUSTERED_INDEX
//           4) by Sc_significant_nk_leading_colcount
//           5) by Sc_indexname                          as tiebreaker
//
//    If list of scores is empty, no index is usable.
//
type Index_score struct {
	Sc_xtable           *ast.Xtable_table
	Sc_base_table_qname rsql.Object_qname_t
	Sc_base_table_alias string
	Sc_indexname        string
	Sc_indexdef         *rsql.Tabledef
	Sc_sortkind         Sc_sortkind_t // used for sorting the scores from best to worse. SC_SORTKIND_CLUSTERED_KEY, _NONCLUSTERED_PRIMARY_KEY, _NONCLUSTERED_UNIQUE, _NONCLUSTERED_INDEX

	Sc_sarginfos_equal []*ast.Sarginfo // size is length(indexdef.Td_nk). Contains '=' Sarginfos for leading nk fields.
	Sc_sarginfos_low   []*ast.Sarginfo // size is length(indexdef.Td_nk). Contains '>' and '>=' Sarginfos for leading nk fields.
	Sc_sarginfos_high  []*ast.Sarginfo // size is 1. Contains '<' and '<=' Sarginfo for first nk fields.

	Sc_lookup_type csr.Sc_lookup_type_t // if Sc_sarginfos_equal[0] contains a Sarginfo, it is SC_LOOKUP_EQUALITY. Else, if Sc_sarginfos_low[0] or Sc_sarginfos_high[0] contains a Sarginfo, SC_LOOKUP_RANGE. Else, index is not usable.

	Sc_nk_leading_colcount int // if Sc_lookup_type is SC_LOOKUP_EQUALITY, count of leading Sarginfos in Sc_sarginfos_equal. Else, count of leading Sarginfos in Sc_sarginfos_low (can be 0).
	Sc_nk_total_colcount   int // len(indexdef.Td_nk)

	Sc_significant_nk_leading_colcount int // same as Sc_nk_leading_colcount, at least 1. But for SC_SORTKIND_NONCLUSTERED_INDEX, if no Sarginfo for rowid, Sc_nk_leading_colcount-1
	Sc_significant_nk_total_colcount   int // same as above

	Sc_significant_weight     int // Sc_significant_nk_leading_colcount value, scaled relative to Sc_significant_weight_max
	Sc_significant_weight_max int // a common multiple of Sc_significant_nk_total_colcount value for all Index_scores

	Sc_condinfos []*ast.Condinfo // other conditions that can be applied to the table
}

func Sarg_list_is_empty(list []*ast.Sarginfo) bool {
	for _, sarginfo := range list {
		if sarginfo != nil {
			return false
		}
	}

	return true
}

// optimize finds the best index for the real table xtbl.
//
func (opz *Optimizer) optimize(xtbl *ast.Xtable_table, sarginfos_map map[Sargkey]*ast.Sarginfo, condinfos_map map[Condkey]*ast.Condinfo) *Index_score {

	gtabledef := xtbl.Tbl_cursor.Gtabledef

	// create list of indexes of the table

	list_index := make([]*rsql.Tabledef, 0, len(gtabledef.Indexmap)+1)

	list_index = append(list_index, gtabledef.Base)

	for _, indexdef := range gtabledef.Indexmap {
		list_index = append(list_index, indexdef)
	}

	// fill in list of Index_scores. Each index has an Index_score that describes its usablility for lookup.

	list_scores := make([]*Index_score, 0, len(list_index))

LABEL_ANALYZE_INDEX:
	for _, indexdef := range list_index {
		var sortkind Sc_sortkind_t

		switch {
		case indexdef.Td_type == rsql.TD_TYPE_BASE_TABLE: // always clustered
			sortkind = SC_SORTKIND_CLUSTERED_KEY
		case indexdef.Td_index_type == rsql.TD_PRIMARY_KEY:
			sortkind = SC_SORTKIND_NONCLUSTERED_PRIMARY_KEY
		case indexdef.Td_index_type == rsql.TD_UNIQUE:
			sortkind = SC_SORTKIND_NONCLUSTERED_UNIQUE
		case indexdef.Td_index_type == rsql.TD_INDEX:
			sortkind = SC_SORTKIND_NONCLUSTERED_INDEX
		default:
			panic("impossible")
		}

		index_score := &Index_score{
			Sc_xtable:           xtbl,
			Sc_base_table_qname: xtbl.Tbl_qname,
			Sc_base_table_alias: xtbl.Tbl_alias,
			Sc_indexname:        indexdef.Td_index_name,
			Sc_indexdef:         indexdef,
			Sc_sortkind:         sortkind,
		}
		index_score.Sc_sarginfos_equal = make([]*ast.Sarginfo, len(indexdef.Td_nk))
		index_score.Sc_sarginfos_low = make([]*ast.Sarginfo, len(indexdef.Td_nk))
		index_score.Sc_sarginfos_high = make([]*ast.Sarginfo, 1)

		index_score.Sc_nk_total_colcount = len(indexdef.Td_nk)

		// for each native key field of the index, fill in corresponding Sarginfo if exists for SARG_EQUAL.

		for i, coldef := range indexdef.Td_nk {
			var (
				sarginfo *ast.Sarginfo
				ok       bool
			)

			if sarginfo, ok = sarginfos_map[Sargkey{Sarg_xtable: xtbl, Sarg_col_base_seqno: coldef.Cd_base_seqno, Sarg_comp: csr.SARG_EQUAL}]; ok == false {
				break
			}

			rsql.Assert(sarginfo.Comp == csr.SARG_EQUAL)
			index_score.Sc_sarginfos_equal[i] = sarginfo // trailing fields may remain nil
		}

		// for each native key field of the index, fill in corresponding Sarginfo if exists for SARG_GREATER_EQUAL and SARG_GREATER.

		for i, coldef := range indexdef.Td_nk {
			var (
				sarginfo *ast.Sarginfo
				ok       bool
			)

			if sarginfo, ok = sarginfos_map[Sargkey{Sarg_xtable: xtbl, Sarg_col_base_seqno: coldef.Cd_base_seqno, Sarg_comp: csr.SARG_GREATER_EQUAL}]; ok == false {
				if sarginfo, ok = sarginfos_map[Sargkey{Sarg_xtable: xtbl, Sarg_col_base_seqno: coldef.Cd_base_seqno, Sarg_comp: csr.SARG_GREATER}]; ok == false {
					break
				}
			}

			rsql.Assert(sarginfo.Comp == csr.SARG_GREATER_EQUAL || sarginfo.Comp == csr.SARG_GREATER)
			index_score.Sc_sarginfos_low[i] = sarginfo // trailing fields may remain nil
		}

		// for first native key field of the index, fill in corresponding Sarginfo if exists for SARG_LESS_EQUAL and SARG_LESS.

		for i, coldef := range indexdef.Td_nk[:1] { // first nk field only
			var (
				sarginfo *ast.Sarginfo
				ok       bool
			)

			if sarginfo, ok = sarginfos_map[Sargkey{Sarg_xtable: xtbl, Sarg_col_base_seqno: coldef.Cd_base_seqno, Sarg_comp: csr.SARG_LESS_EQUAL}]; ok == false {
				if sarginfo, ok = sarginfos_map[Sargkey{Sarg_xtable: xtbl, Sarg_col_base_seqno: coldef.Cd_base_seqno, Sarg_comp: csr.SARG_LESS}]; ok == false {
					break
				}
			}

			rsql.Assert(sarginfo.Comp == csr.SARG_LESS_EQUAL || sarginfo.Comp == csr.SARG_LESS)
			index_score.Sc_sarginfos_high[i] = sarginfo // trailing fields remain nil
		}

		// if index is usable for equality or range lookup, put it in list_scores

		switch {
		case index_score.Sc_sarginfos_equal[0] != nil: // equality lookup can be performed
			index_score.Sc_lookup_type = csr.SC_LOOKUP_EQUALITY

			for _, s := range index_score.Sc_sarginfos_equal {
				if s == nil {
					break
				}

				index_score.Sc_nk_leading_colcount++ // count of leading Sargs for lookup
			}

		case index_score.Sc_sarginfos_low[0] != nil || index_score.Sc_sarginfos_high[0] != nil: // range lookup can be performed
			index_score.Sc_lookup_type = csr.SC_LOOKUP_RANGE

			for _, s := range index_score.Sc_sarginfos_low {
				if s == nil {
					break
				}

				index_score.Sc_nk_leading_colcount++ // count of leading Sargs for lookup
			}

		default: // index is not usable for lookup
			continue LABEL_ANALYZE_INDEX // index is not usable for lookup
		}

		index_score.Sc_significant_nk_leading_colcount = index_score.Sc_nk_leading_colcount
		if index_score.Sc_lookup_type == csr.SC_LOOKUP_RANGE && index_score.Sc_significant_nk_leading_colcount == 0 { // only Sc_sarginfos_high[0] sarg exists
			index_score.Sc_significant_nk_leading_colcount = 1
		}
		index_score.Sc_significant_nk_total_colcount = index_score.Sc_nk_total_colcount

		if index_score.Sc_sortkind == SC_SORTKIND_NONCLUSTERED_INDEX {
			if index_score.Sc_nk_leading_colcount != index_score.Sc_nk_total_colcount {
				index_score.Sc_significant_nk_total_colcount--
			}
		}

		list_scores = append(list_scores, index_score)
	}

	// if no index is usable for lookup, table must be scanned.

	if len(list_scores) == 0 || xtbl.Tbl_hint_index == "0" {
		score_for_base_table_scan := &Index_score{
			Sc_xtable:           xtbl,
			Sc_base_table_qname: xtbl.Tbl_qname,
			Sc_base_table_alias: xtbl.Tbl_alias,
			Sc_indexname:        gtabledef.Base.Td_index_name,
			Sc_indexdef:         gtabledef.Base,
			Sc_sortkind:         SC_SORTKIND_CLUSTERED_KEY,
			Sc_lookup_type:      csr.SC_LOOKUP_BASE_TABLE_SCAN,
		}

		score_for_base_table_scan.Sc_condinfos = create_condinfos_list(condinfos_map, score_for_base_table_scan)

		if len(score_for_base_table_scan.Sc_condinfos) == 0 {
			score_for_base_table_scan = nil
		}

		if entry, ok := opz.Map_xtbl[xtbl]; ok == true {
			if last_table, ok := opz.Last_table_in_from[entry.xtbl_from]; ok == true {
				if xtbl == last_table { // if table is last table in JOIN tree, a basic scan without Condinfos is more efficient, to avoid evaluating condition expressions twice.
					score_for_base_table_scan = nil
				}
			}
		}

		if rsql.G_DEBUG_FLAG {
			if score_for_base_table_scan != nil {
				fmt.Println("---- SCORE FOR BASE TABLE SCAN ----")
				fmt.Println(score_for_base_table_scan.String())
			}
		}

		return score_for_base_table_scan // nil if no Condinfos, or for last table in list
	}

	// fill in Sc_significant_weight and Sc_significant_weight_max fields

	sort.Sort(List_of_scores_2(list_scores)) // sort in-place. Thanks to this sort, common multiple value won't change randomly.

	common_multiple := list_scores[0].Sc_significant_nk_total_colcount

	for _, score := range list_scores { // find a common multiple (note: it is not the least common multiple !) for all Sc_significant_nk_total_colcount
		if common_multiple%score.Sc_significant_nk_total_colcount != 0 {
			common_multiple = common_multiple * score.Sc_significant_nk_total_colcount
		}
	}

	for _, score := range list_scores {
		score.Sc_significant_weight_max = common_multiple

		score.Sc_significant_weight = score.Sc_significant_nk_leading_colcount * (common_multiple / score.Sc_significant_nk_total_colcount)
	}

	// sort the list of scores

	sort.Sort(List_of_scores(list_scores)) // sort in-place

	winning_score := list_scores[0]

	winning_score.Sc_condinfos = create_condinfos_list(condinfos_map, winning_score)

	// if index hint, choose the specified index if exists

	if xtbl.Tbl_hint_index != "" {
		for _, score := range list_scores {
			if score.Sc_indexname == xtbl.Tbl_hint_index {
				winning_score = score
				break
			}
		}
	}

	if rsql.G_DEBUG_FLAG {
		fmt.Println("--- LIST OF SCORES FOR FAST LOOKUP ---")
		for i, score := range list_scores {
			fmt.Printf("SCORE %d\n", i)
			fmt.Println(score.String() + "\n")
		}

		fmt.Printf("------------------ winning score: %s\n\n", winning_score.Sc_indexname)
	}

	return winning_score
}

type List_of_scores []*Index_score

func (list List_of_scores) Len() int {

	return len(list)
}

func (list List_of_scores) Swap(i, j int) {

	list[i], list[j] = list[j], list[i]
}

// The sort is done in the following order:
//
//           1) by Sc_lookup_type                        SC_LOOKUP_EQUALITY, SC_LOOKUP_RANGE
//           2) by Sc_significant_weight (desc):         the portion of index that is used for search
//           3) by Sc_sortkind:                          SC_SORTKIND_CLUSTERED_KEY, _NONCLUSTERED_PRIMARY_KEY, _NONCLUSTERED_UNIQUE, _NONCLUSTERED_INDEX
//           4) by Sc_significant_nk_leading_colcount
//           5) by Sc_indexname                          as tiebreaker
//
func (list List_of_scores) Less(i, j int) bool {

	// sort by Sc_lookup_type

	if list[i].Sc_lookup_type != list[j].Sc_lookup_type {
		return list[i].Sc_lookup_type < list[j].Sc_lookup_type
	}

	// sort by Sc_significant_weight (desc order)

	if list[i].Sc_significant_weight != list[j].Sc_significant_weight {
		return list[i].Sc_significant_weight > list[j].Sc_significant_weight // descending order
	}

	// sort by Sc_sortkind

	if list[i].Sc_sortkind != list[j].Sc_sortkind {
		return list[i].Sc_sortkind < list[j].Sc_sortkind
	}

	// sort by Sc_significant_nk_leading_colcount

	if list[i].Sc_significant_nk_leading_colcount != list[j].Sc_significant_nk_leading_colcount {
		return list[i].Sc_significant_nk_leading_colcount < list[j].Sc_significant_nk_leading_colcount
	}

	// sort by Sc_indexname

	return list[i].Sc_indexname < list[j].Sc_indexname
}

type List_of_scores_2 []*Index_score

func (list List_of_scores_2) Len() int {

	return len(list)
}

func (list List_of_scores_2) Swap(i, j int) {

	list[i], list[j] = list[j], list[i]
}

// Sort scores in descending Sc_significant_nk_total_colcount order.
// This is done to have a small common multiple.
// This is done also for aesthetical reason because the common multiple value may have different value if order of scores in list is different (because the order of this list is random).
//
func (list List_of_scores_2) Less(i, j int) bool {

	return list[i].Sc_significant_nk_total_colcount > list[j].Sc_significant_nk_total_colcount
}

func (score *Index_score) String() string {
	var s string

	s = fmt.Sprintf("base table name:              %s\n", score.Sc_base_table_qname)
	s += fmt.Sprintf("base table alias:             %s\n", score.Sc_base_table_alias)
	s += fmt.Sprintf("index name:                   %s\n", score.Sc_indexname)
	s += fmt.Sprintf("lookup type:                  %s\n", score.Sc_lookup_type)
	s += fmt.Sprintf("sort kind:                    %s\n", score.Sc_sortkind)

	s += "nk sargs for equality:        "
	for _, sarginfo := range score.Sc_sarginfos_equal {
		prim_name := "nil"
		if sarginfo != nil {
			prim_name = sarginfo.Tok_a_column.Prim_name
		}
		s += fmt.Sprintf(" %-8s", prim_name)
	}
	s += "\n"

	s += "nk sargs for low:             "
	for _, sarginfo := range score.Sc_sarginfos_low {
		prim_name := "nil"
		if sarginfo != nil {
			prim_name = sarginfo.Tok_a_column.Prim_name
		}
		s += fmt.Sprintf(" %-8s", prim_name)
	}
	s += "\n"

	s += "nk sargs for high:            "
	for _, sarginfo := range score.Sc_sarginfos_high {
		prim_name := "nil"
		if sarginfo != nil {
			prim_name = sarginfo.Tok_a_column.Prim_name
		}
		s += fmt.Sprintf(" %-8s", prim_name)
	}
	s += "\n"

	s += fmt.Sprintf("nk leading/total:             %2d %2d\n", score.Sc_nk_leading_colcount, score.Sc_nk_total_colcount)
	s += fmt.Sprintf("significant nk leading/total: %2d %2d\n", score.Sc_significant_nk_leading_colcount, score.Sc_significant_nk_total_colcount)
	s += fmt.Sprintf("significant weight/max:       %2d %2d\n", score.Sc_significant_weight, score.Sc_significant_weight_max)

	s += fmt.Sprintf("nb Condinfos:                 %2d\n", len(score.Sc_condinfos))

	return s
}

// create list of Condinfos for the table specified by index_score.
//
// Returns nil if no Condinfos found.
//
func create_condinfos_list(condinfos_map map[Condkey]*ast.Condinfo, index_score *Index_score) []*ast.Condinfo {
	var list []*ast.Condinfo
	var xtable *ast.Xtable_table

	xtable = index_score.Sc_xtable

	// fill in all conditions for the table

	for condkey, condinfo := range condinfos_map {
		if condkey.Cond_xtable != xtable {
			continue
		}

		list = append(list, condinfo)
	}

	// sort the list

	sort.Sort(List_of_condinfos(list))

	// remove Sargs from list

	switch index_score.Sc_lookup_type {
	case csr.SC_LOOKUP_EQUALITY:
	LOOP_CONDINFOS_EQ:
		for i, condinfo := range list {
			for _, sarginfo := range index_score.Sc_sarginfos_equal[:index_score.Sc_nk_leading_colcount] {
				if condinfo.Cond_dataslot == sarginfo.Comp_dataslot {
					list[i] = nil
					continue LOOP_CONDINFOS_EQ
				}
			}
		}

	case csr.SC_LOOKUP_RANGE:
	LOOP_CONDINFOS_RANGE:
		for i, condinfo := range list {
			for _, sarginfo := range index_score.Sc_sarginfos_low[:index_score.Sc_nk_leading_colcount] {
				if condinfo.Cond_dataslot == sarginfo.Comp_dataslot {
					list[i] = nil
					continue LOOP_CONDINFOS_RANGE
				}
			}

			if sarginfo := index_score.Sc_sarginfos_high[0]; sarginfo != nil {
				if condinfo.Cond_dataslot == sarginfo.Comp_dataslot {
					list[i] = nil
					continue LOOP_CONDINFOS_RANGE
				}
			}
		}

	case csr.SC_LOOKUP_BASE_TABLE_SCAN:
		// keep all Condinfos

	default:
		panic("impossible")
	}

	remove_nil_in_list_of_condinfos(&list) // if no element in list, set list to nil

	return list // nil if no Condinfos
}

// remove nil entries in list.
//
func remove_nil_in_list_of_condinfos(list *[]*ast.Condinfo) {

	if len(*list) == 0 {
		*list = nil
		return
	}

	// compact the list in-place, by removing all nil Conditions

	j := 0

	for i, c := range *list {
		if c == nil {
			continue
		}

		(*list)[j] = (*list)[i]
		j++
	}

	*list = (*list)[:j]

	if len(*list) == 0 {
		*list = nil
	}
}

type List_of_condinfos []*ast.Condinfo

func (list List_of_condinfos) Len() int {

	return len(list)
}

func (list List_of_condinfos) Swap(i, j int) {

	list[i], list[j] = list[j], list[i]
}

func (list List_of_condinfos) Less(i, j int) bool {

	return list[i].Cond_seqno < list[j].Cond_seqno
}

//==========================================================

// optimize_table_cursor_for_lookup_equality puts optimization information in the cursor.
//
// The cursor will use this information to access the table and filter the results.
//
func optimize_table_cursor_for_lookup_equality(curs *csr.Cursor_table, index_score *Index_score) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(index_score.Sc_lookup_type == csr.SC_LOOKUP_EQUALITY)

	curs.Lookup_type = csr.SC_LOOKUP_EQUALITY

	// index to use, and index leaf record

	curs.Index_to_use = index_score.Sc_indexdef

	curs.Index_leaf_record, curs.Index_is_covering = data.New_index_leaf_row_from_base_fields(rsql.KIND_COL_LEAF, curs.Index_to_use, curs.Base_tabledef, curs.Base_fields)

	// create a leaf row (base or index row) for lookup, with dataslots pointing to Sarg dataslots

	leaf_row_for_lookup := data.New_leaf_row_for_nk_lookup(rsql.KIND_RO_LEAF, curs.Index_to_use) // row with NULL dataslots for index nk fields

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ { // fill with expressions for leading nk fields. Remaining nk fields are NULL dataslots.
		sarginfo := index_score.Sc_sarginfos_equal[i]

		pos := i
		if curs.Index_to_use.Td_type == rsql.TD_TYPE_BASE_TABLE {
			pos = int(curs.Index_to_use.Td_nk[i].Cd_base_seqno)
		}

		leaf_row_for_lookup[pos] = sarginfo.Tok_expr.Prim_dataslot
	}

	curs.Index_lookup = leaf_row_for_lookup

	// create one basicblock for evaluating all expressions of native key Sargs. The first cursor.Next call must evaluate this basicblock.

	curs.Index_nk_sargs_expressions = make([]rsql.IDataslot, 0, index_score.Sc_nk_leading_colcount)

	basicblock := &rsql.Basicblock{}
	bbk := (*emit.BBlock)(basicblock)

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ {
		sarginfo := index_score.Sc_sarginfos_equal[i]
		bbk.Instr_walk_Token_primary(sarginfo.Tok_expr)
		curs.Index_nk_sargs_expressions = append(curs.Index_nk_sargs_expressions, sarginfo.Tok_expr.Prim_dataslot)
	}

	if len(basicblock.Bb_instr3ac_list) != 0 {
		curs.Index_nk_sargs_expression_bb = basicblock
	}

	// create list of basicblocks, for each native key Sarg. The instructions evaluate the comparison operator, but not the expression, as it has already been evaluated previously.

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ {
		sarginfo := index_score.Sc_sarginfos_equal[i]

		basicblock := &rsql.Basicblock{}
		bbk := (*emit.BBlock)(basicblock)

		bbk.Instr_walk_Token_primary_for_sarg_comparison(sarginfo.Tok_comp, sarginfo.Comp_args_reversed) // doesn't walk the expression subtree

		rsql.Assert(len(basicblock.Bb_instr3ac_list) != 0)
		curs.Index_nk_sargs_bb = append(curs.Index_nk_sargs_bb, basicblock)
	}

	// create list of result dataslots, for each native key Sarg

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ {
		sarginfo := index_score.Sc_sarginfos_equal[i]

		curs.Index_nk_sargs_result = append(curs.Index_nk_sargs_result, sarginfo.Comp_dataslot)
	}

	// if Sargs are specified for all native key fields, at most one result record can be returned

	if index_score.Sc_nk_leading_colcount == index_score.Sc_nk_total_colcount {
		curs.Index_nk_fully_sarged = true
	}

	// create list of basicblocks, for each Condinfo

	for _, condinfo := range index_score.Sc_condinfos {
		basicblock := &rsql.Basicblock{}
		bbk := (*emit.BBlock)(basicblock)

		bbk.Instr_walk_Token_primary(condinfo.Tok_cond)

		curs.Index_other_conditions_bb = append(curs.Index_other_conditions_bb, basicblock)
	}

	// create list of result dataslots, for each Condinfo

	for _, condinfo := range index_score.Sc_condinfos {
		curs.Index_other_conditions_result = append(curs.Index_other_conditions_result, condinfo.Cond_dataslot)
	}

	// create Selpads

	if curs.Index_selpad, rsql_err = btree.New_selpad(curs.Index_to_use); rsql_err != nil {
		return rsql_err
	}

	if curs.Index_is_covering == false {
		if curs.Base_table_selpad, rsql_err = btree.New_selpad(curs.Base_tabledef); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

// optimize_table_cursor_for_lookup_range puts optimization information in the cursor.
//
// The cursor will use this information to access the table and filter the results.
//
func optimize_table_cursor_for_lookup_range(curs *csr.Cursor_table, index_score *Index_score) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(index_score.Sc_lookup_type == csr.SC_LOOKUP_RANGE)

	curs.Lookup_type = csr.SC_LOOKUP_RANGE

	// index to use, and index leaf record

	curs.Index_to_use = index_score.Sc_indexdef

	curs.Index_leaf_record, curs.Index_is_covering = data.New_index_leaf_row_from_base_fields(rsql.KIND_COL_LEAF, curs.Index_to_use, curs.Base_tabledef, curs.Base_fields)

	// create a leaf row (base or index row) for lookup, with dataslots pointing to Sarg dataslots

	leaf_row_for_lookup := data.New_leaf_row_for_nk_lookup(rsql.KIND_RO_LEAF, curs.Index_to_use) // row with NULL dataslots for index nk fields

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ { // fill with expressions for leading nk fields. Remaining nk fields are NULL dataslots.
		sarginfo := index_score.Sc_sarginfos_low[i]

		pos := i
		if curs.Index_to_use.Td_type == rsql.TD_TYPE_BASE_TABLE {
			pos = int(curs.Index_to_use.Td_nk[i].Cd_base_seqno)
		}

		leaf_row_for_lookup[pos] = sarginfo.Tok_expr.Prim_dataslot
	}

	curs.Index_lookup = leaf_row_for_lookup

	// create one basicblock for evaluating all expressions of native key Sargs. The first cursor.Next call must evaluate this basicblock.

	curs.Index_nk_sargs_expressions = make([]rsql.IDataslot, 0, index_score.Sc_nk_leading_colcount)

	basicblock := &rsql.Basicblock{}
	bbk := (*emit.BBlock)(basicblock)

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ { // for low limit
		sarginfo := index_score.Sc_sarginfos_low[i]
		bbk.Instr_walk_Token_primary(sarginfo.Tok_expr)
		curs.Index_nk_sargs_expressions = append(curs.Index_nk_sargs_expressions, sarginfo.Tok_expr.Prim_dataslot)
	}

	if sarginfo := index_score.Sc_sarginfos_high[0]; sarginfo != nil { // for high limit
		bbk.Instr_walk_Token_primary(sarginfo.Tok_expr)
		curs.Index_nk_sargs_expressions = append(curs.Index_nk_sargs_expressions, sarginfo.Tok_expr.Prim_dataslot)
	}

	if len(basicblock.Bb_instr3ac_list) != 0 {
		curs.Index_nk_sargs_expression_bb = basicblock
	}

	// create list of basicblocks, for each native key Sarg. The instructions evaluate the comparison operator, but not the expression, as it has already been evaluated previously.

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ {
		sarginfo := index_score.Sc_sarginfos_low[i]

		basicblock := &rsql.Basicblock{}
		bbk := (*emit.BBlock)(basicblock)

		bbk.Instr_walk_Token_primary_for_sarg_comparison(sarginfo.Tok_comp, sarginfo.Comp_args_reversed) // doesn't walk the expression subtree

		rsql.Assert(len(basicblock.Bb_instr3ac_list) != 0)
		curs.Index_nk_sargs_bb = append(curs.Index_nk_sargs_bb, basicblock)
	}

	// create list of result dataslots, for each native key Sarg

	for i := 0; i < index_score.Sc_nk_leading_colcount; i++ {
		sarginfo := index_score.Sc_sarginfos_low[i]

		curs.Index_nk_sargs_result = append(curs.Index_nk_sargs_result, sarginfo.Comp_dataslot)
	}

	// create basicblock and result dataslot for high limit

	if sarginfo := index_score.Sc_sarginfos_high[0]; sarginfo != nil {
		basicblock := &rsql.Basicblock{}
		bbk := (*emit.BBlock)(basicblock)

		bbk.Instr_walk_Token_primary_for_sarg_comparison(sarginfo.Tok_comp, sarginfo.Comp_args_reversed) // doesn't walk the expression subtree

		rsql.Assert(len(basicblock.Bb_instr3ac_list) != 0)
		curs.Index_nk_high_limit_bb = basicblock

		curs.Index_nk_high_limit_col_dataslot = sarginfo.Tok_a.Prim_dataslot // Tok_a is the column of the index.     Tok_a <op> expression
		curs.Index_nk_high_limit_result = sarginfo.Comp_dataslot
	}

	// create list of basicblocks, for each Condinfo

	for _, condinfo := range index_score.Sc_condinfos {
		basicblock := &rsql.Basicblock{}
		bbk := (*emit.BBlock)(basicblock)

		bbk.Instr_walk_Token_primary(condinfo.Tok_cond)

		curs.Index_other_conditions_bb = append(curs.Index_other_conditions_bb, basicblock)
	}

	// create list of result dataslots, for each Condinfo

	for _, condinfo := range index_score.Sc_condinfos {
		curs.Index_other_conditions_result = append(curs.Index_other_conditions_result, condinfo.Cond_dataslot)
	}

	// create Selpads

	if curs.Index_selpad, rsql_err = btree.New_selpad(curs.Index_to_use); rsql_err != nil {
		return rsql_err
	}

	if curs.Index_is_covering == false {
		if curs.Base_table_selpad, rsql_err = btree.New_selpad(curs.Base_tabledef); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

// optimize_table_cursor_for_lookup_base_table_scan puts optimization information in the cursor.
//
// The cursor will use this information to access the table and filter the results.
//
func optimize_table_cursor_for_lookup_base_table_scan(curs *csr.Cursor_table, index_score *Index_score) *rsql.Error {

	rsql.Assert(index_score.Sc_lookup_type == csr.SC_LOOKUP_BASE_TABLE_SCAN)

	curs.Lookup_type = csr.SC_LOOKUP_BASE_TABLE_SCAN

	// create list of basicblocks, for each Condinfo

	for _, condinfo := range index_score.Sc_condinfos {
		basicblock := &rsql.Basicblock{}
		bbk := (*emit.BBlock)(basicblock)

		bbk.Instr_walk_Token_primary(condinfo.Tok_cond)

		curs.Index_other_conditions_bb = append(curs.Index_other_conditions_bb, basicblock)
	}

	// create list of result dataslots, for each Condinfo

	for _, condinfo := range index_score.Sc_condinfos {
		curs.Index_other_conditions_result = append(curs.Index_other_conditions_result, condinfo.Cond_dataslot)
	}

	return nil
}
