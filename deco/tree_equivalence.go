package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
)

func compare_Token_primaries_shallow(tok_a *ast.Token_primary, tok_b *ast.Token_primary) bool {

	if tok_a.Prim_dataslot.Kind() != tok_b.Prim_dataslot.Kind() {
		return false
	}

	// for constants or constant-folded values, variables and columns, check if dataslots are the same

	if tok_a.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF|rsql.KIND_COL_LEAF) != 0 { // during decorating stage, all KIND_RO_LEAF dataslots have been put in decor.CONSTANTS table
		if tok_a.Prim_dataslot != tok_b.Prim_dataslot {
			return false
		}

		return true
	}

	// here, tokens are KIND_TMP

	if tok_a.Prim_instruction_code != tok_b.Prim_instruction_code {
		return false
	}

	// check count of arguments

	if len(tok_a.Prim_listexpr) != len(tok_b.Prim_listexpr) {
		return false
	}

	if (tok_a.Prim_left == nil && tok_b.Prim_left != nil) || (tok_a.Prim_left != nil && tok_b.Prim_left == nil) {
		return false
	}

	if (tok_a.Prim_right == nil && tok_b.Prim_right != nil) || (tok_a.Prim_right != nil && tok_b.Prim_right == nil) {
		return false
	}

	if (tok_a.Prim_syscollator == nil && tok_b.Prim_syscollator != nil) || (tok_a.Prim_syscollator != nil && tok_b.Prim_syscollator == nil) {
		return false
	}

	if (tok_a.Prim_syslanguage == nil && tok_b.Prim_syslanguage != nil) || (tok_a.Prim_syslanguage != nil && tok_b.Prim_syslanguage == nil) {
		return false
	}

	// check dataslots attributes are same (datatype, precision, scale, fixlen_flag)

	if data.Data_XXX_result_properties_equal(tok_a.Prim_dataslot, tok_b.Prim_dataslot) == false {
		return false
	}

	return true
}

func Compare_tok_trees(tok_a *ast.Token_primary, tok_b *ast.Token_primary) bool {

	// shallow comparison of tokens

	if compare_Token_primaries_shallow(tok_a, tok_b) == false {
		return false
	}

	if tok_a.Prim_dataslot.Kind()&(rsql.KIND_RO_LEAF|rsql.KIND_VAR_LEAF|rsql.KIND_COL_LEAF) != 0 { // for KIND_RO_LEAF, children must not be walked
		return true
	}

	// compare children

	if tok_a.Prim_left != nil && Compare_tok_trees(tok_a.Prim_left, tok_b.Prim_left) == false {
		return false
	}

	if tok_a.Prim_right != nil && Compare_tok_trees(tok_a.Prim_right, tok_b.Prim_right) == false {
		return false
	}

	if tok_a.Prim_syscollator != nil && Compare_tok_trees(tok_a.Prim_syscollator, tok_b.Prim_syscollator) == false {
		return false
	}

	if tok_a.Prim_syslanguage != nil && Compare_tok_trees(tok_a.Prim_syslanguage, tok_b.Prim_syslanguage) == false {
		return false
	}

	for i := 0; i < len(tok_a.Prim_listexpr); i++ {
		if Compare_tok_trees(tok_a.Prim_listexpr[i], tok_b.Prim_listexpr[i]) == false {
			return false
		}
	}

	return true
}
