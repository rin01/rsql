package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
)

// Dictionary_CONSTANT contains all constants dataslots in a SQL batch, with no duplicate. Table_CONSTANT is indeed a set of constant values.
// These dataslots must be KIND_RO_LEAF.
//
type Dictionary_CONSTANT struct {
	Tbl map[uint32][]rsql.IDataslot
}

func (dc *Dictionary_CONSTANT) Initialize() {

	dc.Tbl = make(map[uint32][]rsql.IDataslot, SPEC_DEFAULT_DICTIONARY_CONSTANT_CAPACITY) // initial capacity 50 is good enough
}

// Get_RO_LEAF_Data_XXX returns a pointer to a KIND_RO_LEAF Data_XXX.
// It can point to the same dataslot as the argument passed, or to a dataslot with same value if it already exists in the constant table.
//
func (dc *Dictionary_CONSTANT) Get_RO_LEAF_Data_XXX(dataslot rsql.IDataslot) (res rsql.IDataslot, hashval uint32) {
	var (
		bucket []rsql.IDataslot
	)

	// check if dataslot already exists

	hashval = dataslot.RO_LEAF_hashval()

	bucket = dc.Tbl[hashval]

	for _, elem := range bucket { // check if bucket already contains dataslot of same value
		if data.RO_LEAF_Data_XXX_equal(dataslot, elem) == true {
			return elem, hashval
		}
	}

	// here, dataslot has not been found in bucket

	dc.Tbl[hashval] = append(bucket, dataslot) // insert it in bucket

	return dataslot, hashval
}

// Const_walk_Token_primary walks the AST. If a Token_primary has a constant dataslot, it is put in a table of constants.
//
// If the constant dataslot already exists in the table, it is reused.
//
func (dc *Dictionary_CONSTANT) Const_walk_Token_primary(tok *ast.Token_primary) {

	/* if KIND_RO_LEAF, KIND_VAR_LEAF, KIND_COL_LEAF, return */

	switch tok.Prim_dataslot.Kind() {
	case rsql.KIND_VAR_LEAF, rsql.KIND_COL_LEAF: // in this case, it is a variable or a column. Just return.
		return

	case rsql.KIND_RO_LEAF: // literal constant, or immutable value computed by constant folding.
		tok.Prim_dataslot, _ = dc.Get_RO_LEAF_Data_XXX(tok.Prim_dataslot) // return unique RO_LEAF dataslot stored in constant table
		return
	}

	/* walk syscollator and syslanguage operands */

	if tok.Prim_syscollator != nil {
		dc.Const_walk_Token_primary(tok.Prim_syscollator)
	}

	if tok.Prim_syslanguage != nil {
		dc.Const_walk_Token_primary(tok.Prim_syslanguage)
	}

	/* walk operands */

	switch tok.Prim_type {
	case ast.TOK_PRIM_OPERATOR:
		switch tok.Prim_cardinality {
		case ast.TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL:
			dc.Const_walk_Token_primary(tok.Prim_left)

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS,
			ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE:
			dc.Const_walk_Token_primary(tok.Prim_left)

			dc.Const_walk_Token_primary(tok.Prim_right)

		case ast.TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST:
			dc.Const_walk_Token_primary(tok.Prim_left)

			for _, arg := range tok.Prim_listexpr {
				dc.Const_walk_Token_primary(arg)
			}

		default:
			panic("cardinality unknown")
		}

	case ast.TOK_PRIM_CAST:
		dc.Const_walk_Token_primary(tok.Prim_left) // walk left operand

	case ast.TOK_PRIM_SYSFUNC:
		for _, arg := range tok.Prim_listexpr {
			dc.Const_walk_Token_primary(arg) // walk each operand
		}

	case ast.TOK_PRIM_CASE:
		for _, arg := range tok.Prim_listexpr {
			dc.Const_walk_Token_primary(arg) // walk each operand
		}

		/*
		   case ast.TOK_PRIM_SUBEXP :
		       switch ( tok->prim_subtype )
		         {
		           case TOK_PRIM_SUBEXP_CASE :

		             for ( i=0; i < tok->prim_listexpr_count; i++ )
		               {
		                 Const_walk_Token_primary(tok->prim_listexpr[i], e);    // walk each operand
		               }

		           default :
		             fatal("bof");
		             break;
		         }
		     }

		   case ast.TOK_PRIM_SUBQUERY :
		       return
		*/
	default:
		panic("impossible")
	}
}
