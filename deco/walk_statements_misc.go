package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
	"rsql/lex"
)

// wacf_Token_stmt_USE walks the USE statement.
//
// This statement overwrites global variables _@current_db_name, _@current_user_name, etc, with new values.
//
func (decor *Decorator) wacf_Token_stmt_USE(tok *ast.Token_stmt_USE) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err                *rsql.Error
		database_name_token_OUT *ast.Token_primary
		database_dbid_token_OUT *ast.Token_primary
		schema_name_token_OUT   *ast.Token_primary
		schema_schid_token_OUT  *ast.Token_primary
		user_name_token_OUT     *ast.Token_primary
		user_palid_token_OUT    *ast.Token_primary
	)

	rsql.Assert(tok.Tok_category == ast.TOK_STMT_USE)

	// create and walk OUT variables

	database_name_token_OUT = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_NAME)
	database_name_token_OUT.Tok_batch_line = tok.Tok_batch_line
	if rsql_err = decor.walk_Token_primary_and_simplify(database_name_token_OUT); rsql_err != nil { // walk the operand
		return rsql_err
	}

	database_dbid_token_OUT = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_ID)
	database_dbid_token_OUT.Tok_batch_line = tok.Tok_batch_line
	if rsql_err = decor.walk_Token_primary_and_simplify(database_dbid_token_OUT); rsql_err != nil { // walk the operand
		return rsql_err
	}

	schema_name_token_OUT = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_SCHEMA_NAME)
	schema_name_token_OUT.Tok_batch_line = tok.Tok_batch_line
	if rsql_err = decor.walk_Token_primary_and_simplify(schema_name_token_OUT); rsql_err != nil { // walk the operand
		return rsql_err
	}

	schema_schid_token_OUT = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_SCHEMA_ID)
	schema_schid_token_OUT.Tok_batch_line = tok.Tok_batch_line
	if rsql_err = decor.walk_Token_primary_and_simplify(schema_schid_token_OUT); rsql_err != nil { // walk the operand
		return rsql_err
	}

	user_name_token_OUT = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_USER_NAME)
	user_name_token_OUT.Tok_batch_line = tok.Tok_batch_line
	if rsql_err = decor.walk_Token_primary_and_simplify(user_name_token_OUT); rsql_err != nil { // walk the operand
		return rsql_err
	}

	user_palid_token_OUT = decor.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_USER_ID)
	user_palid_token_OUT.Tok_batch_line = tok.Tok_batch_line
	if rsql_err = decor.walk_Token_primary_and_simplify(user_palid_token_OUT); rsql_err != nil { // walk the operand
		return rsql_err
	}

	tok.Use_database_name_OUT = database_name_token_OUT
	tok.Use_database_dbid_OUT = database_dbid_token_OUT
	tok.Use_schema_name_OUT = schema_name_token_OUT
	tok.Use_schema_schid_OUT = schema_schid_token_OUT
	tok.Use_user_name_OUT = user_name_token_OUT
	tok.Use_user_palid_OUT = user_palid_token_OUT

	return nil
}

func (decor *Decorator) wacf_Token_stmt_THROW(tok *ast.Token_stmt_THROW) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_THROW)

	if tok.Th_error_number, rsql_err = decor.Wacf_expression_with_promotion(tok.Th_error_number, rsql.DATATYPE_INT); rsql_err != nil {
		return rsql_err
	}

	if tok.Th_message, rsql_err = decor.Wacf_expression_with_promotion(tok.Th_message, rsql.DATATYPE_VARCHAR); rsql_err != nil {
		return rsql_err
	}

	if tok.Th_severity, rsql_err = decor.Wacf_expression_with_promotion(tok.Th_severity, rsql.DATATYPE_INT); rsql_err != nil {
		return rsql_err
	}

	if tok.Th_state, rsql_err = decor.Wacf_expression_with_promotion(tok.Th_state, rsql.DATATYPE_TINYINT); rsql_err != nil {
		return rsql_err
	}

	// check error number if literal

	if val, ok := data.Get_value_from_literal_xxxINT(tok.Th_error_number.Prim_dataslot); ok == true { // if error number argument is a integer literal, check if valid range
		if val < rsql.THROW_MESSAGE_ID_LIMIT_LOWEST || val > rsql.THROW_MESSAGE_ID_LIMIT_HIGHEST {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_THROW_BAD_ERROR_NUMBER, rsql.ERROR_BATCH_ABORT)
		}
	}

	return nil
}

func (decor *Decorator) wacf_Token_stmt_PRINT(tok *ast.Token_stmt_PRINT) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err *rsql.Error
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_PRINT)

	for _, tok_expr := range tok.Pr_listexpr {
		if rsql_err = decor.Wacf_expression(tok_expr); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}
