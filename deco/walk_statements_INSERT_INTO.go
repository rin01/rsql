package deco

import (
	"rsql"
	"rsql/ast"
	"rsql/data"
	"rsql/dict"
)

func (decor *Decorator) wacf_Token_stmt_INSERT_INTO(tok *ast.Token_stmt_INSERT_INTO) *rsql.Error { // wacf means walk-constfold
	var (
		rsql_err            *rsql.Error
		gtabledef           *rsql.GTabledef
		base_tabledef       *rsql.Tabledef
		base_row            rsql.Row
		col_count           int
		values              []*ast.Token_primary
		tok_promotion_token *ast.Token_primary
		colname             string
		coldef              *rsql.Coldef
		ok                  bool
		sample_Data_XXX_to  rsql.IDataslot
		listcol_base_seqno  []uint16
	)

	rsql.Assert(tok.Category() == ast.TOK_STMT_INSERT_INTO)

	// get GTabledef from dict

	switch {
	case tok.Ins_table_qname.Is_variable_table():
		if gtabledef, rsql_err = decor.Temptable_bag.Get(tok.Ins_table_qname.Object_name); rsql_err != nil {
			return rsql_err
		}

	default:
		if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(tok.Ins_table_qname); rsql_err != nil {
			return rsql_err
		}
	}

	tok.Ins_gtabledef = gtabledef

	base_tabledef = gtabledef.Base

	// create list of column names if missing

	if len(tok.Ins_listcol) == 0 {
		tok.Ins_listcol = make([]string, 0, len(base_tabledef.Td_coldefs))

		rsql.Assert(base_tabledef.Td_coldefs[0].Cd_colname == rsql.ROWID)

		for _, coldef := range base_tabledef.Td_coldefs { // for all columns
			if coldef.Cd_autonum != 0 { // except ROWID and IDENTITY columns
				continue
			}
			tok.Ins_listcol = append(tok.Ins_listcol, coldef.Cd_colname)
		}
	}

	// create base row with all column dataslots

	base_row = make(rsql.Row, len(base_tabledef.Td_coldefs)) // base_row is the row to insert

	for i, coldef := range base_tabledef.Td_coldefs {
		base_row[i] = data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
	}

	tok.Ins_base_row = base_row // base_row contains NULL values

	// create list of base_seqno for listed columns

	col_count = len(tok.Ins_listcol)

	listcol_base_seqno = make([]uint16, 0, col_count)

	for _, colname = range tok.Ins_listcol {
		if coldef, ok = gtabledef.Colmap[colname]; ok == false {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_MASTER_COLUMN_NAME_NOT_EXISTS, rsql.ERROR_BATCH_ABORT, colname)
		}

		if coldef.Cd_autonum == rsql.CD_AUTONUM_IDENTITY {
			tok.Ins_explicit_IDENTITY_provided = true
		}

		listcol_base_seqno = append(listcol_base_seqno, coldef.Cd_base_seqno)
	}

	tok.Ins_listcol_base_seqno = listcol_base_seqno

	// walk columns

	switch tok.Ins_instruction_code {
	case rsql.INSTR_STMT_INSERT_VALUES:
		for _, values = range tok.Ins_values_array { // for each VALUES clause. Note: the VALUES dataslots will be put in a copy of tok.Ins_base_row by the rsql/emit package.
			if len(values) != col_count {
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_VALUES_LISTEXPR_COUNT_FOR_INSERT_INTO, rsql.ERROR_BATCH_ABORT)
			}

			// for each field of a VALUES

			for k, _ := range values {
				colname = tok.Ins_listcol[k]

				if coldef, ok = gtabledef.Colmap[colname]; ok == false {
					panic("impossible")
				}

				// replace DEFAULT clause

				if values[k] == nil {
					values[k] = decor.Create_TOK_PRIM_LITERAL_NULL() // for the moment, DEFAULT column value is NULL
				}

				// walk column expression, put promotion token and constfold

				sample_Data_XXX_to = base_row[coldef.Cd_base_seqno]

				if tok_promotion_token, rsql_err = decor.Wacf_expression_FITTING_for_INSERT_column(values[k], sample_Data_XXX_to); rsql_err != nil { // for VARBINARY and VARCHAR, precision may be smaller than precision of sample_Data_XXX_to
					return rsql_err
				}

				values[k] = tok_promotion_token // may be values[k]
			}
		}

		return nil

	case rsql.INSTR_STMT_INSERT_SELECT:
		// walk SELECT clause

		xtable := tok.Ins_xtable // SELECT or UNIONed SELECTs
		rsql.Assert(xtable.Type() == ast.XT_SELECT || xtable.Type() == ast.XT_UNION)

		if rsql_err := decor.wacf_Xtable(xtable); rsql_err != nil {
			return rsql_err
		}

		if xtable.Col_count() != col_count {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_SELECT_COL_COUNT_FOR_INSERT_INTO, rsql.ERROR_BATCH_ABORT)
		}

		// optimize SELECT tables

		optimizer := New_optimizer()

		if rsql_err := optimizer.Optimize_all(xtable); rsql_err != nil {
			return rsql_err
		}

		// create CAST tokens from SELECT dataslots to base_row dataslots

		tok.Ins_conversion_tokens = make([]*ast.Token_primary, 0, col_count)

		for i := 0; i < col_count; i++ { // for all columns listed in SELECT clause of SELECT or UNION
			var (
				tok_from         *ast.Token_primary
				conversion_token *ast.Token_primary
			)

			base_seqno := listcol_base_seqno[i]
			target_dataslot := base_row[base_seqno]

			switch xtable.Type() {
			case ast.XT_SELECT:
				tok_from = xtable.(*ast.Xtable_select).Sel_columns[i].Col_expression

			case ast.XT_UNION:
				tok_from = xtable.(*ast.Xtable_union).Un_equalized_row[i]

			default:
				panic("impossible")
			}

			if conversion_token, rsql_err = decor.Token_primary_FITTING_new_to_target_dataslot_for_INSERT(tok_from, target_dataslot); rsql_err != nil { // normal CAST or a function (for date<->varchar conversions), or tok_from if no conversion needed
				return rsql_err
			}

			if conversion_token != tok_from { // conversion is needed
				tok.Ins_conversion_tokens = append(tok.Ins_conversion_tokens, conversion_token)
			} else {
				base_row[base_seqno] = tok_from.Prim_dataslot // for VARBINARY and VARCHAR, precision can be smaller than precision specified in Coldef
			}
		}

		tok.Ins_conversion_basicblock = decor.create_new_Basicblock() // create Basicblock for storing instructions of tok.Ins_conversion_tokens

		return nil

	default:
		panic("impossible")
	}
}

// Token_primary_FITTING_new_to_target_dataslot_for_INSERT creates a CAST Token_primary to convert columns of SELECT into column datatypes of INSERT INTO, with precision that fits with target precision.
//
// For VARBINARY and VARCHAR, precision of the returned token CAN BE SMALLER than precision of target column !
//
// If source and target dataslots are both VARBINARY, a verification token INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY is returned, to check VARBINARY length before insertion in table.
// If source and target dataslots are both VARCHAR, a verification token INSTR_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR is returned, to check VARCHAR length before insertion in table.
//
func (decor *Decorator) Token_primary_FITTING_new_to_target_dataslot_for_INSERT(tok_from *ast.Token_primary, target_dataslot rsql.IDataslot) (*ast.Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		tok_cast *ast.Token_primary
	)

	// if source datatype and target datatype are both VARBINARY, create a verif token

	if tok_from.Prim_dataslot.Datatype() == rsql.DATATYPE_VARBINARY && target_dataslot.Datatype() == rsql.DATATYPE_VARBINARY {
		precision_to := target_dataslot.(*data.VARBINARY).Data_precision

		tok_verif := decor.Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARBINARY_new_with_Dataslot(tok_from, precision_to) // note that this function creates a new dataslot
		if tok_verif == tok_from {                                                                                   // verif token is not needed
			return tok_from, nil // precision of tok_from is <= precision of target
		}

		tok_verif.Prim_dataslot = target_dataslot // replace the dataslot created above by target_dataslot
		return tok_verif, nil
	}

	// if source datatype and target datatype are both VARCHAR, create a verif token

	if tok_from.Prim_dataslot.Datatype() == rsql.DATATYPE_VARCHAR && target_dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
		precision_to := target_dataslot.(*data.VARCHAR).Data_precision
		fixlen_flag_to := target_dataslot.(*data.VARCHAR).Data_fixlen_flag

		tok_verif := decor.Token_primary_SYSFUNC_FIT_NO_TRUNCATE_VARCHAR_new_with_Dataslot(tok_from, precision_to, fixlen_flag_to) // note that this function creates a new dataslot
		if tok_verif == tok_from {                                                                                                 // verif token is not needed
			return tok_from, nil // precision of tok_from is <= precision of target
		}

		tok_verif.Prim_dataslot = target_dataslot // replace the dataslot created above by target_dataslot
		return tok_verif, nil
	}

	// create CAST or conversion token (never plain copy token)

	tok_cast, rsql_err = decor.Token_primary_CAST_EXACT_new_to_target_dataslot(tok_from, target_dataslot) // returns tok_from if datatype, p, s, and fixlen_flag are same for tok_from and target

	return tok_cast, rsql_err
}
