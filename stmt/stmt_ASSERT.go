package stmt

import (
	"strings"

	"rsql"
	"rsql/data"
)

func ASSERT_(dataslot *data.BOOLEAN) *rsql.Error {

	if dataslot.Is_true() { // if true, return
		return nil
	}

	return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ASSERT_FAILED, rsql.ERROR_BATCH_ABORT)
}

func ASSERT_NULL_(dataslot rsql.IDataslot) *rsql.Error {

	if dataslot.Error() == nil && dataslot.NULL_flag() == true { // if NULL, return
		return nil
	}

	return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ASSERT_NULL_FAILED, rsql.ERROR_BATCH_ABORT)
}

func ASSERT_ERROR_(dataslot rsql.IDataslot, error_suffix *data.VARCHAR) *rsql.Error {
	var (
		rsql_err        *rsql.Error
		rsql_err_string string
	)

	if error_suffix.Error() != nil || error_suffix.NULL_flag() == true { // if error_suffix is error or NULL
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ASSERT_ERROR_BAD_SUFFIX, rsql.ERROR_BATCH_ABORT)
	}

	rsql_err = dataslot.Error()

	if rsql_err == nil { // if no error, assertion fails
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ASSERT_ERROR_NONE_FOUND, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err.Severity_id != rsql.ERROR_BATCH_ABORT { // if severity is more serious than BATCH_ABORT, we want assert to stop the batch, by just handing over the error
		return rsql_err
	}

	rsql_err_string = rsql.MESSAGE_MAP[rsql_err.Message_id]

	if strings.HasSuffix(rsql_err_string, error_suffix.Stringval()) { // success, error is the expected one
		return nil
	}

	return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ASSERT_ERROR_FAILED, rsql.ERROR_BATCH_ABORT, rsql_err_string, error_suffix.Stringval()) // error is not the expected one
}
