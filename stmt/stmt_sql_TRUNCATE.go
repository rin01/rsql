package stmt

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
)

func TRUNCATE_TABLE(context *rsql.Context, table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef, shrink_file bool) *rsql.Error {
	var (
		deleted_row_count int64
	)

	if shrink_file == true && context.Ctx_trancount != 0 { // no transaction allowed when files are shrinked, because rollback could be impossible as pages have been removed
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "TRUNCATE TABLE WITH SHRINK_FILE")
	}

	// modify info pages

	wcache := context.Ctx_wcache.(*cache.Wcache)

	deleted_row_count = btree.Truncate_base_table_and_indexes(wcache, gtabledef)

	// confirm modifications

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	if context.Ctx_trancount == 0 {
		wcache.COMMIT() // table pages are written to disk
	}

	// physical shrinking of the files

	if shrink_file == true { // keep only two pages in files (zone allocator 0 and page info)
		rsql.Assert(wcache.Permanent_tables_count() == 0)

		wcache.Filebag().Shrink_file(gtabledef.Base, 2)
		//println("           TRUNCATE WITH SHRINK", gtabledef.Base.Td_tblid)

		for _, tabledef := range gtabledef.Indexmap {
			//println("           TRUNCATE WITH SHRINK", tabledef.Td_tblid)
			wcache.Filebag().Shrink_file(tabledef, 2)
		}

		wcache.Filebag().Sync_modified_files()
	}

	// send record count

	wcache.Wpc_last_rowcount = deleted_row_count

	if context.Ctx_nocount == false {
		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_EXECUTION_FINISHED)) // prepare response type
		context.Ctx_mw.WriteInt64(deleted_row_count)
		if rsql_err := context.Writer_flush(); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}
