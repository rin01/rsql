package stmt

import (
	"log"
	"os"
	"sync/atomic"
	"time"

	"rsql"
	"rsql/cache"
	"rsql/dict"
)

func SHUTDOWN(context *rsql.Context, nowait bool) *rsql.Error {
	var (
		workers_count int
		i             int
	)

	// only sa can run this command

	if context.Ctx_login_id != dict.SA_LOGIN_ID {
		return rsql.New_Error(rsql.ERROR_DICT, rsql.ERROR_MASTER_OPERATION_ALLOWED_ONLY_TO_SA, rsql.ERROR_BATCH_ABORT)
	}

	// shutdown

	log.Println("server is being shutdown...")

	if rsql_err := context.Send_MESSAGE("server is being shutdown..."); rsql_err != nil {
		goto LABEL_SHUTDOWN_IMMEDIATE
	}

	atomic.StoreUint32(&rsql.G_server_shutdown_flag, 1)

	i = 0
	for { // waits one minute max for workers to terminate
		workers_count = cache.G_WORKER_QUEUE.Workers_in_use()

		if workers_count == 1 { // no more worker except the current worker, make some cleanup
			// rsql.Must_remove_directory_content(rsql.DIRECTORY_TEMPDIR)  // cannot remove the files (in particular the current journal),
			// rsql.Must_remove_directory_content(rsql.DIRECTORY_JOURNALS) // because they can be used by the current worker. On Windows, closing an open file crashes the program.

			if rsql_err := context.Send_MESSAGE("server has been shutdown normally."); rsql_err != nil {
				goto LABEL_SHUTDOWN_IMMEDIATE
			}

			log.Println("server has been shutdown normally.")
			os.Exit(0)
		}

		if nowait {
			goto LABEL_SHUTDOWN_IMMEDIATE
		}

		if i%10 == 0 {
			if rsql_err := context.Send_MESSAGE("%d workers are still running.", workers_count); rsql_err != nil {
				goto LABEL_SHUTDOWN_IMMEDIATE
			}
		}

		if i == 60 {
			break
		}

		i++

		time.Sleep(1 * time.Second)
	}

LABEL_SHUTDOWN_IMMEDIATE:

	if rsql_err := context.Send_MESSAGE("server has been shutdown immediately."); rsql_err != nil {
		// pass
	}

	workers_count = cache.G_WORKER_QUEUE.Workers_in_use()

	log.Printf("%d workers are still running.", workers_count)
	log.Println("server has been shutdown immediately.")
	os.Exit(1)

	return nil
}

// SIGTERM_handler is called when SIGTERM signal is received, indicating that the server should shutdown.
//
// It is triggered by systemd, which sends SIGTERM to stop the server.
// In the unit file for the service, the option "TimeoutStopSec=" specifies when systemd should kill the server with SIGKILL signal, if the server is still running.
//
func SIGTERM_handler() {
	var (
		workers_count int
		i             int
	)

	log.Println("server is being shutdown...")

	atomic.StoreUint32(&rsql.G_server_shutdown_flag, 1)

	i = 0
	for { // waits 60 minutes max for workers to terminate. Systemd unit file should specify a shorter delay with option "TimeoutStopSec=".
		workers_count = cache.G_WORKER_QUEUE.Workers_in_use()

		if workers_count == 0 {
			// no more worker, make some cleanup

			rsql.Must_remove_directory_content(rsql.DIRECTORY_TEMPDIR)
			rsql.Must_remove_directory_content(rsql.DIRECTORY_JOURNALS)

			log.Println("server has been shutdown normally.")
			os.Exit(0)
		}

		if i == 3600 {
			break
		}

		i++

		time.Sleep(1 * time.Second)
	}

	workers_count = cache.G_WORKER_QUEUE.Workers_in_use()

	log.Printf("%d workers are still running.", workers_count)
	log.Println("server has been shutdown immediately.")
	os.Exit(1)
}

// Windows_service_stop is called by the Windows service handler, when a stop command is received.
//
func Windows_service_stop() (stopped bool) {
	var (
		workers_count int
		i             int
	)

	log.Println("server is being shutdown...")

	atomic.StoreUint32(&rsql.G_server_shutdown_flag, 1)

	i = 0
	for { // waits 1 minute max for workers to terminate.
		workers_count = cache.G_WORKER_QUEUE.Workers_in_use()

		if workers_count == 0 {
			// no more worker, make some cleanup

			rsql.Must_remove_directory_content(rsql.DIRECTORY_TEMPDIR)
			rsql.Must_remove_directory_content(rsql.DIRECTORY_JOURNALS)

			log.Println("server has been shutdown normally.")
			return true
		}

		if i == 60 {
			break
		}

		i++

		time.Sleep(1 * time.Second)
	}

	workers_count = cache.G_WORKER_QUEUE.Workers_in_use()

	log.Printf("%d workers are still running.", workers_count)
	log.Println("server has been shutdown immediately.")

	return false
}
