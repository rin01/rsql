package stmt

import (
	"rsql"
	"rsql/data"
)

func THROW(error_number *data.INT, message *data.VARCHAR, severity *data.INT, state *data.TINYINT) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	// if an argument is error, return it

	if rsql_err = error_number.Error(); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = message.Error(); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = severity.Error(); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = state.Error(); rsql_err != nil {
		return rsql_err
	}

	errnum := error_number.Data_val()

	if errnum < rsql.THROW_MESSAGE_ID_LIMIT_LOWEST || errnum > rsql.THROW_MESSAGE_ID_LIMIT_HIGHEST {
		rsql_err := rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_THROW_BAD_ERROR_NUMBER, rsql.ERROR_BATCH_ABORT)

		rsql_err.State = rsql.State_t(state.Data_val()) // state 127 is important to copy, as it stops batch processing by some RSQL clients

		return rsql_err
	}

	// create error. Severity is ignored.

	rsql_err = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_THROW, rsql.ERROR_BATCH_ABORT)

	rsql_err.Message_id = rsql.Message_id_t(errnum)
	rsql_err.State = rsql.State_t(state.Data_val())

	if message.NULL_flag() == false {
		rsql_err.Message_fmt_string = message.Stringval() // if message string exists, override the original error message
	}

	return rsql_err
}
