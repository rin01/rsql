package stmt

import (
	"archive/tar"
	"bufio"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"time"

	"golang.org/x/text/collate"

	"rsql"
	"rsql/dict"
)

const WRITER_BUFSIZE = 32768

// BACKUP DATABASE database_name
//  TO DISK =  'file_name'
//
//
// Example:
//
//    BACKUP DATABASE mydb
//      TO DISK = '/Backups/mydb.rbak'
//
func BACKUP(context *rsql.Context, database_name string, filename string, params rsql.Backup_params) (rsql_err *rsql.Error) {
	var (
		err                      error
		buff                     *bytes.Buffer
		dump_dir                 string
		file_path                string
		file                     *os.File
		buffered_writer          *bufio.Writer
		gzip_writer              *gzip.Writer
		tar_writer               *tar.Writer
		coll                     *collate.Collator
		list_of_principals       []*dict.Backup_principal_item
		list_of_gtabledefs_perms []*dict.Backup_gtabledef_item
	)

	if rsql_err := dict.MASTER.Check_is_SA_or_DBO(context, database_name); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	if rsql_err := dict.MASTER.Check_databases_status_not_corrupted(database_name); rsql_err != nil { // database should not be corrupted
		return rsql_err
	}

	//====== get file path =====

	file_path = filename
	if filepath.IsAbs(file_path) == false { // if file path is not absolute, append it to default dump directory
		dump_dir = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_DUMP_DIR)
		if dump_dir == "" {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_DIR_NOT_INITIALIZED, rsql.ERROR_BATCH_ABORT)
		}

		file_path = filepath.Join(dump_dir, filename)
	}

	file_path = filepath.Clean(file_path)
	if rsql_err := rsql.Check_forbidden_file_path(file_path); rsql_err != nil {
		return rsql_err
	}

	//====== create Writer for output file ======

	file, err = os.Create(file_path)
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}
	defer func() {
		err := file.Close()
		if err != nil && rsql_err == nil { // if file.Close() failed and no other error already occured
			rsql_err = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}
	}()

	buffered_writer = bufio.NewWriterSize(file, WRITER_BUFSIZE)

	gzip_writer = gzip.NewWriter(buffered_writer)

	tar_writer = tar.NewWriter(gzip_writer)

	//====== create tar files ======

	// get sorted list of principals, gtabledefs and permissions

	if list_of_principals, list_of_gtabledefs_perms, rsql_err = dict.MASTER.Retrieve_sorted_list_of_principals_gtabledefs_for_BACKUP(context, database_name, params); rsql_err != nil {
		return rsql_err
	}

	// save database name

	buff = &bytes.Buffer{}

	fmt.Fprintf(buff, "database_name:[%s]", database_name)

	internal_database_name_path := filepath.Join("rsqlbackup", "database")
	if rsql_err := writeln_string_to_tar(internal_database_name_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save current date

	buff.Reset()

	fmt.Fprintf(buff, "backup_date:[%s]", time.Now().Format("2006-01-02 15:04:05"))

	internal_date_path := filepath.Join("rsqlbackup", "date")
	if rsql_err := writeln_string_to_tar(internal_date_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save logins

	buff.Reset()

	for _, principal := range list_of_principals {
		if principal.Pal_type != dict.PAL_TYPE_USER {
			continue
		}

		if principal.Lg_system_flag != 0 { // sa always exists in any database
			continue
		}

		fmt.Fprintf(buff, "login:[%s];user_name:[%s];hash:[%s];default_database:[%s];default_language:[%s];disabled:[%s]\n", principal.Lg_name, principal.Pal_name, principal.Lg_password_hash, principal.Lg_default_database, principal.Lg_default_language, bool_to_tf(principal.Lg_disabled_flag))
	}

	internal_logins_path := filepath.Join("rsqlbackup", "logins")
	if rsql_err := write_string_to_tar(internal_logins_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save database owner

	buff.Reset()

	db_owner := "sa"
	for _, principal := range list_of_principals {
		if principal.Pal_type != dict.PAL_TYPE_USER {
			continue
		}

		if principal.Pal_system_flag == dict.PAL_SYSFLAG_USER_DBO {
			db_owner = principal.Lg_name
			break
		}
	}

	fmt.Fprintf(buff, "db_owner:[%s]\n", db_owner)

	internal_db_owner_path := filepath.Join("rsqlbackup", "db_owner")
	if rsql_err := write_string_to_tar(internal_db_owner_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save users

	buff.Reset()

	for _, principal := range list_of_principals {
		if principal.Pal_type != dict.PAL_TYPE_USER {
			continue
		}

		fmt.Fprintf(buff, "user:[%s];for_login:[%s]\n", principal.Pal_name, principal.Lg_name) // dbo may be bound to a login that is not sa
	}

	internal_users_path := filepath.Join("rsqlbackup", "users")
	if rsql_err := write_string_to_tar(internal_users_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save roles

	buff.Reset()

	for _, principal := range list_of_principals {
		if principal.Pal_type != dict.PAL_TYPE_ROLE {
			continue
		}

		if principal.Pal_system_flag != 0 { // public role always exist in any database
			continue
		}

		fmt.Fprintf(buff, "role:[%s]\n", principal.Pal_name)
	}

	internal_roles_path := filepath.Join("rsqlbackup", "roles")
	if rsql_err := write_string_to_tar(internal_roles_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save memberships

	buff.Reset()

	for _, principal := range list_of_principals {
		if len(principal.Pal_member_of) == 0 {
			continue
		}

		for _, role_name := range principal.Pal_member_of {
			fmt.Fprintf(buff, "principal:[%s];member_of:[%s]\n", principal.Pal_name, role_name)
		}
	}

	internal_membership_path := filepath.Join("rsqlbackup", "memberships")
	if rsql_err := write_string_to_tar(internal_membership_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save some info about tables

	buff.Reset()

	fmt.Fprintf(buff, "count_of_tables:[%d]\n", len(list_of_gtabledefs_perms))

	internal_table_start_path := filepath.Join("rsqlbackup", "info_tables")
	if rsql_err := write_string_to_tar(internal_table_start_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// save tables

	if coll, rsql_err = rsql.Get_collator(params.Bk_server_default_collation); rsql_err != nil {
		return rsql_err
	}

	for i, item := range list_of_gtabledefs_perms {
		if err := write_gtabledef_to_tar(context, database_name, item, i, coll, tar_writer); err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}
	}

	//====== close all writers ======

	if err := tar_writer.Close(); err != nil { // Close also Flush to underlying writer
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	if err := gzip_writer.Close(); err != nil { // Close also Flush to underlying writer
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	if err := buffered_writer.Flush(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	return nil
}

// write_gtabledef_to_tar writes script, index2file, and base and index files of a gtabledef to tar_writer.
//
func write_gtabledef_to_tar(context *rsql.Context, database_name string, item *dict.Backup_gtabledef_item, i int, coll *collate.Collator, tar_writer *tar.Writer) *rsql.Error {
	var (
		gtabledef                      *rsql.GTabledef
		list_of_indexes                []*rsql.Tabledef
		buff                           *bytes.Buffer
		sql_script                     string
		internal_table_info_path       string
		internal_table_index2file_path string
	)

	gtabledef = item.Gt_gtabledef

	// tables/t1/script

	sql_script = dict.Sql_creation_script_table(database_name, "dbo", gtabledef, "", coll)

	internal_table_info_path = filepath.Join("rsqlbackup", "tables", fmt.Sprintf("gtable%d", i), "script")
	if rsql_err := writeln_string_to_tar(internal_table_info_path, sql_script, tar_writer); rsql_err != nil {
		return rsql_err
	}

	// tables/gtable1/index2file

	for _, tabledef := range gtabledef.Indexmap {
		list_of_indexes = append(list_of_indexes, tabledef)
	}

	sort.Sort(rsql.New_Indexes_by_name(list_of_indexes, coll))

	buff = &bytes.Buffer{}

	for j, indexdef := range list_of_indexes {
		fmt.Fprintf(buff, "index%d:[%s]\n", j, indexdef.Td_index_name)
	}

	internal_table_index2file_path = filepath.Join("rsqlbackup", "tables", fmt.Sprintf("gtable%d", i), "index2file")
	if rsql_err := write_string_to_tar(internal_table_index2file_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	// files tables/gtable1/base, index0, ...indexn

	if err := write_tabledef_to_tar(context, gtabledef.Gtable_name, i, gtabledef.Base, 0, tar_writer); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	for j, indexdef := range list_of_indexes {
		if rsql_err := write_tabledef_to_tar(context, gtabledef.Gtable_name, i, indexdef, j, tar_writer); rsql_err != nil {
			return rsql_err
		}
	}

	// tables/gtable1/permissions

	buff.Reset()

	for _, perm := range item.Gt_permissions {
		if perm.Pm_grant != 0 {
			fmt.Fprintf(buff, "principal:[%s];grant:[%s]\n", perm.Pm_principal, bk_permissions_table(perm.Pm_grant))
		}
		if perm.Pm_deny != 0 {
			fmt.Fprintf(buff, "principal:[%s];deny:[%s]\n", perm.Pm_principal, bk_permissions_table(perm.Pm_deny))
		}
	}

	internal_permissions_path := filepath.Join("rsqlbackup", "tables", fmt.Sprintf("gtable%d", i), "permissions")
	if rsql_err := write_string_to_tar(internal_permissions_path, string(buff.Bytes()), tar_writer); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func writeln_string_to_tar(internal_file_path string, s string, tar_writer *tar.Writer) *rsql.Error {

	return write_string_to_tar(internal_file_path, s+"\n", tar_writer)
}

func write_string_to_tar(internal_file_path string, s string, tar_writer *tar.Writer) *rsql.Error {

	hdr := &tar.Header{
		Name: internal_file_path,
		Mode: 0660,
		Size: int64(len(s)),
	}

	if err := tar_writer.WriteHeader(hdr); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	if _, err := tar_writer.Write([]byte(s)); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	return nil
}

func write_tabledef_to_tar(context *rsql.Context, gtable_name string, i int, tabledef *rsql.Tabledef, j int, tar_writer *tar.Writer) *rsql.Error {
	var (
		err                 error
		rsql_err            *rsql.Error
		file_path           string
		file                *os.File
		fileinfo            os.FileInfo
		internal_table_name string
		internal_file_path  string
		file_size           int64
		bytes_count         int64
	)

	file_path = tabledef.Td_file_path

	if file, err = os.Open(file_path); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}
	defer file.Close()

	// write header

	if fileinfo, err = file.Stat(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	switch tabledef.Td_type {
	case rsql.TD_TYPE_BASE_TABLE:
		internal_table_name = "base"
		rsql.Assert(j == 0)
	default:
		internal_table_name = fmt.Sprintf("%s%d", "index", j)
	}

	internal_file_path = filepath.Join("rsqlbackup", "tables", fmt.Sprintf("gtable%d", i), internal_table_name)
	file_size = fileinfo.Size()

	hdr := &tar.Header{
		Name: internal_file_path,
		Mode: 0660,
		Size: file_size,
	}

	if err := tar_writer.WriteHeader(hdr); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// write content

	if bytes_count, rsql_err = copy_buffer(context, tar_writer, file); rsql_err != nil {
		return rsql_err
	}

	if bytes_count != file_size {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BACKUP_IO_ERROR, rsql.ERROR_BATCH_ABORT, fmt.Errorf("file length mismatch (%d != %d) for backup %s", bytes_count, file_size, file_path))
	}

	return nil
}

func bool_to_tf(b bool) string {

	if b == true {
		return "t"
	}

	return "f"
}

func bk_permissions_table(perm rsql.Permission_t) string {
	var (
		res string
	)

	if perm == 0 {
		return ""
	}

	if perm&rsql.PERMISSION_SELECT != 0 {
		res += "S"
	}
	if perm&rsql.PERMISSION_UPDATE != 0 {
		res += "U"
	}
	if perm&rsql.PERMISSION_INSERT != 0 {
		res += "I"
	}
	if perm&rsql.PERMISSION_DELETE != 0 {
		res += "D"
	}

	return res
}

func copy_buffer(context *rsql.Context, dest io.Writer, src io.Reader) (total_written int64, rsql_err *rsql.Error) {
	var (
		buf []byte
	)

	buf = make([]byte, 32*1024)

	for {
		bytes_read, err_read := src.Read(buf) // read into buff (can be partial read)
		if bytes_read > 0 {                   // if read successful
			bytes_written, err_write := dest.Write(buf[:bytes_read]) // write all read bytes into dest
			total_written += int64(bytes_written)

			if err_write != nil {
				return total_written, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COPY_IO_ERROR, rsql.ERROR_BATCH_ABORT, err_write) // error during write
			}
			if bytes_read != bytes_written { // should never happen (write must return an error in this case)
				return total_written, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COPY_IO_ERROR, rsql.ERROR_BATCH_ABORT, io.ErrShortWrite)
			}
		}
		if err_read == io.EOF {
			return total_written, nil // all src has been read and written to dest
		}
		if err_read != nil {
			return total_written, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COPY_IO_ERROR, rsql.ERROR_BATCH_ABORT, err_read) // error when reading src
		}

		if rsql_err := context.Check_keepalive(); rsql_err != nil { // check periodically if client is alive
			return total_written, rsql_err
		}
	}

	panic("unreachable")
}
