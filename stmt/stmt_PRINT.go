package stmt

import (
	"rsql"
	"rsql/data"
)

func PRINT(context *rsql.Context, args []rsql.IDataslot) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	// check if an expression contains an error

	for _, dataslot := range args {
		if rsql_err = dataslot.Error(); rsql_err != nil {
			return rsql_err
		}
	}

	// write response type

	context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_PRINT))

	// write list of datatypes

	context.Ctx_mw.WriteArrayHeader(uint32(len(args)))

	for _, dataslot := range args {
		data.Write_datatype_msgp(context.Ctx_mw, dataslot)
	}

	// write list of expressions

	context.Ctx_mw.WriteArrayHeader(uint32(len(args)))

	for _, dataslot := range args {
		data.Write_dataslot_msgp(context.Ctx_mw, dataslot)
	}

	// flush data to client

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}
