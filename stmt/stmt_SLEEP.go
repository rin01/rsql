package stmt

import (
	"time"

	"rsql"
)

func SLEEP(context *rsql.Context, duration float64) *rsql.Error {

	const PERIOD = 5 // in seconds (5 seconds)

	ticker := time.NewTicker(PERIOD * time.Second)
	defer ticker.Stop()

	timer := time.NewTimer(time.Duration(duration*1000000) * time.Microsecond)
	defer timer.Stop()

	for {
		select {
		case <-ticker.C:
			if rsql_err := context.Check_keepalive(); rsql_err != nil { // check periodically if client is alive
				return rsql_err
			}

		case <-timer.C:
			return nil
		}
	}

	return nil
}
