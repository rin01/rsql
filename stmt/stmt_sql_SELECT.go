package stmt

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/csr"
	"rsql/data"
)

func SELECT_OR_UNION(context *rsql.Context, curs csr.Cursor, curs_orderby *csr.Cursor_table, colname_list []string, params *rsql.Select_params) *rsql.Error {
	var (
		rsql_err             *rsql.Error
		TOP_value            int64
		record               []rsql.IDataslot
		record_count         int64
		record_count_orderby int64
		inspad               *btree.Inspad
		wcache               *cache.Wcache
		gtabledef_orderby    *rsql.GTabledef
		tabledef_orderby     *rsql.Tabledef
	)

	wcache = context.Ctx_wcache.(*cache.Wcache)

	TOP_value = params.TOP_value

	switch curs := curs.(type) {
	case *csr.Cursor_select:
		record = curs.Cols_dataslots

	case *csr.Cursor_union:
		record = curs.Equalized_dataslots

	default:
		panic("impossible")
	}

	//======= process ORDER BY =======

	if curs_orderby != nil {
		// create insertion_row

		dataslot_rowid := data.New_BIGINT_NULL(rsql.KIND_COL_LEAF)

		insertion_row := make([]rsql.IDataslot, 1, len(record)+1)
		insertion_row[0] = dataslot_rowid
		insertion_row = append(insertion_row, record...)

		// create sorttable

		gtabledef_orderby = curs_orderby.Gtabledef
		tabledef_orderby = curs_orderby.Base_tabledef

		if rsql_err := wcache.Create_info_page_and_first_zone_allocator(tabledef_orderby); rsql_err != nil {
			return rsql_err
		}

		// fill sorttable with all records

		if inspad, rsql_err = btree.New_inspad(tabledef_orderby, rsql.Object_qname_t{Object_name: gtabledef_orderby.Gtable_name}); rsql_err != nil {
			return rsql_err
		}

		curs.Reset()

		for {
			var rsql_err *rsql.Error
			var rc csr.State_t

			if rc, rsql_err = curs.Next(context); rsql_err != nil {
				return rsql_err
			}

			if rc == csr.CURS_RECORD_RESET {
				break
			}

			// insert record

			if rsql_err := btree.Check_dataslot_error_and_NOT_NULL_and_update_ROWID_and_IDENTITY_columns(wcache, gtabledef_orderby, insertion_row, false); rsql_err != nil { // check that no dataslot contains error. Update value of ROWID.
				return rsql_err
			}

			// insert row into base table

			if rsql_err := inspad.Insert_row_into_table(wcache, tabledef_orderby, insertion_row); rsql_err != nil {
				return rsql_err
			}

			record_count_orderby++

			// check periodically if client is alive

			if rsql_err := context.Check_keepalive(); rsql_err != nil {
				return rsql_err
			}
		}

		wcache.Transform_all_DRAFT_elements_to_TRANSITORY_for_group_or_sort_table(cache.Tblid_t(tabledef_orderby.Td_tblid))

		curs = curs_orderby
		record = curs_orderby.Base_fields
		record = record[1:] // discard rowid column
	}

	//======= if @variable = columns, assign to variables and exit ======

	if len(params.Set_variables_list) > 0 {
		curs.Reset()

		for {
			var rsql_err *rsql.Error
			var rc csr.State_t

			if rc, rsql_err = curs.Next(context); rsql_err != nil {
				return rsql_err
			}

			if rc == csr.CURS_RECORD_RESET {
				break
			}

			if _, rsql_err := rsql.Execute_basicblock(context, params.Set_variables_bb); rsql_err != nil {
				return rsql_err
			}

			record_count++

			break // no need to make further iteration. Besides, if the variables are used in e.g WHERE clause, it results in undefined behaviour. (to avoid that, we could store the values in temporary dataslots, and copy them when the loop terminates. But I don't see the need of it.)
		}

		wcache.Wpc_last_rowcount = record_count

		goto LABEL_END_SELECT
	}

	//======= send record layout =======

	context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_RECORD_LAYOUT)) // write response type

	context.Ctx_mw.WriteArrayHeader(uint32(len(record))) // write list of column names

	for i, _ := range record {
		context.Ctx_mw.WriteString(colname_list[i])
	}

	context.Ctx_mw.WriteArrayHeader(uint32(len(record))) // write list of datatypes

	for _, dataslot := range record {
		data.Write_datatype_msgp(context.Ctx_mw, dataslot)
	}

	if rsql_err = context.Writer_error(); rsql_err != nil {
		return rsql_err
	}

	// ======= send records  =======

	curs.Reset()

	for {
		var rsql_err *rsql.Error
		var rc csr.State_t

		if TOP_value >= 0 && record_count >= TOP_value {
			break
		}

		if rc, rsql_err = curs.Next(context); rsql_err != nil {
			return rsql_err
		}

		if rc == csr.CURS_RECORD_RESET {
			break
		}

		// send record

		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_RECORD)) // write response type

		context.Ctx_mw.WriteArrayHeader(uint32(len(record))) // write record

		for _, dataslot := range record {
			data.Write_dataslot_msgp(context.Ctx_mw, dataslot)
		}

		if rsql_err = context.Writer_error(); rsql_err != nil {
			return rsql_err
		}

		record_count++
	}

	// send record count

	wcache.Wpc_last_rowcount = record_count

	context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_RECORD_FINISHED)) // write response type. Always send it to client, to signify that all rows have been sent, as drivers need it.
	context.Ctx_mw.WriteInt64(record_count)
	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

LABEL_END_SELECT:
	//====== discard sort table pages from wcache ======

	if tabledef_orderby != nil {
		wcache.Discard_all_TRANSITORY_elements_for_group_or_sort_table(cache.Tblid_t(tabledef_orderby.Td_tblid))
	}

	return nil
}
