package stmt

import (
	"rsql"
	"rsql/cache"
	"rsql/data"
	"rsql/dict"
)

func USE(context *rsql.Context, database_name string, vars_OUT []rsql.IDataslot) *rsql.Error {
	var (
		database_dbid int64
		schema_name   string
		schema_schid  int64
		user_name     string
		user_palid    int64
		rsql_err      *rsql.Error
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)
	wcache.Wpc_last_rowcount = 0

	if database_dbid, schema_name, schema_schid, user_name, user_palid, rsql_err = dict.MASTER.Use_sysdatabase(context.Ctx_login_id, context.Ctx_login_name_for_info, database_name); rsql_err != nil {
		return rsql_err // error aborts the session
	}

	// assign values to _@current_db_name, etc

	if rsql_err = data.Assign_VARCHAR_value(vars_OUT[0].(*data.VARCHAR), database_name); rsql_err != nil { // _@current_db_name
		return rsql_err
	}

	data.Assign_BIGINT_value(vars_OUT[1].(*data.BIGINT), database_dbid) // _@current_db_id

	if rsql_err = data.Assign_VARCHAR_value(vars_OUT[2].(*data.VARCHAR), schema_name); rsql_err != nil { // _@current_schema_name
		return rsql_err
	}

	data.Assign_BIGINT_value(vars_OUT[3].(*data.BIGINT), schema_schid) // _@current_schema_id

	if rsql_err = data.Assign_VARCHAR_value(vars_OUT[4].(*data.VARCHAR), user_name); rsql_err != nil { // _@current_user_name
		return rsql_err
	}

	data.Assign_BIGINT_value(vars_OUT[5].(*data.BIGINT), user_palid) // _@current_user_id

	return nil
}

// BEGIN_TRANSACTION is the start of an explicit transaction.
//
func BEGIN_TRANSACTION(context *rsql.Context) *rsql.Error {

	wcache := context.Ctx_wcache.(*cache.Wcache)
	wcache.Wpc_last_rowcount = 0

	if context.Ctx_trancount >= cache.TRANCOUNT_MAX {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANCOUNT_MAX_LIMIT, rsql.ERROR_BATCH_ABORT, cache.TRANCOUNT_MAX)
	}

	context.Ctx_trancount++ // increment transaction count

	return nil
}

// COMMIT_TRANSACTION is the end of an explicit transaction, and applies all changes to disk.
//
// If transaction count <= 0, there is no corresponding BEGIN TRAN and an error is raised.
// If transaction count >  1, COMMIT is in a nested explicit transaction. It just decrements transaction count.
// If transaction count == 1, a real commit is performed and all changes are copied to the table files. Transaction count is reset to 0.
//
func COMMIT_TRANSACTION(context *rsql.Context) *rsql.Error {

	wcache := context.Ctx_wcache.(*cache.Wcache)
	wcache.Wpc_last_rowcount = 0

	if context.Ctx_trancount <= 0 { // if COMMIT statement is not inside an explicit transaction, raise an error
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COMMIT_WITHOUT_BEGIN_TRAN, rsql.ERROR_BATCH_ABORT)
	}

	if context.Ctx_trancount > 1 { // if COMMIT statement is inside a nested explicit transaction, just decrement transaction count
		context.Ctx_trancount--
		return nil
	}

	// commit the changes

	rsql.Assert(context.Ctx_trancount == 1)

	wcache.COMMIT() // table pages are written to disk

	context.Ctx_trancount = 0 // decrement transaction count to 0

	return nil
}

// ROLLBACK_TRANSACTION is the end of an explicit transaction, and don't apply any change to disk.
// No change is copied to the table files.
// Transaction count is forced to 0.
//
func ROLLBACK_TRANSACTION(context *rsql.Context) *rsql.Error {

	wcache := context.Ctx_wcache.(*cache.Wcache)
	wcache.Wpc_last_rowcount = 0

	if context.Ctx_trancount <= 0 { // if ROLLBACK statement is not inside an explicit transaction, raise an error
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ROLLBACK_WITHOUT_BEGIN_TRAN, rsql.ERROR_BATCH_ABORT)
	}

	wcache.ROLLBACK() // changes are discarded

	context.Ctx_trancount = 0 // force transaction count to 0

	return nil
}
