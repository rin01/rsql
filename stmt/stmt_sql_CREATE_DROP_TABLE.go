package stmt

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/csr"
	"rsql/data"
	"rsql/dict"
)

func init() {

	rsql.Fill_index_with_records = fill_index_with_records
}

func CREATE_TABLE(context *rsql.Context, database_name string, schema_name string, gtabledef *rsql.GTabledef) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "CREATE TABLE")
	}

	if rsql_err := dict.MASTER.Add_systable(context, database_name, schema_name, gtabledef); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func ALTER_TABLE(context *rsql.Context, args dict.Table_params) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "ALTER TABLE")
	}

	if rsql_err := dict.MASTER.Change_systable(context, args.Table_qname, args.Change_table_name, args.Change_table_name_original, args.Column_name, args.Change_column_name, args.Change_column_name_original, args.Change_column_nullability); rsql_err != nil {
		return rsql_err // the server code will make the ROLLBACK
	}

	return nil
}

func DROP_TABLE(context *rsql.Context, table_qname rsql.Object_qname_t) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "DROP TABLE")
	}

	if rsql_err := dict.MASTER.Remove_systable(context, table_qname); rsql_err != nil {
		return rsql_err // the server code will make the ROLLBACK
	}

	return nil
}

func CREATE_INDEX(context *rsql.Context, args dict.Index_params) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		gtabledef     *rsql.GTabledef
		gtabledef_new *rsql.GTabledef // if new index is clustered, the whole table must be dropped and recreated
	)

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "CREATE INDEX")
	}

	if gtabledef_new, rsql_err = dict.MASTER.Add_sysindex(context, args.Table_qname, args.Index_name, args.Index_name_original, args.Index_type, args.Cluster_type, args.Colname_list); rsql_err != nil {
		return rsql_err // the server code will make the ROLLBACK
	}

	if gtabledef_new == nil { // index was nonclustered and successfully created
		return nil
	}

	//=== index to create is clustered. Drop and recreate a new table. ===

	wcache := context.Ctx_wcache.(*cache.Wcache)

	if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(args.Table_qname); rsql_err != nil {
		return rsql_err
	}

	page_info := wcache.Fetch_TRANSITORY_or_PUBLIC_page_info_and_pin_it(gtabledef.Base) // read page info
	defer wcache.Unpin(page_info)
	ppage_info := wcache.Ppage(page_info)
	root_page_no := ppage_info.Pinf_root_page_no()

	if root_page_no != 0 { // table is not empty, error
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_CREATE_CLUSTERED_INDEX_TABLE_NOT_EMPTY, rsql.ERROR_BATCH_ABORT, args.Table_qname.String())
	}

	if rsql_err := DROP_TABLE(context, args.Table_qname); rsql_err != nil { // DROP TABLE
		return rsql_err
	}

	if rsql_err := CREATE_TABLE(context, args.Table_qname.Database_name, args.Table_qname.Schema_name, gtabledef_new); rsql_err != nil { // CREATE TABLE
		return rsql_err
	}

	return nil
}

func ALTER_INDEX(context *rsql.Context, table_qname rsql.Object_qname_t, index_name string, change_index_name string, change_index_name_original string) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "ALTER INDEX")
	}

	if rsql_err := dict.MASTER.Change_sysindex(context, table_qname, index_name, change_index_name, change_index_name_original); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func DROP_INDEX(context *rsql.Context, table_qname rsql.Object_qname_t, index_name string) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "DROP INDEX")
	}

	if rsql_err := dict.MASTER.Remove_sysindex(context, table_qname, index_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func fill_index_with_records(context *rsql.Context, table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef, indexdef *rsql.Tabledef) *rsql.Error {
	var (
		rsql_err *rsql.Error
		curs     *csr.Cursor_table
		inspad   *btree.Inspad
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)

	curs = csr.New_cursor_table(gtabledef)

	// create dataslots in cursor

	for _, coldef := range indexdef.Td_coldefs {
		col_base_seqno := coldef.Cd_base_seqno

		dataslot := data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
		curs.Base_fields[col_base_seqno] = dataslot

		rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)
	}

	// read all records from base table, and insert them in index

	if inspad, rsql_err = btree.New_inspad(indexdef, table_qname); rsql_err != nil {
		return rsql_err
	}

	curs.Reset()

	for {
		// read record to insert

		var rsql_err *rsql.Error
		var rc csr.State_t

		if rc, rsql_err = curs.Next(context); rsql_err != nil {
			return rsql_err
		}

		if rc == csr.CURS_RECORD_RESET {
			break
		}

		// insert record into index table

		index_row := btree.Create_shallow_index_row_from_pool(indexdef, curs.Base_fields)

		if rsql_err = inspad.Insert_row_into_table(wcache, indexdef, index_row); rsql_err != nil {
			return rsql_err
		}

		btree.Put_row_into_pool(index_row)
	}

	return nil
}
