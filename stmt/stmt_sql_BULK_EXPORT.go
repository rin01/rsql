package stmt

import (
	"bufio"
	"os"
	"path/filepath"
	"time"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/htmlindex"
	"golang.org/x/text/transform" // Transformer is an interface, having methods Transform() and Reset()

	"rsql"
	"rsql/cache"
	"rsql/csr"
	"rsql/data"
	"rsql/dict"
	"rsql/lang"
)

const BULK_EXPORT_BYTE_BUFFER_DEFAULT_CAPACITY = 300

func BULK_EXPORT(context *rsql.Context, curs csr.Cursor, filename string, params rsql.Bulk_export_params) (rsql_err *rsql.Error) {
	var (
		err                   error
		date_format           []byte
		time_format           []byte
		datetime_format       []byte
		langinfo              *lang.Langinfo
		bulk_dir              string
		file_path             string
		bytes_fieldterminator []byte
		bytes_rowterminator   []byte

		file        *os.File
		file_writer *Export_writer

		record      []rsql.IDataslot
		byte_buffer []byte

		record_count     int64
		time_start       time.Time
		time_start_delta time.Time
	)

	const RECORD_COUNT_FOR_MESSAGE = 100000 // a message is sent to the client each time this number of records have been exported

	//====== prepare date, time, and datetime format string =====

	date_format = []byte(params.Be_opt_date_format)
	time_format = []byte(params.Be_opt_time_format)
	datetime_format = []byte(params.Be_opt_datetime_format)

	langinfo = lang.LANGINFO_MAP["en-us"]

	//====== get file path =====

	file_path = filename
	if filepath.IsAbs(file_path) == false { // if file path is not absolute, append it to default bulk directory
		bulk_dir = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_BULK_DIR)
		if bulk_dir == "" {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_DIR_NOT_INITIALIZED, rsql.ERROR_BATCH_ABORT)
		}

		file_path = filepath.Join(bulk_dir, filename)
	}

	file_path = filepath.Clean(file_path)
	if rsql_err := rsql.Check_forbidden_file_path(file_path); rsql_err != nil {
		return rsql_err
	}

	//====== create Writer for output file ======

	file, err = os.Create(file_path)
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_EXPORT_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}
	defer func() {
		err := file.Close()
		if err != nil && rsql_err == nil { // if file.Close() failed and no other error already occured
			rsql_err = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_EXPORT_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}
	}()

	if file_writer, rsql_err = get_bulk_export_writer(file, params.Be_opt_codepage); rsql_err != nil {
		return rsql_err
	}

	// ======= get record source =======

	switch curs := curs.(type) {
	case *csr.Cursor_select:
		record = curs.Cols_dataslots

	case *csr.Cursor_union:
		record = curs.Equalized_dataslots

	case *csr.Cursor_table:
		record = curs.Base_fields // ROWID field has no dataslot, because we don't want to export it

	default:
		panic("impossible")
	}

	// ======= write records  =======

	bytes_fieldterminator = []byte(params.Be_opt_fieldterminator)
	bytes_rowterminator = []byte(params.Be_opt_rowterminator)

	time_start = time.Now()
	time_start_delta = time_start

	byte_buffer = make([]byte, BULK_EXPORT_BYTE_BUFFER_DEFAULT_CAPACITY)

	curs.Reset()

	for {
		var rsql_err *rsql.Error
		var rc csr.State_t

		if rc, rsql_err = curs.Next(context); rsql_err != nil { // if error on a column
			file_writer.Close() // just to flush file_writer
			return rsql_err
		}

		if rc == csr.CURS_RECORD_RESET {
			break
		}

		// write record

		byte_buffer = byte_buffer[:0]

		for _, dataslot := range record { // for all columns
			if dataslot == nil { // nil for ROWID
				continue
			}

			rsql.Assert(dataslot.Error() == nil) // already checked by curs.Next()

			if dataslot.NULL_flag() == true { // if NULL, output is an empty string
				byte_buffer = append(byte_buffer, params.Be_opt_fieldterminator...)
				continue
			}

			if dataslot.Datatype() == rsql.DATATYPE_VARCHAR {
				if dataslot.(*data.VARCHAR).Contains(bytes_fieldterminator) == true {
					return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_EXPORT_FIELD_CONTAINS_FIELDTERMINATOR, rsql.ERROR_BATCH_ABORT)
				}

				if dataslot.(*data.VARCHAR).Contains(bytes_rowterminator) == true {
					return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_EXPORT_FIELD_CONTAINS_ROWTERMINATOR, rsql.ERROR_BATCH_ABORT)
				}
			}

			byte_buffer = data.Append_export(byte_buffer, dataslot, date_format, time_format, datetime_format, langinfo) // write column as string into buffer

			byte_buffer = append(byte_buffer, params.Be_opt_fieldterminator...) // write fieldterminator into buffer
		}

		byte_buffer = byte_buffer[:len(byte_buffer)-len(params.Be_opt_fieldterminator)] // discard last fieldterminator
		byte_buffer = append(byte_buffer, params.Be_opt_rowterminator...)

		if _, err = file_writer.Write(byte_buffer); err != nil { // write record
			file_writer.Close() // just to flush file_writer
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_EXPORT_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}

		record_count++

		if record_count%RECORD_COUNT_FOR_MESSAGE == 0 {
			if rsql_err := context.Send_MESSAGE("records exported: %14d   rec/s: %.0f", record_count, RECORD_COUNT_FOR_MESSAGE/(time.Since(time_start_delta).Seconds())); rsql_err != nil {
				return rsql_err
			}

			time_start_delta = time.Now()
		}

		// check periodically if client is alive

		if rsql_err := context.Check_keepalive(); rsql_err != nil {
			return rsql_err
		}
	}

	if rsql_err := context.Send_MESSAGE("records exported: %14d   in   : %.0f seconds", record_count, time.Since(time_start).Seconds()); rsql_err != nil {
		return rsql_err
	}

	if err = file_writer.Close(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_EXPORT_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// send record count

	wcache := context.Ctx_wcache.(*cache.Wcache)
	wcache.Wpc_last_rowcount = record_count

	if context.Ctx_nocount == false {
		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_EXECUTION_FINISHED)) // prepare response type
		context.Ctx_mw.WriteInt64(record_count)
		if rsql_err := context.Writer_flush(); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

// Export_writer is the Writer for BULK EXPORT.
//
type Export_writer struct {
	buffered_writer *bufio.Writer
	utf8_writer     *transform.Writer // write from utf8 to given encoding
}

func (w *Export_writer) Write(data []byte) (n int, err error) {

	if w.utf8_writer != nil {
		return w.utf8_writer.Write(data)
	}

	return w.buffered_writer.Write(data)
}

// Close tries hard to close the Writer.
// It flushes and closes underlying writers, except the file.
//
// But you must close the file yourself.
//
func (w *Export_writer) Close() error {
	var (
		err1, err2 error
	)

	if w.utf8_writer != nil {
		err1 = w.utf8_writer.Close() // flush utf8_writer if any
	}

	err2 = w.buffered_writer.Flush() // flush buffered_writer

	if err1 != nil {
		return err1
	}

	if err2 != nil {
		return err2
	}

	return nil
}

func get_bulk_export_writer(file *os.File, encoding_name string) (*Export_writer, *rsql.Error) {
	var (
		err             error
		enc             encoding.Encoding // just an interface, with methods NewDecoder() and NewEncoder() returning a Transformer. Real implementation of encoders/decoders are in golang.org/x/text/encoding/charmap package.
		encoder         transform.Transformer
		buffered_writer *bufio.Writer
		utf8_writer     *transform.Writer // write from utf8 to given encoding
	)

	// create bufio.Writer object to buffer the file

	buffered_writer = bufio.NewWriter(file)

	// get encoder

	switch encoding_name {
	case "utf8", "utf-8":
		encoding_name = "raw"
	case "acp":
		encoding_name = "windows-1252"
	}

	if encoding_name == "raw" {
		export_writer := &Export_writer{
			buffered_writer: buffered_writer,
			utf8_writer:     nil,
		}

		return export_writer, nil //===>  if "raw", just return a buffered Writer
	}

	//=== create a Writer that converts data to utf8 ===

	if enc, err = htmlindex.Get(encoding_name); err != nil { // get encoding from a usual name, case insensitive
		return nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ENCODING_NAME_NOT_FOUND, rsql.ERROR_BATCH_ABORT, encoding_name)

	}

	encoder = enc.NewEncoder() // encodes from utf8 to given encoding

	// create transform.Writer object

	utf8_writer = transform.NewWriter(buffered_writer, encoder) // transform.Writer object implements io.Writer interface

	export_writer := &Export_writer{
		buffered_writer: buffered_writer,
		utf8_writer:     utf8_writer,
	}

	return export_writer, nil
}
