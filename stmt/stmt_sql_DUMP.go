package stmt

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"rsql"
	"rsql/dict"
)

func DUMP_PARAMETERS(context *rsql.Context, dump_params rsql.Dump_params) (rsql_err *rsql.Error) {
	var (
		err         error
		dump_dir    string
		file_path   string
		file        *os.File
		file_writer *bufio.Writer

		list []dict.Show_sysparameter_item
	)

	// get list of sysparameters

	show_params := rsql.Show_params{Sh_output_type: rsql.SH_OUTPUT_SQL, Sh_server_default_collation: dump_params.Dp_server_default_collation}

	if _, list, rsql_err = dict.MASTER.Retrieve_sorted_list_of_sysparameters(context, show_params); rsql_err != nil {
		return rsql_err
	}

	// get file path

	file_path = dump_params.Dp_filename
	if filepath.IsAbs(file_path) == false { // if file path is not absolute, append it to default dump directory
		dump_dir = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_DUMP_DIR)
		if dump_dir == "" {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_DIR_NOT_INITIALIZED, rsql.ERROR_BATCH_ABORT)
		}

		file_path = filepath.Join(dump_dir, dump_params.Dp_filename)
	}

	// create Writer for output file

	file, err = os.Create(file_path)
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}
	defer func() {
		err := file.Close()
		if err != nil && rsql_err == nil { // if file.Close() failed and no other error already occured
			rsql_err = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}
	}()

	file_writer = bufio.NewWriter(file)

	// write sysparameters

	for _, elem := range list {
		s := elem.Output_text

		fmt.Fprintln(file_writer, s)
	}

	// flush

	err = file_writer.Flush()
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// send message

	if rsql_err := context.Send_MESSAGE(fmt.Sprintf("DUMP PARAMETERS '%s' file created.", file_path)); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func DUMP_LOGINS(context *rsql.Context, dump_params rsql.Dump_params) (rsql_err *rsql.Error) {
	var (
		err         error
		dump_dir    string
		file_path   string
		file        *os.File
		file_writer *bufio.Writer

		list []dict.Show_login_item
	)

	// get list of logins

	show_params := rsql.Show_params{Sh_output_type: rsql.SH_OUTPUT_SQL, Sh_server_default_collation: dump_params.Dp_server_default_collation}

	if _, list, rsql_err = dict.MASTER.Retrieve_sorted_list_of_logins(context, show_params); rsql_err != nil {
		return rsql_err
	}

	// get file path

	file_path = dump_params.Dp_filename
	if filepath.IsAbs(file_path) == false { // if file path is not absolute, append it to default dump directory
		dump_dir = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_DUMP_DIR)
		if dump_dir == "" {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_DIR_NOT_INITIALIZED, rsql.ERROR_BATCH_ABORT)
		}

		file_path = filepath.Join(dump_dir, dump_params.Dp_filename)
	}

	// create Writer for output file

	file, err = os.Create(file_path)
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}
	defer func() {
		err := file.Close()
		if err != nil && rsql_err == nil { // if file.Close() failed and no other error already occured
			rsql_err = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}
	}()

	file_writer = bufio.NewWriter(file)

	// write logins

	for _, elem := range list {
		s := elem.Output_text

		fmt.Fprintln(file_writer, s)
	}

	// flush

	err = file_writer.Flush()
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// send message

	if rsql_err := context.Send_MESSAGE(fmt.Sprintf("DUMP LOGINS '%s' file created.", file_path)); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func DUMP_DATABASE(context *rsql.Context, dump_params rsql.Dump_params) (rsql_err *rsql.Error) {
	var (
		err         error
		dump_dir    string
		file_path   string
		file        *os.File
		file_writer *bufio.Writer

		list_databases         []dict.Show_database_item
		list_roles             []dict.Show_principal_item
		list_users             []dict.Show_principal_item
		role_membership_script string
		user_membership_script string
		list_tables            []dict.Show_table_item
	)

	// get list of databases, roles, users

	show_params := rsql.Show_params{Sh_output_type: rsql.SH_OUTPUT_SQL, Sh_database_name: dump_params.Dp_database_name, Sh_server_default_collation: dump_params.Dp_server_default_collation}

	if _, list_databases, rsql_err = dict.MASTER.Retrieve_sorted_list_of_databases(context, show_params); rsql_err != nil { // list of only one database. Last argument 'true' is to comment out the statements.
		return rsql_err
	}

	if _, list_roles, role_membership_script, rsql_err = dict.MASTER.Retrieve_sorted_list_of_principals(context, show_params, dict.PAL_TYPE_ROLE); rsql_err != nil {
		return rsql_err
	}

	if _, list_users, user_membership_script, rsql_err = dict.MASTER.Retrieve_sorted_list_of_principals(context, show_params, dict.PAL_TYPE_USER); rsql_err != nil {
		return rsql_err
	}

	show_params.Sh_option_PERM = true
	if _, list_tables, rsql_err = dict.MASTER.Retrieve_sorted_list_of_GTabledefs(context, show_params); rsql_err != nil {
		return rsql_err
	}

	// get file path

	file_path = dump_params.Dp_filename
	if filepath.IsAbs(file_path) == false { // if file path is not absolute, append it to default dump directory
		dump_dir = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_DUMP_DIR)
		if dump_dir == "" {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_DIR_NOT_INITIALIZED, rsql.ERROR_BATCH_ABORT)
		}

		file_path = filepath.Join(dump_dir, dump_params.Dp_filename)
	}

	// create Writer for output file

	file, err = os.Create(file_path)
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}
	defer func() {
		err := file.Close()
		if err != nil && rsql_err == nil { // if file.Close() failed and no other error already occured
			rsql_err = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}
	}()

	file_writer = bufio.NewWriter(file)

	// write

	fmt.Fprintf(file_writer, fmt.Sprintf("/*============== DATABASE [%s] ==============*/\n", show_params.Sh_database_name))
	fmt.Fprintf(file_writer, "\n")

	for _, elem := range list_databases {
		if elem.Output_text == "" {
			continue
		}

		ss := strings.Split(elem.Output_text, "\n")
		for i, s := range ss {
			ss[i] = "-- " + s
		}
		s := strings.Join(ss, "\n")

		fmt.Fprintln(file_writer, s)
	}

	fmt.Fprintf(file_writer, "\n")
	fmt.Fprintf(file_writer, fmt.Sprintf("-- USE [%s];\n", show_params.Sh_database_name))
	fmt.Fprintf(file_writer, "\n")
	fmt.Fprintf(file_writer, "/*============== ROLES ==============*/\n")
	fmt.Fprintf(file_writer, "\n")

	for _, elem := range list_roles {
		if elem.Output_text == "" {
			continue
		}
		s := elem.Output_text

		fmt.Fprintln(file_writer, s)
	}

	if role_membership_script != "" {
		fmt.Fprintf(file_writer, "\n")
		fmt.Fprintln(file_writer, role_membership_script)
	}

	fmt.Fprintf(file_writer, "\n")

	fmt.Fprintf(file_writer, "/*============== USERS ==============*/\n")
	fmt.Fprintf(file_writer, "\n")

	for _, elem := range list_users {
		if elem.Output_text == "" {
			continue
		}
		s := elem.Output_text

		fmt.Fprintln(file_writer, s)
	}

	if user_membership_script != "" {
		fmt.Fprintf(file_writer, "\n")
		fmt.Fprintln(file_writer, user_membership_script)
	}

	fmt.Fprintf(file_writer, "\n")

	fmt.Fprintf(file_writer, "/*============== TABLES ==============*/\n")
	fmt.Fprintf(file_writer, "\n")

	for _, elem := range list_tables {
		s := elem.Output_text

		fmt.Fprintln(file_writer, s)
		fmt.Fprintf(file_writer, "\n")
	}

	// flush

	err = file_writer.Flush()
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// send message

	if rsql_err := context.Send_MESSAGE(fmt.Sprintf("DUMP DATABASE [%s], '%s' file created.", show_params.Sh_database_name, file_path)); rsql_err != nil {
		return rsql_err
	}

	return nil
}
