package stmt

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/csr"
)

func UPDATE(context *rsql.Context, table_qname rsql.Object_qname_t, params rsql.Update_params) *rsql.Error {
	var (
		udpad_bag          btree.Udpad_bag
		curs               csr.Cursor
		target_cursor      *csr.Cursor_table
		insertion_base_row rsql.Row
		count              int
		deleted_row_count  int64
		inserted_row_count int64
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)

	curs = params.Ud_cursor.(csr.Cursor)                        // a cursor or a tree of cursors, from the FROM clause of UPDATE
	target_cursor = params.Ud_target_cursor.(*csr.Cursor_table) // Cursor_table, pointing in the cursor of target table in the tree above
	insertion_base_row = params.Ud_insertion_base_row           // row to insert into target table, containing SET clauses expressions

	udpad_bag.Initialize(table_qname, target_cursor.Gtabledef)

	// ======= scan all records of FROM clause  =======

	curs.Reset()

	for {
		var rsql_err *rsql.Error
		var rc csr.State_t

		if rc, rsql_err = curs.Next(context); rsql_err != nil { // TRANSITORY or PUBLIC pages. Scan the FROM xtable.
			return rsql_err
		}

		if rc == csr.CURS_RECORD_RESET {
			break
		}

		// delete record from base table and indexes (only indexes with columns that are updated are updated)

		if count, rsql_err = btree.Delete_row(wcache, &udpad_bag.Delpad_bag, target_cursor.Gtabledef, target_cursor.Base_fields, params.Ud_indexes_to_update); rsql_err != nil { // count is 0 or 1
			return rsql_err
		}

		if count == 0 { // if record not found, continue loop
			continue
		}

		deleted_row_count += int64(count)

		// evaluate insertion_base_row

		if _, rsql_err := rsql.Execute_basicblock(context, params.Ud_basicblock); rsql_err != nil {
			return rsql_err
		}

		for _, dataslot := range insertion_base_row {
			if rsql_err := dataslot.Error(); rsql_err != nil {
				return rsql_err
			}
		}

		// insert record into base table and indexes (only indexes with columns that are updated are updated)

		if rsql_err = btree.Insert_row_into_base_table_and_indexes(wcache, &udpad_bag.Inspad_bag, target_cursor.Gtabledef, insertion_base_row, params.Ud_indexes_to_update, true); rsql_err != nil { // "true" is explicit_IDENTITY_provided argument
			return rsql_err
		}

		inserted_row_count++

		// check periodically if client is alive

		if rsql_err := context.Check_keepalive(); rsql_err != nil {
			return rsql_err
		}
	}

	rsql.Assert(deleted_row_count == inserted_row_count)

	// confirm the modifications

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	if context.Ctx_trancount == 0 {
		wcache.COMMIT() // table pages are written to disk
	}

	// send record count

	wcache.Wpc_last_rowcount = inserted_row_count

	if context.Ctx_nocount == false {
		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_EXECUTION_FINISHED)) // prepare response type
		context.Ctx_mw.WriteInt64(inserted_row_count)
		if rsql_err := context.Writer_flush(); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}
