package stmt

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/csr"
)

func DELETE(context *rsql.Context, curs csr.Cursor, target_cursor *csr.Cursor_table) *rsql.Error {
	var (
		delpad_bag        btree.Delpad_bag
		count             int
		deleted_row_count int64
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)

	delpad_bag.Initialize(target_cursor.Gtabledef)

	// ======= scan all records of FROM clause  =======

	curs.Reset()

	for {
		var rsql_err *rsql.Error
		var rc csr.State_t

		if rc, rsql_err = curs.Next(context); rsql_err != nil { // TRANSITORY or PUBLIC pages
			return rsql_err
		}

		if rc == csr.CURS_RECORD_RESET {
			break
		}

		// delete record base table and indexes

		if count, rsql_err = btree.Delete_row(wcache, &delpad_bag, target_cursor.Gtabledef, target_cursor.Base_fields, nil); rsql_err != nil { // count is 0 or 1
			return rsql_err
		}

		deleted_row_count += int64(count)

		// check periodically if client is alive

		if rsql_err := context.Check_keepalive(); rsql_err != nil {
			return rsql_err
		}
	}

	// confirm the modifications

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	if context.Ctx_trancount == 0 {
		wcache.COMMIT() // table pages are written to disk
	}

	// send record count

	wcache.Wpc_last_rowcount = deleted_row_count

	if context.Ctx_nocount == false {
		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_EXECUTION_FINISHED)) // prepare response type
		context.Ctx_mw.WriteInt64(deleted_row_count)
		if rsql_err := context.Writer_flush(); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}
