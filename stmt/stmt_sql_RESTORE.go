package stmt

import (
	"archive/tar"
	"bufio"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"rsql"
	"rsql/ast"
	"rsql/cache"
	"rsql/dict"
)

const READER_BUFSIZE = 32768

// RESTORE DATABASE database_name
//  FROM DISK =  'file_name'
//
//
// Example:
//
//    RESTORE DATABASE mydb
//      FROM DISK = '/Backups/mydb.rbak'
//
func RESTORE(context *rsql.Context, database_name string, filename string, params rsql.Restore_params) (rsql_err *rsql.Error) {
	var (
		err             error
		dump_dir        string
		file_path       string
		file            *os.File
		buffered_reader *bufio.Reader
		gzip_reader     *gzip.Reader
		tar_reader      *tar.Reader

		scanner                     *bufio.Scanner
		content                     []byte
		backup_database_name        string
		backup_date                 string
		db_owner                    string
		internal_database_name_path string
		internal_date_path          string
		internal_logins_path        string
		internal_db_owner_path      string
		internal_users_path         string
		internal_roles_path         string
		internal_membership_path    string
	)

	if rsql_err := dict.MASTER.Check_is_SA(context); rsql_err != nil { // check if operation is allowed
		return rsql_err
	}

	//====== get file path =====

	file_path = filename
	if filepath.IsAbs(file_path) == false { // if file path is not absolute, append it to default dump directory
		dump_dir = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_DUMP_DIR)
		if dump_dir == "" {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_DUMP_DIR_NOT_INITIALIZED, rsql.ERROR_BATCH_ABORT)
		}

		file_path = filepath.Join(dump_dir, filename)
	}

	file_path = filepath.Clean(file_path)
	if rsql_err := rsql.Check_forbidden_file_path(file_path); rsql_err != nil {
		return rsql_err
	}

	//====== create Reader for input file ======

	file, err = os.Open(file_path)
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}
	defer func() {
		err := file.Close()
		if err != nil && rsql_err == nil { // if file.Close() failed and no other error already occured
			rsql_err = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}
	}()

	buffered_reader = bufio.NewReaderSize(file, READER_BUFSIZE)

	if gzip_reader, err = gzip.NewReader(buffered_reader); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	tar_reader = tar.NewReader(gzip_reader)

	//====== read tar files ======

	// read database name

	internal_database_name_path = filepath.Join("rsqlbackup", "database")
	if backup_database_name, rsql_err = read_next_key_value_entry_from_tar(internal_database_name_path, "database_name", tar_reader); rsql_err != nil {
		return rsql_err
	}

	if database_name != backup_database_name {
		if params.Rt_replace == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_NEED_REPLACE_OPTION, rsql.ERROR_BATCH_ABORT, database_name, backup_database_name)
		}
	}

	// drop and create target database

	if rsql_err := context.Send_MESSAGE("Drop and create database [%s].", database_name); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := DROP_DATABASE(context, database_name); rsql_err != nil { // drop database if exists
		if rsql_err.Message_id != rsql.ERROR_MASTER_DATABASE_NAME_NOT_EXISTS {
			return rsql_err
		}
	}

	if rsql_err := CREATE_DATABASE(context, database_name); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = dict.MASTER.Change_database_status(context, database_name, dict.DB_STATUS_CORRUPTED); rsql_err != nil { // database status is corrupted until it is fully restored
		return rsql_err
	}

	// read date of backup

	internal_date_path = filepath.Join("rsqlbackup", "date")
	if backup_date, rsql_err = read_next_key_value_entry_from_tar(internal_date_path, "backup_date", tar_reader); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE("Restore database [%s] from backup file (database: [%s], date: %s).", database_name, backup_database_name, backup_date); rsql_err != nil {
		return rsql_err
	}

	// skip login, user and membership creation if no_user option

	if params.Rt_no_user {
		goto LABEL_TABLE_MARKER
	}

	// read logins

	internal_logins_path = filepath.Join("rsqlbackup", "logins")
	if content, rsql_err = read_next_string_entry_from_tar(internal_logins_path, tar_reader); rsql_err != nil {
		return rsql_err
	}

	scanner = bufio.NewScanner(bytes.NewBuffer(content))
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "" {
			continue
		}

		m, rsql_err := map_from_backup_line(internal_logins_path, scanner.Text())
		if rsql_err != nil {
			return rsql_err
		}

		login_name, ok := m["login"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_logins_path, "login")
		}
		login_user, ok := m["user_name"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_logins_path, "user_name")
		}
		login_hash, ok := m["hash"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_logins_path, "hash")
		}
		login_default_database, ok := m["default_database"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_logins_path, "default_database")
		}
		login_default_language, ok := m["default_language"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_logins_path, "default_language")
		}
		login_disabled, ok := m["disabled"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_logins_path, "disabled")
		}
		login_params := dict.Login_params{Lgp_name: login_name, Lgp_password_hash: login_hash, Lgp_default_database: login_default_database, Lgp_default_language: login_default_language}

		// add login if missing

		if rsql_err := dict.MASTER.Add_syslogin(context, login_name, login_params); rsql_err != nil {
			if rsql_err.Message_id != rsql.ERROR_MASTER_LOGIN_NAME_DUPLICATE {
				return rsql_err
			}
		} else { // new login has been created
			if rsql_err := context.Send_MESSAGE("Created login [%s] for user [%s].", login_name, login_user); rsql_err != nil {
				return rsql_err
			}

			// disable if needed

			if login_disabled == "t" {
				if rsql_err := context.Send_MESSAGE("Disable login [%s].", login_name); rsql_err != nil {
					return rsql_err
				}

				changes := dict.Login_params{Lgp_disable_option: "disable"}
				if rsql_err := dict.MASTER.Change_syslogin(context, login_name, changes); rsql_err != nil {
					return rsql_err
				}
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// read database owner

	internal_db_owner_path = filepath.Join("rsqlbackup", "db_owner")
	if db_owner, rsql_err = read_next_key_value_entry_from_tar(internal_db_owner_path, "db_owner", tar_reader); rsql_err != nil {
		return rsql_err
	}

	if db_owner != "sa" {
		if params.Rt_verbose {
			if rsql_err := context.Send_MESSAGE("Database owner set to %s.", db_owner); rsql_err != nil {
				return rsql_err
			}
		}

		if rsql_err := dict.MASTER.Change_authorization(context, database_name, db_owner); rsql_err != nil {
			return rsql_err
		}
	}

	// read users

	internal_users_path = filepath.Join("rsqlbackup", "users")
	if content, rsql_err = read_next_string_entry_from_tar(internal_users_path, tar_reader); rsql_err != nil {
		return rsql_err
	}

	scanner = bufio.NewScanner(bytes.NewBuffer(content))
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "" {
			continue
		}

		m, rsql_err := map_from_backup_line(internal_users_path, scanner.Text())
		if rsql_err != nil {
			return rsql_err
		}

		user_name, ok := m["user"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_users_path, "user")
		}
		for_login, ok := m["for_login"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_users_path, "for_login")
		}

		if user_name == "dbo" {
			continue
		}

		if params.Rt_verbose {
			if rsql_err := context.Send_MESSAGE("Create user [%s] for login [%s].", user_name, for_login); rsql_err != nil {
				return rsql_err
			}
		}

		if rsql_err := dict.MASTER.Add_sysprincipal_user(context, database_name, user_name, for_login); rsql_err != nil {
			return rsql_err
		}
	}
	if err := scanner.Err(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// read roles

	internal_roles_path = filepath.Join("rsqlbackup", "roles")
	if content, rsql_err = read_next_string_entry_from_tar(internal_roles_path, tar_reader); rsql_err != nil {
		return rsql_err
	}

	scanner = bufio.NewScanner(bytes.NewBuffer(content))
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "" {
			continue
		}

		m, rsql_err := map_from_backup_line(internal_roles_path, scanner.Text())
		if rsql_err != nil {
			return rsql_err
		}

		role_name, ok := m["role"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_roles_path, "role")
		}

		if params.Rt_verbose {
			if rsql_err := context.Send_MESSAGE("Create role [%s].", role_name); rsql_err != nil {
				return rsql_err
			}
		}

		if rsql_err := dict.MASTER.Add_sysprincipal_role(context, database_name, role_name); rsql_err != nil {
			return rsql_err
		}
	}
	if err := scanner.Err(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// read memberships

	internal_membership_path = filepath.Join("rsqlbackup", "memberships")
	if content, rsql_err = read_next_string_entry_from_tar(internal_membership_path, tar_reader); rsql_err != nil {
		return rsql_err
	}

	scanner = bufio.NewScanner(bytes.NewBuffer(content))
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "" {
			continue
		}

		m, rsql_err := map_from_backup_line(internal_membership_path, scanner.Text())
		if rsql_err != nil {
			return rsql_err
		}

		principal_name, ok := m["principal"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_membership_path, "principal")
		}
		member_of, ok := m["member_of"]
		if ok == false {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_ATTRIBUTE_MISSING, rsql.ERROR_BATCH_ABORT, internal_membership_path, "member_of")
		}

		if params.Rt_verbose {
			if rsql_err := context.Send_MESSAGE("Principal [%s] is member of role [%s].", principal_name, member_of); rsql_err != nil {
				return rsql_err
			}
		}

		if rsql_err := dict.MASTER.Change_sysprincipal_role(context, database_name, member_of, "", principal_name, ""); rsql_err != nil {
			return rsql_err
		}
	}
	if err := scanner.Err(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	// read entries until table marker

LABEL_TABLE_MARKER:
	for {
		hdr, err := tar_reader.Next()
		if err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}

		if strings.HasSuffix(hdr.Name, "info_tables") {
			break
		}
	}

	// read all tables entries

	var gtable_no int = 0

	for {
		var (
			err         error
			hdr         *tar.Header
			schema_name string
			gtabledef   *rsql.GTabledef
			script      []byte
			index2file  []byte
			bytes_count int64
		)

		//==== create table ====

		internal_tables_gtable_prefix := filepath.Join("rsqlbackup", "tables", fmt.Sprintf("gtable%d", gtable_no))
		internal_tables_gtable_script := filepath.Join(internal_tables_gtable_prefix, "script")

		hdr, err = tar_reader.Next()
		if err == io.EOF { // no more table in the backup file
			break
		}
		if err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_script, err)
		}

		if hdr.Name != internal_tables_gtable_script {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_script, "entry not found")
		}

		if script, err = ioutil.ReadAll(tar_reader); err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_script, err)
		}

		if schema_name, gtabledef, rsql_err = ast.Get_gtabledef_from_create_table_script(database_name, script); rsql_err != nil {
			return rsql_err
		}

		if rsql_err := context.Send_MESSAGE("Create table [%s].[%s].[%s].", database_name, schema_name, gtabledef.Gtable_name); rsql_err != nil {
			return rsql_err
		}

		if rsql_err := CREATE_TABLE(context, database_name, schema_name, gtabledef); rsql_err != nil {
			return rsql_err
		}

		// read index2file

		internal_tables_gtable_index2file := filepath.Join(internal_tables_gtable_prefix, "index2file")

		hdr, err = tar_reader.Next()
		if err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_index2file, err)
		}

		if hdr.Name != internal_tables_gtable_index2file {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_index2file, "entry not found")
		}

		if index2file, err = ioutil.ReadAll(tar_reader); err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_index2file, err)
		}

		index_count := 0
		map_index2file := make(map[string]string, 10)

		scanner = bufio.NewScanner(bytes.NewBuffer(index2file))
		for scanner.Scan() {
			if strings.TrimSpace(scanner.Text()) == "" {
				continue
			}

			index_key := fmt.Sprintf("index%d", index_count)

			pair, remaining, rsql_err := eat_key_value_pair(internal_tables_gtable_index2file, scanner.Text())
			if rsql_err != nil {
				return rsql_err
			}

			if pair.key != index_key {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_index2file, "index key not found")
			}

			if remaining != "" {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_index2file, "index2file entry not valid")
			}

			map_index2file[index_key] = pair.val
			index_count++
		}
		if err := scanner.Err(); err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}

		// copy base file

		internal_tables_gtable_base := filepath.Join(internal_tables_gtable_prefix, "base")

		hdr, err = tar_reader.Next()
		if err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_base, err)
		}

		if hdr.Name != internal_tables_gtable_base {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_base, "entry not found")
		}

		if bytes_count, rsql_err = restore_backup_tablefile(context, internal_tables_gtable_base, gtabledef.Base, tar_reader); rsql_err != nil {
			return rsql_err
		}

		if bytes_count != hdr.Size {
			err := fmt.Errorf("file length mismatch (%d != %d) for restore %s", bytes_count, hdr.Size, gtabledef.Base.Td_file_path)
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_base, err)
		}

		for index_no := 0; index_no < index_count; index_no++ {
			var (
				index_name  string
				tabledef    *rsql.Tabledef
				bytes_count int64
				ok          bool
			)

			indexn := fmt.Sprintf("index%d", index_no)
			internal_tables_gtable_indexn := filepath.Join(internal_tables_gtable_prefix, indexn)

			hdr, err = tar_reader.Next()
			if err != nil {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_indexn, err)
			}

			if hdr.Name != internal_tables_gtable_indexn {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_indexn, "entry not found")
			}

			if index_name, ok = map_index2file[indexn]; ok == false {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_indexn, "index not found in map_index2file")
			}

			if params.Rt_verbose {
				if rsql_err := context.Send_MESSAGE("    create index [%s].", index_name); rsql_err != nil {
					return rsql_err
				}
			}

			if tabledef, ok = gtabledef.Indexmap[index_name]; ok == false {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_indexn, "index Tabledef not found")
			}

			if bytes_count, rsql_err = restore_backup_tablefile(context, internal_tables_gtable_indexn, tabledef, tar_reader); rsql_err != nil {
				return rsql_err
			}

			if bytes_count != hdr.Size {
				err := fmt.Errorf("file length mismatch (%d != %d) for restore %s", bytes_count, hdr.Size, tabledef.Td_file_path)
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_indexn, err)
			}
		}

		// read permissions

		var permissions []byte

		internal_tables_gtable_permissions := filepath.Join(internal_tables_gtable_prefix, "permissions")

		hdr, err = tar_reader.Next()
		if err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_permissions, err)
		}

		if hdr.Name != internal_tables_gtable_permissions {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_permissions, "entry not found")
		}

		if permissions, err = ioutil.ReadAll(tar_reader); err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_permissions, err)
		}

		object_name := rsql.Object_qname_t{Database_name: database_name, Schema_name: schema_name, Object_name: gtabledef.Gtable_name, Object_name_original: gtabledef.Gtable_name}

		scanner = bufio.NewScanner(bytes.NewBuffer(permissions))
		for scanner.Scan() {
			if strings.TrimSpace(scanner.Text()) == "" {
				continue
			}

			pair, remaining, rsql_err := eat_key_value_pair(internal_tables_gtable_permissions, scanner.Text())
			if rsql_err != nil {
				return rsql_err
			}

			if pair.key != "principal" {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_permissions, "principal key not found")
			}

			principal_name := pair.val

			pair, remaining, rsql_err = eat_key_value_pair(internal_tables_gtable_permissions, remaining)
			if rsql_err != nil {
				return rsql_err
			}

			var grant_permission rsql.Permission_t
			var deny_permission rsql.Permission_t

			switch pair.key {
			case "grant":
				grant_permission = rt_permissions_table(pair.val)
			case "deny":
				deny_permission = rt_permissions_table(pair.val)
			default:
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_permissions, "grant or deny key not found")
			}

			if remaining != "" {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_tables_gtable_permissions, "permission entry not valid")
			}

			if rsql_err := dict.MASTER.Grant_or_deny_syspermission_for_principals(context, object_name, grant_permission, deny_permission, []string{principal_name}); rsql_err != nil {
				return rsql_err
			}
		}
		if err := scanner.Err(); err != nil {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
		}

		gtable_no++
	}

	//====== close all readers ======

	if err := gzip_reader.Close(); err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_IO_ERROR, rsql.ERROR_BATCH_ABORT, err)
	}

	//====== database status is set to ONLINE ======

	if rsql_err = dict.MASTER.Change_database_status(context, database_name, dict.DB_STATUS_ONLINE); rsql_err != nil {
		return rsql_err
	}

	return nil
}

// used by eat_key_value_pair
//
type pair_of_strings struct {
	key string
	val string
}

// eat_key_value_pair eats the first key:[value] pair in a string.
// The string must be in format format  key:[value];key:[value];...
//
// Values must be enclosed between brackets.
//
// The section argument is just the path, that is used for error message if content is invalid.
//
func eat_key_value_pair(section string, s string) (pair *pair_of_strings, remaining string, rsql_err *rsql.Error) {
	var i int

	s = strings.TrimSpace(s)

	if len(s) == 0 {
		return nil, "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, section, "no key:[value] pair in empty string")
	}

	// read key

	if i = strings.IndexByte(s, ':'); i < 0 {
		return nil, "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, section, "colon expected in key:[value] pair")
	}

	key := strings.TrimSpace(s[:i])

	if len(key) == 0 {
		return nil, "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, section, "key cannot be empty string")
	}

	if strings.ContainsAny(key, ";[]") {
		return nil, "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, section, "semicolon or brackets are not allowed in key string")
	}

	s = strings.TrimSpace(s[i+1:]) // remaining string

	// read [value]

	if len(s) == 0 || s[0] != '[' { // opening bracket is mandatory
		return nil, "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, section, "opening bracket expected in key:[value] pair")
	}

	if i = strings.IndexByte(s, ']'); i < 0 { // closing bracket must exist
		return nil, "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, section, "closing bracket expected in key:[value] pair")
	}
	val := s[1:i] // discard [ and ]

	pair = &pair_of_strings{key: key, val: val}

	s = strings.TrimSpace(s[i+1:]) // remaining string
	if len(s) == 0 || s == ";" {   // just a terminating semicolon
		return pair, "", nil
	}

	if s[0] != ';' {
		return nil, "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, section, "semicolon expected to separate key:[value] pairs")
	}
	s = strings.TrimSpace(s[1:]) // skip semicolon

	return pair, s, nil
}

// map_from_backup_line reads a string of format  key:[value];key:[value];...
// and returns a map of keys and values.
//
// Values must be enclosed between brackets.
//
// The section argument is just the path, that is used for error message if content is invalid.
//
func map_from_backup_line(section string, s string) (map[string]string, *rsql.Error) {
	var (
		m map[string]string
	)

	s = strings.TrimSpace(s)

	if len(s) == 0 {
		return nil, nil
	}

	m = make(map[string]string, 10)

	for {
		pair, remaining, rsql_err := eat_key_value_pair(section, s)
		if rsql_err != nil {
			return nil, rsql_err
		}

		m[pair.key] = pair.val

		if remaining == "" {
			return m, nil
		}

		s = remaining
	}

	panic("unreachable")
}

func read_next_string_entry_from_tar(internal_file_path string, tar_reader *tar.Reader) (content []byte, rsql_err *rsql.Error) {

	hdr, err := tar_reader.Next()
	if err == io.EOF {
		return nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, "entry not found")
	}
	if err != nil {
		return nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, err)
	}

	if internal_file_path != hdr.Name {
		return nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, fmt.Sprintf("got %s instead", hdr.Name))
	}

	// read entry content

	if content, err = ioutil.ReadAll(tar_reader); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, err)
	}

	return content, nil
}

func read_next_key_value_entry_from_tar(internal_file_path string, key string, tar_reader *tar.Reader) (val string, rsql_err *rsql.Error) {
	var (
		content   []byte
		pair      *pair_of_strings
		remaining string
	)

	if content, rsql_err = read_next_string_entry_from_tar(internal_file_path, tar_reader); rsql_err != nil {
		return "", rsql_err
	}

	if pair, remaining, rsql_err = eat_key_value_pair(internal_file_path, string(content)); rsql_err != nil {
		return "", rsql_err
	}

	_ = remaining

	if pair.key != key {
		return "", rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, fmt.Sprintf("key %s not found", key))
	}

	return pair.val, nil
}

func restore_backup_tablefile(context *rsql.Context, internal_file_path string, tabledef *rsql.Tabledef, tar_reader *tar.Reader) (bytes_written int64, rsql_err *rsql.Error) {
	var (
		err         error
		tablefile   *os.File
		bytes_count int64
	)

	if tablefile, err = os.OpenFile(tabledef.Td_file_path, os.O_WRONLY, rsql.TBLFILE_PERM); err != nil {
		return 0, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, err)
	}

	if bytes_count, rsql_err = copy_and_adjust_pages(context, tablefile, tar_reader, tabledef); rsql_err != nil {
		return 0, rsql_err
	}

	if err := tablefile.Sync(); err != nil {
		return 0, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, err)
	}

	if err := tablefile.Close(); err != nil {
		return 0, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_RESTORE_SECTION_ERROR, rsql.ERROR_BATCH_ABORT, internal_file_path, err)
	}

	return bytes_count, nil
}

func rt_permissions_table(permstr string) rsql.Permission_t {
	var (
		perm rsql.Permission_t
	)

	if len(permstr) == 0 {
		return 0
	}

	for _, c := range permstr {
		switch c {
		case 'S':
			perm |= rsql.PERMISSION_SELECT
		case 'U':
			perm |= rsql.PERMISSION_UPDATE
		case 'I':
			perm |= rsql.PERMISSION_INSERT
		case 'D':
			perm |= rsql.PERMISSION_DELETE
		default:
			// ignore
		}
	}

	return perm
}

func copy_and_adjust_pages(context *rsql.Context, dest io.Writer, src io.Reader, tabledef *rsql.Tabledef) (total_written int64, rsql_err *rsql.Error) {
	var (
		buf cache.Page
	)

	for {
		bytes_read, err_read := io.ReadFull(src, buf.Pg_canvas[:]) // read full page)
		if bytes_read > 0 {                                        // if read successful
			buf.Set_pg_tblid(cache.Tblid_t(tabledef.Td_tblid))
			buf.Set_pg_dbid(cache.Dbid_t(tabledef.Td_dbid))
			buf.Set_pg_schid(cache.Schid_t(tabledef.Td_schid))
			buf.Set_pg_base_gtblid(cache.Tblid_t(tabledef.Td_base_gtblid))

			bytes_written, err_write := dest.Write(buf.Pg_canvas[:bytes_read]) // write all read bytes into dest
			total_written += int64(bytes_written)

			if err_write != nil {
				return total_written, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COPY_IO_ERROR, rsql.ERROR_BATCH_ABORT, err_write) // error during write
			}
			if bytes_read != bytes_written { // should never happen (write must return an error in this case)
				return total_written, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COPY_IO_ERROR, rsql.ERROR_BATCH_ABORT, io.ErrShortWrite)
			}
		}
		if err_read == io.EOF {
			return total_written, nil // all src has been read and written to dest
		}
		if err_read != nil {
			return total_written, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_COPY_IO_ERROR, rsql.ERROR_BATCH_ABORT, err_read) // error when reading src
		}

		if rsql_err := context.Check_keepalive(); rsql_err != nil { // check periodically if client is alive
			return total_written, rsql_err
		}
	}

	panic("unreachable")
}
