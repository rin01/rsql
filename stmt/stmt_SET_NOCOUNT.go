package stmt

import (
	"rsql"
)

func SET_NOCOUNT(context *rsql.Context, arg bool) *rsql.Error {

	context.Ctx_nocount = arg

	return nil
}
