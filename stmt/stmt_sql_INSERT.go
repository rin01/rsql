package stmt

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"path/filepath"
	"time"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/htmlindex"
	"golang.org/x/text/transform" // Transformer is an interface, having methods Transform() and Reset()

	"rsql"
	"rsql/btree"
	"rsql/cache"
	"rsql/csr"
	"rsql/data"
	"rsql/dict"
)

func INSERT_VALUES(context *rsql.Context, table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef, list_rows []rsql.Row, explicit_IDENTITY_provided bool) *rsql.Error {
	var (
		rsql_err     *rsql.Error
		inspad_bag   btree.Inspad_bag
		record_count int64
	)

	// insert

	wcache := context.Ctx_wcache.(*cache.Wcache)

	inspad_bag.Initialize(table_qname, gtabledef)

	if rsql_err = btree.Insert_series_of_rows(wcache, &inspad_bag, gtabledef, list_rows, explicit_IDENTITY_provided); rsql_err != nil { // all pages with inserted records are DRAFT pages
		return rsql_err
	}

	// confirm the modifications

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	if context.Ctx_trancount == 0 {
		wcache.COMMIT() // table pages are written to disk
	}

	// send record count

	record_count = int64(len(list_rows))
	wcache.Wpc_last_rowcount = record_count

	if context.Ctx_nocount == false {
		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_EXECUTION_FINISHED)) // prepare response type
		context.Ctx_mw.WriteInt64(record_count)
		if rsql_err := context.Writer_flush(); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

func INSERT_SELECT(context *rsql.Context, table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef, params rsql.Insert_select_params) *rsql.Error {
	var (
		curs         csr.Cursor
		inspad_bag   btree.Inspad_bag
		record_count int64
	)

	wcache := context.Ctx_wcache.(*cache.Wcache)

	inspad_bag.Initialize(table_qname, gtabledef)

	curs = params.Ins_cursor.(csr.Cursor)

	// ======= insert records  =======

	base_row := params.Ins_base_row

	curs.Reset()

	for {
		// read record to insert

		var rsql_err *rsql.Error
		var rc csr.State_t

		if rc, rsql_err = curs.Next(context); rsql_err != nil {
			return rsql_err
		}

		if rc == csr.CURS_RECORD_RESET {
			break
		}

		// prepare base_row

		if _, rsql_err := rsql.Execute_basicblock(context, params.Ins_conversion_basicblock); rsql_err != nil {
			return rsql_err
		}

		for _, dataslot := range base_row {
			if rsql_err := dataslot.Error(); rsql_err != nil {
				return rsql_err
			}
		}

		// insert line into base table and indexes
		//rsql.Println(data.Debug_display_row(base_row, 20, '|', false))

		if rsql_err = btree.Insert_row_into_base_table_and_indexes(wcache, &inspad_bag, gtabledef, base_row, nil, params.Ins_explicit_IDENTITY_provided); rsql_err != nil { // all pages with inserted records are DRAFT pages
			return rsql_err
		}

		record_count++

		// check periodically if client is alive

		if rsql_err := context.Check_keepalive(); rsql_err != nil {
			return rsql_err
		}
	}

	// confirm the modifications

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	if context.Ctx_trancount == 0 {
		wcache.COMMIT() // table pages are written to disk
	}

	// send record count

	wcache.Wpc_last_rowcount = record_count

	if context.Ctx_nocount == false {
		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_EXECUTION_FINISHED)) // prepare response type
		context.Ctx_mw.WriteInt64(record_count)
		if rsql_err := context.Writer_flush(); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

func BULK_INSERT(context *rsql.Context, table_qname rsql.Object_qname_t, filename string, params rsql.Bulk_insert_params, gtabledef *rsql.GTabledef, syslang *data.SYSLANGUAGE) *rsql.Error {
	var (
		rsql_err                *rsql.Error
		err                     error
		ok                      bool
		bulk_dir                string
		file_path               string
		field_varchar           *data.VARCHAR
		base_tabledef           *rsql.Tabledef
		base_row                rsql.Row
		file                    *os.File
		file_reader             io.Reader
		scanner                 *bufio.Scanner
		firstline_no            uint64
		lastline_no             uint64
		line_no                 uint64
		line                    []byte
		line_remaining          []byte
		field_seqno             int
		endxpos                 int
		field                   []byte
		fieldterminator         []byte
		fieldterminator_length  int
		last_field_in_line_flag bool
		base_coldefs_count      int
		row_dataslot            rsql.IDataslot
		style                   *data.INT
		index_ROWID             int
		index_IDENTITY          int
		time_start              time.Time
		time_start_delta        time.Time

		inspad_bag btree.Inspad_bag

		record_count int64
	)

	const RECORD_COUNT_FOR_MESSAGE = 100000 // a message is sent to the client each time this number of records have been inserted

	wcache := context.Ctx_wcache.(*cache.Wcache)

	inspad_bag.Initialize(table_qname, gtabledef)

	base_tabledef = gtabledef.Base
	base_row, index_ROWID, index_IDENTITY = data.New_leaf_row(rsql.KIND_COL_LEAF, base_tabledef)
	rsql.Assert(index_ROWID == 0)
	rsql.Assert(base_row[0].Datatype() == rsql.DATATYPE_BIGINT) // ROWID is always NULL, and is always the first column of base table

	base_coldefs_count = len(base_tabledef.Td_coldefs)

	fieldterminator = []byte(params.Bi_opt_fieldterminator)
	fieldterminator_length = len(fieldterminator)
	rsql.Assert(fieldterminator_length > 0)

	field_varchar = data.New_VARCHAR_NULL(rsql.KIND_COL_LEAF, rsql.DATATYPE_VARCHAR_PRECISION_MAX, false)

	style = data.New_literal_INT_value(0) // used by date, time and datetime columns. For bulk insert, style is always 0, the DMY order being given by the current language.

	//====== get file path =====

	file_path = filename
	if filepath.IsAbs(file_path) == false { // if file path is not absolute, append it to default bulk directory
		bulk_dir = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_BULK_DIR)
		if bulk_dir == "" {
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_DIR_NOT_INITIALIZED, rsql.ERROR_BATCH_ABORT)
		}

		file_path = filepath.Join(bulk_dir, filename)
	}

	file_path = filepath.Clean(file_path)
	if rsql_err := rsql.Check_forbidden_file_path(file_path); rsql_err != nil {
		return rsql_err
	}

	//====== create scanner for reading input file ======

	file, err = os.Open(file_path)
	if err != nil {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_FILE_OPEN_FAILED, rsql.ERROR_BATCH_ABORT, err)
	}
	defer file.Close()

	if file_reader, rsql_err = get_bulk_insert_reader(file, params.Bi_opt_codepage); rsql_err != nil {
		return rsql_err
	}

	scanner = bufio.NewScanner(file_reader)

	// create split function for rowterminator if needed

	if params.Bi_opt_rowterminator != "\n" && params.Bi_opt_rowterminator != "\r\n" {
		split_func := create_rowterminator_split_function(params.Bi_opt_rowterminator)
		scanner.Split(split_func)
	}

	//====== for each line in file ======

	time_start = time.Now()
	time_start_delta = time_start

	firstline_no = params.Bi_opt_firstrow
	lastline_no = params.Bi_opt_lastrow

	if lastline_no != 0 && lastline_no < firstline_no {
		return nil
	}

	line_no = 0

	for {
		if scanner.Scan() == false { // read a line from file
			err := scanner.Err() // nil if EOF
			if err == nil {
				break //=====> EOF has been reached. Exit loop.
			}

			line_no++ // show proper line_no in following error message

			if rsql_err, ok = err.(*rsql.Error); ok == true {
				rsql_err.More_info("File \"%s\", line %d.", filename, line_no)
				return rsql_err
			}
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_SCANNER_FAILURE, rsql.ERROR_BATCH_ABORT, filename, line_no, err)
		}

		line_no++ // line has been read

		if line_no < firstline_no {
			continue
		}

		// process line

		line = scanner.Bytes()

		line_remaining = line
		field_seqno = 1 // column 0 is ROWID, which has no corresponding field in the file to import, and will be filled with an incremental value that is automatically generated
		last_field_in_line_flag = false

		for { // for each field in line
			// get field

			endxpos = bytes.Index(line_remaining, fieldterminator)
			if endxpos == -1 { // last field in line
				endxpos = len(line_remaining)
				last_field_in_line_flag = true
				field = line_remaining
			} else {
				field = line_remaining[:endxpos]
				line_remaining = line_remaining[endxpos+fieldterminator_length:]
			}

			if params.Bi_opt_rtrim { // if option rtrim, skip right blanks
				for i := len(field) - 1; i >= 0; i-- {
					if field[i] != ' ' {
						field = field[:i+1]
						break
					}
				}
			}

			// fill in dataslot. Note that IDENTITY column is always NOT NULL.

			row_dataslot = base_row[field_seqno]

			switch {
			case field_seqno == index_IDENTITY && params.Bi_opt_keepidentity == false: // ignore field in import file
				row_dataslot.Set_to_NULL() // even if column has NOT NULL clause. Indeed, a NULL value will be replaced by the next calculated identity value.

			case len(field) == 0: // if field is empty, it means NULL
				if base_tabledef.Td_coldefs[field_seqno].Cd_NOT_NULL_flag == true {
					return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BULK_INSERT_COLUMN_DOESNT_ALLOW_NULL, rsql.ERROR_BATCH_ABORT, filename, line_no, base_tabledef.Td_coldefs[field_seqno].Cd_colname)
				}

				row_dataslot.Set_to_NULL()

			case row_dataslot.Datatype() == rsql.DATATYPE_VARCHAR:
				switch {
				case len(field) == 1 && field[0] == 0x00: // if field is 0x00, it means that we want an empty string
					data.Assign_VARCHAR_empty_string(row_dataslot.(*data.VARCHAR))

				default:
					if rsql_err = data.Assign_VARCHAR_bytes(row_dataslot.(*data.VARCHAR), field, true); rsql_err != nil { // last arg normalizes field to NFC
						rsql_err.More_info("File \"%s\", line %d.", filename, line_no)
						return rsql_err
					}
				}

			default: // datatype is not VARCHAR
				if rsql_err = data.Assign_VARCHAR_bytes(field_varchar, field, false); rsql_err != nil { // last arg indicates that normalization of field is not needed
					rsql_err.More_info("File \"%s\", line %d.", filename, line_no)
					return rsql_err
				}

				data.Convert_VARCHAR_to_XXX(row_dataslot, field_varchar, style, syslang)
			}

			if rsql_err := row_dataslot.Error(); rsql_err != nil { // if error in field, return
				rsql_err.More_info("File \"%s\", line %d.", filename, line_no)
				return rsql_err
			}

			// break if last field in line

			if last_field_in_line_flag {
				break
			}

			field_seqno++

			if field_seqno >= base_coldefs_count {
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TOO_MANY_COLUMNS_IN_INPUT_FILE, rsql.ERROR_BATCH_ABORT, filename, line_no)
			}
		}

		if field_seqno != base_coldefs_count-1 { // -1 because base_coldefs_count includes ROWID
			return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_BAD_COLUMNS_COUNT_IN_INPUT_FILE, rsql.ERROR_BATCH_ABORT, filename, line_no, field_seqno, base_coldefs_count-1)
		}

		// insert line into base table and indexes
		//rsql.Println(data.Debug_display_row(base_row, 20, '|', false))

		if rsql_err = btree.Insert_row_into_base_table_and_indexes(wcache, &inspad_bag, gtabledef, base_row, nil, params.Bi_opt_keepidentity); rsql_err != nil { // all pages with inserted records are DRAFT pages
			rsql_err.More_info("File \"%s\", line %d.", filename, line_no)
			return rsql_err
		}

		record_count++

		if record_count%RECORD_COUNT_FOR_MESSAGE == 0 {
			if rsql_err := context.Send_MESSAGE("records inserted: %14d   rec/s: %.0f", record_count, RECORD_COUNT_FOR_MESSAGE/(time.Since(time_start_delta).Seconds())); rsql_err != nil {
				return rsql_err
			}

			time_start_delta = time.Now()
		}

		// check periodically if client is alive

		if rsql_err := context.Check_keepalive(); rsql_err != nil {
			return rsql_err
		}

		// if line_no limit has been reached, exit loop

		if lastline_no != 0 && line_no >= lastline_no {
			break
		}
	}

	// confirm the modifications

	if line_no != 0 { // at least one line has been inserted in table. Else, wcache has not been modified.
		wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

		if context.Ctx_trancount == 0 {
			wcache.COMMIT() // table pages are written to disk
		}
	}

	if rsql_err := context.Send_MESSAGE("records inserted: %14d   in   : %.0f seconds", record_count, time.Since(time_start).Seconds()); rsql_err != nil {
		return rsql_err
	}

	// send record count

	wcache.Wpc_last_rowcount = record_count

	if context.Ctx_nocount == false {
		context.Ctx_mw.WriteUint8(uint8(rsql.RESTYP_EXECUTION_FINISHED)) // prepare response type
		context.Ctx_mw.WriteInt64(record_count)
		if rsql_err := context.Writer_flush(); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

func get_bulk_insert_reader(file *os.File, encoding_name string) (io.Reader, *rsql.Error) {
	var (
		err             error
		enc             encoding.Encoding // just an interface, with methods NewDecoder() and NewEncoder() returning a Transformer. Real implementation of encoders/decoders are in golang.org/x/text/encoding/charmap package.
		decoder         transform.Transformer
		buffered_reader *bufio.Reader
		utf8_reader     *transform.Reader
	)

	// create buffio.Reader object to buffer the file

	buffered_reader = bufio.NewReader(file)

	// get decoder

	switch encoding_name {
	case "utf8", "utf-8":
		encoding_name = "raw"
	case "acp":
		encoding_name = "windows-1252"
	}

	if encoding_name == "raw" {
		return buffered_reader, nil //===>  if "raw", just return a buffered Reader
	}

	//=== create a Reader that converts original file content to utf8 ===

	if enc, err = htmlindex.Get(encoding_name); err != nil { // get encoding from a usual name, case insensitive
		return nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_ENCODING_NAME_NOT_FOUND, rsql.ERROR_BATCH_ABORT, encoding_name)

	}

	decoder = enc.NewDecoder() // decodes to utf8

	// create transform.Reader object

	utf8_reader = transform.NewReader(buffered_reader, decoder) // transform.Reader object implements io.Reader interface

	return utf8_reader, nil
}

// create_rowterminator_split_function creates a split function object, similar to bufio.ScanLines(), but with any row terminator.
//
// As it is not possible to pass a rowterminator as argument to the split function, we use a closure.
//
// rowterminator MUST BE TERMINATED BY \n OR \r\n.
//
func create_rowterminator_split_function(rowterminator string) func(data []byte, atEOF bool) (advance int, token []byte, err error) {
	var (
		terminator []byte
	)

	rsql.Assert(rowterminator[len(rowterminator)-1] == '\n')
	rowterminator = rowterminator[:len(rowterminator)-1] // discard \n

	if len(rowterminator) > 0 && rowterminator[len(rowterminator)-1] == '\r' {
		rowterminator = rowterminator[:len(rowterminator)-1] // discard \r
	}

	terminator = []byte(rowterminator)

	// create split function

	split_function := func(data []byte, atEOF bool) (advance int, token []byte, err error) {

		if atEOF && len(data) == 0 {
			return 0, nil, nil
		}

		lookup_pos := 0

		for {
			i := bytes.IndexByte(data[lookup_pos:], '\n')
			if i < 0 {
				break
			}

			endx_pos := i + lookup_pos
			rsql.Assert(data[endx_pos] == '\n')
			if endx_pos > 0 && data[endx_pos-1] == '\r' {
				endx_pos--
			}

			if bytes.HasSuffix(data[:endx_pos], terminator) == false {
				lookup_pos += i + 1
				continue
			}

			return lookup_pos + i + 1, data[:endx_pos-len(terminator)], nil
		}

		// if EOF is true, we are at the last line, which is not terminated by rowterminator

		if atEOF {
			return 0, nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_LAST_LINE_WITHOUT_ROWTERMINATOR, rsql.ERROR_BATCH_ABORT)
		}

		// no complete line found, request more data

		return 0, nil, nil
	}

	return split_function
}
