package stmt

import (
	"fmt"
	"os"

	"rsql"
	"rsql/cache"
	"rsql/data"
	"rsql/dict"
	"rsql/lang"
	"rsql/lk"
)

func SHOW_COLLATIONS(context *rsql.Context) *rsql.Error {

	// print

	if rsql_err := context.Send_MESSAGE(rsql.G_LIST_OF_RSQL_COLLATIONS_FOR_DISPLAY); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_LANGUAGES(context *rsql.Context) *rsql.Error {

	// print

	if rsql_err := context.Send_MESSAGE(lang.G_LIST_OF_RSQL_LANGUAGES_FOR_DISPLAY); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_LOCKS(context *rsql.Context) *rsql.Error {
	var (
		list []string
	)

	if rsql_err := dict.MASTER.Check_SA(context); rsql_err != nil {
		return rsql_err
	}

	// get list of locks

	list = lk.G_LOCK_MANAGER.Get_locks_list(context.Ctx_worker_id)

	// print

	if rsql_err := context.Send_MESSAGE_no_flush("LIST OF LOCKS\n-------------"); rsql_err != nil { // print header
		return rsql_err
	}

	for _, s := range list { // print list of locks
		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_WORKERS(context *rsql.Context) *rsql.Error {
	var (
		list []string
	)

	if rsql_err := dict.MASTER.Check_SA(context); rsql_err != nil {
		return rsql_err
	}

	// get list of workers

	list = cache.G_WORKER_QUEUE.List_workers()

	// print

	if rsql_err := context.Send_MESSAGE_no_flush("LIST OF WORKERS\n---------------"); rsql_err != nil { // print header
		return rsql_err
	}

	for _, s := range list { // print list of workers
		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_INFO(context *rsql.Context, params rsql.Show_info_params) *rsql.Error {
	var (
		err                    error
		rsql_err               *rsql.Error
		hostname               string
		dirname                string
		database_name_extended string
		database_info          string
	)

	if hostname, err = os.Hostname(); err != nil {
		hostname = err.Error()
	}

	if dirname, err = os.Getwd(); err != nil {
		dirname = err.Error()
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("INFO for worker id %d\n--------------------------------------------------", context.Ctx_worker_id)); rsql_err != nil { // print header
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("server address:            %s", rsql.G_LISTENER_ADDRESS)); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("server name:               %s", params.Shinf_servername)); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("host:                      %s", hostname)); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("base directory:            %s", dirname)); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("version:                   %s", rsql.VERSION)); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("server default collation:  %s", params.Shinf_server_default_collation)); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("login:                     %s", params.Shinf_login.(*data.VARCHAR).Stringval())); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("current user:              %s", params.Shinf_current_user.(*data.VARCHAR).Stringval())); rsql_err != nil {
		return rsql_err
	}

	database_name_extended = params.Shinf_current_database.(*data.VARCHAR).Stringval()
	if database_info, rsql_err = dict.MASTER.Get_sysdatabase_info(database_name_extended); rsql_err != nil {
		database_info = "[" + rsql_err.Error_short() + "]"
	}
	if database_info != "" {
		database_name_extended += "   " + database_info
	}

	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("current database:          %s", database_name_extended)); rsql_err != nil {
		return rsql_err
	}

	language, first_day_of_week, DMY := params.Shinf_current_language.(*data.SYSLANGUAGE).Values()
	if rsql_err := context.Send_MESSAGE_no_flush(fmt.Sprintf("current language:          %s, first day of week:%d, %s", language, first_day_of_week, DMY)); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	// flush

	if rsql_err := context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

const (
	MESSAGE_NO_OBJECT_FOUND = "\nSHOW: object [%s] not found."
	MESSAGE_COUNT           = "\nelement count: %d"
)

func SHOW_PARAMETER(context *rsql.Context, params rsql.Show_params) *rsql.Error {
	var (
		rsql_err *rsql.Error
		header   string
		list     []dict.Show_sysparameter_item
	)

	// get list of sysparameters

	if header, list, rsql_err = dict.MASTER.Retrieve_sorted_list_of_sysparameters(context, params); rsql_err != nil {
		return rsql_err
	}

	// print

	if header != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(header); rsql_err != nil { // print header
			return rsql_err
		}
	}

	for _, elem := range list { // print list of parameters
		s := elem.Output_text

		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_LOGIN(context *rsql.Context, params rsql.Show_params) *rsql.Error {
	var (
		rsql_err *rsql.Error
		header   string
		list     []dict.Show_login_item
		count    int
	)

	// get list of logins

	if header, list, rsql_err = dict.MASTER.Retrieve_sorted_list_of_logins(context, params); rsql_err != nil {
		return rsql_err
	}

	// print

	if header != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(header); rsql_err != nil { // print header
			return rsql_err
		}
	}

	for _, elem := range list { // print list of logins
		if rsql.Rtrim(elem.Output_text) == "" {
			continue
		}

		s := elem.Output_text

		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}

		count++
	}

	// print element count

	if params.Sh_output_type&(rsql.SH_OUTPUT_NORMAL|rsql.SH_OUTPUT_ID) != 0 {
		if rsql_err := context.Send_MESSAGE_no_flush(MESSAGE_COUNT, count); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_DATABASE(context *rsql.Context, params rsql.Show_params) *rsql.Error {
	var (
		rsql_err *rsql.Error
		header   string
		list     []dict.Show_database_item
		count    int
	)

	// get list of databases

	if header, list, rsql_err = dict.MASTER.Retrieve_sorted_list_of_databases(context, params); rsql_err != nil {
		return rsql_err
	}

	// print

	if header != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(header); rsql_err != nil { // print header
			return rsql_err
		}
	}

	for i, elem := range list { // print list of databases
		if rsql.Rtrim(elem.Output_text) == "" {
			continue
		}

		s := elem.Output_text

		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}

		if params.Sh_output_type == rsql.SH_OUTPUT_SQL && i < len(list)-1 {
			if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
				return rsql_err
			}
		}

		count++
	}

	// print element count

	if params.Sh_output_type&(rsql.SH_OUTPUT_NORMAL|rsql.SH_OUTPUT_ID) != 0 {
		if rsql_err := context.Send_MESSAGE_no_flush(MESSAGE_COUNT, count); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_USER(context *rsql.Context, params rsql.Show_params) *rsql.Error {
	var (
		rsql_err              *rsql.Error
		header                string
		list                  []dict.Show_principal_item
		sql_membership_script string
		count                 int
	)

	// get list of sysparameters

	if header, list, sql_membership_script, rsql_err = dict.MASTER.Retrieve_sorted_list_of_principals(context, params, dict.PAL_TYPE_USER); rsql_err != nil {
		return rsql_err
	}

	// print

	if header != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(header); rsql_err != nil { // print header
			return rsql_err
		}
	}

	for _, elem := range list { // print list of users
		if rsql.Rtrim(elem.Output_text) == "" {
			continue
		}

		s := elem.Output_text

		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}

		//if params.Sh_output_type & (rsql.SH_OUTPUT_NORMAL|rsql.SH_OUTPUT_ID) != 0 && params.Sh_option_PERM && i < len(list)-1 {
		//	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		//		return rsql_err
		//	}
		//}

		count++
	}

	// print membership script ALTER ROLE ADD MEMBER ...

	if sql_membership_script != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
			return rsql_err
		}

		if rsql_err := context.Send_MESSAGE_no_flush(sql_membership_script); rsql_err != nil {
			return rsql_err
		}
	}

	// print element count

	if params.Sh_output_type&(rsql.SH_OUTPUT_NORMAL|rsql.SH_OUTPUT_ID) != 0 {
		if rsql_err := context.Send_MESSAGE_no_flush(MESSAGE_COUNT, count); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_ROLE(context *rsql.Context, params rsql.Show_params) *rsql.Error {
	var (
		rsql_err              *rsql.Error
		header                string
		list                  []dict.Show_principal_item
		sql_membership_script string
		count                 int
	)

	// get list of sysparameters

	if header, list, sql_membership_script, rsql_err = dict.MASTER.Retrieve_sorted_list_of_principals(context, params, dict.PAL_TYPE_ROLE); rsql_err != nil {
		return rsql_err
	}

	// print

	if header != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(header); rsql_err != nil { // print header
			return rsql_err
		}
	}

	for _, elem := range list { // print list of roles
		if rsql.Rtrim(elem.Output_text) == "" {
			continue
		}

		s := elem.Output_text

		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}

		//if params.Sh_output_type & (rsql.SH_OUTPUT_NORMAL|rsql.SH_OUTPUT_ID) != 0 && params.Sh_option_PERM && i < len(list)-1 {
		//	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		//		return rsql_err
		//	}
		//}

		count++
	}

	// print membership script ALTER ROLE ADD MEMBER ...

	if sql_membership_script != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
			return rsql_err
		}

		if rsql_err := context.Send_MESSAGE_no_flush(sql_membership_script); rsql_err != nil {
			return rsql_err
		}
	}

	// print element count

	if params.Sh_output_type&(rsql.SH_OUTPUT_NORMAL|rsql.SH_OUTPUT_ID) != 0 {
		if rsql_err := context.Send_MESSAGE_no_flush(MESSAGE_COUNT, count); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func SHOW_TABLE(context *rsql.Context, params rsql.Show_params) *rsql.Error {
	var (
		rsql_err *rsql.Error
		header   string
		list     []dict.Show_table_item
		count    int
	)

	// get list of tables

	if header, list, rsql_err = dict.MASTER.Retrieve_sorted_list_of_GTabledefs(context, params); rsql_err != nil {
		return rsql_err
	}

	// print

	if header != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(header); rsql_err != nil { // print header
			return rsql_err
		}
	}

	for i, elem := range list { // print list of tables
		if rsql.Rtrim(elem.Output_text) == "" {
			continue
		}

		s := elem.Output_text

		if rsql_err := context.Send_MESSAGE_no_flush(s); rsql_err != nil {
			return rsql_err
		}

		if params.Sh_output_type == rsql.SH_OUTPUT_SQL && i < len(list)-1 {
			if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
				return rsql_err
			}
		}

		count++
	}

	// if count == 0, print no element found

	if params.Sh_output_type == rsql.SH_OUTPUT_SQL && count == 0 && params.Sh_object_name != "" {
		if rsql_err := context.Send_MESSAGE_no_flush(MESSAGE_NO_OBJECT_FOUND, params.Sh_object_name); rsql_err != nil {
			return rsql_err
		}
	}

	// print element count

	if params.Sh_output_type&(rsql.SH_OUTPUT_NORMAL|rsql.SH_OUTPUT_ID) != 0 {
		if rsql_err := context.Send_MESSAGE_no_flush(MESSAGE_COUNT, count); rsql_err != nil {
			return rsql_err
		}
	}

	// flush

	if rsql_err := context.Send_MESSAGE_no_flush(""); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}
