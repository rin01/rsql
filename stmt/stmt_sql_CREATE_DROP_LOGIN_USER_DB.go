package stmt

import (
	"rsql"
	"rsql/dict"
)

func ALTER_SERVER_PARAMETER(context *rsql.Context, param_id dict.Server_parameter_t, value_int int64, value_string string) *rsql.Error {

	switch param_id {
	case dict.PARAM_SERVER_DEFAULT_COLLATION, // change takes effect after restart
		dict.PARAM_SERVER_SERVERNAME:
		if rsql_err := dict.MASTER.Update_sysparameter_value_string(context, param_id, value_string, dict.PARAM_UPDATE_AT_RESTART); rsql_err != nil {
			return rsql_err
		}

		if rsql_err := context.Send_MESSAGE("NOTE: the new value of parameter \"%s\" will take effect only after the database server is restarted.", param_id.String()); rsql_err != nil {
			return rsql_err
		}

	case dict.PARAM_SERVER_DEFAULT_DATABASE, // change takes effect immediately
		dict.PARAM_SERVER_DEFAULT_LANGUAGE,
		dict.PARAM_SERVER_BULK_DIR,
		dict.PARAM_SERVER_DUMP_DIR:
		if rsql_err := dict.MASTER.Update_sysparameter_value_string(context, param_id, value_string, dict.PARAM_UPDATE_IMMEDIATE); rsql_err != nil {
			return rsql_err
		}

	case dict.PARAM_SERVER_WORKERS_MAX, // change takes effect after restart
		dict.PARAM_SERVER_GLOBAL_PAGE_CACHE_MEMORY,
		dict.PARAM_SERVER_LOCK_TICKER_INTERVAL,
		dict.PARAM_SERVER_LOCK_TIMEOUT_TICKS_COUNT,
		dict.PARAM_SERVER_LOGGING_MAX_SIZE,
		dict.PARAM_SERVER_LOGGING_MAX_COUNT,
		dict.PARAM_SERVER_LOGGING_LOCALTIME,
		dict.PARAM_SERVER_READ_TIMEOUT,
		dict.PARAM_SERVER_WCACHE_MEMORY_MAX,
		dict.PARAM_SERVER_WCACHE_MODIF_MAX,
		dict.PARAM_SERVER_BATCH_TEXT_MAX_SIZE,
		dict.PARAM_SERVER_BATCH_INSERTS_MAX_COUNT:
		if rsql_err := dict.MASTER.Update_sysparameter_value_int(context, param_id, value_int, dict.PARAM_UPDATE_AT_RESTART); rsql_err != nil {
			return rsql_err
		}

		if rsql_err := context.Send_MESSAGE("NOTE: the new value of parameter \"%s\" will take effect only after the database server is restarted.", param_id.String()); rsql_err != nil {
			return rsql_err
		}

	case // change takes effect immediately
		dict.PARAM_SERVER_QUOTED_IDENTIFIER:
		if rsql_err := dict.MASTER.Update_sysparameter_value_int(context, param_id, value_int, dict.PARAM_UPDATE_IMMEDIATE); rsql_err != nil {
			return rsql_err
		}

	default:
		panic("unexpected ALTER SERVER PARAMETER")
	}

	return nil
}

func CREATE_LOGIN(context *rsql.Context, login_name string, login_params dict.Login_params) *rsql.Error {

	if rsql_err := dict.MASTER.Add_syslogin(context, login_name, login_params); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func ALTER_LOGIN(context *rsql.Context, login_name string, changes dict.Login_params) *rsql.Error {

	if rsql_err := dict.MASTER.Change_syslogin(context, login_name, changes); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func DROP_LOGIN(context *rsql.Context, login_name string) *rsql.Error {

	if rsql_err := dict.MASTER.Remove_syslogin(context, login_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func CREATE_DATABASE(context *rsql.Context, database_name string) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "CREATE DATABASE") // in fact, there is no problem running CREATE DATABASE inside a transaction, but there is no reason to allow that.
	}

	if rsql_err := dict.MASTER.Add_sysdatabase(context, database_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func ALTER_DATABASE(context *rsql.Context, database_name string, changes dict.Database_params) *rsql.Error {

	const change_name_message = `INFO: database name changed from [%s] to [%s].
       If you want to change the default database of the affected logins, just copy-paste and execute the following line to generate the proper ALTER LOGIN statements:
       SHOW TEMPLATE = '{{if eq .default_database_name "%s"}}ALTER LOGIN [{{.login_name}}] WITH DEFAULT_DATABASE=[%s];{{end}}' LOGINS
`

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "ALTER DATABASE") // in fact, there is no problem running ALTER DATABASE inside a transaction, but there is no reason to allow that.
	}

	if rsql_err := dict.MASTER.Change_sysdatabase(context, database_name, changes); rsql_err != nil {
		return rsql_err
	}

	// if database name has been modified, display a message

	if changes.Dbp_name != "" && changes.Dbp_name != database_name {
		if rsql_err := context.Send_MESSAGE(change_name_message, database_name, changes.Dbp_name, database_name, changes.Dbp_name); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

func DROP_DATABASE(context *rsql.Context, database_name string) *rsql.Error {

	if context.Ctx_trancount != 0 { // to allow DROP DATABASE inside a transaction, we would have to purge wcache from all pages belonging to the database.
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "DROP DATABASE") // So, it is simpler to just forbid this statement to run inside a transaction
	}

	if rsql_err := dict.MASTER.Remove_sysdatabase(context, database_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func CREATE_USER(context *rsql.Context, database_name string, user_name string, login_name string) *rsql.Error {

	if rsql_err := dict.MASTER.Add_sysprincipal_user(context, database_name, user_name, login_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func ALTER_USER(context *rsql.Context, database_name string, user_name string, change_name string, change_login string) *rsql.Error {

	if rsql_err := dict.MASTER.Change_sysprincipal_user(context, database_name, user_name, change_name, change_login); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func DROP_USER(context *rsql.Context, database_name string, user_name string) *rsql.Error {

	if rsql_err := dict.MASTER.Remove_sysprincipal_user(context, database_name, user_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func CREATE_ROLE(context *rsql.Context, database_name string, role_name string) *rsql.Error {

	if rsql_err := dict.MASTER.Add_sysprincipal_role(context, database_name, role_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func ALTER_ROLE(context *rsql.Context, database_name string, role_name string, change_name string, add_member_name string, drop_member_name string) *rsql.Error {

	if rsql_err := dict.MASTER.Change_sysprincipal_role(context, database_name, role_name, change_name, add_member_name, drop_member_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func DROP_ROLE(context *rsql.Context, database_name string, role_name string) *rsql.Error {

	if rsql_err := dict.MASTER.Remove_sysprincipal_role(context, database_name, role_name); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func GRANT(context *rsql.Context, object_name rsql.Object_qname_t, permission rsql.Permission_t, list_of_principal_names []string) *rsql.Error {

	if rsql_err := dict.MASTER.Grant_or_deny_syspermission_for_principals(context, object_name, permission, 0, list_of_principal_names); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func DENY(context *rsql.Context, object_name rsql.Object_qname_t, permission rsql.Permission_t, list_of_principal_names []string) *rsql.Error {

	if rsql_err := dict.MASTER.Grant_or_deny_syspermission_for_principals(context, object_name, 0, permission, list_of_principal_names); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func REVOKE(context *rsql.Context, object_name rsql.Object_qname_t, permission rsql.Permission_t, list_of_principal_names []string, from_all bool) *rsql.Error {

	if rsql_err := dict.MASTER.Revoke_syspermission_for_principals(context, object_name, permission, list_of_principal_names, from_all); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func ALTER_AUTHORIZATION(context *rsql.Context, database_name string, change_owner_login string) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "ALTER AUTHORIZATION") // in fact, there is no problem running ALTER AUTHORIZATION inside a transaction, but there is no reason to allow that.
	}

	if rsql_err := dict.MASTER.Change_authorization(context, database_name, change_owner_login); rsql_err != nil {
		return rsql_err
	}

	return nil
}
