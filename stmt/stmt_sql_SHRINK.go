package stmt

import (
	"rsql"
	"rsql/btree"
	"rsql/cache"
)

func SHRINK_TABLE(context *rsql.Context, table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef) *rsql.Error {

	if context.Ctx_trancount != 0 {
		return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TRANSACTION_FORBIDDEN_FOR_STMT, rsql.ERROR_BATCH_ABORT, "SHRINK TABLE")
	}

	// modify info pages

	wcache := context.Ctx_wcache.(*cache.Wcache)

	shrink_list := btree.Shrink_base_table_and_indexes(wcache, gtabledef)

	// note: there is no pages to discard from wcache (as is the case for TRUNCATE), because SHRINK is run inside its own transaction and thus has no page in wcache.
	//       So, there is no need to call    wcache.Discard_all_TRANSITORY_elements_and_pages_from_wcache(cache.Tblid_t(tabledef.Td_tblid), uint64(highest_used_page_no+1))

	// confirm modifications

	wcache.Transform_all_DRAFT_elements_to_TRANSITORY()

	rsql.Assert(context.Ctx_trancount == 0)

	wcache.COMMIT() // info page must be modified on disk, before we can physically truncate the file, because the trailing pages are getting physically lost.

	// physical shrinking of the files

	for _, item := range shrink_list {
		//println("           SHRINK", item.Sh_tabledef.Td_tblid, item.Sh_highest_used_page_no)
		wcache.Filebag().Shrink_file(item.Sh_tabledef, int64(item.Sh_highest_used_page_no+1))
	}

	wcache.Filebag().Sync_modified_files()

	return nil
}
