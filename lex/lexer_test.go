package lex

import (
	"fmt"
	"math"
	"strings"
	"testing"

	"rsql"
)

func Test_lexer_eat_hexa(t *testing.T) {
	var (
		rsql_err_message_id  rsql.Message_id_t
		rsql_err_message_txt string
	)

	samples := []struct {
		spl_in         string
		spl_res        uint32
		spl_message_id rsql.Message_id_t
	}{
		{"0000", 0, 0},
		{"0001", 1, 0},
		{"000f", 15, 0},
		{"00ff", 255, 0},
		{"ffff", 65535, 0},
		{"ffffx", 65535, 0},
		{"FFFFx", 65535, 0},
		{"fffx", 0, rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{"xfff", 0, rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{"FFF", 0, rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{"f", 0, rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{"", 0, rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
	}

	for _, sample := range samples {

		spl_in, spl_res, spl_message_id := sample.spl_in, sample.spl_res, sample.spl_message_id

		l := New_lexer()
		txt := []byte(spl_in)
		l.Attach_batch(txt)

		r, rsql_err := l.eat_hexa0(4)
		rsql_err_message_id = 0
		rsql_err_message_txt = ""
		if rsql_err != nil {
			rsql_err_message_id = rsql_err.Message_id
			rsql_err_message_txt = rsql_err.Error()
		}

		if r != spl_res || rsql_err_message_id != spl_message_id {
			//			fmt.Println(spl_in, r, spl_res, err_message_id, spl_message_id)
			t.Fatalf("failure <%s> != <%d> %v %v", spl_in, spl_res, rsql_err_message_id, spl_message_id)
		}

		//		fmt.Printf("<%s> <%d> [%v] [%v] %s\n", spl_in, spl_res, err_message_id, spl_message_id, err_message_txt)
	}

	_ = rsql_err_message_txt

}

func Test_lexer_To_uint16(t *testing.T) {

	ss := []struct {
		in  string
		out uint16
	}{
		{"123", 123},
		{"65535", math.MaxUint16},
		{"0", 0},
	}

	for _, sample := range ss {

		lexeme := Lexeme{LEX_LITERAL_NUMBER, LEX_LITERAL_NUMBER_INTEGRAL, sample.in}
		if i, rsql_err := lexeme.To_uint16(); rsql_err != nil || i != sample.out {
			t.Fatalf("Lexeme %s should be convertible to uint32", lexeme.Lex_word)
		}
	}

	// check errors

	sss := []struct {
		in  string
		out rsql.Message_id_t
	}{
		{"abc", rsql.ERROR_LEXEME_TO_UINT16_FAILED},
		{"-1", rsql.ERROR_LEXEME_TO_UINT16_FAILED},
		{"65538", rsql.ERROR_LEXEME_TO_UINT16_FAILED},
		{"", rsql.ERROR_LEXEME_TO_UINT16_FAILED},
	}

	for _, sample := range sss {

		lexeme := Lexeme{LEX_LITERAL_NUMBER, LEX_LITERAL_NUMBER_INTEGRAL, sample.in}
		if _, rsql_err := lexeme.To_uint16(); rsql_err == nil || rsql_err.Message_id != sample.out {
			t.Fatalf("Lexeme %s should return the error %v", lexeme.Lex_word, sample.out)
		}
	}

}

func Test_lexer_Is_identpart_max(t *testing.T) {

	lexeme := Create_lexeme(LEX_IDENTPART, LEX_IDENTPART_SUB, "asdf")
	if lexeme.Is_identpart_max() == true {
		t.Fatalf("Lexeme %s should not be 'max'", lexeme.Lex_word)
	}

	lexeme = Create_lexeme(LEX_IDENTPART, LEX_IDENTPART_SUB, "max")
	if lexeme.Is_identpart_max() == false {
		t.Fatalf("Lexeme %s should be 'max'", lexeme.Lex_word)
	}

	lexeme = Create_lexeme(LEX_IDENTPART, LEX_IDENTPART_SUB, "mac")
	if lexeme.Is_identpart_max() == true {
		t.Fatalf("Lexeme %s should not be 'max'", lexeme.Lex_word)
	}

	lexeme = Create_lexeme(LEX_IDENTPART, LEX_IDENTPART_SUB, "MaX")
	if lexeme.Is_identpart_max() == false {
		t.Fatalf("Lexeme %s should be 'max'", lexeme.Lex_word)
	}

	lexeme = Create_lexeme(LEX_IDENTPART, LEX_IDENTPART_SUB, "MAX")
	if lexeme.Is_identpart_max() == false {
		t.Fatalf("Lexeme %s should be 'max'", lexeme.Lex_word)
	}

	lexeme = Create_lexeme(LEX_LITERAL_STRING, LEX_LITERAL_STRING_SUB, "MaX")
	if lexeme.Is_identpart_max() == true {
		t.Fatalf("Lexeme %s should not be 'max'", lexeme.Lex_word)
	}

}

func Test_lexer_Attach_batch(t *testing.T) {

	var (
		rsql_err *rsql.Error
	)

	samples := []struct {
		spl_input               string
		spl_expected_message_id rsql.Message_id_t
	}{
		{"+", 0},
		{" +", 0},
		{"    --asdf\n+", 0},
		{"    --asdf\r\n/*as\ndf/*as\ndf*/*/+", 0},
		{"  /*asdf*/  +", 0},
		{"  /*asdf*/    /*asdf\n*/+", 0},
		{"  /*asdf/*/*wqe/r*we\n*/*/as\r\ndf*/+", 0},
		{"  /*asdf*/+", 0},
		{"  /*asdf*/--asdf\n+", 0},
		{"  /*asdf", rsql.ERROR_MISSING_END_COMMENT_MARK},
		{"  /*asdf/*ewrtwer*/adsf", rsql.ERROR_MISSING_END_COMMENT_MARK},
		{"  --asdfadsf\r\n/*asdf/*ewrtwer*/adsf", rsql.ERROR_MISSING_END_COMMENT_MARK},
	}

	// test all samples

	for i := range samples {

		// init Lexer

		sample := samples[i]

		l := New_lexer()

		if l.Current_lexeme != LEXEME_INVALID {
			t.Fatal("Uninitialized lexer must contain LEXEME_INVALID")
		}

		txt := []byte(sample.spl_input)
		rsql_err = l.Attach_batch(txt)

		if l.Current_lexeme != LEXEME_START_OF_BATCH {
			t.Fatal("Uninitialized lexer must contain LEXEME_START_OF_BATCH")
		}

		// if whitespace has been successfully skipped

		if rsql_err == nil && sample.spl_expected_message_id == 0 {
			if l.Rune0 != '+' {
				t.Fatalf("<%s>: l.Rune0 should be '+'", sample.spl_input)
			}
			continue
		}

		// if error occured

		if rsql_err.Message_id != sample.spl_expected_message_id {
			t.Fatalf("<%s>: error should be %v", sample.spl_input, sample.spl_expected_message_id)
		}
		continue
	}
}

func Test_lexer_Eat_next_lexeme(t *testing.T) {

	var (
		lexeme              Lexeme
		lexeme_original     Lexeme
		rsql_err            *rsql.Error
		rsql_err_message_id rsql.Message_id_t
	)

	samples := []struct {
		spl_input                    string
		spl_option_quoted_identifier bool
		spl_expected_subtype         Lex_subtype_t
		spl_expected_word            string
		spl_expected_word_original   string
		spl_expected_message_id      rsql.Message_id_t
	}{
		{"+", false, LEX_OPERATOR_PLUS, "+", "", 0}, // in this test, a sample contains a lexeme, which may optionally be followed by trailing whitespaces, or "end" keyword lexeme (LEXEME_KEYWORD_END)
		{"End", false, LEX_KEYWORD_END, "end", "", 0},
		{"+End", false, LEX_OPERATOR_PLUS, "+", "", 0},
		{"+    ", false, LEX_OPERATOR_PLUS, "+", "", 0},
		{"+   /*", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_MISSING_END_COMMENT_MARK},
		{"+=", false, LEX_OPERATOR_ASSIGN_PLUS, "+=", "", 0},
		{"=", false, LEX_OPERATOR_COMP_EQUAL, "=", "", 0},
		{">=", false, LEX_OPERATOR_COMP_GREATER_EQUAL, ">=", "", 0},
		{"<=", false, LEX_OPERATOR_COMP_LESS_EQUAL, "<=", "", 0},
		{"!>", false, LEX_OPERATOR_COMP_LESS_EQUAL, "<=", "", 0},
		{"!", false, LEX_EXCLAMATIONMARK_SUB, "!", "", 0},
		{"!end", false, LEX_EXCLAMATIONMARK_SUB, "!", "", 0},
		{"LIKE", false, LEX_OPERATOR_LIKE, "like", "", 0},

		{"@a", false, LEX_VARIABLE_SUB, "@a", "", 0},
		{"@世界", false, LEX_VARIABLE_SUB, "@世界", "", 0},
		{"     @a    ", false, LEX_VARIABLE_SUB, "@a", "", 0},
		{"@a End", false, LEX_VARIABLE_SUB, "@a", "", 0},
		{"@", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_VARIABLE_INVALID_START_CHAR},
		{"@1", false, LEX_VARIABLE_SUB, "@1", "", 0},
		{"@_", false, LEX_VARIABLE_SUB, "@_", "", 0},
		{"@@", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_ATFUNC_INVALID_START_CHAR}, // invalid ATFUNC
		{"@$", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_VARIABLE_INVALID_START_CHAR},
		{"@#", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_VARIABLE_INVALID_START_CHAR},

		{"@@", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_ATFUNC_INVALID_START_CHAR},
		{"@@@", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_ATFUNC_INVALID_START_CHAR},
		{"@@$", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_ATFUNC_INVALID_START_CHAR},
		{"@@#", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_ATFUNC_INVALID_START_CHAR},
		{"@@1", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_ATFUNC_INVALID_START_CHAR},
		{"@@a", false, LEX_ATFUNC_SUB, "@@a", "", 0},
		{"@@Language", false, LEX_ATFUNC_SUB, "@@language", "", 0},
		{"@@世界", false, LEX_ATFUNC_SUB, "@@世界", "", 0},

		{"[]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_EMPTY},
		{"[#]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_INVALID_HASHSIGN_ONLY},
		{"[#####]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_INVALID_HASHSIGN_ONLY},
		{"[", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"[ ]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"[\"]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"[']", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"[[]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"\"]\"", true, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"[ a]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"[\x00]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR},
		{"[a\x00]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_CONTINUE_CHAR},
		{"[a']", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_CONTINUE_CHAR},
		{"[a\"]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_CONTINUE_CHAR},
		{"[a[]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_CONTINUE_CHAR},
		{"\"a]\"", true, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_INVALID_CONTINUE_CHAR},
		{"[A]", false, LEX_IDENTPART_SUB, "a", "A", 0},
		{"[#A]", false, LEX_IDENTPART_SUB, "#a", "#A", 0},
		{"[世界  世界  ]", false, LEX_IDENTPART_SUB, "世界  世界  ", "世界  世界  ", 0},
		{"[a", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_CLOSING_DELIMITER_MISSING},
		{"[a     ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_DELIMITED_CLOSING_DELIMITER_MISSING},
		{"\"世界  世界  \"", true, LEX_IDENTPART_SUB, "世界  世界  ", "世界  世界  ", 0},
		{"\"#世界  世界  \"", true, LEX_IDENTPART_SUB, "#世界  世界  ", "#世界  世界  ", 0},

		{"A", false, LEX_IDENTPART_SUB, "a", "A", 0},
		{"世界", false, LEX_IDENTPART_SUB, "世界", "世界", 0},
		{"#A", false, LEX_IDENTPART_SUB, "#a", "#A", 0},
		{"A#@$_", false, LEX_IDENTPART_SUB, "a#@$_", "A#@$_", 0},
		{"##@$_", false, LEX_IDENTPART_SUB, "##@$_", "##@$_", 0},
		{"#####", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_INVALID_HASHSIGN_ONLY},
		{"From", false, LEX_KEYWORD_FROM, "from", "", 0},
		{" From", false, LEX_KEYWORD_FROM, "from", "", 0},
		{"/*asdf\r\n/*asdf*/*/--asfasfd\r\nFrom", false, LEX_KEYWORD_FROM, "from", "", 0},
		{"    /*asdf\r\n/*asdf*/*/--asfasfd\r\nFrom", false, LEX_KEYWORD_FROM, "from", "", 0},
		{"IN", false, LEX_OPERATOR_IN, "in", "", 0},
		{"   IN  --asdf\n", false, LEX_OPERATOR_IN, "in", "", 0},
		{" /*asdfasdf*/  IN  --asdf\n       /*asdf*/ ", false, LEX_OPERATOR_IN, "in", "", 0},

		{"$a", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$世界", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$a   /*sadf asdf*/    --     asdf\n      --asdasdf\n", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$a   /*sadf asdf*/    --     asdf\n      --asdasdf\n       ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$A", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$1", false, LEX_LITERAL_NUMBER_MONEY, "1", "", 0},
		{"$_", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$@", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$$", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$#", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$10", false, LEX_LITERAL_NUMBER_MONEY, "10", "", 0},
		{"$   0", false, LEX_LITERAL_NUMBER_MONEY, "0", "", 0},
		{"$   00000", false, LEX_LITERAL_NUMBER_MONEY, "0", "", 0},
		{"$   10", false, LEX_LITERAL_NUMBER_MONEY, "10", "", 0},
		{"$   10.", false, LEX_LITERAL_NUMBER_MONEY, "10.", "", 0},
		{"$   10.4", false, LEX_LITERAL_NUMBER_MONEY, "10.4", "", 0},
		{"$   .34", false, LEX_LITERAL_NUMBER_MONEY, "0.34", "", 0},
		{"$   0.34", false, LEX_LITERAL_NUMBER_MONEY, "0.34", "", 0},
		{"$   0.", false, LEX_LITERAL_NUMBER_MONEY, "0.", "", 0},
		{"$   .0", false, LEX_LITERAL_NUMBER_MONEY, "0.0", "", 0},
		{"$   .000", false, LEX_LITERAL_NUMBER_MONEY, "0.000", "", 0},
		{"$   -.000", false, LEX_LITERAL_NUMBER_MONEY, "-0.000", "", 0},
		{"$   000000.34", false, LEX_LITERAL_NUMBER_MONEY, "0.34", "", 0},
		{"$  +  10", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$  +a", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$  +", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$   ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID},
		{"$-10", false, LEX_LITERAL_NUMBER_MONEY, "-10", "", 0},
		{" 20", false, LEX_LITERAL_NUMBER_INTEGRAL, "20", "", 0},
		{" 020", false, LEX_LITERAL_NUMBER_INTEGRAL, "20", "", 0},
		{" 00000000020", false, LEX_LITERAL_NUMBER_INTEGRAL, "20", "", 0},
		{" 0", false, LEX_LITERAL_NUMBER_INTEGRAL, "0", "", 0},
		{" 00000", false, LEX_LITERAL_NUMBER_INTEGRAL, "0", "", 0},
		{" 0.", false, LEX_LITERAL_NUMBER_NUMERIC, "0.", "", 0},
		{" .0", false, LEX_LITERAL_NUMBER_NUMERIC, "0.0", "", 0},
		{" 0.7", false, LEX_LITERAL_NUMBER_NUMERIC, "0.7", "", 0},
		{" .7", false, LEX_LITERAL_NUMBER_NUMERIC, "0.7", "", 0},
		{" 00000.7", false, LEX_LITERAL_NUMBER_NUMERIC, "0.7", "", 0},
		{" 0.70", false, LEX_LITERAL_NUMBER_NUMERIC, "0.70", "", 0},
		{" 0.70e6", false, LEX_LITERAL_NUMBER_FLOAT, "0.70e6", "", 0},
		{" 0.70E+0006", false, LEX_LITERAL_NUMBER_FLOAT, "0.70e6", "", 0},
		{" 0000.70E-00060", false, LEX_LITERAL_NUMBER_FLOAT, "0.70e-60", "", 0},
		{" 0.70E-a", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_EXPONENT_PART_MISSING},
		{" 0.70E-", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_EXPONENT_PART_MISSING},
		{" 0.70E", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_EXPONENT_PART_MISSING},
		{" 0.70E- ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_EXPONENT_PART_MISSING},
		{"$40f", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_SUFFIX_FORBIDDEN},
		{"$40d", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_MONEY_SUFFIX_FORBIDDEN},
		{" 0.70E+0006d", false, LEX_LITERAL_NUMBER_NUMERIC, "0.70e6", "", 0},
		{" 0.70f", false, LEX_LITERAL_NUMBER_FLOAT, "0.70", "", 0},
		{" 0.70e-0050", false, LEX_LITERAL_NUMBER_FLOAT, "0.70e-50", "", 0},
		{" 0.70e-0050d", false, LEX_LITERAL_NUMBER_NUMERIC, "0.70e-50", "", 0},
		{" 0005.70f", false, LEX_LITERAL_NUMBER_FLOAT, "5.70", "", 0},
		{" 0005f", false, LEX_LITERAL_NUMBER_FLOAT, "5", "", 0},
		{" 0005d", false, LEX_LITERAL_NUMBER_NUMERIC, "5", "", 0},
		{" 0.70a ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_FOLLOWED_BY_IDENTCHAR},
		{" 0.70fa ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_FOLLOWED_BY_IDENTCHAR},
		{" 0.70f7 ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_FOLLOWED_BY_IDENTCHAR},

		{" 0x ", false, LEX_LITERAL_HEXASTRING_SUB, "0x", "", 0},
		{" 0xr ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_HEXASTRING_FOLLOWED_BY_IDENTCHAR},
		{" 0x0 ", false, LEX_LITERAL_HEXASTRING_SUB, "0x00", "", 0},
		{" 0x2 ", false, LEX_LITERAL_HEXASTRING_SUB, "0x02", "", 0},
		{" 0x02 ", false, LEX_LITERAL_HEXASTRING_SUB, "0x02", "", 0},
		{" 0x02abcd ", false, LEX_LITERAL_HEXASTRING_SUB, "0x02abcd", "", 0},
		{" 0x2abcd ", false, LEX_LITERAL_HEXASTRING_SUB, "0x02abcd", "", 0},
		{" 0x12z ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_HEXASTRING_FOLLOWED_BY_IDENTCHAR},

		{` ''  `, false, LEX_LITERAL_STRING_SUB, ``, "", 0},
		{`''`, false, LEX_LITERAL_STRING_SUB, ``, "", 0},
		{`''''`, false, LEX_LITERAL_STRING_SUB, `'`, "", 0},
		{`'"'`, false, LEX_LITERAL_STRING_SUB, `"`, "", 0},
		{` 'A'  `, false, LEX_LITERAL_STRING_SUB, `A`, "", 0},
		{`N'A'  `, false, LEX_LITERAL_STRING_SUB, `A`, "", 0},
		{` 'A'''  `, false, LEX_LITERAL_STRING_SUB, `A'`, "", 0},
		{` '''A'  `, false, LEX_LITERAL_STRING_SUB, `'A`, "", 0},
		{` 'A''A'  `, false, LEX_LITERAL_STRING_SUB, `A'A`, "", 0},
		{` 'A''A''''B''C'''  `, false, LEX_LITERAL_STRING_SUB, `A'A''B'C'`, "", 0},
		{` 'Hello\Bye'  `, false, LEX_LITERAL_STRING_SUB, `Hello\Bye`, "", 0},
		{`'\'`, false, LEX_LITERAL_STRING_SUB, `\`, "", 0},
		{" 'Hello\nBye'  ", false, LEX_LITERAL_STRING_SUB, "Hello\nBye", "", 0},
		{" 'Hello\\Bye'  ", false, LEX_LITERAL_STRING_SUB, "Hello\\Bye", "", 0},
		{" 'Hello\\\nBye'  ", false, LEX_LITERAL_STRING_SUB, "HelloBye", "", 0},
		{" 'Hello\\\r\nBye'  ", false, LEX_LITERAL_STRING_SUB, "HelloBye", "", 0},
		{" 'Hello\\\n\\\nBye'  ", false, LEX_LITERAL_STRING_SUB, "HelloBye", "", 0},
		{" 'Hello\\nBye'  ", false, LEX_LITERAL_STRING_SUB, "Hello\\nBye", "", 0},
		{" e'Hello\\nBye'  ", false, LEX_LITERAL_STRING_SUB, "Hello\nBye", "", 0},
		{" e'Hello\\u00e9ye'  ", false, LEX_LITERAL_STRING_SUB, "Helloéye", "", 0},
		{" e'\\u00E9'  ", false, LEX_LITERAL_STRING_SUB, "é", "", 0},
		{" e'\\U000000E9'  ", false, LEX_LITERAL_STRING_SUB, "é", "", 0},
		{" e'\\uz'  ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{" e'\\uz'", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{" e'\\uz", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{" e'\\uz0e9'  ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{" e'\\u00Ez'  ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{" e'\\u0zE9'  ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID},
		{" e'Hello\\'Bye\\'Bye'  ", false, LEX_LITERAL_STRING_SUB, "Hello'Bye'Bye", "", 0},
		{" e'Hello\\\"Bye\\\"Bye\\\"'  ", false, LEX_LITERAL_STRING_SUB, "Hello\"Bye\"Bye\"", "", 0},
		{" e'Hello\\Bye\\\r'  ", false, LEX_LITERAL_STRING_SUB, "HelloBye\r", "", 0},
		{" e'Hello\\Bye\\\ra'  ", false, LEX_LITERAL_STRING_SUB, "HelloBye\ra", "", 0},
		{" e'\\u0065\\u0301'  ", false, LEX_LITERAL_STRING_SUB, "\u00E9", "", 0},
		{" e'e\\u0301'  ", false, LEX_LITERAL_STRING_SUB, "é", "", 0},
		{" 'Hello\nBye\\\n'  ", false, LEX_LITERAL_STRING_SUB, "Hello\nBye", "", 0},
		{" 'Hello\nBye\\\na'  ", false, LEX_LITERAL_STRING_SUB, "Hello\nByea", "", 0},
		{" 'Hello\nBye\\\r\na'  ", false, LEX_LITERAL_STRING_SUB, "Hello\nByea", "", 0},

		{" @" + strings.Repeat("X", SPEC_VARIABLE_MAX_LENGTH-1), false, LEX_VARIABLE_SUB, "@" + strings.Repeat("x", SPEC_VARIABLE_MAX_LENGTH-1), "", 0},
		{" @" + strings.Repeat("x", SPEC_VARIABLE_MAX_LENGTH), false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_VARIABLE_INVALID_TOO_LONG},

		{" @@" + strings.Repeat("X", SPEC_ATFUNC_MAX_LENGTH-2), false, LEX_ATFUNC_SUB, "@@" + strings.Repeat("x", SPEC_ATFUNC_MAX_LENGTH-2), "", 0},
		{" @@" + strings.Repeat("x", SPEC_ATFUNC_MAX_LENGTH-1), false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_ATFUNC_INVALID_TOO_LONG},

		{strings.Repeat("X", SPEC_IDENTPART_MAX_LENGTH), false, LEX_IDENTPART_SUB, strings.Repeat("x", SPEC_IDENTPART_MAX_LENGTH), strings.Repeat("X", SPEC_IDENTPART_MAX_LENGTH), 0},
		{strings.Repeat("x", SPEC_IDENTPART_MAX_LENGTH+1), false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_IDENTPART_INVALID_TOO_LONG},

		{strings.Repeat("7", SPEC_NUMBER_MAX_LENGTH), false, LEX_LITERAL_NUMBER_INTEGRAL, strings.Repeat("7", SPEC_NUMBER_MAX_LENGTH), "", 0},
		{strings.Repeat("7", SPEC_NUMBER_MAX_LENGTH+1), false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_NUMBER_INVALID_TOO_LONG},

		{" 0x" + strings.Repeat("a", SPEC_HEXASTRING_MAX_LENGTH-2), false, LEX_LITERAL_HEXASTRING_SUB, "0x" + strings.Repeat("a", SPEC_HEXASTRING_MAX_LENGTH-2), "", 0},
		{" 0x" + strings.Repeat("a", SPEC_HEXASTRING_MAX_LENGTH-1), false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_HEXASTRING_INVALID_TOO_LONG},

		{" '" + strings.Repeat("X", SPEC_STRING_MAX_LENGTH) + "'  ", false, LEX_LITERAL_STRING_SUB, strings.Repeat("X", SPEC_STRING_MAX_LENGTH), "", 0},
		{" '" + strings.Repeat("x", SPEC_STRING_MAX_LENGTH+1) + "'  ", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_LITERAL_STRING_INVALID_TOO_LONG},

		{"\\", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_INVALID_CHARACTER_IN_BATCH},
		{"]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_INVALID_CHARACTER_IN_BATCH},
		{"   ]", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_INVALID_CHARACTER_IN_BATCH},
		{"   \u0001", false, LEX_START_OF_BATCH_SUB, LEXEME_START_OF_BATCH.Lex_word, "", rsql.ERROR_INVALID_CHARACTER_IN_BATCH_CODEPOINT},
	}

	// test all samples

	for i := range samples {

		// init Lexer

		sample := samples[i]

		//println("SAMPLE", sample.spl_input)

		l := New_lexer()
		txt := []byte(sample.spl_input)
		l.Set_option_quoted_identifier(sample.spl_option_quoted_identifier)
		rsql_err = l.Attach_batch(txt)

		if rsql_err != nil {
			t.Fatalf("<%s>: Attach_batch() should not fail", sample.spl_input)
		}

		//=== get Lexeme ===

		rsql_err = l.Eat_next_lexeme()
		lexeme = l.Current_lexeme
		lexeme_original = l.Current_lexeme_original

		//=== check result ===

		rsql_err_message_id = 0
		if rsql_err != nil {
			rsql_err_message_id = rsql_err.Message_id
		}

		if rsql_err_message_id != sample.spl_expected_message_id { // check error
			t.Fatalf("<%s>: Bad error: got <%v>, expected <%v>", sample.spl_input, rsql_err_message_id, sample.spl_expected_message_id)
		}

		// if error

		if rsql_err != nil {
			if lexeme.Lex_subtype != sample.spl_expected_subtype {
				t.Fatalf("<%s>: rsql_err occurred. Lexeme has bad type/subtype: got <%s>, expected %d", sample.spl_input, lexeme.Type_string(), sample.spl_expected_subtype)
			}

			if lexeme.Lex_word != sample.spl_expected_word {
				//fmt.Printf("<%s>: Got word <%v>, expected <%v>", sample.spl_input, []byte(lexeme.Lex_word), []byte(sample.spl_expected_word))
				t.Fatalf("<%s>: rsql_err occurred. Got word <%s>, expected <%s>", sample.spl_input, lexeme.Lex_word, sample.spl_expected_word)
			}

			if lexeme_original.Lex_word != sample.spl_expected_word_original {
				//fmt.Printf("<%s>: Got word <%v>, expected <%v>", sample.spl_input, []byte(lexeme_original.Lex_word), []byte(sample.spl_expected_word_original))
				t.Fatalf("<%s>: rsql_err occurred.  Got word original <%s>, expected <%s>", sample.spl_input, lexeme_original.Lex_word, sample.spl_expected_word_original)
			}

			continue // continue testing with next sample
		}

		// no error, lexeme has been successfully read

		if lexeme.Lex_subtype != sample.spl_expected_subtype {
			t.Fatalf("<%s>: lexeme has bad type/subtype: got <%s>, expected %d", sample.spl_input, lexeme.Type_string(), sample.spl_expected_subtype)
		}

		if lexeme.Lex_word != sample.spl_expected_word {
			//fmt.Printf("<%s>: Got word <%v>, expected <%v>", sample.spl_input, []byte(lexeme.Lex_word), []byte(sample.spl_expected_word))
			t.Fatalf("<%s>: Got word <%s>, expected <%s>", sample.spl_input, lexeme.Lex_word, sample.spl_expected_word)
		}

		if lexeme_original.Lex_word != sample.spl_expected_word_original {
			//fmt.Printf("<%s>: Got word <%v>, expected <%v>", sample.spl_input, []byte(lexeme_original.Lex_word), []byte(sample.spl_expected_word_original))
			t.Fatalf("<%s>: Got word original <%s>, expected <%s>", sample.spl_input, lexeme_original.Lex_word, sample.spl_expected_word_original)
		}

		if lexeme.Lex_type != LEX_IDENTPART && lexeme_original.Lex_type != LEX_INVALID {
			t.Fatalf("<%s>: lexeme_original should be LEX_INVALID, got <%s>", sample.spl_input, lexeme_original.Type_string())
		}

		//=== check that next lexeme is LEXEME_END_OF_BATCH or LEXEME_KEYWORD_END ===

		rsql_err = l.Eat_next_lexeme()
		lexeme = l.Current_lexeme
		lexeme_original = l.Current_lexeme_original

		if lexeme != LEXEME_END_OF_BATCH && lexeme != LEXEME_KEYWORD_END {
			t.Fatalf("<%s>: lexeme should be LEXEME_END_OF_BATCH or LEXEME_KEYWORD_END", sample.spl_input)
		}
	}

}

func Test_read_all_lexemes(t *testing.T) {

	var (
		text            string
		lexeme          Lexeme
		lexeme_original Lexeme
		rsql_err        *rsql.Error
	)

	text = `  Print 'HELLO hélène and O''Hara !'; 
                          Print 12.34 + 5 -- bla bla
                          Print 0x1234 + $12
                          Print SIN(5) + ARTICLES.PRICE
`

	//=== init Lexer ===

	l := New_lexer()

	if rsql_err = l.Attach_batch([]byte(text)); rsql_err != nil {
		fmt.Println(rsql_err)
		t.Fatal("cannot attach batch")
	}

	//=== get all Lexemes ===

	lexeme = l.Current_lexeme // LEX_START_OF_BATCH
	lexeme_original = l.Current_lexeme_original

	fmt.Printf("%5d:%3d  %-30s  %-30s  %-30s\n", l.Current_line.No, l.Current_line.Pos, lexeme.Type_string(), "<"+lexeme.Lex_word+">", "<"+lexeme_original.Lex_word+">") // LEX_START_OF_BATCH

	for {
		if rsql_err = l.Eat_next_lexeme(); rsql_err != nil {
			fmt.Println(rsql_err)
			t.Fatal("cannot attach batch")
		}

		lexeme = l.Current_lexeme
		lexeme_original = l.Current_lexeme_original

		fmt.Printf("%5d:%3d  %-30s  %-30s  %-30s\n", l.Current_line.No, l.Current_line.Pos, lexeme.Type_string(), "<"+lexeme.Lex_word+">", "<"+lexeme_original.Lex_word+">")

		if lexeme.Lex_type == LEX_END_OF_BATCH {
			break
		}
	}
}
