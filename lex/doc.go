// Package lex contains the lexer of RSQL.
//
// The Lexer parses a SQL batch, and returns a Lexeme at each call to l.Eat_next_lexeme().
//
// A lexeme is just a string with some more information. It contains a type and subtype, e.g. LEX_KEYWORD/LEX_KEYWORD_ALTER for keywords, or LEX_IDENTPART/LEX_IDENTPART_SUB for identifiers. See static_tables.go for details.
//
// The returned lexemes are lowercase for LEX_KEYWORD, LEX_OPERATOR, LEX_VARIABLE, LEX_ATFUNC, LEX_IDENTPART.
//
//
// Sample program for using lex package
//
//        package main
//
//        import (
//              "fmt"
//              "os"
//              "io/ioutil"
//
//              "rsql"
//              "rsql/lex"
//        )
//
//        func main() {
//              var (
//                    err error
//                    text []byte
//                    lexeme          lex.Lexeme
//                    rsql_err        *rsql.Error
//              )
//
//              filename := os.Args[1]
//
//              if text, err = ioutil.ReadFile(filename); err != nil {
//                    fmt.Println(err)
//                    os.Exit(1)
//              }
//
//              //=== init Lexer ===
//
//              l := lex.New_lexer()
//
//              if rsql_err = l.Attach_batch(text); rsql_err != nil {
//                    fmt.Println(rsql_err)
//                    os.Exit(1)
//              }
//
//              //=== get all Lexemes ===
//
//              lexeme = l.Current_lexeme
//              fmt.Printf("%5d:%3d  %-30s  %-30s\n", l.Current_line.No, l.Current_line.Pos, lexeme.Type_string(), "<"+lexeme.Lex_word+">")
//
//              for {
//                    if rsql_err = l.Eat_next_lexeme(); rsql_err != nil {
//                          fmt.Println(rsql_err)
//                          os.Exit(1)
//                    }
//
//                    lexeme = l.Current_lexeme
//
//                    fmt.Printf("%5d:%3d  %-30s  %-30s\n", l.Current_line.No, l.Current_line.Pos, lexeme.Type_string(), "<"+lexeme.Lex_word+">")
//
//                    if lexeme.Lex_type == lex.LEX_END_OF_BATCH {
//                          break
//                    }
//              }
//        }
//
package lex
