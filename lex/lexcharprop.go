package lex

import (
	"rsql"

	"unicode"
)

const (
	ASCII_PLAINSPACE                = 1  // \t and space
	ASCII_CR_LF                     = 2  // \r and \n
	ASCII_OPERATOR                  = 4  // ! % & * + - / < = > ? ^| ~
	ASCII_HASH_DOLLAR_AT_UNDERSCORE = 8  // # $ @ _
	ASCII_A_Z                       = 16 // a-z A-Z
	ASCII_0_9                       = 32 // 0-9
	ASCII_HEXA                      = 64 // 0-9 a-f A-F

	// ASCII_STOP_SKIP_WS is used by function lexer_skip_whitespace().
	// It contains all graphical (that is, not space, tab, cr, lf ...) characters, except '/' and '-' because they can be the start of a SQL comment.
	ASCII_STOP_SKIP_WS = 128 // @ # $ ? " ' ( ) [ ] { } * + % & | ~ ! < = > \ ^ , . : ; a-z A-Z 0-9 _  (note: '/' and '-' are not in this list)

	ASCII_XID_START    = (ASCII_HASH_DOLLAR_AT_UNDERSCORE | ASCII_A_Z)             // # $ @ _ a-z A-Z        Used by uc_is_ascii_xid_start(), uc_is_variable_start() etc, which can exclude some more characters of this set.
	ASCII_XID_CONTINUE = (ASCII_HASH_DOLLAR_AT_UNDERSCORE | ASCII_A_Z | ASCII_0_9) // # $ @ _ a-z A-Z 0-9    Used by uc_is_ascii_xid_continue(), uc_is_variable_continue() etc, which can exclude some more characters of this set.
	ASCII_WHITESPACE   = (ASCII_PLAINSPACE | ASCII_CR_LF)
)

// Array of ascii character for quick property lookup.
//
// Array of 128 elements, with various properties for all ascii characters (0...127).
//
// Used to speed up property retrieval for ascii characters, so that unicode function calls can be avoided for them.
//
var G_ASCII_PROPERTIES = [128]byte{

	// Basic Latin - C0 controls

	0,                // U+0000 ( n/a ) NULL                  '\0'
	0,                // U+0001 ( n/a ) START OF HEADING
	0,                // U+0002 ( n/a ) START OF TEXT
	0,                // U+0003 ( n/a ) END OF TEXT
	0,                // U+0004 ( n/a ) END OF TRANSMISSION
	0,                // U+0005 ( n/a ) ENQUIRY
	0,                // U+0006 ( n/a ) ACKNOWLEDGE
	0,                // U+0007 ( n/a ) BELL                  '\a'
	0,                // U+0008 ( n/a ) BACKSPACE             '\b'
	ASCII_PLAINSPACE, // U+0009 (     ) HORIZONTAL TABULATION '\t'
	ASCII_CR_LF,      // U+000A (     ) LINE FEED (LF)        '\n'
	0,                // U+000B (     ) VERTICAL TABULATION   '\v'
	0,                // U+000C (     ) FORM FEED (FF)        '\f'
	ASCII_CR_LF,      // U+000D (     ) CARRIAGE RETURN (CR)  '\r'
	0,                // U+000E ( n/a ) SHIFT OUT
	0,                // U+000F ( n/a ) SHIFT IN
	0,                // U+0010 ( n/a ) DATA LINK ESCAPE
	0,                // U+0011 ( n/a ) DEVICE CONTROL ONE
	0,                // U+0012 ( n/a ) DEVICE CONTROL TWO
	0,                // U+0013 ( n/a ) DEVICE CONTROL THREE
	0,                // U+0014 ( n/a ) DEVICE CONTROL FOUR
	0,                // U+0015 ( n/a ) NEGATIVE ACKNOWLEDGE
	0,                // U+0016 ( n/a ) SYNCHRONOUS IDLE
	0,                // U+0017 ( n/a ) END OF TRANSMISSION BLOCK
	0,                // U+0018 ( n/a ) CANCEL
	0,                // U+0019 ( n/a ) END OF MEDIUM
	0,                // U+001A ( n/a ) SUBSTITUTE
	0,                // U+001B ( n/a ) ESCAPE
	0,                // U+001C ( n/a ) INFORMATION SEPARATOR FOUR
	0,                // U+001D ( n/a ) INFORMATION SEPARATOR THREE
	0,                // U+001E ( n/a ) INFORMATION SEPARATOR TWO
	0,                // U+001F ( n/a ) INFORMATION SEPARATOR ONE

	// Basic Latin - ASCII punctuation and symbols

	ASCII_PLAINSPACE,                                     // U+0020 (   ) SPACE
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+0021 ( ! ) EXCLAMATION MARK
	ASCII_STOP_SKIP_WS,                                   // U+0022 ( " ) QUOTATION MARK
	ASCII_STOP_SKIP_WS | ASCII_HASH_DOLLAR_AT_UNDERSCORE, // U+0023 ( # ) NUMBER SIGN
	ASCII_STOP_SKIP_WS | ASCII_HASH_DOLLAR_AT_UNDERSCORE, // U+0024 ( $ ) DOLLAR SIGN
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+0025 ( % ) PERCENT SIGN
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+0026 ( & ) AMPERSAND
	ASCII_STOP_SKIP_WS,                                   // U+0027 ( ' ) APOSTROPHE
	ASCII_STOP_SKIP_WS,                                   // U+0028 ( ( ) LEFT PARENTHESIS
	ASCII_STOP_SKIP_WS,                                   // U+0029 ( ) ) RIGHT PARENTHESIS
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+002A ( * ) ASTERISK
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+002B ( + ) PLUS SIGN
	ASCII_STOP_SKIP_WS,                                   // U+002C ( , ) COMMA
	ASCII_OPERATOR,                                       // U+002D ( - ) HYPHEN-MINUS
	ASCII_STOP_SKIP_WS,                                   // U+002E ( . ) FULL STOP
	ASCII_OPERATOR,                                       // U+002F ( / ) SOLIDUS

	// Basic Latin - ASCII digits

	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0030 ( 0 ) DIGIT ZERO
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0031 ( 1 ) DIGIT ONE
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0032 ( 2 ) DIGIT TWO
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0033 ( 3 ) DIGIT THREE
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0034 ( 4 ) DIGIT FOUR
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0035 ( 5 ) DIGIT FIVE
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0036 ( 6 ) DIGIT SIX
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0037 ( 7 ) DIGIT SEVEN
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0038 ( 8 ) DIGIT EIGHT
	ASCII_STOP_SKIP_WS | ASCII_0_9 | ASCII_HEXA, // U+0039 ( 9 ) DIGIT NINE

	// Basic Latin - ASCII punctuation and symbols

	ASCII_STOP_SKIP_WS,                                   // U+003A ( : ) COLON
	ASCII_STOP_SKIP_WS,                                   // U+003B ( ; ) SEMICOLON
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+003C ( < ) LESS-THAN SIGN
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+003D ( = ) EQUALS SIGN
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+003E ( > ) GREATER-THAN SIGN
	ASCII_STOP_SKIP_WS,                                   // U+003F ( ? ) QUESTION MARK
	ASCII_STOP_SKIP_WS | ASCII_HASH_DOLLAR_AT_UNDERSCORE, // U+0040 ( @ ) COMMERCIAL AT

	// Basic Latin - Uppercase Latin alphabet

	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0041 ( A ) LATIN CAPITAL LETTER A
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0042 ( B ) LATIN CAPITAL LETTER B
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0043 ( C ) LATIN CAPITAL LETTER C
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0044 ( D ) LATIN CAPITAL LETTER D
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0045 ( E ) LATIN CAPITAL LETTER E
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0046 ( F ) LATIN CAPITAL LETTER F
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0047 ( G ) LATIN CAPITAL LETTER G
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0048 ( H ) LATIN CAPITAL LETTER H
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0049 ( I ) LATIN CAPITAL LETTER I
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+004A ( J ) LATIN CAPITAL LETTER J
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+004B ( K ) LATIN CAPITAL LETTER K
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+004C ( L ) LATIN CAPITAL LETTER L
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+004D ( M ) LATIN CAPITAL LETTER M
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+004E ( N ) LATIN CAPITAL LETTER N
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+004F ( O ) LATIN CAPITAL LETTER O
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0050 ( P ) LATIN CAPITAL LETTER P
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0051 ( Q ) LATIN CAPITAL LETTER Q
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0052 ( R ) LATIN CAPITAL LETTER R
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0053 ( S ) LATIN CAPITAL LETTER S
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0054 ( T ) LATIN CAPITAL LETTER T
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0055 ( U ) LATIN CAPITAL LETTER U
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0056 ( V ) LATIN CAPITAL LETTER V
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0057 ( W ) LATIN CAPITAL LETTER W
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0058 ( X ) LATIN CAPITAL LETTER X
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0059 ( Y ) LATIN CAPITAL LETTER Y
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+005A ( Z ) LATIN CAPITAL LETTER Z

	// Basic Latin - ASCII punctuation and symbols

	ASCII_STOP_SKIP_WS,                                   // U+005B ( [ ) LEFT SQUARE BRACKET
	ASCII_STOP_SKIP_WS,                                   // U+005C ( \ ) REVERSE SOLIDUS
	ASCII_STOP_SKIP_WS,                                   // U+005D ( ] ) RIGHT SQUARE BRACKET
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR,                  // U+005E ( ^ ) CIRCUMFLEX ACCENT
	ASCII_STOP_SKIP_WS | ASCII_HASH_DOLLAR_AT_UNDERSCORE, // U+005F ( _ ) LOW LINE
	0, // U+0060 ( ` ) GRAVE ACCENT

	// Basic Latin - Lowercase Latin alphabet

	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0061 ( a ) LATIN SMALL LETTER A
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0062 ( b ) LATIN SMALL LETTER B
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0063 ( c ) LATIN SMALL LETTER C
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0064 ( d ) LATIN SMALL LETTER D
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0065 ( e ) LATIN SMALL LETTER E
	ASCII_STOP_SKIP_WS | ASCII_A_Z | ASCII_HEXA, // U+0066 ( f ) LATIN SMALL LETTER F
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0067 ( g ) LATIN SMALL LETTER G
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0068 ( h ) LATIN SMALL LETTER H
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0069 ( i ) LATIN SMALL LETTER I
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+006A ( j ) LATIN SMALL LETTER J
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+006B ( k ) LATIN SMALL LETTER K
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+006C ( l ) LATIN SMALL LETTER L
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+006D ( m ) LATIN SMALL LETTER M
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+006E ( n ) LATIN SMALL LETTER N
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+006F ( o ) LATIN SMALL LETTER O
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0070 ( p ) LATIN SMALL LETTER P
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0071 ( q ) LATIN SMALL LETTER Q
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0072 ( r ) LATIN SMALL LETTER R
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0073 ( s ) LATIN SMALL LETTER S
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0074 ( t ) LATIN SMALL LETTER T
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0075 ( u ) LATIN SMALL LETTER U
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0076 ( v ) LATIN SMALL LETTER V
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0077 ( w ) LATIN SMALL LETTER W
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0078 ( x ) LATIN SMALL LETTER X
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+0079 ( y ) LATIN SMALL LETTER Y
	ASCII_STOP_SKIP_WS | ASCII_A_Z,              // U+007A ( z ) LATIN SMALL LETTER Z

	// Basic Latin - ASCII punctuation and symbols

	ASCII_STOP_SKIP_WS,                  // U+007B ( { ) LEFT CURLY BRACKET
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR, // U+007C ( | ) VERTICAL LINE
	ASCII_STOP_SKIP_WS,                  // U+007D ( } ) RIGHT CURLY BRACKET
	ASCII_STOP_SKIP_WS | ASCII_OPERATOR, // U+007E ( ~ ) TILDE

	// Basic Latin - Control character

	0, // U+007F (  n/a ) DELETE
}

// uc_is_hexa tests if uc is hexa.
func uc_is_hexa(uc rune) bool {

	return uc < 128 && G_ASCII_PROPERTIES[uc]&ASCII_HEXA != 0
}

// uc_is_a_z tests if uc is a-z A-Z.
func uc_is_a_z(uc rune) bool {

	//	return uc < 128 && G_ASCII_PROPERTIES[uc]&ASCII_A_Z != 0   // it works, but I think the line below is faster.

	return uc < 128 && ((uc >= 'a' && uc <= 'z') || (uc >= 'A' && uc <= 'Z'))
}

// uc_is_0_9 tests if uc is 0_9.
func uc_is_0_9(uc rune) bool {

	return uc >= '0' && uc <= '9'
}

// uc_is_operator tests if uc is operator.
func uc_is_operator(uc rune) bool {

	return uc < 128 && G_ASCII_PROPERTIES[uc]&ASCII_OPERATOR != 0
}

// uc_is_ascii_plainspace tests if uc is ascii plain space.
func uc_is_ascii_plainspace(uc rune) bool {

	return uc < 128 && G_ASCII_PROPERTIES[uc]&ASCII_PLAINSPACE != 0
}

// uc_is_variable_start tests if uc is start character (character after the prefix '@') of a variable.
//
//       Allowed characters:
//          - Letters 'a'...'z' and 'A'...'Z'
//          - Digits '0'...'9'. E.g. variable "@1" is allowed.
//          - Character '_'
//          - Unicode code points that have property XID_START are allowed (for the moment, there is no XID_START in unicode package, so we approximate it).
//
//       NOT ALLOWED: '@', '$', '#'
//
func uc_is_variable_start(uc rune) bool {

	if uc == '@' || uc == '$' || uc == '#' { // '@', '$' and '#' cannot be start of variable name
		return false
	}

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&(ASCII_XID_START|ASCII_0_9) != 0
	}

	return unicode.IsLetter(uc) || unicode.IsDigit(uc) // XID_Start property would be better, but it doesn't exist yet
}

// uc_is_variable_continue tests if uc is continuing character of a variable.
//
//       Allowed characters:
//          - Letters 'a'...'z' and 'A'...'Z'
//          - Digits '0'...'9'
//          - Character '_'
//          - Unicode code points that have property XID_CONTINUE are allowed (for the moment, there is no XID_CONTINUE in unicode package, so we approximate it).
//          - '@', '$', '#'
//
func uc_is_variable_continue(uc rune) bool {

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&ASCII_XID_CONTINUE != 0
	}

	return unicode.IsLetter(uc) || unicode.IsDigit(uc) // XID_Continue property would be better, but it doesn't exist yet
}

// uc_is_atfunc_start tests if uc is start character (character after the prefix "@@") of a @@function name.
//
//       Allowed characters:
//          - Letters 'a'...'z' and 'A'...'Z'
//          - Character '_'
//          - Unicode code points that have property XID_START are allowed (for the moment, there is no XID_START in unicode package, so we approximate it).
//
//       NOT ALLOWED:
//          - '@', '$', '#'
//          - Digits '0'...'9'
//
func uc_is_atfunc_start(uc rune) bool {

	if uc == '@' || uc == '$' || uc == '#' { // '@' or '$' cannot be start of variable name
		return false
	}

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&ASCII_XID_START != 0
	}

	return unicode.IsLetter(uc) // XID_Start property would be better, but it doesn't exist yet
}

// uc_is_atfunc_continue tests if uc is continuing character of a @@function name.
//
//       Allowed characters:
//          - Letters 'a'...'z' and 'A'...'Z'
//          - Digits '0'...'9'
//          - Character '_'
//          - Unicode code points that have property XID_CONTINUE are allowed (for the moment, there is no XID_CONTINUE in unicode package, so we approximate it).
//          - '@', '$', '#'
//
func uc_is_atfunc_continue(uc rune) bool {

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&ASCII_XID_CONTINUE != 0
	}

	return unicode.IsLetter(uc) || unicode.IsDigit(uc) // XID_Continue property would be better, but it doesn't exist yet
}

//  uc_is_identpart_start tests if uc is start character of a non-delimited identifier.
//
//       Allowed characters:
//          - Letters 'a'...'z' and 'A'...'Z'
//          - Character '_'
//          - Character '#' is allowed, meaning the identifier is a temporary object.
//          - Unicode code points that have property XID_START are allowed (for the moment, there is no XID_START in unicode package, so we approximate it).
//
//       NOT ALLOWED:
//          - '@', '$'
//          - Digits '0'...'9'
//
func uc_is_identpart_start(uc rune) bool {

	if uc == '@' || uc == '$' { // '@' or '$' cannot be start of an ident
		return false
	}

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&ASCII_XID_START != 0
	}

	return unicode.IsLetter(uc) // XID_Start property would be better, but it doesn't exist yet
}

// uc_is_identpart_continue tests if uc is continuing character of an identpart.
//
//       Allowed characters:
//          - Letters 'a'...'z' and 'A'...'Z'
//          - Digits '0'...'9'
//          - Character '_'
//          - Unicode code points that have property XID_CONTINUE are allowed (for the moment, there is no XID_CONTINUE in unicode package, so we approximate it).
//          - '@', '$', '#'
//
func uc_is_identpart_continue(uc rune) bool {

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&ASCII_XID_CONTINUE != 0
	}

	return unicode.IsLetter(uc) || unicode.IsDigit(uc) // XID_Continue property would be better, but it doesn't exist yet
}

// uc_is_identpart_start_extended tests if uc is start character of a delimited identifier.
//
// Operators and punctuations are allowed as start character, but not space.
// '@' is not allowed because it is reserved for table variables.
// '$' is not allowed because it is for system generated tables, like "$grouptable$".
//
//       NOT ALLOWED:
//          - '   "    [    ]   .   @     $
//
func uc_is_identpart_start_extended(uc rune) bool {

	if uc == '"' || uc == '\'' || uc == '[' || uc == ']' || uc == '.' || uc == '@' || uc == '$' {
		return false
	}

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&(ASCII_STOP_SKIP_WS|ASCII_OPERATOR) != 0
	}

	return unicode.IsLetter(uc) || unicode.IsDigit(uc)
}

// uc_is_identpart_continue_extended tests if uc is continuing character of a delimited identifier.
//
// Spaces, operators and punctuations are allowed.
//
//       NOT ALLOWED:
//          - '   "    [    ]
//
func uc_is_identpart_continue_extended(uc rune) bool {

	if uc == '"' || uc == '\'' || uc == '[' || uc == ']' || uc == '.' {
		return false
	}

	if uc < 128 {
		return G_ASCII_PROPERTIES[uc]&(ASCII_PLAINSPACE|ASCII_STOP_SKIP_WS|ASCII_OPERATOR) != 0
	}

	return unicode.IsLetter(uc) || unicode.IsDigit(uc)
}

// uc_ascii_to_lowercase converts a character to lowercase if it is in the ascii range.
//
// 'A'...'Z' is converted to 'a'...'z'.
// Else, the character is unchanged.
//
func uc_ascii_to_lowercase(uc rune) rune {

	if uc >= 'A' && uc <= 'Z' {
		return (uc - 'A') + 'a'
	}

	return uc
}

// uc_hexa_digit_value converts a hexa digit into a value.
//
func Uc_hexa_digit_value(uc rune) (uint8, *rsql.Error) {
	var val uint8

	switch {
	case uc >= '0' && uc <= '9':
		val = uint8(uc - '0')

	case uc >= 'A' && uc <= 'F':
		val = uint8((uc - 'A') + 10)

	case uc >= 'a' && uc <= 'f':
		val = uint8((uc - 'a') + 10)

	default: // bad hexa digit
		return 0, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_BAD_HEXA_DIGIT, rsql.ERROR_BATCH_ABORT, uc)
	}

	return val, nil
}
