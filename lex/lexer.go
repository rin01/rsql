package lex

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"

	"golang.org/x/text/unicode/norm"

	"rsql"
)

func assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

type (
	Lex_type_t    uint32 // LEX_KEYWORD, LEX_OPERATOR, LEX_IDENTPART, etc
	Lex_subtype_t uint32 // LEX_KEYWORD_ADD, LEX_OPERATOR_MINUS, LEX_IDENTPART_SUB, etc

	Txtdim_t int64 // type for offsets in batch text. It allows negative number, because we use -1 e.g. in eat_literal_number().
)

// Lexemes are used by the lexer. Lexemes contain keywords, identifiers, operators, etc.
// The successive lexemes are read by the lexer, and returned by each call of l.Eat_next_lexeme().
//
// A Lexeme is a string with some type information.
// It contains a Lex_type and Lex_subtype indicating if it is a keyword, identifier, etc.
// If it is LEX_KEYWORD, LEX_OPERATOR, LEX_VARIABLE, LEX_ATFUNC, LEX_IDENTPART, the Lex_word string is lowercase.
//
type Lexeme struct {
	Lex_type    Lex_type_t    // LEX_KEYWORD, LEX_OPERATOR, LEX_IDENTPART, etc
	Lex_subtype Lex_subtype_t // LEX_KEYWORD_ADD, LEX_OPERATOR_MINUS, LEX_IDENTPART_SUB, etc
	Lex_word    string        // string containing the literal word. It is in lowercase for LEX_KEYWORD, LEX_OPERATOR, LEX_VARIABLE, LEX_ATFUNC, LEX_IDENTPART.
}

func (lexeme *Lexeme) Type_string() string {
	var (
		lex_type        Lex_type_t
		lex_subtype     Lex_subtype_t
		lex_type_string string
	)

	lex_type = lexeme.Lex_type
	lex_subtype = lexeme.Lex_subtype

	switch lex_type {
	case LEX_INVALID:
		lex_type_string = "LEX_INVALID"

	case LEX_LITERAL_NUMBER:
		switch lex_subtype {
		case LEX_LITERAL_NUMBER_INTEGRAL:
			lex_type_string = "LEX_LITERAL_NUMBER/_INTEGRAL"
		case LEX_LITERAL_NUMBER_MONEY:
			lex_type_string = "LEX_LITERAL_NUMBER/_MONEY"
		case LEX_LITERAL_NUMBER_NUMERIC:
			lex_type_string = "LEX_LITERAL_NUMBER/_NUMERIC"
		case LEX_LITERAL_NUMBER_FLOAT:
			lex_type_string = "LEX_LITERAL_NUMBER/_FLOAT"
		default:
			panic("unknown lex_subtype for LEX_LITERAL_NUMBER")
		}
	case LEX_LITERAL_HEXASTRING:
		lex_type_string = "LEX_LITERAL_HEXASTRING"
	case LEX_LITERAL_STRING:
		lex_type_string = "LEX_LITERAL_STRING"
	case LEX_VARIABLE:
		lex_type_string = "LEX_VARIABLE"
	case LEX_ATFUNC:
		lex_type_string = "LEX_ATFUNC"
	case LEX_IDENTPART:
		lex_type_string = "LEX_IDENTPART"
	case LEX_KEYWORD:
		lex_type_string = "LEX_KEYWORD"
	case LEX_OPERATOR:
		lex_type_string = "LEX_OPERATOR"
	case LEX_LPAREN:
		lex_type_string = "LEX_LPAREN"
	case LEX_RPAREN:
		lex_type_string = "LEX_RPAREN"
	case LEX_LCURLY:
		lex_type_string = "LEX_LCURLY"
	case LEX_RCURLY:
		lex_type_string = "LEX_RCURLY"
	case LEX_COMMA:
		lex_type_string = "LEX_COMMA"
	case LEX_DOT:
		lex_type_string = "LEX_DOT"
	case LEX_SEMICOLON:
		lex_type_string = "LEX_SEMICOLON"
	case LEX_COLON:
		lex_type_string = "LEX_COLON"
	case LEX_QUESTIONMARK:
		lex_type_string = "LEX_QUESTIONMARK"
	case LEX_EXCLAMATIONMARK:
		lex_type_string = "LEX_EXCLAMATIONMARK"
	case LEX_START_OF_BATCH:
		lex_type_string = "LEX_START_OF_BATCH"
	case LEX_END_OF_BATCH:
		lex_type_string = "LEX_END_OF_BATCH"
	case LEX_DISPLAYWORD:
		lex_type_string = "LEX_DISPLAYWORD"

	default:
		panic("lex type unknown")
	}

	return lex_type_string
}

func (lexeme *Lexeme) To_uint16() (uint16, *rsql.Error) {

	res, err := strconv.ParseInt(lexeme.Lex_word, 10, 64)

	if err != nil || res < 0 || res > math.MaxUint16 {
		return 0, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_LEXEME_TO_UINT16_FAILED, rsql.ERROR_BATCH_ABORT, lexeme.Lex_word)
	}

	return uint16(res), nil
}

func (lexeme *Lexeme) Is_identpart_max() bool {

	if lexeme.Lex_type != LEX_IDENTPART || len(lexeme.Lex_word) != 3 {
		return false
	}

	if lexeme.Lex_word == "max" {
		return true
	}

	return false
}

func (lexeme *Lexeme) Is_auxword(auxword Auxword_t) bool {

	if lexeme.Lex_type != LEX_IDENTPART {
		return false
	}

	if lexeme.Lex_word != string(auxword) {
		return false
	}

	return true
}

func (lexeme *Lexeme) String() string {

	if lexeme == nil {
		return ""
	}

	if lexeme.Lex_type == LEX_LITERAL_STRING {
		return "'" + lexeme.Lex_word + "'"
	}

	return lexeme.Lex_word
}

// Create_lexeme returns a lexeme.
// Argument 'word' is the string in original case.
// For LEX_OPERATOR, LEX_VARIABLE, LEX_ATFUNC, LEX_IDENTPART, the string stored in lexeme will be lowercased.
// You cannot create a LEX_KEYWORD lexeme, because they already exist in G_KEYWORDS_OPERATORS map.
//
func Create_lexeme(lex_type Lex_type_t, lex_subtype Lex_subtype_t, word string) Lexeme {

	assert(lex_type != LEX_KEYWORD)

	// in particular, for LEX_LITERAL_STRING, LEX_LITERAL_HEXASTRING, LEX_LITERAL_NUMBER, don't lowercase

	if lex_type&(LEX_LITERAL_STRING|LEX_LITERAL_HEXASTRING|LEX_LITERAL_NUMBER) != 0 {
		return Lexeme{Lex_type: lex_type, Lex_subtype: lex_subtype, Lex_word: word}
	}

	return Lexeme{Lex_type: lex_type, Lex_subtype: lex_subtype, Lex_word: strings.ToLower(word)} // lowercase for LEX_IDENTPART, etc
}

// Create_lexeme_original is same as Create_lexeme, but don't lowercase.
// It can only be user for LEX_IDENTPART. Else panics.
//
func Create_lexeme_original(lex_type Lex_type_t, lex_subtype Lex_subtype_t, word string) Lexeme {

	assert(lex_type == LEX_IDENTPART)

	return Lexeme{Lex_type: lex_type, Lex_subtype: lex_subtype, Lex_word: word}
}

// Coord_t contains current line number and position, for error display.
//
type Coord_t struct {
	No  uint32
	Pos Txtdim_t
}

// Lexer takes a sql batch as input, and calling Eat_next_lexeme() returns the successive Lexemes in l.Current_lexeme.
//
// For the user, the important fields are :
//   - Current_line                  line no and position of current lexeme.
//   - Current_lexeme                lexeme that has just been read, in lowercase except for LEX_LITERAL_STRING, LEX_LITERAL_HEXASTRING, LEX_LITERAL_NUMBER.
//   - Rune0                         next character to parse. A call to Eat_next_lexeme() will begin to parse starting at this position. Always valid, contains 0 if Current_lexeme==LEXEME_END_OR_BATCH.
//   - Rune1                         lookahead character. Always valid, contains 0 if Current_lexeme==LEXEME_END_OR_BATCH.
//   - Rune2                         lookahead character. Always valid, contains 0 if Current_lexeme==LEXEME_END_OR_BATCH.
//
// Rune0, Rune1 and Rune2 acts like a sliding window, which moves exactly one rune forwards at each call to next_rune().
//
// IF YOU ADD A NEW FIELD TO Lexer STRUCT, YOU MUST MODIFY Memorize_to() and Restore_from() ACCORDINGLY.
//
type Lexer struct {
	option_quoted_identifier bool // if false, literal strings may be enclosed by double-quotes (but not identifiers). If true, identifiers may be enclosed by double-quotes (but not literal strings).

	batch_text   []byte   // SQL batch, UTF-8, normalized in NFC (NOTE: there is no requirement to terminate the text by 0, like in C language which needs a 0 to terminate strings)
	batch_length Txtdim_t // length of batch

	Current_line_old        Coord_t // last value of Current_line.
	Current_line            Coord_t // Will be read by parser. Line no (starting from 1) and position (starting from 1) of the current lexeme. Eat_next_lexeme() updates this value.
	Current_lexeme          Lexeme  // Will be read by parser. Current lexeme. Eat_next_lexeme() updates this value. Contains LEXEME_END_OF_BATCH when EOS.
	Current_lexeme_original Lexeme  // for LEX_IDENTPART, contains lexeme in original case. Else, LEXEME_INVALID.
	Info_line               Coord_t // line no and pos used for error localization. This information is updated by Eat_next_lexeme() or Info_line_update().

	line_no      uint32   // line no of Rune0. skip_whitespace() and eat_literal_string() update this value.
	line_offset  Txtdim_t // pointer to start of line of Rune0. skip_whitespace() and eat_literal_string() update this value. NOTE: May be equal to l.batch_length if \n is last character in batch.
	EOS          bool     // end-of-batch flag. If true, Rune0, Rune1, Rune2 == 0.
	rune0_offset Txtdim_t // position of Rune0. After Eat_next_lexeme(), points to the first non whitespace rune after the current lexeme. Equal to l.batch_length when EOS.
	Rune0        rune     // Next rune to parse. Rune0, Rune1 and Rune2 make up a sliding window.

	rune1_offset Txtdim_t // position of Rune1. Equal to l.batch_length when EOS.
	Rune1        rune     // lookahead rune. 0 when EOS.
	rune2_offset Txtdim_t // position of Rune2. Equal to l.batch_length when EOS.
	Rune2        rune     // lookahead rune. next_rune() will read a new rune at rune3_offset and put it here. 0 when EOS.

	rune3_offset Txtdim_t // next_rune() will read a new rune at this offset and advance rune3_offset.
}

// Lexer_bak is used to save the state of the Lexer (current lexing position in batch).
// USed by l.Memorize_to() and l.Restore_from().
//
type Lexer_bak struct {
	Current_line_old        Coord_t
	Current_line            Coord_t
	Current_lexeme          Lexeme
	Current_lexeme_original Lexeme
	Info_line               Coord_t

	line_no      uint32
	line_offset  Txtdim_t
	EOS          bool
	rune0_offset Txtdim_t
	Rune0        rune

	rune1_offset Txtdim_t
	Rune1        rune
	rune2_offset Txtdim_t
	Rune2        rune

	rune3_offset Txtdim_t
}

func New_lexer() *Lexer {

	l := &Lexer{}

	return l
}

func (l *Lexer) Memorize_to(lbak *Lexer_bak) {
	lbak.Current_line_old = l.Current_line_old
	lbak.Current_line = l.Current_line
	lbak.Current_lexeme = l.Current_lexeme
	lbak.Current_lexeme_original = l.Current_lexeme_original
	lbak.Info_line = l.Info_line

	lbak.line_no = l.line_no
	lbak.line_offset = l.line_offset
	lbak.EOS = l.EOS
	lbak.rune0_offset = l.rune0_offset
	lbak.Rune0 = l.Rune0

	lbak.rune1_offset = l.rune1_offset
	lbak.Rune1 = l.Rune1
	lbak.rune2_offset = l.rune2_offset
	lbak.Rune2 = l.Rune2

	lbak.rune3_offset = l.rune3_offset
}

func (l *Lexer) Restore_from(lbak *Lexer_bak) {
	l.Current_line_old = lbak.Current_line_old
	l.Current_line = lbak.Current_line
	l.Current_lexeme = lbak.Current_lexeme
	l.Current_lexeme_original = lbak.Current_lexeme_original
	l.Info_line = lbak.Info_line

	l.line_no = lbak.line_no
	l.line_offset = lbak.line_offset
	l.EOS = lbak.EOS
	l.rune0_offset = lbak.rune0_offset
	l.Rune0 = lbak.Rune0

	l.rune1_offset = lbak.rune1_offset
	l.Rune1 = lbak.Rune1
	l.rune2_offset = lbak.rune2_offset
	l.Rune2 = lbak.Rune2

	l.rune3_offset = lbak.rune3_offset
}

// next_rune reads one more rune.
// If invalid utf8 sequence is encountered, Rune0 will contain "Unicode replacement character" '\uFFFD'.
// If no more rune is available, Rune0 == 0 and EOS == true.
//
// Rune0 contains the current rune. Rune1 and Rune2 are lookahead runes.
//
func (l *Lexer) next_rune() {

	// shift <R0 R1 R2> sliding window

	l.rune0_offset, l.Rune0 = l.rune1_offset, l.Rune1
	l.rune1_offset, l.Rune1 = l.rune2_offset, l.Rune2
	l.rune2_offset = l.rune3_offset

	// if end-of-batch is reached, put code point 0 in Rune2 and return

	if l.rune3_offset >= l.batch_length {
		l.rune2_offset, l.Rune2 = l.batch_length, '\000'
		l.rune3_offset = l.batch_length

		if l.rune0_offset == l.batch_length { // set flag only when Rune0 is beyond last rune in string
			l.EOS = true
		}
		return
	}

	// read one more rune into Rune2 and update l.rune3_offset

	uc := rune(l.batch_text[l.rune3_offset]) // read one byte

	if uc < 128 { // if ascii
		l.Rune2 = uc
		l.rune3_offset += 1
		return
	}

	// read multibyte utf8 rune

	uc, width := utf8.DecodeRune(l.batch_text[l.rune3_offset:]) // if error, "Unicode replacement character" '\uFFFD'

	l.Rune2 = uc
	l.rune3_offset += Txtdim_t(width)
}

// Attach_batch normalizes to NFC and attaches a batch to the lexer, and initializes the lexer state.
// The batch text is then ready to be parsed.
//
// It returns an error if the batch begins with a star comment /* that has no ending mark */
//
func (l *Lexer) Attach_batch(batch_text []byte) *rsql.Error {
	var (
		rsql_err              *rsql.Error
		batch_text_normalized []byte
	)

	// normalize batch to NFC

	batch_text_normalized = batch_text

	if norm.NFC.IsNormal(batch_text) == false {
		batch_text_normalized = norm.NFC.Append(nil, batch_text...)
	}

	// init

	l.batch_text = batch_text_normalized
	l.batch_length = Txtdim_t(len(l.batch_text))

	l.Current_line_old.No = 0 // l.Eat_next_lexeme() will update these fields
	l.Current_line_old.Pos = 0
	l.Current_line.No = 0
	l.Current_line.Pos = 0
	l.Current_lexeme = LEXEME_START_OF_BATCH
	l.Current_lexeme_original = LEXEME_INVALID
	l.Info_line = l.Current_line

	l.line_no = 1 // numbering begins by 1
	l.line_offset = 0

	l.next_rune() // fill Rune2
	l.next_rune() // fill Rune1
	l.next_rune() // fill Rune0

	// skip whitespace at start of batch so that the lexer is ready to read the first lexeme.

	if rsql_err = l.skip_whitespace(); rsql_err != nil { // may update l.line_no and l.line_offset, and lexer position info. l.Rune0 contains first non-whitespace character to parse.
		return rsql_err
	}

	return nil
}

// Set_option_quoted_identifier sets the corresponding lexer option.
// You should call this function before your first call to l.Eat_next_lexeme().
// You can call it before of after l.Attach_batch().
//
func (l *Lexer) Set_option_quoted_identifier(opt_quot bool) {
	l.option_quoted_identifier = opt_quot
}

// skip_whitespace skips all whitespaces and comments.
// Comments beginning with -- are skipped up to \n or end-of-batch.
//
// Star comments are also skipped and can be nested.
//
// This function updates
//   - l.Rune0, l.Rune1, l.Rune2
//   - l.rune0_offset, l.rune1_offset, l.rune2_offset, l.rune3_offset, EOS
//   - l.line_no
//   - l.line_offset
//
// When this function returns, l.rune0_offset will point to a non-skippable rune, start of the next lexeme to read.
//
// If l.rune0_offset already points to a non-skippable rune or to end-of-batch when this function is called, the function just returns.
//
// An error is returned if end-of-batch is reached when inside a star comment.
//
func (l *Lexer) skip_whitespace() *rsql.Error {
	var (
		line_no                    uint32
		line_offset                Txtdim_t
		comment_star_nesting_level uint32
		rune_ascii_property        uint8
	)

	/* initialization */

	line_no = l.line_no
	line_offset = l.line_offset

	comment_star_nesting_level = 0

	/* skip all the whitespaces and comments */

	// In the loop below, the current position advances as long as it reads whitespaces or comment content.
	// The only ways to quit this loop are :
	//                    - when an ascii graphical character (ASCII_STOP_SKIP_WS) is read, or when '/' or '-' that is not start of comment is read.
	//                    - when a rune with 'White_Space' property not set is read.
	//                    - when end-of-batch is reached.
MAIN_LOOP:
	for l.EOS == false { // main skip loop

		/*** fast path for ascii characters and comments ***/

		if l.Rune0 < 128 {

			rune_ascii_property = G_ASCII_PROPERTIES[l.Rune0]

			switch {
			/* tab or space (most common case to continue skipping) */

			case rune_ascii_property&ASCII_PLAINSPACE != 0:
				l.next_rune()
				continue // continue main skip loop

			/* graphical character and not start of comment quick break (most common case to stop skipping) */

			case (rune_ascii_property&ASCII_STOP_SKIP_WS != 0) || (l.Rune0 == '/' && l.Rune1 != '*') || (l.Rune0 == '-' && l.Rune1 != '-'):
				break MAIN_LOOP // break main skip loop

			/* carriage return or linefeed skipping */

			case rune_ascii_property&ASCII_CR_LF != 0:
				if l.Rune0 == '\n' { // increment current line number for '\n only'.
					line_no++
					line_offset = l.rune1_offset // line_offset may be equal to l.batch_length if \n is last character in batch.
				}
				l.next_rune()
				continue // continue main skip loop

			/* skip star comment block (or return error if end of comment not found) */

			case l.Rune0 == '/' && l.Rune1 == '*': // start of '/*' comment found.
				comment_star_nesting_level = 1
				l.next_rune()
				l.next_rune()

				for comment_star_nesting_level > 0 { // skip all runes until closing '*/' sequence is found.
					switch {
					case l.EOS: // if end-of-batch reached with no closing '*/' sequence found, throw an error.
						return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_MISSING_END_COMMENT_MARK, rsql.ERROR_BATCH_ABORT)

					case l.Rune0 != '*' && l.Rune0 != '/' && l.Rune0 != '\n': // skip any rune except '*', '/' and '\n'. These three exceptions are handled below.
						l.next_rune()

					case l.Rune0 == '*' && l.Rune1 == '/': // end of comment
						comment_star_nesting_level--
						l.next_rune()
						l.next_rune()

					case l.Rune0 == '/' && l.Rune1 == '*': // new nested comment
						comment_star_nesting_level++
						l.next_rune()
						l.next_rune()

					case l.Rune0 == '\n': // end of line found
						line_no++
						line_offset = l.rune1_offset // line_offset may be equal to l.batch_length if \n is last character in batch.
						l.next_rune()

					default: // skip ordinary '*' or '/'
						l.next_rune()
					}
				} // end of skip comment loop.

				continue // l.Rune0 is the rune following the closing '*/' sequence. Continue main skip loop.

			/* skip -- comment, which encompasses up to \n, or up to the rune before end-of-batch */

			case l.Rune0 == '-' && l.Rune1 == '-': // start of -- comment found
				l.next_rune()
				l.next_rune()

				for l.EOS == false { // at most, skip up to the end of file
					if l.Rune0 == '\n' { // skip the end of line that mark the end of comment
						line_no++
						line_offset = l.rune1_offset // line_offset may be equal to l.batch_length if \n is last character in batch.
						l.next_rune()
						break // break comment -- loop
					}

					l.next_rune()
				}

				continue // l.Rune0 is the rune that follows \n, or the fictious rune 0 which is set when EOS is reached. Continue main skip loop.

			/* other ascii character */

			default:
				break MAIN_LOOP // character is ascii, but not ASCII_PLAINSPACE, nor ASCII_CR_LF, nor ASCII_STOP_SKIP_WS, nor '/', nor '-'. Only control characters come here.
			}

			/*** slow path for non-ascii characters ***/

		} else {

			if unicode.IsSpace(l.Rune0) { // unicode 'White_Space' property. Contains other whitespace characters.
				l.next_rune()
				continue // continue main skip loop
			}

			break MAIN_LOOP // break main skip loop
		}

		// body of main skip loop

	} // end of main skip loop

	/* update line info */

	l.line_no = line_no
	l.line_offset = line_offset // may be equal to l.batch_length if \n is last character in batch.

	return nil
}

// eat_arith_operator reads an arithmetic operator lexeme at the current position.
// This function is called by Eat_next_lexeme().
//
// Character at current position must be a special arithmetic character like '+', '*' etc.
//
// A lexeme is made of one or two operator characters, e.g. '+', '>='
//
// Whitespaces are then skipped and the current position is updated.
//
// This function eat only operators made up of characters like '+', '*', etc.
// Operators made up of letters like 'And', 'Or', etc must be read by eat_keyword_or_operator_or_identpart().
//
// Lexeme Lex_type will be LEX_OPERATOR.
//
// Lexeme Lex_subtype will be LEX_OPERATOR_PLUS, LEX_OPERATOR_MINUS, etc.
//
// NOTE: this function can return LEXEME_EXCLAMATIONMARK for an isolated exclamation mark, which is a punctuation lexeme and not an operator lexeme.
//
func (l *Lexer) eat_arith_operator() (Lexeme, *rsql.Error) {
	var (
		lexeme   Lexeme
		rsql_err *rsql.Error
	)

	assert(uc_is_operator(l.Rune0))

	switch l.Rune0 {

	case '+': // artithmetic operators
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_PLUS
		} else {
			return LEXEME_OPERATOR_PLUS, nil
		}

	case '-':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_MINUS
		} else {
			return LEXEME_OPERATOR_MINUS, nil
		}

	case '*':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_MULT
		} else {
			return LEXEME_OPERATOR_MULT, nil
		}

	case '/':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_DIV
		} else {
			return LEXEME_OPERATOR_DIV, nil
		}

	case '%':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_MOD
		} else {
			return LEXEME_OPERATOR_MOD, nil
		}

	case '&': // bit operators
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_BIT_AND
		} else {
			return LEXEME_OPERATOR_BIT_AND, nil
		}

	case '|':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_BIT_OR
		} else {
			return LEXEME_OPERATOR_BIT_OR, nil
		}

	case '^':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_ASSIGN_BIT_XOR
		} else {
			return LEXEME_OPERATOR_BIT_XOR, nil
		}

	case '~':
		lexeme = LEXEME_OPERATOR_BIT_NOT

	case '=': // comparison operator
		lexeme = LEXEME_OPERATOR_COMP_EQUAL

	case '>':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		if l.Rune0 == '=' {
			lexeme = LEXEME_OPERATOR_COMP_GREATER_EQUAL
		} else {
			return LEXEME_OPERATOR_COMP_GREATER, nil
		}

	case '<':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		switch l.Rune0 {
		case '=':
			lexeme = LEXEME_OPERATOR_COMP_LESS_EQUAL
		case '>':
			lexeme = LEXEME_OPERATOR_COMP_NOT_EQUAL
		default:
			return LEXEME_OPERATOR_COMP_LESS, nil
		}

	case '!':
		l.next_rune()
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return LEXEME_INVALID, rsql_err
		}

		switch l.Rune0 {
		case '=':
			lexeme = LEXEME_OPERATOR_COMP_NOT_EQUAL // same as <>
		case '<':
			lexeme = LEXEME_OPERATOR_COMP_GREATER_EQUAL // same as >=
		case '>':
			lexeme = LEXEME_OPERATOR_COMP_LESS_EQUAL // same as <=
		default:
			return LEXEME_EXCLAMATIONMARK, nil // The '!' operator alone is not a valid operator, and is returned as a punctuation lexeme.
		}

	default:
		panic("operator unknown")
	}

	// skip last character for operators with multiple characters, e.g.    +=   >=   !=    etc

	l.next_rune()

	// go to next parsing position

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, rsql_err
	}

	return lexeme, nil
}

// eat_variable reads variable lexeme at the current position.
// This function is called by Eat_next_lexeme().
//
// First character to read must be a single '@'.
//
// If second character is '@', an error is returned.
//
// Lexeme Lex_type will be LEX_VARIABLE.
//
// Lexeme Lex_subtype will be LEX_VARIABLE_SUB.
//
func (l *Lexer) eat_variable() (Lexeme, *rsql.Error) {
	var (
		p_start  Txtdim_t
		lexeme   Lexeme
		rsql_err *rsql.Error
	)

	// note : the prefix '@' belongs to the variable name.
	//        A variable name can be '@a', '@10' or '@myvar'.
	//        It cannot be '@' or 'myvar'.
	//        It cannot be '@@', '@@@@@' etc.

	/* initialization */

	p_start = l.rune0_offset

	assert(l.Rune0 == '@') //  must begin with '@'

	l.next_rune() // skip '@'

	/* read rest of variable name */

	if !uc_is_variable_start(l.Rune0) { // check first char of variable name, which must be a valid start character (note: '@', '$', '#' are not valid)
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_VARIABLE_INVALID_START_CHAR, rsql.ERROR_BATCH_ABORT)
	}

	l.next_rune()

	for uc_is_variable_continue(l.Rune0) { // loop for other variable name characters (note: '@', '$', '#' are valid)
		l.next_rune()
	}

	/* create lexeme */

	variable_name := l.batch_text[p_start:l.rune0_offset]

	if len(variable_name) > SPEC_VARIABLE_MAX_LENGTH {
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_VARIABLE_INVALID_TOO_LONG, rsql.ERROR_BATCH_ABORT)
	}

	lexeme = Create_lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, string(variable_name)) // LEX_VARIABLE lexeme

	/* go to next parsing position */

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, rsql_err
	}

	return lexeme, nil
}

// eat_atfunc reads atfunc (e.g. @@rowcount) lexeme at the current position.
// This function is called by Eat_next_lexeme().
//
// The two first characters to read must be "@@".
//
// Lexeme Lex_type will be LEX_ATFUNC.
//
// Lexeme Lex_subtype will be LEX_ATFUNC_SUB.
//
func (l *Lexer) eat_atfunc() (Lexeme, *rsql.Error) {
	var (
		p_start  Txtdim_t
		lexeme   Lexeme
		rsql_err *rsql.Error
	)

	// note : the prefix '@@' belongs to the function name.

	/* initialization */

	p_start = l.rune0_offset

	assert(l.Rune0 == '@' && l.Rune1 == '@') //  must begin with '@@'

	l.next_rune() // skip '@@'
	l.next_rune()

	/* read rest of atfunc name */

	if !uc_is_atfunc_start(l.Rune0) { // check first char of atfunc name, which must be a valid start character (note: '@', '$', '#' are not valid)
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_ATFUNC_INVALID_START_CHAR, rsql.ERROR_BATCH_ABORT)
	}

	l.next_rune()

	for uc_is_atfunc_continue(l.Rune0) { // loop for other atfunc name characters (note: '@', '$', '#' are valid)
		l.next_rune()
	}

	/* create lexeme */

	atfunc_name := l.batch_text[p_start:l.rune0_offset]

	if len(atfunc_name) > SPEC_ATFUNC_MAX_LENGTH {
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_ATFUNC_INVALID_TOO_LONG, rsql.ERROR_BATCH_ABORT)
	}

	lexeme = Create_lexeme(LEX_ATFUNC, LEX_ATFUNC_SUB, string(atfunc_name)) // LEX_ATFUNC lexeme

	/* go to next parsing position */

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, rsql_err
	}

	return lexeme, nil
}

// eat_delimited_identpart reads a delimited identpart at the current position.
// This function is called by Eat_next_lexeme().
//
// The identpart is delimited by brackets e.g. [my ident].
// It may also be delimited by double-quotes e,g, "my ident" if l.option_quoted_identifier == true.
//
// Lexeme Lex_type will be LEX_IDENTPART.
//
// Lexeme Lex_subtype will be LEX_IDENTPART_SUB.
//
// The returned lexeme_original contains the lexeme in original case.
//
func (l *Lexer) eat_delimited_identpart() (lexeme Lexeme, lexeme_original Lexeme, rsql_err *rsql.Error) {
	var (
		c                 byte
		closing_delimiter rune
		p_start           Txtdim_t
		p_endx            Txtdim_t
	)

	assert(l.Rune0 == '[' || l.Rune0 == '"')

	/* read identpart, delimited with '[' and ']', or double-quote if option_quoted_identifier is true */

	closing_delimiter = ']'

	if l.Rune0 == '"' {
		closing_delimiter = '"'
	}

	l.next_rune() // skip opening delimiter
	p_start = l.rune0_offset

	if l.Rune0 == closing_delimiter { // check if identpart is not empty string
		return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_DELIMITED_EMPTY, rsql.ERROR_BATCH_ABORT)
	}

	if !uc_is_identpart_start_extended(l.Rune0) { // check first char of identpart
		return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_DELIMITED_INVALID_START_CHAR, rsql.ERROR_BATCH_ABORT)
	}

	l.next_rune()

	for l.EOS == false { // don't read beyond EOS
		if l.Rune0 == closing_delimiter { // read up to closing delimiter
			break
		}

		if !uc_is_identpart_continue_extended(l.Rune0) { // if invalid character, return error
			return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_DELIMITED_INVALID_CONTINUE_CHAR, rsql.ERROR_BATCH_ABORT)
		}

		l.next_rune()
	}

	if l.Rune0 != closing_delimiter {
		return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_DELIMITED_CLOSING_DELIMITER_MISSING, rsql.ERROR_BATCH_ABORT)
	}

	p_endx = l.rune0_offset

	l.next_rune() // skip the closing delimiter

	/* create lexeme */

	identpart := l.batch_text[p_start:p_endx]

	if len(identpart) > SPEC_IDENTPART_MAX_LENGTH {
		return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_INVALID_TOO_LONG, rsql.ERROR_BATCH_ABORT)
	}

	if identpart[0] == '#' { // check that identifier is not made up only of hash signs #
		for _, c = range identpart {
			if c != '#' {
				break
			}
		}

		if c == '#' {
			return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_INVALID_HASHSIGN_ONLY, rsql.ERROR_BATCH_ABORT)
		}
	}

	identpart_str := string(identpart)

	lexeme = Create_lexeme(LEX_IDENTPART, LEX_IDENTPART_SUB, identpart_str)                   // LEX_IDENTPART lexeme
	lexeme_original = Create_lexeme_original(LEX_IDENTPART, LEX_IDENTPART_SUB, identpart_str) //   same, but in original case

	/* go to next parsing position */

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, LEXEME_INVALID, rsql_err
	}

	return lexeme, lexeme_original, nil
}

// eat_keyword_or_operator_or_identpart reads keyword, operator or identpart lexeme at the current position.
// This function is called by Eat_next_lexeme().
//
// The identpart cannot be delimited, e.g [my ident] or "my_ident" are not allowed.
//
// Lex_type may be LEX_KEYWORD, LEX_OPERATOR or LEX_IDENTPART.
//
//      - for Lex_type LEX_KEYWORD, Lex_subtype will be LEX_KEYWORD_ADD, LEX_KEYWORD_ALTER, etc.
//      - for Lex_type LEX_OPERATOR, Lex_subtype will be LEX_OPERATOR_LOGICAL_AND, LEX_OPERATOR_LOGICAL_OR, etc.
//      - for Lex_type LEX_IDENTPART, Lex_subtype will be LEX_IDENTPART_SUB.
//
// For LEX_IDENTPART, lexeme_original contains the lexeme in original case. For other Lex_types, it contains LEXEME_INVALID.
//
func (l *Lexer) eat_keyword_or_operator_or_identpart() (lexeme Lexeme, lexeme_original Lexeme, rsql_err *rsql.Error) {
	var (
		c       byte
		p_start Txtdim_t // start of keyword, operator or identpart
		ok      bool
	)

	/* initialization */

	p_start = l.rune0_offset

	assert(l.Rune0 != '[' && l.Rune0 != '"') //  identifier must be non-delimited

	/* read identifier */

	if !uc_is_identpart_start(l.Rune0) { // check first char of identifier, which must be a valid start character (note: '@', '$' are not valid, '#' is valid)
		return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_INVALID_START_CHAR, rsql.ERROR_BATCH_ABORT)
	}

	l.next_rune()

	for uc_is_identpart_continue(l.Rune0) { // loop for other identifier characters (note: '@', '$', '#' are valid)
		l.next_rune()
	}

	/* check if identifier is a keyword or operator, or create it if LEX_IDENTPART */

	identpart := l.batch_text[p_start:l.rune0_offset]

	if len(identpart) > SPEC_IDENTPART_MAX_LENGTH {
		return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_INVALID_TOO_LONG, rsql.ERROR_BATCH_ABORT)
	}

	if identpart[0] == '#' { // check that identifier is not made up only of hash signs #
		for _, c = range identpart {
			if c != '#' {
				break
			}
		}

		if c == '#' {
			return LEXEME_INVALID, LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_IDENTPART_INVALID_HASHSIGN_ONLY, rsql.ERROR_BATCH_ABORT)
		}
	}

	identpart_str := string(identpart)
	identpart_str_lc := strings.ToLower(identpart_str)

	lexeme, ok = G_KEYWORDS_OPERATORS[identpart_str_lc] // if identpart is in fact a keyword or an operator, retrieve the corresponding lexeme

	if ok == false { // identpart is an identifier
		lexeme = Create_lexeme(LEX_IDENTPART, LEX_IDENTPART_SUB, identpart_str)                   // LEX_IDENTPART lexeme
		lexeme_original = Create_lexeme_original(LEX_IDENTPART, LEX_IDENTPART_SUB, identpart_str) //   same, but in original case
	}

	/* go to next parsing position */

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, LEXEME_INVALID, rsql_err
	}

	return lexeme, lexeme_original, nil
}

// eat_literal_number reads literal number lexeme at the current position.
// l.rune0_offset points to a string that begins with [0-9], or '.' followed by [0-9] (to not be confused with a dot followed by an identifier).
//
// The number has no sign (except for money, which can contain a sign '+' or '-' after the '$' prefix).
//
// It can also point to '$' for money number, like $123.45 or $-45 or $ -45 (spaces and sign are allowed after the '$' prefix).
//
// This money case excepted, space is not allowed anywhere in the number.
//
// Lexeme Lex_type will be LEX_LITERAL_NUMBER.
//
//     - If it begins with '$', Lex_subtype will be a LEX_LITERAL_NUMBER_MONEY, with '-' sign if any. (exponent 'E' or 'e' is allowed).
//     - If it contains a 'd' suffix, it will be LEX_LITERAL_NUMBER_NUMERIC. (exponent 'E' or 'e' is allowed).
//     - If it contains a 'f' suffix, it will be a LEX_LITERAL_NUMBER_FLOAT. (exponent 'E' or 'e' is allowed).
//     - Else, if it just contains a 'e' or 'E' exponent, it will be a LEX_LITERAL_NUMBER_FLOAT.
//     - Else, if if contains a fractional point '.', it is a LEX_LITERAL_NUMBER_NUMERIC.
//     - Else, it contains only digits [0-9] like 1234. It is a LEX_LITERAL_NUMBER_INTEGRAL.
//
// Return a "normalized" Lexeme, which contains no spaces, no leading '0', nor suffix 'd' or 'f', nor prefix '$'.
// It is a pure number without any "decoration" (suffix, prefix, etc).
// Exponent 'e' is lowercase, and trailing 0 are kept as it is important for precision of numeric types.
//
func (l *Lexer) eat_literal_number() (Lexeme, *rsql.Error) {
	var (
		exists_money_prefix_part bool = false // '$' (decnumber type)
		money_sign               byte = '+'

		exists_integer_part  bool     = false
		p_integer_part_start Txtdim_t = -1 // >=0 if exists_integer_part = true
		p_integer_part_end   Txtdim_t = -1 // >=0 if exists_integer_part = true

		exists_fractional_point bool = false

		exists_fractional_part  bool     = false
		p_fractional_part_start Txtdim_t = -1 // >=0 if exists_fractional_part = true
		p_fractional_part_end   Txtdim_t = -1 // >=0 if exists_fractional_part = true

		exists_exponent_part  bool     = false
		exponent_sign         byte     = '+'
		p_exponent_part_start Txtdim_t = -1 // >=0 if exists_exponent_part = true
		p_exponent_part_end   Txtdim_t = -1 // >=0 if exists_exponent_part = true

		exists_suffix_part bool = false // 'd' (decnumber type) or 'f' (C double type)
		suffix_char        byte = '\000'

		// note : All p_xxx_part_end offsets point to the last character belonging to the part being parsed, not to the character following it.

		number_length Txtdim_t
		p_work        Txtdim_t
		p_num         Txtdim_t

		subtype  Lex_subtype_t
		lexeme   Lexeme
		rsql_err *rsql.Error

		workbuff  [SPEC_NUMBER_MAX_LENGTH]byte // buffer in which number is cleanly rewritten
		workslice []byte
	)

	// here, l.rune0_offset points to first character, which is [0-9], or '.' followed by [0-9]
	// In this case, the beginning of the string is thus always a valid number.
	// It can also be '$' for money number. If '$' is not followed by a valid number, an error will be returned.

	/* eat money prefix part if any */

	if l.Rune0 == '$' {

		exists_money_prefix_part = true
		l.next_rune()

		for l.Rune0 == ' ' { // skip plain spaces
			l.next_rune()
		}

		if l.Rune0 == '-' { // eat money sign if any
			money_sign = '-'
			l.next_rune()
		} else if l.Rune0 == '+' {
			l.next_rune()
		}

		if !(uc_is_0_9(l.Rune0) || (l.Rune0 == '.' && uc_is_0_9(l.Rune1))) {
			return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_NUMBER_MONEY_INVALID, rsql.ERROR_BATCH_ABORT) // no digit found for money number
		}
	}

	/* here, l.rune0_offset points to [0..9] or '.' followed by [0..9] */

	assert(uc_is_0_9(l.Rune0) || (l.Rune0 == '.' && uc_is_0_9(l.Rune1)))

	/* skip leading zeroes of integer part if any */

	if l.Rune0 == '0' { // exists at least a zero digit in the integer part.
		exists_integer_part = true

		for l.Rune1 == '0' { // skip all leading 0 but one
			l.next_rune()
		}

		p_integer_part_start = l.rune0_offset // points to last leading zero. (provisional. Can be modified by the code just below)
		p_integer_part_end = l.rune0_offset   // same as above
		l.next_rune()                         // skip last leading 0
	}

	// here, at l.rune0_offset, the character is [1-9] or '.' or 'e' or 'E', or 'd' or 'f', or another character not belonging to the number.

	/* eat integer part if any */

	if uc_is_0_9(l.Rune0) { // exists at least a non-zero digit 1..9 in the integer part.
		exists_integer_part = true
		p_integer_part_start = l.rune0_offset

		for uc_is_0_9(l.Rune1) { // 0..9
			l.next_rune()
		}

		p_integer_part_end = l.rune0_offset
		l.next_rune()
	}

	/* eat fractional point and fractional part if any */

	if l.Rune0 == '.' {
		exists_fractional_point = true
		l.next_rune() // skip fractional point

		if uc_is_0_9(l.Rune0) { // exists at least a digit 0..9 in the fractional part.
			exists_fractional_part = true
			p_fractional_part_start = l.rune0_offset

			for uc_is_0_9(l.Rune1) { // 0..9
				l.next_rune()
			}

			p_fractional_part_end = l.rune0_offset
			l.next_rune()
		}
	}

	// === at this point, at least one of the precedent steps has been processed.
	// So far, we have read the integer and fractional part of the number, e.g. :  0      00005       5       123      0.      123.     123.456       0.7       .123

	/* eat exponent part if any */

	if l.Rune0 == 'E' || l.Rune0 == 'e' {
		exists_exponent_part = true
		l.next_rune()

		if l.Rune0 == '-' { // eat sign if any.
			exponent_sign = '-'
			l.next_rune()
		} else if l.Rune0 == '+' {
			l.next_rune()
		}

		if !uc_is_0_9(l.Rune0) { // if no digit 0..9 found in the exponent part. Return error.
			return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_NUMBER_EXPONENT_PART_MISSING, rsql.ERROR_BATCH_ABORT) // exponent digits are mandatory.
		}

		if l.Rune0 == '0' { // exists at least a zero digit in the exponent part.

			for l.Rune1 == '0' { // skip all leading 0 but one
				l.next_rune()
			}

			p_exponent_part_start = l.rune0_offset // points to last leading zero. (provisional. Can be modified by the code just below)
			p_exponent_part_end = l.rune0_offset   // same as above
			l.next_rune()
		}

		if uc_is_0_9(l.Rune0) { // exists at least a non-zero digit 1..9 in the exponent part. Eat it.
			p_exponent_part_start = l.rune0_offset

			for uc_is_0_9(l.Rune1) { // 0..9
				l.next_rune()
			}

			p_exponent_part_end = l.rune0_offset
			l.next_rune()
		}
	}

	/* eat suffix part if any */

	if l.Rune0 == 'd' || l.Rune0 == 'f' {
		exists_suffix_part = true
		suffix_char = byte(l.Rune0)
		l.next_rune()
	}

	if exists_money_prefix_part && exists_suffix_part { // money prefix and suffix together are forbidden.
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_NUMBER_MONEY_SUFFIX_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
	}

	/* === at this point, the number has been entirely read === */

	/* define subtype */

	switch {
	case exists_money_prefix_part: // '$' prefix. Force subtype to MONEY.
		subtype = LEX_LITERAL_NUMBER_MONEY

	case exists_suffix_part: // 'd' or 'f' suffix. Force subtype to FLOAT or NUMERIC.
		switch suffix_char {
		case 'd':
			subtype = LEX_LITERAL_NUMBER_NUMERIC
		case 'f':
			subtype = LEX_LITERAL_NUMBER_FLOAT
		default:
			panic("literal numeric suffix unknown")
		}

	case exists_exponent_part: // no 'd' or 'f' suffix, but exponent part exists.
		subtype = LEX_LITERAL_NUMBER_FLOAT

	case exists_fractional_point: // no 'd' or 'f' suffix, and no exponent part.
		subtype = LEX_LITERAL_NUMBER_NUMERIC

	default: // no 'd' or 'f' suffix, no exponent part, and no fractional point.
		subtype = LEX_LITERAL_NUMBER_INTEGRAL
	}

	/* compute string length and resize workslice */

	number_length = 0

	if money_sign == '-' {
		number_length += 1 // '-'
	}

	if exists_integer_part {
		number_length += (p_integer_part_end - p_integer_part_start + 1)
	} else {
		number_length += 1 // '0'
	}

	if exists_fractional_point {
		number_length += 1 // '.'
	}

	if exists_fractional_part {
		number_length += (p_fractional_part_end - p_fractional_part_start + 1)
	}

	if exists_exponent_part {
		number_length += 1 // 'e' character

		if exponent_sign == '-' {
			number_length += 1 // '-'
		}

		number_length += (p_exponent_part_end - p_exponent_part_start + 1) // exponent part always exists if exists_exponent_part is true.
	}

	if number_length > SPEC_NUMBER_MAX_LENGTH { // if number is too long, return error
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_NUMBER_INVALID_TOO_LONG, rsql.ERROR_BATCH_ABORT)
	}

	workslice = workbuff[:number_length]

	/* write number in workslice */

	p_work = 0

	if money_sign == '-' {
		workslice[p_work] = '-' // '-'
		p_work++
	}

	if exists_integer_part { // write integer part.
		p_num = p_integer_part_start

		for p_num <= p_integer_part_end {
			workslice[p_work] = l.batch_text[p_num]
			p_work++
			p_num++
		}
	} else {
		workslice[p_work] = '0' // if no integer part, write '0'
		p_work++
	}

	if exists_fractional_point {
		workslice[p_work] = '.' // write fractional point.
		p_work++
	}

	if exists_fractional_part { // copy fractional part.
		p_num = p_fractional_part_start

		for p_num <= p_fractional_part_end {
			workslice[p_work] = l.batch_text[p_num]
			p_work++
			p_num++
		}
	}

	if exists_exponent_part {
		workslice[p_work] = 'e' // write exponent 'e'.
		p_work++

		if exponent_sign == '-' {
			workslice[p_work] = '-' // write exponent sign '-'.
			p_work++
		}

		p_num = p_exponent_part_start // write exponent part, which always exists if exists_exponent_part is true.

		for p_num <= p_exponent_part_end {
			workslice[p_work] = l.batch_text[p_num]
			p_work++
			p_num++
		}
	}

	assert(p_work == number_length)

	/* check if number is not immediately followed by an identifier, e.g. 123.45abc */

	if uc_is_identpart_continue(l.Rune0) {
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_NUMBER_FOLLOWED_BY_IDENTCHAR, rsql.ERROR_BATCH_ABORT)
	}

	/* create lexeme */

	lexeme = Create_lexeme(LEX_LITERAL_NUMBER, subtype, string(workslice))

	/* go to next parsing position */

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, rsql_err
	}

	return lexeme, nil
}

// eat_literal_hexastring reads literal hexa lexeme at the current position.
// l.rune0_offset points to a string that begins with '0x' or '0X'.
//
// Leading 0 are significant. 0x00001234 is not the same hexa string as 0x1234
//
// '0x' is a valid hexa string.
//
// A hexa string length must be even. If number is odd, a supplementary leading 0 will be inserted ( 0x123 -> 0x0123 ).
//
// Lexeme Lex_type will be LEX_LITERAL_HEXASTRING.
//
// Lexeme Lex_subtype will be LEX_LITERAL_HEXASTRING_SUB.
//
// Return a "normalized" Lexeme with lowercase digits, and supplementary leading 0 inserted if needed.
//
func (l *Lexer) eat_literal_hexastring() (Lexeme, *rsql.Error) {

	const WORKBUFFER_DEFAULT_SIZE = 100 // default size for workbuffer.

	var (
		p_start Txtdim_t
		p_end   Txtdim_t

		p_work            Txtdim_t
		p_num             Txtdim_t
		number_length     Txtdim_t                      // number of digits
		hexastring_length Txtdim_t                      // '0x' + (supplementary '0' if odd number of digits) + number of digits
		workbuffer        [WORKBUFFER_DEFAULT_SIZE]byte // buffer in which hexastring is "normalized". If not large enough, workslice will create another bigger buffer.
		workslice         []byte

		lexeme   Lexeme
		rsql_err *rsql.Error
	)

	assert(l.Rune0 == '0' && (l.Rune1 == 'x' || l.Rune1 == 'X'))

	/* scan hexa digits */

	l.next_rune() // skip prefix '0x'
	l.next_rune()

	if !uc_is_hexa(l.Rune0) { // hexastring is just "0x" --goto--> LABEL_CREATE_HEXASTRING
		workslice = append(workslice, "0x"...)
		goto LABEL_CREATE_HEXASTRING
	}

	p_start = l.rune0_offset

	for uc_is_hexa(l.Rune1) { // read hexa digits
		l.next_rune()
	}

	p_end = l.rune0_offset
	l.next_rune()

	// here, l.rune0_offset points to the character following the last hexa digit.

	/* compute hexastring length */

	number_length = p_end - p_start + 1

	hexastring_length = 2 + number_length // '0x' + hexa digits

	if uint64(number_length)&1 != 0 { // if length is not even
		hexastring_length = 2 + 1 + number_length // '0x' + '0' + hexa digits
	}

	if hexastring_length > SPEC_HEXASTRING_MAX_LENGTH {
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_HEXASTRING_INVALID_TOO_LONG, rsql.ERROR_BATCH_ABORT)
	}

	/* write hexastring into workslice */

	if hexastring_length <= WORKBUFFER_DEFAULT_SIZE {
		workslice = workbuffer[:hexastring_length]
	} else {
		workslice = make([]byte, hexastring_length)
	}

	p_work = 0

	workslice[p_work] = '0' // write '0x' prefix
	p_work++
	workslice[p_work] = 'x'
	p_work++

	if uint64(number_length)&1 != 0 { // write supplementary '0'
		workslice[p_work] = '0'
		p_work++
	}

	p_num = p_start

	for p_num <= p_end { // copy hexa digits in lower case
		workslice[p_work] = byte(uc_ascii_to_lowercase(rune(l.batch_text[p_num])))
		p_work++
		p_num++
	}

	assert(p_work == hexastring_length)

LABEL_CREATE_HEXASTRING:

	/* check if hexastring is not immediately followed by an identifier, e.g. 0xffeexyz */

	if uc_is_identpart_continue(l.Rune0) {
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_HEXASTRING_FOLLOWED_BY_IDENTCHAR, rsql.ERROR_BATCH_ABORT)
	}

	/* create lexeme */

	lexeme = Create_lexeme(LEX_LITERAL_HEXASTRING, LEX_LITERAL_HEXASTRING_SUB, string(workslice))

	/* go to next parsing position */

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, rsql_err
	}

	return lexeme, nil
}

// eat_hexa0 is a helper function that reads hexa digits and convert them into uint32.
// The hexa number can contain up to 8 hex digits.
//
// So, 'length' is a value from 0 to 8. Usually, it will be 4 or 8
//
// This function returns an error if non-hexa digit is encountered in the 'length' runes to parse, or if l.EOS occurs.
//
// This function is used by eat_literal_string(), when reading \u or \U unicode escape sequence.
//
func (l *Lexer) eat_hexa0(length Txtdim_t) (uint32, *rsql.Error) {
	var (
		i, k          Txtdim_t
		c             byte
		hexa_sequence [8]byte // used only if error occurs
		p_start       Txtdim_t

		val     uint32
		res_val uint32 = 0
	)

	assert(length <= 8)

	p_start = l.rune0_offset // this position may point to the fictious 0 at end-of-string

	for i = 0; i < length; i++ { // try to read exactly 'length' runes

		switch {
		case l.Rune0 >= '0' && l.Rune0 <= '9':
			val = uint32(l.Rune0) - '0'

		case l.Rune0 >= 'A' && l.Rune0 <= 'F':
			val = (uint32(l.Rune0) - 'A') + 10

		case l.Rune0 >= 'a' && l.Rune0 <= 'f':
			val = (uint32(l.Rune0) - 'a') + 10

		default: // bad hexa character, return error. This bad character may be the fictious 0 found when l.EOS==true, which occurs when there are not enough digits at end-of-batch.

			for k = 0; k < length; k++ { // copy bad hexa string into hexa_sequence[] buffer,
				if p_start+k >= l.batch_length { // if end-of-batch is reached, fill out hexa_sequence buffer with '?'
					hexa_sequence[k] = '?'
					continue
				}

				c = l.batch_text[p_start+k]

				if unicode.IsPrint(rune(c)) { // if printable rune, display it
					hexa_sequence[k] = c
				} else {
					hexa_sequence[k] = '?' // else, display a '?'
				}
			}

			return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_STRING_UHEXACODE_INVALID, rsql.ERROR_BATCH_ABORT, string(hexa_sequence[:k]))
		}

		res_val += val << uint((length-i-1)*4)

		l.next_rune()
	}

	return res_val, nil
}

// eat_escape_sequence0 is a helper function that reads an escape sequence, e.g. "\n" or "\t", and returns the rune value of the escape sequence.
//
// This function returns an error if an invalid hexa digit is encountered in the \u or \U hexa escape sequence.
//
//      Valid escape sequences are :
//          - \n  \t  \\  \'  \"
//          - \uxxxx or \Uxxxxxxxx  where x is a hexa digit
//
// IMPORTANT: the current position should not point to a single backslash at end of line, that is, \ immediately followed by lf or crlf. It panics if it is the case. The caller must take care of this special case.
//
// If the escape sequence is unknown ( \ followed by non-special character, \w for instance), the backslash is just skipped.
//
// The caller must first check the 'rsql_err' value for error, and then the 'ok' value.
// If the returned 'ok' value is true, it indicates that the escape sequence has returned a rune that the caller can use. Else, the sequence has only been skipped.
//
func (l *Lexer) eat_escape_sequence0() (uc rune, ok bool, rsql_err *rsql.Error) {
	var (
		code_point   uint32
		escaped_rune rune = utf8.RuneError
	)

	assert(l.Rune0 == '\\')

	l.next_rune() // skip backslash

	if l.EOS { /* if backslash is last character of the batch */
		return utf8.RuneError, false, nil // just return. The backslash rune has only been skipped.
	}

	switch l.Rune0 {
	case 'u': // unicode code point \uxxxx
		l.next_rune()
		if code_point, rsql_err = l.eat_hexa0(4); rsql_err != nil { // returns error if invalid hex digit, or not enough digits.
			return utf8.RuneError, false, rsql_err
		}
		return rune(code_point), true, nil

	case 'U': // unicode code point \Uxxxxxxxx
		l.next_rune()
		if code_point, rsql_err = l.eat_hexa0(8); rsql_err != nil { // returns error if invalid hex digit, or not enough digits.
			return utf8.RuneError, false, rsql_err
		}
		return rune(code_point), true, nil

	case '\\':
		escaped_rune = '\\'

	case '\'':
		escaped_rune = '\''

	case '"':
		escaped_rune = '"'

	case 'n':
		escaped_rune = '\n'

	case 'r':
		escaped_rune = '\r'

	case 't':
		escaped_rune = '\t'

	case '\n': // backslash at end of line. Caller must take care of this case.
		panic("forbidden")

	case '\r':
		if l.Rune1 == '\n' { // backslash at end of line. Caller must take care of this case.
			panic("forbidden")
		}
		return utf8.RuneError, false, nil // else, just return. Only the backslash rune has been eaten.

	default: // else, just return. Only the backslash rune has been eaten.
		return utf8.RuneError, false, nil
	}

	l.next_rune()

	return escaped_rune, true, nil
}

// eat_literal_string reads literal string lexeme at the current position.
//
//     l.rune0_offset points to a string that begins with
//        - '
//        - or   N'
//        - or   e'
//        - or   "    (allowed only if option_quoted_identifier is false)
//        - or   N"   (allowed only if option_quoted_identifier is false)
//        - or   e"   (allowed only if option_quoted_identifier is false)
//
//     Examples :
//        - 'normal string literal. O''Hara.'  Double the quote if you need one in the string.
//        - N'normal string like above'        This is because Microsoft SQL Server uses prefix 'N' to indicate unicode string.
//                                             But as we always encode in UTF-8, so we don't need this prefix.
//        - e'H\u00e9l\u00e8ne O''Hara'        \uxxxx, \Uxxxxxxxx, \n, \r, \t are allowed. This example gives the string   Hélène O'Hara.
//
// If option_quoted_identifier is false, " can be used instead of ' as string delimiter.
//
// Lexeme Lex_type will be LEX_LITERAL_STRING.
//
// Lexeme Lex_subtype will be LEX_LITERAL_STRING_SUB.
//
func (l *Lexer) eat_literal_string(option_quoted_identifier bool) (Lexeme, *rsql.Error) {

	const WORKBUFFER_DEFAULT_SIZE = 100

	var (
		escape_mode              bool = false
		delim                    rune
		line_no                  uint32
		line_offset              Txtdim_t
		p_seg_start              Txtdim_t
		p_seg_endx               Txtdim_t
		last_segment             []byte
		literal_string           []byte
		escaped_rune             rune
		flag_append_escaped_rune bool = false
		flag_workbuffer_is_used  bool = false
		lexeme                   Lexeme
		rsql_err                 *rsql.Error

		workbuffer [WORKBUFFER_DEFAULT_SIZE]byte // size must be large enough to avoid most memory allocation by append()
		workslice  []byte                        = workbuffer[:0]
	)

	/* initialization */

	line_no = l.line_no // coordinates in case of error
	line_offset = l.line_offset

	// here, l.rune0_offset points to first character, which may be:    N   e   '   "

	switch {
	case l.Rune0 == 'N': // if prefix N, just skip it.
		l.next_rune()

	case l.Rune0 == 'e': // 'e' is prefix for an escapable string.
		escape_mode = true
		l.next_rune()
	}

	switch {
	case l.Rune0 == '\'': // here, the current character which is the string delimiter must be a single or double quote.
		delim = '\''

	case !option_quoted_identifier && l.Rune0 == '"':
		delim = '"'

	default:
		panic("pointer doesn't point at start of a string literal.")
	}

	/* process the string */

	l.next_rune()                // skip string delimiter
	p_seg_start = l.rune0_offset // p_seg_start points to the first character of the string.
	p_seg_endx = -1

	// loop for each rune in the string

MAIN_LOOP:
	for l.EOS == false { /* [normal runes][escape seq]   [normal runes][escape seq]   [normal runes][escape seq] ... [normal runes] */

		switch {
		case l.Rune0 == delim:
			if l.Rune1 == delim { /* if quote doubled  */
				l.next_rune()
				p_seg_endx = l.rune0_offset // keep the first quote
				l.next_rune()
				flag_append_escaped_rune = false
				break // break switch
			}

			p_seg_endx = l.rune0_offset /* terminating quote is found */
			break MAIN_LOOP

		case l.Rune0 == '\\':
			switch {
			case l.Rune1 == '\n': /* backslash at end of line */
				p_seg_endx = l.rune0_offset
				l.next_rune()
				l.next_rune()
				line_no++
				line_offset = l.rune0_offset
				flag_append_escaped_rune = false

			case l.Rune1 == '\r' && l.Rune2 == '\n': /* backslash at end of line */
				p_seg_endx = l.rune0_offset
				l.next_rune()
				l.next_rune()
				l.next_rune()
				line_no++
				line_offset = l.rune0_offset
				flag_append_escaped_rune = false

			case escape_mode: /* if backslash escape */
				p_seg_endx = l.rune0_offset
				if escaped_rune, flag_append_escaped_rune, rsql_err = l.eat_escape_sequence0(); rsql_err != nil {
					return LEXEME_INVALID, rsql_err
				}

			default: // backslash is considered as a normal character
				l.next_rune()
				continue
			}

		case l.Rune0 == '\n':
			l.next_rune()
			line_no++
			line_offset = l.rune0_offset
			continue

		default: /* normal character */
			l.next_rune()
			continue
		}

		// here, we must process a doubled delimiter, a backslash at end of line, or an escape sequence.

		if p_seg_endx > p_seg_start { // append the segment before the escape sequence if any
			flag_workbuffer_is_used = true
			last_segment = l.batch_text[p_seg_start:p_seg_endx]
			workslice = norm.NFC.Append(workslice, last_segment...)
		}

		if flag_append_escaped_rune { // append the rune returned by the escape sequence if any
			flag_workbuffer_is_used = true
			workslice = norm.NFC.AppendString(workslice, string(escaped_rune))
		}

		p_seg_start = l.rune0_offset
		p_seg_endx = -1

	} // end MAIN LOOP

	// here, l.rune0_offset points normally to the ending delimiter. But if ending delimiter is missing, it points to the fictious rune \0 with l.EOS==true.

	/* check that string is terminated by ending delimiter */

	if l.EOS { // if ending delimiter not found, return error
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_STRING_ENDING_QUOTE_MISSING, rsql.ERROR_BATCH_ABORT)
	}

	l.next_rune() // skip ending delimiter

	/* append last segment if needed */

	literal_string = l.batch_text[p_seg_start:p_seg_endx] // normal case if no escaping sequence in string

	if flag_workbuffer_is_used {
		if p_seg_endx > p_seg_start {
			last_segment = l.batch_text[p_seg_start:p_seg_endx]
			workslice = norm.NFC.Append(workslice, last_segment...)
		}
		literal_string = workslice
	}

	/* create lexeme */

	if len(literal_string) > SPEC_STRING_MAX_LENGTH {
		return LEXEME_INVALID, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_LITERAL_STRING_INVALID_TOO_LONG, rsql.ERROR_BATCH_ABORT)
	}

	lexeme = Create_lexeme(LEX_LITERAL_STRING, LEX_LITERAL_STRING_SUB, string(literal_string))

	/* update line info */

	l.line_no = line_no
	l.line_offset = line_offset // may be equal to l.batch_length if \n is last character in batch.

	/* go to next parsing position */

	if rsql_err = l.skip_whitespace(); rsql_err != nil {
		return LEXEME_INVALID, rsql_err
	}

	return lexeme, nil
}

// Eat_next_lexeme reads the lexeme at the current position in the SQL batch.
// This function is the core function of the lexer.
//
// It reads one or two characters at the l.rune0_offset position, and calls a specialized lexeme reading function accordingly, acting like a multiplexer.
//
//       This function stores a Lexeme at each call in l.Current_lexeme, in lowercase for identifiers, keywords, operators, variables, atfuncs.
//
//       It can contain successively :
//          - LEXEME_INVALID, before a batch has been attached.
//          - LEXEME_START_OF_BATCH, if l.Eat_next_lexeme() has not been called yet.
//          - a lexeme for keyword, operator, identpart, literal, or punctuations.
//          - LEXEME_END_OF_BATCH
//
//       For error display, the field l.Current_line contains the line no and position of the lexeme.
//       l.Current_lexeme contains the last lexeme correctly read.
//
//       For LEX_IDENTPART, l.Current_lexeme_original contains lexeme in original case. For other lex_types, it contains LEXEME_INVALID.
//
func (l *Lexer) Eat_next_lexeme() *rsql.Error {
	var (
		flag_punctuation bool = false
		uc               rune
		lexeme           Lexeme
		lexeme_original  Lexeme
		rsql_err         *rsql.Error
	)

	/* put the coordinates (line_no/pos) of the next lexeme we are going to read in l.Current_line.No/Pos, so that they appear in case of error.
	   l.Current_lexeme still contains the lexeme previously read successfully. It will be updated at the end of this function.
	*/

	l.Current_line_old = l.Current_line // save last line/pos
	l.Current_line.No = l.line_no
	l.Current_line.Pos = l.rune0_offset - l.line_offset + 1 // pos is indexed from 1
	l.Info_line = l.Current_line

	uc = l.Rune0 // current rune

	/* multiplexer */

	switch {

	case uc == '[':
		if lexeme, lexeme_original, rsql_err = l.eat_delimited_identpart(); rsql_err != nil {
			return rsql_err
		}

	case uc_is_operator(uc): // arithmetic operator   +  -  >=  etc
		if lexeme, rsql_err = l.eat_arith_operator(); rsql_err != nil {
			return rsql_err
		}

	case uc == '@':
		switch {
		case l.Rune1 == '@': // atfunc   @@
			if lexeme, rsql_err = l.eat_atfunc(); rsql_err != nil {
				return rsql_err
			}
		default: // variable   @
			if lexeme, rsql_err = l.eat_variable(); rsql_err != nil {
				return rsql_err
			}
		}

	case uc == '\'' || // single quoted string    '  e'  N'
		(uc == 'e' && l.Rune1 == '\'') ||
		(uc == 'N' && l.Rune1 == '\''):
		if lexeme, rsql_err = l.eat_literal_string(l.option_quoted_identifier); rsql_err != nil {
			return rsql_err
		}

	case l.option_quoted_identifier && uc == '"': // double quoted identpart    "       (if option quoted ident ON)
		if lexeme, lexeme_original, rsql_err = l.eat_delimited_identpart(); rsql_err != nil {
			return rsql_err
		}

	case !l.option_quoted_identifier && (uc == '"' || // double quoted string    "  e"  N"  (if option quoted ident OFF)
		(uc == 'e' && l.Rune1 == '"') ||
		(uc == 'N' && l.Rune1 == '"')):
		if lexeme, rsql_err = l.eat_literal_string(l.option_quoted_identifier); rsql_err != nil {
			return rsql_err
		}

	case uc_is_identpart_start(uc): // ident or keyword or operator, or ident    # _ a-z A-Z or any Letter
		if lexeme, lexeme_original, rsql_err = l.eat_keyword_or_operator_or_identpart(); rsql_err != nil {
			return rsql_err
		}

	case uc == '$':
		if lexeme, rsql_err = l.eat_literal_number(); rsql_err != nil {
			return rsql_err
		}

	case uc == '0' && (l.Rune1 == 'x' || l.Rune1 == 'X'): // literal hexa    0x  0X
		if lexeme, rsql_err = l.eat_literal_hexastring(); rsql_err != nil {
			return rsql_err
		}

	case uc_is_0_9(uc) || (uc == '.' && uc_is_0_9(l.Rune1)): // literal number ordinary  0  1  9     .0  .1  .9
		if lexeme, rsql_err = l.eat_literal_number(); rsql_err != nil {
			return rsql_err
		}

	case uc == '.': // dot
		lexeme = LEXEME_DOT
		flag_punctuation = true

	case uc == ',': // comma
		lexeme = LEXEME_COMMA
		flag_punctuation = true

	case uc == '(': // left paren
		lexeme = LEXEME_LPAREN
		flag_punctuation = true

	case uc == ')': // right paren
		lexeme = LEXEME_RPAREN
		flag_punctuation = true

	case uc == ';': // semicolon
		lexeme = LEXEME_SEMICOLON
		flag_punctuation = true

	case uc == ':': // colon
		lexeme = LEXEME_COLON
		flag_punctuation = true

	case uc == '?': // questionmark
		lexeme = LEXEME_QUESTIONMARK
		flag_punctuation = true

	case uc == '{': // left curly
		lexeme = LEXEME_LCURLY
		flag_punctuation = true

	case uc == '}': // right curly
		lexeme = LEXEME_RCURLY
		flag_punctuation = true

	case l.EOS == true: // end-of-file
		if l.Current_lexeme.Lex_type == LEX_END_OF_BATCH { // forbidden to read end-of-file more than once
			panic("You have already got end-of-batch lexeme.")
		}
		lexeme = LEXEME_END_OF_BATCH // normal end of file

	default: // else, input is invalid, because the character is not start of a valid lexeme, e.g. ] \0 etc
		if unicode.IsPrint(uc) { // if ascii printable, display it in error message.
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_CHARACTER_IN_BATCH, rsql.ERROR_BATCH_ABORT, uc)
		}
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_CHARACTER_IN_BATCH_CODEPOINT, rsql.ERROR_BATCH_ABORT, uc) // else, just display the code point in error message.
	}

	// if punctuation, skip whitespace

	if flag_punctuation {
		l.next_rune() // skip operator character
		if rsql_err = l.skip_whitespace(); rsql_err != nil {
			return rsql_err
		}
	}

	// here, the next lexeme has been successfully read.

	/* make the current lexeme contains the newly read lexeme */

	l.Current_lexeme = lexeme                   //  note: for LEX_LITERAL_NUMBER and LEX_LITERAL_HEXA, the lexeme has been "normalized". see eat_literal_number() and eat_literal_hexa()
	l.Current_lexeme_original = lexeme_original //  only valid for LEX_IDENTPART

	return nil
}

// Info_line_put_old updates line information by setting it to the previous lexeme line/pos, which is displayed in case of error.
//
//       If you want to have the previous line/pos in error message, which is necessary in some cases to have a better line coordinate, you must call Info_line_put_old() just after calling Eat_next_lexeme().
//       You can then use Info_line_update() to manually refresh it, or wait until the next call to Eat_next_lexeme().
//
func (l *Lexer) Info_line_put_old() {

	l.Info_line = l.Current_line_old
}

// Info_line_update updates line information, which is displayed in case of error.
// Info_line is updated each time a new lexeme is read.
// But if Info_line_put_old() has been called, Info_line is set to the previous lexeme line/pos, until Info_line_update() or Eat_next_lexeme() is used.
//
func (l *Lexer) Info_line_update() {

	l.Info_line = l.Current_line
}

func (l *Lexer) Print_Info_line() {

	fmt.Printf("Info_line: %d:%d\n", l.Info_line.No, l.Info_line.Pos)
}
