package lex

import (
	"strings"
)

// All Lex_type_t constants.
// NOTE: some words like "and", "between", etc, are considered to be operator, and not keyword.
//
const (
	LEX_INVALID Lex_type_t = 0 // used only when an internal lexer function returns an error. It is only used internally by the lexer, and the caller never sees it.

	LEX_KEYWORD         Lex_type_t = 1 << iota // Lexeme Lex_type. Lex_subtypes are LEX_KEYWORD_ADD, etc.
	LEX_OPERATOR                               // Lexeme Lex_type. Lex_subtypes are LEX_OPERATOR_LOGICAL_AND, LEX_OPERATOR_PLUS, etc
	LEX_LPAREN                                 // Lexeme Lex_type. Lex_subtype is LEX_LPAREN_SUB.
	LEX_RPAREN                                 // Lexeme Lex_type. Lex_subtype is LEX_RPAREN_SUB.
	LEX_LCURLY                                 // Lexeme Lex_type. Lex_subtype is LEX_LCURLY_SUB.
	LEX_RCURLY                                 // Lexeme Lex_type. Lex_subtype is LEX_LCURLY_SUB.
	LEX_COMMA                                  // Lexeme Lex_type. Lex_subtype is LEX_COMMA_SUB.
	LEX_DOT                                    // Lexeme Lex_type. Lex_subtype is LEX_DOT_SUB.
	LEX_SEMICOLON                              // Lexeme Lex_type. Lex_subtype is LEX_SEMICOLON_SUB.
	LEX_COLON                                  // Lexeme Lex_type. Lex_subtype is LEX_COLON_SUB.
	LEX_QUESTIONMARK                           // Lexeme Lex_type. Lex_subtype is LEX_QUESTIONMARK_SUB.
	LEX_EXCLAMATIONMARK                        // Lexeme Lex_type. Lex_subtype is LEX_EXCLAMATIONMARK_SUB.
	LEX_START_OF_BATCH                         // Lexeme Lex_type. Lex_subtype is LEX_START_OF_BATCH_SUB.
	LEX_END_OF_BATCH                           // Lexeme Lex_type. Lex_subtype is LEX_END_OF_BATCH_SUB.
	LEX_DISPLAYWORD                            // Lexeme Lex_type. Lex_subtype is LEX_DISPLAYWORD_IS_NULL, etc. They are used to attach a lexeme to some tokens, just for display when displaying the AST tree (e.g. "in_list", "is_null").

	LEX_LITERAL_NUMBER     // Lexeme Lex_type. Lex_subtypes are LEX_LITERAL_NUMBER_INTEGRAL, etc.
	LEX_LITERAL_HEXASTRING // Lexeme Lex_type. Lex_subtype is LEX_LITERAL_HEXASTRING_SUB.
	LEX_LITERAL_STRING     // Lexeme Lex_type. Lex_subtype is LEX_LITERAL_STRING_SUB.
	LEX_VARIABLE           // Lexeme Lex_type. Lex_subtype is LEX_VARIABLE_SUB.
	LEX_ATFUNC             // Lexeme Lex_type. Lex_subtype is LEX_ATFUNC_SUB.
	LEX_IDENTPART          // Lexeme Lex_type. Lex_subtype is LEX_IDENTPART_SUB.
)

// All Lex_subtype_t constants.
// The values for each subtype don't overlap. This way, there is no need to compare also Lex_types when comparing Lex_subtypes.
//
const (
	LEX_INVALID_SUB Lex_subtype_t = 0 // used only when an internal lexer function returns an error. It is only used internally by the lexer, and the caller never sees it.

	LEX_KEYWORD_ASSERT_ Lex_subtype_t = iota + 1 // Lexeme Lex_subtype for Lex_type LEX_KEYWORD
	LEX_KEYWORD_ASSERT_NULL_
	LEX_KEYWORD_ASSERT_ERROR_
	LEX_KEYWORD_ADD
	LEX_KEYWORD_ALTER
	LEX_KEYWORD_AS
	LEX_KEYWORD_ASC
	LEX_KEYWORD_AUTHORIZATION
	LEX_KEYWORD_BACKUP
	LEX_KEYWORD_BEGIN
	LEX_KEYWORD_BREAK
	LEX_KEYWORD_BROWSE
	LEX_KEYWORD_BULK
	LEX_KEYWORD_BY
	LEX_KEYWORD_CASCADE
	LEX_KEYWORD_CASE
	LEX_KEYWORD_CAST
	LEX_KEYWORD_CHECK
	LEX_KEYWORD_CHECKPOINT
	LEX_KEYWORD_CLOSE
	LEX_KEYWORD_CLUSTERED
	// COALESCE        no need to have it as a keyword. It is a normal sysfunc.
	LEX_KEYWORD_COLLATE
	LEX_KEYWORD_COLUMN
	LEX_KEYWORD_COMMIT
	LEX_KEYWORD_COMPUTE
	LEX_KEYWORD_CONSTRAINT
	LEX_KEYWORD_CONTAINS
	LEX_KEYWORD_CONTAINSTABLE
	LEX_KEYWORD_CONTINUE
	LEX_KEYWORD_CONVERT
	LEX_KEYWORD_CREATE
	LEX_KEYWORD_CROSS
	LEX_KEYWORD_CURRENT
	LEX_KEYWORD_CURRENT_TIMESTAMP // sysfunc without parentheses
	LEX_KEYWORD_CURRENT_USER      // sysfunc without parentheses
	LEX_KEYWORD_CURSOR
	LEX_KEYWORD_DATABASE
	LEX_KEYWORD_DBCC
	LEX_KEYWORD_DEALLOCATE
	LEX_KEYWORD_DEBUG // I add this keyword for rsql. It is not a MS SQL Server keyword.
	LEX_KEYWORD_DECLARE
	LEX_KEYWORD_DEFAULT
	LEX_KEYWORD_DELETE
	LEX_KEYWORD_DENY
	LEX_KEYWORD_DESC
	LEX_KEYWORD_DISK
	LEX_KEYWORD_DISTINCT
	LEX_KEYWORD_DISTRIBUTED
	// DOUBLE     I don't put it in keyword, because I want it in specialword as datatype.
	LEX_KEYWORD_DROP
	LEX_KEYWORD_DUMP
	LEX_KEYWORD_ELSE
	LEX_KEYWORD_END
	LEX_KEYWORD_ERRLVL
	LEX_KEYWORD_EXCEPT
	LEX_KEYWORD_EXEC
	LEX_KEYWORD_EXECUTE
	LEX_KEYWORD_EXISTS
	LEX_KEYWORD_EXIT
	LEX_KEYWORD_EXTERNAL
	LEX_KEYWORD_FALSE // I add this keyword for rsql. It is not a MS SQL Server keyword.
	LEX_KEYWORD_FETCH
	LEX_KEYWORD_FILE
	LEX_KEYWORD_FILLFACTOR
	LEX_KEYWORD_FOR
	LEX_KEYWORD_FOREIGN
	LEX_KEYWORD_FREETEXT
	LEX_KEYWORD_FREETEXTTABLE
	LEX_KEYWORD_FROM
	LEX_KEYWORD_FULL
	LEX_KEYWORD_FUNCTION
	LEX_KEYWORD_GOTO
	LEX_KEYWORD_GRANT
	LEX_KEYWORD_GROUP
	LEX_KEYWORD_HAVING
	LEX_KEYWORD_HOLDLOCK
	LEX_KEYWORD_IDENTITY
	LEX_KEYWORD_IDENTITY_INSERT
	LEX_KEYWORD_IDENTITYCOL
	LEX_KEYWORD_IF
	LEX_KEYWORD_INDEX
	LEX_KEYWORD_INNER
	LEX_KEYWORD_INSERT
	LEX_KEYWORD_INTERSECT
	LEX_KEYWORD_INTO
	LEX_KEYWORD_JOIN
	LEX_KEYWORD_KEY
	LEX_KEYWORD_KILL
	LEX_KEYWORD_LEFT // for LEFT JOIN, but is also the sysfunc LEFT()
	LEX_KEYWORD_LINENO
	LEX_KEYWORD_LOAD
	LEX_KEYWORD_NATIONAL
	LEX_KEYWORD_NOCHECK
	LEX_KEYWORD_NONCLUSTERED
	LEX_KEYWORD_NULL
	LEX_KEYWORD_OF
	LEX_KEYWORD_OFF
	LEX_KEYWORD_OFFSETS
	LEX_KEYWORD_ON
	LEX_KEYWORD_OPEN
	LEX_KEYWORD_OPENDATASOURCE
	LEX_KEYWORD_OPENQUERY
	LEX_KEYWORD_OPENROWSET
	LEX_KEYWORD_OPENXML
	LEX_KEYWORD_OPTION
	LEX_KEYWORD_ORDER
	LEX_KEYWORD_OUTER
	LEX_KEYWORD_OVER
	LEX_KEYWORD_PERCENT
	LEX_KEYWORD_PIVOT
	LEX_KEYWORD_PLAN
	LEX_KEYWORD_PRECISION
	LEX_KEYWORD_PRIMARY
	LEX_KEYWORD_PRINT
	LEX_KEYWORD_PROC
	LEX_KEYWORD_PROCEDURE
	// LEX_KEYWORD_PUBLIC // I want it to be a normal group name, which is an identifier
	LEX_KEYWORD_RAISERROR
	LEX_KEYWORD_READ
	LEX_KEYWORD_READTEXT
	LEX_KEYWORD_RECONFIGURE
	LEX_KEYWORD_REFERENCES
	LEX_KEYWORD_REPLICATION
	LEX_KEYWORD_RESTORE
	LEX_KEYWORD_RESTRICT
	LEX_KEYWORD_RETURN
	LEX_KEYWORD_REVERT
	LEX_KEYWORD_REVOKE
	LEX_KEYWORD_RIGHT // for RIGHT JOIN, but is also the sysfunc RIGHT()
	LEX_KEYWORD_ROLE  // I add this keyword for rsql. It is not a MS SQL Server keyword.
	LEX_KEYWORD_ROLLBACK
	LEX_KEYWORD_ROWCOUNT
	LEX_KEYWORD_ROWGUIDCOL
	LEX_KEYWORD_RULE
	LEX_KEYWORD_SAVE
	LEX_KEYWORD_SCHEMA
	LEX_KEYWORD_SECURITYAUDIT
	LEX_KEYWORD_SELECT
	LEX_KEYWORD_SESSION_USER // sysfunc without parentheses
	LEX_KEYWORD_SET
	LEX_KEYWORD_SETUSER
	LEX_KEYWORD_SHOW   // I add this keyword for rsql. It is not a MS SQL Server keyword.
	LEX_KEYWORD_SLEEP  // I add this keyword for rsql. It is not a MS SQL Server keyword.
	LEX_KEYWORD_SHRINK // I add this keyword for rsql. It is not a MS SQL Server keyword.
	LEX_KEYWORD_SHUTDOWN
	LEX_KEYWORD_STATISTICS
	LEX_KEYWORD_SYSTEM_USER // sysfunc without parentheses
	LEX_KEYWORD_TABLE
	LEX_KEYWORD_TABLESAMPLE
	LEX_KEYWORD_TEXTSIZE
	LEX_KEYWORD_THEN
	LEX_KEYWORD_THROW
	LEX_KEYWORD_TO
	LEX_KEYWORD_TOP
	LEX_KEYWORD_TRAN
	LEX_KEYWORD_TRANSACTION
	LEX_KEYWORD_TRIGGER
	LEX_KEYWORD_TRUE // I add this keyword for rsql. It is not a MS SQL Server keyword.
	LEX_KEYWORD_TRUNCATE
	LEX_KEYWORD_TSEQUAL
	LEX_KEYWORD_UNION
	LEX_KEYWORD_UNIQUE
	LEX_KEYWORD_UNPIVOT
	LEX_KEYWORD_UPDATE
	LEX_KEYWORD_UPDATETEXT
	LEX_KEYWORD_USE
	LEX_KEYWORD_USER // sysfunc without parentheses
	LEX_KEYWORD_VALUES
	LEX_KEYWORD_VARYING
	LEX_KEYWORD_VIEW
	LEX_KEYWORD_WAITFOR
	LEX_KEYWORD_WHEN
	LEX_KEYWORD_WHERE
	LEX_KEYWORD_WHILE
	LEX_KEYWORD_WITH
	LEX_KEYWORD_WRITETEXT

	LEX_OPERATOR_LOGICAL_AND
	LEX_OPERATOR_LOGICAL_OR
	LEX_OPERATOR_LOGICAL_NOT
	LEX_OPERATOR_IS
	LEX_OPERATOR_LIKE
	LEX_OPERATOR_ESCAPE // pseudo-operator with no precedence, used in combination with LIKE operator
	LEX_OPERATOR_BETWEEN
	LEX_OPERATOR_IN
	LEX_OPERATOR_SOME
	LEX_OPERATOR_ANY
	LEX_OPERATOR_ALL

	LEX_OPERATOR_PLUS
	LEX_OPERATOR_MINUS
	LEX_OPERATOR_MULT
	LEX_OPERATOR_DIV
	LEX_OPERATOR_MOD
	LEX_OPERATOR_BIT_AND
	LEX_OPERATOR_BIT_OR
	LEX_OPERATOR_BIT_XOR
	LEX_OPERATOR_BIT_NOT
	LEX_OPERATOR_COMP_EQUAL
	LEX_OPERATOR_COMP_GREATER
	LEX_OPERATOR_COMP_LESS
	LEX_OPERATOR_COMP_GREATER_EQUAL
	LEX_OPERATOR_COMP_LESS_EQUAL
	LEX_OPERATOR_COMP_NOT_EQUAL
	LEX_OPERATOR_ASSIGN_PLUS
	LEX_OPERATOR_ASSIGN_MINUS
	LEX_OPERATOR_ASSIGN_MULT
	LEX_OPERATOR_ASSIGN_DIV
	LEX_OPERATOR_ASSIGN_MOD
	LEX_OPERATOR_ASSIGN_BIT_AND
	LEX_OPERATOR_ASSIGN_BIT_OR
	LEX_OPERATOR_ASSIGN_BIT_XOR

	LEX_LPAREN_SUB          // Lexeme Lex_subtype for Lex_type LEX_LPAREN
	LEX_RPAREN_SUB          // Lexeme Lex_subtype for Lex_type LEX_RPAREN
	LEX_LCURLY_SUB          // Lexeme Lex_subtype for Lex_type LEX_LCURLY
	LEX_RCURLY_SUB          // Lexeme Lex_subtype for Lex_type LEX_RCURLY
	LEX_COMMA_SUB           // Lexeme Lex_subtype for Lex_type LEX_COMMA
	LEX_DOT_SUB             // Lexeme Lex_subtype for Lex_type LEX_DOT
	LEX_SEMICOLON_SUB       // Lexeme Lex_subtype for Lex_type LEX_SEMICOLON
	LEX_COLON_SUB           // Lexeme Lex_subtype for Lex_type LEX_COLON
	LEX_QUESTIONMARK_SUB    // Lexeme Lex_subtype for Lex_type LEX_QUESTIONMARK
	LEX_EXCLAMATIONMARK_SUB // Lexeme Lex_subtype for Lex_type LEX_EXCLAMATIONMARK
	LEX_START_OF_BATCH_SUB  // Lexeme Lex_subtype for Lex_type LEX_START_OF_BATCH
	LEX_END_OF_BATCH_SUB    // Lexeme Lex_subtype for Lex_type LEX_END_OF_BATCH

	LEX_DISPLAYWORD_EMPTY_STRING                  // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_IS_NULL                       // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_IS_NOT_NULL                   // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_IN_LIST                       // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_NOT_IN_LIST                   // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_NOT_LIKE                      // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_NOT_BETWEEN                   // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_SUBQUERY_ONE                  // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_SUBQUERY_MANY                 // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_TRUE                          // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_FALSE                         // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_FIRSTDAYOFWEEK                // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_DMY                           // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_UTC_TO_LOCAL                  // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_COMPILE_VARCHAR_TO_REGEXPLIKE // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_FIT_NO_TRUNCATE_VARBINARY     // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD
	LEX_DISPLAYWORD_FIT_NO_TRUNCATE_VARCHAR       // Lexeme Lex_subtype for Lex_type LEX_DISPLAYWORD

	LEX_LITERAL_HEXASTRING_SUB  // Lexeme Lex_subtype for Lex_type LEX_LITERAL_HEXASTRING. It will be converted to VARBINARY by the decorator.
	LEX_LITERAL_STRING_SUB      // Lexeme Lex_subtype for Lex_type LEX_LITERAL_STRING. It will be converted to CHAR or VARCHAR by the decorator.
	LEX_LITERAL_NUMBER_INTEGRAL // Lexeme Lex_subtype for Lex_type LEX_LITERAL_NUMBER. It will be converted to BIT, TINYINT, SMALLINT, INT, BIGINT, or NUMERIC by the decorator.
	LEX_LITERAL_NUMBER_MONEY    // Lexeme Lex_subtype for Lex_type LEX_LITERAL_NUMBER. It will be converted to MONEY by the decorator.
	LEX_LITERAL_NUMBER_NUMERIC  // Lexeme Lex_subtype for Lex_type LEX_LITERAL_NUMBER. It will be converted to NUMERIC by the decorator.
	LEX_LITERAL_NUMBER_FLOAT    // Lexeme Lex_subtype for Lex_type LEX_LITERAL_NUMBER. It will be converted to FLOAT by the decorator.
	LEX_VARIABLE_SUB            // Lexeme Lex_subtype for Lex_type LEX_VARIABLE
	LEX_ATFUNC_SUB              // Lexeme Lex_subtype for Lex_type LEX_ATFUNC
	LEX_IDENTPART_SUB           // Lexeme Lex_subtype for Lex_type LEX_IDENTPART

	aLL_LEXEMES_ARRAY_SIZE // NOTE: This is not a Lex_subtype, but just the highest iota value used so far. It is used as estimated size of aLL_LEXEMES_ARRAY.
)

// G_KEYWORDS_OPERATORS map contains all keywords, and also operators like "and", "between", etc, in lowercase.
// Each string key stores the corresponding Lexeme.
//     E.g. "select" -> LEXEME_KEYWORD_SELECT
//          "and"    -> LEXEME_OPERATOR_LOGICAL_AND
//
var G_KEYWORDS_OPERATORS = make(map[string]Lexeme, aLL_LEXEMES_ARRAY_SIZE) // in fact, the map will contain less than aLL_LEXEMES_ARRAY_SIZE elements, but it is a good approximation

// Constant Lexemes (for keywords, operators, punctuation, etc)
//
var (
	LEXEME_INVALID Lexeme = Lexeme{} // it contains zero values: Lexeme{0, 0, ""}. It is only used internally by the lexer, and the caller never sees it, except in l.Current_lexeme_original for non identpart lexemes.

	LEXEME_KEYWORD_ASSERT_           Lexeme
	LEXEME_KEYWORD_ASSERT_NULL_      Lexeme
	LEXEME_KEYWORD_ASSERT_ERROR_     Lexeme
	LEXEME_KEYWORD_ADD               Lexeme
	LEXEME_KEYWORD_ALTER             Lexeme
	LEXEME_KEYWORD_AS                Lexeme
	LEXEME_KEYWORD_ASC               Lexeme
	LEXEME_KEYWORD_AUTHORIZATION     Lexeme
	LEXEME_KEYWORD_BACKUP            Lexeme
	LEXEME_KEYWORD_BEGIN             Lexeme
	LEXEME_KEYWORD_BREAK             Lexeme
	LEXEME_KEYWORD_BROWSE            Lexeme
	LEXEME_KEYWORD_BULK              Lexeme
	LEXEME_KEYWORD_BY                Lexeme
	LEXEME_KEYWORD_CASCADE           Lexeme
	LEXEME_KEYWORD_CASE              Lexeme
	LEXEME_KEYWORD_CAST              Lexeme
	LEXEME_KEYWORD_CHECK             Lexeme
	LEXEME_KEYWORD_CHECKPOINT        Lexeme
	LEXEME_KEYWORD_CLOSE             Lexeme
	LEXEME_KEYWORD_CLUSTERED         Lexeme
	LEXEME_KEYWORD_COLLATE           Lexeme
	LEXEME_KEYWORD_COLUMN            Lexeme
	LEXEME_KEYWORD_COMMIT            Lexeme
	LEXEME_KEYWORD_COMPUTE           Lexeme
	LEXEME_KEYWORD_CONSTRAINT        Lexeme
	LEXEME_KEYWORD_CONTAINS          Lexeme
	LEXEME_KEYWORD_CONTAINSTABLE     Lexeme
	LEXEME_KEYWORD_CONTINUE          Lexeme
	LEXEME_KEYWORD_CONVERT           Lexeme
	LEXEME_KEYWORD_CREATE            Lexeme
	LEXEME_KEYWORD_CROSS             Lexeme
	LEXEME_KEYWORD_CURRENT           Lexeme
	LEXEME_KEYWORD_CURRENT_TIMESTAMP Lexeme
	LEXEME_KEYWORD_CURRENT_USER      Lexeme
	LEXEME_KEYWORD_CURSOR            Lexeme
	LEXEME_KEYWORD_DATABASE          Lexeme
	LEXEME_KEYWORD_DBCC              Lexeme
	LEXEME_KEYWORD_DEALLOCATE        Lexeme
	LEXEME_KEYWORD_DEBUG             Lexeme
	LEXEME_KEYWORD_DECLARE           Lexeme
	LEXEME_KEYWORD_DEFAULT           Lexeme
	LEXEME_KEYWORD_DELETE            Lexeme
	LEXEME_KEYWORD_DENY              Lexeme
	LEXEME_KEYWORD_DESC              Lexeme
	LEXEME_KEYWORD_DISK              Lexeme
	LEXEME_KEYWORD_DISTINCT          Lexeme
	LEXEME_KEYWORD_DISTRIBUTED       Lexeme
	LEXEME_KEYWORD_DROP              Lexeme
	LEXEME_KEYWORD_DUMP              Lexeme
	LEXEME_KEYWORD_ELSE              Lexeme
	LEXEME_KEYWORD_END               Lexeme
	LEXEME_KEYWORD_ERRLVL            Lexeme
	LEXEME_KEYWORD_EXCEPT            Lexeme
	LEXEME_KEYWORD_EXEC              Lexeme
	LEXEME_KEYWORD_EXECUTE           Lexeme
	LEXEME_KEYWORD_EXISTS            Lexeme
	LEXEME_KEYWORD_EXIT              Lexeme
	LEXEME_KEYWORD_EXTERNAL          Lexeme
	LEXEME_KEYWORD_FALSE             Lexeme
	LEXEME_KEYWORD_FETCH             Lexeme
	LEXEME_KEYWORD_FILE              Lexeme
	LEXEME_KEYWORD_FILLFACTOR        Lexeme
	LEXEME_KEYWORD_FOR               Lexeme
	LEXEME_KEYWORD_FOREIGN           Lexeme
	LEXEME_KEYWORD_FREETEXT          Lexeme
	LEXEME_KEYWORD_FREETEXTTABLE     Lexeme
	LEXEME_KEYWORD_FROM              Lexeme
	LEXEME_KEYWORD_FULL              Lexeme
	LEXEME_KEYWORD_FUNCTION          Lexeme
	LEXEME_KEYWORD_GOTO              Lexeme
	LEXEME_KEYWORD_GRANT             Lexeme
	LEXEME_KEYWORD_GROUP             Lexeme
	LEXEME_KEYWORD_HAVING            Lexeme
	LEXEME_KEYWORD_HOLDLOCK          Lexeme
	LEXEME_KEYWORD_IDENTITY          Lexeme
	LEXEME_KEYWORD_IDENTITY_INSERT   Lexeme
	LEXEME_KEYWORD_IDENTITYCOL       Lexeme
	LEXEME_KEYWORD_IF                Lexeme
	LEXEME_KEYWORD_INDEX             Lexeme
	LEXEME_KEYWORD_INNER             Lexeme
	LEXEME_KEYWORD_INSERT            Lexeme
	LEXEME_KEYWORD_INTERSECT         Lexeme
	LEXEME_KEYWORD_INTO              Lexeme
	LEXEME_KEYWORD_JOIN              Lexeme
	LEXEME_KEYWORD_KEY               Lexeme
	LEXEME_KEYWORD_KILL              Lexeme
	LEXEME_KEYWORD_LEFT              Lexeme
	LEXEME_KEYWORD_LINENO            Lexeme
	LEXEME_KEYWORD_LOAD              Lexeme
	LEXEME_KEYWORD_NATIONAL          Lexeme
	LEXEME_KEYWORD_NOCHECK           Lexeme
	LEXEME_KEYWORD_NONCLUSTERED      Lexeme
	LEXEME_KEYWORD_NULL              Lexeme
	LEXEME_KEYWORD_OF                Lexeme
	LEXEME_KEYWORD_OFF               Lexeme
	LEXEME_KEYWORD_OFFSETS           Lexeme
	LEXEME_KEYWORD_ON                Lexeme
	LEXEME_KEYWORD_OPEN              Lexeme
	LEXEME_KEYWORD_OPENDATASOURCE    Lexeme
	LEXEME_KEYWORD_OPENQUERY         Lexeme
	LEXEME_KEYWORD_OPENROWSET        Lexeme
	LEXEME_KEYWORD_OPENXML           Lexeme
	LEXEME_KEYWORD_OPTION            Lexeme
	LEXEME_KEYWORD_ORDER             Lexeme
	LEXEME_KEYWORD_OUTER             Lexeme
	LEXEME_KEYWORD_OVER              Lexeme
	LEXEME_KEYWORD_PERCENT           Lexeme
	LEXEME_KEYWORD_PIVOT             Lexeme
	LEXEME_KEYWORD_PLAN              Lexeme
	LEXEME_KEYWORD_PRECISION         Lexeme
	LEXEME_KEYWORD_PRIMARY           Lexeme
	LEXEME_KEYWORD_PRINT             Lexeme
	LEXEME_KEYWORD_PROC              Lexeme
	LEXEME_KEYWORD_PROCEDURE         Lexeme
	LEXEME_KEYWORD_RAISERROR         Lexeme
	LEXEME_KEYWORD_READ              Lexeme
	LEXEME_KEYWORD_READTEXT          Lexeme
	LEXEME_KEYWORD_RECONFIGURE       Lexeme
	LEXEME_KEYWORD_REFERENCES        Lexeme
	LEXEME_KEYWORD_REPLICATION       Lexeme
	LEXEME_KEYWORD_RESTORE           Lexeme
	LEXEME_KEYWORD_RESTRICT          Lexeme
	LEXEME_KEYWORD_RETURN            Lexeme
	LEXEME_KEYWORD_REVERT            Lexeme
	LEXEME_KEYWORD_REVOKE            Lexeme
	LEXEME_KEYWORD_RIGHT             Lexeme
	LEXEME_KEYWORD_ROLE              Lexeme
	LEXEME_KEYWORD_ROLLBACK          Lexeme
	LEXEME_KEYWORD_ROWCOUNT          Lexeme
	LEXEME_KEYWORD_ROWGUIDCOL        Lexeme
	LEXEME_KEYWORD_RULE              Lexeme
	LEXEME_KEYWORD_SAVE              Lexeme
	LEXEME_KEYWORD_SCHEMA            Lexeme
	LEXEME_KEYWORD_SECURITYAUDIT     Lexeme
	LEXEME_KEYWORD_SELECT            Lexeme
	LEXEME_KEYWORD_SESSION_USER      Lexeme
	LEXEME_KEYWORD_SET               Lexeme
	LEXEME_KEYWORD_SETUSER           Lexeme
	LEXEME_KEYWORD_SHOW              Lexeme
	LEXEME_KEYWORD_SLEEP             Lexeme
	LEXEME_KEYWORD_SHRINK            Lexeme
	LEXEME_KEYWORD_SHUTDOWN          Lexeme
	LEXEME_KEYWORD_STATISTICS        Lexeme
	LEXEME_KEYWORD_SYSTEM_USER       Lexeme
	LEXEME_KEYWORD_TABLE             Lexeme
	LEXEME_KEYWORD_TABLESAMPLE       Lexeme
	LEXEME_KEYWORD_TEXTSIZE          Lexeme
	LEXEME_KEYWORD_THEN              Lexeme
	LEXEME_KEYWORD_THROW             Lexeme
	LEXEME_KEYWORD_TO                Lexeme
	LEXEME_KEYWORD_TOP               Lexeme
	LEXEME_KEYWORD_TRAN              Lexeme
	LEXEME_KEYWORD_TRANSACTION       Lexeme
	LEXEME_KEYWORD_TRIGGER           Lexeme
	LEXEME_KEYWORD_TRUE              Lexeme
	LEXEME_KEYWORD_TRUNCATE          Lexeme
	LEXEME_KEYWORD_TSEQUAL           Lexeme
	LEXEME_KEYWORD_UNION             Lexeme
	LEXEME_KEYWORD_UNIQUE            Lexeme
	LEXEME_KEYWORD_UNPIVOT           Lexeme
	LEXEME_KEYWORD_UPDATE            Lexeme
	LEXEME_KEYWORD_UPDATETEXT        Lexeme
	LEXEME_KEYWORD_USE               Lexeme
	LEXEME_KEYWORD_USER              Lexeme
	LEXEME_KEYWORD_VALUES            Lexeme
	LEXEME_KEYWORD_VARYING           Lexeme
	LEXEME_KEYWORD_VIEW              Lexeme
	LEXEME_KEYWORD_WAITFOR           Lexeme
	LEXEME_KEYWORD_WHEN              Lexeme
	LEXEME_KEYWORD_WHERE             Lexeme
	LEXEME_KEYWORD_WHILE             Lexeme
	LEXEME_KEYWORD_WITH              Lexeme
	LEXEME_KEYWORD_WRITETEXT         Lexeme

	LEXEME_OPERATOR_LOGICAL_AND Lexeme
	LEXEME_OPERATOR_LOGICAL_OR  Lexeme
	LEXEME_OPERATOR_LOGICAL_NOT Lexeme
	LEXEME_OPERATOR_IS          Lexeme
	LEXEME_OPERATOR_LIKE        Lexeme
	LEXEME_OPERATOR_ESCAPE      Lexeme
	LEXEME_OPERATOR_BETWEEN     Lexeme
	LEXEME_OPERATOR_IN          Lexeme
	LEXEME_OPERATOR_SOME        Lexeme
	LEXEME_OPERATOR_ANY         Lexeme
	LEXEME_OPERATOR_ALL         Lexeme

	LEXEME_OPERATOR_PLUS               Lexeme
	LEXEME_OPERATOR_MINUS              Lexeme
	LEXEME_OPERATOR_MULT               Lexeme
	LEXEME_OPERATOR_DIV                Lexeme
	LEXEME_OPERATOR_MOD                Lexeme
	LEXEME_OPERATOR_BIT_AND            Lexeme
	LEXEME_OPERATOR_BIT_OR             Lexeme
	LEXEME_OPERATOR_BIT_XOR            Lexeme
	LEXEME_OPERATOR_BIT_NOT            Lexeme
	LEXEME_OPERATOR_COMP_EQUAL         Lexeme
	LEXEME_OPERATOR_COMP_GREATER       Lexeme
	LEXEME_OPERATOR_COMP_LESS          Lexeme
	LEXEME_OPERATOR_COMP_GREATER_EQUAL Lexeme
	LEXEME_OPERATOR_COMP_LESS_EQUAL    Lexeme
	LEXEME_OPERATOR_COMP_NOT_EQUAL     Lexeme
	LEXEME_OPERATOR_ASSIGN_PLUS        Lexeme
	LEXEME_OPERATOR_ASSIGN_MINUS       Lexeme
	LEXEME_OPERATOR_ASSIGN_MULT        Lexeme
	LEXEME_OPERATOR_ASSIGN_DIV         Lexeme
	LEXEME_OPERATOR_ASSIGN_MOD         Lexeme
	LEXEME_OPERATOR_ASSIGN_BIT_AND     Lexeme
	LEXEME_OPERATOR_ASSIGN_BIT_OR      Lexeme
	LEXEME_OPERATOR_ASSIGN_BIT_XOR     Lexeme

	LEXEME_LPAREN          Lexeme
	LEXEME_RPAREN          Lexeme
	LEXEME_LCURLY          Lexeme
	LEXEME_RCURLY          Lexeme
	LEXEME_COMMA           Lexeme
	LEXEME_DOT             Lexeme
	LEXEME_SEMICOLON       Lexeme
	LEXEME_COLON           Lexeme
	LEXEME_QUESTIONMARK    Lexeme
	LEXEME_EXCLAMATIONMARK Lexeme
	LEXEME_START_OF_BATCH  Lexeme
	LEXEME_END_OF_BATCH    Lexeme

	LEXEME_DISPLAYWORD_EMPTY_STRING                  Lexeme
	LEXEME_DISPLAYWORD_IS_NULL                       Lexeme
	LEXEME_DISPLAYWORD_IS_NOT_NULL                   Lexeme
	LEXEME_DISPLAYWORD_IN_LIST                       Lexeme
	LEXEME_DISPLAYWORD_NOT_IN_LIST                   Lexeme
	LEXEME_DISPLAYWORD_NOT_LIKE                      Lexeme
	LEXEME_DISPLAYWORD_NOT_BETWEEN                   Lexeme
	LEXEME_DISPLAYWORD_SUBQUERY_ONE                  Lexeme
	LEXEME_DISPLAYWORD_SUBQUERY_MANY                 Lexeme
	LEXEME_DISPLAYWORD_TRUE                          Lexeme
	LEXEME_DISPLAYWORD_FALSE                         Lexeme
	LEXEME_DISPLAYWORD_FIRSTDAYOFWEEK                Lexeme
	LEXEME_DISPLAYWORD_DMY                           Lexeme
	LEXEME_DISPLAYWORD_UTC_TO_LOCAL                  Lexeme
	LEXEME_DISPLAYWORD_COMPILE_VARCHAR_TO_REGEXPLIKE Lexeme
	LEXEME_DISPLAYWORD_FIT_NO_TRUNCATE_VARBINARY     Lexeme
	LEXEME_DISPLAYWORD_FIT_NO_TRUNCATE_VARCHAR       Lexeme

	LEXEME_SYSVAR_CURRENT_LANGUAGE    Lexeme
	LEXEME_SYSVAR_SYSTEM_USER_NAME    Lexeme
	LEXEME_SYSVAR_SYSTEM_USER_ID      Lexeme
	LEXEME_SYSVAR_CURRENT_DB_NAME     Lexeme
	LEXEME_SYSVAR_CURRENT_DB_ID       Lexeme
	LEXEME_SYSVAR_CURRENT_SCHEMA_NAME Lexeme
	LEXEME_SYSVAR_CURRENT_SCHEMA_ID   Lexeme
	LEXEME_SYSVAR_CURRENT_USER_NAME   Lexeme
	LEXEME_SYSVAR_CURRENT_USER_ID     Lexeme
	LEXEME_SYSVAR_CURRENT_TIMESTAMP   Lexeme
)

// init_contains_only_lowercase_ascii is a helper function for init().
// It checks that string contains only lowercase a...z letters, but no digits, underscore, or other character.
//
func init_contains_only_lowercase_a_z_letters(s string) bool {

	for _, c := range s {
		if !(c >= 'a' && c <= 'z') {
			return false
		}
	}

	return true
}

// init_create_Lexeme is a helper function for init().
// It also puts the lexeme in G_KEYWORDS_OPERATORS map.
//
func init_create_Lexeme(lex_type Lex_type_t, lex_subtype Lex_subtype_t, word_lc string) Lexeme {

	// check that word_lc is in lowercase
	for word_lc != strings.ToLower(word_lc) {
		panic("lex.init(): lexeme \"" + word_lc + "\" is not in lowercase")
	}

	// create Lexeme element

	lexeme := Lexeme{Lex_type: lex_type, Lex_subtype: lex_subtype, Lex_word: word_lc}

	// put Lexeme entry in G_KEYWORDS_OPERATORS map, only for keywords and operators made of letters ("like", "and", etc, and not "+", "/", etc)
	if lex_type == LEX_KEYWORD || (lex_type == LEX_OPERATOR && init_contains_only_lowercase_a_z_letters(word_lc)) {
		G_KEYWORDS_OPERATORS[word_lc] = lexeme
	}

	return lexeme
}

// init initializes G_KEYWORDS_OPERATORS map, and also all LEXEME_xxx constants.
//
func init() {
	LEXEME_KEYWORD_ASSERT_ = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ASSERT_, "_assert_")
	LEXEME_KEYWORD_ASSERT_NULL_ = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ASSERT_NULL_, "_assert_null_")
	LEXEME_KEYWORD_ASSERT_ERROR_ = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ASSERT_ERROR_, "_assert_error_")
	LEXEME_KEYWORD_ADD = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ADD, "add")
	LEXEME_KEYWORD_ALTER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ALTER, "alter")
	LEXEME_KEYWORD_AS = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_AS, "as")
	LEXEME_KEYWORD_ASC = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ASC, "asc")
	LEXEME_KEYWORD_AUTHORIZATION = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_AUTHORIZATION, "authorization")
	LEXEME_KEYWORD_BACKUP = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_BACKUP, "backup")
	LEXEME_KEYWORD_BEGIN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_BEGIN, "begin")
	LEXEME_KEYWORD_BREAK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_BREAK, "break")
	LEXEME_KEYWORD_BROWSE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_BROWSE, "browse")
	LEXEME_KEYWORD_BULK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_BULK, "bulk")
	LEXEME_KEYWORD_BY = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_BY, "by")
	LEXEME_KEYWORD_CASCADE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CASCADE, "cascade")
	LEXEME_KEYWORD_CASE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CASE, "case")
	LEXEME_KEYWORD_CAST = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CAST, "cast")
	LEXEME_KEYWORD_CHECK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CHECK, "check")
	LEXEME_KEYWORD_CHECKPOINT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CHECKPOINT, "checkpoint")
	LEXEME_KEYWORD_CLOSE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CLOSE, "close")
	LEXEME_KEYWORD_CLUSTERED = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CLUSTERED, "clustered")
	LEXEME_KEYWORD_COLLATE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_COLLATE, "collate")
	LEXEME_KEYWORD_COLUMN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_COLUMN, "column")
	LEXEME_KEYWORD_COMMIT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_COMMIT, "commit")
	LEXEME_KEYWORD_COMPUTE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_COMPUTE, "compute")
	LEXEME_KEYWORD_CONSTRAINT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CONSTRAINT, "constraint")
	LEXEME_KEYWORD_CONTAINS = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CONTAINS, "contains")
	LEXEME_KEYWORD_CONTAINSTABLE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CONTAINSTABLE, "containstable")
	LEXEME_KEYWORD_CONTINUE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CONTINUE, "continue")
	LEXEME_KEYWORD_CONVERT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CONVERT, "convert")
	LEXEME_KEYWORD_CREATE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CREATE, "create")
	LEXEME_KEYWORD_CROSS = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CROSS, "cross")
	LEXEME_KEYWORD_CURRENT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CURRENT, "current")
	LEXEME_KEYWORD_CURRENT_TIMESTAMP = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CURRENT_TIMESTAMP, "current_timestamp")
	LEXEME_KEYWORD_CURRENT_USER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CURRENT_USER, "current_user")
	LEXEME_KEYWORD_CURSOR = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_CURSOR, "cursor")
	LEXEME_KEYWORD_DATABASE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DATABASE, "database")
	LEXEME_KEYWORD_DBCC = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DBCC, "dbcc")
	LEXEME_KEYWORD_DEALLOCATE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DEALLOCATE, "deallocate")
	LEXEME_KEYWORD_DEBUG = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DEBUG, "debug")
	LEXEME_KEYWORD_DECLARE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DECLARE, "declare")
	LEXEME_KEYWORD_DEFAULT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DEFAULT, "default")
	LEXEME_KEYWORD_DELETE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DELETE, "delete")
	LEXEME_KEYWORD_DENY = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DENY, "deny")
	LEXEME_KEYWORD_DESC = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DESC, "desc")
	LEXEME_KEYWORD_DISK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DISK, "disk")
	LEXEME_KEYWORD_DISTINCT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DISTINCT, "distinct")
	LEXEME_KEYWORD_DISTRIBUTED = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DISTRIBUTED, "distributed")
	LEXEME_KEYWORD_DROP = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DROP, "drop")
	LEXEME_KEYWORD_DUMP = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_DUMP, "dump")
	LEXEME_KEYWORD_ELSE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ELSE, "else")
	LEXEME_KEYWORD_END = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_END, "end")
	LEXEME_KEYWORD_ERRLVL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ERRLVL, "errlvl")
	LEXEME_KEYWORD_EXCEPT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_EXCEPT, "except")
	LEXEME_KEYWORD_EXEC = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_EXEC, "exec")
	LEXEME_KEYWORD_EXECUTE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_EXECUTE, "execute")
	LEXEME_KEYWORD_EXISTS = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_EXISTS, "exists")
	LEXEME_KEYWORD_EXIT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_EXIT, "exit")
	LEXEME_KEYWORD_EXTERNAL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_EXTERNAL, "external")
	LEXEME_KEYWORD_FALSE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FALSE, "false")
	LEXEME_KEYWORD_FETCH = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FETCH, "fetch")
	LEXEME_KEYWORD_FILE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FILE, "file")
	LEXEME_KEYWORD_FILLFACTOR = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FILLFACTOR, "fillfactor")
	LEXEME_KEYWORD_FOR = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FOR, "for")
	LEXEME_KEYWORD_FOREIGN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FOREIGN, "foreign")
	LEXEME_KEYWORD_FREETEXT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FREETEXT, "freetext")
	LEXEME_KEYWORD_FREETEXTTABLE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FREETEXTTABLE, "freetexttable")
	LEXEME_KEYWORD_FROM = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FROM, "from")
	LEXEME_KEYWORD_FULL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FULL, "full")
	LEXEME_KEYWORD_FUNCTION = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_FUNCTION, "function")
	LEXEME_KEYWORD_GOTO = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_GOTO, "goto")
	LEXEME_KEYWORD_GRANT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_GRANT, "grant")
	LEXEME_KEYWORD_GROUP = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_GROUP, "group")
	LEXEME_KEYWORD_HAVING = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_HAVING, "having")
	LEXEME_KEYWORD_HOLDLOCK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_HOLDLOCK, "holdlock")
	LEXEME_KEYWORD_IDENTITY = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_IDENTITY, "identity")
	LEXEME_KEYWORD_IDENTITY_INSERT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_IDENTITY_INSERT, "identity_insert")
	LEXEME_KEYWORD_IDENTITYCOL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_IDENTITYCOL, "identitycol")
	LEXEME_KEYWORD_IF = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_IF, "if")
	LEXEME_KEYWORD_INDEX = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_INDEX, "index")
	LEXEME_KEYWORD_INNER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_INNER, "inner")
	LEXEME_KEYWORD_INSERT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_INSERT, "insert")
	LEXEME_KEYWORD_INTERSECT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_INTERSECT, "intersect")
	LEXEME_KEYWORD_INTO = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_INTO, "into")
	LEXEME_KEYWORD_JOIN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_JOIN, "join")
	LEXEME_KEYWORD_KEY = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_KEY, "key")
	LEXEME_KEYWORD_KILL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_KILL, "kill")
	LEXEME_KEYWORD_LEFT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_LEFT, "left")
	LEXEME_KEYWORD_LINENO = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_LINENO, "lineno")
	LEXEME_KEYWORD_LOAD = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_LOAD, "load")
	LEXEME_KEYWORD_NATIONAL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_NATIONAL, "national")
	LEXEME_KEYWORD_NOCHECK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_NOCHECK, "nocheck")
	LEXEME_KEYWORD_NONCLUSTERED = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_NONCLUSTERED, "nonclustered")
	LEXEME_KEYWORD_NULL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_NULL, "null")
	LEXEME_KEYWORD_OF = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OF, "of")
	LEXEME_KEYWORD_OFF = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OFF, "off")
	LEXEME_KEYWORD_OFFSETS = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OFFSETS, "offsets")
	LEXEME_KEYWORD_ON = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ON, "on")
	LEXEME_KEYWORD_OPEN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OPEN, "open")
	LEXEME_KEYWORD_OPENDATASOURCE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OPENDATASOURCE, "opendatasource")
	LEXEME_KEYWORD_OPENQUERY = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OPENQUERY, "openquery")
	LEXEME_KEYWORD_OPENROWSET = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OPENROWSET, "openrowset")
	LEXEME_KEYWORD_OPENXML = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OPENXML, "openxml")
	LEXEME_KEYWORD_OPTION = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OPTION, "option")
	LEXEME_KEYWORD_ORDER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ORDER, "order")
	LEXEME_KEYWORD_OUTER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OUTER, "outer")
	LEXEME_KEYWORD_OVER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_OVER, "over")
	LEXEME_KEYWORD_PERCENT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PERCENT, "percent")
	LEXEME_KEYWORD_PIVOT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PIVOT, "pivot")
	LEXEME_KEYWORD_PLAN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PLAN, "plan")
	LEXEME_KEYWORD_PRECISION = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PRECISION, "precision")
	LEXEME_KEYWORD_PRIMARY = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PRIMARY, "primary")
	LEXEME_KEYWORD_PRINT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PRINT, "print")
	LEXEME_KEYWORD_PROC = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PROC, "proc")
	LEXEME_KEYWORD_PROCEDURE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_PROCEDURE, "procedure")
	LEXEME_KEYWORD_RAISERROR = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_RAISERROR, "raiserror")
	LEXEME_KEYWORD_READ = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_READ, "read")
	LEXEME_KEYWORD_READTEXT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_READTEXT, "readtext")
	LEXEME_KEYWORD_RECONFIGURE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_RECONFIGURE, "reconfigure")
	LEXEME_KEYWORD_REFERENCES = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_REFERENCES, "references")
	LEXEME_KEYWORD_REPLICATION = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_REPLICATION, "replication")
	LEXEME_KEYWORD_RESTORE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_RESTORE, "restore")
	LEXEME_KEYWORD_RESTRICT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_RESTRICT, "restrict")
	LEXEME_KEYWORD_RETURN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_RETURN, "return")
	LEXEME_KEYWORD_REVERT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_REVERT, "revert")
	LEXEME_KEYWORD_REVOKE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_REVOKE, "revoke")
	LEXEME_KEYWORD_RIGHT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_RIGHT, "right")
	LEXEME_KEYWORD_ROLE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ROLE, "role")
	LEXEME_KEYWORD_ROLLBACK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ROLLBACK, "rollback")
	LEXEME_KEYWORD_ROWCOUNT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ROWCOUNT, "rowcount")
	LEXEME_KEYWORD_ROWGUIDCOL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_ROWGUIDCOL, "rowguidcol")
	LEXEME_KEYWORD_RULE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_RULE, "rule")
	LEXEME_KEYWORD_SAVE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SAVE, "save")
	LEXEME_KEYWORD_SCHEMA = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SCHEMA, "schema")
	LEXEME_KEYWORD_SECURITYAUDIT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SECURITYAUDIT, "securityaudit")
	LEXEME_KEYWORD_SELECT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SELECT, "select")
	LEXEME_KEYWORD_SESSION_USER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SESSION_USER, "session_user")
	LEXEME_KEYWORD_SET = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SET, "set")
	LEXEME_KEYWORD_SETUSER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SETUSER, "setuser")
	LEXEME_KEYWORD_SHOW = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SHOW, "show")
	LEXEME_KEYWORD_SLEEP = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SLEEP, "sleep")
	LEXEME_KEYWORD_SHRINK = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SHRINK, "shrink")
	LEXEME_KEYWORD_SHUTDOWN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SHUTDOWN, "shutdown")
	LEXEME_KEYWORD_STATISTICS = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_STATISTICS, "statistics")
	LEXEME_KEYWORD_SYSTEM_USER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_SYSTEM_USER, "system_user")
	LEXEME_KEYWORD_TABLE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TABLE, "table")
	LEXEME_KEYWORD_TABLESAMPLE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TABLESAMPLE, "tablesample")
	LEXEME_KEYWORD_TEXTSIZE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TEXTSIZE, "textsize")
	LEXEME_KEYWORD_THEN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_THEN, "then")
	LEXEME_KEYWORD_THROW = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_THROW, "throw")
	LEXEME_KEYWORD_TO = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TO, "to")
	LEXEME_KEYWORD_TOP = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TOP, "top")
	LEXEME_KEYWORD_TRAN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TRAN, "tran")
	LEXEME_KEYWORD_TRANSACTION = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TRANSACTION, "transaction")
	LEXEME_KEYWORD_TRIGGER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TRIGGER, "trigger")
	LEXEME_KEYWORD_TRUE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TRUE, "true")
	LEXEME_KEYWORD_TRUNCATE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TRUNCATE, "truncate")
	LEXEME_KEYWORD_TSEQUAL = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_TSEQUAL, "tsequal")
	LEXEME_KEYWORD_UNION = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_UNION, "union")
	LEXEME_KEYWORD_UNIQUE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_UNIQUE, "unique")
	LEXEME_KEYWORD_UNPIVOT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_UNPIVOT, "unpivot")
	LEXEME_KEYWORD_UPDATE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_UPDATE, "update")
	LEXEME_KEYWORD_UPDATETEXT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_UPDATETEXT, "updatetext")
	LEXEME_KEYWORD_USE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_USE, "use")
	LEXEME_KEYWORD_USER = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_USER, "user")
	LEXEME_KEYWORD_VALUES = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_VALUES, "values")
	LEXEME_KEYWORD_VARYING = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_VARYING, "varying")
	LEXEME_KEYWORD_VIEW = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_VIEW, "view")
	LEXEME_KEYWORD_WAITFOR = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_WAITFOR, "waitfor")
	LEXEME_KEYWORD_WHEN = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_WHEN, "when")
	LEXEME_KEYWORD_WHERE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_WHERE, "where")
	LEXEME_KEYWORD_WHILE = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_WHILE, "while")
	LEXEME_KEYWORD_WITH = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_WITH, "with")
	LEXEME_KEYWORD_WRITETEXT = init_create_Lexeme(LEX_KEYWORD, LEX_KEYWORD_WRITETEXT, "writetext")

	LEXEME_OPERATOR_LOGICAL_AND = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_LOGICAL_AND, "and")
	LEXEME_OPERATOR_LOGICAL_OR = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_LOGICAL_OR, "or")
	LEXEME_OPERATOR_LOGICAL_NOT = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_LOGICAL_NOT, "not")
	LEXEME_OPERATOR_IS = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_IS, "is")
	LEXEME_OPERATOR_LIKE = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_LIKE, "like")
	LEXEME_OPERATOR_ESCAPE = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ESCAPE, "escape")
	LEXEME_OPERATOR_BETWEEN = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_BETWEEN, "between")
	LEXEME_OPERATOR_IN = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_IN, "in")
	LEXEME_OPERATOR_SOME = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_SOME, "some")
	LEXEME_OPERATOR_ANY = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ANY, "any")
	LEXEME_OPERATOR_ALL = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ALL, "all")

	LEXEME_OPERATOR_PLUS = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_PLUS, "+") // mathematic symbol operators are not put in G_KEYWORDS_OPERATORS
	LEXEME_OPERATOR_MINUS = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_MINUS, "-")
	LEXEME_OPERATOR_MULT = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_MULT, "*")
	LEXEME_OPERATOR_DIV = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_DIV, "/")
	LEXEME_OPERATOR_MOD = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_MOD, "%")
	LEXEME_OPERATOR_BIT_AND = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_BIT_AND, "&")
	LEXEME_OPERATOR_BIT_OR = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_BIT_OR, "|")
	LEXEME_OPERATOR_BIT_XOR = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_BIT_XOR, "^")
	LEXEME_OPERATOR_BIT_NOT = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_BIT_NOT, "~")
	LEXEME_OPERATOR_COMP_EQUAL = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_COMP_EQUAL, "=")
	LEXEME_OPERATOR_COMP_GREATER = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_COMP_GREATER, ">")
	LEXEME_OPERATOR_COMP_LESS = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_COMP_LESS, "<")
	LEXEME_OPERATOR_COMP_GREATER_EQUAL = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_COMP_GREATER_EQUAL, ">=")
	LEXEME_OPERATOR_COMP_LESS_EQUAL = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_COMP_LESS_EQUAL, "<=")
	LEXEME_OPERATOR_COMP_NOT_EQUAL = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_COMP_NOT_EQUAL, "<>")
	LEXEME_OPERATOR_ASSIGN_PLUS = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_PLUS, "+=")
	LEXEME_OPERATOR_ASSIGN_MINUS = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_MINUS, "-=")
	LEXEME_OPERATOR_ASSIGN_MULT = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_MULT, "*=")
	LEXEME_OPERATOR_ASSIGN_DIV = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_DIV, "/=")
	LEXEME_OPERATOR_ASSIGN_MOD = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_MOD, "%=")
	LEXEME_OPERATOR_ASSIGN_BIT_AND = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_BIT_AND, "&=")
	LEXEME_OPERATOR_ASSIGN_BIT_OR = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_BIT_OR, "|=")
	LEXEME_OPERATOR_ASSIGN_BIT_XOR = init_create_Lexeme(LEX_OPERATOR, LEX_OPERATOR_ASSIGN_BIT_XOR, "^=")

	LEXEME_LPAREN = init_create_Lexeme(LEX_LPAREN, LEX_LPAREN_SUB, "(") // punctuations, brackets, etc are not put in G_KEYWORDS_OPERATORS
	LEXEME_RPAREN = init_create_Lexeme(LEX_RPAREN, LEX_RPAREN_SUB, ")")
	LEXEME_LCURLY = init_create_Lexeme(LEX_LCURLY, LEX_LCURLY_SUB, "{")
	LEXEME_RCURLY = init_create_Lexeme(LEX_RCURLY, LEX_RCURLY_SUB, "}")
	LEXEME_COMMA = init_create_Lexeme(LEX_COMMA, LEX_COMMA_SUB, ",")
	LEXEME_DOT = init_create_Lexeme(LEX_DOT, LEX_DOT_SUB, ".")
	LEXEME_SEMICOLON = init_create_Lexeme(LEX_SEMICOLON, LEX_SEMICOLON_SUB, ";")
	LEXEME_COLON = init_create_Lexeme(LEX_COLON, LEX_COLON_SUB, ":")
	LEXEME_QUESTIONMARK = init_create_Lexeme(LEX_QUESTIONMARK, LEX_QUESTIONMARK_SUB, "?")
	LEXEME_EXCLAMATIONMARK = init_create_Lexeme(LEX_EXCLAMATIONMARK, LEX_EXCLAMATIONMARK_SUB, "!")
	LEXEME_START_OF_BATCH = init_create_Lexeme(LEX_START_OF_BATCH, LEX_START_OF_BATCH_SUB, "<start_of_batch>")
	LEXEME_END_OF_BATCH = init_create_Lexeme(LEX_END_OF_BATCH, LEX_END_OF_BATCH_SUB, "<end_of_batch>")

	LEXEME_DISPLAYWORD_EMPTY_STRING = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_EMPTY_STRING, "") // displaywords are not put in G_KEYWORDS_OPERATORS
	LEXEME_DISPLAYWORD_IS_NULL = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_IS_NULL, "is_null")
	LEXEME_DISPLAYWORD_IS_NOT_NULL = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_IS_NOT_NULL, "is_not_null")
	LEXEME_DISPLAYWORD_IN_LIST = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_IN_LIST, "in_list")
	LEXEME_DISPLAYWORD_NOT_IN_LIST = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_NOT_IN_LIST, "not_in_list")
	LEXEME_DISPLAYWORD_NOT_LIKE = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_NOT_LIKE, "not_like")
	LEXEME_DISPLAYWORD_NOT_BETWEEN = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_NOT_BETWEEN, "not_between")
	LEXEME_DISPLAYWORD_SUBQUERY_ONE = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_SUBQUERY_ONE, "subquery_one")
	LEXEME_DISPLAYWORD_SUBQUERY_MANY = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_SUBQUERY_MANY, "subquery_many")
	LEXEME_DISPLAYWORD_TRUE = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_TRUE, "true")
	LEXEME_DISPLAYWORD_FALSE = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_FALSE, "false")
	LEXEME_DISPLAYWORD_FIRSTDAYOFWEEK = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_FIRSTDAYOFWEEK, "firstdayofweek")
	LEXEME_DISPLAYWORD_DMY = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_DMY, "dmy")
	LEXEME_DISPLAYWORD_UTC_TO_LOCAL = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_UTC_TO_LOCAL, "utc_to_local")
	LEXEME_DISPLAYWORD_COMPILE_VARCHAR_TO_REGEXPLIKE = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_COMPILE_VARCHAR_TO_REGEXPLIKE, "compile_varchar_to_regexplike") // function used by decorator
	LEXEME_DISPLAYWORD_FIT_NO_TRUNCATE_VARBINARY = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_FIT_NO_TRUNCATE_VARBINARY, "fit_varbinary_no_truncate")             // function used by decorator
	LEXEME_DISPLAYWORD_FIT_NO_TRUNCATE_VARCHAR = init_create_Lexeme(LEX_DISPLAYWORD, LEX_DISPLAYWORD_FIT_NO_TRUNCATE_VARCHAR, "fit_varchar_no_truncate")                   // function used by decorator

	LEXEME_SYSVAR_CURRENT_LANGUAGE = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_language")
	LEXEME_SYSVAR_SYSTEM_USER_NAME = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@system_user_name") // not put in G_KEYWORDS_OPERATORS
	LEXEME_SYSVAR_SYSTEM_USER_ID = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@system_user_id")
	LEXEME_SYSVAR_CURRENT_DB_NAME = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_db_name")
	LEXEME_SYSVAR_CURRENT_DB_ID = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_db_id")
	LEXEME_SYSVAR_CURRENT_SCHEMA_NAME = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_schema_name")
	LEXEME_SYSVAR_CURRENT_SCHEMA_ID = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_schema_id")
	LEXEME_SYSVAR_CURRENT_USER_NAME = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_user_name")
	LEXEME_SYSVAR_CURRENT_USER_ID = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_user_id")
	LEXEME_SYSVAR_CURRENT_TIMESTAMP = init_create_Lexeme(LEX_VARIABLE, LEX_VARIABLE_SUB, "_@current_timestamp")
}
