// +build !trial

package lex

// SPEC_xxx constants are used by the lexer to limit the max length of lexemes IN BYTES, that is, variable names, identifiers, numbers, strings, etc.
//
const (
	SPEC_VARIABLE_MAX_LENGTH   = 128
	SPEC_ATFUNC_MAX_LENGTH     = 128
	SPEC_IDENTPART_MAX_LENGTH  = 128
	SPEC_NUMBER_MAX_LENGTH     = 128
	SPEC_HEXASTRING_MAX_LENGTH = 18000 // in bytes. There are 2 hexa digits per byte. This length includes also the "0x" prefix. Note: the parser will later also limit the max length of a VARBINARY, in byte count. PROD:18000
	SPEC_STRING_MAX_LENGTH     = 32000 // in bytes (utf8 byte count). Note: the parser will later also limit the max length of a VARCHAR, in rune count.
)
