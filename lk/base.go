package lk

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"

	"rsql"
)

// global lock manager object
//
var G_LOCK_MANAGER Lock_manager

/********************************************************************/
/*                                                                  */
/*                    registration public API                       */
/*                                                                  */
/********************************************************************/

// Lock type are only LOCK_SHARED (database is being used, or table is being read), or LOCK_EXCLUSIVE (database is created or deleted, or table is created, written to, deleted).
//   - LOCK_EXCLUSIVE: only one worker can have this lock
//   - LOCK_SHARED:    many workers can have this lock at the same time.
//
// LOCK_EXCLUSIVE and LOCK_SHARED are mutually incompatible.
//
// NOTE: when a table is LOCK_EXCLUSIVE, the database is usually LOCK_SHARED.
// A database needs LOCK_EXCLUSIVE only when it is created, deleted, or backed up.
//
type Lock_t int8

const (
	LOCK_SHARED    Lock_t = 1 // note that the expression    LOCK_SHARED < LOCK_EXCLUSIVE         is used in the code. So, don't change these constant values !
	LOCK_EXCLUSIVE Lock_t = 2
)

func (l Lock_t) String() string {
	switch l {
	case LOCK_SHARED:
		return "LOCK_SHARED"
	case LOCK_EXCLUSIVE:
		return "LOCK_EXCLUSIVE"
	default:
		return "---"
	}
}

// Registration is a list of databases and objects that the worker wants to lock.
//
// When the worker wants to acquire all requested locks in Registration:
//           - it passes the Registration object to the lock manager, and waits.
//           - the lock manager will reserve and acquire all requested locks, and then return to the worker the Registration with all locks acquired.
//           - the worker wakes up and can resume its activity
//
// Lock states are:
//           - requested, but not reserved
//           - requested and reserved
//           - acquired
//
type Registration struct {
	Reg_dbreslist  Dbres_list
	Reg_objreslist Objres_list

	reg_worker_id                         uint32        // locks are requested by this worker
	reg_requested_locks_are_reserved_flag bool          // used internally by the lock manager during locking acquisition process. True if it has reserved all requested locks. It will be set to false when all requested locks will be acquired.
	reg_timestamp                         uint64        // set by the lock manager when it receives lock request
	reg_answer_chan                       chan answer_t // channel that the lock manager uses to communicate with the worker
}

func New_registration(worker_id uint32) *Registration {
	var registration Registration

	registration.Reg_dbreslist = make(Dbres_list, 0, SPEC_DEFAULT_REG_DBRESLIST_CAPACITY)
	registration.Reg_objreslist = make(Objres_list, 0, SPEC_DEFAULT_REG_OBJRESLIST_CAPACITY)

	registration.reg_worker_id = worker_id
	registration.reg_requested_locks_are_reserved_flag = false // true if requested locks have been put in reserved state by the lock manager goroutine, during lock acquisition process
	registration.reg_timestamp = 0
	registration.reg_answer_chan = make(chan answer_t)

	return &registration
}

// Register_db enlists a database name in the Registration.
// If database name already exists in Registration, requested lock type may be upgraded (LOCK_SHARED->LOCK_EXCLUSIVE) if needed, else nothing is done.
// There is no database name duplicate.
//
// Permission is used to implement the database READ_ONLY mode. Permissions will accumulate in the database entry in Registration.
// You will pass PERMISSION_MODIFY_DATABASE/MODIFY_PRINCIPAL/MODIFY_OBJECT.
// The function Register_object can pass PERMISSION_SELECT/INSERT/UPDATE/DELETE.
//
func (registration *Registration) Register_db(db_name string, lock Lock_t, permission rsql.Permission_t) {

	registration.Reg_dbreslist.put(db_name, lock, permission)
}

// Register_db enlists an object name in the Registration.
// If object name already exists in Registration, requested lock type may be upgraded (LOCK_SHARED->LOCK_EXCLUSIVE) if needed, else nothing is done.
// There is no object name duplicate.
//
// This function also registers the database name for LOCK_SHARED (when working on objects, there is no need to put LOCK_EXCLUSIVE on the database).
//
// Permission is used to implement the database READ_ONLY mode. Permissions will accumulate in the database entry in Registration.
// You will pass PERMISSION_MODIFY_OBJECT/SELECT/INSERT/UPDATE/DELETE.
//
// Permission is also used to check if the user has all needed permissions to work on the object.
//
func (registration *Registration) Register_object(db_name string, schema_name string, object_name string, lock Lock_t, permission rsql.Permission_t) {

	registration.Reg_dbreslist.put(db_name, LOCK_SHARED, permission) // register db

	registration.Reg_objreslist.put(db_name, schema_name, object_name, lock, permission) // register object
}

// Register_object_qname is same as Register_object, but takes rsql.Object_qname_t as argument.
//
func (registration *Registration) Register_object_qname(qname rsql.Object_qname_t, lock Lock_t, permission rsql.Permission_t) {

	registration.Reg_dbreslist.put(qname.Database_name, LOCK_SHARED, permission) // register db

	registration.Reg_objreslist.put(qname.Database_name, qname.Schema_name, qname.Object_name, lock, permission) // register object
}

func (registration *Registration) Debug_print() {

	fmt.Printf("--- Registration content for worker id %d ---\n", registration.reg_worker_id)

	for _, elem := range registration.Reg_dbreslist {
		fmt.Printf("db:  %-30s  req:%-15s   acq:%-15s   perm:%-30s   %p\n", elem.Db_name, elem.lock_requested, elem.lock_acquired, elem.Permissions_required, elem.ptr_lockelem)
	}

	for _, elem := range registration.Reg_objreslist {
		fmt.Printf("obj: %-30s  req:%-15s   acq:%-15s   perm:%-30s   %p\n", strings.Join([]string{elem.Db_name, elem.Schema_name, elem.Object_name}, "."), elem.lock_requested, elem.lock_acquired, elem.Permissions_required, elem.ptr_lockelem)
	}

	fmt.Println("")
}

/********************************************************************/
/*                                                                  */
/*                 registration list implementation                 */
/*                                                                  */
/********************************************************************/

// list of database names, and requested locks.
//
type Dbres_list []Dbres_list_elem

// list of requested and already acquired locks on databases.
//
type Dbres_list_elem struct {
	Db_name string // database name

	lock_requested Lock_t // the worker wants to acquire this lock on the database. It is set to 0 when the lock is acquired.
	lock_acquired  Lock_t // this worker has acquired this lock on the database

	ptr_lockelem *lock_element // if a lock has been acquired, or waiting for a lock, points to a global lock_element for the database, created if needed by the lock manager.

	Permissions_required rsql.Permission_t // This field IS NOT USED by the lock manager. It contains permissions required to work on the database, PERMISSION_SELECT/INSERT/UPDATE/DELETE/MODIFY_DATABASE/MODIFY_PRINCIPAL/MODIFY_OBJECT
}

// put enlists a database name and requested lock.
// If database name is already reserved or acquired, lock_requested may be upgraded (LOCK_SHARED->LOCK_EXCLUSIVE), or nothing is done.
// There is no database name duplicate in the list.
//
// Permission can be PERMISSION_SELECT/INSERT/UPDATE/DELETE/MODIFY_DATABASE/MODIFY_PRINCIPAL/MODIFY_OBJECT. It is used to implement database READ_ONLY mode, to see what kind of statements the batch contains.
//
func (dbreslist *Dbres_list) put(db_name string, lock_requested Lock_t, permission rsql.Permission_t) {

	// check if element already exists

	for i, e := range *dbreslist {
		if !(db_name == e.Db_name) {
			continue
		}

		(*dbreslist)[i].Permissions_required |= permission // add permission

		if lock_requested <= e.lock_acquired || lock_requested <= e.lock_requested { // if lock is already acquired or requested, do nothing
			return
		}

		(*dbreslist)[i].lock_requested = lock_requested // upgrade lock
		return
	}

	// element doesn't exist. Add one.

	*dbreslist = append(*dbreslist, Dbres_list_elem{Db_name: db_name, lock_requested: lock_requested, Permissions_required: permission})
}

// list of object names, and requested locks.
//
type Objres_list []Objres_list_elem

// list of requested and already acquired locks on objects.
//
type Objres_list_elem struct {
	Db_name     string
	Schema_name string
	Object_name string

	lock_requested Lock_t // the worker wants to acquire this lock on the object. It is set to 0 when the lock is acquired.
	lock_acquired  Lock_t // this worker has acquired this lock on the object

	ptr_lockelem *lock_element // if a lock has been acquired, or waiting for a lock, points to a global lock_element for the database, created if needed by the lock manager.

	Permissions_required rsql.Permission_t // This field IS NOT USED by the lock manager. It contains only the four permissions required to work on the data of the object, PERMISSION_SELECT/INSERT/UPDATE/DELETE
}

// put enlists an object name and requested lock.
// If object name is already reserved or acquired, lock_requested may be upgraded (LOCK_SHARED->LOCK_EXCLUSIVE), or nothing is done.
// There is no object name duplicate in the list.
//
// Permission can be PERMISSION_SELECT/INSERT/UPDATE/DELETE/MODIFY_OBJECT. It is used to implement database READ_ONLY mode, to see what kind of statements the batch contains.
//
func (objreslist *Objres_list) put(db_name string, schema_name string, object_name string, lock_requested Lock_t, permission rsql.Permission_t) {

	// check if element already exists

	for i, e := range *objreslist {
		if !(object_name == e.Object_name && schema_name == e.Schema_name && db_name == e.Db_name) {
			continue
		}

		(*objreslist)[i].Permissions_required |= permission // add SELECT, UPDATE, INSERT, DELETE, MODIFY_OBJECT permission

		if lock_requested <= e.lock_acquired || lock_requested <= e.lock_requested { // if lock is already acquired or requested, do nothing
			return
		}

		(*objreslist)[i].lock_requested = lock_requested // upgrade lock
		return
	}

	// element doesn't exist. Add one.

	*objreslist = append(*objreslist, Objres_list_elem{Db_name: db_name, Schema_name: schema_name, Object_name: object_name, lock_requested: lock_requested, Permissions_required: permission})
}

/********************************************************************/
/*                                                                  */
/*                      central lock registry                       */
/*                                                                  */
/********************************************************************/

// Lock_manager is mainly a map, with db or object name as key, and each entry pointing to a lock_element.
//
type Lock_manager struct {
	lmgr_hashtable map[lock_key]*lock_element // all locks are stored here

	lmgr_acquire_channel chan *Registration // buffered channel, acting as an ordered queue. When a worker requests locks, it sends a Registration object in this channel and waits for the answer.
	lmgr_waiting_queue   Waiting_queue      // if the worker must wait for locks, Registration is put in this queue

	lmgr_release_channel chan *Registration // buffered channel, acting as an ordered queue. When the worker wants to free all locks, it sends Registration object in this channel and waits for the answer.

	lmgr_ticker              *time.Ticker // ticker for timeout. If a worker waits more than lmgr_timeout_ticks_count, a timeout answer is returned.
	lmgr_timestamp           uint64       // current timestamp, incremented at each tick of ticker.
	lmgr_timeout_ticks_count uint64       // ticks count before a worker will timeout

	lmgr_diagnostic_channel chan *Diagnostic // buffered channel, acting as an ordered queue. When a worker requests diagnostic information about lock manager, it sends a Diagnostic object in this channel and waits for the answer.
}

type Waiting_queue []*Registration

func (q *Waiting_queue) Insert_last(reg *Registration) {
	(*q) = append(*q, reg)
}

func (q *Waiting_queue) Delete(i int) {
	copy((*q)[i:], (*q)[i+1:])
	(*q)[len(*q)-1] = nil
	*q = (*q)[:len(*q)-1]
}

func (q *Waiting_queue) Delete_nil_entries() {

	k := 0
	for i := 0; i < len(*q); i++ {
		if (*q)[i] == nil {
			continue
		}

		if i == k {
			k++
			continue
		}

		(*q)[k] = (*q)[i]
		k++
	}

	*q = (*q)[:k]
}

type lock_type_t uint8

const (
	TYPE_DB_LOCK     lock_type_t = 1
	TYPE_OBJECT_LOCK lock_type_t = 2
)

type lock_key struct {
	lk_lock_type lock_type_t // TYPE_DB_LOCK, TYPE_OBJECT_LOCK

	lk_db_name     string
	lk_schema_name string // not used for TYPE_DB_LOCK
	lk_object_name string // not used for TYPE_DB_LOCK
}

type lock_element struct {
	le_lock_type lock_type_t // TYPE_DB_LOCK, TYPE_OBJECT_LOCK

	le_lock_exclusive          uint64 // bit i is set if worker i has acquired an exclusive lock
	le_lock_shared             uint64 // bit i is set if worker i has acquired a shared lock
	le_lock_reserved_exclusive uint64 // bit i is set if worker i has reserved an exclusive lock
	le_lock_reserved_shared    uint64 // bit i is set if worker i has reserved a shared lock

	le_refcount uint32 // Registration entries keep a pointer to lock_elements, when they are waiting or have acquired locks. If refcount is 0, lock_element is deleted from the map.
}

func debug_print_uint64_lock(v uint64) string {

	list := make([]string, 0, 6) // size is good enough

	for i := 0; i < 64; i++ {
		if v&(1<<uint(i)) == 0 {
			continue
		}

		list = append(list, strconv.Itoa(i))
	}

	return strings.Join(list, ",")
}

// Returned value from check_locks() function.
//
//    Locks are reserved or acquired in all-or-none manner.
//    Locks must be reserved before being acquired. But if they can be reserved and also acquired, reservation step is skipped.
//
//	  LOCKING_CAPABILITY_NONE:     impossible to reserve the requested locks. The worker must wait.
//	  LOCKING_CAPABILITY_RESERVE:  impossible to acquire the requested locks. The locks are reserved and the worker must wait.
//	  LOCKING_CAPABILITY_ACQUIRE:  locks have been acquired. The worker can go on.
//
type Locking_capability_t uint8

const (
	LOCKING_CAPABILITY_NONE    Locking_capability_t = 0
	LOCKING_CAPABILITY_RESERVE Locking_capability_t = 1
	LOCKING_CAPABILITY_ACQUIRE Locking_capability_t = 2
)

func (lc Locking_capability_t) String() string {

	switch lc {
	case LOCKING_CAPABILITY_NONE:
		return "LOCKING_CAPABILITY_NONE"
	case LOCKING_CAPABILITY_RESERVE:
		return "LOCKING_CAPABILITY_RESERVE"
	case LOCKING_CAPABILITY_ACQUIRE:
		return "LOCKING_CAPABILITY_ACQUIRE"
	default:
		return "unknown Locking_capability_t"
	}
}

type answer_t uint8

const (
	ANSWER_TIMEOUT                  answer_t = iota + 1 // Lock() function returns this value if timeout occured
	ANSWER_REQUESTED_LOCKS_ACQUIRED                     // Lock() function returns this value if all requested locks have been acquired
	_ANSWER_LOCKS_FREED                                 // Unlock() sends this value into the answer channel, to unblock the waiting worker. The user never need this value.
	_ANSWER_LIST_LOCKS_DONE                             // Get_list() sends this value into the answer channel, to unblock the waiting worker. The user never need this value.
)

func (ans answer_t) String() string {

	switch ans {
	case ANSWER_TIMEOUT:
		return "ANSWER_TIMEOUT"
	case ANSWER_REQUESTED_LOCKS_ACQUIRED:
		return "ANSWER_REQUESTED_LOCKS_ACQUIRED"
	case _ANSWER_LOCKS_FREED:
		return "_ANSWER_LOCKS_FREED"
	case _ANSWER_LIST_LOCKS_DONE:
		return "_ANSWER_LIST_LOCKS_DONE"
	default:
		return "unknown answer_t"
	}
}

func (lockmgr *Lock_manager) Initialize(ticker_interval time.Duration, timeout_ticks_count uint64) {
	rsql.Assert(lockmgr.lmgr_hashtable == nil)

	lockmgr.lmgr_hashtable = make(map[lock_key]*lock_element, SPEC_LOCK_MANAGER_MAP_DEFAULT_SIZE) // create map that keeps trak of all locks

	lockmgr.lmgr_acquire_channel = make(chan *Registration, SPEC_CHANNEL_DEFAULT_LENGTH) // worker sends Registration object in this channel, to acquire locks
	lockmgr.lmgr_waiting_queue = make([]*Registration, 0, SPEC_CHANNEL_DEFAULT_LENGTH)

	lockmgr.lmgr_release_channel = make(chan *Registration) // worker sends Registration object in this channel, to release locks

	lockmgr.lmgr_ticker = time.NewTicker(ticker_interval) // ticker init
	lockmgr.lmgr_timeout_ticks_count = timeout_ticks_count

	lockmgr.lmgr_diagnostic_channel = make(chan *Diagnostic, SPEC_CHANNEL_DEFAULT_LENGTH) // worker sends Diagnostic object in this channel, to get info about the lock manager

	// run dedicated goroutine for lock administration

	go lockmgr.administer()
}

// check_locks only check if all requested locks in Registration can be reserved of acquired.
// It also creates new lock_elements for all Registration entries, if not already exist.
//
//     Checks if all locks can be reserved (if they are already in the reserved state, this check is no-op):
//         - if yes, checks if all locks can be acquired:
//                - if yes, returns LOCKING_CAPABILITY_ACQUIRE
//                - else, returns LOCKING_CAPABILITY_RESERVE
//         - else, returns LOCKING_CAPABILITY_NONE
//
//     A worker waiting for locks can have the requested locks:
//         - in reserved state:     the function must only check if the requested locks can be acquired.
//         - not in reserved state: the function must check if the requested locks can be reserved, and only then if they can be acquired.
//
func (lockmgr *Lock_manager) check_locks(registration *Registration) Locking_capability_t {
	var (
		ok                 bool
		locking_capability Locking_capability_t
		worker_bitmask     uint64

		other_lock_exclusive          uint64
		other_lock_shared             uint64
		other_lock_reserved_exclusive uint64
		other_lock_reserved_shared    uint64
	)

	locking_capability = LOCKING_CAPABILITY_ACQUIRE // if registration.reg_requested_locks_are_reserved_flag == false, we try to reserve->acquire all requested locks. If one lock cannot be acquired, this flag is downgraded to LOCKING_CAPABILITY_RESERVE.

	worker_bitmask = 1 << registration.reg_worker_id

	//=== check db locks ===

	for i, e := range registration.Reg_dbreslist { // for each requested lock
		if e.lock_requested == 0 { // a lock has already been acquired in previous batch in same transaction
			continue
		}

		// get lock_element

		lockelem := e.ptr_lockelem
		if lockelem == nil {
			key := lock_key{lk_lock_type: TYPE_DB_LOCK, lk_db_name: e.Db_name}

			if lockelem, ok = lockmgr.lmgr_hashtable[key]; ok == false { // if no lock_element, create a new one
				lockelem = &lock_element{le_lock_type: TYPE_DB_LOCK, le_refcount: 0}
				lockmgr.lmgr_hashtable[key] = lockelem
			}

			registration.Reg_dbreslist[i].ptr_lockelem = lockelem
			lockelem.le_refcount++
		}

		// get info about locks reserved or acquired by other workers

		other_lock_exclusive = lockelem.le_lock_exclusive &^ worker_bitmask
		other_lock_shared = lockelem.le_lock_shared &^ worker_bitmask
		other_lock_reserved_exclusive = lockelem.le_lock_reserved_exclusive &^ worker_bitmask
		other_lock_reserved_shared = lockelem.le_lock_reserved_shared &^ worker_bitmask

		// check if requested locks can be reserved or acquired.

		switch registration.reg_requested_locks_are_reserved_flag {
		case false: // check if requested locks can be reserved. If yes, check if it can be acquired.
			switch e.lock_requested {
			case LOCK_EXCLUSIVE:
				if (other_lock_reserved_exclusive | other_lock_reserved_shared) != 0 { // check if requested lock can be reserved. If not, return, because requested locks must be reserved as a whole.
					return LOCKING_CAPABILITY_NONE
				}
				if locking_capability == LOCKING_CAPABILITY_ACQUIRE { // check if requested lock can be acquired. If not, disable this check, because requested locks must be acquired as a whole.
					if (other_lock_exclusive | other_lock_shared) != 0 {
						locking_capability = LOCKING_CAPABILITY_RESERVE
					}
				}

			case LOCK_SHARED:
				if other_lock_reserved_exclusive != 0 { // check if requested lock can be reserved. If not, return, because requested locks must be reserved as a whole.
					return LOCKING_CAPABILITY_NONE
				}
				if locking_capability == LOCKING_CAPABILITY_ACQUIRE { // check if requested lock can be acquired. If not, disable this check, because requested locks must be acquired as a whole.
					if other_lock_exclusive != 0 {
						locking_capability = LOCKING_CAPABILITY_RESERVE
					}
				}
			}

		default: // true. Requested locks are already in reserved state. Check if they can be acquired. If not, return, because requested locks must be acquired as a whole.
			switch e.lock_requested {
			case LOCK_EXCLUSIVE:
				if (other_lock_exclusive | other_lock_shared) != 0 {
					return LOCKING_CAPABILITY_RESERVE
				}

			case LOCK_SHARED:
				if other_lock_exclusive != 0 {
					return LOCKING_CAPABILITY_RESERVE
				}
			}
		}
	}

	//=== check object locks ===

	for i, e := range registration.Reg_objreslist { // for each requested lock
		if e.lock_requested == 0 { // a lock has already been acquired in previous batch in same transaction
			continue
		}

		// get lock_element

		lockelem := e.ptr_lockelem
		if lockelem == nil {
			key := lock_key{lk_lock_type: TYPE_OBJECT_LOCK, lk_db_name: e.Db_name, lk_schema_name: e.Schema_name, lk_object_name: e.Object_name}

			if lockelem, ok = lockmgr.lmgr_hashtable[key]; ok == false { // if no lock_element, create a new one
				lockelem = &lock_element{le_lock_type: TYPE_OBJECT_LOCK, le_refcount: 0}
				lockmgr.lmgr_hashtable[key] = lockelem
			}

			registration.Reg_objreslist[i].ptr_lockelem = lockelem
			lockelem.le_refcount++
		}

		// get info about locks reserved or acquired by other workers

		other_lock_exclusive = lockelem.le_lock_exclusive &^ worker_bitmask
		other_lock_shared = lockelem.le_lock_shared &^ worker_bitmask
		other_lock_reserved_exclusive = lockelem.le_lock_reserved_exclusive &^ worker_bitmask
		other_lock_reserved_shared = lockelem.le_lock_reserved_shared &^ worker_bitmask

		// check if requested locks can be reserved or acquired.

		switch registration.reg_requested_locks_are_reserved_flag {
		case false: // check if requested locks can be reserved. If yes, check if it can be acquired.
			switch e.lock_requested {
			case LOCK_EXCLUSIVE:
				if (other_lock_reserved_exclusive | other_lock_reserved_shared) != 0 { // check if requested lock can be reserved. If not, return, because requested locks must be reserved as a whole.
					return LOCKING_CAPABILITY_NONE
				}
				if locking_capability == LOCKING_CAPABILITY_ACQUIRE { // check if requested lock can be acquired. If not, disable this check, because requested locks must be acquired as a whole.
					if (other_lock_exclusive | other_lock_shared) != 0 {
						locking_capability = LOCKING_CAPABILITY_RESERVE
					}
				}

			case LOCK_SHARED:
				if other_lock_reserved_exclusive != 0 { // check if requested lock can be reserved. If not, return, because requested locks must be reserved as a whole.
					return LOCKING_CAPABILITY_NONE
				}
				if locking_capability == LOCKING_CAPABILITY_ACQUIRE { // check if requested lock can be acquired. If not, disable this check, because requested locks must be acquired as a whole.
					if other_lock_exclusive != 0 {
						locking_capability = LOCKING_CAPABILITY_RESERVE
					}
				}
			}

		default: // true. Requested locks are already in reserved state. Check if they can be acquired. If not, return, because requested locks must be acquired as a whole.
			switch e.lock_requested {
			case LOCK_EXCLUSIVE:
				if (other_lock_exclusive | other_lock_shared) != 0 {
					return LOCKING_CAPABILITY_RESERVE
				}

			case LOCK_SHARED:
				if other_lock_exclusive != 0 {
					return LOCKING_CAPABILITY_RESERVE
				}
			}
		}
	}

	return locking_capability // LOCKING_CAPABILITY_ACQUIRE or LOCKING_CAPABILITY_RESERVE
}

// reserve_locks reserves all requested locks in Registration.
//
func (lockmgr *Lock_manager) reserve_locks(registration *Registration) {
	var (
		worker_bitmask uint64
	)

	// requested locks have already been reserved, return

	if registration.reg_requested_locks_are_reserved_flag == true {
		return
	}

	worker_bitmask = 1 << registration.reg_worker_id

	// reserve db locks

	for _, e := range registration.Reg_dbreslist { // for each requested lock
		if e.lock_requested == 0 { // a lock has already been acquired in previous batch in same transaction
			continue
		}

		lockelem := e.ptr_lockelem // get lock_element

		switch e.lock_requested {
		case LOCK_EXCLUSIVE:
			rsql.Assert((lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&^worker_bitmask == 0)
			lockelem.le_lock_reserved_exclusive |= worker_bitmask

		case LOCK_SHARED:
			rsql.Assert(lockelem.le_lock_reserved_exclusive == 0)
			lockelem.le_lock_reserved_shared |= worker_bitmask
		}
	}

	// reserve object locks

	for _, e := range registration.Reg_objreslist { // for each requested lock
		if e.lock_requested == 0 { // a lock has already been acquired in previous batch in same transaction
			continue
		}

		lockelem := e.ptr_lockelem // get lock_element

		switch e.lock_requested {
		case LOCK_EXCLUSIVE:
			rsql.Assert((lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&^worker_bitmask == 0)
			lockelem.le_lock_reserved_exclusive |= worker_bitmask

		case LOCK_SHARED:
			rsql.Assert(lockelem.le_lock_reserved_exclusive == 0)
			lockelem.le_lock_reserved_shared |= worker_bitmask
		}
	}

	// requested locks have been reserved

	registration.reg_requested_locks_are_reserved_flag = true
}

// acquire_locks acquires all requested locks in Registration.
//
func (lockmgr *Lock_manager) acquire_locks(registration *Registration) {
	var (
		worker_bitmask uint64
	)

	worker_bitmask = 1 << registration.reg_worker_id

	// acquire db locks

	for i, e := range registration.Reg_dbreslist { // for each requested lock
		if e.lock_requested == 0 { // a lock has already been acquired in previous batch in same transaction
			continue
		}

		lockelem := e.ptr_lockelem // get lock_element

		switch e.lock_requested {
		case LOCK_EXCLUSIVE:
			rsql.Assert((lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&^worker_bitmask == 0)
			lockelem.le_lock_exclusive |= worker_bitmask
			lockelem.le_lock_shared &^= worker_bitmask
			lockelem.le_lock_reserved_exclusive &^= worker_bitmask
			lockelem.le_lock_reserved_shared &^= worker_bitmask

		case LOCK_SHARED:
			rsql.Assert(lockelem.le_lock_exclusive|lockelem.le_lock_reserved_exclusive == 0)
			lockelem.le_lock_shared |= worker_bitmask
			lockelem.le_lock_reserved_shared &^= worker_bitmask
		}

		registration.Reg_dbreslist[i].lock_requested = 0
		registration.Reg_dbreslist[i].lock_acquired = e.lock_requested
	}

	// acquire object locks

	for i, e := range registration.Reg_objreslist { // for each requested lock
		if e.lock_requested == 0 { // a lock has already been acquired in previous batch in same transaction
			continue
		}

		lockelem := e.ptr_lockelem // get lock_element

		switch e.lock_requested {
		case LOCK_EXCLUSIVE:
			rsql.Assert((lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&^worker_bitmask == 0)
			lockelem.le_lock_exclusive |= worker_bitmask
			lockelem.le_lock_shared &^= worker_bitmask
			lockelem.le_lock_reserved_exclusive &^= worker_bitmask
			lockelem.le_lock_reserved_shared &^= worker_bitmask

		case LOCK_SHARED:
			rsql.Assert(lockelem.le_lock_exclusive|lockelem.le_lock_reserved_exclusive == 0)
			lockelem.le_lock_shared |= worker_bitmask
			lockelem.le_lock_reserved_shared &^= worker_bitmask
		}

		registration.Reg_objreslist[i].lock_requested = 0
		registration.Reg_objreslist[i].lock_acquired = e.lock_requested
	}

	// requested locks have been acquired

	registration.reg_requested_locks_are_reserved_flag = false

	if rsql.G_DEBUG_FLAG {
		fmt.Println("Acquired")
		G_LOCK_MANAGER.Debug_print()
	}
}

// free_locks frees all locks in Registration, and delete lock objects in lock manager that are not referenced any more.
// This function is called when the worker wants to release all acquired locks, when the batch terminates.
//
// Returns true if Registration contained one or more acquired locks.
// All Registration entries are deleted.
//
func (lockmgr *Lock_manager) free_locks(registration *Registration) (registration_contained_acquired_locks bool) {
	var (
		worker_bitmask uint64
	)

	rsql.Assert(registration.reg_requested_locks_are_reserved_flag == false) // no reserved locks can exist in this Registration

	worker_bitmask = 1 << registration.reg_worker_id

	// free db locks

	for i, e := range registration.Reg_dbreslist { // clear each entry in Registration
		lockelem := e.ptr_lockelem // get lock_element
		if lockelem == nil {
			registration.Reg_dbreslist[i] = Dbres_list_elem{}
			continue
		}

		registration_contained_acquired_locks = true

		lockelem.le_lock_exclusive &^= worker_bitmask
		lockelem.le_lock_shared &^= worker_bitmask
		rsql.Assert((lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&worker_bitmask == 0) // no reserved locks can exist

		lockelem.le_refcount--
		if lockelem.le_refcount == 0 { // if no more worker references this lock_element, delete it
			rsql.Assert(lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared == 0)
			key := lock_key{lk_lock_type: TYPE_DB_LOCK, lk_db_name: e.Db_name}
			delete(lockmgr.lmgr_hashtable, key)
		}

		registration.Reg_dbreslist[i] = Dbres_list_elem{}
	}

	registration.Reg_dbreslist = registration.Reg_dbreslist[:0] // truncate db list in Registration

	// free object locks

	for i, e := range registration.Reg_objreslist { // clear each entry in Registration
		lockelem := e.ptr_lockelem // get lock_element
		if lockelem == nil {
			registration.Reg_objreslist[i] = Objres_list_elem{}
			continue
		}

		registration_contained_acquired_locks = true

		lockelem.le_lock_exclusive &^= worker_bitmask
		lockelem.le_lock_shared &^= worker_bitmask
		rsql.Assert((lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&worker_bitmask == 0) // no reserved locks can exist

		lockelem.le_refcount--
		if lockelem.le_refcount == 0 { // if no more worker references this lock_element, delete it
			rsql.Assert(lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared == 0)
			key := lock_key{lk_lock_type: TYPE_OBJECT_LOCK, lk_db_name: e.Db_name, lk_schema_name: e.Schema_name, lk_object_name: e.Object_name}
			delete(lockmgr.lmgr_hashtable, key)
		}

		registration.Reg_objreslist[i] = Objres_list_elem{}
	}

	registration.Reg_objreslist = registration.Reg_objreslist[:0] // truncate object list in Registration

	if rsql.G_DEBUG_FLAG {
		fmt.Println("Release")
		G_LOCK_MANAGER.Debug_print()
	}

	return registration_contained_acquired_locks
}

// unreserve_locks dereferences lock objects for entries in Registration that have reserved locks.
//
// If a Registration entry that has reserved lock, but not acquired lock, its reference to the lock object must be removed.
//
// If a lock element is no more referenced, it is removed from the lock manager.
//
// When calling this function, Registration.reg_requested_locks_are_reserved_flag must be true, which means that reserved locks exist.
//
func (lockmgr *Lock_manager) unreserve_locks(registration *Registration) {
	var (
		worker_bitmask uint64
	)

	rsql.Assert(registration.reg_requested_locks_are_reserved_flag == true)

	worker_bitmask = 1 << registration.reg_worker_id

	// dereference db locks

	for i, e := range registration.Reg_dbreslist { // for each entry in Registration
		lockelem := e.ptr_lockelem
		lockelem.le_lock_reserved_exclusive &^= worker_bitmask // maybe lock is acquired and not reserved, but we clear the reserved bit to be sure
		lockelem.le_lock_reserved_shared &^= worker_bitmask

		if e.lock_acquired != 0 {
			continue
		}

		rsql.Assert((lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&worker_bitmask == 0)

		lockelem.le_refcount--
		registration.Reg_dbreslist[i].ptr_lockelem = nil

		if lockelem.le_refcount == 0 { // if no more worker references this lock_element, delete it
			rsql.Assert(lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared == 0)
			key := lock_key{lk_lock_type: TYPE_DB_LOCK, lk_db_name: e.Db_name}
			delete(lockmgr.lmgr_hashtable, key)
		}
	}

	// dereference object locks

	for i, e := range registration.Reg_objreslist { // for each entry in Registration
		lockelem := e.ptr_lockelem
		lockelem.le_lock_reserved_exclusive &^= worker_bitmask // maybe lock is acquired and not reserved, but we clear the reserved bit to be sure
		lockelem.le_lock_reserved_shared &^= worker_bitmask

		if e.lock_acquired != 0 {
			continue
		}

		rsql.Assert((lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared)&worker_bitmask == 0)

		lockelem.le_refcount--
		registration.Reg_objreslist[i].ptr_lockelem = nil

		if lockelem.le_refcount == 0 { // if no more worker references this lock_element, delete it
			rsql.Assert(lockelem.le_lock_exclusive|lockelem.le_lock_shared|lockelem.le_lock_reserved_exclusive|lockelem.le_lock_reserved_shared == 0)
			key := lock_key{lk_lock_type: TYPE_OBJECT_LOCK, lk_db_name: e.Db_name, lk_schema_name: e.Schema_name, lk_object_name: e.Object_name}
			delete(lockmgr.lmgr_hashtable, key)
		}
	}

	// locks are no more reserved

	registration.reg_requested_locks_are_reserved_flag = false
}

/********************************************************************/
/*                                                                  */
/*                          lock manager API                        */
/*                                                                  */
/********************************************************************/

// Acquire acquires all requested locks in Registration, or times out.
//
// In case of timeout, an error is returned.
//
// If you have called Acquire(), then you must always call Release() to free the lock elements.
//
// The call sequence is:
//      - registration.Register_object(...)  or      registration.Register_db(...)
//      - (Register method is called multiple time to register all requested locks)
//
//      - if rsql_err := G_LOCK_MANAGER.Acquire(registration); rsql_err != nil {
//		return rsql_err
//	  }
//      - (do your stuff)
//
//      - registration.Register_object(...)  or      registration.Register_db(...)        more locks can be requested
//      - (Register method is called multiple time to register all requested locks)
//
//      - if rsql_err := G_LOCK_MANAGER.Acquire(registration); rsql_err != nil {
//		return rsql_err
//	  }
//      - (do your stuff)
//
//      - G_LOCK_MANAGER.Release(registration)
//
// If the locks are successfully acquired, lock entries in Registration object are modified from requested to acquired.
// In case of timeout, Registration object is not modified. In particular, locks already acquired are kept this way.
// So, it is safer to always call Release(), also if you received a timeout error.
//
// Acquire can be called multiple times. If all locks in Registration are already acquired, the function does nothing.
//
func (lockmgr *Lock_manager) Acquire(registration *Registration) *rsql.Error {
	var (
		answer answer_t
	)

	ans_chan := registration.reg_answer_chan

	lockmgr.lmgr_acquire_channel <- registration // send Registration object to lock manager

	answer = <-ans_chan // wait for its answer (locks acquired, or timeout)

	if answer == ANSWER_TIMEOUT {
		return rsql.New_Error(rsql.ERROR_RESOURCE, rsql.ERROR_LOCKING_TIMEOUT, rsql.ERROR_BATCH_ABORT)
	}

	return nil
}

// Release frees all locks in Registration, and release lock objects.
// After the call, Registration object is empty. It can be called many times, but subsequent calls just do nothing as Registration object is empty.
//
func (lockmgr *Lock_manager) Release(registration *Registration) {

	ans_chan := registration.reg_answer_chan

	lockmgr.lmgr_release_channel <- registration // send Registration object to lock manager

	<-ans_chan // wait for its answer (locks freed), which is almost immediate
}

type Diagnostic struct {
	dg_worker_id  uint32
	dg_locks_list []string // only used by Get_locks_list(). The lock manager will put there a list of all the locks, ready to be printed.

	dg_answer_chan chan answer_t // channel that the lock manager uses to communicate with the worker
}

// Get_locks_list gets the list of all locks in the lock manager.
//
func (lockmgr *Lock_manager) Get_locks_list(worker_id uint32) []string {

	diag := Diagnostic{}
	diag.dg_worker_id = worker_id
	diag.dg_answer_chan = make(chan answer_t)

	ans_chan := diag.dg_answer_chan

	lockmgr.lmgr_diagnostic_channel <- &diag // send Diagnostic object to lock manager

	answer := <-ans_chan // wait for its answer. The lock manager responds immediately.
	rsql.Assert(answer == _ANSWER_LIST_LOCKS_DONE)

	return diag.dg_locks_list
}

// get_locks_list_for_display returns a list of locks, as []string to be printed.
//
func get_locks_list_for_display(lmgr_hashtable map[lock_key]*lock_element) []string {

	locks_list := make([]string, 0, len(lmgr_hashtable))

	for key, elem := range lmgr_hashtable {
		s := ""

		switch key.lk_lock_type {
		case TYPE_DB_LOCK:
			s = fmt.Sprintf("db:  %-50s  X:%-15s   S:%-15s   resX:%-15s   resS:%-15s    refcount:%d", key.lk_db_name,
				debug_print_uint64_lock(elem.le_lock_exclusive), debug_print_uint64_lock(elem.le_lock_shared), debug_print_uint64_lock(elem.le_lock_reserved_exclusive), debug_print_uint64_lock(elem.le_lock_reserved_shared), elem.le_refcount)

		case TYPE_OBJECT_LOCK:
			s = fmt.Sprintf("obj: %-50s  X:%-15s   S:%-15s   resX:%-15s   resS:%-15s    refcount:%d", strings.Join([]string{key.lk_db_name, key.lk_schema_name, key.lk_object_name}, "."),
				debug_print_uint64_lock(elem.le_lock_exclusive), debug_print_uint64_lock(elem.le_lock_shared), debug_print_uint64_lock(elem.le_lock_reserved_exclusive), debug_print_uint64_lock(elem.le_lock_reserved_shared), elem.le_refcount)

		default:
			panic("impossible")
		}

		locks_list = append(locks_list, s)
	}

	sort.Strings(locks_list) // sort the list

	return locks_list
}

// for debugging only.
// WARNING: not thread-safe. If called outside of the lock manager code, it is possible that locks are being acquired or released.
//          Reading lockmgr.lmgr_hashtable when the lock manager is possibly modifying it is no safe and a crash is possible.
//
func (lockmgr *Lock_manager) Debug_print() {

	fmt.Println("--- Lock manager content ---")

	for key, elem := range lockmgr.lmgr_hashtable {
		switch key.lk_lock_type {
		case TYPE_DB_LOCK:
			fmt.Printf("db:  %-30s  X:%-15s   S:%-15s   resX:%-15s   resS:%-15s    refcount:%d\n", key.lk_db_name,
				debug_print_uint64_lock(elem.le_lock_exclusive), debug_print_uint64_lock(elem.le_lock_shared), debug_print_uint64_lock(elem.le_lock_reserved_exclusive), debug_print_uint64_lock(elem.le_lock_reserved_shared), elem.le_refcount)

		case TYPE_OBJECT_LOCK:
			fmt.Printf("obj: %-30s  X:%-15s   S:%-15s   resX:%-15s   resS:%-15s    refcount:%d\n", strings.Join([]string{key.lk_db_name, key.lk_schema_name, key.lk_object_name}, "."),
				debug_print_uint64_lock(elem.le_lock_exclusive), debug_print_uint64_lock(elem.le_lock_shared), debug_print_uint64_lock(elem.le_lock_reserved_exclusive), debug_print_uint64_lock(elem.le_lock_reserved_shared), elem.le_refcount)

		default:
			panic("impossible")
		}
	}

	fmt.Println("")
}

/********************************************************************/
/*                                                                  */
/*                          lock manager goroutine                  */
/*                                                                  */
/********************************************************************/

// administer if the goroutine in charge of managing locks.
//
// It receives Registration objects from workers, and tries to acquire all the requested locks.
// It then returns to the worker the Registration object with all locks acquired.
// If it fails for too long, a timeout is returned to the worker.
//
func (lockmgr *Lock_manager) administer() {

	for {
		check_other_workers_flag := false

		select {
		case registration := <-lockmgr.lmgr_acquire_channel: // locks acquisition, first try
			locking_capability := lockmgr.check_locks(registration) // create lock objects if necessary

			switch locking_capability {
			case LOCKING_CAPABILITY_NONE:
				registration.reg_timestamp = lockmgr.lmgr_timestamp
				lockmgr.lmgr_waiting_queue.Insert_last(registration)

			case LOCKING_CAPABILITY_RESERVE:
				lockmgr.reserve_locks(registration)
				registration.reg_timestamp = lockmgr.lmgr_timestamp
				lockmgr.lmgr_waiting_queue.Insert_last(registration)

			case LOCKING_CAPABILITY_ACQUIRE:
				lockmgr.acquire_locks(registration)
				registration.reg_timestamp = 0
				registration.reg_answer_chan <- ANSWER_REQUESTED_LOCKS_ACQUIRED // unblock worker
			}

			continue // continue loop

		case registration := <-lockmgr.lmgr_release_channel: // free locks. A worker that acquired locks, or a worker that timed out, must both eventually call Release() to free locks and lock_elements.
			check_other_workers_flag = lockmgr.free_locks(registration) // free locks and lock_elements
			registration.reg_answer_chan <- _ANSWER_LOCKS_FREED         // Registration list of locks is now empty

		case <-lockmgr.lmgr_ticker.C: // timeout ticker
			lockmgr.lmgr_timestamp++
			current_timestamp := lockmgr.lmgr_timestamp

			wq_entries_nilified := false
			for i, reg_aux := range lockmgr.lmgr_waiting_queue { // check if a waiting worker is timing out
				if current_timestamp-reg_aux.reg_timestamp > lockmgr.lmgr_timeout_ticks_count { //fmt.Println("TIMEOUT", 1);G_LOCK_MANAGER.Debug_print()
					if reg_aux.reg_requested_locks_are_reserved_flag == true {
						lockmgr.unreserve_locks(reg_aux) // unreserve entries, and remove lock objects if refcount == 0
						check_other_workers_flag = true
					}
					lockmgr.lmgr_waiting_queue[i] = nil
					wq_entries_nilified = true
					reg_aux.reg_timestamp = 0
					reg_aux.reg_answer_chan <- ANSWER_TIMEOUT // locks acquired in a previous transaction are not touched. Registration has not been modified.
				}
			}

			if wq_entries_nilified {
				lockmgr.lmgr_waiting_queue.Delete_nil_entries()
			}

		case diag := <-lockmgr.lmgr_diagnostic_channel: // request for information about lock manager
			diag.dg_locks_list = get_locks_list_for_display(lockmgr.lmgr_hashtable) // create a list of locks as []string
			diag.dg_answer_chan <- _ANSWER_LIST_LOCKS_DONE                          // unblock worker
		} // end select

		// after locks have been released or if timout has discarded reserved locks, checks if other waiting workers can reserve or acquire locks

		if check_other_workers_flag == true {
			wq_entries_nilified := false

			for i, reg_aux := range lockmgr.lmgr_waiting_queue { // for all waiting Registrations
				locking_capability := lockmgr.check_locks(reg_aux)

				switch locking_capability {
				case LOCKING_CAPABILITY_NONE:
					// pass

				case LOCKING_CAPABILITY_RESERVE:
					lockmgr.reserve_locks(reg_aux)

				case LOCKING_CAPABILITY_ACQUIRE:
					lockmgr.acquire_locks(reg_aux)
					lockmgr.lmgr_waiting_queue[i] = nil
					wq_entries_nilified = true
					reg_aux.reg_timestamp = 0
					reg_aux.reg_answer_chan <- ANSWER_REQUESTED_LOCKS_ACQUIRED
				}
			}

			if wq_entries_nilified {
				lockmgr.lmgr_waiting_queue.Delete_nil_entries()
			}
		}

	} // end main loop
}
