package lk

import (
	"fmt"
	"testing"
	"time"
	//"rsql"
)

var xxx = 1

func Test_lock_manager(t *testing.T) {

	lk.G_LOCK_MANAGER.Initialize(1000, 4)

	go f4()

	go f5()
	go f6()
	go f7()
	go f8()
	go f9()
	go f10()
	time.Sleep(0 * time.Second)

	fmt.Println("start")

	registration := New_registration(1)

	registration.Register_db("prout", LOCK_SHARED)
	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 1)
		G_LOCK_MANAGER.Debug_print()
		registration.Debug_print()
		goto AA
	}

	time.Sleep(4 * time.Second)

	registration.Register_db("prout", LOCK_SHARED)
	registration.Register_db("dbic", LOCK_SHARED)
	registration.Register_db("dwh", LOCK_SHARED)
	registration.Register_db("coucou", LOCK_SHARED)
	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		registration.Debug_print()
		goto AA
	}
	/*
		registration.Register_db("dbic", LOCK_EXCLUSIVE)
		if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
			fmt.Println("TIMEOUT", 1)
		}

		registration.Register_object("dbic", "dbo", "agences", LOCK_SHARED, 0)
		if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
			fmt.Println("TIMEOUT", 1)
		}
	*/
	time.Sleep(10 * time.Second)
	fmt.Println("----------------------------------")

	G_LOCK_MANAGER.Release(registration)

	G_LOCK_MANAGER.Debug_print()

AA:

	select {}
}

func f4() {
	registration := New_registration(4)

	registration.Register_db("coucou", LOCK_EXCLUSIVE)

	//time.Sleep(1 * time.Second)

	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 4)
	}

	time.Sleep(20 * time.Second)
	G_LOCK_MANAGER.Release(registration)

	fmt.Println("--------------- 4 -------------------")

	G_LOCK_MANAGER.Release(registration)

	fmt.Println("xxx")
	G_LOCK_MANAGER.Debug_print()

}
func f5() {
	if xxx == 0 {
		return
	}

	registration := New_registration(5)

	registration.Register_db("dwh", LOCK_SHARED)
	registration.Register_db("dbic", LOCK_SHARED)

	time.Sleep(1 * time.Second)

	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 5)
		return
	}

	time.Sleep(1 * time.Second)
	G_LOCK_MANAGER.Release(registration)
}

func f6() {
	if xxx == 0 {
		return
	}

	registration := New_registration(6)

	registration.Register_db("dwh", LOCK_SHARED)
	registration.Register_db("dbic", LOCK_SHARED)

	time.Sleep(1 * time.Second)

	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 6)
		return
	}

	time.Sleep(1 * time.Second)
	G_LOCK_MANAGER.Release(registration)
}

func f7() {
	if xxx == 0 {
		return
	}

	registration := New_registration(7)

	registration.Register_db("dwh", LOCK_EXCLUSIVE)
	registration.Register_db("dbic", LOCK_SHARED)

	time.Sleep(1 * time.Second)

	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 7)
		return
	}

	time.Sleep(1 * time.Second)
	G_LOCK_MANAGER.Release(registration)
}

func f8() {
	fmt.Println("run 8")
	if xxx == 0 {
		return
	}

	registration := New_registration(8)

	registration.Register_db("dwh", LOCK_SHARED)
	registration.Register_db("dbic", LOCK_SHARED)

	time.Sleep(1000 * time.Millisecond)

	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 8)
		return
	}

	time.Sleep(1 * time.Second)
	G_LOCK_MANAGER.Release(registration)
}

func f9() {
	if xxx == 0 {
		return
	}

	registration := New_registration(9)

	registration.Register_db("dwh", LOCK_SHARED)
	registration.Register_db("dbic", LOCK_SHARED)

	time.Sleep(1 * time.Second)

	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 9)
		return
	}

	time.Sleep(1 * time.Second)
	G_LOCK_MANAGER.Release(registration)
}

func f10() {
	if xxx == 0 {
		return
	}
	time.Sleep(0 * time.Second)

	registration := New_registration(10)

	registration.Register_db("dwh", LOCK_SHARED)
	registration.Register_db("dbic", LOCK_SHARED)

	time.Sleep(1 * time.Second)

	if ans := G_LOCK_MANAGER.Acquire(registration); ans == ANSWER_TIMEOUT {
		fmt.Println("TIMEOUT", 10)
		return
	}

	time.Sleep(1 * time.Second)
	G_LOCK_MANAGER.Release(registration)
}
