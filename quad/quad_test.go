package quad

import (
	"math"
	"testing"

	"rsql"
)

func float64_equal(a float64, r float64) bool {

	const (
		EPSILON_ZERO  float64 = 1e-300    // used to compute equality of two float64, when value to check is close to zero
		EPSILON_RATIO float64 = 0.0000001 // used to compute equality of two float64. It is good enough for testing.
	)

	if math.Abs(r) < EPSILON_ZERO { // if check value is close to zero
		if math.Abs(a) < EPSILON_ZERO {
			return true
		}
		return false
	}

	if math.Abs((a-r)/r) < EPSILON_RATIO {
		return true
	}

	return false
}

func Test_quad_init(t *testing.T) {

	//	fmt.Println(DECQUAD_Pmax)

}

func Test_from_int32_raw(t *testing.T) {
	var (
		a MyQuad
	)

	var samples = []struct {
		in  int32
		out string
	}{
		{1, "1"},
		{-123456, "-123456"},
		{math.MaxInt32, "2147483647"},
		{math.MinInt32, "-2147483648"},
	}

	for _, sample := range samples {

		in := sample.in
		out := sample.out

		From_int32_raw(&a, in)

		if a.String_raw() != out {
			t.Fatalf("error during conversion of \"%d\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
		}
	}

}

func Test_from_int32(t *testing.T) {
	var (
		a        MyQuad
		rsql_err *rsql.Error
	)

	var samples = []struct {
		in            int32
		res_precision uint16
		res_scale     uint16
		out           string
		message_id    rsql.Message_id_t
	}{
		{1, 6, 2, "100e-2", 0},
		{-123456, 6, 0, "-123456", 0},
		{-123456, 5, 0, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{math.MaxInt32, 10, 0, "2147483647", 0},
		{math.MinInt32, 10, 0, "-2147483648", 0},
	}

	for _, sample := range samples {

		in := sample.in
		res_precision := sample.res_precision
		res_scale := sample.res_scale
		out := sample.out
		message_id := sample.message_id

		rsql_err = From_int32(&a, res_precision, res_scale, in)

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%d\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if a.String_raw() != out {
				t.Fatalf("error during conversion of \"%d\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%d\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%d\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}

	}
}

func Test_from_bytes_and_string_raw(t *testing.T) {
	var (
		a        MyQuad
		rsql_err *rsql.Error
	)

	var samples = []struct {
		in         string
		out        string
		message_id rsql.Message_id_t
	}{
		{"", "", rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"    +Inf    ", "+Inf", 0},
		{"    Nan    ", "Nan", 0},
		{"   123   ", "123", 0},
		{"123e6000", "123e6000", 0},
		{"1.23", "123e-2", 0},
		{"-1.23", "-123e-2", 0},
		{"1.23e30", "123e28", 0},
		{"    123e7000    ", "+Inf", 0},
		{"-123e7000", "-Inf", 0},
		{"123e-7000", "0e-6176", 0},
		{"123a", "", rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"aaa", "", rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"0100", "100", 0},
		{"12345678901234567890123456789012.34", "1234567890123456789012345678901234e-2", 0},
		{"99999999999999999999999999999999.99", "9999999999999999999999999999999999e-2", 0},
		{"99999999999999999999999999999999.995", "1000000000000000000000000000000000e-1", 0},
		{"9999999999999999999999999999999999", "9999999999999999999999999999999999", 0},
		{"9999999999999999999999999999999999.3", "9999999999999999999999999999999999", 0},
		{"9999999999999999999999999999999999.5", "1000000000000000000000000000000000e1", 0},
		{"0.99999999999999999999999999999999993", "9999999999999999999999999999999999e-34", 0},
		{"0.99999999999999999999999999999999995", "1000000000000000000000000000000000e-33", 0},
	}

	for _, sample := range samples {

		in := sample.in
		out := sample.out
		message_id := sample.message_id

		// From_bytes_raw

		rsql_err = From_bytes_raw(&a, []byte(in))

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if a.String_raw() != out {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}

		From_string_raw(&a, "75639") // put garbage

		// From_string_raw

		rsql_err = From_string_raw(&a, in)

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if a.String_raw() != out {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}
	}

}

func Test_from_bytes_and_string(t *testing.T) {
	var (
		a        MyQuad
		rsql_err *rsql.Error
	)

	var samples = []struct {
		in            string
		res_precision uint16
		res_scale     uint16
		out           string
		message_id    rsql.Message_id_t
	}{
		{"", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"    +Inf    ", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"    Nan    ", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_NAN},
		{"   123   ", 6, 2, "12300e-2", 0},
		{"    123e6000", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"    123e7000", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"1.23", 6, 2, "123e-2", 0},
		{"-1.23", 6, 2, "-123e-2", 0},
		{"1.23e30", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"-123e7000", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"123e-7000", 6, 2, "0e-2", 0},
		{"123a", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"aaa", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"0100", 6, 2, "10000e-2", 0},
		{"1234.555", 6, 2, "123456e-2", 0},
		{"9999.99", 6, 2, "999999e-2", 0},
		{"9999.995", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"-9999.99", 6, 2, "-999999e-2", 0},
		{"-9999.995", 6, 2, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"12345678901234567890123456789012.34", 34, 2, "1234567890123456789012345678901234e-2", 0},
		{"99999999999999999999999999999999.99", 34, 2, "9999999999999999999999999999999999e-2", 0},
		{"99999999999999999999999999999999.995", 34, 2, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"9999999999999999999999999999999999", 34, 0, "9999999999999999999999999999999999", 0},
		{"9999999999999999999999999999999999.3", 34, 0, "9999999999999999999999999999999999", 0},
		{"9999999999999999999999999999999999.5", 34, 0, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"0.99999999999999999999999999999999993", 34, 34, "9999999999999999999999999999999999e-34", 0},
		{"0.99999999999999999999999999999999995", 34, 34, "", rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"0.999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999", 34, 34, "", rsql.ERROR_SQLDATA_NUMERIC_INVALID_TEXT_TOO_LONG},
	}

	for _, sample := range samples {

		in := sample.in
		res_precision := sample.res_precision
		res_scale := sample.res_scale
		out := sample.out
		message_id := sample.message_id

		// From_bytes_raw

		rsql_err = From_bytes(&a, res_precision, res_scale, []byte(in))

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if a.String_raw() != out {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}

		From_string_raw(&a, "75639") // put garbage

		// From_string_raw

		rsql_err = From_string(&a, res_precision, res_scale, in)

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if a.String_raw() != out {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}
	}

}

func Test_from_bytes_and_string_with_implied_p_s(t *testing.T) {
	var (
		a         MyQuad
		rsql_err  *rsql.Error
		precision uint16
		scale     uint16
	)

	var samples = []struct {
		in            string
		out           string
		out_precision uint16
		out_scale     uint16
		message_id    rsql.Message_id_t
	}{
		{"", "", 0, 0, rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"Nan", "", 0, 0, rsql.ERROR_SQLDATA_NUMERIC_NAN},
		{"Inf", "", 0, 0, rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"-Inf", "", 0, 0, rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"12aa", "", 0, 0, rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX},
		{"1.23", "123e-2", 3, 2, 0},
		{"0", "0", 1, 0, 0},
		{"  1  ", "1", 1, 0, 0},
		{"-1", "-1", 1, 0, 0},
		{"12", "12", 2, 0, 0},
		{"12e2", "1200", 4, 0, 0},
		{"8e33", "8000000000000000000000000000000000", 34, 0, 0},
		{"8.123e33", "8123000000000000000000000000000000", 34, 0, 0},
		{"8e-2", "8e-2", 2, 2, 0},
		{"1234567890123456789012345678901234", "1234567890123456789012345678901234", 34, 0, 0},
		{"-1234567890123456789012345678901234", "-1234567890123456789012345678901234", 34, 0, 0},
		{"1234567890123456789012345678901234.4", "1234567890123456789012345678901234", 34, 0, 0},
		{"1234567890123456789012345678901234.6", "1234567890123456789012345678901235", 34, 0, 0},
		{"9999999999999999999999999999999999.4", "9999999999999999999999999999999999", 34, 0, 0},
		{"9999999999999999999999999999999999.6", "9999999999999999999999999999999999", 0, 0, rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{".99999999999999999999999999999999994", "9999999999999999999999999999999999e-34", 34, 34, 0},
		{".99999999999999999999999999999999996", "1000000000000000000000000000000000e-33", 34, 33, 0},
		{"12345678901234567890123456789012.34", "1234567890123456789012345678901234e-2", 34, 2, 0},
		{"99999999999999999999999999999999.99", "9999999999999999999999999999999999e-2", 34, 2, 0},
		{"99999999999999999999999999999999.995", "1000000000000000000000000000000000e-1", 34, 1, 0},
		{"9999999999999999999999999999999999", "9999999999999999999999999999999999", 34, 0, 0},
		{"9999999999999999999999999999999999.3", "9999999999999999999999999999999999", 34, 0, 0},
		{"9999999999999999999999999999999999.5", "", 0, 0, rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},
		{"0.99999999999999999999999999999999993", "9999999999999999999999999999999999e-34", 34, 34, 0},
		{"0.99999999999999999999999999999999995", "1000000000000000000000000000000000e-33", 34, 33, 0},
	}

	for _, sample := range samples {

		in := sample.in
		out := sample.out
		out_precision := sample.out_precision
		out_scale := sample.out_scale
		message_id := sample.message_id

		// From_bytes_raw

		precision, scale, rsql_err = From_bytes_with_implied_p_s(&a, []byte(in))

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if a.String_raw() != out {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
			}

			if precision != out_precision || scale != out_scale {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\" with precision scale %d %d, expected %d %d", in, a.String_raw(), precision, scale, out_precision, out_scale)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}

		From_string_raw(&a, "75639") // put garbage

		// From_string_raw

		precision, scale, rsql_err = From_string_with_implied_p_s(&a, in)

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if a.String_raw() != out {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, a.String_raw(), out)
			}

			if precision != out_precision || scale != out_scale {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\" with precision scale %d %d, expected %d %d", in, a.String_raw(), precision, scale, out_precision, out_scale)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}
	}

}

func Test_String(t *testing.T) {
	var (
		a MyQuad
	)

	var samples = []struct {
		in  string
		out string
	}{
		{"Nan", "NaN"},
		{"   1   ", "1"},
		{"-123456", "-123456"},
		{"2147483647", "2147483647"},
		{"-2147483648", "-2147483648"},
		{"214748.3647", "214748.3647"},
		{"1234567890123456789012345678901234", "1234567890123456789012345678901234"},
		{"1000000000000000000000000000000000", "1000000000000000000000000000000000"},
		{"1000000000000000000000000000000000.", "1000000000000000000000000000000000"},
		{".1234567890123456789012345678901234", "0.1234567890123456789012345678901234"},
		{".1000000000000000000000000000000000", "0.1000000000000000000000000000000000"},
		{"14e20", "1.4E+21"},
		{"14e200", "1.4E+201"},
		{"140e40", "1.40E+42"},
		{"1000000000000000000.000000000000000", "1000000000000000000.000000000000000"},
		{"100000.00000000000000", "100000.00000000000000"},
		{"12345678901234567890123456789012345", "1.234567890123456789012345678901235E+34"},
		{"12345678901234567890123456789012346", "1.234567890123456789012345678901235E+34"},
		{"1e9000", "Infinity"},
		{"-1e9000", "-Infinity"},
		{"1e-9000", "0E-6176"},
		{"1234567890123456789012345678901234612345678901234567890123456789012346", "1.234567890123456789012345678901235E+69"},
	}

	for _, sample := range samples {

		in := sample.in
		out := sample.out

		if rsql_err := From_string_raw(&a, in); rsql_err != nil {
			t.Fatalf("error during creation of \"%s\". Got error \"%s\"", in, rsql_err.Message_id)
		}

		if a.String() != out {
			t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, a.String(), out)
		}

	}

}

func Test_To_int32_truncate(t *testing.T) {
	var (
		a        MyQuad
		rsql_err *rsql.Error
		r        int32
	)

	var samples = []struct {
		in         string
		out        int32
		message_id rsql.Message_id_t
	}{
		{"1", 1, 0},
		{"    +Inf    ", 0, rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"    Nan    ", 0, rsql.ERROR_SQLDATA_NUMERIC_NAN},
		{"   123   ", 123, 0},
		{"0.0000000000001", 0, 0},
		{"123e600", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"123e6000", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"1.23", 1, 0},
		{"2147483647", math.MaxInt32, 0},
		{"2147483648", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"-2147483648", math.MinInt32, 0},
		{"-2147483648.4", math.MinInt32, 0},
		{"-2147483648.6", math.MinInt32, 0},
		{"-2147483649", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
	}

	for _, sample := range samples {

		in := sample.in
		out := sample.out
		message_id := sample.message_id

		if rsql_err = From_string_raw(&a, in); rsql_err != nil {
			t.Fatalf("error during creation of \"%s\". Got error \"%s\"", in, rsql_err.Message_id)
		}

		r, rsql_err = To_int32_truncate(&a)

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if r != out {
				t.Fatalf("error during conversion of \"%s\". Got %d, expected %d", in, r, out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}
	}

}

func Test_To_int64_truncate(t *testing.T) {
	var (
		a        MyQuad
		rsql_err *rsql.Error
		r        int64
	)

	var samples = []struct {
		in         string
		out        int64
		message_id rsql.Message_id_t
	}{
		{"1", 1, 0},
		{"    +Inf    ", 0, rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"    Nan    ", 0, rsql.ERROR_SQLDATA_NUMERIC_NAN},
		{"   123   ", 123, 0},
		{"0.0000000000001", 0, 0},
		{"123e600", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"123e6000", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"1.23", 1, 0},
		{"9223372036854775807", math.MaxInt64, 0},
		{"9223372036854775808", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"-9223372036854775808", math.MinInt64, 0},
		{"-9223372036854775808.4", math.MinInt64, 0},
		{"-9223372036854775808.6", math.MinInt64, 0},
		{"-9223372036854775809", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"1000000000000000000", 1000000000000000000, 0},
		{"1e2", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
	}

	for _, sample := range samples {

		in := sample.in
		out := sample.out
		message_id := sample.message_id

		if rsql_err = From_string_raw(&a, in); rsql_err != nil {
			t.Fatalf("error during creation of \"%s\". Got error \"%s\"", in, rsql_err.Message_id)
		}

		r, rsql_err = To_int64_truncate(&a)

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if r != out {
				t.Fatalf("error during conversion of \"%s\". Got %d, expected %d", in, r, out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}
	}

}

func Test_To_double(t *testing.T) {
	var (
		a        MyQuad
		rsql_err *rsql.Error
		r        float64
	)

	var samples = []struct {
		in         string
		out        float64
		message_id rsql.Message_id_t
	}{
		{"1", 1, 0},
		{"    +Inf    ", 0, rsql.ERROR_SQLDATA_NUMERIC_INFINITE},
		{"    Nan    ", 0, rsql.ERROR_SQLDATA_NUMERIC_NAN},
		{"   123   ", 123, 0},
		{"0.0000000000001", 1e-13, 0},
		{"0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001", 1e-98, 0},
		{"123e600", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"123e6000", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"1.23", 1.23, 0},
		{"9223372036854775807", math.MaxInt64, 0},
		{"9223372036854775808", 9223372036854775808, 0},
		{"-9223372036854775808", math.MinInt64, 0},
		{"-9223372036854775808.4", math.MinInt64, 0},
		{"-9223372036854775808.6", math.MinInt64, 0},
		{"-9223372036854775809", -9223372036854775809, 0},
		{"1000000000000000000", 1000000000000000000, 0},
		{"1e308", 1e308, 0},
		{"1e309", 0, rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE},
		{"9223372036.854775808", 9223372036.854775808, 0},
	}

	for _, sample := range samples {

		in := sample.in
		out := sample.out
		message_id := sample.message_id

		if rsql_err = From_string_raw(&a, in); rsql_err != nil {
			t.Fatalf("error during creation of \"%s\". Got error \"%s\"", in, rsql_err.Message_id)
		}

		r, rsql_err = To_float64(&a)

		switch {
		case message_id == 0:
			if rsql_err != nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\"", in, rsql_err.Message_id)
			}

			if float64_equal(out, r) == false {
				t.Fatalf("error during conversion of \"%s\". Got %g, expected %g", in, r, out)
			}

		default: // error expected
			if rsql_err == nil {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, "nil", message_id)
			}

			if rsql_err.Message_id != message_id {
				t.Fatalf("error during conversion of \"%s\". Got \"%s\", expected \"%s\"", in, rsql_err.Message_id, message_id)
			}
		}
	}

}

func Test_compress(t *testing.T) {
	var (
		a        MyQuad
		res      MyQuad
		dest     []byte
		length   uint16
		rsql_err *rsql.Error
	)

	var samples = []struct {
		in string
	}{
		{"1"},
		{"0"},
		{"    +Inf    "},
		{"    Nan    "},
		{"   123   "},
		{"0.0000000000001"},
		{"0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001"},
		{"123e60"},
		{"1.23"},
		{"9223372036854775807"},
		{"9223372036854775808"},
		{"-9223372036854775808"},
		{"-9223372036854775808.4"},
		{"-9223372036854775808.6"},
		{"-9223372036854775809"},
		{"1000000000000000000"},
		{"1e308"},
		{"9223372036.854775808"},
		{"-9223372036854775808.4e-42"},
		{"-9223372036854775808.4e142"},
		{"1234567890123456789012345678901234"},
		{"-1234567890123456789012345678901234"},
		{"1234567890123456789012345678901234e123"},
		{"1234567890123456789012345678901234e-123"},
		{"9999999999999999999999999999999999"},
		{"-9999999999999999999999999999999999"},
		{"1234567890123456.789012345678901234"},
		{"-123456789012345.6789012345678901234"},
	}

	for _, sample := range samples {

		in := sample.in

		if rsql_err = From_string_raw(&a, in); rsql_err != nil {
			t.Fatalf("error during creation of \"%s\". Got error \"%s\"", in, rsql_err.Message_id)
		}

		dest, length = Append_compressed_bytes(dest[:0], a)
		if int(length) != len(dest) {
			t.Fatalf("error for \"%s\".", in)
		}

		res = Uncompress_bytes(dest)

		if res != a { // bit equality
			t.Fatalf("equality error for \"%s\". %s != %s", in, a.String(), res.String())
		}
	}

}
