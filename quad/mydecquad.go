package quad

/*
#cgo CFLAGS: -Wall -Wno-unused-value
#include "mydecquad.h"
*/
import "C"

import (
	"bytes"
	"fmt"
	"strings"
	"unsafe"

	"rsql"
)

// Note: in the comment block for cgo above, the path for LDFLAGS must be an "absolute" path.
//       If it is a relative path, it seems that it is relative to the current directory when "go build" is run.
//       As we want to run "go build rsql/quad" from any location, the path must be absolute.
//       See   Issue 5428 in May 2013: cmd/ld: relative #cgo LDFLAGS -L does not work
//             This problem is still not resolved in May 2014.

// Note: CFLAGS directive:
//       On Linux, cgo doesn't activate warnings when compiling .c files to object files.
//       They are enabled on OS X, though.
//       That's why I must add the warning flags here.
//       As the decNumber library displays many -Wunused-value warnings, I disable them.
//
//       cgo bug: the flag -Wall will raise a [-Wunused-variable] warning for functions with the signature  void f(void).
//       This is certainly due to a bug in cgo.

// MyQuad is a struct that just contains a C.decQuad value.
type MyQuad struct {
	mq_val C.decQuad
}

const (
	S_DECQUAD_Pmax        = C.DECQUAD_Pmax          // number of digits in coefficient
	S_DECQUAD_Bytes       = C.DECQUAD_Bytes         // size in bytes of decQuad
	S_DECQUAD_String      = C.DECQUAD_String        // buffer capacity for C.decQuadToString()
	S_STRING_RAW_CAPACITY = C.S_STRING_RAW_CAPACITY // buffer capacity for C.mdq_to_mallocated_string_raw()
)

var G_DECQUAD_MACROS string // for information

/************************************************************************/
/*                                                                      */
/*                            init function                             */
/*                                                                      */
/************************************************************************/

func init() {

	C.mdq_init()

	G_DECQUAD_MACROS = fmt.Sprintf("decQuad module: DECDPUN %d, DECSUBSET %d, DECEXTFLAG %d. Constants DECQUAD_Pmax %d, DECQUAD_String %d DECQUAD_Bytes %d.", C.DECDPUN, C.DECSUBSET, C.DECEXTFLAG, C.DECQUAD_Pmax, C.DECQUAD_String, C.DECQUAD_Bytes)

	// check that rsql.DATATYPE_NUMERIC_PRECISION_MAX has correct value.

	if rsql.DATATYPE_NUMERIC_PRECISION_MAX != C.DECQUAD_Pmax {
		panic("rsql.DATATYPE_NUMERIC_PRECISION_MAX must be equal to C.DECQUAD_Pmax.")
	}

	if S_DECQUAD_Bytes != 16 { // S_DECQUAD_Bytes MUST NOT BE > 16, because Append_compressed_bytes() will silently fail if it is not the case
		panic("S_DECQUAD_Bytes != 16")
	}

}

/************************************************************************/
/*                                                                      */
/*                          utility functions                           */
/*                                                                      */
/************************************************************************/

// get_rsql_error_message_id converts C.xxx error code into rsql.ERROR_xxx error code.
// All C.MDQ_ERROR_DEC_XXX errors come from the C decNumber library.
//
func get_rsql_error_message_id(mdqerr C.uint32_t) rsql.Message_id_t {

	switch mdqerr {
	case C.MDQ_ERROR_INFINITE:
		return rsql.ERROR_SQLDATA_NUMERIC_INFINITE
	case C.MDQ_ERROR_NAN:
		return rsql.ERROR_SQLDATA_NUMERIC_NAN
	case C.MDQ_ERROR_OVERFLOW:
		return rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW
	case C.MDQ_ERROR_OUT_OF_RANGE:
		return rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE

	case C.MDQ_ERROR_DEC_UNLISTED:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_UNLISTED
	case C.MDQ_ERROR_DEC_INVALID_OPERATION:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_INVALID_OPERATION
	case C.MDQ_ERROR_DEC_DIVISION_BY_ZERO:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_DIVISION_BY_ZERO
	case C.MDQ_ERROR_DEC_OVERFLOW:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_OVERFLOW
	case C.MDQ_ERROR_DEC_UNDERFLOW:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_UNDERFLOW
	case C.MDQ_ERROR_DEC_DIVISION_IMPOSSIBLE:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_DIVISION_IMPOSSIBLE
	case C.MDQ_ERROR_DEC_DIVISION_UNDEFINED:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_DIVISION_UNDEFINED
	case C.MDQ_ERROR_DEC_CONVERSION_SYNTAX:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_CONVERSION_SYNTAX
	case C.MDQ_ERROR_DEC_INSUFFICIENT_STORAGE:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_INSUFFICIENT_STORAGE
	case C.MDQ_ERROR_DEC_INVALID_CONTEXT:
		return rsql.ERROR_SQLDATA_NUMERIC_DEC_INVALID_CONTEXT
	}

	panic("never get here")
}

// Plain_zero sets argument to 0.
//
func Plain_zero(r *MyQuad) {

	r.mq_val = C.mdq_decQuadZero(r.mq_val)
}

// Is_zero checks if argument == 0
//
func Is_zero(r *MyQuad) bool {
	var res C.uint32_t

	res = C.mdq_decQuadIsZero(r.mq_val)

	if res == 0 {
		return false
	}

	return true
}

// Is_negative checks if argument < 0.
// If 0 or positive, returns false.
//
func Is_negative(r *MyQuad) bool {
	var res C.uint32_t

	res = C.mdq_decQuadIsNegative(r.mq_val) // 1 if value is less than zero and not a NaN, or 0 otherwise.

	if res == 0 {
		return false
	}

	return true
}

/************************************************************************/
/*                                                                      */
/*                      arithmetic operations                           */
/*                                                                      */
/************************************************************************/

func Hash(a *MyQuad) uint32 {
	var hash C.uint32_t

	hash = C.mdq_hash(a.mq_val)

	return uint32(hash)
}

func Unary_minus(r *MyQuad, precision uint16, scale uint16, a *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_unary_minus(C.uint16_t(precision), C.uint16_t(scale), a.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Add(r *MyQuad, precision uint16, scale uint16, a *MyQuad, b *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_add(C.uint16_t(precision), C.uint16_t(scale), a.mq_val, b.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Subtract(r *MyQuad, precision uint16, scale uint16, a *MyQuad, b *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_subtract(C.uint16_t(precision), C.uint16_t(scale), a.mq_val, b.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Multiply(r *MyQuad, precision uint16, scale uint16, a *MyQuad, b *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_multiply(C.uint16_t(precision), C.uint16_t(scale), a.mq_val, b.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Divide(r *MyQuad, precision uint16, scale uint16, a *MyQuad, b *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_divide(C.uint16_t(precision), C.uint16_t(scale), a.mq_val, b.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

// Compare compares a with b.
// returns 1 (greater), 0 (equal), or -1 (less)
// Never fails.
// Nan < -Inf < normal number < +Inf
//
func Compare(a *MyQuad, b *MyQuad) int {
	var res C.int32_t

	res = C.mdq_compare(a.mq_val, b.mq_val)

	return int(res)
}

// Compare compares a with b.
// returns 1 (greater), 0 (equal), or -1 (less)
// Never fails.
//
//     *** THIS FUNCTION MUST BE USED ONLY FOR TESTS ***
//         In tests, we want to compare not only the value of a and b, but also their exponents.
//         E.g. "12.5" != "12.50"
//
func Check_equality_FOR_TEST(a *MyQuad, b *MyQuad) bool {
	var res C.int32_t

	res = C.mdq_check_equality_FOR_TEST(a.mq_val, b.mq_val)

	return int(res) != 0
}

func Zero(r *MyQuad, precision uint16, scale uint16) {

	r.mq_val = C.mdq_zero(C.uint16_t(precision), C.uint16_t(scale))
}

func Copy(r *MyQuad, precision uint16, scale uint16, a *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_copy(C.uint16_t(precision), C.uint16_t(scale), a.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Abs(r *MyQuad, precision uint16, scale uint16, a *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_abs(C.uint16_t(precision), C.uint16_t(scale), a.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Ceiling(r *MyQuad, precision uint16, scale uint16, a *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_ceiling(C.uint16_t(precision), C.uint16_t(scale), a.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Floor(r *MyQuad, precision uint16, scale uint16, a *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_floor(C.uint16_t(precision), C.uint16_t(scale), a.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Sign(r *MyQuad, precision uint16, scale uint16, a *MyQuad) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_sign(C.uint16_t(precision), C.uint16_t(scale), a.mq_val); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Power(r *MyQuad, precision uint16, scale uint16, a *MyQuad, b float64) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_power(C.uint16_t(precision), C.uint16_t(scale), a.mq_val, C.double(b)); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Round(r *MyQuad, precision uint16, scale uint16, a *MyQuad, a_precision uint16, a_scale uint16, b int32, truncate_flag uint8) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_round(C.uint16_t(precision), C.uint16_t(scale), a.mq_val, C.uint16_t(a_precision), C.uint16_t(a_scale), C.int32_t(b), C.uint8_t(truncate_flag)); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

func Round_for_formatting(r *MyQuad, a *MyQuad, b int32) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_round_for_formatting(a.mq_val, C.int32_t(b)); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

/************************************************************************/
/*                                                                      */
/*                      conversion operations                           */
/*                                                                      */
/************************************************************************/

// Max text length that is convertible to a MyQuad value, by From_bytes_raw(), From_bytes() or From_string().
const TEXT_MAX_LENGTH_FOR_CONVERSION = 100

// From_int32 write value into MyQuad.
// The result number is not constrained by any precision or scale.
//
func From_int32_raw(r *MyQuad, value int32) {
	var res C.decQuad

	res = C.mdq_from_int32_raw(C.int32_t(value))

	r.mq_val = res
}

// From_int32 write value into MyQuad.
//
func From_int32(r *MyQuad, precision uint16, scale uint16, value int32) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_from_int32(C.uint16_t(precision), C.uint16_t(scale), C.int32_t(value)); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

// From_int64 write value into MyQuad.
// The result number is not constrained by any precision or scale.
//
func From_int64_raw(r *MyQuad, value int64) {
	var res C.decQuad

	res = C.mdq_from_int64_raw(C.int64_t(value))

	r.mq_val = res
}

// From_int64 write value into MyQuad.
//
func From_int64(r *MyQuad, precision uint16, scale uint16, value int64) *rsql.Error {
	var ret C.Ret_decQuad

	if ret = C.mdq_from_int64(C.uint16_t(precision), C.uint16_t(scale), C.int64_t(value)); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

// Byteslice2Cstring creates a copy of bs in C heap, and returns a pointer to it.
//
func Byteslice2Cstring(bs []byte) *C.char {
	var (
		p         unsafe.Pointer
		bs_into_c []byte
		n         int
	)

	n = len(bs)

	if p = C.malloc(C.size_t(n + 1)); p == nil { // +1 for terminating 0
		panic("ENOMEM: C heap malloc() failed.")
	}

	bs_into_c = (*[0x7fffffff]byte)(p)[:n+1]
	for i := 0; i < n; i++ {
		bs_into_c[i] = bs[i]
	}
	bs_into_c[n] = 0

	return (*C.char)(p)
}

// Byteslice_into_C creates a byteslice, with p (pointer into C heap) as underlying array.
// length is string length, not capacity.
//
func Byteslice_into_C(p *C.char, length C.size_t) []byte {
	var (
		bs_into_c []byte
	)

	bs_into_c = (*[0x7fffffff]byte)(unsafe.Pointer(p))[:length]

	return bs_into_c
}

// From_bytes_raw writes the value of a byte slice ascii text, converted to numeric, into MyQuad.
// Leading and trailing spaces in value argument are trimmed.
// Returns error if invalid text.
//
// Argument can be "Inf", "-Inf" or "Nan", and no error code is returned in these cases.
//
// The result number is not constrained by any precision or scale.
//
func From_bytes_raw(r *MyQuad, value []byte) *rsql.Error {
	var (
		p   *C.char
		ret C.Ret_decQuad
	)

	value = bytes.TrimSpace(value)

	if len(value) > TEXT_MAX_LENGTH_FOR_CONVERSION {
		return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_INVALID_TEXT_TOO_LONG, rsql.ERROR_BATCH_ABORT, value)
	}

	p = Byteslice2Cstring(value) // C string is terminated by 0

	if ret = C.mdq_from_bytes_raw_and_free(p); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

// From_string_raw writes the value of a string, converted to numeric, into MyQuad.
// Leading and trailing spaces in value argument are trimmed.
// Returns error if invalid text.
//
// Argument can be "Inf", "-Inf" or "Nan", and no error code is returned in these cases.
//
// The result number is not constrained by any precision or scale.
//
func From_string_raw(r *MyQuad, value string) *rsql.Error {
	var (
		p   *C.char
		ret C.Ret_decQuad
	)

	value = strings.TrimSpace(value)

	if len(value) > TEXT_MAX_LENGTH_FOR_CONVERSION {
		return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_INVALID_TEXT_TOO_LONG, rsql.ERROR_BATCH_ABORT, value)
	}

	p = C.CString(value) // C string is terminated by 0

	if ret = C.mdq_from_bytes_raw_and_free(p); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

// From_bytes writes the value of a byte slice ascii text, converted to numeric, into MyQuad.
// Leading and trailing spaces in value argument are trimmed.
// Returns error if invalid text.
//
func From_bytes(r *MyQuad, precision uint16, scale uint16, value []byte) *rsql.Error {
	var (
		p   *C.char
		ret C.Ret_decQuad
	)

	value = bytes.TrimSpace(value)

	if len(value) > TEXT_MAX_LENGTH_FOR_CONVERSION {
		return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_INVALID_TEXT_TOO_LONG, rsql.ERROR_BATCH_ABORT, value)
	}

	p = Byteslice2Cstring(value) // C string is terminated by 0

	if ret = C.mdq_from_bytes_and_free(C.uint16_t(precision), C.uint16_t(scale), p); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

// From_string writes the value of a string, converted to numeric, into MyQuad.
// Leading and trailing spaces in value argument are trimmed.
// Returns error if invalid text.
//
func From_string(r *MyQuad, precision uint16, scale uint16, value string) *rsql.Error {
	var (
		p   *C.char
		ret C.Ret_decQuad
	)

	value = strings.TrimSpace(value)

	if len(value) > TEXT_MAX_LENGTH_FOR_CONVERSION {
		return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_INVALID_TEXT_TOO_LONG, rsql.ERROR_BATCH_ABORT, value)
	}

	p = C.CString(value) // C string is terminated by 0

	if ret = C.mdq_from_bytes_and_free(C.uint16_t(precision), C.uint16_t(scale), p); ret.mdqerr != 0 {
		return rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return nil
}

// From_bytes_with_implied_p_s writes the value of a byte slice ascii text, converted to numeric, into MyQuad.
// Leading and trailing spaces in value argument are trimmed.
// Precision and scale implied by the string value are returned (e.g. "123.45" returns <5,2,nil> ).
// Precision and scale must be valid for NUMERIC type. Else, an error is returned.
// Returns error if invalid text.
//
func From_bytes_with_implied_p_s(r *MyQuad, value []byte) (precision uint16, scale uint16, rsql_err *rsql.Error) {
	var (
		p   *C.char
		ret C.Ret_decQuad_p_s
	)

	value = bytes.TrimSpace(value)

	if len(value) > TEXT_MAX_LENGTH_FOR_CONVERSION {
		return 0, 0, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_INVALID_TEXT_TOO_LONG, rsql.ERROR_BATCH_ABORT, value)
	}

	p = Byteslice2Cstring(value) // C string is terminated by 0

	if ret = C.mdq_from_bytes_with_implied_p_s_and_free(p); ret.mdqerr != 0 {
		return 0, 0, rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return uint16(ret.precision), uint16(ret.scale), nil
}

// From_string_with_implied_p_s writes the value of a string, converted to numeric, into MyQuad.
// Leading and trailing spaces in value argument are trimmed.
// Precision and scale implied by the string value are returned (e.g. "123.45" returns <5,2,nil> ).
// Precision and scale must be valid for NUMERIC type. Else, an error is returned.
// Returns error if invalid text.
//
func From_string_with_implied_p_s(r *MyQuad, value string) (precision uint16, scale uint16, rsql_err *rsql.Error) {
	var (
		p   *C.char
		ret C.Ret_decQuad_p_s
	)

	value = strings.TrimSpace(value)

	if len(value) > TEXT_MAX_LENGTH_FOR_CONVERSION {
		return 0, 0, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_INVALID_TEXT_TOO_LONG, rsql.ERROR_BATCH_ABORT, value)
	}

	p = C.CString(value) // C string is terminated by 0

	if ret = C.mdq_from_bytes_with_implied_p_s_and_free(p); ret.mdqerr != 0 {
		return 0, 0, rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	r.mq_val = ret.val
	return uint16(ret.precision), uint16(ret.scale), nil
}

// AppendQuad appends string representation of decQuad into byte slice.
// This representation is the BEST TO DISPLAY decQuad, because it shows all numbers having exponent between 0 and -34 (DECQUAD_Pmax), that is, all numbers in the NUMERIC range, without using exponent notation.
//
// All digits of the coefficient are displayed, e.g. 12344567890.123456789000000000000000
// If the number must have an exponent because it is too large or too small, we can have    1.4E+201     0E-6176    Infinity
//
func AppendQuad(dst []byte, a *MyQuad) []byte {
	var (
		ret_str   C.Ret_str
		str_slice []byte

		ret               C.Ret_BCD
		d                 byte
		skip_leading_zero bool = true
		mdqerr            C.uint32_t
		exp               int32
		sign              uint32
		BCD_slice         []byte

		buff [S_DECQUAD_String]byte // array size is max of DECQUAD_String and DECQUAD_Pmax. DECQUAD_String is larger.
	)

	// fill BCD array

	ret = C.mdq_to_mallocated_BCD(a.mq_val) // sign will be 1 for negative and non-zero number, else, 0. If Inf or Nan, returns an error.
	BCD_slice = Byteslice_into_C(ret.BCD, ret.capacity)
	exp = int32(ret.exp)
	sign = uint32(ret.sign)
	mdqerr = ret.mdqerr

	if exp > 0 || exp < -S_DECQUAD_Pmax || mdqerr != 0 { // if decQuad value is not in NUMERIC range, or Inf or Nan, we want our function to output the number, or Infinity, or NaN.
		ret_str = C.mdq_to_mallocated_QuadToString(a.mq_val) // may use exponent notation
		str_slice = Byteslice_into_C(ret_str.s, ret_str.length)

		dst = append(dst, str_slice...) // write buff into destination and return
		C.free(unsafe.Pointer(ret_str.s))
		C.free(unsafe.Pointer(ret.BCD))
		return dst
	}

	// write string. Here, the number is not Inf nor Nan.

	i := 0

	integral_part_length := len(BCD_slice) + int(exp) // here, exp is negative

	BCD_integral_part := BCD_slice[:integral_part_length]
	BCD_fractional_part := BCD_slice[integral_part_length:]

	for _, d = range BCD_integral_part { // ==== write integral part ====
		if skip_leading_zero && d == 0 {
			continue
		} else {
			skip_leading_zero = false
		}
		buff[i] = '0' + d
		i++
	}

	if i == 0 { // write '0' if integral part is 0
		buff[i] = '0'
		i++
	}

	if sign != 0 {
		dst = append(dst, '-') // write '-' sign if any into destination
	}

	dst = append(dst, buff[:i]...) // write integral part into destination

	if exp == 0 { // if no fractional part, just return
		C.free(unsafe.Pointer(ret.BCD))
		return dst
	}

	dst = append(dst, '.') // ==== write fractional part ====

	i = 0
	for _, d = range BCD_fractional_part {
		buff[i] = '0' + d
		i++
	}

	dst = append(dst, buff[:i]...) // write fractional part into destination

	C.free(unsafe.Pointer(ret.BCD))
	return dst
}

// String is the preferred way to display a decQuad number.
//
func (a *MyQuad) String() string {
	var buffer [S_DECQUAD_String]byte // to avoid reallocation, this capacity is needed to receive result of C.mdq_to_mallocated_QuadToString(), and also big enough to receive [sign] + [DECQUAD_Pmax digits] + [fractional dot]

	ss := AppendQuad(buffer[:0], a)

	return string(ss)
}

// String_raw returns the representation of a decQuad as an integer coefficient with an exponent.
// E.g. 123456e-2
// This function is used for testing, to see exactly the coefficient and exponent stored in the decQuad.
// IT IS NOT USEFUL FOR NORMAL USE AND SHOULD BE AVOIDED.
//
func (a *MyQuad) String_raw() string {
	var (
		ret C.Ret_str
	)

	ret = C.mdq_to_mallocated_string_raw(a.mq_val)

	s := C.GoStringN(ret.s, C.int(ret.length)) // copy C string into Go string

	C.free(unsafe.Pointer(ret.s))

	return s
}

// To_int32_truncate converts MyQuad value to int32.
// Returns an error if Inf or Nan or conversion failed.
//
func To_int32_truncate(a *MyQuad) (int32, *rsql.Error) {
	var (
		ret C.Ret_int32
	)

	if ret = C.mdq_to_int32_truncate(a.mq_val); ret.mdqerr != 0 {
		return 0, rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	return int32(ret.val), nil
}

// To_int32_round converts MyQuad value to int32.
// Returns an error if Inf or Nan or conversion failed.
//
func To_int32_round(a *MyQuad) (int32, *rsql.Error) {
	var (
		ret C.Ret_int32
	)

	if ret = C.mdq_to_int32_round(a.mq_val); ret.mdqerr != 0 {
		return 0, rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	return int32(ret.val), nil
}

// To_int64_truncate converts MyQuad value to int64.
// Returns an error if Inf or Nan or conversion failed.
//
func To_int64_truncate(a *MyQuad) (int64, *rsql.Error) {
	var (
		ret C.Ret_int64
	)

	if ret = C.mdq_to_int64_truncate(a.mq_val); ret.mdqerr != 0 {
		return 0, rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	return int64(ret.val), nil
}

// To_int64_round converts MyQuad value to int64.
// Returns an error if Inf or Nan or conversion failed.
//
func To_int64_round(a *MyQuad) (int64, *rsql.Error) {
	var (
		ret C.Ret_int64
	)

	if ret = C.mdq_to_int64_round(a.mq_val); ret.mdqerr != 0 {
		return 0, rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	return int64(ret.val), nil
}

// To_float64 converts MyQuad value to float64.
// Returns an error if Inf or Nan or conversion failed.
//
func To_float64(a *MyQuad) (float64, *rsql.Error) {
	var (
		ret C.Ret_double
	)

	if ret = C.mdq_to_double(a.mq_val); ret.mdqerr != 0 {
		return 0, rsql.New_Error(rsql.ERROR_ARITH, get_rsql_error_message_id(ret.mdqerr), rsql.ERROR_BATCH_ABORT)
	}

	return float64(ret.val), nil
}

// To_byte_representation returns the internal decQuad byte array.
//
func To_byte_representation(a MyQuad) (res [S_DECQUAD_Bytes]byte) {
	var (
		i int
	)

	// https://golang.org/cmd/cgo/ : As Go doesn't have support for C's union type in the general case, C's union types are represented as a Go byte array with the same length.
	// decQuad type is a union. So, it is an array of Go bytes.

	for i = 0; i < S_DECQUAD_Bytes; i++ {
		res[i] = a.mq_val[i] // it is a Go 'byte', not C.uint8_t, because decQuad is a union
	}

	return res
}

// From_byte_representation returns decQuad from byte representation.
//
func From_byte_representation(a [S_DECQUAD_Bytes]byte) (res MyQuad) {
	var (
		i int
	)

	// https://golang.org/cmd/cgo/ : As Go doesn't have support for C's union type in the general case, C's union types are represented as a Go byte array with the same length.
	// decQuad type is a union. So, it is an array of Go bytes.

	for i = 0; i < S_DECQUAD_Bytes; i++ {
		res.mq_val[i] = a[i] // it is a Go 'byte', not C.uint8_t, because decQuad is a union
	}

	return res
}

// Append_compressed_bytes appends the internal decQuad byte array, in a compressed representation.
// It is useful for serializing MyQuad to disk, as it often contains a section of bytes full of 0.
//
func Append_compressed_bytes(dest []byte, a MyQuad) (dest_modified []byte, length uint16) {
	var (
		i                   int
		start_0_sect        int
		start_zero_section  int
		endx_zero_section   int
		length_zero_section int
		head                uint8
	)

	// find longest section of zeroes in byte representation of MyQuad

	start_0_sect = -1

	for i = 0; i < S_DECQUAD_Bytes; i++ {
		if a.mq_val[i] == 0 { // it is a Go 'byte', not C.uint8_t, because decQuad is a union
			if start_0_sect < 0 {
				start_0_sect = i
			}
			continue
		}

		if start_0_sect >= 0 {
			if i-start_0_sect > endx_zero_section-start_zero_section {
				start_zero_section = start_0_sect
				endx_zero_section = i
			}

			start_0_sect = -1
		}
	}

	if start_0_sect >= 0 { // trailing section of zeroes
		if i-start_0_sect > endx_zero_section-start_zero_section {
			start_zero_section = start_0_sect
			endx_zero_section = i
		}
	}

	// serialize

	length_zero_section = endx_zero_section - start_zero_section

	if length_zero_section > 15 { // if all bytes are 0, length_zero_section == 16 and won't fit in head byte
		length_zero_section = 15 // never happens, as a real MyQuad never has 0 for all bytes
	}

	head = uint8(start_zero_section) | uint8(length_zero_section)<<4 // 4 bits for start_zero_section, and 4 bits for length_zero_section. This means that S_DECQUAD_Bytes SHOULD NOT BE > 16.

	dest = append(dest, head)
	dest = append(dest, a.mq_val[:start_zero_section]...)
	dest = append(dest, a.mq_val[start_zero_section+length_zero_section:]...)

	return dest, uint16(1 + S_DECQUAD_Bytes - length_zero_section)
}

// Uncompress_bytes uncompresses bytes in src into MyQuad.
// It is the opposite of Append_compressed_bytes().
//
func Uncompress_bytes(src []byte) (res MyQuad) {
	var (
		start_zero_section  int
		length_zero_section int
		length_src          int
	)

	// get head info

	start_zero_section = int(src[0] & 0x0F)
	length_zero_section = int(src[0] >> 4)

	src = src[1:] // discard head

	length_src = S_DECQUAD_Bytes - length_zero_section
	rsql.Assert(len(src) == length_src)

	// fill MyQuad

	for i := 0; i < start_zero_section; i++ { // copy the part before zero section
		res.mq_val[i] = src[i] // it is a Go 'byte', not C.uint8_t, because decQuad is a union
	}

	for i := start_zero_section; i < length_src; i++ { // copy the part after zero section
		res.mq_val[length_zero_section+i] = src[i] // it is a Go 'byte', not C.uint8_t, because decQuad is a union
	}

	return res
}
