package data

import (
	"rsql"
)

// Jump_out_true_false prepares basicblock_source to jump to basicblock_true or basicblock_false, depending on the value of data_condition.
//
func Jump_out_true_false(basicblock_source *rsql.Basicblock, data_condition *BOOLEAN, basicblock_true *rsql.Basicblock, basicblock_false *rsql.Basicblock) *rsql.Error {

	if data_condition.data_error != nil {
		return data_condition.data_error
	}

	basicblock_source.Bb_jump_target = basicblock_false

	if data_condition.data_NULL_flag == false && data_condition.data_val == true {
		basicblock_source.Bb_jump_target = basicblock_true
	}

	return nil
}

// Jump_out prepares basicblock_source to jump to basicblock_target unconditionnaly.
//
func Jump_out(basicblock_source *rsql.Basicblock, basicblock_target *rsql.Basicblock) {

	basicblock_source.Bb_jump_target = basicblock_target
}

// Jump_if_NULL_or_false prepares basicblock_source to jump to basicblock_target, only if data_condition is NULL or false.
//
func Jump_if_NULL_or_false(basicblock_source *rsql.Basicblock, data_condition *BOOLEAN, basicblock_target *rsql.Basicblock) *rsql.Error {

	if data_condition.data_error != nil {
		return data_condition.data_error
	}

	if data_condition.data_NULL_flag == true || data_condition.data_val == false { // if NULL or false
		basicblock_source.Bb_jump_target = basicblock_target
	}

	return nil
}

// Jump_return
//
func Jump_return(basicblock_source *rsql.Basicblock, returned_value *INT) (retval int64, rsql_err *rsql.Error) {

	if returned_value.data_error != nil {
		return 0, returned_value.data_error
	}

	retval = 0
	if returned_value.data_NULL_flag == false {
		retval = returned_value.data_val
	}

	basicblock_source.Bb_jump_target = nil

	return retval, nil
}
