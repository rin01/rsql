package data

import (
	"fmt"
	"math"
	"strconv"

	"rsql"
	"rsql/quad"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *FLOAT) RO_LEAF_hashval() uint32 {
	var (
		IEEE754 uint64
		hash    uint32
	)

	hash = uint32(rsql.DATATYPE_FLOAT)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		IEEE754 = math.Float64bits(a.data_val) // note: the float64 value originates from literal text in SQL script,, and same text gives the same binary representation for float64.

		hash += uint32(IEEE754)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(IEEE754 >> 32)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *FLOAT) String() string {

	if a.data_error != nil {
		return "FLOAT: error"
	}

	if a.data_NULL_flag == true {
		return "FLOAT: null"
	}

	return fmt.Sprintf("FLOAT: %g", a.data_val)
}

// New_FLOAT_NULL creates a new data.FLOAT initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_FLOAT_NULL(kind rsql.Kind_t) *FLOAT {

	dataslot := &FLOAT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_FLOAT,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		data_val: 0.,
	}

	return dataslot
}

// New_literal_FLOAT_value creates a new data.FLOAT from a value.
// Returns an error if val is Inf or Nan.
//
func New_literal_FLOAT_value(val float64) (*FLOAT, *rsql.Error) {

	switch {
	case math.IsNaN(val): // check if NaN
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)

	case math.IsInf(val, 0): // check if +Inf or -Inf
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
	}

	dataslot := &FLOAT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_FLOAT,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: val,
	}

	return dataslot, nil
}

// New_literal_FLOAT creates a new data.FLOAT from literal integer string.
// Returns an error if invalid string, or if result is Inf or Nan.
//
func New_literal_FLOAT(s string) (*FLOAT, *rsql.Error) {
	var (
		err error
		val float64
	)

	if val, err = strconv.ParseFloat(s, 64); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_CONVERSION_STRING_TO_FLOAT_FAILED, rsql.ERROR_BATCH_ABORT, s)
	}

	dataslot, rsql_err := New_literal_FLOAT_value(val)

	return dataslot, rsql_err
}

func Unary_minus_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = -a_val

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Add_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val + b_val

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Subtract_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val - b_val

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Multiply_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val * b_val

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Divide_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	if b_val == 0. { // Golang specs: The result of a floating-point or complex division by zero is not specified beyond the IEEE-754 standard; whether a run-time panic occurs is implementation-specific.
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_DIVIDE_BY_ZERO, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = a_val / b_val

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Comp_equal_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if math.IsNaN(b.data_val) { // if b is Nan
		if math.IsNaN(a.data_val) { // if a is also Nan, a (Nan) == b (Nan)
			r.data_val = true
			return
		}
		r.data_val = false // else, a (not-Nan) > b (Nan)
		return
	}

	if math.IsNaN(a.data_val) { // if a is Nan, ( and here, b is not-Nan ), a < b */
		r.data_val = false // a (Nan) < b (not-Nan)
		return
	}

	// here, a and b are not Nan

	r_val = a.data_val == b.data_val

	r.data_val = r_val
}

func Comp_greater_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if math.IsNaN(b.data_val) { // if b is Nan
		if math.IsNaN(a.data_val) { // if a is also Nan, a (Nan) == b (Nan)
			r.data_val = false
			return
		}
		r.data_val = true // else, a (not-Nan) > b (Nan)
		return
	}

	if math.IsNaN(a.data_val) { // if a is Nan, ( and here, b is not-Nan ), a < b */
		r.data_val = false // a (Nan) < b (not-Nan)
		return
	}

	// here, a and b are not Nan

	r_val = a.data_val > b.data_val

	r.data_val = r_val
}

func Comp_less_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if math.IsNaN(b.data_val) { // if b is Nan
		if math.IsNaN(a.data_val) { // if a is also Nan, a (Nan) == b (Nan)
			r.data_val = false
			return
		}
		r.data_val = false // else, a (not-Nan) > b (Nan)
		return
	}

	if math.IsNaN(a.data_val) { // if a is Nan, ( and here, b is not-Nan ), a < b */
		r.data_val = true // a (Nan) < b (not-Nan)
		return
	}

	// here, a and b are not Nan

	r_val = a.data_val < b.data_val

	r.data_val = r_val
}

func Comp_greater_equal_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if math.IsNaN(b.data_val) { // if b is Nan
		if math.IsNaN(a.data_val) { // if a is also Nan, a (Nan) == b (Nan)
			r.data_val = true
			return
		}
		r.data_val = true // else, a (not-Nan) > b (Nan)
		return
	}

	if math.IsNaN(a.data_val) { // if a is Nan, ( and here, b is not-Nan ), a < b */
		r.data_val = false // a (Nan) < b (not-Nan)
		return
	}

	// here, a and b are not Nan

	r_val = a.data_val >= b.data_val

	r.data_val = r_val
}

func Comp_less_equal_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if math.IsNaN(b.data_val) { // if b is Nan
		if math.IsNaN(a.data_val) { // if a is also Nan, a (Nan) == b (Nan)
			r.data_val = true
			return
		}
		r.data_val = false // else, a (not-Nan) > b (Nan)
		return
	}

	if math.IsNaN(a.data_val) { // if a is Nan, ( and here, b is not-Nan ), a < b */
		r.data_val = true // a (Nan) < b (not-Nan)
		return
	}

	// here, a and b are not Nan

	r_val = a.data_val <= b.data_val

	r.data_val = r_val
}

func Comp_not_equal_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if math.IsNaN(b.data_val) { // if b is Nan
		if math.IsNaN(a.data_val) { // if a is also Nan, a (Nan) == b (Nan)
			r.data_val = false
			return
		}
		r.data_val = true // else, a (not-Nan) > b (Nan)
		return
	}

	if math.IsNaN(a.data_val) { // if a is Nan, ( and here, b is not-Nan ), a < b */
		r.data_val = true // a (Nan) < b (not-Nan)
		return
	}

	// here, a and b are not Nan

	r_val = a.data_val != b.data_val

	r.data_val = r_val
}

func Is_null_FLOAT(r *BOOLEAN, a *FLOAT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_FLOAT(r *BOOLEAN, a *FLOAT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_FLOAT checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_FLOAT(r *BOOLEAN, a *FLOAT, b_list []*FLOAT) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_FLOAT(r, a, b)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_FLOAT(r *BOOLEAN, a *FLOAT, b_list []*FLOAT) {

	In_list_FLOAT(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_FLOAT(r *FLOAT, a_list []Case_element_t) {
	var (
		elem_val *FLOAT
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*FLOAT)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			r.data_val = elem_val.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

// Cast_FLOAT_to_VARCHAR casts FLOAT to VARCHAR.
// If precision of target is unsufficient, returns an error.
//
func Cast_FLOAT_to_VARCHAR(r *VARCHAR, a *FLOAT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res := strconv.AppendFloat(r.data_val[:0], a.data_val, 'g', 6, 64) // same as C language format "%.6g". Like MS SQL Server, display 6 digits:    1234.12    123456    1.23457e+06

	if len(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_CAST_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, res, r.Data_precision) // display the formatted string res, which is better than the sheer value of a.data_val
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

// Cast_FLOAT_to_BIT casts FLOAT to BIT.
// If FLOAT != 0.0, BIT is set to 1. Else, BIT is set to 0.
//
func Cast_FLOAT_to_BIT(r *BIT, a *FLOAT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = 0
	if a.data_val != 0. {
		r.data_val = 1
	}
}

func Cast_FLOAT_to_TINYINT(r *TINYINT, a *FLOAT) {
	var (
		a_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	switch {
	case math.IsNaN(a_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(a_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	if a_val > math.MaxUint8 || a_val < 0. {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = int64(a_val) // like MS SQL Server, truncate
}

func Cast_FLOAT_to_SMALLINT(r *SMALLINT, a *FLOAT) {
	var (
		a_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	switch {
	case math.IsNaN(a_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(a_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	if a_val > math.MaxInt16 || a_val < math.MinInt16 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = int64(a_val) // like MS SQL Server, truncate
}

func Cast_FLOAT_to_INT(r *INT, a *FLOAT) {
	var (
		a_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	switch {
	case math.IsNaN(a_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(a_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	if a_val > math.MaxInt32 || a_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_CAST_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = int64(a_val) // like MS SQL Server, truncate. Note: float64 is approximately 15 digits precision. So, it can accurately represent an int32, which is 10 digits precision.
}

// Cast_FLOAT_to_BIGINT casts FLOAT into BIGINT.
//
//      NOTE: the range of float64 values that can be converted to int64 is explained herebelow.
//
//      math.MaxInt64 == 9223372036854775807
//      But float64 cannot store this number, because it has only 15 significant digits, whereas int64 has 19 significant digits.
//
//      The consecutive numbers around math.MaxInt64 that are actually stored in float64 are:
//
//          9223372036854777856.0
//          9223372036854775808.0  // == float64(math.MaxInt64), which overflows math.MaxInt64
//          9223372036854774784.0  // highest float64 which can be converted to int64
//          9223372036854773760.0
//          9223372036854772736.0
//
//      So, float64(math.MaxInt64), which is 9223372036854775808.0, must be excluded from valid range, as it overflows math.MaxInt64 == 9223372036854775807.
//      We also exclude float64(math.MinInt64), which is -9223372036854775808.0, for symetry with the upper range.
//
func Cast_FLOAT_to_BIGINT(r *BIGINT, a *FLOAT) {
	var (
		a_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	switch {
	case math.IsNaN(a_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(a_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	if a_val >= float64(math.MaxInt64) || a_val <= float64(math.MinInt64) { // Note: see explanation above
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_CAST_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = int64(a_val) // like MS SQL Server, truncate
}

func Cast_FLOAT_to_NUMERIC(r *NUMERIC, a *FLOAT) {

	var buff = make([]byte, 30) // large enough for all string representation of float64

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	switch {
	case math.IsNaN(a.data_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(a.data_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	buff = strconv.AppendFloat(buff[:0], a.data_val, 'e', 16, 64) // same as C language format "%.16e". +-D.DDDDDDDDDDDDDDDDe+-QQQ

	r.data_error = quad.From_bytes(&r.data_val, r.Data_precision, r.Data_scale, buff)

}

func Cast_FLOAT_to_FLOAT(r *FLOAT, a *FLOAT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Assign_FLOAT(r *FLOAT, a *FLOAT) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

func Copy_FLOAT(r *FLOAT, a *FLOAT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_FLOAT(kind rsql.Kind_t, a *FLOAT) *FLOAT {

	return New_FLOAT_NULL(kind)
}
