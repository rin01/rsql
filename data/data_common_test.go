package data

import (
	"bytes"
	"fmt"
	"math"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"time"

	"rsql"
	"rsql/like"
	"rsql/mdat"
	"rsql/mstr"
	"rsql/quad"
)

// helper function to check equality of two float64.
//
func float64_equal(a float64, r float64) bool {
	const (
		EPSILON_ZERO  float64 = 1e-300 // used to compute equality of two float64, when value to check is close to zero
		EPSILON_RATIO float64 = 0.001  // used to compute equality of two float64. It is good enough for testing.
	)

	if math.IsNaN(a) && math.IsNaN(r) {
		return true
	}

	if (math.IsInf(a, 1) && math.IsInf(r, 1)) || (math.IsInf(a, -1) && math.IsInf(r, -1)) {
		return true
	}

	if math.Abs(r) < EPSILON_ZERO { // if check value is close to zero
		if math.Abs(a) < EPSILON_ZERO {
			return true
		}
		return false
	}

	if math.Abs((a-r)/r) < EPSILON_RATIO {
		return true
	}

	return false
}

// tst_sample_t contains an operator or function to test, its operands, and the expected result.
//
// E.g.   {Add_INT, []interface{}{r_INT, math.MaxInt32, 0}, r_INT, math.MaxInt32},
//
type tst_sample_t struct {
	function     interface{}   // function to test
	operands     []interface{} // function arguments
	result       interface{}   // argument that receives the result of the function
	result_check interface{}   // expected result
}

func (sample *tst_sample_t) String() string {
	var (
		bbuff bytes.Buffer
	)

	// function name

	funcname := runtime.FuncForPC(reflect.ValueOf(sample.function).Pointer()).Name()

	fmt.Fprint(&bbuff, funcname)

	// operands

	fmt.Fprint(&bbuff, "(")

	for i, op := range sample.operands {

		if op == sample.result {
			fmt.Fprint(&bbuff, "<dest>")
		} else {
			fmt.Fprintf(&bbuff, "%s", tst_value2string(op))
		}

		if i < len(sample.operands)-1 {
			fmt.Fprint(&bbuff, ", ")
		}
	}

	fmt.Fprint(&bbuff, ")")

	// result obtained

	fmt.Fprintf(&bbuff, " <dest> == %s, ", tst_value2string(sample.result))

	// result expected

	fmt.Fprintf(&bbuff, " expected %s", tst_value2string(sample.result_check))

	return bbuff.String()
}

func tst_value2string(op interface{}) string {

	switch op := op.(type) {
	case nil:
		return "<NULL>"

	case bool:
		return fmt.Sprintf("%t", op)

	case string:
		return fmt.Sprintf("\"%s\"", op)

	case int:
		return strconv.Itoa(op)

	case float64:
		return strconv.FormatFloat(op, 'g', 20, 64)

	case rsql.Message_id_t:
		return op.String()

	case Compsort_t:
		return strconv.Itoa(int(op))

	case *Compsort_t:
		return strconv.Itoa(int(*op))

	case *VOID:
		return "<VOID>"

	case *SYSCOLLATOR:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("SYSCOLLATOR: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "SYSCOLLATOR: <NULL>"
		}

		return fmt.Sprintf("SYSCOLLATOR: \"%s\"", op.data_collation_string)

	case *SYSLANGUAGE:
		return fmt.Sprintf("SYSLANGUAGE: \"%s\" %d %s", op.data_langinfo.Lng_locale_name, op.data_firstdayofweek, op.data_DMY)

	case *BOOLEAN:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("BOOLEAN: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "BOOLEAN: <NULL>"
		}

		return fmt.Sprintf("BOOLEAN: %t", op.data_val)

	case *VARBINARY:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("VARBINARY: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "VARBINARY: <NULL>"
		}

		return fmt.Sprintf("VARBINARY: %+q", op.data_val)

	case *VARCHAR:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("VARCHAR: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "VARCHAR: <NULL>"
		}

		return fmt.Sprintf("VARCHAR: \"%s\"", op.data_val)

	case *REGEXPLIKE:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("REGEXPLIKE: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "REGEXPLIKE: <NULL>"
		}

		return fmt.Sprintf("REGEXPLIKE: \"%s\"", op.data_val.String())

	case *BIT:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("BIT: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "BIT: <NULL>"
		}

		return fmt.Sprintf("BIT: %d", op.data_val)

	case *TINYINT:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("TINYINT: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "TINYINT: <NULL>"
		}

		return fmt.Sprintf("TINYINT: %d", op.data_val)

	case *SMALLINT:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("SMALLINT: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "SMALLINT: <NULL>"
		}

		return fmt.Sprintf("SMALLINT: %d", op.data_val)

	case *INT:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("INT: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "INT: <NULL>"
		}

		return fmt.Sprintf("INT: %d", op.data_val)

	case *BIGINT:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("BIGINT: %s", op.Error().Message_id)

		case op.NULL_flag():
			return "BIGINT: <NULL>"
		}

		return fmt.Sprintf("BIGINT: %d", op.data_val)

	case *FLOAT:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("FLOAT: %s", op.Error().Message_id.String())

		case op.NULL_flag():
			return "FLOAT: <NULL>"
		}

		return fmt.Sprintf("FLOAT: %.16f", op.data_val)

	case *MONEY:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("MONEY: %s", op.Error().Message_id)

		case op.NULL_flag():
			return "MONEY: <NULL>"
		}

		return fmt.Sprintf("MONEY: %s", op.data_val.String())

	case *NUMERIC:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("NUMERIC: %s", op.Error().Message_id)

		case op.NULL_flag():
			return "NUMERIC: <NULL>"
		}

		return fmt.Sprintf("NUMERIC: %s", op.data_val.String())

	case *DATE:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("DATE: %s", op.Error().Message_id)

		case op.NULL_flag():
			return "DATE: <NULL>"
		}

		return fmt.Sprintf("DATE: %s", op.data_val.String())

	case *TIME:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("TIME: %s", op.Error().Message_id)

		case op.NULL_flag():
			return "TIME: <NULL>"
		}

		return fmt.Sprintf("TIME: %s", op.data_val.String())

	case *DATETIME:
		switch {
		case op.Error() != nil:
			return fmt.Sprintf("DATETIME: %s", op.Error().Message_id)

		case op.NULL_flag():
			return "DATETIME: <NULL>"
		}

		return fmt.Sprintf("DATETIME: %s", op.data_val.String())

	case Case_element_t:
		return "{" + tst_value2string(op.Cond) + ", " + tst_value2string(op.Val) + "}"

	default:
		return "<UNKNOWN>"
	}
}

func init() {

	var a int64 = math.MaxInt64

	if int64(int(a)) != a {
		panic("The test does not work if size of int is not 64 bits, For BIGINT test, literal number operands in tst_sample_t.operands are stored as int, and are then being converted to int64. So, int must be same size as int64.")
		// NOTE: another solution could be to cast each literal number bigger than int to int64, in tst_sample_t.operands
	}

}

// tst_op2VOID creates a *VOID from the "nil" value stored in an interface{}, in 'op' argument.
//
func tst_op2VOID(op interface{}) *VOID {
	var res *VOID

	switch op.(type) {
	case nil:
		res = New_VOID_NULL(rsql.KIND_RO_LEAF)

	default:
		panic("cannot create a VOID from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2SYSCOLLATOR creates a *SYSCOLLATOR from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a SYSCOLLATOR with an error.
//     - nil creates a NULL SYSCOLLATOR
//     - a pointer to *SYSCOLLATOR. It is returned as is.
//     - else, a regular SYSCOLLATOR is created from string value
//
func tst_op2SYSCOLLATOR(op interface{}) *SYSCOLLATOR {
	var (
		res *SYSCOLLATOR
	)

	switch op := op.(type) {
	case *SYSCOLLATOR: // returned as-is. It is always a plain argument to a function, it is never the result of a function.
		res = op

	case rsql.Message_id_t:
		res = New_literal_SYSCOLLATOR("abcd")
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field

	case nil:
		res = New_literal_SYSCOLLATOR("abcd")
		res.data_NULL_flag = true

	case string:
		res = New_literal_SYSCOLLATOR(op)

	default:
		panic("cannot create a VARCHAR from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2SYSLANGUAGE creates a *SYSLANGUAGE from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a SYSLANGUAGE with an error.
//     - nil creates a NULL SYSLANGUAGE
//     - a pointer to *SYSLANGUAGE. It is returned as is.
//     - else, a regular SYSLANGUAGE is created from string value
//
func tst_op2SYSLANGUAGE(op interface{}) *SYSLANGUAGE {
	var (
		rsql_err *rsql.Error
		res      *SYSLANGUAGE
	)

	switch op := op.(type) {
	case *SYSLANGUAGE: // returned as-is
		res = op

	case rsql.Message_id_t:
		res = New_SYSLANGUAGE_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field

	case nil:
		res = New_SYSLANGUAGE_NULL(rsql.KIND_RO_LEAF)
		res.data_NULL_flag = true

	case string:
		if res, rsql_err = New_literal_SYSLANGUAGE_value(op); rsql_err != nil {
			panic(rsql_err)
		}

	default:
		panic("cannot create a VARCHAR from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2BOOLEAN creates a *BOOLEAN from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a BOOLEAN with an error.
//     - nil creates a NULL BOOLEAN
//     - a pointer to *BOOLEAN. It is used to pass the BOOLEAN which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular BOOLEAN is created from bool value
//
func tst_op2BOOLEAN(op interface{}) *BOOLEAN {
	var res *BOOLEAN

	switch op := op.(type) {
	case *BOOLEAN: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = true                                                                             // put garbage in this field

	case rsql.Message_id_t:
		res = New_BOOLEAN_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field
		res.data_val = true       // put garbage in this field

	case nil:
		res = New_BOOLEAN_NULL(rsql.KIND_RO_LEAF)
		res.data_val = true // put garbage in this field

	case bool:
		res = New_literal_BOOLEAN_value(op)

	default:
		panic("cannot create a BOOLEAN from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2VARBINARY creates a *VARBINARY from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a VARBINARY with an error.
//     - nil creates a NULL VARBINARY
//     - a pointer to *VARBINARY. It is returned as is.
//     - else, a regular VARBINARY is created from string value
//
func tst_op2VARBINARY(op interface{}) *VARBINARY {
	var (
		res      *VARBINARY
		rsql_err *rsql.Error
	)

	switch op := op.(type) {
	case *VARBINARY: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = []byte("QIRHE")                                                                  // put garbage in this field

	case rsql.Message_id_t:
		res = New_VARBINARY_NULL(rsql.KIND_RO_LEAF, 10)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true       // put garbage in this field
		res.data_val = []byte("TUEFJK") // put garbage in this field

	case nil:
		res = New_VARBINARY_NULL(rsql.KIND_RO_LEAF, 10)
		res.data_val = []byte("HKFIR") // put garbage in this field

	case []byte:
		res, rsql_err = New_literal_VARBINARY_value(op)
		rsql.Assert(rsql_err == nil)

	case string:
		res, rsql_err = New_literal_VARBINARY_value([]byte(op))
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a VARBINARY from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2VARCHAR creates a *VARCHAR from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a VARCHAR with an error.
//     - nil creates a NULL VARCHAR
//     - a pointer to *VARCHAR. It is returned as is.
//     - else, a regular VARCHAR is created from string value
//
func tst_op2VARCHAR(op interface{}) *VARCHAR {
	var (
		res      *VARCHAR
		rsql_err *rsql.Error
	)

	switch op := op.(type) {
	case *VARCHAR: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = mstr.Mystr("XMSJDF")                                                             // put garbage in this field

	case rsql.Message_id_t:
		res = New_VARCHAR_NULL(rsql.KIND_RO_LEAF, 10, false)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true         // put garbage in this field
		res.data_val = mstr.Mystr("dgbw") // put garbage in this field

	case nil:
		res = New_VARCHAR_NULL(rsql.KIND_RO_LEAF, 10, false)
		res.data_val = mstr.Mystr("asduf") // put garbage in this field

	case string:
		res, rsql_err = New_literal_VARCHAR(op)
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a VARCHAR from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2REGEXPLIKE creates a *REGEXPLIKE from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a REGEXPLIKE with an error.
//     - nil creates a NULL REGEXPLIKE
//     - a pointer to *REGEXPLIKE. It is returned as is.
//     - else, a regular REGEXPLIKE is created from string value
//
func tst_op2REGEXPLIKE(op interface{}) *REGEXPLIKE {
	var (
		res      *REGEXPLIKE
		rsql_err *rsql.Error
	)

	switch op := op.(type) {
	case *REGEXPLIKE: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = like.Likexp_bundle{}                                                             // put garbage in this field

	case rsql.Message_id_t:
		res = New_REGEXPLIKE_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true           // put garbage in this field
		res.data_val = like.Likexp_bundle{} // put garbage in this field

	case nil:
		res = New_REGEXPLIKE_NULL(rsql.KIND_RO_LEAF)
		res.data_val = like.Likexp_bundle{} // put garbage in this field

	case string:
		res, rsql_err = New_literal_REGEXPLIKE_value(op, '\\')
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a REGEXPLIKE from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2BIT creates a *BIT from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a BIT with an error.
//     - nil creates a NULL BIT
//     - a pointer to *BIT. It is used to pass the BIT which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular BIT is created from number
//
func tst_op2BIT(op interface{}) *BIT {
	var res *BIT

	switch op := op.(type) {
	case *BIT: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = 354                                                                              // put garbage in this field

	case rsql.Message_id_t:
		res = New_BIT_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field
		res.data_val = 62         // put garbage in this field

	case nil:
		res = New_BIT_NULL(rsql.KIND_RO_LEAF)
		res.data_val = 827 // put garbage in this field

	case uint8:
		res = New_literal_BIT_value(op)

	case int:
		res = New_literal_BIT_value(uint8(op))

	default:
		panic("cannot create a BIT from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2TINYINT creates a *TINYINT from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a TINYINT with an error.
//     - nil creates a NULL TINYINT
//     - a pointer to *TINYINT. It is used to pass the TINYINT which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular TINYINT is created from number
//
func tst_op2TINYINT(op interface{}) *TINYINT {
	var res *TINYINT

	switch op := op.(type) {
	case *TINYINT: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = 34                                                                               // put garbage in this field

	case rsql.Message_id_t:
		res = New_TINYINT_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field
		res.data_val = 84         // put garbage in this field

	case nil:
		res = New_TINYINT_NULL(rsql.KIND_RO_LEAF)
		res.data_val = 11 // put garbage in this field

	case uint8:
		res = New_literal_TINYINT_value(op)

	case int:
		rsql.Assert(op >= 0 && op <= math.MaxUint8)
		res = New_literal_TINYINT_value(uint8(op))

	default:
		panic("cannot create a TINYINT from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2SMALLINT creates a *SMALLINT from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a SMALLINT with an error.
//     - nil creates a NULL SMALLINT
//     - a pointer to *SMALLINT. It is used to pass the SMALLINT which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular SMALLINT is created from number
//
func tst_op2SMALLINT(op interface{}) *SMALLINT {
	var res *SMALLINT

	switch op := op.(type) {
	case *SMALLINT: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = -44                                                                              // put garbage in this field

	case rsql.Message_id_t:
		res = New_SMALLINT_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field
		res.data_val = 67         // put garbage in this field

	case nil:
		res = New_SMALLINT_NULL(rsql.KIND_RO_LEAF)
		res.data_val = 11 // put garbage in this field

	case int16:
		res = New_literal_SMALLINT_value(op)

	case int:
		rsql.Assert(op >= math.MinInt16 && op <= math.MaxInt16)
		res = New_literal_SMALLINT_value(int16(op))

	default:
		panic("cannot create a SMALLINT from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2INT creates a *INT from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates an INT with an error.
//     - nil creates a NULL INT
//     - a pointer to *INT. It is used to pass the INT which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular INT is created from number
//
func tst_op2INT(op interface{}) *INT {
	var res *INT

	switch op := op.(type) {
	case *INT: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = 6456                                                                             // put garbage in this field

	case rsql.Message_id_t:
		res = New_INT_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field
		res.data_val = -332       // put garbage in this field

	case nil:
		res = New_INT_NULL(rsql.KIND_RO_LEAF)
		res.data_val = 97 // put garbage in this field

		//	case int32:    int32 is synonym of rune
		//		res = New_literal_INT_value(op)

	case int:
		rsql.Assert(op >= math.MinInt32 && op <= math.MaxInt32)
		res = New_literal_INT_value(int32(op))

	case rune:
		rsql.Assert(op >= math.MinInt32 && op <= math.MaxInt32)
		res = New_literal_INT_value(int32(op))

	default:
		panic("cannot create an INT from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2BIGINT creates a *BIGINT from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a BIGINT with an error.
//     - nil creates a NULL BIGINT
//     - a pointer to *BIGINT. It is used to pass the BIGINT which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular BIGINT is created from number
//
func tst_op2BIGINT(op interface{}) *BIGINT {
	var res *BIGINT

	switch op := op.(type) {
	case *BIGINT: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = 635                                                                              // put garbage in this field

	case rsql.Message_id_t:
		res = New_BIGINT_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field
		res.data_val = 23         // put garbage in this field

	case nil:
		res = New_BIGINT_NULL(rsql.KIND_RO_LEAF)
		res.data_val = 75 // put garbage in this field

	case int:
		res = New_literal_BIGINT_value(int64(op))

	case int64:
		res = New_literal_BIGINT_value(op)

	default:
		panic("cannot create a BIGINT from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2MONEY creates a *MONEY from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a MONEY with an error.
//     - nil creates a NULL MONEY
//     - a pointer to *MONEY. It is used to pass the MONEY which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular MONEY is created from string
//
func tst_op2MONEY(op interface{}) *MONEY {
	var (
		rsql_err *rsql.Error
		res      *MONEY
	)

	switch op := op.(type) {
	case *MONEY: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		if rsql_err = quad.From_string(&res.data_val, 6, 3, "278.382"); rsql_err != nil {               // put garbage in this field
			panic("error when putting garbage in MONEY for test")
		}

	case rsql.Message_id_t:
		res = New_MONEY_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true                                                        // put garbage in this field
		if rsql_err = quad.From_string(&res.data_val, 6, 3, "74.324"); rsql_err != nil { // put garbage in this field
			panic("error when putting garbage in MONEY for test")
		}

	case nil:
		res = New_MONEY_NULL(rsql.KIND_RO_LEAF)
		if rsql_err = quad.From_string(&res.data_val, 6, 3, "234.032"); rsql_err != nil { // put garbage in this field
			panic("error when putting garbage in MONEY for test")
		}

	case string:
		oplower := strings.ToLower(op)
		if oplower == "nan" || oplower == "+inf" || oplower == "inf" || oplower == "-inf" || strings.HasSuffix(oplower, "raw") { // "raw" suffix allows to create a number that is out of MONEY range, like "1e100raw"
			op = strings.TrimSuffix(oplower, "raw")

			res = New_MONEY_NULL(rsql.KIND_RO_LEAF)

			res.data_error = quad.From_string_raw(&res.data_val, op)
			res.data_NULL_flag = false

			break
		}

		res, rsql_err = New_literal_MONEY(op)
		if rsql_err != nil {
			println(rsql_err.Message_id.String())
			fmt.Printf("THE MONEY VALUE %s IS TOO LARGE FOR MONEY. PLEASE PUT THE SUFFIX \"raw\" TO ALLOW NUMBER THAT OVERFLOWS THE TARGET PRECISION AND SCALE, LIKE THIS: \"%sraw\"\n", op, op)
		}
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a MONEY from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2NUMERIC creates a *NUMERIC from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a NUMERIC with an error.
//     - nil creates a NULL NUMERIC
//     - a pointer to *NUMERIC. It is used to pass the NUMERIC which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular NUMERIC is created from string
//
func tst_op2NUMERIC(op interface{}) *NUMERIC {
	var (
		rsql_err *rsql.Error
		res      *NUMERIC
	)

	switch op := op.(type) {
	case *NUMERIC: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		if rsql_err = quad.From_string(&res.data_val, 6, 3, "278.382"); rsql_err != nil {               // put garbage in this field
			panic("error when putting garbage in NUMERIC for test")
		}

	case rsql.Message_id_t:
		res = New_NUMERIC_NULL(rsql.KIND_RO_LEAF, 12, 2)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true                                                        // put garbage in this field
		if rsql_err = quad.From_string(&res.data_val, 6, 3, "74.324"); rsql_err != nil { // put garbage in this field
			panic("error when putting garbage in NUMERIC for test")
		}

	case nil:
		res = New_NUMERIC_NULL(rsql.KIND_RO_LEAF, 12, 2)
		if rsql_err = quad.From_string(&res.data_val, 6, 3, "234.032"); rsql_err != nil { // put garbage in this field
			panic("error when putting garbage in NUMERIC for test")
		}

	case string:
		oplower := strings.ToLower(op)
		if oplower == "nan" || oplower == "+inf" || oplower == "inf" || oplower == "-inf" || strings.HasSuffix(oplower, "raw") { // "raw" suffix allows to create a number that is out of NUMERIC range, like "1e100raw"
			op = strings.TrimSuffix(oplower, "raw")

			res = New_NUMERIC_NULL(rsql.KIND_RO_LEAF, 12, 2)

			res.data_error = quad.From_string_raw(&res.data_val, op)
			res.data_NULL_flag = false

			break
		}

		res, rsql_err = New_literal_NUMERIC(op)
		if rsql_err != nil {
			fmt.Printf("THE NUMERIC VALUE %s IS TOO LARGE FOR NUMERIC. PLEASE PUT THE SUFFIX \"raw\" TO ALLOW NUMBER THAT OVERFLOWS THE TARGET PRECISION AND SCALE, LIKE THIS: \"%sraw\"\n", op, op)
		}
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a NUMERIC from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2FLOAT creates a *FLOAT from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a FLOAT with an error.
//     - nil creates a NULL FLOAT
//     - a pointer to *FLOAT. It is used to pass the FLOAT which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular FLOAT is created from float number
//
func tst_op2FLOAT(op interface{}) *FLOAT {
	var (
		rsql_err *rsql.Error
		res      *FLOAT
	)

	switch op := op.(type) {
	case *FLOAT: // will receive result of computation
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val = 123.45                                                                           // put garbage in this field

	case rsql.Message_id_t:
		res = New_FLOAT_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field
		res.data_val = 123        // put garbage in this field

	case nil:
		res = New_FLOAT_NULL(rsql.KIND_RO_LEAF)
		res.data_val = 123 // put garbage in this field

	case float64:
		res, rsql_err = New_literal_FLOAT_value(op)
		if rsql_err != nil {
			fmt.Printf("New_literal_FLOAT_value(\"%g\") raises an error if Inf or Nan. In the test, panic() occurs due to rsql.Assert() just below.", op)
			fmt.Println(rsql_err)
		}
		rsql.Assert(rsql_err == nil)

	case string:
		res = New_FLOAT_NULL(rsql.KIND_RO_LEAF)
		oplower := strings.ToLower(op)
		switch oplower {
		case "nan":
			res.data_val = math.NaN()
		case "+inf", "inf":
			res.data_val = math.Inf(1)
		case "-inf":
			res.data_val = math.Inf(-1)
		default:
			panic("invalid string for creating FLOAT: " + op)
		}
		res.data_NULL_flag = false

	default:
		panic("cannot create a FLOAT from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2DATE creates a *DATE from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a DATE with an error.
//     - nil creates a NULL DATE
//     - a pointer to *DATE. It is used to pass the DATE which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular DATE is created from string
//
func tst_op2DATE(op interface{}) *DATE {
	var (
		rsql_err *rsql.Error
		res      *DATE
	)

	switch op := op.(type) {
	case *DATE:
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val.Time = time.Date(2345, time.July, 10, 0, 0, 0, 0, time.UTC)

	case rsql.Message_id_t:
		res = New_DATE_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field

	case nil:
		res = New_DATE_NULL(rsql.KIND_RO_LEAF)

	case string:
		res, rsql_err = New_literal_DATE_value(op)
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a DATE from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2TIME creates a *TIME from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a TIME with an error.
//     - nil creates a NULL TIME
//     - a pointer to *TIME. It is used to pass the TIME which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular TIME is created from string
//
func tst_op2TIME(op interface{}) *TIME {
	var (
		rsql_err *rsql.Error
		res      *TIME
	)

	switch op := op.(type) {
	case *TIME:
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val.Time = time.Date(1900, 1, 1, 22, 33, 44, 55, time.UTC)

	case rsql.Message_id_t:
		res = New_TIME_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field

	case nil:
		res = New_TIME_NULL(rsql.KIND_RO_LEAF)

	case string:
		res, rsql_err = New_literal_TIME_value(op)
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a TIME from the operand type, as it is unknown.")
	}

	return res
}

// tst_op2DATETIME creates a *DATETIME from a value stored in an interface{}, in 'op' argument.
//
// This value can be as follows:
//     - a rsql.Message_id_t creates a DATETIME with an error.
//     - nil creates a NULL DATETIME
//     - a pointer to *DATETIME. It is used to pass the DATETIME which will receive the result of the computation. It is returned, with some garbage put in its field.
//     - else, a regular DATETIME is created from string
//
func tst_op2DATETIME(op interface{}) *DATETIME {
	var (
		rsql_err *rsql.Error
		res      *DATETIME
	)

	switch op := op.(type) {
	case *DATETIME:
		res = op
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_GARBAGE, rsql.ERROR_BATCH_ABORT) // put garbage in this field
		res.data_val.Time = time.Date(2345, time.July, 10, 27, 32, 41, 50, time.UTC)

	case rsql.Message_id_t:
		res = New_DATETIME_NULL(rsql.KIND_RO_LEAF)
		res.data_error = rsql.New_Error(rsql.ERROR_GENERAL, op, rsql.ERROR_BATCH_ABORT)
		res.data_NULL_flag = true // put garbage in this field

	case nil:
		res = New_DATETIME_NULL(rsql.KIND_RO_LEAF)

	case string:
		res, rsql_err = New_literal_DATETIME_value(op)
		rsql.Assert(rsql_err == nil)

	default:
		panic("cannot create a DATETIME from the operand type, as it is unknown.")
	}

	return res
}

// tst_function_tester just run the function 'myfunc', with the arguments 'ops'.
//    - myfunc is the function to test, e.g. Add_INT
//    - ops is a list of the function arguments, e.g. []interface{}{r_INT, nil, 33}
//            The operands will be automatically converted to the proper datatype, e.g. *INT, *FLOAT, etc.
//
func tst_function_tester(myfunc interface{}, ops ...interface{}) {

	switch myfunc := myfunc.(type) {
	case func(*BOOLEAN, *BOOLEAN): // ---------- BOOLEAN -------------
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2BOOLEAN(ops[1])
		myfunc(op0, op1) // execute function
		blotch(op0, op1) // write garbage in data_val field of the arguments, except in op0 because it contains the result

	case func(*BOOLEAN, *BOOLEAN, *BOOLEAN):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2BOOLEAN(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*VARBINARY, *VARBINARY, *VARBINARY): // ---------- VARBINARY -------------
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2VARBINARY(ops[0]), tst_op2VARBINARY(ops[1]), tst_op2VARBINARY(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *VARBINARY, *VARBINARY):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2VARBINARY(ops[1]), tst_op2VARBINARY(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*VARBINARY, *VARBINARY) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2VARBINARY(ops[1]), tst_op2VARBINARY(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *VARBINARY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2VARBINARY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *VARBINARY, []*VARBINARY):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2VARBINARY(ops[1])
		oplist := []*VARBINARY{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2VARBINARY(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARBINARY, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2VARBINARY(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2VARBINARY(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*VARBINARY, *VARBINARY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARBINARY(ops[0]), tst_op2VARBINARY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *VARBINARY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2VARBINARY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *VARBINARY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2VARBINARY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *VARBINARY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2VARBINARY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *VARBINARY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2VARBINARY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *VARBINARY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2VARBINARY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARBINARY, *BOOLEAN, *VARBINARY, *VARBINARY):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARBINARY(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2VARBINARY(ops[2]), tst_op2VARBINARY(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*VARBINARY, *INT, []*VARBINARY):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2VARBINARY(ops[0]), tst_op2INT(ops[1])
		oplist := []*VARBINARY{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2VARBINARY(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARBINARY, []*VARBINARY):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2VARBINARY(ops[0])
		oplist := []*VARBINARY{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2VARBINARY(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*BOOLEAN, *VARCHAR, *VARCHAR, *SYSCOLLATOR): // ---------- VARCHAR -------------
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2BOOLEAN(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSCOLLATOR(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*VARBINARY, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARBINARY(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARCHAR, *VARCHAR, *SYSCOLLATOR) Compsort_t:
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := ops[0].(*Compsort_t), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSCOLLATOR(ops[3])
		*op0 = myfunc(op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *VARCHAR, *REGEXPLIKE, *SYSCOLLATOR):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2BOOLEAN(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2REGEXPLIKE(ops[2]), tst_op2SYSCOLLATOR(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *VARCHAR, []*VARCHAR, *SYSCOLLATOR):
		rsql.Assert(len(ops) >= 3)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2VARCHAR(ops[1])
		oplist := []*VARCHAR{}
		for _, e := range ops[2 : len(ops)-1] {
			oplist = append(oplist, tst_op2VARCHAR(e))
		}
		op999 := tst_op2SYSCOLLATOR(ops[len(ops)-1])
		myfunc(op0, op1, oplist, op999)
		blotch(op0, op1, oplist, op999)

	case func(*VARCHAR, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*REGEXPLIKE, *VARCHAR, *VARCHAR):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2REGEXPLIKE(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BIT, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *VARCHAR):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2VARCHAR(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARCHAR, *VARCHAR, *INT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2VARCHAR(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2INT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*VARCHAR, *VARCHAR, *INT, *INT, *VARCHAR):
		rsql.Assert(len(ops) == 5)
		op0, op1, op2, op3, op4 := tst_op2VARCHAR(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3]), tst_op2VARCHAR(ops[4])
		myfunc(op0, op1, op2, op3, op4)
		blotch(op0, op1, op2, op3, op4)

	case func(*VARCHAR, *VARCHAR, *INT, *INT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*INT, *VARCHAR, *VARCHAR, *INT, *SYSCOLLATOR):
		rsql.Assert(len(ops) == 5)
		op0, op1, op2, op3, op4 := tst_op2INT(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2INT(ops[3]), tst_op2SYSCOLLATOR(ops[4])
		myfunc(op0, op1, op2, op3, op4)
		blotch(op0, op1, op2, op3, op4)

	case func(*VARCHAR, *VARCHAR, *VARCHAR, *VARCHAR, *SYSCOLLATOR):
		rsql.Assert(len(ops) == 5)
		op0, op1, op2, op3, op4 := tst_op2VARCHAR(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2VARCHAR(ops[3]), tst_op2SYSCOLLATOR(ops[4])
		myfunc(op0, op1, op2, op3, op4)
		blotch(op0, op1, op2, op3, op4)

	case func(*VARCHAR, *VARCHAR, *VARCHAR):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2VARCHAR(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *VARCHAR, *VARCHAR, *INT, *SYSCOLLATOR):
		rsql.Assert(len(ops) == 5)
		op0, op1, op2, op3, op4 := tst_op2BOOLEAN(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2INT(ops[3]), tst_op2SYSCOLLATOR(ops[4])
		myfunc(op0, op1, op2, op3, op4)
		blotch(op0, op1, op2, op3, op4)

	case func(*DATE, *VARCHAR, *INT, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2DATE(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2INT(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*TIME, *VARCHAR, *INT, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2TIME(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2INT(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*DATETIME, *VARCHAR, *INT, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2DATETIME(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2INT(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*INT, *VARCHAR, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2INT(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2SYSLANGUAGE(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*VARCHAR, *BOOLEAN, *VARCHAR, *VARCHAR):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2VARCHAR(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*VARCHAR, *VARCHAR, *VARCHAR, *SYSCOLLATOR):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2VARCHAR(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSCOLLATOR(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*VARCHAR, *INT, []*VARCHAR):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2INT(ops[1])
		oplist := []*VARCHAR{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2VARCHAR(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, []*VARCHAR):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2VARCHAR(ops[0])
		oplist := []*VARCHAR{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2VARCHAR(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*VARCHAR, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2VARCHAR(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2VARCHAR(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*BIT, *BIT): // ---------- BIT -------------
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *BIT, *BIT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BIT(ops[0]), tst_op2BIT(ops[1]), tst_op2BIT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BIT, *BIT, *BIT, *BIT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2BIT(ops[0]), tst_op2BIT(ops[1]), tst_op2BIT(ops[2]), tst_op2BIT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *BIT, *BIT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2BIT(ops[1]), tst_op2BIT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BIT, *BIT) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2BIT(ops[1]), tst_op2BIT(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *BIT, []*BIT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2BIT(ops[1])
		oplist := []*BIT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2BIT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *BIT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2BIT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *BOOLEAN, *BIT, *BIT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2BIT(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2BIT(ops[2]), tst_op2BIT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BIT, *INT, []*BIT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2INT(ops[1])
		oplist := []*BIT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2BIT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*BIT, []*BIT):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2BIT(ops[0])
		oplist := []*BIT{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2BIT(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*BIT, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2BIT(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2BIT(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*TINYINT, *TINYINT): // ---------- TINYINT -------------
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *TINYINT, *TINYINT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2TINYINT(ops[0]), tst_op2TINYINT(ops[1]), tst_op2TINYINT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*TINYINT, *TINYINT, *TINYINT, *TINYINT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2TINYINT(ops[0]), tst_op2TINYINT(ops[1]), tst_op2TINYINT(ops[2]), tst_op2TINYINT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *TINYINT, *TINYINT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2TINYINT(ops[1]), tst_op2TINYINT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*TINYINT, *TINYINT) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2TINYINT(ops[1]), tst_op2TINYINT(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *TINYINT, []*TINYINT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2TINYINT(ops[1])
		oplist := []*TINYINT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2TINYINT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *TINYINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2TINYINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *BOOLEAN, *TINYINT, *TINYINT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2TINYINT(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2TINYINT(ops[2]), tst_op2TINYINT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*TINYINT, *INT, []*TINYINT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2INT(ops[1])
		oplist := []*TINYINT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2TINYINT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*TINYINT, []*TINYINT):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2TINYINT(ops[0])
		oplist := []*TINYINT{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2TINYINT(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*TINYINT, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2TINYINT(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2TINYINT(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*SMALLINT, *SMALLINT): // ---------- SMALLINT -------------
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *SMALLINT, *SMALLINT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2SMALLINT(ops[0]), tst_op2SMALLINT(ops[1]), tst_op2SMALLINT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*SMALLINT, *SMALLINT, *SMALLINT, *SMALLINT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2SMALLINT(ops[0]), tst_op2SMALLINT(ops[1]), tst_op2SMALLINT(ops[2]), tst_op2SMALLINT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *SMALLINT, *SMALLINT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2SMALLINT(ops[1]), tst_op2SMALLINT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*SMALLINT, *SMALLINT) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2SMALLINT(ops[1]), tst_op2SMALLINT(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *SMALLINT, []*SMALLINT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2SMALLINT(ops[1])
		oplist := []*SMALLINT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2SMALLINT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *SMALLINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2SMALLINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *BOOLEAN, *SMALLINT, *SMALLINT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2SMALLINT(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2SMALLINT(ops[2]), tst_op2SMALLINT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*SMALLINT, *INT, []*SMALLINT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2INT(ops[1])
		oplist := []*SMALLINT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2SMALLINT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*SMALLINT, []*SMALLINT):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2SMALLINT(ops[0])
		oplist := []*SMALLINT{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2SMALLINT(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*SMALLINT, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2SMALLINT(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2SMALLINT(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*INT, *INT): // ---------- INT -------------
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *INT, *INT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2INT(ops[0]), tst_op2INT(ops[1]), tst_op2INT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*INT, *INT, *FLOAT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2INT(ops[0]), tst_op2INT(ops[1]), tst_op2FLOAT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*INT, *INT, *INT, *INT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2INT(ops[0]), tst_op2INT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *INT, *INT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2INT(ops[1]), tst_op2INT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*INT, *INT) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2INT(ops[1]), tst_op2INT(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *INT, []*INT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2INT(ops[1])
		oplist := []*INT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2INT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *INT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2INT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARCHAR, *INT, *VARCHAR, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2INT(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*INT, *BOOLEAN, *INT, *INT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2INT(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*INT, *INT, []*INT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2INT(ops[1])
		oplist := []*INT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2INT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*INT, []*INT):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2INT(ops[0])
		oplist := []*INT{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2INT(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*INT, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2INT(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2INT(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*BIGINT, *BIGINT): // ---------- BIGINT -------------
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *BIGINT, *BIGINT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BIGINT(ops[0]), tst_op2BIGINT(ops[1]), tst_op2BIGINT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BIGINT, *BIGINT, *FLOAT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BIGINT(ops[0]), tst_op2BIGINT(ops[1]), tst_op2FLOAT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BIGINT, *BIGINT, *INT, *INT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2BIGINT(ops[0]), tst_op2BIGINT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *BIGINT, *BIGINT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2BIGINT(ops[1]), tst_op2BIGINT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BIGINT, *BIGINT) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2BIGINT(ops[1]), tst_op2BIGINT(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *BIGINT, []*BIGINT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2BIGINT(ops[1])
		oplist := []*BIGINT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2BIGINT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *BIGINT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2BIGINT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARCHAR, *BIGINT, *VARCHAR, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2BIGINT(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BIGINT, *BOOLEAN, *BIGINT, *BIGINT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2BIGINT(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2BIGINT(ops[2]), tst_op2BIGINT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BIGINT, *INT, []*BIGINT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2INT(ops[1])
		oplist := []*BIGINT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2BIGINT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*BIGINT, []*BIGINT):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2BIGINT(ops[0])
		oplist := []*BIGINT{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2BIGINT(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*BIGINT, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2BIGINT(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2BIGINT(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*VARCHAR, *MONEY): // ---------- MONEY -------------
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2MONEY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *MONEY):
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2MONEY(ops[1])
		rsql.Assert(len(ops) == 2)
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *MONEY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2MONEY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *MONEY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2MONEY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *MONEY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2MONEY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*MONEY, *MONEY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2MONEY(ops[0]), tst_op2MONEY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*MONEY, *MONEY, *MONEY):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2MONEY(ops[0]), tst_op2MONEY(ops[1]), tst_op2MONEY(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *MONEY):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2MONEY(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *MONEY, []*MONEY):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2MONEY(ops[1])
		oplist := []*MONEY{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2MONEY(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*MONEY, *BOOLEAN, *MONEY, *MONEY):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2MONEY(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2MONEY(ops[2]), tst_op2MONEY(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*MONEY, *INT, []*MONEY):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2MONEY(ops[0]), tst_op2INT(ops[1])
		oplist := []*MONEY{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2MONEY(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*MONEY, []*MONEY):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2MONEY(ops[0])
		oplist := []*MONEY{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2MONEY(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*MONEY, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2MONEY(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2MONEY(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*NUMERIC): // ---------- NUMERIC -------------
		rsql.Assert(len(ops) == 1)
		op0 := tst_op2NUMERIC(ops[0])
		myfunc(op0)
		blotch(op0)

	case func(*NUMERIC, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *NUMERIC, *NUMERIC):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2NUMERIC(ops[0]), tst_op2NUMERIC(ops[1]), tst_op2NUMERIC(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*NUMERIC, *NUMERIC, *FLOAT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2NUMERIC(ops[0]), tst_op2NUMERIC(ops[1]), tst_op2FLOAT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*NUMERIC, *NUMERIC, *INT, *INT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2NUMERIC(ops[0]), tst_op2NUMERIC(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *NUMERIC, *NUMERIC):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2NUMERIC(ops[1]), tst_op2NUMERIC(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*NUMERIC, *NUMERIC) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2NUMERIC(ops[1]), tst_op2NUMERIC(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *NUMERIC, []*NUMERIC):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2NUMERIC(ops[1])
		oplist := []*NUMERIC{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2NUMERIC(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *NUMERIC):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2NUMERIC(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARCHAR, *NUMERIC, *VARCHAR, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2NUMERIC(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*NUMERIC, *BOOLEAN, *NUMERIC, *NUMERIC):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2NUMERIC(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2NUMERIC(ops[2]), tst_op2NUMERIC(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*NUMERIC, *INT, []*NUMERIC):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2INT(ops[1])
		oplist := []*NUMERIC{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2NUMERIC(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*NUMERIC, []*NUMERIC):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2NUMERIC(ops[0])
		oplist := []*NUMERIC{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2NUMERIC(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*NUMERIC, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2NUMERIC(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2NUMERIC(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*FLOAT): // ---------- FLOAT -------------
		rsql.Assert(len(ops) == 1)
		op0 := tst_op2FLOAT(ops[0])
		myfunc(op0)
		blotch(op0)

	case func(*FLOAT, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*FLOAT, *FLOAT, *FLOAT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2FLOAT(ops[0]), tst_op2FLOAT(ops[1]), tst_op2FLOAT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*FLOAT, *FLOAT, *INT, *INT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2FLOAT(ops[0]), tst_op2FLOAT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *FLOAT, *FLOAT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2FLOAT(ops[1]), tst_op2FLOAT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*FLOAT, *FLOAT) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2FLOAT(ops[1]), tst_op2FLOAT(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *FLOAT, []*FLOAT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2FLOAT(ops[1])
		oplist := []*FLOAT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2FLOAT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*VARCHAR, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIT, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIT(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TINYINT, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TINYINT(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*SMALLINT, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2SMALLINT(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BIGINT, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BIGINT(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*NUMERIC, *FLOAT):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2NUMERIC(ops[0]), tst_op2FLOAT(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARCHAR, *FLOAT, *INT, *INT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2FLOAT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*VARCHAR, *FLOAT, *VARCHAR, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2FLOAT(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*FLOAT, *BOOLEAN, *FLOAT, *FLOAT):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2FLOAT(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2FLOAT(ops[2]), tst_op2FLOAT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*FLOAT, *INT, []*FLOAT):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2FLOAT(ops[0]), tst_op2INT(ops[1])
		oplist := []*FLOAT{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2FLOAT(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*FLOAT, []*FLOAT):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2FLOAT(ops[0])
		oplist := []*FLOAT{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2FLOAT(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*FLOAT, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2FLOAT(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2FLOAT(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*DATE, *INT, *INT, *INT): // ---------- DATE -------------
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2DATE(ops[0]), tst_op2INT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*VARCHAR, *DATE, *INT, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2DATE(ops[1]), tst_op2INT(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*BOOLEAN, *DATE):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2DATE(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *DATE, []*DATE):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2DATE(ops[1])
		oplist := []*DATE{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2DATE(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*DATE, *DATE):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATE(ops[0]), tst_op2DATE(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *DATE):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2DATE(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATE, *DATE, *DATE):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2DATE(ops[0]), tst_op2DATE(ops[1]), tst_op2DATE(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*DATE, *BOOLEAN, *DATE, *DATE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2DATE(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2DATE(ops[2]), tst_op2DATE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*DATE, *INT, []*DATE):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2DATE(ops[0]), tst_op2INT(ops[1])
		oplist := []*DATE{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2DATE(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*DATE, []*DATE):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2DATE(ops[0])
		oplist := []*DATE{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2DATE(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*DATE, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2DATE(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2DATE(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*DATE, *DATE, *INT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2DATE(ops[0]), tst_op2DATE(ops[1]), tst_op2INT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*TIME, *INT, *INT, *INT, *INT, *INT): // ---------- TIME -------------
		rsql.Assert(len(ops) == 6)
		op0, op1, op2, op3, op4, op5 := tst_op2TIME(ops[0]), tst_op2INT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3]), tst_op2INT(ops[4]), tst_op2INT(ops[5])
		myfunc(op0, op1, op2, op3, op4, op5)
		blotch(op0, op1, op2, op3, op4, op5)

	case func(*VARCHAR, *TIME, *INT, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2TIME(ops[1]), tst_op2INT(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*TIME, *INT, *TIME):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2TIME(ops[0]), tst_op2INT(ops[1]), tst_op2TIME(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *TIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2TIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *TIME, []*TIME):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2TIME(ops[1])
		oplist := []*TIME{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2TIME(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*TIME, *TIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TIME(ops[0]), tst_op2TIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *TIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2TIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TIME, *TIME, *TIME):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2TIME(ops[0]), tst_op2TIME(ops[1]), tst_op2TIME(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*TIME, *BOOLEAN, *TIME, *TIME):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2TIME(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2TIME(ops[2]), tst_op2TIME(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*TIME, *INT, []*TIME):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2TIME(ops[0]), tst_op2INT(ops[1])
		oplist := []*TIME{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2TIME(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*TIME, []*TIME):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2TIME(ops[0])
		oplist := []*TIME{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2TIME(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*TIME, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2TIME(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2TIME(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*BOOLEAN, *DATETIME, *DATETIME): // ---------- DATETIME -------------
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2BOOLEAN(ops[0]), tst_op2DATETIME(ops[1]), tst_op2DATETIME(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*DATETIME, *DATETIME) Compsort_t:
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := ops[0].(*Compsort_t), tst_op2DATETIME(ops[1]), tst_op2DATETIME(ops[2])
		*op0 = myfunc(op1, op2)
		blotch(op0, op1, op2)

	case func(*BOOLEAN, *DATETIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2DATETIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*BOOLEAN, *DATETIME, []*DATETIME):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2BOOLEAN(ops[0]), tst_op2DATETIME(ops[1])
		oplist := []*DATETIME{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2DATETIME(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*INT, *DATETIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2INT(ops[0]), tst_op2DATETIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*INT, *DATETIME, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2INT(ops[0]), tst_op2DATETIME(ops[1]), tst_op2SYSLANGUAGE(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*VARCHAR, *DATETIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2VARCHAR(ops[0]), tst_op2DATETIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*VARCHAR, *DATETIME, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2VARCHAR(ops[0]), tst_op2DATETIME(ops[1]), tst_op2SYSLANGUAGE(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*DATETIME, *INT, *DATETIME):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2DATETIME(ops[0]), tst_op2INT(ops[1]), tst_op2DATETIME(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*INT, *DATETIME, *DATETIME):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2INT(ops[0]), tst_op2DATETIME(ops[1]), tst_op2DATETIME(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*VARCHAR, *DATETIME, *VARCHAR, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2DATETIME(ops[1]), tst_op2VARCHAR(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*VARCHAR, *DATETIME, *INT, *SYSLANGUAGE):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2VARCHAR(ops[0]), tst_op2DATETIME(ops[1]), tst_op2INT(ops[2]), tst_op2SYSLANGUAGE(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*DATETIME, *INT, *INT, *INT, *INT, *INT, *INT, *INT):
		rsql.Assert(len(ops) == 8)
		op0, op1, op2, op3, op4, op5, op6, op7 := tst_op2DATETIME(ops[0]), tst_op2INT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3]), tst_op2INT(ops[4]), tst_op2INT(ops[5]), tst_op2INT(ops[6]), tst_op2INT(ops[7])
		myfunc(op0, op1, op2, op3, op4, op5, op6, op7)
		blotch(op0, op1, op2, op3, op4, op5, op6, op7)

	case func(*DATETIME, *INT, *INT, *INT, *INT, *INT, *INT, *INT, *INT):
		rsql.Assert(len(ops) == 9)
		op0, op1, op2, op3, op4, op5, op6, op7, op8 := tst_op2DATETIME(ops[0]), tst_op2INT(ops[1]), tst_op2INT(ops[2]), tst_op2INT(ops[3]), tst_op2INT(ops[4]), tst_op2INT(ops[5]), tst_op2INT(ops[6]), tst_op2INT(ops[7]), tst_op2INT(ops[8])
		myfunc(op0, op1, op2, op3, op4, op5, op6, op7, op8)
		blotch(op0, op1, op2, op3, op4, op5, op6, op7, op8)

	case func(*DATETIME, *DATETIME, *DATETIME):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2DATETIME(ops[0]), tst_op2DATETIME(ops[1]), tst_op2DATETIME(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*DATETIME, *BOOLEAN, *DATETIME, *DATETIME):
		rsql.Assert(len(ops) == 4)
		op0, op1, op2, op3 := tst_op2DATETIME(ops[0]), tst_op2BOOLEAN(ops[1]), tst_op2DATETIME(ops[2]), tst_op2DATETIME(ops[3])
		myfunc(op0, op1, op2, op3)
		blotch(op0, op1, op2, op3)

	case func(*DATETIME, *INT, []*DATETIME):
		rsql.Assert(len(ops) >= 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2INT(ops[1])
		oplist := []*DATETIME{}
		for _, e := range ops[2:] {
			oplist = append(oplist, tst_op2DATETIME(e))
		}
		myfunc(op0, op1, oplist)
		blotch(op0, op1, oplist)

	case func(*DATETIME, []*DATETIME):
		rsql.Assert(len(ops) >= 1)
		op0 := tst_op2DATETIME(ops[0])
		oplist := []*DATETIME{}
		for _, e := range ops[1:] {
			oplist = append(oplist, tst_op2DATETIME(e))
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*DATETIME, []Case_element_t):
		rsql.Assert(len(ops) >= 1 && uint64(len(ops))&0x0001 == 1)
		op0 := tst_op2DATETIME(ops[0])
		oplist := []Case_element_t{}
		for i := 1; i < len(ops); i += 2 {
			case_element := Case_element_t{tst_op2BOOLEAN(ops[i]), tst_op2DATETIME(ops[i+1])}
			oplist = append(oplist, case_element)
		}
		myfunc(op0, oplist)
		blotch(op0, oplist)

	case func(*DATETIME, *DATETIME, *INT):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2DATETIME(ops[0]), tst_op2DATETIME(ops[1]), tst_op2INT(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	case func(*DATE, *DATETIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATE(ops[0]), tst_op2DATETIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*TIME, *DATETIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2TIME(ops[0]), tst_op2DATETIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *DATETIME):
		rsql.Assert(len(ops) == 2)
		op0, op1 := tst_op2DATETIME(ops[0]), tst_op2DATETIME(ops[1])
		myfunc(op0, op1)
		blotch(op0, op1)

	case func(*DATETIME, *DATE, *TIME):
		rsql.Assert(len(ops) == 3)
		op0, op1, op2 := tst_op2DATETIME(ops[0]), tst_op2DATE(ops[1]), tst_op2TIME(ops[2])
		myfunc(op0, op1, op2)
		blotch(op0, op1, op2)

	default:
		panic("######### tst_function_tester(): function signature is unknown. Please, add it in the list above. ###########")
	}

	//=== if result is VARCHAR, put garbage in VARCHAR operands, to be sure that result doesn't point to a VARCHAR operand ===

	for i := 1; i < len(ops); i++ {
		if operand, ok := ops[i].(*VARCHAR); ok == true {
			operand.data_val = append(operand.data_val[:0], '#') // # is the garbage
		}
	}

}

// blotch writes garbage in the arguments.
// These arguments should be the same as thoses passed to the tested function.
// The first argument is left intact, as it contains the result of the tested function.
//
func blotch(args ...interface{}) {

	for i := 1; i < len(args); i++ {
		blotch_arg(args[i])
	}

}

func blotch_arg(arg interface{}) {

	switch e := arg.(type) {
	case *BOOLEAN:
		e.data_val = !e.data_val

	case *VARBINARY:
		e.data_val = append(e.data_val[:0], '#') // any improbable character is ok as garbage. We put one character to avoid resizing, so that source original buffer is written to.

	case *VARCHAR:
		e.data_val = append(e.data_val[:0], '#') // same comment as for *MONEY

	case *BIT:
		e.data_val = 74635278

	case *TINYINT:
		e.data_val = 352499943

	case *SMALLINT:
		e.data_val = 63845777366

	case *INT:
		e.data_val = 9473856576743

	case *BIGINT:
		e.data_val = 1837745467684544

	case *MONEY:
		quad.From_int32_raw(&e.data_val, 536499744)

	case *NUMERIC:
		quad.From_int32_raw(&e.data_val, 396175646)

	case *FLOAT:
		e.data_val = 263.46758

	case *DATE:
		e.data_val.Time = time.Date(5048, 4, 3, 13, 20, 10, 5, time.UTC)

	case *TIME:
		e.data_val.Time = time.Date(7463, 10, 2, 2, 5, 1, 8, time.UTC)

	case *DATETIME:
		e.data_val.Time = time.Date(546, 2, 13, 20, 45, 18, 17, time.UTC)

	case []*BOOLEAN:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*VARBINARY:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*VARCHAR:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*BIT:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*TINYINT:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*SMALLINT:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*INT:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*BIGINT:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*MONEY:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*NUMERIC:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*FLOAT:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*DATE:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*TIME:
		for _, ee := range e {
			blotch_arg(ee)
		}
	case []*DATETIME:
		for _, ee := range e {
			blotch_arg(ee)
		}

	case []Case_element_t:
		for _, ee := range e {
			blotch_arg(ee.Val)
		}

	default:
		// do nothing for other datatypes. E.g. *VOID is not touched because it has no internal value.
	}
}

func tst_function_result_checker(r interface{}, result interface{}) (ok bool) {

	// check if Compsort_t

	if _, ok := result.(Compsort_t); ok {
		if _, ok := r.(*Compsort_t); ok == false {
			return false
		}

		if *r.(*Compsort_t) != result.(Compsort_t) {
			return false
		}
		return true
	}

	// check if error or NULL

	switch result := result.(type) {
	case rsql.Message_id_t: // an error is expected
		if r.(rsql.IDataslot).Error() == nil || r.(rsql.IDataslot).Error().Message_id != result {
			return false
		}
		return true // result is error as expected

	case nil: // NULL is expected
		if r.(rsql.IDataslot).Error() != nil {
			return false
		}

		if r.(rsql.IDataslot).NULL_flag() == false {
			return false
		}
		return true // result is NULL as expected
	}

	// check the value

	if r.(rsql.IDataslot).Error() != nil || r.(rsql.IDataslot).NULL_flag() {
		return false
	}

	switch r := r.(type) {

	case *BOOLEAN:
		r_BOOLEAN_check := tst_op2BOOLEAN(result)
		if r.data_val != r_BOOLEAN_check.data_val {
			return false
		}

	case *VARBINARY:
		r_VARBINARY_check := tst_op2VARBINARY(result)
		if bytes.Equal(r.data_val, r_VARBINARY_check.data_val) == false {
			return false
		}

	case *VARCHAR:
		r_VARCHAR_check := tst_op2VARCHAR(result)
		if mstr.Equal_strict(r.data_val, r_VARCHAR_check.data_val) == false {
			return false
		}

	case *REGEXPLIKE:
		r_REGEXPLIKE_check := tst_op2REGEXPLIKE(result)
		if r.data_val.String() != r_REGEXPLIKE_check.data_val.String() {
			return false
		}

	case *BIT:
		r_BIT_check := tst_op2BIT(result)
		if r.data_val != r_BIT_check.data_val {
			return false
		}

	case *TINYINT:
		r_TINYINT_check := tst_op2TINYINT(result)
		if r.data_val != r_TINYINT_check.data_val {
			return false
		}

	case *SMALLINT:
		r_SMALLINT_check := tst_op2SMALLINT(result)
		if r.data_val != r_SMALLINT_check.data_val {
			return false
		}

	case *INT:
		r_INT_check := tst_op2INT(result)
		if r.data_val != r_INT_check.data_val {
			return false
		}

	case *BIGINT:
		r_BIGINT_check := tst_op2BIGINT(result)
		if r.data_val != r_BIGINT_check.data_val {
			return false
		}

	case *MONEY:
		r_MONEY_check := tst_op2MONEY(result)
		if quad.Check_equality_FOR_TEST(&r.data_val, &r_MONEY_check.data_val) == false {
			return false
		}

	case *NUMERIC:
		r_NUMERIC_check := tst_op2NUMERIC(result)
		if quad.Check_equality_FOR_TEST(&r.data_val, &r_NUMERIC_check.data_val) == false {
			return false
		}

	case *FLOAT:
		r_FLOAT_check := tst_op2FLOAT(result)
		if float64_equal(r.data_val, r_FLOAT_check.data_val) == false {
			return false
		}

	case *DATE:
		r_DATE_check := tst_op2DATE(result)
		if mdat.Equal(r.data_val, r_DATE_check.data_val) == false {
			return false
		}

	case *TIME:
		r_TIME_check := tst_op2TIME(result)
		if mdat.Equal(r.data_val, r_TIME_check.data_val) == false {
			return false
		}

	case *DATETIME:
		r_DATETIME_check := tst_op2DATETIME(result)
		if mdat.Equal(r.data_val, r_DATETIME_check.data_val) == false {
			return false
		}

	default:
		panic("unknown type to test")
	}

	return true
}
