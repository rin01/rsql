package data

import (
	"time"

	"rsql"
	"rsql/mdat"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *DATETIME) RO_LEAF_hashval() uint32 {
	var (
		seconds     uint64 // unix seconds
		nanoseconds uint64
		hash        uint32
	)

	hash = uint32(rsql.DATATYPE_DATETIME)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		seconds = uint64(a.data_val.Unix())
		nanoseconds = uint64(a.data_val.Nanosecond())

		hash += uint32(seconds)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(seconds >> 32)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(nanoseconds)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(nanoseconds >> 32)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *DATETIME) String() string {

	if a.data_error != nil {
		return "DATETIME: error"
	}

	if a.data_NULL_flag == true {
		return "DATETIME: null"
	}

	return a.data_val.Format("DATETIME: 2006-01-02 15:04:05.999999999")
}

// New_DATETIME_NULL creates a new data.DATETIME initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_DATETIME_NULL(kind rsql.Kind_t) *DATETIME {

	dataslot := &DATETIME{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_DATETIME,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		data_val: mdat.MyDatetime{Time: mdat.TIME_1900_01_01},
	}

	return dataslot
}

// New_literal_DATETIME_value creates a new data.DATETIME from a value.
//
// Value string must be of the form "1983-04-20 13:30:04.10".
//
func New_literal_DATETIME_value(val string) (*DATETIME, *rsql.Error) {
	var (
		err error
		t   time.Time
	)

	if t, err = mdat.Parse([]byte(val), "YMD", 0, mdat.PARSE_MODE_DATETIME); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_CONVERSION_STRING_TO_DATETIME_FAILED, rsql.ERROR_BATCH_ABORT, val)
	}

	dataslot := &DATETIME{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_DATETIME,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: mdat.MyDatetime{Time: t},
	}

	return dataslot, nil
}

// New_VARIABLE_DATETIME_NULL creates a new data.DATETIME, used when we create a new global variable with NULL initial value.
//
// Exactly the same as New_VARCHAR_NULL, except that the dataslot is KIND_VAR_LEAF.
//
func New_VARIABLE_DATETIME_NULL() *DATETIME {
	var (
		dataslot *DATETIME
	)

	dataslot = New_DATETIME_NULL(rsql.KIND_VAR_LEAF)

	return dataslot
}

func Add_date_time_DATETIME(r *DATETIME, a *DATE, b *TIME) {
	var (
		rsql_err            *rsql.Error
		nanosecond_duration int64
		res                 time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	nanosecond_duration = int64(b.data_val.Sub(mdat.TIME_1900_01_01))

	if res, rsql_err = mdat.Add_nanosecond_duration(a.data_val, nanosecond_duration); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Comp_equal_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val.Equal(b.data_val.Time)

	r.data_val = r_val
}

func Comp_greater_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val.After(b.data_val.Time)

	r.data_val = r_val
}

func Comp_less_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val.Before(b.data_val.Time)

	r.data_val = r_val
}

func Comp_greater_equal_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = !a.data_val.Before(b.data_val.Time)

	r.data_val = r_val
}

func Comp_less_equal_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = !a.data_val.After(b.data_val.Time)

	r.data_val = r_val
}

func Comp_not_equal_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = !a.data_val.Equal(b.data_val.Time)

	r.data_val = r_val
}

func Is_null_DATETIME(r *BOOLEAN, a *DATETIME) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_DATETIME(r *BOOLEAN, a *DATETIME) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_DATETIME checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_DATETIME(r *BOOLEAN, a *DATETIME, b_list []*DATETIME) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_DATETIME(r, a, b)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_DATETIME(r *BOOLEAN, a *DATETIME, b_list []*DATETIME) {

	In_list_DATETIME(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_DATETIME(r *DATETIME, a_list []Case_element_t) {
	var (
		elem_val *DATETIME
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*DATETIME)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			r.data_val = elem_val.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_DATETIME_to_DATE(r *DATE, a *DATETIME) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res := a.data_val.Truncate(time.Hour * 24)

	r.data_val.Time = res
}

func Cast_DATETIME_to_TIME(r *TIME, a *DATETIME) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	hour, minute, second := a.data_val.Clock()
	nanosecond := a.data_val.Nanosecond()
	res := time.Date(1900, 1, 1, hour, minute, second, nanosecond, time.UTC)

	r.data_val.Time = res
}

func Cast_DATETIME_to_DATETIME(r *DATETIME, a *DATETIME) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Assign_DATETIME(r *DATETIME, a *DATETIME) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

func Copy_DATETIME(r *DATETIME, a *DATETIME) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_DATETIME(kind rsql.Kind_t, a *DATETIME) *DATETIME {

	return New_DATETIME_NULL(kind)
}
