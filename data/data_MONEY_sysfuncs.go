package data

import (
	"rsql/quad"
)

func Sysfunc_isnull_MONEY(r *MONEY, a *MONEY, b *MONEY) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
	}
}

func Sysfunc_iif_MONEY(r *MONEY, a *BOOLEAN, b *MONEY, c *MONEY) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	r.data_val = c.data_val
}

func Sysfunc_choose_MONEY(r *MONEY, a *INT, b_list []*MONEY) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	r.data_val = elem.data_val
}

func Sysfunc_coalesce_MONEY(r *MONEY, a_list []*MONEY) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = elem.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_MONEY(r *BOOLEAN, a *MONEY, b *MONEY, c *MONEY) {
	var (
		res_b int
		res_c int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	res_b = quad.Compare(&a.data_val, &b.data_val) // never fails
	res_c = quad.Compare(&a.data_val, &c.data_val) // never fails

	r.data_val = (res_b >= 0 && res_c <= 0)
}

func Sysfunc_not_between_MONEY(r *BOOLEAN, a *MONEY, b *MONEY, c *MONEY) {

	Sysfunc_between_MONEY(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_MONEY(r *VARCHAR, a *MONEY) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "MONEY"...)
}
