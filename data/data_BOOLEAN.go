package data

import (
	"fmt"

	"rsql"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *BOOLEAN) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_BOOLEAN)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		if a.data_val == true {
			hash += 1
		}
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *BOOLEAN) String() string {

	if a.data_error != nil {
		return "BOOLEAN: error"
	}

	if a.data_NULL_flag == true {
		return "BOOLEAN: null"
	}

	return fmt.Sprintf("BOOLEAN: %t", a.data_val)
}

func (a *BOOLEAN) Is_true() bool {

	if a.data_error == nil && a.data_NULL_flag == false && a.data_val == true {
		return true
	}

	return false
}

// New_BOOLEAN_NULL creates a new data.BOOLEAN initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_BOOLEAN_NULL(kind rsql.Kind_t) *BOOLEAN {

	dataslot := &BOOLEAN{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_BOOLEAN,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		data_val: false,
	}

	return dataslot
}

// New_literal_BOOLEAN_value creates a new data.BOOLEAN from a value.
//
func New_literal_BOOLEAN_value(val bool) *BOOLEAN {

	dataslot := &BOOLEAN{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_BOOLEAN,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: val,
	}

	return dataslot
}

// New_literal_BOOLEAN creates a new data.BOOLEAN from literal string.
//
func New_literal_BOOLEAN(s string) *BOOLEAN {

	if s == "true" {
		return New_literal_BOOLEAN_value(true)
	}

	return New_literal_BOOLEAN_value(false)
}

// Logical_unary_not_BOOLEAN computes the logical NOT operation.
//
func Logical_unary_not_BOOLEAN(r *BOOLEAN, a *BOOLEAN) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = !a.data_val
}

// Logical_and_BOOLEAN computes the logical AND operation.
//
//     Note that MS SQL Server,     false AND error -> false
//                          but     error AND false -> error
//
//     For rsql, we prefer to have false as result in both cases.
//
func Logical_and_BOOLEAN(r *BOOLEAN, a *BOOLEAN, b *BOOLEAN) {

	// if a is TRUE or FALSE

	if a.data_error == nil && a.data_NULL_flag == false {
		if a.data_val == false {
			r.data_error = nil // if FALSE, result is always FALSE
			r.data_NULL_flag = false
			r.data_val = false
			return
		}

		// if TRUE, result is same as the other operand (error, NULL, TRUE, or FALSE)

		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if b is TRUE or FALSE

	if b.data_error == nil && b.data_NULL_flag == false {
		if b.data_val == false {
			r.data_error = nil // if FALSE, result is always FALSE
			r.data_NULL_flag = false
			r.data_val = false
			return
		}

		// if TRUE, result is same as the other operand (error, NULL, TRUE, or FALSE)

		r.data_error = a.data_error
		r.data_NULL_flag = a.data_NULL_flag
		r.data_val = a.data_val
		return
	}

	// if a is error, return error

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	// if b is error, return error

	if b.data_error != nil {
		r.data_error = b.data_error
		return
	}

	// a and b are NULL, return NULL

	rsql.Assert(a.data_NULL_flag == true && b.data_NULL_flag == true)

	r.data_error = nil
	r.data_NULL_flag = true
}

// Logical_or_BOOLEAN computes the logical OR operation.
//
//     Note that MS SQL Server,     true OR error -> true
//                          but     error OR true -> error
//
//     For rsql, we prefer to have true as result in both cases.
//
func Logical_or_BOOLEAN(r *BOOLEAN, a *BOOLEAN, b *BOOLEAN) {

	// if a is TRUE or FALSE

	if a.data_error == nil && a.data_NULL_flag == false {
		if a.data_val == true {
			r.data_error = nil // if TRUE, result is always TRUE
			r.data_NULL_flag = false
			r.data_val = true
			return
		}

		// if FALSE, result is same as the other operand (error, NULL, TRUE, or FALSE)

		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if b is TRUE or FALSE

	if b.data_error == nil && b.data_NULL_flag == false {
		if b.data_val == true {
			r.data_error = nil // if TRUE, result is always TRUE
			r.data_NULL_flag = false
			r.data_val = true
			return
		}

		// if FALSE, result is same as the other operand (error, NULL, TRUE, or FALSE)

		r.data_error = a.data_error
		r.data_NULL_flag = a.data_NULL_flag
		r.data_val = a.data_val
		return
	}

	// if a is error, return error

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	// if b is error, return error

	if b.data_error != nil {
		r.data_error = b.data_error
		return
	}

	// a and b are NULL, return NULL

	rsql.Assert(a.data_NULL_flag == true && b.data_NULL_flag == true)

	r.data_error = nil
	r.data_NULL_flag = true
}

func Copy_BOOLEAN(r *BOOLEAN, a *BOOLEAN) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_BOOLEAN(kind rsql.Kind_t, a *BOOLEAN) *BOOLEAN {

	return New_BOOLEAN_NULL(kind)
}
