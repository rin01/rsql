package data

import (
	"bytes"

	"rsql"
	"rsql/quad"
)

// RO_LEAF_Data_XXX_equal compares two dataslots for equality, comparing datatypes, attributes and value.
// Dataslot must be KIND_RO_LEAF. This means that it is a constant literal, or it is a constant computed during constant folding stage.
//
func RO_LEAF_Data_XXX_equal(dataslot_a rsql.IDataslot, dataslot_b rsql.IDataslot) bool {
	var (
		dataslot_a_datatype rsql.Datatype_t
		dataslot_b_datatype rsql.Datatype_t

		//dataslot_a_VOID *VOID
		dataslot_a_SYSCOLLATOR *SYSCOLLATOR
		dataslot_a_SYSLANGUAGE *SYSLANGUAGE
		dataslot_a_BOOLEAN     *BOOLEAN
		dataslot_a_VARBINARY   *VARBINARY
		dataslot_a_VARCHAR     *VARCHAR
		dataslot_a_REGEXPLIKE  *REGEXPLIKE
		dataslot_a_BIT         *BIT
		dataslot_a_TINYINT     *TINYINT
		dataslot_a_SMALLINT    *SMALLINT
		dataslot_a_INT         *INT
		dataslot_a_BIGINT      *BIGINT
		dataslot_a_MONEY       *MONEY
		dataslot_a_NUMERIC     *NUMERIC
		dataslot_a_FLOAT       *FLOAT
		dataslot_a_DATE        *DATE
		dataslot_a_TIME        *TIME
		dataslot_a_DATETIME    *DATETIME

		//dataslot_b_VOID *VOID
		dataslot_b_SYSCOLLATOR *SYSCOLLATOR
		dataslot_b_SYSLANGUAGE *SYSLANGUAGE
		dataslot_b_BOOLEAN     *BOOLEAN
		dataslot_b_VARBINARY   *VARBINARY
		dataslot_b_VARCHAR     *VARCHAR
		dataslot_b_REGEXPLIKE  *REGEXPLIKE
		dataslot_b_BIT         *BIT
		dataslot_b_TINYINT     *TINYINT
		dataslot_b_SMALLINT    *SMALLINT
		dataslot_b_INT         *INT
		dataslot_b_BIGINT      *BIGINT
		dataslot_b_MONEY       *MONEY
		dataslot_b_NUMERIC     *NUMERIC
		dataslot_b_FLOAT       *FLOAT
		dataslot_b_DATE        *DATE
		dataslot_b_TIME        *TIME
		dataslot_b_DATETIME    *DATETIME
	)

	rsql.Assert(dataslot_a.Kind() == rsql.KIND_RO_LEAF && dataslot_b.Kind() == rsql.KIND_RO_LEAF)

	dataslot_a_datatype = dataslot_a.Datatype()
	dataslot_b_datatype = dataslot_b.Datatype()

	if dataslot_a_datatype != dataslot_b_datatype { // if not same datatype, return false.
		return false
	}

	if dataslot_a.Error() != nil || dataslot_b.Error() != nil {
		return false // even if both dataslots have the same Message_id, the errors originate from different line/pos. So, we consider them to be different.
	}

	if dataslot_a.NULL_flag() == true || dataslot_b.NULL_flag() == true {
		if dataslot_a.NULL_flag() == dataslot_b.NULL_flag() {
			return true
		}
		return false
	}

	// dataslots are not error nor NULL. Check their values for equality.

	switch dataslot_a_datatype {
	case rsql.DATATYPE_VOID:
		return true

	case rsql.DATATYPE_SYSCOLLATOR:
		dataslot_a_SYSCOLLATOR = dataslot_a.(*SYSCOLLATOR)
		dataslot_b_SYSCOLLATOR = dataslot_b.(*SYSCOLLATOR)

		if dataslot_a_SYSCOLLATOR.data_collation_string == dataslot_b_SYSCOLLATOR.data_collation_string {
			return true
		}

	case rsql.DATATYPE_SYSLANGUAGE:
		dataslot_a_SYSLANGUAGE = dataslot_a.(*SYSLANGUAGE)
		dataslot_b_SYSLANGUAGE = dataslot_b.(*SYSLANGUAGE)

		if dataslot_a_SYSLANGUAGE.data_langinfo.Lng_locale_name == dataslot_b_SYSLANGUAGE.data_langinfo.Lng_locale_name &&
			dataslot_a_SYSLANGUAGE.data_firstdayofweek == dataslot_b_SYSLANGUAGE.data_firstdayofweek &&
			dataslot_a_SYSLANGUAGE.data_DMY == dataslot_b_SYSLANGUAGE.data_DMY {
			return true
		}

	case rsql.DATATYPE_BOOLEAN:
		dataslot_a_BOOLEAN = dataslot_a.(*BOOLEAN)
		dataslot_b_BOOLEAN = dataslot_b.(*BOOLEAN)

		if dataslot_a_BOOLEAN.data_val == dataslot_b_BOOLEAN.data_val {
			return true
		}

	case rsql.DATATYPE_VARBINARY:
		dataslot_a_VARBINARY = dataslot_a.(*VARBINARY)
		dataslot_b_VARBINARY = dataslot_b.(*VARBINARY)

		if dataslot_a_VARBINARY.Data_precision == dataslot_b_VARBINARY.Data_precision &&
			bytes.Compare(dataslot_a_VARBINARY.data_val, dataslot_b_VARBINARY.data_val) == 0 {
			return true
		}

	case rsql.DATATYPE_VARCHAR:
		dataslot_a_VARCHAR = dataslot_a.(*VARCHAR)
		dataslot_b_VARCHAR = dataslot_b.(*VARCHAR)

		if dataslot_a_VARCHAR.Data_precision == dataslot_b_VARCHAR.Data_precision &&
			dataslot_a_VARCHAR.Data_fixlen_flag == dataslot_b_VARCHAR.Data_fixlen_flag &&
			bytes.Compare(dataslot_a_VARCHAR.data_val, dataslot_b_VARCHAR.data_val) == 0 {
			return true
		}

	case rsql.DATATYPE_REGEXPLIKE:
		dataslot_a_REGEXPLIKE = dataslot_a.(*REGEXPLIKE)
		dataslot_b_REGEXPLIKE = dataslot_b.(*REGEXPLIKE)

		if dataslot_a_REGEXPLIKE.data_val.Equal(&dataslot_b_REGEXPLIKE.data_val) == true {
			return true
		}

	case rsql.DATATYPE_BIT:
		dataslot_a_BIT = dataslot_a.(*BIT)
		dataslot_b_BIT = dataslot_b.(*BIT)

		if dataslot_a_BIT.data_val == dataslot_b_BIT.data_val {
			return true
		}

	case rsql.DATATYPE_TINYINT:
		dataslot_a_TINYINT = dataslot_a.(*TINYINT)
		dataslot_b_TINYINT = dataslot_b.(*TINYINT)

		if dataslot_a_TINYINT.data_val == dataslot_b_TINYINT.data_val {
			return true
		}

	case rsql.DATATYPE_SMALLINT:
		dataslot_a_SMALLINT = dataslot_a.(*SMALLINT)
		dataslot_b_SMALLINT = dataslot_b.(*SMALLINT)

		if dataslot_a_SMALLINT.data_val == dataslot_b_SMALLINT.data_val {
			return true
		}

	case rsql.DATATYPE_INT:
		dataslot_a_INT = dataslot_a.(*INT)
		dataslot_b_INT = dataslot_b.(*INT)

		if dataslot_a_INT.data_val == dataslot_b_INT.data_val {
			return true
		}

	case rsql.DATATYPE_BIGINT:
		dataslot_a_BIGINT = dataslot_a.(*BIGINT)
		dataslot_b_BIGINT = dataslot_b.(*BIGINT)

		if dataslot_a_BIGINT.data_val == dataslot_b_BIGINT.data_val {
			return true
		}

	case rsql.DATATYPE_MONEY:
		dataslot_a_MONEY = dataslot_a.(*MONEY)
		dataslot_b_MONEY = dataslot_b.(*MONEY)

		if quad.Compare(&dataslot_a_MONEY.data_val, &dataslot_b_MONEY.data_val) == 0 {
			return true
		}

	case rsql.DATATYPE_NUMERIC:
		dataslot_a_NUMERIC = dataslot_a.(*NUMERIC)
		dataslot_b_NUMERIC = dataslot_b.(*NUMERIC)

		if dataslot_a_NUMERIC.Data_precision == dataslot_b_NUMERIC.Data_precision &&
			dataslot_a_NUMERIC.Data_scale == dataslot_b_NUMERIC.Data_scale &&
			quad.Compare(&dataslot_a_NUMERIC.data_val, &dataslot_b_NUMERIC.data_val) == 0 {
			return true
		}

	case rsql.DATATYPE_FLOAT:
		dataslot_a_FLOAT = dataslot_a.(*FLOAT)
		dataslot_b_FLOAT = dataslot_b.(*FLOAT)

		if dataslot_a_FLOAT.data_val == dataslot_b_FLOAT.data_val {
			return true
		}

	case rsql.DATATYPE_DATE:
		dataslot_a_DATE = dataslot_a.(*DATE)
		dataslot_b_DATE = dataslot_b.(*DATE)

		if dataslot_a_DATE.data_val.Time.Equal(dataslot_b_DATE.data_val.Time) {
			return true
		}

	case rsql.DATATYPE_TIME:
		dataslot_a_TIME = dataslot_a.(*TIME)
		dataslot_b_TIME = dataslot_b.(*TIME)

		if dataslot_a_TIME.data_val.Time.Equal(dataslot_b_TIME.data_val.Time) {
			return true
		}

	case rsql.DATATYPE_DATETIME:
		dataslot_a_DATETIME = dataslot_a.(*DATETIME)
		dataslot_b_DATETIME = dataslot_b.(*DATETIME)

		if dataslot_a_DATETIME.data_val.Time.Equal(dataslot_b_DATETIME.data_val.Time) {
			return true
		}

	default:
		panic("datatype not expected")
	}

	return false
}

// Data_XXX_result_properties_equal compares two result dataslots, and return true if datatypes and properties are the same.
//
func Data_XXX_result_properties_equal(dataslot_a rsql.IDataslot, dataslot_b rsql.IDataslot) bool {
	var (
		dataslot_a_datatype rsql.Datatype_t
		dataslot_b_datatype rsql.Datatype_t

		dataslot_a_VARBINARY *VARBINARY
		dataslot_a_VARCHAR   *VARCHAR
		dataslot_a_NUMERIC   *NUMERIC

		dataslot_b_VARBINARY *VARBINARY
		dataslot_b_VARCHAR   *VARCHAR
		dataslot_b_NUMERIC   *NUMERIC
	)

	dataslot_a_datatype = dataslot_a.Datatype()
	dataslot_b_datatype = dataslot_b.Datatype()

	if dataslot_a_datatype != dataslot_b_datatype { // if not same datatype, return false.
		return false
	}

	switch dataslot_a_datatype {
	case rsql.DATATYPE_VOID,
		rsql.DATATYPE_SYSCOLLATOR,
		rsql.DATATYPE_SYSLANGUAGE,
		rsql.DATATYPE_BOOLEAN,

		rsql.DATATYPE_REGEXPLIKE,
		rsql.DATATYPE_BIT,
		rsql.DATATYPE_TINYINT,
		rsql.DATATYPE_SMALLINT,
		rsql.DATATYPE_INT,
		rsql.DATATYPE_BIGINT,
		rsql.DATATYPE_MONEY,

		rsql.DATATYPE_FLOAT,
		rsql.DATATYPE_DATE,
		rsql.DATATYPE_TIME,
		rsql.DATATYPE_DATETIME:
		return true

	case rsql.DATATYPE_VARBINARY:
		dataslot_a_VARBINARY = dataslot_a.(*VARBINARY)
		dataslot_b_VARBINARY = dataslot_b.(*VARBINARY)

		if dataslot_a_VARBINARY.Data_precision == dataslot_b_VARBINARY.Data_precision {
			return true
		}

	case rsql.DATATYPE_VARCHAR:
		dataslot_a_VARCHAR = dataslot_a.(*VARCHAR)
		dataslot_b_VARCHAR = dataslot_b.(*VARCHAR)

		if dataslot_a_VARCHAR.Data_precision == dataslot_b_VARCHAR.Data_precision &&
			dataslot_a_VARCHAR.Data_fixlen_flag == dataslot_b_VARCHAR.Data_fixlen_flag {
			return true
		}

	case rsql.DATATYPE_NUMERIC:
		dataslot_a_NUMERIC = dataslot_a.(*NUMERIC)
		dataslot_b_NUMERIC = dataslot_b.(*NUMERIC)

		if dataslot_a_NUMERIC.Data_precision == dataslot_b_NUMERIC.Data_precision &&
			dataslot_a_NUMERIC.Data_scale == dataslot_b_NUMERIC.Data_scale {
			return true
		}

	default:
		panic("datatype not expected")
	}

	return false
}
