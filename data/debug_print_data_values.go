package data

import (
	"fmt"
	"unicode/utf8"

	"rsql"
	"rsql/mbin"
	"rsql/quad"
)

// Debug_display_row displays a string representation of a Row.
// It is used for debugging only, as it is quite rough.
//
// width_limit is the maximum width for VARBINARY and VARCHAR.
// If option_ms is true, milliseconds are displayed for TIME and DATETIME.
//
func Debug_display_row(row rsql.Row, width_limit int, separator byte, option_ms bool) string {
	const DEFAULT_BUFF_CAPACITY = 200

	var (
		buff   []byte = make([]byte, 0, DEFAULT_BUFF_CAPACITY)
		s      string
		result []byte
	)

	if width_limit < 1 {
		width_limit = 1
	}

	result = append(result[:0], separator)

	for _, row_dataslot := range row {
		// create a display string for dataslot value

		switch dataslot := row_dataslot.(type) {
		case *VARBINARY:
			switch {
			case dataslot.data_error != nil:
				buff = append(buff[:0], "[ERR]"...)
			case dataslot.data_NULL_flag == true:
				buff = append(buff[:0], "␀"...)
			default:
				buff = append(buff[:0], "0x"...)
				buff = mbin.Append_hexastring(buff, dataslot.data_val)

				if len(buff) > width_limit {
					buff = buff[:width_limit]
					buff[width_limit-1] = '~'
				}
			}

			s = fmt.Sprintf("%-*s", width_limit, buff)

		case *VARCHAR:
			switch {
			case dataslot.data_error != nil:
				s = fmt.Sprintf("%-*.*s", width_limit, width_limit, "[ERR]")
			case dataslot.data_NULL_flag == true:
				s = fmt.Sprintf("%-*.*s", width_limit, width_limit, "␀")
			default:
				if len(dataslot.data_val) <= width_limit || utf8.RuneCount(dataslot.data_val) <= width_limit {
					s = fmt.Sprintf("%-*s", width_limit, dataslot.data_val)
				} else {
					s = string(dataslot.data_val)

					rune_count := 1
					for i, _ := range s {
						if rune_count >= width_limit {
							s = s[:i] + "~"
							break // break for
						}
						rune_count++
					}

					rsql.Assert(utf8.RuneCountInString(s) == width_limit)
				}
			}

		case *BIT:
			switch {
			case dataslot.data_error != nil:
				s = "E"
			case dataslot.data_NULL_flag == true:
				s = "␀"
			default:
				s = fmt.Sprintf("%1d", dataslot.data_val)
			}

		case *TINYINT:
			switch {
			case dataslot.data_error != nil:
				s = "ERR"
			case dataslot.data_NULL_flag == true:
				s = "  ␀"
			default:
				s = fmt.Sprintf("%3d", dataslot.data_val)
			}

		case *SMALLINT:
			switch {
			case dataslot.data_error != nil:
				s = " [ERR]"
			case dataslot.data_NULL_flag == true:
				s = "     ␀"
			default:
				s = fmt.Sprintf("%6d", dataslot.data_val)
			}

		case *INT:
			switch {
			case dataslot.data_error != nil:
				s = "      [ERR]"
			case dataslot.data_NULL_flag == true:
				s = "          ␀"
			default:
				s = fmt.Sprintf("%11d", dataslot.data_val)
			}

		case *BIGINT:
			switch {
			case dataslot.data_error != nil:
				s = "               [ERR]"
			case dataslot.data_NULL_flag == true:
				s = "                   ␀"
			default:
				s = fmt.Sprintf("%20d", dataslot.data_val)
			}

		case *MONEY:
			width := 2 + int(dataslot.Data_precision) // +2 for sign and decimal point

			switch {
			case dataslot.data_error != nil:
				buff = append(buff[:0], "[ERR]"...)
			case dataslot.data_NULL_flag == true:
				buff = append(buff[:0], "␀"...)
			default:
				buff = quad.AppendQuad(buff[:0], &dataslot.data_val)
			}

			s = fmt.Sprintf("%*s", width, buff)

		case *NUMERIC:
			width := 3 + int(dataslot.Data_precision) // +3 for sign, 0, decimal point

			switch {
			case dataslot.data_error != nil:
				buff = append(buff[:0], "[ERR]"...)
			case dataslot.data_NULL_flag == true:
				buff = append(buff[:0], "␀"...)
			default:
				buff = quad.AppendQuad(buff[:0], &dataslot.data_val)
			}

			s = fmt.Sprintf("%*s", width, buff)

		case *FLOAT:
			switch {
			case dataslot.data_error != nil:
				s = "     [ERR]"
			case dataslot.data_NULL_flag == true:
				s = "         ␀"
			default:
				s = fmt.Sprintf("%10.4g", dataslot.data_val) // you can change with if you want more digits
			}

		case *DATE:
			switch {
			case dataslot.data_error != nil:
				s = "     [ERR]"
			case dataslot.data_NULL_flag == true:
				s = "         ␀"
			default:
				s = dataslot.data_val.Time.Format("2006-01-02")
			}

		case *TIME:
			if option_ms {
				switch {
				case dataslot.data_error != nil:
					s = "       [ERR]"
				case dataslot.data_NULL_flag == true:
					s = "           ␀"
				default:
					s = dataslot.data_val.Time.Format("15:04:05.000") // you can add more 9 to display more ns
				}
			} else {
				switch {
				case dataslot.data_error != nil:
					s = "   [ERR]"
				case dataslot.data_NULL_flag == true:
					s = "       ␀"
				default:
					s = dataslot.data_val.Time.Format("15:04:05")
				}
			}

		case *DATETIME:
			if option_ms {
				switch {
				case dataslot.data_error != nil:
					s = "                  [ERR]"
				case dataslot.data_NULL_flag == true:
					s = "                      ␀"
				default:
					s = dataslot.data_val.Time.Format("2006-01-02 15:04:05.000") // you can add more 9 to display more ns
				}
			} else {
				switch {
				case dataslot.data_error != nil:
					s = "              [ERR]"
				case dataslot.data_NULL_flag == true:
					s = "                  ␀"
				default:
					s = dataslot.data_val.Time.Format("2006-01-02 15:04:05")
				}
			}

		default:
			panic("unexpected datatype")
		}

		// append s to result buffer

		result = append(result, s...)

		// append separator

		result = append(result, separator)
	}

	return string(result)
}
