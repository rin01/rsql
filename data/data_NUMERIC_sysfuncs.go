package data

import (
	"fmt"
	"math"
	"unicode/utf8"

	"rsql"
	"rsql/format"
	"rsql/quad"
)

func Sysfunc_abs_NUMERIC(r *NUMERIC, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Abs(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val)

}

func Sysfunc_ceiling_NUMERIC(r *NUMERIC, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Ceiling(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val)

}

func Sysfunc_floor_NUMERIC(r *NUMERIC, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Floor(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val)

}

func Sysfunc_sign_NUMERIC(r *NUMERIC, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Sign(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val)

}

func Sysfunc_power_NUMERIC(r *NUMERIC, a *NUMERIC, b *FLOAT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Power(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val, b.data_val)

}

// Sysfunc_round_NUMERIC rounds or truncate number to given precision.
//
//        ROUND ( numeric_expression, decimal precision [ ,truncate flag ] )
//
// The rounding mode is round-half-up, like MS SQL Server.
//
func Sysfunc_round_NUMERIC(r *NUMERIC, a *NUMERIC, b *INT, c *INT) {
	var (
		truncate_flag uint8
	)

	/* if error in a or b operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* is c is error, returns error */

	if c.data_error != nil {
		r.data_error = c.data_error
		return
	}

	// // If c is not NULL and != 0, truncate. Else, if c is NULL or 0, round.

	if c.data_NULL_flag == false && c.data_val != 0 { // if c is not NULL and != 0
		truncate_flag = 1
	}

	/* operation */

	r.data_error = quad.Round(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val, a.Data_precision, a.Data_scale, int32(b.data_val), truncate_flag)

}

// Sysfunc_format_NUMERIC converts NUMERIC to VARCHAR, as requested by the format string.
//
//        FORMAT ( numeric expression, format [ ,language ] )
//
// The format string is e.g. "#'0.00" or "#'##0.00" which will print the number with group separators, with two digits after the fractional point.
// The format string "0" will round the number to nearest integer value.
// The rounding mode is round-half-up, like MS SQL Server.
//
func Sysfunc_format_NUMERIC(r *VARCHAR, a *NUMERIC, b *VARCHAR, syslang *SYSLANGUAGE) {
	var (
		err  error
		res  []byte
		nfmt format.Numberfmt
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &syslang.Header) {
		return
	}

	/* operation */

	nfmt.Get_format_info(b.data_val) // 1st parse of format string, to analyze it

	nfmt.Fill_MyQuad(&a.data_val) // put string representation of number in nfmt

	nfmt.Set_langinfo(syslang.data_langinfo) // put locale information in nfmt

	if res, err = nfmt.Append_formatted_number(r.data_val[:0], b.data_val); err != nil { // 2nd parse of format string, to output result
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_FORMAT, rsql.ERROR_BATCH_ABORT, a.data_val.String(), b.data_val)
		return
	}

	// check result length

	if len(res) > int(r.Data_precision) && utf8.RuneCount(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_FORMAT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String(), b.data_val)
		return
	}

	r.data_val = res
}

func Sysfunc_isnumeric_NUMERIC(r *INT, a *NUMERIC) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = 0
	if a.data_NULL_flag == false {
		r.data_val = 1
	}
}

func Sysfunc_isnull_NUMERIC(r *NUMERIC, a *NUMERIC, b *NUMERIC) {

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		if r.data_error == nil && r.data_NULL_flag == false {
			r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &b.data_val)
		}
		return
	}

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	if r.data_error == nil && r.data_NULL_flag == false {
		r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val)
	}
}

func Sysfunc_iif_NUMERIC(r *NUMERIC, a *BOOLEAN, b *NUMERIC, c *NUMERIC) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		if r.data_error == nil && r.data_NULL_flag == false {
			r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &b.data_val)
		}
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	if r.data_error == nil && r.data_NULL_flag == false {
		r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &c.data_val)
	}
}

func Sysfunc_choose_NUMERIC(r *NUMERIC, a *INT, b_list []*NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	if r.data_error == nil && r.data_NULL_flag == false {
		r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &elem.data_val)
	}
}

func Sysfunc_coalesce_NUMERIC(r *NUMERIC, a_list []*NUMERIC) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			if r.data_error == nil && r.data_NULL_flag == false {
				r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &elem.data_val)
			}
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC, c *NUMERIC) {
	var (
		res_b int
		res_c int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	res_b = quad.Compare(&a.data_val, &b.data_val) // never fails
	res_c = quad.Compare(&a.data_val, &c.data_val) // never fails

	r.data_val = (res_b >= 0 && res_c <= 0)
}

func Sysfunc_not_between_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC, c *NUMERIC) {

	Sysfunc_between_NUMERIC(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_NUMERIC(r *VARCHAR, a *NUMERIC) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], fmt.Sprintf("NUMERIC(%d, %d)", a.Data_precision, a.Data_scale)...)
}

// Sysfunc_aggr_count_NUMERIC is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_NUMERIC(r *INT, a *NUMERIC) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val >= math.MaxInt32 { // check that r.data_val won't go beyond MaxInt32
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_count_big_NUMERIC is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_big_NUMERIC(r *BIGINT, a *NUMERIC) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val == math.MaxInt64 { // check that r.data_val won't go beyond MaxInt64
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_sum_NUMERIC is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_sum_NUMERIC(r *NUMERIC, a *NUMERIC) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	r.data_error = quad.Add(&r.data_val, r.Data_precision, r.Data_scale, &r.data_val, &a.data_val)
}

// Sysfunc_aggr_min_NUMERIC is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_min_NUMERIC(r *NUMERIC, a *NUMERIC) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if quad.Compare(&a.data_val, &r.data_val) < 0 { // never fails
		r.data_val = a.data_val
	}
}

// Sysfunc_aggr_max_NUMERIC is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_max_NUMERIC(r *NUMERIC, a *NUMERIC) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if quad.Compare(&a.data_val, &r.data_val) > 0 { // never fails
		r.data_val = a.data_val
	}
}
