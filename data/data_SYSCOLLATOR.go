package data

import (
	"fmt"

	"rsql"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *SYSCOLLATOR) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_SYSCOLLATOR)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += rsql.Joaat_hash_str(a.data_collation_string)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *SYSCOLLATOR) String() string {

	if a.data_error != nil {
		return "SYSCOLLATOR: error"
	}

	if a.data_NULL_flag == true {
		return "SYSCOLLATOR: null"
	}

	return fmt.Sprintf("SYSCOLLATOR: %s", a.data_collation_string)
}

// New_literal_SYSCOLLATOR creates a new data.SYSCOLLATOR from a string.
//
// THE collate.Collator OBJECT IS NOT CREATED IN THIS FUNCTION.
// It will be created lazily later, in the VM, to avoid creating useless collation objects.
//
func New_literal_SYSCOLLATOR(collation_string string) *SYSCOLLATOR {

	dataslot := &SYSCOLLATOR{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_SYSCOLLATOR,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},

		data_collation_string: collation_string,
		data_cached_collator:  nil,
	}

	return dataslot
}

// New_literal_SYSCOLLATOR_NO_LAZY creates a new data.SYSCOLLATOR from a string.
//
// It is the same as New_literal_SYSCOLLATOR(), but the collate.Collator object IS CREATED in this function.
//
func New_literal_SYSCOLLATOR_NO_LAZY(collation_string string) (*SYSCOLLATOR, *rsql.Error) {
	var (
		rsql_err *rsql.Error
	)

	dataslot := &SYSCOLLATOR{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_SYSCOLLATOR,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},

		data_collation_string: collation_string,
		data_cached_collator:  nil,
	}

	/* create collator */

	if dataslot.data_cached_collator, rsql_err = rsql.Get_collator(dataslot.data_collation_string); rsql_err != nil {
		return nil, rsql_err
	}

	return dataslot, nil
}
