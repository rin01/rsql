package data

func Sysfunc_typeof_BOOLEAN(r *VARCHAR, a *BOOLEAN) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "BOOLEAN"...)
}
