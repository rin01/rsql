package data

import (
	"fmt"
	"strings"

	"rsql"
	"rsql/lang"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *SYSLANGUAGE) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_SYSLANGUAGE)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += rsql.Joaat_hash_str(a.data_langinfo.Lng_locale_name)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(a.data_firstdayofweek)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(a.data_DMY[0])
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (syslang *SYSLANGUAGE) String() string {

	if syslang.data_error != nil {
		return "SYSLANGUAGE: error"
	}

	if syslang.data_NULL_flag == true {
		return "SYSLANGUAGE: null"
	}

	return fmt.Sprintf("SYSLANGUAGE: %s, %d, %s", syslang.data_langinfo.Lng_locale_name, syslang.data_firstdayofweek, syslang.data_DMY)
}

func (syslang *SYSLANGUAGE) Values() (language string, first_day_of_week int, DMY string) {

	if syslang.data_error != nil {
		return "error", 0, ""
	}

	if syslang.data_NULL_flag == true {
		return "null", 0, ""
	}

	return syslang.data_langinfo.Lng_locale_name, syslang.data_firstdayofweek, syslang.data_DMY
}

// Set_firstdayofweek is only used for test.
//
func (syslang *SYSLANGUAGE) Set_firstdayofweek(val int) {

	rsql.Assert(val >= 0 && val <= 6)

	syslang.data_firstdayofweek = val
}

// Set_DMY is only used for test.
//
func (syslang *SYSLANGUAGE) Set_DMY(val string) {

	rsql.Assert(val == "DMY" || val == "MDY" || val == "YMD")

	syslang.data_DMY = val
}

// New_SYSLANGUAGE_NULL creates a new data.SYSLANGUAGE initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_SYSLANGUAGE_NULL(kind rsql.Kind_t) *SYSLANGUAGE {

	dataslot := &SYSLANGUAGE{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_SYSLANGUAGE,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
	}

	return dataslot
}

// New_literal_SYSLANGUAGE_value creates a new data.SYSLANGUAGE from a value.
//
// Valid languages exist in lang.LANGUAGE_TO_TAG_MAP, else returns an error.
//
//     Valid languages can be of two forms:
//         - "French", "Spanish", "English", etc. This form is used by MS SQL Server, and allowed by rsql for compatibility with MS SQL Server.
//         - [fr-FR], [fr-CH], [en-US], etc. This is the preferred form, because some information can change for the same language depending on countries.
//
func New_literal_SYSLANGUAGE_value(val string) (*SYSLANGUAGE, *rsql.Error) {
	var (
		val_lc       string
		langinfo     *lang.Langinfo
		langinfo_key string
		ok           bool
	)

	dataslot := &SYSLANGUAGE{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_SYSLANGUAGE,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
	}

	val_lc = strings.ToLower(val)

	if langinfo_key, ok = lang.LANGUAGE_TO_TAG_MAP[val_lc]; ok == false {
		langinfo_key = val_lc
	}

	if langinfo, ok = lang.LANGINFO_MAP[langinfo_key]; ok == false {
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_INVALID_LOCALE, rsql.ERROR_BATCH_ABORT, val)
	}

	dataslot.data_langinfo = langinfo
	dataslot.data_firstdayofweek = langinfo.Lng_firstdayofweek
	dataslot.data_DMY = langinfo.Lng_DMY

	return dataslot, nil
}

// New_VARIABLE_SYSLANGUAGE_value creates a new data.SYSLANGUAGE, used when we create a new global variable with initial value.
//
// Exactly the same as New_literal_SYSLANGUAGE_value, except that the dataslot is KIND_VAR_LEAF.
//
func New_VARIABLE_SYSLANGUAGE_value(val string) (*SYSLANGUAGE, *rsql.Error) {
	var (
		dataslot *SYSLANGUAGE
		rsql_err *rsql.Error
	)

	if dataslot, rsql_err = New_literal_SYSLANGUAGE_value(val); rsql_err != nil {
		return nil, rsql_err
	}

	dataslot.Data_kind = rsql.KIND_VAR_LEAF

	return dataslot, nil
}

// Must_New_literal_SYSLANGUAGE_value creates a new data.SYSLANGUAGE from a value.
// Panics if error occurs.
//
func Must_New_literal_SYSLANGUAGE_value(val string) *SYSLANGUAGE {
	var (
		syslanguage *SYSLANGUAGE
		rsql_err    *rsql.Error
	)

	if syslanguage, rsql_err = New_literal_SYSLANGUAGE_value(val); rsql_err != nil {
		panic(rsql_err)
	}

	return syslanguage
}

func Assign_SYSLANGUAGE(r *SYSLANGUAGE, a *SYSLANGUAGE) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_langinfo = a.data_langinfo
	r.data_firstdayofweek = a.data_firstdayofweek
	r.data_DMY = a.data_DMY

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}
