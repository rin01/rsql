package data

import (
	"math"

	"rsql"
)

func Sysfunc_isnumeric_SMALLINT(r *INT, a *SMALLINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = 0
	if a.data_NULL_flag == false {
		r.data_val = 1
	}
}

func Sysfunc_isnull_SMALLINT(r *SMALLINT, a *SMALLINT, b *SMALLINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
	}
}

func Sysfunc_iif_SMALLINT(r *SMALLINT, a *BOOLEAN, b *SMALLINT, c *SMALLINT) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	r.data_val = c.data_val
}

func Sysfunc_choose_SMALLINT(r *SMALLINT, a *INT, b_list []*SMALLINT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	r.data_val = elem.data_val
}

func Sysfunc_coalesce_SMALLINT(r *SMALLINT, a_list []*SMALLINT) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = elem.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_SMALLINT(r *BOOLEAN, a *SMALLINT, b *SMALLINT, c *SMALLINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	r_val = (a.data_val >= b.data_val) && (a.data_val <= c.data_val)

	r.data_val = r_val
}

func Sysfunc_not_between_SMALLINT(r *BOOLEAN, a *SMALLINT, b *SMALLINT, c *SMALLINT) {

	Sysfunc_between_SMALLINT(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_SMALLINT(r *VARCHAR, a *SMALLINT) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "SMALLINT"...)
}

// Sysfunc_aggr_count_SMALLINT is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_SMALLINT(r *INT, a *SMALLINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val >= math.MaxInt32 { // check that r.data_val won't go beyond MaxInt32
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_count_big_SMALLINT is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_big_SMALLINT(r *BIGINT, a *SMALLINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val == math.MaxInt64 { // check that r.data_val won't go beyond MaxInt64
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_sum_SMALLINT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_sum_SMALLINT(r *BIGINT, a *SMALLINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	a_val = a.data_val
	b_val = r.data_val

	switch {
	case b_val > 0: // if b > 0, check that a+b won't go beyond MaxInt64
		if a_val > math.MaxInt64-b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}

	case b_val < 0: // if b < 0, check that a+b won't go below MinInt64
		if a_val < math.MinInt64-b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}
	}

	r_val = a_val + b_val

	r.data_val = r_val
}

// Sysfunc_aggr_min_SMALLINT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_min_SMALLINT(r *SMALLINT, a *SMALLINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val < r.data_val {
		r.data_val = a.data_val
	}
}

// Sysfunc_aggr_max_SMALLINT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_max_SMALLINT(r *SMALLINT, a *SMALLINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val > r.data_val {
		r.data_val = a.data_val
	}
}
