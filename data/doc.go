/*
Package data contains the structures of all SQL datatypes used by rsql.

These datatypes are VOID, SYSCOLLATOR, SYSLANGUAGE, BOOLEAN, VARBINARY, VARCHAR, REGEXPLIKE, BIT, TINYINT, SMALLINT, INT, BIGINT, MONEY, NUMERIC, FLOAT, DATE, TIME, DATETIME.
    - VOID are created only by untyped NULL literals in a SQL expression, e.g. INSERT INTO T VALUES(NULL, 'hello').
    - SYSCOLLATOR contain collation data, used for VARCHAR operations, like sorting or searching.
    - SYSLANGUAGE contain language data, used for DATETIME operations (localized day of week or month name). Also used for number formatting (thousand separator and decimal point symbols).
    - BOOLEAN are created only as result of a boolean expression, e.g. SELECT ... WHERE NAME = 'fred'
    - REGEXPLIKE are used only by the LIKE operator.
    - you cannot explicitely DECLARE variables of type VOID, SYSCOLLATOR, SYSLANGUAGE, BOOLEAN, REGEXPLIKE.

In rsql source code, these datatypes are structs with names like Data_INT, Data_DATETIME, etc. They are called "dataslots".

    Several fields are set only at the parse stage (during decoration or the AST tree), and are not modified during the execution by the VM.
    They describe the properties of the dataslot:
        - Data_datatype
        - Data_kind
        - Data_precision
        - Data_scale
        - Data_fixlen_flag

    Other fields contain dataslot's value, and are updated during execution by the VM:
        - data_error
        - data_NULL_flag
        - data_val

    SQL operators and builtin functions take dataslots as arguments.
    As an example, let's have the expression "price + 100.50".
    The virual machine will call the function Add_NUMERIC(r *NUMERIC, a *NUMERIC, b *NUMERIC), with 'a' pointing to the NUMERIC that stores 'price' column value, and 'b' pointing to the NUMERIC storing 100.50 value.
    The result will be stored in 'r'.
    When an error occurs during the operation (overflow, division by 0, impossible conversion, etc), the field r.data_error is filled with the error.
        If an operator is NULL, the result is often NULL, which sets the field r.data_NULL_flag to true.
        Else, r.data_error is set to nil, r.data_NULL_flag is set to false, and the result is written in r.data_val.

*/
package data
