package data

import (
	"fmt"
	"math"

	"rsql"
	"rsql/quad"
)

//======================================================================================================
// NOTE: ALL ERRORS ARE "NUMERIC" ERRORS, E.G. ERROR_SQLDATA_NUMERIC_CAST_VARCHAR_OVERFLOW
//       This is because most operations and functions for MONEY are performed by NUMERIC functions.
//======================================================================================================

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *MONEY) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_MONEY)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += quad.Hash(&a.data_val)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *MONEY) String() string {

	if a.data_error != nil {
		return "MONEY: error"
	}

	if a.data_NULL_flag == true {
		return "MONEY: null"
	}

	return fmt.Sprintf("MONEY: %s", a.data_val.String())
}

// New_MONEY_NULL creates a new data.MONEY initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_MONEY_NULL(kind rsql.Kind_t) *MONEY {

	dataslot := &MONEY{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_MONEY,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		Data_precision: rsql.DATATYPE_PRECISION_MONEY,
		Data_scale:     rsql.DATATYPE_SCALE_MONEY,
	}

	quad.Zero(&dataslot.data_val, rsql.DATATYPE_PRECISION_MONEY, rsql.DATATYPE_SCALE_MONEY)

	return dataslot
}

// New_literal_MONEY creates a new data.MONEY, converting string argument to number.
// Returns an error if invalid string, or if result is Inf or Nan.
//
func New_literal_MONEY(s string) (*MONEY, *rsql.Error) {

	dataslot := &MONEY{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_MONEY,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		Data_precision: rsql.DATATYPE_PRECISION_MONEY,
		Data_scale:     rsql.DATATYPE_SCALE_MONEY,
	}

	rsql_err := quad.From_string(&dataslot.data_val, rsql.DATATYPE_PRECISION_MONEY, rsql.DATATYPE_SCALE_MONEY, s) // returns an error if invalid string, or if result is Inf or Nan.
	if rsql_err != nil {
		return nil, rsql_err
	}

	return dataslot, nil
}

func Is_null_MONEY(r *BOOLEAN, a *MONEY) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_MONEY(r *BOOLEAN, a *MONEY) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_MONEY checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_MONEY(r *BOOLEAN, a *MONEY, b_list []*MONEY) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_NUMERIC(r, (*NUMERIC)(a), (*NUMERIC)(b))

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_MONEY(r *BOOLEAN, a *MONEY, b_list []*MONEY) {

	In_list_MONEY(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_MONEY(r *MONEY, a_list []Case_element_t) {
	var (
		elem_val *MONEY
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*MONEY)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			r.data_val = elem_val.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

// Cast_MONEY_to_VARCHAR casts MONEY to VARCHAR.
// If precision of target is unsufficient, returns an error.
//
// Exactly the same as Cast_NUMERIC_to_VARCHAR(), but rounds to 2 digits after fractional point.
//
func Cast_MONEY_to_VARCHAR(r *VARCHAR, a *MONEY) {
	var (
		q quad.MyQuad
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if rsql_err := quad.Copy(&q, rsql.DATATYPE_PRECISION_MONEY, 2, &a.data_val); rsql_err != nil { // quantize with just 2 decimals (note: rsql.DATATYPE_SCALE_MONEY==4)
		r.data_error = rsql_err
		return
	}

	res := quad.AppendQuad(r.data_val[:0], &q)

	if len(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, res, r.Data_precision)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

// Cast_MONEY_to_TINYINT casts MONEY to TINYINT
// Money values are rounded when cast to any integer type.
//
// Exactly the same as Cast_NUMERIC_to_TINYINT(), except the rounding mode.
//
func Cast_MONEY_to_TINYINT(r *TINYINT, a *MONEY) {
	var (
		rsql_err *rsql.Error
		val      int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int32_round(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	if val > math.MaxUint8 || val < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		return
	}

	r.data_val = int64(val)
}

// Cast_MONEY_to_SMALLINT casts MONEY to SMALLINT
// Money values are rounded when cast to any integer type.
//
// Exactly the same as Cast_NUMERIC_to_SMALLINT(), except the rounding mode.
//
func Cast_MONEY_to_SMALLINT(r *SMALLINT, a *MONEY) {
	var (
		rsql_err *rsql.Error
		val      int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int32_round(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	if val > math.MaxInt16 || val < math.MinInt16 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		return
	}

	r.data_val = int64(val)
}

// Cast_MONEY_to_INT casts MONEY to INT
// Money values are rounded when cast to any integer type.
//
// Exactly the same as Cast_NUMERIC_to_INT(), except the rounding mode.
//
func Cast_MONEY_to_INT(r *INT, a *MONEY) {
	var (
		rsql_err *rsql.Error
		val      int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int32_round(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	r.data_val = int64(val)
}

// Cast_MONEY_to_BIGINT casts MONEY to BIGINT
// Money values are rounded when cast to any integer type.
//
// Exactly the same as Cast_NUMERIC_to_BIGINT(), except the rounding mode.
//
func Cast_MONEY_to_BIGINT(r *BIGINT, a *MONEY) {
	var (
		rsql_err *rsql.Error
		val      int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int64_round(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	r.data_val = val
}

func Cast_MONEY_to_MONEY(r *MONEY, a *MONEY) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Assign_MONEY(r *MONEY, a *MONEY) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val // we don't need decQuadCopy()

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

func Copy_MONEY(r *MONEY, a *MONEY) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_MONEY(kind rsql.Kind_t, a *MONEY) *MONEY {

	return New_MONEY_NULL(kind)
}
