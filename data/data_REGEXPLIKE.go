package data

import (
	"unicode/utf8"

	"rsql"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *REGEXPLIKE) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_REGEXPLIKE)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += a.data_val.Hash()
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *REGEXPLIKE) String() string {

	if a.data_error != nil {
		return "REGEXPLIKE: error"
	}

	if a.data_NULL_flag == true {
		return "REGEXPLIKE: null"
	}

	return "REGEXPLIKE: ..."
}

// New_REGEXPLIKE_NULL creates a new data.REGEXPLIKE initialized to NULL.
//
// In the AST tree, data.REGEXPLIKE inherits the collation from the VARCHAR it has been cast from.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_REGEXPLIKE_NULL(kind rsql.Kind_t) *REGEXPLIKE {

	dataslot := &REGEXPLIKE{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_REGEXPLIKE,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
	}

	return dataslot
}

// New_literal_REGEXPLIKE_value creates a new data.REGEXPLIKE from a value.
// Returns an error if string too long or invalid.
// val is a LIKE expression, e.g. 'art[_][a-dz][0-9]%'
// ec argument is the escape character. Pass like.NO_ESCAPE_RUNE to disable it.
//
func New_literal_REGEXPLIKE_value(val string, ec rune) (*REGEXPLIKE, *rsql.Error) {

	runecount := utf8.RuneCountInString(val)

	if runecount > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
	}

	dataslot := &REGEXPLIKE{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_REGEXPLIKE,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
	}

	if err := dataslot.data_val.Parse_likexp(val, ec); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
	}

	return dataslot, nil
}
