package data

import (
	"sync"

	"rsql"
	"rsql/mdat"
	"rsql/quad"

	"rsql/msgp"
)

//==========================================================================================
//           global pool of []byte buffers used by append_msgp for MONEY and NUMERIC
//==========================================================================================

// used by append_msgp for MONEY and NUMERIC is transient, only used inside the function

const APPEND_MSGP_SCRATCH_BUFFER_CAPACITY = 50 // good enough to store MONEY or NUMERIC values

var g_pool_for_append_msgp = sync.Pool{
	New: func() interface{} {
		return make([]byte, APPEND_MSGP_SCRATCH_BUFFER_CAPACITY)
	},
}

//==========================================================================================
//                              dataslots to messagepack
//==========================================================================================

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *VOID) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	dest = msgp.AppendNil(dest)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *BOOLEAN) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendBool(dest, a.data_val)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *VARBINARY) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendBytes(dest, a.data_val)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *VARCHAR) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendStringFromBytes(dest, a.data_val)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *BIT) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendUint8(dest, uint8(a.data_val))

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *TINYINT) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendUint8(dest, uint8(a.data_val))

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *SMALLINT) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendInt64(dest, a.data_val)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *INT) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendInt64(dest, a.data_val)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *BIGINT) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendInt64(dest, a.data_val)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *MONEY) append_msgp(dest []byte) []byte {
	var (
		scratch []byte
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	scratch = g_pool_for_append_msgp.Get().([]byte) // get []byte from pool

	scratch = quad.AppendQuad((scratch)[:0], &a.data_val)

	dest = msgp.AppendStringFromBytes(dest, scratch)

	g_pool_for_append_msgp.Put(scratch) // put back []byte to the pool

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *NUMERIC) append_msgp(dest []byte) []byte {
	var (
		scratch []byte
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	scratch = g_pool_for_append_msgp.Get().([]byte) // get []byte from pool

	scratch = quad.AppendQuad((scratch)[:0], &a.data_val)

	dest = msgp.AppendStringFromBytes(dest, scratch)

	g_pool_for_append_msgp.Put(scratch) // put back []byte to the pool

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *FLOAT) append_msgp(dest []byte) []byte {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	dest = msgp.AppendFloat64(dest, a.data_val)

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *DATE) append_msgp(dest []byte) []byte {
	var (
		unix_seconds int64
		val_sec      int64
		val_days     int64
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	// note: struct time.Time has fields
	//          sec   int64
	//          nsec  int32     [0, 999999999]

	unix_seconds = a.data_val.Unix()

	val_sec = unix_seconds - mdat.UNIX_SEC_LOWEST // seconds since 0001.01.01

	val_days = val_sec / mdat.SECONDS_PER_DAY // days since 0001.01.01

	rsql.Assert(val_days >= 0 && val_days < mdat.NUMBER_OF_DAYS_FROM_LOWEST_TO_UPPER_SENTINEL) // val_days is [0 ... 3652058], which fits in uint32 range

	// serialize

	dest = msgp.AppendUint32(dest, uint32(val_days))

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *TIME) append_msgp(dest []byte) []byte {
	var (
		unix_seconds  int64
		val_sec       int64
		delta_seconds int64
		delta_ns      int
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	// note: struct time.Time has fields
	//          sec   int64
	//          nsec  int32     [0, 999999999]

	unix_seconds = a.data_val.Unix()

	val_sec = unix_seconds - mdat.UNIX_SEC_1900_01_01 // seconds since 1900.01.01

	// delta seconds within one day

	rsql.Assert(val_sec < mdat.SECONDS_PER_DAY)

	delta_seconds = val_sec // [0 ... 86399], which fits in uint32 range

	// delta nanoseconds within one second

	delta_ns = a.data_val.Nanosecond() // [0 ... 999999999], which fits in uint32 range

	// serialize

	dest = msgp.AppendArrayHeader(dest, 2) // serialized as <seconds, ns>

	dest = msgp.AppendUint32(dest, uint32(delta_seconds))
	dest = msgp.AppendUint32(dest, uint32(delta_ns))

	return dest
}

// append_msgp appends the value of the dataslot, encoded as messagepack.
//
//       THE DATASLOT SHOULD NOT CONTAIN AN ERROR !
//
func (a *DATETIME) append_msgp(dest []byte) []byte {
	var (
		unix_seconds  int64
		val_sec       int64
		val_days      int64
		delta_seconds int64
		delta_ns      int
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		dest = msgp.AppendNil(dest)
		return dest
	}

	// note: struct time.Time has fields
	//          sec   int64
	//          nsec  int32     [0, 999999999]

	unix_seconds = a.data_val.Unix()

	val_sec = unix_seconds - mdat.UNIX_SEC_LOWEST // seconds since 0001.01.01

	val_days = val_sec / mdat.SECONDS_PER_DAY // days since 0001.01.01

	rsql.Assert(val_days >= 0 && val_days < mdat.NUMBER_OF_DAYS_FROM_LOWEST_TO_UPPER_SENTINEL) // val_days is [0 ... 3652058], which fits in uint32 range

	// delta seconds within one day

	delta_seconds = val_sec - val_days*mdat.SECONDS_PER_DAY // [0 ... 86399], which fits in uint32 range

	// delta nanoseconds within one second

	delta_ns = a.data_val.Nanosecond() // [0 ... 999999999], which fits in uint32 range

	// serialize

	dest = msgp.AppendArrayHeader(dest, 3) // serialized as <number of days, seconds, ns>

	dest = msgp.AppendUint32(dest, uint32(val_days))
	dest = msgp.AppendUint32(dest, uint32(delta_seconds))
	dest = msgp.AppendUint32(dest, uint32(delta_ns))

	return dest
}

// Write_dataslot_msgp is the main function that writes dataslot as messagepack into msgp.Writer.
//
func Write_dataslot_msgp(mw *msgp.Writer, dataslot rsql.IDataslot) {
	var (
		dest []byte
	)

	dest = mw.TruncatedStaging()

	switch dataslot.Datatype() {
	case rsql.DATATYPE_VOID:
		dest = dataslot.(*VOID).append_msgp(dest)

	case rsql.DATATYPE_BOOLEAN:
		dest = dataslot.(*BOOLEAN).append_msgp(dest)

	case rsql.DATATYPE_VARBINARY:
		dest = dataslot.(*VARBINARY).append_msgp(dest)

	case rsql.DATATYPE_VARCHAR:
		dest = dataslot.(*VARCHAR).append_msgp(dest)

	case rsql.DATATYPE_BIT:
		dest = dataslot.(*BIT).append_msgp(dest)

	case rsql.DATATYPE_TINYINT:
		dest = dataslot.(*TINYINT).append_msgp(dest)

	case rsql.DATATYPE_SMALLINT:
		dest = dataslot.(*SMALLINT).append_msgp(dest)

	case rsql.DATATYPE_INT:
		dest = dataslot.(*INT).append_msgp(dest)

	case rsql.DATATYPE_BIGINT:
		dest = dataslot.(*BIGINT).append_msgp(dest)

	case rsql.DATATYPE_MONEY:
		dest = dataslot.(*MONEY).append_msgp(dest)

	case rsql.DATATYPE_NUMERIC:
		dest = dataslot.(*NUMERIC).append_msgp(dest)

	case rsql.DATATYPE_FLOAT:
		dest = dataslot.(*FLOAT).append_msgp(dest)

	case rsql.DATATYPE_DATE:
		dest = dataslot.(*DATE).append_msgp(dest)

	case rsql.DATATYPE_TIME:
		dest = dataslot.(*TIME).append_msgp(dest)

	case rsql.DATATYPE_DATETIME:
		dest = dataslot.(*DATETIME).append_msgp(dest)

	default:
		panic("impossible")
	}

	mw.SetStaging(dest)

	mw.WriteStaging()
}

//==========================================================================================
//                              datatypes to messagepack
//==========================================================================================

// These datatype codes are only used when rsql sends data to the client.
// They are never used for internal use by rsql server. They only have a meaning for the client, when it must decode the received data.
//
//            NEVER CHANGE THESE VALUES, AS THEY ARE PART OF THE API, AS SEEN BY THE CLIENT
//
const (
	MSGP_DATATYPE_VOID      = 1
	MSGP_DATATYPE_BOOLEAN   = 2
	MSGP_DATATYPE_VARBINARY = 4
	MSGP_DATATYPE_VARCHAR   = 6

	MSGP_DATATYPE_BIT      = 9
	MSGP_DATATYPE_TINYINT  = 10
	MSGP_DATATYPE_SMALLINT = 11
	MSGP_DATATYPE_INT      = 12
	MSGP_DATATYPE_BIGINT   = 13

	MSGP_DATATYPE_MONEY   = 15
	MSGP_DATATYPE_NUMERIC = 16
	MSGP_DATATYPE_FLOAT   = 17

	MSGP_DATATYPE_DATE     = 19
	MSGP_DATATYPE_TIME     = 20
	MSGP_DATATYPE_DATETIME = 21
)

// Write_datatype_msgp is the main function that writes datatype as messagepack into msgp.Writer.
//
func Write_datatype_msgp(mw *msgp.Writer, dataslot rsql.IDataslot) {
	var (
		dest []byte
	)

	dest = mw.TruncatedStaging()

	switch dataslot.Datatype() {
	case rsql.DATATYPE_VOID:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_VOID)

	case rsql.DATATYPE_BOOLEAN:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_BOOLEAN)

	case rsql.DATATYPE_VARBINARY:
		dest = msgp.AppendArrayHeader(dest, 2) // serialized as <datatype, precision>
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_VARBINARY)
		dest = msgp.AppendUint16(dest, dataslot.(*VARBINARY).Data_precision)

	case rsql.DATATYPE_VARCHAR:
		dest = msgp.AppendArrayHeader(dest, 3) // serialized as <datatype, precision, fixlen_flag>
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_VARCHAR)
		dest = msgp.AppendUint16(dest, dataslot.(*VARCHAR).Data_precision)
		dest = msgp.AppendBool(dest, dataslot.(*VARCHAR).Data_fixlen_flag)

	case rsql.DATATYPE_BIT:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_BIT)

	case rsql.DATATYPE_TINYINT:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_TINYINT)

	case rsql.DATATYPE_SMALLINT:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_SMALLINT)

	case rsql.DATATYPE_INT:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_INT)

	case rsql.DATATYPE_BIGINT:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_BIGINT)

	case rsql.DATATYPE_MONEY:
		dest = msgp.AppendArrayHeader(dest, 3) // serialized as <datatype, precision, scale>
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_MONEY)
		dest = msgp.AppendUint16(dest, dataslot.(*MONEY).Data_precision)
		dest = msgp.AppendUint16(dest, dataslot.(*MONEY).Data_scale)

	case rsql.DATATYPE_NUMERIC:
		dest = msgp.AppendArrayHeader(dest, 3) // serialized as <datatype, precision, scale>
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_NUMERIC)
		dest = msgp.AppendUint16(dest, dataslot.(*NUMERIC).Data_precision)
		dest = msgp.AppendUint16(dest, dataslot.(*NUMERIC).Data_scale)

	case rsql.DATATYPE_FLOAT:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_FLOAT)

	case rsql.DATATYPE_DATE:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_DATE)

	case rsql.DATATYPE_TIME:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_TIME)

	case rsql.DATATYPE_DATETIME:
		dest = msgp.AppendArrayHeader(dest, 1)
		dest = msgp.AppendUint8(dest, MSGP_DATATYPE_DATETIME)

	default:
		panic("impossible")
	}

	mw.SetStaging(dest)

	mw.WriteStaging()
}
