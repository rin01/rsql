package data

import (
	"strings"

	"rsql"
	"rsql/dict"
)

func Sysfunc_suser_name_VARCHAR(r *VARCHAR, a *BIGINT) {
	var (
		login_name string
		ok         bool
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if login_name, ok = dict.MASTER.Get_syslogin_name(a.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = append(r.data_val[:0], login_name...)
}

func Sysfunc_suser_id_BIGINT(r *BIGINT, a *VARCHAR) {
	var (
		login_id int64
		ok       bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if login_id, ok = dict.MASTER.Get_syslogin_id(a.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = login_id
}

func Sysfunc_db_name_VARCHAR(r *VARCHAR, a *BIGINT) {
	var (
		database_name string
		ok            bool
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if database_name, ok = dict.MASTER.Get_sysdatabase_name(a.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = append(r.data_val[:0], database_name...)
}

func Sysfunc_db_id_BIGINT(r *BIGINT, a *VARCHAR) {
	var (
		database_id int64
		ok          bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if database_id, ok = dict.MASTER.Get_sysdatabase_id(a.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = database_id
}

func Sysfunc_schema_name_VARCHAR(r *VARCHAR, a *BIGINT, b *BIGINT) {
	var (
		schema_name string
		ok          bool
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if schema_name, ok = dict.MASTER.Get_sysschema_name(a.data_val, b.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = append(r.data_val[:0], schema_name...)
}

func Sysfunc_schema_id_BIGINT(r *BIGINT, a *BIGINT, b *VARCHAR) {
	var (
		schema_id int64
		ok        bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if schema_id, ok = dict.MASTER.Get_sysschema_id(a.data_val, b.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = schema_id
}

func Sysfunc_principal_name_VARCHAR(r *VARCHAR, a *BIGINT, b *BIGINT) {
	var (
		user_name string
		ok        bool
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if user_name, ok = dict.MASTER.Get_sysprincipal_name(a.data_val, b.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = append(r.data_val[:0], user_name...)
}

func Sysfunc_principal_id_BIGINT(r *BIGINT, a *BIGINT, b *VARCHAR) {
	var (
		user_id int64
		ok      bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if user_id, ok = dict.MASTER.Get_sysprincipal_id(a.data_val, b.data_val); ok == false {
		r.data_NULL_flag = true
		return
	}

	r.data_val = user_id
}

func Sysfunc_object_id_BIGINT(r *BIGINT, a *VARCHAR, b *VARCHAR, c *VARCHAR, d *VARCHAR) {
	var (
		rsql_err  *rsql.Error
		object_id int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header, &d.Header) {
		return
	}

	/* operation */

	// get object qualified name

	parts_string := strings.ToLower(string(c.data_val))
	list_parts := strings.Split(parts_string, ".")

	for i, s := range list_parts {
		ss := strings.TrimSpace(s)
		if len(ss) >= 2 {
			if ss[0] == '[' && ss[len(ss)-1] == ']' {
				ss = ss[1 : len(ss)-1]
			}
		}

		list_parts[i] = ss
	}

	if string(d.data_val) != "U" {
		r.data_NULL_flag = true
		return
	}

	switch len(list_parts) {
	case 3:
		// pass

	case 2:
		list_parts = append(list_parts[:0], []string{string(a.data_val), list_parts[0], list_parts[1]}...)

	case 1:
		list_parts = append(list_parts[:0], []string{string(a.data_val), string(b.data_val), list_parts[0]}...)

	default:
		r.data_NULL_flag = true
		return
	}

	if list_parts[0] == "" {
		list_parts[0] = string(a.data_val)
	}

	if list_parts[1] == "" {
		list_parts[1] = string(b.data_val)
	}

	if list_parts[2] == "" {
		r.data_NULL_flag = true
		return
	}

	if object_id, rsql_err = dict.MASTER.Get_gtblid(rsql.Object_qname_t{Database_name: list_parts[0], Schema_name: list_parts[1], Object_name: list_parts[2], Object_name_original: list_parts[2]}); rsql_err != nil {
		r.data_NULL_flag = true
		return
	}

	r.data_val = object_id
}
