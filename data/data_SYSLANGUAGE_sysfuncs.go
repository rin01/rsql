package data

import (
	"strings"

	"rsql"
)

//  Sysfunc_atatdatefirst_SYSLANGUAGE implements the SQL function @@DATEFIRST.
//
func Sysfunc_atatdatefirst_SYSLANGUAGE(r *INT, a *SYSLANGUAGE) {

	/* if error or NULL in operand, returns error */

	if r.Header.process_error_and_NULL(&a.Header) {
		if r.data_error == nil {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_CANNOT_BE_NULL, rsql.ERROR_BATCH_ABORT)
		}
		return
	}

	/* operation */

	r.data_val = int64(a.data_firstdayofweek)

	if r.data_val == 0 {
		r.data_val = 7 // 0 is replaced by 7
	}
}

//  Sysfunc_atatdateformat_SYSLANGUAGE implements the SQL function @@DATEFORMAT.
//
func Sysfunc_atatdateformat_SYSLANGUAGE(r *VARCHAR, a *SYSLANGUAGE) {

	/* if error or NULL in operand, returns error */

	if r.Header.process_error_and_NULL(&a.Header) {
		if r.data_error == nil {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_CANNOT_BE_NULL, rsql.ERROR_BATCH_ABORT)
		}
		return
	}

	/* operation */

	r.data_val = append(r.data_val[:0], a.data_DMY...)
}

// Sysfunc_atatlanguage_SYSLANGUAGE implements the SQL function @@LANGUAGE.
//
func Sysfunc_atatlanguage_SYSLANGUAGE(r *VARCHAR, a *SYSLANGUAGE) {

	/* if error or NULL in operand, returns error */

	if r.Header.process_error_and_NULL(&a.Header) {
		if r.data_error == nil {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_CANNOT_BE_NULL, rsql.ERROR_BATCH_ABORT)
		}
		return
	}

	/* operation */

	res := append(r.data_val[:0], a.data_langinfo.Lng_locale_name...)

	if len(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_TO_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, res, r.Data_precision)
		return
	}

	r.data_val = res
}

// Sysfunc_firstdayofweek_SYSLANGUAGE implements SQL statement SET DATEFIRST.
//
func Sysfunc_firstdayofweek_SYSLANGUAGE(r *SYSLANGUAGE, a *SYSLANGUAGE, b *INT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		if r.data_error == nil {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_FIRSTDAYOFWEEK_FAILURE, rsql.ERROR_BATCH_ABORT)
		}
		return
	}

	/* operation */

	if b.data_val < 1 || b.data_val > 7 { // b is in base 1
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_FIRSTDAYOFWEEK_BAD_VALUE, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	firstdayofweek := int(b.data_val) // 7 for MS SQL Server is replaced by 0 for us
	if firstdayofweek == 7 {
		firstdayofweek = 0
	}

	r.data_langinfo = a.data_langinfo
	r.data_firstdayofweek = firstdayofweek
	r.data_DMY = a.data_DMY
}

// Sysfunc_dmy_SYSLANGUAGE implements SQL statement SET DATEFORMAT.
//
func Sysfunc_dmy_SYSLANGUAGE(r *SYSLANGUAGE, a *SYSLANGUAGE, b *VARCHAR) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		if r.data_error == nil {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_DMY_FAILURE, rsql.ERROR_BATCH_ABORT)
		}
		return
	}

	/* operation */

	if len(b.data_val) != 3 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_DMY_BAD_VALUE, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	dmy_value := strings.ToUpper(string(b.data_val))

	if dmy_value != "DMY" && dmy_value != "MDY" && dmy_value != "YMD" {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_DMY_BAD_VALUE, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	r.data_langinfo = a.data_langinfo
	r.data_firstdayofweek = a.data_firstdayofweek
	r.data_DMY = dmy_value
}
