package data

import (
	"math"
	"math/rand"
	"strconv"
	"unicode/utf8"

	"rsql"
	"rsql/format"
)

func Sysfunc_abs_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Abs(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_ceiling_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Ceil(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_floor_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Floor(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_sign_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	switch {
	case math.IsNaN(a_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(a_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	switch {
	case a_val > 0.:
		r_val = 1.
	case a_val < 0.:
		r_val = -1.
	default:
		r_val = 0.
	}

	r.data_val = r_val
}

func Sysfunc_power_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = math.Pow(a_val, b_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

// Sysfunc_round_FLOAT rounds or truncate number to given precision.
//
//        ROUND ( numeric_expression, decimal precision [ ,truncate flag ] )
//
// Rounding is half-up, like MS SQL Server.
// NOTE: STR and FORMAT functions round half-even, because they use strconv.AppendFloat which rounds his way.
//
func Sysfunc_round_FLOAT(r *FLOAT, a *FLOAT, b *INT, c *INT) {
	var (
		a_val float64
		b_val int64
		r_val float64

		truncate_flag bool
		divisor       float64
		radical_a_val float64
	)

	/* if error in a or b operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* is c is error, returns error */

	if c.data_error != nil {
		r.data_error = c.data_error
		return
	}

	// If c is not NULL and != 0, truncate. Else, if c is NULL or 0, round.

	if c.data_NULL_flag == false && c.data_val != 0 { // if c is not NULL and != 0
		truncate_flag = true
	}

	/* operation */

	a_val = a.data_val

	switch {
	case math.IsNaN(a_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(a_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	switch {
	case b.data_val == 0: /* if rounding length == 0, just truncate or round the operand */
		if truncate_flag == false { // if round
			if a_val >= 0. {
				a_val += 0.5
			} else {
				a_val += -0.5
			}
		}
		r_val = math.Trunc(a_val)

	default: /* else, make the proper rounding or truncation */
		b_val = -b.data_val // there is no limit to the rounding length for FLOAT.    Note: if b.data_val == MinInt32, then b_val == MaxInt32+1, but it is ok if int is 64 bits, because of the line below.

		divisor = math.Pow10(int(b_val)) // b_val can be a positive or negative number, so divisor will be      +Inf, ... 1000, 100, 10, 1, 0.1, 0.01, 0.001 ... 0.0

		radical_a_val = a_val / divisor // radical_a_val may be 0 or +Inf

		switch {
		case truncate_flag == true:
			r_val = math.Trunc(radical_a_val) * divisor // may be Nan

		default:
			if radical_a_val >= 0. {
				radical_a_val += 0.5
			} else {
				radical_a_val += -0.5
			}
			r_val = math.Trunc(radical_a_val) * divisor // may be Nan
		}
	}

	/* check result */

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_acos_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Acos(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_asin_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Asin(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_atan_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Atan(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_atn2_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = math.Atan2(a_val, b_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_cos_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Cos(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_sin_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Sin(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_tan_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Tan(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_cot_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	tan := math.Tan(a_val)

	if tan == 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = 1. / tan

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_exp_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Exp(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_log_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Log(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_log10_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Log10(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_sqrt_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = math.Sqrt(a_val)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_square_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = a_val * a_val

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_pi_FLOAT(r *FLOAT) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = math.Pi
}

func Sysfunc_degrees_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = a_val * (180.0 / math.Pi)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_radians_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = a_val * (math.Pi / 180.0)

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Sysfunc_rand_FLOAT(r *FLOAT) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = rand.Float64() // multiple goroutine access the same shared source for random number
}

// Sysfunc_str_FLOAT converts FLOAT to VARCHAR, with specified length and decimal number.
//
//        STR ( float expression [ ,length [ ,decimal ] ] )
//
//        NOTE: The rounding is half-to-even (21.5->22, 22.5->22, 23.5->24). It is unlike MS SQL Server, which rounds half-up (21.5->22, 22.5->23, 23.5->24)
//
func Sysfunc_str_FLOAT(r *VARCHAR, a *FLOAT, length *INT, decimal *INT) {

	const DEFAULT_BUFF_CAPACITY = 60

	var (
		length_val      int
		decimal_val     int
		len_buff        int
		buff            = make([]byte, DEFAULT_BUFF_CAPACITY) // plain number comes here, without leading blanks
		second_try_flag bool
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is variable length

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &length.Header) {
		return
	}

	switch {
	case decimal.data_error != nil:
		r.data_error = decimal.data_error
		return

	case decimal.data_NULL_flag == true:
		decimal_val = 0

	default:
		decimal_val = int(decimal.data_val)
		if decimal_val < 0 {
			r.data_NULL_flag = true
			return
		}
		if decimal_val > 16 {
			decimal_val = 16
		}
	}

	switch {
	case length.data_val <= 0:
		r.data_NULL_flag = true
		return

	default:
		length_val = int(length.data_val)
	}

	/* check that target precision is enough for result with length_val */

	if length_val > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, length_val, r.Data_precision)
		return
	}

	/* enlarge the target buffer if necessary, so that target capacity >= length_val */

	if cap(r.data_val) < length_val {
		r.data_val = make([]byte, length_val)
	}

	/* operation */

LABEL_OPERATION:

	buff = strconv.AppendFloat(buff[:0], a.data_val, 'f', decimal_val, 64) // round-half-to-even (unlike MS SQL Server, which rounds round-half-up)
	len_buff = len(buff)

	// if requested length is larger than the content of buff, write it into target

	if len_buff <= length_val {
		r.data_val = r.data_val[:length_val]

		for i := 0; i < length_val-len_buff; i++ {
			r.data_val[i] = ' '
		}
		copy(r.data_val[length_val-len_buff:length_val], buff)
		return
	}

	if second_try_flag == true { // the second try should always succeed. I don't know why it would fail, but just in case, I put this goto here.
		goto LABEL_OPERATION_FAILED
	}

	// content of buff is larger than requested length. Try to reduce decimal, to get a smaller result.

	if decimal_val == 0 { // if already decimal == 0, there is no position to gain. Operation fails.
		goto LABEL_OPERATION_FAILED
	}

	decimal_val = decimal_val - (len_buff - length_val) // reduce decimal of the amount of positions to gain.

	if decimal_val == -1 { // if just one more position is still needed, decimal=0 gains it as the fractional dot is not printed.
		decimal_val = 0
	}

	if decimal_val < 0 {
		goto LABEL_OPERATION_FAILED
	}

	second_try_flag = true
	goto LABEL_OPERATION // second try with decimal reduced, which must work

	// --------------------------------

LABEL_OPERATION_FAILED:

	// if operation failed, write '*' into the target.

	r.data_val = r.data_val[:length_val]

	for i := 0; i < length_val; i++ {
		r.data_val[i] = '*'
	}
}

// Sysfunc_format_FLOAT converts FLOAT to VARCHAR, as requested by the format string.
//
//        FORMAT ( float expression, format [ ,language ] )
//
// The format string is e.g. "#'0.00" or "#'##0.00" which will print the number with group separators, with two digits after the fractional point.
// The format string "0" will round the number to nearest integer value.
//
//        NOTE: The rounding is half-to-even (21.5->22, 22.5->22, 23.5->24). It is unlike MS SQL Server, which rounds half-up (21.5->22, 22.5->23, 23.5->24)
//
func Sysfunc_format_FLOAT(r *VARCHAR, a *FLOAT, b *VARCHAR, syslang *SYSLANGUAGE) {
	var (
		err  error
		res  []byte
		nfmt format.Numberfmt
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &syslang.Header) {
		return
	}

	/* operation */

	nfmt.Get_format_info(b.data_val) // 1st parse of format string, to analyze it

	nfmt.Fill_float64(a.data_val) // put string representation of number in nfmt

	nfmt.Set_langinfo(syslang.data_langinfo) // put locale information in nfmt

	if res, err = nfmt.Append_formatted_number(r.data_val[:0], b.data_val); err != nil { // 2nd parse of format string, to output result
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_FORMAT, rsql.ERROR_BATCH_ABORT, a.data_val, b.data_val)
		return
	}

	// check result length

	if len(res) > int(r.Data_precision) && utf8.RuneCount(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_FORMAT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val, b.data_val)
		return
	}

	r.data_val = res
}

func Sysfunc_isnumeric_FLOAT(r *INT, a *FLOAT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = 0
	if a.data_NULL_flag == false {
		r.data_val = 1
	}
}

func Sysfunc_isnull_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
	}
}

func Sysfunc_iif_FLOAT(r *FLOAT, a *BOOLEAN, b *FLOAT, c *FLOAT) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	r.data_val = c.data_val
}

func Sysfunc_choose_FLOAT(r *FLOAT, a *INT, b_list []*FLOAT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	r.data_val = elem.data_val
}

func Sysfunc_coalesce_FLOAT(r *FLOAT, a_list []*FLOAT) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = elem.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT, c *FLOAT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	r_val = (a.data_val >= b.data_val) && (a.data_val <= c.data_val)

	r.data_val = r_val
}

func Sysfunc_not_between_FLOAT(r *BOOLEAN, a *FLOAT, b *FLOAT, c *FLOAT) {

	Sysfunc_between_FLOAT(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_FLOAT(r *VARCHAR, a *FLOAT) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "FLOAT"...)
}

// Sysfunc_aggr_count_FLOAT is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_FLOAT(r *INT, a *FLOAT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val >= math.MaxInt32 { // check that r.data_val won't go beyond MaxInt32
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_count_big_FLOAT is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_big_FLOAT(r *BIGINT, a *FLOAT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val == math.MaxInt64 { // check that r.data_val won't go beyond MaxInt64
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_sum_FLOAT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_sum_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	/* operation */

	a_val = a.data_val
	b_val = r.data_val

	r_val = a_val + b_val

	switch {
	case math.IsNaN(r_val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

// Sysfunc_aggr_min_FLOAT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_min_FLOAT(r *FLOAT, a *FLOAT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if math.IsNaN(a.data_val) { // ignore NaN
		return
	}

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val < r.data_val {
		r.data_val = a.data_val
	}
}

// Sysfunc_aggr_max_FLOAT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_max_FLOAT(r *FLOAT, a *FLOAT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if math.IsNaN(a.data_val) { // ignore NaN
		return
	}

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val > r.data_val {
		r.data_val = a.data_val
	}
}
