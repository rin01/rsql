package data

import ()

func Sysfunc_isnumeric_VOID(r *INT, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = 0
}

func Sysfunc_isnull_VOID(r *VOID, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_iif_VOID(r *VOID, a *BOOLEAN, b *VOID, c *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_choose_VOID(r *VOID, a *INT, b_list []*VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_coalesce_VOID(r *VOID, a_list []*VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_VOID(r *BOOLEAN, a *VOID, b *VOID, c *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_not_between_VOID(r *BOOLEAN, a *VOID, b *VOID, c *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_typeof_VOID(r *VARCHAR, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "VOID"...)
}
