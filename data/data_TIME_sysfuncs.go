package data

import (
	"time"
	"unicode/utf8"

	"rsql"
	"rsql/format"
	"rsql/mdat"
)

func Sysfunc_dateadd_hour_TIME(r *TIME, a *INT, b *TIME) {
	var (
		delta int64
		res   time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta = a.data_val

	if delta >= 24 || delta <= -24 {
		delta = delta % 24
	}

	res = b.data_val.Add(time.Duration(delta * 3600e9))

	if res.Before(mdat.TIME_1900_01_01) || !res.Before(mdat.TIME_1900_01_02_SENTINEL) {
		hour, minute, second := res.Clock()
		nanosecond := res.Nanosecond()
		res = time.Date(1900, 1, 1, hour, minute, second, nanosecond, time.UTC)
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_minute_TIME(r *TIME, a *INT, b *TIME) {
	var (
		delta int64
		res   time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta = a.data_val

	if delta >= 1440 || delta <= -1440 {
		delta = delta % 1440
	}

	res = b.data_val.Add(time.Duration(delta * 60e9))

	if res.Before(mdat.TIME_1900_01_01) || !res.Before(mdat.TIME_1900_01_02_SENTINEL) {
		hour, minute, second := res.Clock()
		nanosecond := res.Nanosecond()
		res = time.Date(1900, 1, 1, hour, minute, second, nanosecond, time.UTC)
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_second_TIME(r *TIME, a *INT, b *TIME) {
	var (
		delta int64
		res   time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta = a.data_val

	if delta >= 24*3600 || delta <= -24*3600 {
		delta = delta % (24 * 3600)
	}

	res = b.data_val.Add(time.Duration(delta * 1e9))

	if res.Before(mdat.TIME_1900_01_01) || !res.Before(mdat.TIME_1900_01_02_SENTINEL) {
		hour, minute, second := res.Clock()
		nanosecond := res.Nanosecond()
		res = time.Date(1900, 1, 1, hour, minute, second, nanosecond, time.UTC)
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_millisecond_TIME(r *TIME, a *INT, b *TIME) {
	var (
		delta int64
		res   time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta = a.data_val

	res = b.data_val.Add(time.Duration(delta * 1e6))

	if res.Before(mdat.TIME_1900_01_01) || !res.Before(mdat.TIME_1900_01_02_SENTINEL) {
		hour, minute, second := res.Clock()
		nanosecond := res.Nanosecond()
		res = time.Date(1900, 1, 1, hour, minute, second, nanosecond, time.UTC)
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_microsecond_TIME(r *TIME, a *INT, b *TIME) {
	var (
		delta int64
		res   time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta = a.data_val

	res = b.data_val.Add(time.Duration(delta * 1e3))

	if res.Before(mdat.TIME_1900_01_01) || !res.Before(mdat.TIME_1900_01_02_SENTINEL) {
		hour, minute, second := res.Clock()
		nanosecond := res.Nanosecond()
		res = time.Date(1900, 1, 1, hour, minute, second, nanosecond, time.UTC)
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_nanosecond_TIME(r *TIME, a *INT, b *TIME) {
	var (
		delta int64
		res   time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta = a.data_val

	res = b.data_val.Add(time.Duration(delta))

	if res.Before(mdat.TIME_1900_01_01) || !res.Before(mdat.TIME_1900_01_02_SENTINEL) {
		hour, minute, second := res.Clock()
		nanosecond := res.Nanosecond()
		res = time.Date(1900, 1, 1, hour, minute, second, nanosecond, time.UTC)
	}

	r.data_val.Time = res
}

var (
	TFORMAT_0_100  = []byte(`h\:mmtt`)
	TFORMAT_8_108  = []byte(`HH\:mm\:ss`)
	TFORMAT_9_109  = []byte(`h\:mm\:ss\.ffftt`)
	TFORMAT_13_113 = []byte(`HH:mm\:ss\.fff`)
	TFORMAT_14_114 = []byte(`HH:mm\:ss\.fff`)
	TFORMAT_20_120 = []byte(`HH\:mm\:ss`)
	TFORMAT_21_121 = []byte(`HH\:mm\:ss\.fff`)
	TFORMAT_126    = []byte(`HH\:mm\:ss\.fff`)
)

// Sysfunc_convert_TIME_to_VARCHAR formats a TIME into a VARCHAR, with a format style.
//
//      CONVERT ( data_type [ ( length ) ] , time expression [ , style ] )
//
//      NOTE: target may be fixlen, if SQL script was CAST(@mytime as char(50)), which has been transformed by the decorator into CONVERT.
//
func Sysfunc_convert_TIME_to_VARCHAR(r *VARCHAR, a *TIME, b *INT, syslang *SYSLANGUAGE) {
	var (
		format_string []byte
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &syslang.Header) {
		return
	}

	/* operation */

	switch b.data_val {
	case 0, 100:
		format_string = TFORMAT_0_100
	case 8, 108:
		format_string = TFORMAT_8_108
	case 9, 109:
		format_string = TFORMAT_9_109
	case 13, 113:
		format_string = TFORMAT_13_113
	case 14, 114:
		format_string = TFORMAT_14_114
	case 20, 120:
		format_string = TFORMAT_20_120
	case 21, 121:
		format_string = TFORMAT_21_121
	case 126:
		format_string = TFORMAT_126

	default:
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_TIME_CONVERT_VARCHAR_UNSUPPORTED_STYLE, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	res := format.Append_formatted_date(r.data_val[:0], a.data_val.Time, format_string, syslang.data_langinfo)

	// check result length

	if len(res) > int(r.Data_precision) && utf8.RuneCount(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_TIME_CONVERT_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String(), format_string)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

func Sysfunc_timefromparts_TIME(r *TIME, hour *INT, minute *INT, seconds *INT, fractions *INT, precision *INT) {
	var (
		nanoseconds int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&hour.Header, &minute.Header, &seconds.Header, &fractions.Header, &precision.Header) {

		if r.data_error == nil && precision.data_error == nil && precision.data_NULL_flag == true { // if no error in arguments, but precision is NULL, raise error
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_TIME_TIMEFROMPARTS_NULL_PRECISION, rsql.ERROR_BATCH_ABORT)
		}
		return
	}

	// check values are in proper range

	nanoseconds = mdat.Nanoseconds_from_fractions(fractions.data_val, precision.data_val) // -1 if error

	if (hour.data_val >= 0 && hour.data_val <= 23 &&
		minute.data_val >= 0 && minute.data_val <= 59 &&
		seconds.data_val >= 0 && seconds.data_val <= 59 &&
		nanoseconds >= 0 && nanoseconds <= 999999999) == false {

		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_TIME_TIMEFROMPARTS_ILLEGAL_ARGS, rsql.ERROR_BATCH_ABORT, hour.data_val, minute.data_val, seconds.data_val, fractions.data_val, precision.data_val)
		return
	}

	/* operation */

	res := time.Date(1900, 1, 1, int(hour.data_val), int(minute.data_val), int(seconds.data_val), int(nanoseconds), time.UTC)

	r.data_val.Time = res
}

func Sysfunc_isnull_TIME(r *TIME, a *TIME, b *TIME) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
	}
}

func Sysfunc_iif_TIME(r *TIME, a *BOOLEAN, b *TIME, c *TIME) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	r.data_val = c.data_val
}

func Sysfunc_choose_TIME(r *TIME, a *INT, b_list []*TIME) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	r.data_val = elem.data_val
}

func Sysfunc_coalesce_TIME(r *TIME, a_list []*TIME) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = elem.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_TIME(r *BOOLEAN, a *TIME, b *TIME, c *TIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	r_val = !a.data_val.Before(b.data_val.Time) && !a.data_val.After(c.data_val.Time)

	r.data_val = r_val
}

func Sysfunc_not_between_TIME(r *BOOLEAN, a *TIME, b *TIME, c *TIME) {

	Sysfunc_between_TIME(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_TIME(r *VARCHAR, a *TIME) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "TIME"...)
}
