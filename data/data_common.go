package data

import (
	"fmt"
	"strconv"

	"golang.org/x/text/collate"

	"rsql"
	"rsql/format"
	"rsql/lang"
	"rsql/like"
	"rsql/mbin"
	"rsql/mdat"
	"rsql/mstr"
	"rsql/quad"
)

type Compsort_t int // 0 is equality. 1 is greater, and -1 is less.

// Header is embedded by all dataslot objects (data.INT, data.FLOAT, etc).
// The Header struct implements rsql.IDataslot interface.
type Header struct {
	Data_datatype  rsql.Datatype_t
	Data_kind      rsql.Kind_t // KIND_RO_LEAF, KIND_VAR_LEAF, KIND_COL_LEAF, KIND_TMP
	data_error     *rsql.Error // if not nil, indicates that an operation has failed. In this case, data_NULL_flag and data_val fields are undefined.
	data_NULL_flag bool        // if true, the value is NULL. In this case, data_val fields are undefined.
}

func (head *Header) Datatype() rsql.Datatype_t {
	return head.Data_datatype
}

func (head *Header) Kind() rsql.Kind_t {
	return head.Data_kind
}

func (head *Header) Set_kind(kind rsql.Kind_t) {
	head.Data_kind = kind
}

func (head *Header) Error() *rsql.Error {
	return head.data_error
}

func (head *Header) Set_Error(rsql_err *rsql.Error) {
	rsql.Assert(rsql_err != nil)

	head.data_error = rsql_err
}

func (head *Header) NULL_flag() bool {
	if head.data_error != nil {
		panic("NULL_flag() cannot be called if error exists in dataslot")
	}

	return head.data_NULL_flag
}

func (head *Header) Set_to_NULL() {
	head.data_error = nil
	head.data_NULL_flag = true
}

// process_error_and_NULL checks if an operand is flagged as error or NULL.
// If it is the case, it sets data_error and/or data_NULL_flag of the result header, and returns true.
// If no error and no NULL flag are found in all the operands, data_error and data_NULL_flag of the result header are set to nil and false, and the function returns false.
//
func (head *Header) process_error_and_NULL(operands ...*Header) bool {
	var (
		a               *Header
		NULL_flag_found bool
	)

	/* if error operand, returns error */

	for _, a = range operands {
		if a.data_error != nil {
			head.data_error = a.data_error
			return true
		}
		if a.data_NULL_flag {
			NULL_flag_found = true
		}
	}

	/* if no error operands, but NULL operand found, returns NULL */

	if NULL_flag_found {
		head.data_error = nil
		head.data_NULL_flag = true
		return true
	}

	head.data_error = nil
	head.data_NULL_flag = false
	return false
}

// VOID represents a NULL literal
type VOID struct {
	Header
}

// SYSCOLLATOR represents a collator object
//
//     SYSCOLLATOR is always created as a SYSCOLLATOR literal.
//         - Normally, it should never be error. But if the VM cannot lazily create the inner actual collator, data_error will be filled with the proper error, which propagates to the result in the normal way.
//         - It is never NULL.
//
type SYSCOLLATOR struct {
	Header

	data_collation_string string
	data_cached_collator  *collate.Collator // the actual collator is lazily created by the VM when needed
}

// SYSLANGUAGE represents a language object
//
// It can contain an error or a NULL value, like any other datatype.
//
type SYSLANGUAGE struct {
	Header

	data_langinfo       *lang.Langinfo
	data_firstdayofweek int
	data_DMY            string
}

// BOOLEAN represents a boolean value, which is TRUE, FALSE, or NULL (unknown)
type BOOLEAN struct {
	Header

	data_val bool
}

type VARBINARY struct {
	Header
	Data_precision uint16

	data_val mbin.Mybin
}

// VARCHAR represents a string value. The SQL CHAR datatype is in fact data.VARCHAR with Data_fixlen_flag=true.
type VARCHAR struct {
	Header
	Data_precision   uint16
	Data_fixlen_flag bool

	data_val mstr.Mystr
}

type REGEXPLIKE struct {
	Header

	data_val like.Likexp_bundle
}

type BIT struct {
	Header

	data_val int64 // but contains only 0 or 1
}

type TINYINT struct {
	Header

	data_val int64 // but contains only uint8 values
}

type SMALLINT struct {
	Header

	data_val int64 // but contains only int16 values
}

type INT struct {
	Header

	data_val int64 // but contains only int32 values
}

type BIGINT struct {
	Header

	data_val int64
}

type MONEY NUMERIC

type NUMERIC struct {
	Header
	Data_precision uint16
	Data_scale     uint16

	data_val quad.MyQuad
}

type FLOAT struct {
	Header

	data_val float64
}

type DATE DATETIME

type TIME DATETIME

type DATETIME struct {
	Header

	data_val mdat.MyDatetime
}

// Case_element_t is used as element in argument list for CASE expressions.
//
type Case_element_t struct {
	Cond *BOOLEAN
	Val  interface{}
}

// Dataslot_XXX_NULL_new creates a new Dataslot of the requested datatype, initialized to NULL.
//
// It is used to create a new Dataslot for all the Token_primaries, initialized with a NULL value.
//
// Note : for the 'literal' primaries ( string constants, int constants, etc ), which do have a value and are not NULL, you must use Dataslot_literal_FLOAT_new() etc instead.
//
// rsql.DATATYPE_CHAR is not allowed as sql_datatype. Instead, use rsql.DATATYPE_VARCHAR with fixlen_flag=1.
//
// You must pass 0 for arguments that are not relevant for the requested datatype. E.g. pass 0 for sql_scale when you request a VARCHAR.
//
func Dataslot_XXX_NULL_new(kind rsql.Kind_t, sql_datatype rsql.Datatype_t, sql_precision uint16, sql_scale uint16, fixlen_flag bool) rsql.IDataslot {
	var (
		dataslot rsql.IDataslot
	)

	switch sql_datatype {
	case rsql.DATATYPE_VOID:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_VOID_NULL(kind)

	case rsql.DATATYPE_SYSLANGUAGE:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_SYSLANGUAGE_NULL(kind)

	case rsql.DATATYPE_BOOLEAN:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_BOOLEAN_NULL(kind)

	case rsql.DATATYPE_VARBINARY:
		rsql.Assert(sql_precision != 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_VARBINARY_NULL(kind, sql_precision)

	case rsql.DATATYPE_VARCHAR:
		rsql.Assert(sql_precision != 0)
		rsql.Assert(sql_scale == 0)

		dataslot = New_VARCHAR_NULL(kind, sql_precision, fixlen_flag)

	case rsql.DATATYPE_REGEXPLIKE:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_REGEXPLIKE_NULL(kind)

	case rsql.DATATYPE_BIT:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_BIT_NULL(kind)

	case rsql.DATATYPE_TINYINT:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_TINYINT_NULL(kind)

	case rsql.DATATYPE_SMALLINT:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_SMALLINT_NULL(kind)

	case rsql.DATATYPE_INT:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_INT_NULL(kind)

	case rsql.DATATYPE_BIGINT:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_BIGINT_NULL(kind)

	case rsql.DATATYPE_MONEY:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_MONEY_NULL(kind)

	case rsql.DATATYPE_NUMERIC:
		rsql.Assert(sql_precision != 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_NUMERIC_NULL(kind, sql_precision, sql_scale)

	case rsql.DATATYPE_FLOAT:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_FLOAT_NULL(kind)

	case rsql.DATATYPE_DATE:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_DATE_NULL(kind)

	case rsql.DATATYPE_TIME:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_TIME_NULL(kind)

	case rsql.DATATYPE_DATETIME:
		rsql.Assert(sql_precision == 0)
		rsql.Assert(sql_scale == 0)
		rsql.Assert(fixlen_flag == false)

		dataslot = New_DATETIME_NULL(kind)

	default:
		panic("unknown datatype")
	}

	return dataslot
}

func Get_value_from_literal_xxxINT(dataslot rsql.IDataslot) (val int64, ok bool) {

	if !(dataslot.Kind() == rsql.KIND_RO_LEAF && dataslot.Error() == nil && dataslot.NULL_flag() == false) {
		return 0, false
	}

	switch dataslot := dataslot.(type) {
	case *TINYINT:
		val = dataslot.data_val
	case *SMALLINT:
		val = dataslot.data_val
	case *INT:
		val = dataslot.data_val
	case *BIGINT:
		val = dataslot.data_val
	default:
		return 0, false
	}

	return val, true
}

// Get_value_from_xxxINT is used to retrieve ROWID or IDENTITY value, as int64.
// Panics if error or NULL.
//
func Get_value_from_xxxINT(dataslot rsql.IDataslot) (val int64) {

	rsql.Assert(dataslot.Error() == nil && dataslot.NULL_flag() == false)

	switch dataslot := dataslot.(type) {
	case *TINYINT:
		val = dataslot.data_val
	case *SMALLINT:
		val = dataslot.data_val
	case *INT:
		val = dataslot.data_val
	case *BIGINT:
		val = dataslot.data_val
	default:
		panic("impossible")
	}

	return val
}

// Set_value_to_xxxINT is used to sets ROWID or IDENTITY value.
// An error is returned if overflow.
//
func Set_value_to_xxxINT(dataslot rsql.IDataslot, val int64) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	switch dataslot := dataslot.(type) {
	case *TINYINT:
		Set_TINYINT_value_from_int64(dataslot, val)
	case *SMALLINT:
		Set_SMALLINT_value_from_int64(dataslot, val)
	case *INT:
		Set_INT_value_from_int64(dataslot, val)
	case *BIGINT:
		Set_BIGINT_value_from_int64(dataslot, val)
	default:
		panic("impossible")
	}

	if rsql_err = dataslot.Error(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func Set_int32_value(dataslot rsql.IDataslot, val int32) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	switch dataslot := dataslot.(type) {
	case *TINYINT:
		Set_TINYINT_value_from_int64(dataslot, int64(val))
	case *SMALLINT:
		Set_SMALLINT_value_from_int64(dataslot, int64(val))
	case *INT:
		Set_INT_value_from_int64(dataslot, int64(val))
	case *BIGINT:
		Set_BIGINT_value_from_int64(dataslot, int64(val))

	case *MONEY:
		dataslot.data_error = nil
		dataslot.data_NULL_flag = false
		dataslot.data_error = quad.From_int32(&dataslot.data_val, dataslot.Data_precision, dataslot.Data_scale, val)

	case *NUMERIC:
		dataslot.data_error = nil
		dataslot.data_NULL_flag = false
		dataslot.data_error = quad.From_int32(&dataslot.data_val, dataslot.Data_precision, dataslot.Data_scale, val)

	case *FLOAT:
		dataslot.data_error = nil
		dataslot.data_NULL_flag = false
		dataslot.data_val = float64(val)

	default:
		panic("impossible")
	}

	if rsql_err = dataslot.Error(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

// Get_value_from_VARCHAR is used to retrieve _@current_db_name value, as string.
// Panics if error or NULL.
//
func Get_value_from_VARCHAR(dataslot rsql.IDataslot) (val string) {

	rsql.Assert(dataslot.Error() == nil && dataslot.NULL_flag() == false)

	switch dataslot := dataslot.(type) {
	case *VARCHAR:
		return string(dataslot.data_val)
	default:
		panic("impossible")
	}

}

// Convert_VARCHAR_to_XXX is used during BULK INSERT, to convert VARCHAR field into proper target datatype.
//
// style and syslang are only used by DATE, TIME and DATETIME.
//
func Convert_VARCHAR_to_XXX(r rsql.IDataslot, a *VARCHAR, style *INT, syslang *SYSLANGUAGE) {

	switch r.Datatype() {
	case rsql.DATATYPE_VARBINARY:
		Cast_VARCHAR_to_VARBINARY_intern(r.(*VARBINARY), a)
	case rsql.DATATYPE_VARCHAR:
		Cast_VARCHAR_to_VARCHAR(r.(*VARCHAR), a)
	case rsql.DATATYPE_BIT:
		Cast_VARCHAR_to_BIT(r.(*BIT), a)
	case rsql.DATATYPE_TINYINT:
		Cast_VARCHAR_to_TINYINT(r.(*TINYINT), a)
	case rsql.DATATYPE_SMALLINT:
		Cast_VARCHAR_to_SMALLINT(r.(*SMALLINT), a)
	case rsql.DATATYPE_INT:
		Cast_VARCHAR_to_INT(r.(*INT), a)
	case rsql.DATATYPE_BIGINT:
		Cast_VARCHAR_to_BIGINT(r.(*BIGINT), a)
	case rsql.DATATYPE_MONEY:
		Cast_VARCHAR_to_NUMERIC((*NUMERIC)(r.(*MONEY)), a)
	case rsql.DATATYPE_NUMERIC:
		Cast_VARCHAR_to_NUMERIC(r.(*NUMERIC), a)
	case rsql.DATATYPE_FLOAT:
		Cast_VARCHAR_to_FLOAT(r.(*FLOAT), a)
	case rsql.DATATYPE_DATE:
		Sysfunc_convert_VARCHAR_to_DATE(r.(*DATE), a, style, syslang)
	case rsql.DATATYPE_TIME:
		Sysfunc_convert_VARCHAR_to_TIME(r.(*TIME), a, style, syslang)
	case rsql.DATATYPE_DATETIME:
		Sysfunc_convert_VARCHAR_to_DATETIME(r.(*DATETIME), a, style, syslang)
	default:
		panic("impossible")
	}
}

func Dataslot_XXX_copy(dest rsql.IDataslot, src rsql.IDataslot) {

	switch dest.Datatype() {
	case rsql.DATATYPE_VOID:
		Copy_VOID(dest.(*VOID), src.(*VOID))

	case rsql.DATATYPE_BOOLEAN:
		Copy_BOOLEAN(dest.(*BOOLEAN), src.(*BOOLEAN))

	case rsql.DATATYPE_VARBINARY:
		Copy_VARBINARY(dest.(*VARBINARY), src.(*VARBINARY))

	case rsql.DATATYPE_VARCHAR:
		Copy_VARCHAR(dest.(*VARCHAR), src.(*VARCHAR))

	case rsql.DATATYPE_BIT:
		Copy_BIT(dest.(*BIT), src.(*BIT))

	case rsql.DATATYPE_TINYINT:
		Copy_TINYINT(dest.(*TINYINT), src.(*TINYINT))

	case rsql.DATATYPE_SMALLINT:
		Copy_SMALLINT(dest.(*SMALLINT), src.(*SMALLINT))

	case rsql.DATATYPE_INT:
		Copy_INT(dest.(*INT), src.(*INT))

	case rsql.DATATYPE_BIGINT:
		Copy_BIGINT(dest.(*BIGINT), src.(*BIGINT))

	case rsql.DATATYPE_MONEY:
		Copy_MONEY(dest.(*MONEY), src.(*MONEY))

	case rsql.DATATYPE_NUMERIC:
		Copy_NUMERIC(dest.(*NUMERIC), src.(*NUMERIC))

	case rsql.DATATYPE_FLOAT:
		Copy_FLOAT(dest.(*FLOAT), src.(*FLOAT))

	case rsql.DATATYPE_DATE:
		Copy_DATE(dest.(*DATE), src.(*DATE))

	case rsql.DATATYPE_TIME:
		Copy_TIME(dest.(*TIME), src.(*TIME))

	case rsql.DATATYPE_DATETIME:
		Copy_DATETIME(dest.(*DATETIME), src.(*DATETIME))

	default:
		panic("unknown datatype")
	}
}

func Dataslot_XXX_clone(kind rsql.Kind_t, a rsql.IDataslot) rsql.IDataslot {

	switch a.Datatype() {
	case rsql.DATATYPE_VOID:
		return Clone_VOID(kind, a.(*VOID))

	case rsql.DATATYPE_BOOLEAN:
		return Clone_BOOLEAN(kind, a.(*BOOLEAN))

	case rsql.DATATYPE_VARBINARY:
		return Clone_VARBINARY(kind, a.(*VARBINARY))

	case rsql.DATATYPE_VARCHAR:
		return Clone_VARCHAR(kind, a.(*VARCHAR))

	case rsql.DATATYPE_BIT:
		return Clone_BIT(kind, a.(*BIT))

	case rsql.DATATYPE_TINYINT:
		return Clone_TINYINT(kind, a.(*TINYINT))

	case rsql.DATATYPE_SMALLINT:
		return Clone_SMALLINT(kind, a.(*SMALLINT))

	case rsql.DATATYPE_INT:
		return Clone_INT(kind, a.(*INT))

	case rsql.DATATYPE_BIGINT:
		return Clone_BIGINT(kind, a.(*BIGINT))

	case rsql.DATATYPE_MONEY:
		return Clone_MONEY(kind, a.(*MONEY))

	case rsql.DATATYPE_NUMERIC:
		return Clone_NUMERIC(kind, a.(*NUMERIC))

	case rsql.DATATYPE_FLOAT:
		return Clone_FLOAT(kind, a.(*FLOAT))

	case rsql.DATATYPE_DATE:
		return Clone_DATE(kind, a.(*DATE))

	case rsql.DATATYPE_TIME:
		return Clone_TIME(kind, a.(*TIME))

	case rsql.DATATYPE_DATETIME:
		return Clone_DATETIME(kind, a.(*DATETIME))

	default:
		panic("unknown datatype")
	}
}

// used by ast.debug_print functions.
// Returns address of dataslot as hexa string.
//
func Dataslot_address(a rsql.IDataslot) string {

	switch a.Datatype() {
	case rsql.DATATYPE_VOID:
		return fmt.Sprintf("%p", a.(*VOID))

	case rsql.DATATYPE_BOOLEAN:
		return fmt.Sprintf("%p", a.(*BOOLEAN))

	case rsql.DATATYPE_VARBINARY:
		return fmt.Sprintf("%p", a.(*VARBINARY))

	case rsql.DATATYPE_VARCHAR:
		return fmt.Sprintf("%p", a.(*VARCHAR))

	case rsql.DATATYPE_BIT:
		return fmt.Sprintf("%p", a.(*BIT))

	case rsql.DATATYPE_TINYINT:
		return fmt.Sprintf("%p", a.(*TINYINT))

	case rsql.DATATYPE_SMALLINT:
		return fmt.Sprintf("%p", a.(*SMALLINT))

	case rsql.DATATYPE_INT:
		return fmt.Sprintf("%p", a.(*INT))

	case rsql.DATATYPE_BIGINT:
		return fmt.Sprintf("%p", a.(*BIGINT))

	case rsql.DATATYPE_MONEY:
		return fmt.Sprintf("%p", a.(*MONEY))

	case rsql.DATATYPE_NUMERIC:
		return fmt.Sprintf("%p", a.(*NUMERIC))

	case rsql.DATATYPE_FLOAT:
		return fmt.Sprintf("%p", a.(*FLOAT))

	case rsql.DATATYPE_DATE:
		return fmt.Sprintf("%p", a.(*DATE))

	case rsql.DATATYPE_TIME:
		return fmt.Sprintf("%p", a.(*TIME))

	case rsql.DATATYPE_DATETIME:
		return fmt.Sprintf("%p", a.(*DATETIME))

	default:
		panic("unknown datatype")
	}
}

// Append_export is used by BULK EXPORT.
//
func Append_export(dest []byte, a rsql.IDataslot, date_format []byte, time_format []byte, datetime_format []byte, langinfo *lang.Langinfo) []byte {

	rsql.Assert(a.Error() == nil) // panics if error

	if a.NULL_flag() { // if NULL
		return dest
	}

	switch a := a.(type) {
	case *VOID:
		return dest

	case *VARBINARY:
		dest = append(dest, "0x"...)
		return mbin.Append_hexastring(dest, a.data_val)

	case *VARCHAR:
		if len(a.data_val) == 0 {
			return append(dest, 0x00)
		}

		return append(dest, a.data_val...)

	case *BIT:
		c := byte('0')
		if a.data_val != 0 {
			c = '1'
		}
		return append(dest, c)

	case *TINYINT:
		return strconv.AppendInt(dest, a.data_val, 10)

	case *SMALLINT:
		return strconv.AppendInt(dest, a.data_val, 10)

	case *INT:
		return strconv.AppendInt(dest, a.data_val, 10)

	case *BIGINT:
		return strconv.AppendInt(dest, a.data_val, 10)

	case *MONEY:
		return quad.AppendQuad(dest, &a.data_val)

	case *NUMERIC:
		return quad.AppendQuad(dest, &a.data_val)

	case *FLOAT:
		return strconv.AppendFloat(dest, a.data_val, 'e', -1, 64)

	case *DATE:
		return format.Append_formatted_date(dest, a.data_val.Time, date_format, langinfo)

	case *TIME:
		return format.Append_formatted_date(dest, a.data_val.Time, time_format, langinfo)

	case *DATETIME:
		return format.Append_formatted_date(dest, a.data_val.Time, datetime_format, langinfo)

	default:
		panic("impossible")
	}
}

func Clone_row(kind rsql.Kind_t, row rsql.Row) rsql.Row {

	result := make([]rsql.IDataslot, len(row))

	for i, dataslot := range row {
		result[i] = Dataslot_XXX_clone(kind, dataslot)
	}

	return result
}

func Clone_row_for_orderby(kind rsql.Kind_t, row rsql.Row) rsql.Row {

	result := make([]rsql.IDataslot, 0, len(row)+1)

	result = append(result, New_BIGINT_NULL(kind)) // $rowid$

	for _, dataslot := range row {
		result = append(result, Dataslot_XXX_clone(kind, dataslot))
	}

	return result
}

// index from 0 to DATATYPE_PRECISION_BIGINT-1
// Used e.g. for Sysfunc_round_INT().
//
var G_POWER_OF_10 = [rsql.DATATYPE_PRECISION_BIGINT]int64{
	1,
	10,
	100,
	1000,
	10000,
	100000,
	1000000,
	10000000,
	100000000,
	1000000000,

	10000000000,
	100000000000,
	1000000000000,
	10000000000000,
	100000000000000,
	1000000000000000,
	10000000000000000,
	100000000000000000,
	1000000000000000000, // BIGINT contains 19 significant digits
}
