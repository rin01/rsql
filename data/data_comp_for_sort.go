package data

import (
	"bytes"
	"math"
	"time"

	"rsql"
	"rsql/mstr"
	"rsql/quad"
)

// Comp_for_sort_VARBINARY always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_VARBINARY(a *VARBINARY, b *VARBINARY) Compsort_t {
	var (
		res int
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	res = bytes.Compare(a.data_val, b.data_val)

	return Compsort_t(res)
}

// Comp_for_sort_VARCHAR always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
//
// NOTE: coll.data_cached_collator MUST BE INITIALIZED WITH A VALID *collate.Collator, or it panics.
//
func Comp_for_sort_VARCHAR(a *VARCHAR, b *VARCHAR, coll *SYSCOLLATOR) Compsort_t {
	var (
		res int
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	res = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // res is -1, 0, or 1. Never fails.

	return Compsort_t(res)
}

// Comp_for_sort_BIT always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_BIT(a *BIT, b *BIT) Compsort_t {
	var (
		a_val int64
		b_val int64
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case a_val > b_val:
		return 1
	case a_val == b_val:
		return 0
	}

	return -1
}

// Comp_for_sort_TINYINT always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_TINYINT(a *TINYINT, b *TINYINT) Compsort_t {
	var (
		a_val int64
		b_val int64
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case a_val > b_val:
		return 1
	case a_val == b_val:
		return 0
	}

	return -1
}

// Comp_for_sort_SMALLINT always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_SMALLINT(a *SMALLINT, b *SMALLINT) Compsort_t {
	var (
		a_val int64
		b_val int64
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case a_val > b_val:
		return 1
	case a_val == b_val:
		return 0
	}

	return -1
}

// Comp_for_sort_INT always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_INT(a *INT, b *INT) Compsort_t {
	var (
		a_val int64
		b_val int64
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case a_val > b_val:
		return 1
	case a_val == b_val:
		return 0
	}

	return -1
}

// Comp_for_sort_BIGINT always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_BIGINT(a *BIGINT, b *BIGINT) Compsort_t {
	var (
		a_val int64
		b_val int64
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case a_val > b_val:
		return 1
	case a_val == b_val:
		return 0
	}

	return -1
}

// Comp_for_sort_NUMERIC always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_NUMERIC(a *NUMERIC, b *NUMERIC) Compsort_t {
	var (
		res int
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	res = quad.Compare(&a.data_val, &b.data_val) // res is -1, 0, or 1. Never fails.

	return Compsort_t(res)
}

// Comp_for_sort_FLOAT always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_FLOAT(a *FLOAT, b *FLOAT) Compsort_t {
	var (
		a_val float64
		b_val float64
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	// if a is NULL, ( and here, b is non-NULL ), a < b

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation if a or b is Nan */

	if math.IsNaN(b.data_val) { // if b is Nan
		if math.IsNaN(a.data_val) { // if a is also Nan, a (Nan) == b (Nan)
			return 0
		}
		return 1 // else, a (not-Nan) > b (Nan)
	}

	if math.IsNaN(a.data_val) { // if a is Nan, ( and here, b is not-Nan ), a < b
		return -1 // a (Nan) < b (not-Nan)
	}

	/* normal operation (a or b are not Nan) */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case a_val > b_val:
		return 1
	case a_val == b_val:
		return 0
	}

	return -1
}

// Comp_for_sort_DATETIME always returns a result.
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_DATETIME(a *DATETIME, b *DATETIME) Compsort_t {
	var (
		a_val time.Time
		b_val time.Time
	)

	rsql.Assert(a.data_error == nil && b.data_error == nil)

	/* if b is NULL */

	if b.data_NULL_flag {
		if a.data_NULL_flag { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.data_NULL_flag {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	a_val = a.data_val.Time
	b_val = b.data_val.Time

	switch {
	case a_val.After(b_val):
		return 1
	case a_val.Equal(b_val):
		return 0
	}

	return -1
}

//==================== comparison with different datatypes ===================

// Comp_for_sort_xxxINT_yyyINT always returns a result.
//
// Arguments can be TINYINT, SMALLINT, INT or BIGINT.
//
//     NULL value < any non-NULL value
//     NULL value == NULL value
// result is 0 if equal, 1 if greater and -1 if less.
//
// NOTE: if a or b is error, panics.
func Comp_for_sort_xxxINT_yyyINT(a rsql.AnyINT, b rsql.AnyINT) Compsort_t {
	var (
		a_val int64
		b_val int64
	)

	rsql.Assert(a.Error() == nil && b.Error() == nil)

	/* if b is NULL */

	if b.NULL_flag() {
		if a.NULL_flag() { // if a is also NULL, a (NULL) == b (NULL)
			return 0
		}
		return 1 // else, a (non-NULL) > b (NULL)
	}

	/* if a is NULL, ( and here, b is non-NULL ), a < b */

	if a.NULL_flag() {
		return -1 // a (NULL) < b (non-NULL)
	}

	/* operation */

	a_val = a.Data_val()
	b_val = b.Data_val()

	switch {
	case a_val > b_val:
		return 1
	case a_val == b_val:
		return 0
	}

	return -1
}
