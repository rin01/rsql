package data

import (
	"fmt"
	"math"
	"strconv"

	"rsql"
	"rsql/quad"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *NUMERIC) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_NUMERIC)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += uint32(a.Data_precision)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += uint32(a.Data_scale)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += quad.Hash(&a.data_val)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *NUMERIC) String() string {

	if a.data_error != nil {
		return fmt.Sprintf("NUMERIC(%d, %d): error", a.Data_precision, a.Data_scale)
	}

	if a.data_NULL_flag == true {
		return fmt.Sprintf("NUMERIC(%d, %d): null", a.Data_precision, a.Data_scale)
	}

	return fmt.Sprintf("NUMERIC(%d, %d): %s", a.Data_precision, a.Data_scale, a.data_val.String())
}

// New_NUMERIC_NULL creates a new data.NUMERIC initialized to NULL.
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc.
//
func New_NUMERIC_NULL(kind rsql.Kind_t, precision uint16, scale uint16) *NUMERIC {

	rsql.Assert(precision <= rsql.DATATYPE_NUMERIC_PRECISION_MAX && scale <= precision)

	dataslot := &NUMERIC{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_NUMERIC,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		Data_precision: precision,
		Data_scale:     scale,
	}

	quad.Zero(&dataslot.data_val, precision, scale)

	return dataslot
}

// New_literal_NUMERIC creates a new data.NUMERIC, converting string argument to number.
// Returns an error if invalid string, or if result is Inf or Nan.
//
func New_literal_NUMERIC(s string) (*NUMERIC, *rsql.Error) {
	var (
		precision uint16
		scale     uint16
	)

	dataslot := &NUMERIC{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_NUMERIC,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		Data_precision: 0,
		Data_scale:     0,
	}

	precision, scale, rsql_err := quad.From_string_with_implied_p_s(&dataslot.data_val, s) // returns an error if invalid string, or if result is Inf or Nan.
	if rsql_err != nil {
		return nil, rsql_err
	}

	dataslot.Data_precision = precision
	dataslot.Data_scale = scale

	return dataslot, nil
}

// New_literal_INT_BIGINT_or_NUMERIC creates a new data.INT, data.BIGINT or data.NUMERIC, converting a string to number.
// Returns an error if invalid string, or if result is Inf or Nan.
//
// If the literal number is too big to fit in an INT, then a NUMERIC is returned.
//
func New_literal_INT_BIGINT_or_NUMERIC(s string) (rsql.IDataslot, *rsql.Error) {
	var (
		err      error
		val      int64
		dataslot rsql.IDataslot
	)

	if val, err = strconv.ParseInt(s, 10, 64); err == nil { // if int64 value, return a *INT or *BIGINT
		switch {
		case val >= math.MinInt32 && val <= math.MaxInt32:
			dataslot = New_literal_INT_value(int32(val))
			return dataslot, nil

		default:
			dataslot = New_literal_BIGINT_value(val)
			return dataslot, nil
		}
	}

	dataslot, rsql_err := New_literal_NUMERIC(s) // else, try returning a *NUMERIC

	return dataslot, rsql_err
}

func Unary_minus_NUMERIC(r *NUMERIC, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Unary_minus(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val)

}

func Add_NUMERIC(r *NUMERIC, a *NUMERIC, b *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Add(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val, &b.data_val)

}

func Subtract_NUMERIC(r *NUMERIC, a *NUMERIC, b *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Subtract(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val, &b.data_val)

}

func Multiply_NUMERIC(r *NUMERIC, a *NUMERIC, b *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Multiply(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val, &b.data_val)

}

func Divide_NUMERIC(r *NUMERIC, a *NUMERIC, b *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Divide(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val, &b.data_val)

}

func Comp_equal_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = quad.Compare(&a.data_val, &b.data_val) // never fails

	r.data_val = (res == 0)
}

func Comp_greater_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = quad.Compare(&a.data_val, &b.data_val) // never fails

	r.data_val = (res > 0)
}

func Comp_less_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = quad.Compare(&a.data_val, &b.data_val) // never fails

	r.data_val = (res < 0)
}

func Comp_greater_equal_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = quad.Compare(&a.data_val, &b.data_val) // never fails

	r.data_val = (res >= 0)
}

func Comp_less_equal_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = quad.Compare(&a.data_val, &b.data_val) // never fails

	r.data_val = (res <= 0)
}

func Comp_not_equal_NUMERIC(r *BOOLEAN, a *NUMERIC, b *NUMERIC) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = quad.Compare(&a.data_val, &b.data_val) // never fails

	r.data_val = (res != 0)
}

func Is_null_NUMERIC(r *BOOLEAN, a *NUMERIC) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_NUMERIC(r *BOOLEAN, a *NUMERIC) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_NUMERIC checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_NUMERIC(r *BOOLEAN, a *NUMERIC, b_list []*NUMERIC) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_NUMERIC(r, a, b)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_NUMERIC(r *BOOLEAN, a *NUMERIC, b_list []*NUMERIC) {

	In_list_NUMERIC(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_NUMERIC(r *NUMERIC, a_list []Case_element_t) {
	var (
		elem_val *NUMERIC
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*NUMERIC)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			if r.data_error == nil && r.data_NULL_flag == false {
				r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &elem_val.data_val)
			}
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

// Cast_NUMERIC_to_VARCHAR casts NUMERIC to VARCHAR.
// If precision of target is unsufficient, returns an error.
//
func Cast_NUMERIC_to_VARCHAR(r *VARCHAR, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res := quad.AppendQuad(r.data_val[:0], &a.data_val)

	if len(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, res, r.Data_precision)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

// Cast_NUMERIC_to_BIT casts NUMERIC to BIT.
// If NUMERIC != 0, BIT is set to 1. Else, BIT is set to 0.
//
func Cast_NUMERIC_to_BIT(r *BIT, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_val = 0
	if quad.Is_zero(&a.data_val) == false {
		r.data_val = 1
	}
}

// Cast_NUMERIC_to_TINYINT casts NUMERIC to TINYINT
// Numeric values are truncated when cast to any integer type.
//
// Exactly the same as Cast_MONEY_to_TINYINT(), except the rounding mode.
//
func Cast_NUMERIC_to_TINYINT(r *TINYINT, a *NUMERIC) {
	var (
		rsql_err *rsql.Error
		val      int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int32_truncate(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	if val > math.MaxUint8 || val < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		return
	}

	r.data_val = int64(val)
}

// Cast_NUMERIC_to_SMALLINT casts NUMERIC to SMALLINT
// Numeric values are truncated when cast to any integer type.
//
// Exactly the same as Cast_MONEY_to_SMALLINT(), except the rounding mode.
//
func Cast_NUMERIC_to_SMALLINT(r *SMALLINT, a *NUMERIC) {
	var (
		rsql_err *rsql.Error
		val      int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int32_truncate(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	if val > math.MaxInt16 || val < math.MinInt16 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		return
	}

	r.data_val = int64(val)
}

// Cast_NUMERIC_to_INT casts NUMERIC to INT
// Numeric values are truncated when cast to any integer type.
//
// Exactly the same as Cast_MONEY_to_INT(), except the rounding mode.
//
func Cast_NUMERIC_to_INT(r *INT, a *NUMERIC) {
	var (
		rsql_err *rsql.Error
		val      int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int32_truncate(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	r.data_val = int64(val)
}

// Cast_NUMERIC_to_BIGINT casts NUMERIC to BIGINT
// Numeric values are truncated when cast to any integer type.
//
// Exactly the same as Cast_MONEY_to_BIGINT(), except the rounding mode.
//
func Cast_NUMERIC_to_BIGINT(r *BIGINT, a *NUMERIC) {
	var (
		rsql_err *rsql.Error
		val      int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_int64_truncate(&a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	r.data_val = val
}

// Cast_NUMERIC_to_NUMERIC casts NUMERIC to NUMERIC
//
func Cast_NUMERIC_to_NUMERIC(r *NUMERIC, a *NUMERIC) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.Copy(&r.data_val, r.Data_precision, r.Data_scale, &a.data_val)

}

// Cast_NUMERIC_to_FLOAT casts NUMERIC to FLOAT
//
func Cast_NUMERIC_to_FLOAT(r *FLOAT, a *NUMERIC) {
	var (
		rsql_err *rsql.Error
		val      float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if val, rsql_err = quad.To_float64(&a.data_val); rsql_err != nil { // error is returned in particular if a.data_val is Nan or Inf, or if conversion overflow occurs
		r.data_error = rsql_err
		if rsql_err.Message_id == rsql.ERROR_SQLDATA_NUMERIC_OUT_OF_RANGE { // conversion overflow
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_NUMERIC_CAST_FLOAT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String())
		}
		return
	}

	switch { // I put this check to be really sure, but val should never be Nan or Inf here, I think
	case math.IsNaN(val): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(val, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = val
}

func Assign_NUMERIC(r *NUMERIC, a *NUMERIC) *rsql.Error {

	rsql.Assert(r.Data_precision == a.Data_precision)
	rsql.Assert(r.Data_scale == a.Data_scale)

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val // we don't need decQuadCopy() because <p,s> of a and r are the same

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

func Copy_NUMERIC(r *NUMERIC, a *NUMERIC) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_NUMERIC(kind rsql.Kind_t, a *NUMERIC) *NUMERIC {

	return New_NUMERIC_NULL(kind, a.Data_precision, a.Data_scale)
}
