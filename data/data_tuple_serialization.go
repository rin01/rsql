package data

import (
	"math"
	"time"

	"rsql"
	"rsql/cache"
	"rsql/mdat"
	"rsql/quad"
)

func init() {

	rsql.Assert(cache.PAGE_SIZE <= 65536) // because tuple offsets are uint16
}

//==========================================================================================
//
//                              Tabledef ===> Row
//
//==========================================================================================

// New_leaf_row creates a new leaf Row, with NULL dataslots of proper types.
//
func New_leaf_row(kind rsql.Kind_t, tabledef *rsql.Tabledef) (leaf_row rsql.Row, index_ROWID int, index_IDENTITY int) {

	index_ROWID = -1
	index_IDENTITY = -1

	leaf_row = make([]rsql.IDataslot, len(tabledef.Td_coldefs))

	for i, coldef := range tabledef.Td_coldefs {
		if coldef.Cd_autonum != 0 {
			if coldef.Cd_autonum == rsql.CD_AUTONUM_ROWID {
				index_ROWID = i
			} else {
				rsql.Assert(coldef.Cd_autonum == rsql.CD_AUTONUM_IDENTITY)
				index_IDENTITY = i
			}
		}

		leaf_row[i] = Dataslot_XXX_NULL_new(kind, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
	}

	return leaf_row, index_ROWID, index_IDENTITY
}

// New_index_leaf_row_from_base_fields creates a new leaf Row for an index (tabledef), which dataslots point to the corresponding base_fields if it exists.
//
// If covering_index returns true, it means that the index contains all the fields used by the query, and there is no need to read the base table.
//
// If index is not covering, new base nk fields dataslots are put in base_fields, if they not already exist.
//
// NOTE: the returned row CANNOT BE USED FOR INDEX LOOKUP, because index leading nk fields have a dataslots, but trailing nk fields may be nil.
//       It is just used to load the values from the index leaf page, so that Sargs check can be performed.
//
func New_index_leaf_row_from_base_fields(kind rsql.Kind_t, indexdef *rsql.Tabledef, base_tabledef *rsql.Tabledef, base_fields rsql.Row) (index_leaf_row rsql.Row, covering_index bool) {

	// if index is base table, return a copy of base_fields

	if indexdef.Td_type == rsql.TD_TYPE_BASE_TABLE {
		index_leaf_row = make([]rsql.IDataslot, len(indexdef.Td_coldefs))

		rsql.Assert(len(indexdef.Td_coldefs) == len(base_fields))

		for i, dataslot := range base_fields {
			index_leaf_row[i] = dataslot // some dataslots may be nil
		}

		return index_leaf_row, true
	}

	// fill index_leaf_row dataslots

	index_leaf_row = make([]rsql.IDataslot, len(indexdef.Td_coldefs))

	for i, coldef := range indexdef.Td_coldefs { // when an index is used, reference the dataslots in base row. Some index_leaf_row dataslots may be nil.
		base_dataslot := base_fields[coldef.Cd_base_seqno] // in particular, leading index nk fields point to valid dataslots in base table, but trailing nk index fields can be nil
		if base_dataslot != nil {
			index_leaf_row[i] = base_dataslot
		}
	}

	// check if covering index

	covering_index = true

	m := make(map[rsql.IDataslot]struct{}, len(base_fields))

	for _, dataslot := range index_leaf_row { // map of all index dataslots
		if dataslot != nil {
			m[dataslot] = struct{}{}
		}
	}

	for _, dataslot := range base_fields { // check if all dataslots in base_fields are already referenced by index_leaf_row
		if dataslot == nil {
			continue
		}

		if _, ok := m[dataslot]; ok == false {
			covering_index = false
			break
		}
	}

	if covering_index == true {
		return index_leaf_row, covering_index // covering index. No need to read row from base table.
	}

	//===== index is not covering. Some dataslots in base row are not retrieved by the index. So, row from base table must be retrieved. =====

	for _, coldef := range base_tabledef.Td_nk { // base row must contain the base nk fields, because it will be used for lookup in base table
		seqno := coldef.Cd_base_seqno
		if base_fields[seqno] == nil {
			base_fields[seqno] = Dataslot_XXX_NULL_new(kind, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
		}
	}

	for i, coldef := range indexdef.Td_coldefs { // refresh with the new base nk fields added above
		base_dataslot := base_fields[coldef.Cd_base_seqno]
		if base_dataslot != nil {
			index_leaf_row[i] = base_dataslot
		}
	}

	return index_leaf_row, covering_index
}

// New_node_row creates a new node Row, with NULL dataslots of proper types.
//
// A node Row contains a BIGINT field at last position, for target_page_no. This field is not described in Tabledef.Td_nk list.
//
func New_node_row(kind rsql.Kind_t, tabledef *rsql.Tabledef) rsql.Row {

	fields_count := len(tabledef.Td_nk) + 1 // +1 for BIGINT target_page_no as last field

	node_row := make([]rsql.IDataslot, fields_count)

	for i, coldef := range tabledef.Td_nk {
		node_row[i] = Dataslot_XXX_NULL_new(kind, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
	}

	node_row[fields_count-1] = New_BIGINT_NULL(kind) // BIGINT target page_no field

	return node_row
}

// New_leaf_row_for_nk_lookup is used to create the row that will be used for index lookup.
//
// Dataslots are created only for the index nk fields. Other fields are nil.
//
func New_leaf_row_for_nk_lookup(kind rsql.Kind_t, tabledef *rsql.Tabledef) rsql.Row {

	leaf_row := make([]rsql.IDataslot, len(tabledef.Td_coldefs))

	for i, coldef := range tabledef.Td_nk {
		pos := i
		if tabledef.Td_type == rsql.TD_TYPE_BASE_TABLE {
			pos = int(coldef.Cd_base_seqno)
		}

		leaf_row[pos] = Dataslot_XXX_NULL_new(kind, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
	}

	return leaf_row
}

//==========================================================================================
//
//                utility functions to serialize and deserialze int64 value
//
//==========================================================================================

// append_compressed_int64 is a helper function.
// It appends the bytes of val, skipping all leading 0 bytes, or 0xff for negative numbers.
// LSB byte is always appended, even if it is 0.
// Returned length is always >= 1.
//
func append_compressed_int64(dest []byte, val int64) (res []byte, length uint16) {
	const P128 uint64 = 128

	v := uint64(val) // bit pattern of val

	if val < 0 {
		v = ^v // we want leading 1s to be transformed into 0s
	}

	// the highest bit of MSB of the compressed result is the bit sign

	switch {
	case v < P128: // at least one byte is appended, even if it is 0 (because a length of 0 means NULL, which is not what we want)
		dest = append(dest, uint8(val))
		length = 1

	case v < P128<<8:
		dest = append(dest, uint8(val), uint8(val>>8))
		length = 2

	case v < P128<<16:
		dest = append(dest, uint8(val), uint8(val>>8), uint8(val>>16))
		length = 3

	case v < P128<<24:
		dest = append(dest, uint8(val), uint8(val>>8), uint8(val>>16), uint8(val>>24))
		length = 4

	case v < P128<<32:
		dest = append(dest, uint8(val), uint8(val>>8), uint8(val>>16), uint8(val>>24), uint8(val>>32))
		length = 5

	case v < P128<<40:
		dest = append(dest, uint8(val), uint8(val>>8), uint8(val>>16), uint8(val>>24), uint8(val>>32), uint8(val>>40))
		length = 6

	case v < P128<<48:
		dest = append(dest, uint8(val), uint8(val>>8), uint8(val>>16), uint8(val>>24), uint8(val>>32), uint8(val>>40), uint8(val>>48))
		length = 7

	default:
		dest = append(dest, uint8(val), uint8(val>>8), uint8(val>>16), uint8(val>>24), uint8(val>>32), uint8(val>>40), uint8(val>>48), uint8(val>>56))
		length = 8
	}

	return dest, length //===> return
}

// uncompress_int64 returns int64 value, which was compressed with append_compressed_int64().
//
func uncompress_int64(src []byte) int64 {
	var (
		val uint64
	)

	if src[len(src)-1]&0x80 != 0 { // sign bit
		val = (^uint64(0)) << (uint(len(src)) * 8) // for negative numbers, fill leading 0xff bytes
	}

	switch len(src) { // fill trailing bytes from src
	case 1:
		val |= uint64(src[0])

	case 2:
		val |= uint64(src[0]) | uint64(src[1])<<8

	case 3:
		val |= uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16

	case 4:
		val |= uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16 | uint64(src[3])<<24

	case 5:
		val |= uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16 | uint64(src[3])<<24 | uint64(src[4])<<32

	case 6:
		val |= uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16 | uint64(src[3])<<24 | uint64(src[4])<<32 | uint64(src[5])<<40

	case 7:
		val |= uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16 | uint64(src[3])<<24 | uint64(src[4])<<32 | uint64(src[5])<<40 | uint64(src[6])<<48

	case 8:
		val |= uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16 | uint64(src[3])<<24 | uint64(src[4])<<32 | uint64(src[5])<<40 | uint64(src[6])<<48 | uint64(src[7])<<56

	default:
		panic("impossible")
	}

	return int64(val)
}

//==========================================================================================
//
//                              serialization Row ===> Tuple
//
//==========================================================================================

// append_serialized_VARBINARY appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_VARBINARY(dest []byte, a *VARBINARY) (res []byte, length uint16) {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	length = uint16(len(a.data_val))

	dest = append(dest, uint8(0xff))   // append dummy byte, to indicates that the field is not NULL (note: if a field length == 0, it means its value is NULL). It can by any value, as it is never used.
	dest = append(dest, a.data_val...) // append data

	return dest, uint16(1 + length)
}

// append_serialized_VARCHAR appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_VARCHAR(dest []byte, a *VARCHAR) (res []byte, length uint16) {
	var (
		val []byte
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	val = a.data_val

	if a.Data_fixlen_flag == true { // for CHAR, we trim all trailing spaces. Spaces will be appended when deserializing.
		for len(val) > 0 {
			i := len(val) - 1 // index of last byte

			if val[i] == ' ' {
				val = val[:i]
			} else {
				break
			}
		}
	}

	length = uint16(len(val))

	if length == 0 { // if empty string, put 0xff byte, which never exists in utf8. This is because field with 0 length means NULL value.
		dest = append(dest, uint8(0xff))
		length = 1
	}

	dest = append(dest, val...) // append data

	return dest, length
}

// append_serialized_BIT appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_BIT(dest []byte, a *BIT) (res []byte, length uint16) {
	var (
		val uint8
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	val = uint8(a.data_val)

	dest = append(dest, val)

	return dest, 1
}

// append_serialized_TINYINT appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_TINYINT(dest []byte, a *TINYINT) (res []byte, length uint16) {
	var (
		val uint8
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	val = uint8(a.data_val)

	dest = append(dest, val)

	return dest, 1
}

// append_serialized_SMALLINT appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_SMALLINT(dest []byte, a *SMALLINT) (res []byte, length uint16) {
	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	dest, length = append_compressed_int64(dest, a.data_val)

	return dest, length
}

// append_serialized_INT appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_INT(dest []byte, a *INT) (res []byte, length uint16) {
	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	dest, length = append_compressed_int64(dest, a.data_val)

	return dest, length
}

// append_serialized_BIGINT appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_BIGINT(dest []byte, a *BIGINT) (res []byte, length uint16) {
	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	dest, length = append_compressed_int64(dest, a.data_val)

	return dest, length
}

// append_serialized_NUMERIC appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_NUMERIC(dest []byte, a *NUMERIC) (res []byte, length uint16) {

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	dest, length = quad.Append_compressed_bytes(dest, a.data_val) // length is always > 0

	return dest, length
}

// append_serialized_FLOAT appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_FLOAT(dest []byte, a *FLOAT) (res []byte, length uint16) {
	var (
		val uint64
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	val = math.Float64bits(a.data_val)

	dest = append(dest, uint8(val), uint8(val>>8), uint8(val>>16), uint8(val>>24), uint8(val>>32), uint8(val>>40), uint8(val>>48), uint8(val>>56))

	return dest, 8
}

// append_serialized_DATE appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_DATE(dest []byte, a *DATE) (res []byte, length uint16) {
	var (
		unix_seconds int64
		val_sec      int64
		val_days     int64
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	// note: struct time.Time has fields
	//          sec   int64
	//          nsec  int32

	unix_seconds = a.data_val.Unix()

	val_sec = unix_seconds - mdat.UNIX_SEC_LOWEST // seconds since 0001.01.01

	val_days = val_sec / mdat.SECONDS_PER_DAY // days since 0001.01.01

	rsql.Assert(val_days >= 0 && val_days < mdat.NUMBER_OF_DAYS_FROM_LOWEST_TO_UPPER_SENTINEL) // val_days is [0 ... 3652058], which fits in uint32 range

	dest = append(dest, uint8(val_days), uint8(val_days>>8), uint8(val_days>>16), uint8(val_days>>24)) // no need to compress val_days, as usual years are > 1900, which takes 4 bytes

	return dest, 4
}

// append_serialized_TIME appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
func append_serialized_TIME(dest []byte, a *TIME) (res []byte, length uint16) {
	var (
		val int64 // duration in nanoseconds since 1900.01.01
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	// compute Duration since 1900.01.01

	val = a.data_val.Time.Sub(mdat.TIME_1900_01_01).Nanoseconds()

	dest, length = append_compressed_int64(dest, val)

	return dest, length
}

// append_serialized_DATETIME appends the serialized value of 'a' to dest.
// If 'a' is NULL, nothing is appended and returned length is 0.
// If 'a' contains an error, the function panics.
//
// Returned length is 4 if no time part, 8 if time part but no ns, 12 if time part and ns.
//
func append_serialized_DATETIME(dest []byte, a *DATETIME) (res []byte, length uint16) {
	var (
		unix_seconds int64
		nanoseconds  int
		val_sec      uint64
		val_ns       uint32
	)

	rsql.Assert(a.data_error == nil)

	if a.data_NULL_flag == true {
		return dest, 0
	}

	// if no time part, perform DATE serialization, which only appends 4 bytes

	if a.data_val.Has_no_time_part() == true {
		dest, length = append_serialized_DATE(dest, (*DATE)(a))

		return dest, length //===> return
	}

	// note: struct time.Time has fields
	//          sec   int64
	//          nsec  int32

	unix_seconds = a.data_val.Unix()
	nanoseconds = a.data_val.Nanosecond()

	val_sec = uint64(unix_seconds)
	val_ns = uint32(nanoseconds)

	dest = append(dest, uint8(val_sec), uint8(val_sec>>8), uint8(val_sec>>16), uint8(val_sec>>24), uint8(val_sec>>32), uint8(val_sec>>40), uint8(val_sec>>48), uint8(val_sec>>56))
	length = 8

	if val_ns != 0 {
		dest = append(dest, uint8(val_ns), uint8(val_ns>>8), uint8(val_ns>>16), uint8(val_ns>>24))
		length = 12
	}

	return dest, length // length is 4, 8, or 12
}

// Write_serialized_row writes row values into dest []byte.
// Length of dest slice must 0.
//
//      The result []byte layout is:
//              ======================= header of offsets ==================----------------------------- data ------------------------------------
//              | offset 0 | offset 1 | ... | offset n-1 | offset sentinel | serialized field 0 | serialized field 1 | ... | serialized field n-1 |
//              ============================================================-----------------------------------------------------------------------
//
//      offset is a uint16 value:
//              - offset i is the start index of the serialized field i.
//              - offset sentinel is the total length of the tuple, in bytes.
//              - if offset of subsequent tuple == offset of current tuple, length of serialized field is 0, which means that value is NULL
//
func Write_serialized_row(dest rsql.Tuple, row rsql.Row) (res rsql.Tuple, rsql_err *rsql.Error) {
	var (
		offset       int
		min_capacity int
		length       uint16
		i            int
	)

	// create header space for offsets

	rsql.Assert(len(dest) == 0)

	offset = (len(row) + 1) * rsql.FIELD_OFFSET_SIZE // space for all field offsets, and for sentinel field offset. Here, the value of 'offset' is the offset of field 0.

	min_capacity = 4 * offset // 4 times the offset is a good enough starting capacity for dest buffer

	if cap(dest) < min_capacity {
		dest = make([]byte, 0, min_capacity)
	}

	dest = dest[:offset] // header that stores all offsets

	// append serialized data and update offsets

	for i = 0; i < len(row); i++ {
		dataslot := row[i]

		switch dataslot.Datatype() {
		case rsql.DATATYPE_VARBINARY:
			dest, length = append_serialized_VARBINARY(dest, dataslot.(*VARBINARY))

		case rsql.DATATYPE_VARCHAR:
			dest, length = append_serialized_VARCHAR(dest, dataslot.(*VARCHAR))

		case rsql.DATATYPE_BIT:
			dest, length = append_serialized_BIT(dest, dataslot.(*BIT))

		case rsql.DATATYPE_TINYINT:
			dest, length = append_serialized_TINYINT(dest, dataslot.(*TINYINT))

		case rsql.DATATYPE_SMALLINT:
			dest, length = append_serialized_SMALLINT(dest, dataslot.(*SMALLINT))

		case rsql.DATATYPE_INT:
			dest, length = append_serialized_INT(dest, dataslot.(*INT))

		case rsql.DATATYPE_BIGINT:
			dest, length = append_serialized_BIGINT(dest, dataslot.(*BIGINT))

		case rsql.DATATYPE_MONEY:
			dest, length = append_serialized_NUMERIC(dest, (*NUMERIC)(dataslot.(*MONEY)))

		case rsql.DATATYPE_NUMERIC:
			dest, length = append_serialized_NUMERIC(dest, dataslot.(*NUMERIC))

		case rsql.DATATYPE_FLOAT:
			dest, length = append_serialized_FLOAT(dest, dataslot.(*FLOAT))

		case rsql.DATATYPE_DATE:
			dest, length = append_serialized_DATE(dest, dataslot.(*DATE))

		case rsql.DATATYPE_TIME:
			dest, length = append_serialized_TIME(dest, dataslot.(*TIME))

		case rsql.DATATYPE_DATETIME:
			dest, length = append_serialized_DATETIME(dest, dataslot.(*DATETIME))

		default:
			panic("unexpected datatype")
		}

		dest[i*rsql.FIELD_OFFSET_SIZE] = uint8(offset)
		dest[i*rsql.FIELD_OFFSET_SIZE+1] = uint8(offset >> 8)

		offset += int(length)

		if offset > cache.PAGE_CANVAS_LENGTH-cache.TUPLE_OFFSET_SIZE { // the tuple is too large to fit even in a single page
			return nil, rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TUPLE_TOO_LARGE, rsql.ERROR_BATCH_ABORT)
		}
	}

	dest[i*rsql.FIELD_OFFSET_SIZE] = uint8(offset) // offset of sentinel field. Store it in tuple header.
	dest[i*rsql.FIELD_OFFSET_SIZE+1] = uint8(offset >> 8)

	rsql.Assert(offset == len(dest))
	//println("tuple length", len(dest))

	return dest, nil
}

//==========================================================================================
//
//                            deserialization Tuple ===> Row
//
//==========================================================================================

// deserialize_to_VARBINARY deserializes src byte slice to non-NULL VARBINARY.
//
func deserialize_to_VARBINARY(a *VARBINARY, src []byte) {

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	a.data_val = append(a.data_val[:0], src[1:]...) // src[1:] skips dummy byte. If dummy byte exists, VARBINARY is not NULL. If no dummy byte (field length == 0), VARBINARY is NULL, but this function is not called.
}

// deserialize_to_VARCHAR deserializes src byte slice to non-NULL VARCHAR.
//
func deserialize_to_VARCHAR(a *VARCHAR, src []byte) {

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	switch {
	case src[0] == 0xff && len(src) == 1: // if empty string, src is the single byte 0xff, which never exists in utf8. This is because field with 0 length already means NULL value.
		a.data_val = a.data_val[:0]

	default:
		a.data_val = append(a.data_val[:0], src...)
	}

	if a.Data_fixlen_flag == true { // if CHAR, append space padding
		a.data_val.Pad_or_truncate_to_precision(a.Data_precision)
	}
}

// deserialize_to_BIT deserializes src byte slice to non-NULL BIT.
//
func deserialize_to_BIT(a *BIT, src []byte) {
	var (
		val uint8
	)

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	val = src[0]

	a.data_val = 0
	if val != 0 {
		a.data_val = 1
	}
}

// deserialize_to_TINYINT deserializes src byte slice to non-NULL TINYINT.
//
func deserialize_to_TINYINT(a *TINYINT, src []byte) {
	var (
		val uint8
	)

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	val = src[0]

	a.data_val = int64(val)
}

// deserialize_to_SMALLINT deserializes src byte slice to non-NULL SMALLINT.
//
func deserialize_to_SMALLINT(a *SMALLINT, src []byte) {
	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	a.data_val = uncompress_int64(src)
}

// deserialize_to_INT deserializes src byte slice to non-NULL INT.
//
func deserialize_to_INT(a *INT, src []byte) {
	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	a.data_val = uncompress_int64(src)
}

// deserialize_to_BIGINT deserializes src byte slice to non-NULL BIGINT.
//
func deserialize_to_BIGINT(a *BIGINT, src []byte) {
	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	a.data_val = uncompress_int64(src)
}

// deserialize_to_NUMERIC deserializes src byte slice to non-NULL NUMERIC.
//
func deserialize_to_NUMERIC(a *NUMERIC, src []byte) {

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	a.data_val = quad.Uncompress_bytes(src)
}

// deserialize_to_FLOAT deserializes src byte slice to non-NULL FLOAT.
//
func deserialize_to_FLOAT(a *FLOAT, src []byte) {
	var (
		val         uint64
		val_float64 float64
	)

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	val = uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16 | uint64(src[3])<<24 | uint64(src[4])<<32 | uint64(src[5])<<40 | uint64(src[6])<<48 | uint64(src[7])<<56

	val_float64 = math.Float64frombits(val)

	a.data_val = val_float64
}

// deserialize_to_DATE deserializes src byte slice to non-NULL DATE.
//
func deserialize_to_DATE(a *DATE, src []byte) {
	var (
		unix_seconds int64
		val_sec      int64
		val_days     uint32
		res          time.Time
	)

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	val_days = uint32(src[0]) | uint32(src[1])<<8 | uint32(src[2])<<16 | uint32(src[3])<<24 // days since 0001.01.01, having been serialized as uint32

	val_sec = int64(val_days) * mdat.SECONDS_PER_DAY // seconds since 0001.01.01

	unix_seconds = val_sec + mdat.UNIX_SEC_LOWEST

	res = time.Unix(unix_seconds, 0).UTC()

	a.data_val.Time = res
}

// deserialize_to_TIME deserializes src byte slice to non-NULL TIME.
//
func deserialize_to_TIME(a *TIME, src []byte) {
	var (
		nanoseconds int64
		res         time.Time
	)

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	nanoseconds = uncompress_int64(src)

	res = time.Unix(mdat.UNIX_SEC_1900_01_01, nanoseconds).UTC()

	a.data_val.Time = res
}

// deserialize_to_DATETIME deserializes src byte slice to non-NULL DATETIME.
//
func deserialize_to_DATETIME(a *DATETIME, src []byte) {
	var (
		val_sec      uint64
		unix_seconds int64
		val_ns       uint32
		nanoseconds  int32
		res          time.Time
	)

	a.data_error = nil // values on disk are never errors
	a.data_NULL_flag = false

	// if src contains 4 bytes, deserialize it as a DATE

	if len(src) == 4 {
		deserialize_to_DATE((*DATE)(a), src)
		return
	}

	// deserialize as DATETIME

	val_sec = uint64(src[0]) | uint64(src[1])<<8 | uint64(src[2])<<16 | uint64(src[3])<<24 | uint64(src[4])<<32 | uint64(src[5])<<40 | uint64(src[6])<<48 | uint64(src[7])<<56
	unix_seconds = int64(val_sec)

	if len(src) == 12 { // ns have been serialized only if ns != 0
		val_ns = uint32(src[8]) | uint32(src[9])<<8 | uint32(src[10])<<16 | uint32(src[11])<<24
		nanoseconds = int32(val_ns)
	}

	res = time.Unix(unix_seconds, int64(nanoseconds)).UTC()

	a.data_val.Time = res
}

func Deserialize_tuple_field(dataslot rsql.IDataslot, tuple rsql.Tuple, field_no uint16) {
	var (
		offset      uint16
		offset_next uint16
		field       []byte
	)

	x := field_no * rsql.FIELD_OFFSET_SIZE

	offset = uint16(tuple[x]) | uint16(tuple[x+1])<<8
	offset_next = uint16(tuple[x+2]) | uint16(tuple[x+3])<<8

	field = tuple[offset:offset_next]

	// check if value is NULL

	if offset_next-offset == 0 { // if nothing is stored at offset position, value is NULL
		dataslot.Set_to_NULL()

		return //===> return
	}

	// field value is stored at offset position. Deserialize it.

	switch dataslot.Datatype() {
	case rsql.DATATYPE_VARBINARY:
		deserialize_to_VARBINARY(dataslot.(*VARBINARY), field)

	case rsql.DATATYPE_VARCHAR:
		deserialize_to_VARCHAR(dataslot.(*VARCHAR), field)

	case rsql.DATATYPE_BIT:
		deserialize_to_BIT(dataslot.(*BIT), field)

	case rsql.DATATYPE_TINYINT:
		deserialize_to_TINYINT(dataslot.(*TINYINT), field)

	case rsql.DATATYPE_SMALLINT:
		deserialize_to_SMALLINT(dataslot.(*SMALLINT), field)

	case rsql.DATATYPE_INT:
		deserialize_to_INT(dataslot.(*INT), field)

	case rsql.DATATYPE_BIGINT:
		deserialize_to_BIGINT(dataslot.(*BIGINT), field)

	case rsql.DATATYPE_MONEY:
		deserialize_to_NUMERIC((*NUMERIC)(dataslot.(*MONEY)), field)

	case rsql.DATATYPE_NUMERIC:
		deserialize_to_NUMERIC(dataslot.(*NUMERIC), field)

	case rsql.DATATYPE_FLOAT:
		deserialize_to_FLOAT(dataslot.(*FLOAT), field)

	case rsql.DATATYPE_DATE:
		deserialize_to_DATE(dataslot.(*DATE), field)

	case rsql.DATATYPE_TIME:
		deserialize_to_TIME(dataslot.(*TIME), field)

	case rsql.DATATYPE_DATETIME:
		deserialize_to_DATETIME(dataslot.(*DATETIME), field)

	default:
		panic("unexpected datatype")
	}
}

// Read_row_from_leaf_page fills in leaf_row dataslots with data from a tuple in page.
//
func Read_row_from_leaf_page(leaf_row rsql.Row, tabledef *rsql.Tabledef, page *cache.Page, tuple_index int) {
	var (
		coldef_count int
		tuple        rsql.Tuple
	)

	rsql.Assert(page.Pg_type() == cache.PAGE_TYPE_LEAF)
	rsql.Assert(tuple_index < page.Pg_tuple_count())

	tuple = page.Pg_tuple(tuple_index)

	coldef_count = len(tabledef.Td_coldefs)
	rsql.Assert(len(leaf_row) == coldef_count)
	rsql.Assert(tuple.Field_count() == coldef_count)

	// fill in dataslots

	for i, dataslot := range leaf_row {
		if dataslot == nil {
			continue
		}
		Deserialize_tuple_field(dataslot, tuple, uint16(i))
	}
}

// Read_entry_from_node_page fills in node_entry dataslots with data from a tuple in page.
//
//        node_entry contains the native key columns, followed by a *BIGINT for storing the target page_no.
//
func Read_entry_from_node_page(node_entry rsql.Row, tabledef *rsql.Tabledef, page *cache.Page, tuple_index int) {
	var (
		nk_count int
		tuple    rsql.Tuple
	)

	rsql.Assert(page.Pg_type() == cache.PAGE_TYPE_NODE)
	rsql.Assert(tuple_index < page.Pg_tuple_count())

	tuple = page.Pg_tuple(tuple_index)

	nk_count = len(tabledef.Td_nk)
	rsql.Assert(len(node_entry) == nk_count+1)     // contains nk fields, and target page_no field
	rsql.Assert(tuple.Field_count() == nk_count+1) //    same

	// fill in dataslots

	for i, dataslot := range node_entry { // also deserialize target page_no
		Deserialize_tuple_field(dataslot, tuple, uint16(i))
	}
}
