package data

const (
	SYSVAR_VARCHAR_PRECISION_MAX = 255 // precision for system variable of type VARCHAR, e.g. _@current_db
)
