package data

import (
	"fmt"
	"math"
	"strconv"
	"time"

	"rsql"
	"rsql/mdat"
	"rsql/quad"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *INT) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_INT)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += uint32(a.data_val)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *INT) String() string {

	if a.data_error != nil {
		return "INT: error"
	}

	if a.data_NULL_flag == true {
		return "INT: null"
	}

	return fmt.Sprintf("INT: %d", a.data_val)
}

func (a *INT) Data_val() int64 {
	if a.data_error == nil && a.data_NULL_flag == false {
		return a.data_val
	}

	return 0
}

// New_INT_NULL creates a new data.INT initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_INT_NULL(kind rsql.Kind_t) *INT {

	dataslot := &INT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_INT,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		data_val: 0,
	}

	return dataslot
}

// New_literal_INT_value creates a new data.INT from a value.
//
func New_literal_INT_value(val int32) *INT {

	dataslot := &INT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_INT,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: int64(val),
	}

	return dataslot
}

// New_literal_INT creates a new data.INT from literal integer string.
// Returns an error if invalid string.
//
func New_literal_INT(s string) (*INT, *rsql.Error) {
	var (
		err error
		val int64
	)

	if val, err = strconv.ParseInt(s, 10, 64); err != nil { // base 10, int64
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_CONVERSION_STRING_TO_INT_FAILED, rsql.ERROR_BATCH_ABORT, s)
	}

	if val > math.MaxInt32 || val < math.MinInt32 {
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
	}

	dataslot := New_literal_INT_value(int32(val))

	return dataslot, nil
}

// New_VARIABLE_INT_value creates a new data.INT, used when we create a new global variable with initial value.
//
// Exactly the same as New_literal_INT_value, except that the dataslot is KIND_VAR_LEAF.
//
// It was used to create variables _@system_user_id, etc. But now, we prefer to use New_VARIABLE_BIGINT_value() instead, as login_id etc in dictionary are 64 bits values.
//
func New_VARIABLE_INT_value(val int32) *INT {

	dataslot := &INT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_INT,
			Data_kind:      rsql.KIND_VAR_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: int64(val),
	}

	return dataslot
}

func Unary_minus_INT(r *INT, a *INT) {
	var (
		a_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	r_val = -a_val

	if r_val > math.MaxInt32 || r_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Add_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val + b_val

	if r_val > math.MaxInt32 || r_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Subtract_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val - b_val

	if r_val > math.MaxInt32 || r_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Multiply_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val * b_val

	if r_val > math.MaxInt32 || r_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Divide_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	// For Go, the result of division is truncated toward zero.
	// SQL Server follows the same behaviour.

	if b_val == 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_DIVIDE_BY_ZERO, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = a_val / b_val

	if r_val > math.MaxInt32 || r_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Modulo_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	// For Go, the result of division is truncated toward zero.
	// SQL Server follows the same behaviour.

	if b_val == 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_MODULO_BY_ZERO, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = a_val % b_val

	if r_val > math.MaxInt32 || r_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = r_val
}

func Bitwise_and_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int32
		b_val int32
		r_val int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = int32(a.data_val)
	b_val = int32(b.data_val)

	r_val = a_val & b_val

	r.data_val = int64(r_val)
}

func Bitwise_or_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int32
		b_val int32
		r_val int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = int32(a.data_val)
	b_val = int32(b.data_val)

	r_val = a_val | b_val

	r.data_val = int64(r_val)
}

func Bitwise_xor_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int32
		b_val int32
		r_val int32
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = int32(a.data_val)
	b_val = int32(b.data_val)

	r_val = a_val ^ b_val

	r.data_val = int64(r_val)
}

func Bitwise_unary_not_INT(r *INT, a *INT) {
	var (
		a_val int32
		r_val int32
	)

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	a_val = int32(a.data_val)

	r_val = ^a_val

	r.data_val = int64(r_val)
}

func Comp_equal_INT(r *BOOLEAN, a *INT, b *INT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val == b.data_val

	r.data_val = r_val
}

func Comp_greater_INT(r *BOOLEAN, a *INT, b *INT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val > b.data_val

	r.data_val = r_val
}

func Comp_less_INT(r *BOOLEAN, a *INT, b *INT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val < b.data_val

	r.data_val = r_val
}

func Comp_greater_equal_INT(r *BOOLEAN, a *INT, b *INT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val >= b.data_val

	r.data_val = r_val
}

func Comp_less_equal_INT(r *BOOLEAN, a *INT, b *INT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val <= b.data_val

	r.data_val = r_val
}

func Comp_not_equal_INT(r *BOOLEAN, a *INT, b *INT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val != b.data_val

	r.data_val = r_val
}

func Is_null_INT(r *BOOLEAN, a *INT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_INT(r *BOOLEAN, a *INT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_INT checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_INT(r *BOOLEAN, a *INT, b_list []*INT) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_INT(r, a, b)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_INT(r *BOOLEAN, a *INT, b_list []*INT) {

	In_list_INT(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_INT(r *INT, a_list []Case_element_t) {
	var (
		elem_val *INT
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*INT)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			r.data_val = elem_val.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

// Cast_INT_to_VARCHAR casts INT to VARCHAR.
// If precision of target is unsufficient, returns an error.
//
func Cast_INT_to_VARCHAR(r *VARCHAR, a *INT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res := strconv.AppendInt(r.data_val[:0], a.data_val, 10)

	if len(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_CAST_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val, r.Data_precision)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

// Cast_INT_to_BIT casts INT to BIT.
// If INT != 0, BIT is set to 1. Else, BIT is set to 0.
//
func Cast_INT_to_BIT(r *BIT, a *INT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = 0
	if a.data_val != 0 {
		r.data_val = 1
	}
}

func Cast_INT_to_TINYINT(r *TINYINT, a *INT) {
	var (
		a_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if a_val > math.MaxUint8 || a_val < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = a_val
}

func Cast_INT_to_SMALLINT(r *SMALLINT, a *INT) {
	var (
		a_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if a_val > math.MaxInt16 || a_val < math.MinInt16 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = a_val
}

func Cast_INT_to_INT(r *INT, a *INT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_INT_to_BIGINT(r *BIGINT, a *INT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_INT_to_NUMERIC(r *NUMERIC, a *INT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.From_int32(&r.data_val, r.Data_precision, r.Data_scale, int32(a.data_val))
}

func Cast_INT_to_FLOAT(r *FLOAT, a *INT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = float64(a.data_val)
}

func Cast_INT_to_DATETIME(r *DATETIME, a *INT) {
	var (
		unix_sec int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val >= mdat.NUMBER_OF_DAYS_FROM_1900_01_01_TO_UPPER_SENTINEL || a.data_val < mdat.NUMBER_OF_DAYS_FROM_1900_01_01_TO_LOWEST {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	unix_sec = mdat.UNIX_SEC_1900_01_01 + a.data_val*mdat.SECONDS_PER_DAY

	t := time.Unix(unix_sec, 0).UTC() // in theory, time.Unix() may return a time < '0001-01-01' or >= '10000-01-01' if unix_sec is too large (negative or positive). But as we have checked that a.data_val must be in proper range, this cannot happen.

	rsql.Assert(t.Before(mdat.TIME_LOWEST) == false && t.Before(mdat.TIME_UPPER_SENTINEL))

	r.data_val.Time = t
}

func Assign_INT(r *INT, a *INT) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

// Assign_INT_value is used by VM to assign value to OUT variables.
//
func Assign_INT_value(r *INT, val int32) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = int64(val)
}

// Set_INT_value_from_int64 assigns a int64 value to INT.
// If overflow, INT will contain an error.
//
func Set_INT_value_from_int64(r *INT, val int64) {

	r.data_error = nil
	r.data_NULL_flag = false

	if val > math.MaxInt32 || val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT, val)
		return
	}

	r.data_val = val
}

func Copy_INT(r *INT, a *INT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_INT(kind rsql.Kind_t, a *INT) *INT {

	return New_INT_NULL(kind)
}
