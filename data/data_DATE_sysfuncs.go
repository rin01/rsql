package data

import (
	"time"
	"unicode/utf8"

	"rsql"
	"rsql/format"
	"rsql/mdat"
)

func Sysfunc_eomonth_DATE(r *DATE, a *DATE, month_duration *INT) {
	var (
		year               int
		month              time.Month
		day                int
		month_duration_val int64
		val                time.Time
		rsql_err           *rsql.Error
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &month_duration.Header) {
		return
	}

	/* operation */

	month_duration_val = month_duration.data_val

	year, month, day = a.data_val.Date()

	if year, month, day, rsql_err = mdat.Add_month_duration(year, month, day, month_duration_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	day = mdat.Last_day_of_month(year, month)

	val = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)

	r.data_val.Time = val
}

func Sysfunc_bomonth_DATE(r *DATE, a *DATE, month_duration *INT) {
	var (
		year               int
		month              time.Month
		day                int
		month_duration_val int64
		val                time.Time
		rsql_err           *rsql.Error
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &month_duration.Header) {
		return
	}

	/* operation */

	month_duration_val = month_duration.data_val

	year, month, day = a.data_val.Date()

	if year, month, day, rsql_err = mdat.Add_month_duration(year, month, day, month_duration_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	day = 1

	val = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)

	r.data_val.Time = val
}

var (
	DFORMAT_1  = []byte(`MM\/dd\/yy`)
	DFORMAT_2  = []byte(`yy\.MM\.dd`)
	DFORMAT_3  = []byte(`dd\/MM\/yy`)
	DFORMAT_4  = []byte(`dd\.MM\.yy`)
	DFORMAT_5  = []byte(`dd\-MM\-yy`)
	DFORMAT_6  = []byte(`dd MMM yy`)
	DFORMAT_7  = []byte(`MMM dd\, yy`)
	DFORMAT_10 = []byte(`MM\-dd\-yy`)
	DFORMAT_11 = []byte(`yy\/MM\/dd`)
	DFORMAT_12 = []byte(`yyMMdd`)

	DFORMAT_0_100 = []byte(`MMM d yyyy`)
	DFORMAT_101   = []byte(`MM\/dd\/yyyy`)
	DFORMAT_102   = []byte(`yyyy\.MM\.dd`)
	DFORMAT_103   = []byte(`dd\/MM\/yyyy`)
	DFORMAT_104   = []byte(`dd\.MM\.yyyy`)
	DFORMAT_105   = []byte(`dd\-MM\-yyyy`)
	DFORMAT_106   = []byte(`dd MMM yyyy`)
	DFORMAT_107   = []byte(`MMM dd\, yyyy`)
	//	DFORMAT_8_108  = []byte(`HH\:mm\:ss`)
	DFORMAT_9_109  = []byte(`MMM d yyyy`)
	DFORMAT_110    = []byte(`MM\-dd\-yyyy`)
	DFORMAT_111    = []byte(`yyyy\/MM\/dd`)
	DFORMAT_112    = []byte(`yyyyMMdd`)
	DFORMAT_13_113 = []byte(`dd MMM yyyy`)
	//	DFORMAT_14_114 = []byte(`HH:mm\:ss\.fff`)
	DFORMAT_20_120 = []byte(`yyyy\-MM\-dd`)
	DFORMAT_21_121 = []byte(`yyyy\-MM\-dd`)
	DFORMAT_126    = []byte(`yyyy\-MM\-dd`)
)

// Sysfunc_convert_DATE_to_VARCHAR formats a DATE into a VARCHAR, with a format style.
//
//      CONVERT ( data_type [ ( length ) ] , date expression [ , style ] )
//
//      NOTE: target may be fixlen, if SQL script was CAST(@mydate as char(50)), which has been transformed by the decorator into CONVERT.
//
func Sysfunc_convert_DATE_to_VARCHAR(r *VARCHAR, a *DATE, b *INT, syslang *SYSLANGUAGE) {
	var (
		format_string []byte
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &syslang.Header) {
		return
	}

	/* operation */

	switch b.data_val {
	case 1:
		format_string = DFORMAT_1
	case 2:
		format_string = DFORMAT_2
	case 3:
		format_string = DFORMAT_3
	case 4:
		format_string = DFORMAT_4
	case 5:
		format_string = DFORMAT_5
	case 6:
		format_string = DFORMAT_6
	case 7:
		format_string = DFORMAT_7
	case 10:
		format_string = DFORMAT_10
	case 11:
		format_string = DFORMAT_11
	case 12:
		format_string = DFORMAT_12
	case 0, 100:
		format_string = DFORMAT_0_100
	case 101:
		format_string = DFORMAT_101
	case 102:
		format_string = DFORMAT_102
	case 103:
		format_string = DFORMAT_103
	case 104:
		format_string = DFORMAT_104
	case 105:
		format_string = DFORMAT_105
	case 106:
		format_string = DFORMAT_106
	case 107:
		format_string = DFORMAT_107
	// case 8, 108:
	//	format_string = DFORMAT_8_108
	case 9, 109:
		format_string = DFORMAT_9_109
	case 110:
		format_string = DFORMAT_110
	case 111:
		format_string = DFORMAT_111
	case 112:
		format_string = DFORMAT_112
	case 13, 113:
		format_string = DFORMAT_13_113
	// case 14, 114:
	//	format_string = DFORMAT_14_114
	case 20, 120:
		format_string = DFORMAT_20_120
	case 21, 121:
		format_string = DFORMAT_21_121
	case 126:
		format_string = DFORMAT_126

	default:
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATE_CONVERT_VARCHAR_UNSUPPORTED_STYLE, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	res := format.Append_formatted_date(r.data_val[:0], a.data_val.Time, format_string, syslang.data_langinfo)

	// check result length

	if len(res) > int(r.Data_precision) && utf8.RuneCount(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATE_CONVERT_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String(), format_string)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

func Sysfunc_datefromparts_DATE(r *DATE, year *INT, month *INT, day *INT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&year.Header, &month.Header, &day.Header) {
		return
	}

	// check values are in proper range

	if (year.data_val >= 1 && year.data_val <= 9999 &&
		month.data_val >= 1 && month.data_val <= 12 &&
		day.data_val >= 1 && day.data_val <= int64(mdat.Last_day_of_month(int(year.data_val), time.Month(month.data_val)))) == false {

		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATE_DATEFROMPARTS_ILLEGAL_ARGS, rsql.ERROR_BATCH_ABORT, year.data_val, month.data_val, day.data_val)
		return
	}

	/* operation */

	res := time.Date(int(year.data_val), time.Month(month.data_val), int(day.data_val), 0, 0, 0, 0, time.UTC)

	r.data_val.Time = res
}

func Sysfunc_isnull_DATE(r *DATE, a *DATE, b *DATE) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
	}
}

func Sysfunc_iif_DATE(r *DATE, a *BOOLEAN, b *DATE, c *DATE) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	r.data_val = c.data_val
}

func Sysfunc_choose_DATE(r *DATE, a *INT, b_list []*DATE) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	r.data_val = elem.data_val
}

func Sysfunc_coalesce_DATE(r *DATE, a_list []*DATE) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = elem.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_DATE(r *BOOLEAN, a *DATE, b *DATE, c *DATE) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	r_val = !a.data_val.Before(b.data_val.Time) && !a.data_val.After(c.data_val.Time)

	r.data_val = r_val
}

func Sysfunc_not_between_DATE(r *BOOLEAN, a *DATE, b *DATE, c *DATE) {

	Sysfunc_between_DATE(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_DATE(r *VARCHAR, a *DATE) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "DATE"...)
}
