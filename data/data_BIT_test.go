package data

import (
	"math"
	"testing"

	"rsql"
)

var (
	samples_BIT = []tst_sample_t{

		{Bitwise_and_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_and_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_and_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_and_BIT, []interface{}{r_BIT, 1, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_and_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_and_BIT, []interface{}{r_BIT, nil, 1}, r_BIT, nil},
		{Bitwise_and_BIT, []interface{}{r_BIT, 1, nil}, r_BIT, nil},
		{Bitwise_and_BIT, []interface{}{r_BIT, nil, nil}, r_BIT, nil},
		{Bitwise_and_BIT, []interface{}{r_BIT, 0, 0}, r_BIT, 0},
		{Bitwise_and_BIT, []interface{}{r_BIT, 0, 1}, r_BIT, 0},
		{Bitwise_and_BIT, []interface{}{r_BIT, 1, 0}, r_BIT, 0},
		{Bitwise_and_BIT, []interface{}{r_BIT, 1, 1}, r_BIT, 1},

		{Bitwise_or_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_or_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_or_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_or_BIT, []interface{}{r_BIT, 1, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_or_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_or_BIT, []interface{}{r_BIT, nil, 1}, r_BIT, nil},
		{Bitwise_or_BIT, []interface{}{r_BIT, 1, nil}, r_BIT, nil},
		{Bitwise_or_BIT, []interface{}{r_BIT, nil, nil}, r_BIT, nil},
		{Bitwise_or_BIT, []interface{}{r_BIT, 0, 0}, r_BIT, 0},
		{Bitwise_or_BIT, []interface{}{r_BIT, 0, 1}, r_BIT, 1},
		{Bitwise_or_BIT, []interface{}{r_BIT, 1, 0}, r_BIT, 1},
		{Bitwise_or_BIT, []interface{}{r_BIT, 1, 1}, r_BIT, 1},

		{Bitwise_xor_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_xor_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_xor_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_xor_BIT, []interface{}{r_BIT, 1, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_xor_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_xor_BIT, []interface{}{r_BIT, nil, 1}, r_BIT, nil},
		{Bitwise_xor_BIT, []interface{}{r_BIT, 1, nil}, r_BIT, nil},
		{Bitwise_xor_BIT, []interface{}{r_BIT, nil, nil}, r_BIT, nil},
		{Bitwise_xor_BIT, []interface{}{r_BIT, 0, 0}, r_BIT, 0},
		{Bitwise_xor_BIT, []interface{}{r_BIT, 0, 1}, r_BIT, 1},
		{Bitwise_xor_BIT, []interface{}{r_BIT, 1, 0}, r_BIT, 1},
		{Bitwise_xor_BIT, []interface{}{r_BIT, 1, 1}, r_BIT, 0},

		{Bitwise_unary_not_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Bitwise_unary_not_BIT, []interface{}{r_BIT, nil}, r_BIT, nil},
		{Bitwise_unary_not_BIT, []interface{}{r_BIT, 0}, r_BIT, 1},
		{Bitwise_unary_not_BIT, []interface{}{r_BIT, 1}, r_BIT, 0},

		{Comp_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, 0, 0}, r_BOOLEAN, true},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, 0, 1}, r_BOOLEAN, false},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, false},
		{Comp_equal_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, true},

		{Comp_greater_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, 0, 0}, r_BOOLEAN, false},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, 0, 1}, r_BOOLEAN, false},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, true},
		{Comp_greater_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, false},

		{Comp_less_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, 0, 0}, r_BOOLEAN, false},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, 0, 1}, r_BOOLEAN, true},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, false},
		{Comp_less_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, false},

		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, 0, 0}, r_BOOLEAN, true},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, 0, 1}, r_BOOLEAN, false},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, true},
		{Comp_greater_equal_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, true},

		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, 0, 0}, r_BOOLEAN, true},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, 0, 1}, r_BOOLEAN, true},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, false},
		{Comp_less_equal_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, true},

		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, 0, 0}, r_BOOLEAN, false},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, 0, 1}, r_BOOLEAN, true},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, true},
		{Comp_not_equal_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, false},

		{Comp_for_sort_BIT, []interface{}{&r_compsort, nil, 1}, &r_compsort, Compsort_t(-1)},
		{Comp_for_sort_BIT, []interface{}{&r_compsort, 1, nil}, &r_compsort, Compsort_t(1)},
		{Comp_for_sort_BIT, []interface{}{&r_compsort, nil, nil}, &r_compsort, Compsort_t(0)},
		{Comp_for_sort_BIT, []interface{}{&r_compsort, 0, 0}, &r_compsort, Compsort_t(0)},
		{Comp_for_sort_BIT, []interface{}{&r_compsort, 0, 1}, &r_compsort, Compsort_t(-1)},
		{Comp_for_sort_BIT, []interface{}{&r_compsort, 1, 0}, &r_compsort, Compsort_t(1)},
		{Comp_for_sort_BIT, []interface{}{&r_compsort, 1, 1}, &r_compsort, Compsort_t(0)},
		{Comp_for_sort_BIT, []interface{}{&r_compsort, nil, 0}, &r_compsort, Compsort_t(-1)},

		{Is_null_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Is_null_BIT, []interface{}{r_BOOLEAN, nil}, r_BOOLEAN, true},
		{Is_null_BIT, []interface{}{r_BOOLEAN, 0}, r_BOOLEAN, false},
		{Is_null_BIT, []interface{}{r_BOOLEAN, 1}, r_BOOLEAN, false},

		{Is_not_null_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Is_not_null_BIT, []interface{}{r_BOOLEAN, nil}, r_BOOLEAN, false},
		{Is_not_null_BIT, []interface{}{r_BOOLEAN, 0}, r_BOOLEAN, true},
		{Is_not_null_BIT, []interface{}{r_BOOLEAN, 1}, r_BOOLEAN, true},

		{In_list_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{In_list_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{In_list_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{In_list_BIT, []interface{}{r_BOOLEAN, nil}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, nil, nil, nil}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, nil, nil, 1}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1}, r_BOOLEAN, false},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, true},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, false},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, 0, 1, 0}, r_BOOLEAN, true},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 1, 0}, r_BOOLEAN, true},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 1, nil}, r_BOOLEAN, true},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING, 1, 0}, r_BOOLEAN, true},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, true},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING, 0, 0}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING, 0, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, nil, nil}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 0, 0}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, 0, 0, nil}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 0, nil}, r_BOOLEAN, nil},
		{In_list_BIT, []interface{}{r_BOOLEAN, 1, 0, 0, 0}, r_BOOLEAN, false},

		{Not_in_list_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, 1, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, nil}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, nil, nil, nil}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, nil, nil, 1}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, nil, 1}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1}, r_BOOLEAN, true},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, 1}, r_BOOLEAN, false},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, 0}, r_BOOLEAN, true},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, 0, 1, 0}, r_BOOLEAN, false},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 1, 0}, r_BOOLEAN, false},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 1, nil}, r_BOOLEAN, false},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING, 1, 0}, r_BOOLEAN, false},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 1, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, false},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING, 0, 0}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, rsql.ERROR_FOR_TESTING, 0, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, nil}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, nil, nil}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 0, 0}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, 0, 0, nil}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, nil, 0, nil}, r_BOOLEAN, nil},
		{Not_in_list_BIT, []interface{}{r_BOOLEAN, 1, 0, 0, 0}, r_BOOLEAN, true},

		{Case_BIT, []interface{}{r_BIT}, r_BIT, nil},
		{Case_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Case_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Case_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Case_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING}, r_BIT, nil},
		{Case_BIT, []interface{}{r_BIT, nil, nil}, r_BIT, nil},
		{Case_BIT, []interface{}{r_BIT, nil, 1}, r_BIT, nil},
		{Case_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, false, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, nil, nil, false, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, nil, 1, false, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, false, rsql.ERROR_FOR_TESTING, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, false, rsql.ERROR_FOR_TESTING, true, 1, true, rsql.ERROR_FOR_TESTING}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, nil, 1, rsql.ERROR_FOR_TESTING, 0, true, 1, true, 0}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Case_BIT, []interface{}{r_BIT, false, rsql.ERROR_FOR_TESTING, false, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, false, nil, false, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, false, 1, false, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, false, 1, nil, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, false, rsql.ERROR_FOR_TESTING, nil, rsql.ERROR_FOR_TESTING, true, 1, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, false, nil, nil, nil, true, 1, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, true, rsql.ERROR_FOR_TESTING, false, 0, true, 1, true, 0}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Case_BIT, []interface{}{r_BIT, true, nil, false, 0, true, 1, true, 0}, r_BIT, nil},
		{Case_BIT, []interface{}{r_BIT, true, 1, false, 0, true, 1, true, 0}, r_BIT, 1},
		{Case_BIT, []interface{}{r_BIT, false, 1, true, 0, false, 0}, r_BIT, 0},

		{Cast_BIT_to_VARCHAR, []interface{}{r_VARCHAR_1, rsql.ERROR_FOR_TESTING}, r_VARCHAR_1, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_VARCHAR, []interface{}{r_VARCHAR_1, nil}, r_VARCHAR_1, nil},
		{Cast_BIT_to_VARCHAR, []interface{}{r_VARCHAR_1, 0}, r_VARCHAR_1, "0"},
		{Cast_BIT_to_VARCHAR, []interface{}{r_VARCHAR_1, 1}, r_VARCHAR_1, "1"},
		{Cast_BIT_to_VARCHAR, []interface{}{r_VARCHAR_10_fix, 1}, r_VARCHAR_10_fix, "1         "},
		{Cast_BIT_to_VARCHAR, []interface{}{r_VARCHAR_10_fix, 0}, r_VARCHAR_10_fix, "0         "},

		{Cast_BIT_to_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_BIT, []interface{}{r_BIT, nil}, r_BIT, nil},
		{Cast_BIT_to_BIT, []interface{}{r_BIT, 0}, r_BIT, 0},
		{Cast_BIT_to_BIT, []interface{}{r_BIT, 1}, r_BIT, 1},

		{Cast_BIT_to_TINYINT, []interface{}{r_TINYINT, rsql.ERROR_FOR_TESTING}, r_TINYINT, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_TINYINT, []interface{}{r_TINYINT, nil}, r_TINYINT, nil},
		{Cast_BIT_to_TINYINT, []interface{}{r_TINYINT, 0}, r_TINYINT, 0},
		{Cast_BIT_to_TINYINT, []interface{}{r_TINYINT, 1}, r_TINYINT, 1},

		{Cast_BIT_to_SMALLINT, []interface{}{r_SMALLINT, rsql.ERROR_FOR_TESTING}, r_SMALLINT, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_SMALLINT, []interface{}{r_SMALLINT, nil}, r_SMALLINT, nil},
		{Cast_BIT_to_SMALLINT, []interface{}{r_SMALLINT, 0}, r_SMALLINT, 0},
		{Cast_BIT_to_SMALLINT, []interface{}{r_SMALLINT, 1}, r_SMALLINT, 1},

		{Cast_BIT_to_INT, []interface{}{r_INT, rsql.ERROR_FOR_TESTING}, r_INT, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_INT, []interface{}{r_INT, nil}, r_INT, nil},
		{Cast_BIT_to_INT, []interface{}{r_INT, 0}, r_INT, 0},
		{Cast_BIT_to_INT, []interface{}{r_INT, 1}, r_INT, 1},

		{Cast_BIT_to_BIGINT, []interface{}{r_BIGINT, rsql.ERROR_FOR_TESTING}, r_BIGINT, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_BIGINT, []interface{}{r_BIGINT, nil}, r_BIGINT, nil},
		{Cast_BIT_to_BIGINT, []interface{}{r_BIGINT, 0}, r_BIGINT, 0},
		{Cast_BIT_to_BIGINT, []interface{}{r_BIGINT, 1}, r_BIGINT, 1},

		{Cast_BIT_to_NUMERIC, []interface{}{r_NUMERIC_12_2, rsql.ERROR_FOR_TESTING}, r_NUMERIC_12_2, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_NUMERIC, []interface{}{r_NUMERIC_12_2, nil}, r_NUMERIC_12_2, nil},
		{Cast_BIT_to_NUMERIC, []interface{}{r_NUMERIC_12_2, 0}, r_NUMERIC_12_2, "0.00"},
		{Cast_BIT_to_NUMERIC, []interface{}{r_NUMERIC_12_2, 1}, r_NUMERIC_12_2, "  1.00  "},
		{Cast_BIT_to_NUMERIC, []interface{}{r_NUMERIC_3_2, 0}, r_NUMERIC_3_2, "0.00"},
		{Cast_BIT_to_NUMERIC, []interface{}{r_NUMERIC_3_2, 1}, r_NUMERIC_3_2, "1.00"},
		{Cast_BIT_to_NUMERIC, []interface{}{r_NUMERIC_2_2, math.MaxUint8}, r_NUMERIC_2_2, rsql.ERROR_SQLDATA_NUMERIC_OVERFLOW},

		{Cast_BIT_to_FLOAT, []interface{}{r_FLOAT, rsql.ERROR_FOR_TESTING}, r_FLOAT, rsql.ERROR_FOR_TESTING},
		{Cast_BIT_to_FLOAT, []interface{}{r_FLOAT, nil}, r_FLOAT, nil},
		{Cast_BIT_to_FLOAT, []interface{}{r_FLOAT, 0}, r_FLOAT, 0.},
		{Cast_BIT_to_FLOAT, []interface{}{r_FLOAT, 1}, r_FLOAT, 1.},

		{Sysfunc_isnumeric_BIT, []interface{}{r_INT, rsql.ERROR_FOR_TESTING}, r_INT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_isnumeric_BIT, []interface{}{r_INT, nil}, r_INT, 0},
		{Sysfunc_isnumeric_BIT, []interface{}{r_INT, 0}, r_INT, 1},
		{Sysfunc_isnumeric_BIT, []interface{}{r_INT, 1}, r_INT, 1},

		{Sysfunc_isnull_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, nil, nil}, r_BIT, nil},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, nil, 1}, r_BIT, 1},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, nil, 0}, r_BIT, 0},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, 1, rsql.ERROR_FOR_TESTING}, r_BIT, 1},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, 0, rsql.ERROR_FOR_TESTING}, r_BIT, 0},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, 1, nil}, r_BIT, 1},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, 1, 0}, r_BIT, 1},
		{Sysfunc_isnull_BIT, []interface{}{r_BIT, 0, 1}, r_BIT, 0},

		{Sysfunc_iif_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1, 0}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, nil, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, nil, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, nil, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 1, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 1, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 1, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 1, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 0, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 0, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 0, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, nil, 0, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, rsql.ERROR_FOR_TESTING, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, rsql.ERROR_FOR_TESTING, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, rsql.ERROR_FOR_TESTING, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, nil, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, nil, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, nil, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 1, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 1, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 1, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 1, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 0, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 0, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 0, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, false, 0, 0}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, rsql.ERROR_FOR_TESTING, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, rsql.ERROR_FOR_TESTING, 1}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, rsql.ERROR_FOR_TESTING, 0}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, nil, rsql.ERROR_FOR_TESTING}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, nil, nil}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, nil, 1}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, nil, 0}, r_BIT, nil},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 1, rsql.ERROR_FOR_TESTING}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 1, nil}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 1, 1}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 1, 0}, r_BIT, 1},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 0, rsql.ERROR_FOR_TESTING}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 0, nil}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 0, 1}, r_BIT, 0},
		{Sysfunc_iif_BIT, []interface{}{r_BIT, true, 0, 0}, r_BIT, 0},

		{Sysfunc_choose_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil, nil, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, 1, 0, 0}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, nil}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 0}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 0, 1}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 1}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 0, 1}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 1}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 1, 1}, r_BIT, 1},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 2, 1}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, math.MinInt32, 1}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, math.MaxInt32, 1}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0}, r_BIT, 1},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 5, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0}, r_BIT, 0},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 10, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0}, r_BIT, 0},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 4, 1, 0, 1, nil, 0, 0, 0, 0, 1, 0}, r_BIT, nil},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 4, 1, 0, 1, rsql.ERROR_FOR_TESTING, 0, 0, 0, 0, 1, 0}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_choose_BIT, []interface{}{r_BIT, 4, nil, nil, nil, rsql.ERROR_FOR_TESTING, 0, 0, 0, 0, 1, 0}, r_BIT, rsql.ERROR_FOR_TESTING},

		{Sysfunc_coalesce_BIT, []interface{}{r_BIT}, r_BIT, nil},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, nil, nil, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, nil, nil, rsql.ERROR_FOR_TESTING, nil}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, nil, nil, nil}, r_BIT, nil},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, nil, 1, nil, 0, 0}, r_BIT, 1},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, 1, nil, nil, nil, 0, 0}, r_BIT, 1},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, nil, nil, 1}, r_BIT, 1},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, nil, 1, nil, 0, rsql.ERROR_FOR_TESTING, 0, rsql.ERROR_FOR_TESTING}, r_BIT, 1},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, 1, nil, nil, rsql.ERROR_FOR_TESTING, nil, 0, 0}, r_BIT, 1},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, 1, rsql.ERROR_FOR_TESTING}, r_BIT, 1},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, 1, nil, nil, rsql.ERROR_FOR_TESTING, nil, 0, 0}, r_BIT, 1},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, nil, nil, rsql.ERROR_FOR_TESTING, 1, nil, 0, rsql.ERROR_FOR_TESTING, 0, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
		{Sysfunc_coalesce_BIT, []interface{}{r_BIT, rsql.ERROR_FOR_TESTING, nil, nil, 1, nil, 0, rsql.ERROR_FOR_TESTING, 0, rsql.ERROR_FOR_TESTING}, r_BIT, rsql.ERROR_FOR_TESTING},
	}
)

func Test_operators_and_functions_for_BIT(t *testing.T) {

	// check all the samples

	for i, sample := range samples_BIT {

		tst_function_tester(sample.function, sample.operands...) // run the function to test

		if tst_function_result_checker(sample.result, sample.result_check) == false { // check result
			t.Fatalf("--------- Failure at samples[%d] for %s -------", i, sample.String())
		}
	}

}
