package data

import (
	"fmt"
	"strconv"

	"rsql"
	"rsql/quad"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *BIT) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_BIT)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += uint32(a.data_val)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *BIT) String() string {

	if a.data_error != nil {
		return "BIT: error"
	}

	if a.data_NULL_flag == true {
		return "BIT: null"
	}

	return fmt.Sprintf("BIT: %d", a.data_val)
}

// New_BIT_NULL creates a new data.BIT initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_BIT_NULL(kind rsql.Kind_t) *BIT {

	dataslot := &BIT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_BIT,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		data_val: 0,
	}

	return dataslot
}

// New_literal_BIT_value creates a new data.BIT from a value.
// If val == 0, bit value is set to 0. Else, it is set to 1.
//
func New_literal_BIT_value(val uint8) *BIT {

	dataslot := &BIT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_BIT,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: 0,
	}

	if val != 0 {
		dataslot.data_val = 1
	}

	return dataslot
}

func Bitwise_and_BIT(r *BIT, a *BIT, b *BIT) {
	var (
		a_val uint8
		b_val uint8
		r_val uint8
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = uint8(a.data_val)
	b_val = uint8(b.data_val)

	r_val = a_val & b_val // result always 0 or 1

	r.data_val = int64(r_val)
}

func Bitwise_or_BIT(r *BIT, a *BIT, b *BIT) {
	var (
		a_val uint8
		b_val uint8
		r_val uint8
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = uint8(a.data_val)
	b_val = uint8(b.data_val)

	r_val = a_val | b_val // result always 0 or 1

	r.data_val = int64(r_val)
}

func Bitwise_xor_BIT(r *BIT, a *BIT, b *BIT) {
	var (
		a_val uint8
		b_val uint8
		r_val uint8
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = uint8(a.data_val)
	b_val = uint8(b.data_val)

	r_val = a_val ^ b_val // result always 0 or 1

	r.data_val = int64(r_val)
}

func Bitwise_unary_not_BIT(r *BIT, a *BIT) {
	var (
		a_val uint8
		r_val uint8
	)

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	a_val = uint8(a.data_val)

	r_val = ^a_val & 0x01

	r.data_val = int64(r_val)
}

func Comp_equal_BIT(r *BOOLEAN, a *BIT, b *BIT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val == b.data_val

	r.data_val = r_val
}

func Comp_greater_BIT(r *BOOLEAN, a *BIT, b *BIT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val > b.data_val

	r.data_val = r_val
}

func Comp_less_BIT(r *BOOLEAN, a *BIT, b *BIT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val < b.data_val

	r.data_val = r_val
}

func Comp_greater_equal_BIT(r *BOOLEAN, a *BIT, b *BIT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val >= b.data_val

	r.data_val = r_val
}

func Comp_less_equal_BIT(r *BOOLEAN, a *BIT, b *BIT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val <= b.data_val

	r.data_val = r_val
}

func Comp_not_equal_BIT(r *BOOLEAN, a *BIT, b *BIT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val != b.data_val

	r.data_val = r_val
}

func Is_null_BIT(r *BOOLEAN, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_BIT(r *BOOLEAN, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_BIT checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_BIT(r *BOOLEAN, a *BIT, b_list []*BIT) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_BIT(r, a, b)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_BIT(r *BOOLEAN, a *BIT, b_list []*BIT) {

	In_list_BIT(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_BIT(r *BIT, a_list []Case_element_t) {
	var (
		elem_val *BIT
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*BIT)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			r.data_val = elem_val.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

// Cast_BIT_to_VARCHAR casts BIT to VARCHAR.
// If precision of target is unsufficient, returns an error.
//
func Cast_BIT_to_VARCHAR(r *VARCHAR, a *BIT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res := strconv.AppendInt(r.data_val[:0], a.data_val, 10)

	if len(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIT_CAST_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val, r.Data_precision)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

func Cast_BIT_to_BIT(r *BIT, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_BIT_to_TINYINT(r *TINYINT, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_BIT_to_SMALLINT(r *SMALLINT, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_BIT_to_INT(r *INT, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_BIT_to_BIGINT(r *BIGINT, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_BIT_to_NUMERIC(r *NUMERIC, a *BIT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.From_int32(&r.data_val, r.Data_precision, r.Data_scale, int32(a.data_val))

}

func Cast_BIT_to_FLOAT(r *FLOAT, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = float64(a.data_val)
}

func Assign_BIT(r *BIT, a *BIT) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

func (r *BIT) Set_to_0() {
	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = 0
}

func (r *BIT) Set_to_1() {
	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = 1
}

func Copy_BIT(r *BIT, a *BIT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_BIT(kind rsql.Kind_t, a *BIT) *BIT {

	return New_BIT_NULL(kind)
}
