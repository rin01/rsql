package data

import (
	"fmt"
	"math"
	"strconv"
	"time"

	"rsql"
	"rsql/mdat"
	"rsql/quad"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *BIGINT) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_BIGINT)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += uint32(a.data_val)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(a.data_val >> 32)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *BIGINT) String() string {

	if a.data_error != nil {
		return "BIGINT: error"
	}

	if a.data_NULL_flag == true {
		return "BIGINT: null"
	}

	return fmt.Sprintf("BIGINT: %d", a.data_val)
}

func (a *BIGINT) Data_val() int64 {
	if a.data_error == nil && a.data_NULL_flag == false {
		return a.data_val
	}

	return 0
}

// New_BIGINT_NULL creates a new data.BIGINT initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_BIGINT_NULL(kind rsql.Kind_t) *BIGINT {

	dataslot := &BIGINT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_BIGINT,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		data_val: 0,
	}

	return dataslot
}

// New_literal_BIGINT_value creates a new data.BIGINT from a value.
//
func New_literal_BIGINT_value(val int64) *BIGINT {

	dataslot := &BIGINT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_BIGINT,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: val,
	}

	return dataslot
}

// New_VARIABLE_BIGINT_value creates a new data.BIGINT, used when we create a new global variable with initial value.
//
// Exactly the same as New_literal_BIGINT_value, except that the dataslot is KIND_VAR_LEAF.
//
// Used to create variables _@system_user_id, etc.
//
func New_VARIABLE_BIGINT_value(val int64) *BIGINT {

	dataslot := &BIGINT{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_BIGINT,
			Data_kind:      rsql.KIND_VAR_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: val,
	}

	return dataslot
}

func Unary_minus_BIGINT(r *BIGINT, a *BIGINT) {
	var (
		a_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if a_val == math.MinInt64 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = -a_val

	r.data_val = r_val
}

func Add_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case b_val > 0: // if b > 0, check that a+b won't go beyond MaxInt64
		if a_val > math.MaxInt64-b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}

	case b_val < 0: // if b < 0, check that a+b won't go below MinInt64
		if a_val < math.MinInt64-b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}
	}

	r_val = a_val + b_val

	r.data_val = r_val
}

func Subtract_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	switch {
	case b_val > 0: // if b > 0, check that a-b won't go below MinInt64
		if a_val < math.MinInt64+b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}

	case b_val < 0: // if b < 0, check that a-b won't go beyond MaxInt64
		if a_val > math.MaxInt64+b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}
	}

	r_val = a_val - b_val

	r.data_val = r_val
}

func Multiply_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	// note: -5/2 = -2        5/-2 = -2

	switch {
	case a_val > 0:
		switch {
		case b_val >= 0: /*=== a_val > 0, b_val >= 0 ===*/
			if b_val > math.MaxInt64/a_val { // pass the test:      1 * MaxInt64      MaxInt64 * 1      1 * 0      MaxInt64 * 0
				r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
				return
			}

		default: /*=== a_val > 0, b_val < 0 ===*/
			if b_val < math.MinInt64/a_val { // pass the test:      1 * MinInt64      MaxInt64 * -1
				r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
				return
			}
		}

	default:
		switch {
		case b_val > 0: /*=== a_val <= 0, b_val > 0 ===*/
			if a_val < math.MinInt64/b_val { // pass the test:      0 * MaxInt64      -1 * MaxInt64      MinInt64 * 1
				r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
				return
			}

		default: /*=== a_val <= 0, b_val <= 0 ===*/
			if a_val != 0 && b_val < math.MaxInt64/a_val { // pass the test:      0 * 0      0 * 1      0 * MinInt64      -1 * -Maxint64      -MaxInt64 * -1      MinInt64 * 0        NOTE:   -1 * MinIn64  and  MinInt64 * -1  FAIL as expected !
				r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
				return
			}
		}
	}

	r_val = a_val * b_val

	r.data_val = r_val
}

func Divide_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	// For Go, the result of division is truncated toward zero.
	// SQL Server follows the same behaviour.

	if b_val == 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_DIVIDE_BY_ZERO, rsql.ERROR_BATCH_ABORT)
		return
	}

	if a_val == math.MinInt64 && b_val == -1 { // See CERT "INT32-C. Ensure that operations on signed integers do not result in overflow". In Go specifications, math.MinInt64/-1 == math.MinInt64
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = a_val / b_val

	r.data_val = r_val
}

func Modulo_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	// For Go, the result of division is truncated toward zero.
	// SQL Server follows the same behaviour.

	if b_val == 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_MODULO_BY_ZERO, rsql.ERROR_BATCH_ABORT)
		return
	}

	if a_val == math.MinInt64 && b_val == -1 { // See CERT "INT32-C. Ensure that operations on signed integers do not result in overflow". In Go specifications, math.MinInt64/-1 == math.MinInt64 and remainder == 0
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = a_val % b_val

	r.data_val = r_val
}

func Bitwise_and_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val & b_val

	r.data_val = r_val
}

func Bitwise_or_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val | b_val

	r.data_val = r_val
}

func Bitwise_xor_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	r_val = a_val ^ b_val

	r.data_val = r_val
}

func Bitwise_unary_not_BIGINT(r *BIGINT, a *BIGINT) {
	var (
		a_val int64
		r_val int64
	)

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	a_val = a.data_val

	r_val = ^a_val

	r.data_val = r_val
}

func Comp_equal_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val == b.data_val

	r.data_val = r_val
}

func Comp_greater_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val > b.data_val

	r.data_val = r_val
}

func Comp_less_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val < b.data_val

	r.data_val = r_val
}

func Comp_greater_equal_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val >= b.data_val

	r.data_val = r_val
}

func Comp_less_equal_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val <= b.data_val

	r.data_val = r_val
}

func Comp_not_equal_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	r_val = a.data_val != b.data_val

	r.data_val = r_val
}

func Is_null_BIGINT(r *BOOLEAN, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_BIGINT(r *BOOLEAN, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_BIGINT checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_BIGINT(r *BOOLEAN, a *BIGINT, b_list []*BIGINT) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_BIGINT(r, a, b)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_BIGINT(r *BOOLEAN, a *BIGINT, b_list []*BIGINT) {

	In_list_BIGINT(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_BIGINT(r *BIGINT, a_list []Case_element_t) {
	var (
		elem_val *BIGINT
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*BIGINT)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			r.data_val = elem_val.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

// Cast_BIGINT_to_VARCHAR casts BIGINT to VARCHAR.
// If precision of target is unsufficient, returns an error.
//
func Cast_BIGINT_to_VARCHAR(r *VARCHAR, a *BIGINT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res := strconv.AppendInt(r.data_val[:0], a.data_val, 10)

	if len(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_CAST_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val, r.Data_precision)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

// Cast_BIGINT_to_BIT casts BIGINT to BIT.
// If BIGINT != 0, BIT is set to 1. Else, BIT is set to 0.
//
func Cast_BIGINT_to_BIT(r *BIT, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = 0
	if a.data_val != 0 {
		r.data_val = 1
	}
}

func Cast_BIGINT_to_TINYINT(r *TINYINT, a *BIGINT) {
	var (
		a_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if a_val > math.MaxUint8 || a_val < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = a_val
}

func Cast_BIGINT_to_SMALLINT(r *SMALLINT, a *BIGINT) {
	var (
		a_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if a_val > math.MaxInt16 || a_val < math.MinInt16 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = a_val
}

func Cast_BIGINT_to_INT(r *INT, a *BIGINT) {
	var (
		a_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if a_val > math.MaxInt32 || a_val < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_CAST_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	r.data_val = a_val
}

func Cast_BIGINT_to_BIGINT(r *BIGINT, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_BIGINT_to_NUMERIC(r *NUMERIC, a *BIGINT) {
	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_error = quad.From_int64(&r.data_val, r.Data_precision, r.Data_scale, a.data_val)
}

func Cast_BIGINT_to_FLOAT(r *FLOAT, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = float64(a.data_val) // some loss of precision, as int64 has more significant digits than float64
}

func Cast_BIGINT_to_DATETIME(r *DATETIME, a *BIGINT) {
	var (
		unix_sec int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val >= mdat.NUMBER_OF_DAYS_FROM_1900_01_01_TO_UPPER_SENTINEL || a.data_val < mdat.NUMBER_OF_DAYS_FROM_1900_01_01_TO_LOWEST {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	unix_sec = mdat.UNIX_SEC_1900_01_01 + a.data_val*mdat.SECONDS_PER_DAY

	t := time.Unix(unix_sec, 0).UTC() // in theory, time.Unix() may return a time < '0001-01-01' or >= '10000-01-01' if unix_sec is too large (negative or positive). But as we have checked that a.data_val must be in proper range, this cannot happen.

	rsql.Assert(t.Before(mdat.TIME_LOWEST) == false && t.Before(mdat.TIME_UPPER_SENTINEL))

	r.data_val.Time = t
}

func Assign_BIGINT(r *BIGINT, a *BIGINT) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

// Assign_BIGINT_value is used by VM to assign value to OUT variables.
// Also used by btree module to assign page_no to a page entry row.
//
func Assign_BIGINT_value(r *BIGINT, val int64) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = val
}

// Set_BIGINT_value_from_int64 assigns a int64 value to BIGINT.
//
func Set_BIGINT_value_from_int64(r *BIGINT, val int64) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = val
}

func Copy_BIGINT(r *BIGINT, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_BIGINT(kind rsql.Kind_t, a *BIGINT) *BIGINT {

	return New_BIGINT_NULL(kind)
}
