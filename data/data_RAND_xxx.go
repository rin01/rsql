package data

import (
	"math"
	"math/rand"
	"strconv"
	"time"

	"rsql"
	"rsql/mdat"
	"rsql/quad"
)

const (
	RANDOM_CONSONNANTS_UPPER = "BCDFGHJKLMNPQRSTVWXZ" // must be ascii, because the function Sysfunc_random_VARCHAR expects it
	RANDOM_CONSONNANTS       = "bcdfghjklmnpqrstvwxz" // same
	RANDOM_VOWELS_UPPER      = "AEIOU"                // same
	RANDOM_VOWELS            = "aeiou"                // same
)

func Sysfunc_random_VARCHAR(r *VARCHAR, lmin *INT, lmax *INT) {
	var (
		length_min    uint16
		length_max    uint16
		delta         uint16
		length        uint16
		c             uint8
		k             int32
		s             string
		doubling_flag bool // true when sequence of two consonnants or vowels occurs
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&lmin.Header, &lmax.Header) {
		return
	}

	/* operation */

	length_min = uint16(lmin.data_val)
	length_max = uint16(lmax.data_val)

	rsql.Assert(r.Data_precision == length_max && length_min > 0 && length_max > 0 && length_min <= length_max)

	if length_max != length_min {
		delta = uint16(float64(length_max-length_min+1) * rand.Float64())
	}

	length = length_min + delta
	if length > r.Data_precision { // should never happen
		length = r.Data_precision
	}

	r.data_val = r.data_val[:0]

	s = RANDOM_CONSONNANTS_UPPER // first character is uppercase
	if rand.Float32() < 0.5 {
		s = RANDOM_VOWELS_UPPER
	}

	for i := 0; i < int(length); i++ {
		k = rand.Int31n(int32(len(s)))
		c = s[k] // ascii char

		switch s {
		case RANDOM_VOWELS, RANDOM_VOWELS_UPPER:
			s = RANDOM_CONSONNANTS
		default:
			s = RANDOM_VOWELS
		}

		switch doubling_flag {
		case false:
			if rand.Float32() < 0.1 { // sometimes, switch back to have sequence of consonnants or vowels
				switch s {
				case RANDOM_VOWELS:
					s = RANDOM_CONSONNANTS
				default:
					s = RANDOM_VOWELS
				}

				doubling_flag = true
			}

		default: // true
			doubling_flag = false // to avoid consonnants or vowels sequence with more than two characters
		}

		r.data_val = append(r.data_val, c)
	}

}

func Sysfunc_random_INT(r *INT, a *INT, b *INT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	if a_val == b_val {
		r.data_val = a_val
		return
	}

	if b_val < a_val {
		a_val, b_val = b_val, a_val
	}

	r_val = a_val + rand.Int63n(b_val-a_val+1)

	r.data_val = r_val
}

func Sysfunc_random_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {
	var (
		a_val   int64
		b_val   int64
		delta_1 int64
		r_val   int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	if a_val == b_val {
		r.data_val = a_val
		return
	}

	if b_val < a_val {
		a_val, b_val = b_val, a_val
	}

	delta_1 = b_val - a_val + 1
	if delta_1 <= 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_RANDOM_BIGINT_DELTA_TOO_LARGE, rsql.ERROR_BATCH_ABORT, math.MaxInt64-1)
		return
	}

	r_val = a_val + rand.Int63n(delta_1) // rand.Int63n returns a number in the range [0...delta_1[

	r.data_val = r_val
}

func Sysfunc_random_NUMERIC(r *NUMERIC, p *INT, s *INT) {
	var (
		rsql_err        *rsql.Error
		precision       uint16
		scale           uint16
		val             int64
		byteval         [60]byte // length must be >= rsql.DATATYPE_NUMERIC_PRECISION_MAX+18 (because rand.Int63n may append 18 bytes). 60 is good enough.
		byteres         []byte
		integral_length int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&p.Header, &s.Header) {
		return
	}

	/* operation */

	precision = uint16(p.data_val)
	scale = uint16(s.data_val)

	rsql.Assert(r.Data_precision == precision && r.Data_scale == scale)
	byteres = byteval[:0]

	for len(byteres) < int(precision)+1 { // +1 because one digit will be replaced by a decimal point
		val = rand.Int63n(1000000000000000000) // fill in up to 18 digits
		byteres = strconv.AppendInt(byteres, val, 10)
	}

	byteres = byteres[:precision+1] // byte slice containing  precision+1  random digits

	switch precision - scale {
	case 0:
		integral_length = 0
	case 1:
		integral_length = 1
	default:
		integral_length = rand.Int63n(int64(precision-scale)) + 1
	}
	byteres[integral_length] = '.' // put a decimal point at a random position

	if rsql_err = quad.From_bytes(&r.data_val, precision, scale, byteres); rsql_err != nil {
		r.data_error = rsql_err
		return
	}
}

func Sysfunc_random_FLOAT(r *FLOAT, a *FLOAT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	if a_val == b_val {
		r.data_val = a_val
		return
	}

	if b_val < a_val {
		a_val, b_val = b_val, a_val
	}

	r_val = a_val + (b_val-a_val)*rand.Float64()

	switch {
	case math.IsNaN(r_val): // check if NaN
		r_val = a_val // arbitrary choice, instead of returning an error. But this should never happen.

	case math.IsInf(r_val, 0): // check if +Inf or -Inf
		r_val = a_val // arbitrary choice, instead of returning an error. But this should never happen.
	}

	r.data_val = r_val
}

func Sysfunc_random_DATE(r *DATE, a *DATE, b *DATE) {
	var (
		rsql_err *rsql.Error
		a_val    mdat.MyDatetime
		b_val    mdat.MyDatetime

		a_daycount int64
		b_daycount int64
		delta      int64
		res        time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = a.data_val
	b_val = b.data_val

	if a_val.Equal(b_val.Time) == true {
		r.data_val = a_val
		return
	}

	if b_val.Before(a_val.Time) == true {
		a_val, b_val = b_val, a_val
	}

	b_daycount = (b_val.Unix() - mdat.UNIX_SEC_LOWEST) / mdat.SECONDS_PER_DAY
	a_daycount = (a_val.Unix() - mdat.UNIX_SEC_LOWEST) / mdat.SECONDS_PER_DAY

	delta = rand.Int63n(b_daycount - a_daycount + 1) // never overflow, as max val of   b_daycount - a_daycount  is 3652058

	if res, rsql_err = mdat.Add_second_duration(a_val, delta*mdat.SECONDS_PER_DAY); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}
