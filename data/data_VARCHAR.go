package data

import (
	"bytes"
	"fmt"
	"math"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"

	"golang.org/x/text/unicode/norm"

	"rsql"
	"rsql/lang"
	"rsql/like"
	"rsql/mbin"
	"rsql/mstr"
	"rsql/quad"
)

const MYSTR_CAPACITY_DEFAULT = 30 // default capacity VARCHAR buffer for string storage. Operations on VARCHAR increase the capacity as needed.

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *VARCHAR) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_VARCHAR)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += uint32(a.Data_precision)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	if a.Data_fixlen_flag == true {
		hash += 1
	}
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += rsql.Joaat_hash(a.data_val)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *VARCHAR) String() string {
	const MAX_LEN = 30

	typ := "VARCHAR"
	if a.Data_fixlen_flag {
		typ = "CHAR"
	}

	if a.data_error != nil {
		return fmt.Sprintf("%s(%d): error", typ, a.Data_precision)
	}

	if a.data_NULL_flag == true {
		return fmt.Sprintf("%s(%d): null", typ, a.Data_precision)
	}

	if utf8.RuneCount(a.data_val) > MAX_LEN {
		i := 0
		k := 0
		for k, _ = range a.data_val {
			if i == MAX_LEN {
				break
			}
		}

		return fmt.Sprintf("%s(%d): '%s...'", typ, a.Data_precision, a.data_val[:k])
	}

	return fmt.Sprintf("%s(%d): '%s'", typ, a.Data_precision, a.data_val)
}

func (a *VARCHAR) Stringval() string {

	if a.data_error != nil || a.data_NULL_flag == true {
		return ""
	}

	return string(a.data_val)
}

func (a *VARCHAR) Contains(part []byte) bool {

	return bytes.Contains(a.data_val, part)
}

// New_VARCHAR_NULL creates a new data.VARCHAR initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_VARCHAR_NULL(kind rsql.Kind_t, precision uint16, fixlen_flag bool) *VARCHAR {

	rsql.Assert(precision > 0)

	dataslot := &VARCHAR{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VARCHAR,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		Data_precision:   precision,
		Data_fixlen_flag: fixlen_flag,
		data_val:         make(mstr.Mystr, 0, MYSTR_CAPACITY_DEFAULT),
	}

	return dataslot
}

// New_literal_VARCHAR creates a new data.VARCHAR from a string.
// Returns an error if string too long.
//
func New_literal_VARCHAR(s string) (*VARCHAR, *rsql.Error) {

	runecount := utf8.RuneCountInString(s)

	if runecount > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
	}

	dataslot := &VARCHAR{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VARCHAR,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		Data_precision:   uint16(runecount),
		Data_fixlen_flag: false,
		data_val:         mstr.Mystr(s), // converts string to []byte
	}

	if dataslot.Data_precision == 0 { // for literal empty string, we want at least a precision of 1.
		dataslot.Data_precision = 1
	}

	return dataslot, nil
}

// New_VARIABLE_VARCHAR_value creates a new data.VARCHAR, used when we create a new global variable with initial value.
//
// Exactly the same as New_literal_VARCHAR_value, except that the dataslot is KIND_VAR_LEAF.
//
// Used to create variables _@current_db, etc.
//
func New_VARIABLE_VARCHAR_value(val string) (*VARCHAR, *rsql.Error) {

	runecount := utf8.RuneCountInString(val)

	if runecount > SYSVAR_VARCHAR_PRECISION_MAX {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, SYSVAR_VARCHAR_PRECISION_MAX)
	}

	dataslot := &VARCHAR{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VARCHAR,
			Data_kind:      rsql.KIND_VAR_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		Data_precision:   SYSVAR_VARCHAR_PRECISION_MAX,
		Data_fixlen_flag: false,
		data_val:         mstr.Mystr(val), // converts string to []byte
	}

	return dataslot, nil
}

// Add_VARCHAR returns a copy of two strings concatenated together.
//
//        The operands and result are NFC.
//
//        Beware that the result length can be larger than the sum of the two string lengths.
//            We consider it as "pathological" cases, which are rarely seen.
//            If it happens, the result is stored without being truncated, and may be larger than target precision. That's the best we can do, and an expected behavior.
//
//            For example, a = U+1EBF                NFC  length=1  LATIN SMALL LETTER E WITH CIRCUMFLEX AND ACUTE
//                         b = U+0327                NFC  length=1  COMBINING CEDILLA
//                       a+b = U+0229 U+0302 U+0301  NFC  length=3  LATIN SMALL LETTER E WITH CEDILLA, COMBINING CIRCUMFLEX ACCENT, COMBINING ACUTE ACCENT
//
func Add_VARCHAR(r *VARCHAR, a *VARCHAR, b *VARCHAR) {
	var (
		res mstr.Mystr
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = r.data_val[:0]

	if len(a.data_val) > 0 {
		res = append(res, a.data_val...)
	}

	if len(b.data_val) > 0 {
		if len(res) != 0 {
			res = norm.NFC.Append(res, b.data_val...)
		} else {
			res = append(res, b.data_val...)
		}
	}

	// check for overflow

	if len(res) > rsql.DATATYPE_VARCHAR_PRECISION_MAX { // note: if precision < DATATYPE_VARCHAR_PRECISION_MAX, it has been defined by the decorator to be large enough for non-pathological result
		if runecount := utf8.RuneCount(res); runecount > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
			return
		}
	}

	r.data_val = res
}

func Comp_equal_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res      int
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &coll.Header) {
		return
	}

	/* operation */

	res = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // never fails

	r.data_val = (res == 0)
}

func Comp_greater_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res      int
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &coll.Header) {
		return
	}

	/* operation */

	res = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // never fails

	r.data_val = (res > 0)
}

func Comp_less_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res      int
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &coll.Header) {
		return
	}

	/* operation */

	res = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // never fails

	r.data_val = (res < 0)
}

func Comp_greater_equal_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res      int
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &coll.Header) {
		return
	}

	/* operation */

	res = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // never fails

	r.data_val = (res >= 0)
}

func Comp_less_equal_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res      int
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &coll.Header) {
		return
	}

	/* operation */

	res = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // never fails

	r.data_val = (res <= 0)
}

func Comp_not_equal_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res      int
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &coll.Header) {
		return
	}

	/* operation */

	res = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // never fails

	r.data_val = (res != 0)
}

func Comp_like_VARCHAR(r *BOOLEAN, a *VARCHAR, b *REGEXPLIKE, coll *SYSCOLLATOR) {
	var (
		res      bool
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &coll.Header) {
		return
	}

	/* operation */

	res = like.Match(&b.data_val, a.data_val, coll.data_cached_collator) // never fails

	r.data_val = res
}

func Comp_not_like_VARCHAR(r *BOOLEAN, a *VARCHAR, b *REGEXPLIKE, coll *SYSCOLLATOR) {

	Comp_like_VARCHAR(r, a, b, coll)

	r.data_val = !r.data_val
}

func Is_null_VARCHAR(r *BOOLEAN, a *VARCHAR) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_VARCHAR(r *BOOLEAN, a *VARCHAR) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_VARCHAR checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_VARCHAR(r *BOOLEAN, a *VARCHAR, b_list []*VARCHAR, coll *SYSCOLLATOR) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_VARCHAR(r, a, b, coll)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_VARCHAR(r *BOOLEAN, a *VARCHAR, b_list []*VARCHAR, coll *SYSCOLLATOR) {

	In_list_VARCHAR(r, a, b_list, coll)

	r.data_val = !r.data_val
}

func Case_VARCHAR(r *VARCHAR, a_list []Case_element_t) {
	var (
		elem_val *VARCHAR
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*VARCHAR)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			if elem_val.data_error == nil && elem_val.data_NULL_flag == false {
				r.data_val = append(r.data_val[:0], elem_val.data_val...)
			}
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VARCHAR_to_SYSLANGUAGE(r *SYSLANGUAGE, a *VARCHAR) {
	var (
		val_lc       string
		langinfo_key string
		langinfo     *lang.Langinfo
		ok           bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val_lc = strings.ToLower(string(a.data_val))

	if langinfo_key, ok = lang.LANGUAGE_TO_TAG_MAP[val_lc]; ok == false {
		langinfo_key = val_lc
	}

	if langinfo, ok = lang.LANGINFO_MAP[langinfo_key]; ok == false {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_SYSLANGUAGE_INVALID_LOCALE, rsql.ERROR_BATCH_ABORT, a.data_val)
		return
	}

	r.data_langinfo = langinfo
	r.data_firstdayofweek = langinfo.Lng_firstdayofweek
	r.data_DMY = langinfo.Lng_DMY
}

// Cast_VARCHAR_to_VARBINARY_intern is not allowed to be used in normal TSQL script.
// It is only used internally by BULK INSERT.
//
// This function converts a 0x hexa string representation into byte slice.
//
func Cast_VARCHAR_to_VARBINARY_intern(r *VARBINARY, a *VARCHAR) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	number_of_bytes := (len(a.data_val)+1)/2 - 1 // +1 if a.data_val is odd length, /2 because 2 hexa digits make one byte, -1 for the mandatory leading "0x"
	if number_of_bytes > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARBINARY_OVERFLOW, rsql.ERROR_BATCH_ABORT, number_of_bytes, int(r.Data_precision))
		return
	}

	if rsql_err := mbin.Hexa2bin(&r.data_val, a.data_val); rsql_err != nil { // convert hexastring beginning with "0x" to slice of bytes. Empty byte slice is allowed.
		r.data_error = rsql_err
		return
	}

	rsql.Assert(number_of_bytes == len(r.data_val))
}

func Cast_VARCHAR_to_VARCHAR(r *VARCHAR, a *VARCHAR) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	source := a.data_val
	if len(source) > int(r.Data_precision)*utf8.UTFMax { // very coarse truncating
		source = source[:r.Data_precision*utf8.UTFMax]
	}

	r.data_val = append(r.data_val[:0], source...)

	if r.Data_fixlen_flag == false { // if target is not fixlen, just truncate string if too long
		if len(r.data_val) > int(r.Data_precision) { // fast but conservative check
			r.data_val.Truncate_to_precision(r.Data_precision)
		}
	} else {
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision) // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
	}

}

// Cast_VARCHAR_to_BIT casts VARCHAR to BIT.
//
// If string is empty or contains only spaces, it is cast to 0.
//
// If string contains a character which is not 0..9, an error is returned.
//
// If one of these digits is not 0, the string is cast to 1, else, to 0.
//
//        - NULL                is cast to NULL
//        - empty string ''     is cast to 0
//        - space string '    ' is cast to 0
//        - 'FALSE'             is cast to 0
//        - 'TRUE'              is cast to 1
//        - '0'                 is cast to 0
//        - '00000'             is cast to 0
//        - '2345'              is cast to 1
//        - '-2345'             error
//        - ' 2345'             error
//        - 'a2345'             error
//        - ' abcd'             error
//
func Cast_VARCHAR_to_BIT(r *BIT, a *VARCHAR) {
	var (
		val mstr.Mystr
		res int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = bytes.TrimRightFunc(a.data_val, unicode.IsSpace)

	if len(val) == 0 { // if only spaces, return 0
		r.data_val = 0
		return
	}

	if uc, _ := utf8.DecodeRune(val); unicode.IsSpace(uc) { // if leading space exists, an error is returned
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_BIT_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
		return
	}

	if len(val) == 5 && // check if FALSE
		(val[0] == 'f' || val[0] == 'F') &&
		(val[1] == 'a' || val[1] == 'A') &&
		(val[2] == 'l' || val[2] == 'L') &&
		(val[3] == 's' || val[3] == 'S') &&
		(val[4] == 'e' || val[4] == 'E') {
		r.data_val = 0
		return
	}

	if len(val) == 4 && // check if TRUE
		(val[0] == 't' || val[0] == 'T') &&
		(val[1] == 'r' || val[1] == 'R') &&
		(val[2] == 'u' || val[2] == 'U') &&
		(val[3] == 'e' || val[3] == 'E') {
		r.data_val = 1
		return
	}

	for _, c := range val { // all characters must be digits. Else, an error is returned.
		if !(c >= '0' && c <= '9') {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_BIT_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
			return
		}
		if c != '0' { // if non-zero digit found
			res = 1
		}
	}

	r.data_val = res
}

func Cast_VARCHAR_to_TINYINT(r *TINYINT, a *VARCHAR) {
	var (
		err error
		val mstr.Mystr
		res int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = bytes.TrimSpace(a.data_val)

	if len(val) == 0 { // if empty string, return 0
		r.data_val = 0
		return
	}

	if res, err = strconv.ParseInt(string(val), 10, 64); err != nil {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_TINYINT_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
		return
	}

	if res > math.MaxUint8 || res < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, res)
		return
	}

	r.data_val = res
}

func Cast_VARCHAR_to_SMALLINT(r *SMALLINT, a *VARCHAR) {
	var (
		err error
		val mstr.Mystr
		res int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = bytes.TrimSpace(a.data_val)

	if len(val) == 0 { // if empty string, return 0
		r.data_val = 0
		return
	}

	if res, err = strconv.ParseInt(string(val), 10, 64); err != nil {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_SMALLINT_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
		return
	}

	if res > math.MaxInt16 || res < math.MinInt16 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, res)
		return
	}

	r.data_val = res
}

func Cast_VARCHAR_to_INT(r *INT, a *VARCHAR) {
	var (
		err error
		val mstr.Mystr
		res int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = bytes.TrimSpace(a.data_val)

	if len(val) == 0 { // if empty string, return 0
		r.data_val = 0
		return
	}

	if res, err = strconv.ParseInt(string(val), 10, 64); err != nil {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_INT_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
		return
	}

	if res > math.MaxInt32 || res < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT, res)
		return
	}

	r.data_val = res
}

func Cast_VARCHAR_to_BIGINT(r *BIGINT, a *VARCHAR) {
	var (
		err error
		val mstr.Mystr
		res int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = bytes.TrimSpace(a.data_val)

	if len(val) == 0 { // if empty string, return 0
		r.data_val = 0
		return
	}

	if res, err = strconv.ParseInt(string(val), 10, 64); err != nil {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_BIGINT_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
		return
	}

	r.data_val = res
}

func Cast_VARCHAR_to_NUMERIC(r *NUMERIC, a *VARCHAR) {
	var (
		val mstr.Mystr
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = bytes.TrimSpace(a.data_val)

	if len(val) == 0 { // if empty string, return 0
		quad.Zero(&r.data_val, r.Data_precision, r.Data_scale) // never returns error
		return
	}

	r.data_error = quad.From_bytes(&r.data_val, r.Data_precision, r.Data_scale, val)
}

func Cast_VARCHAR_to_FLOAT(r *FLOAT, a *VARCHAR) {
	var (
		err error
		val mstr.Mystr
		res float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = bytes.TrimSpace(a.data_val)

	if len(val) == 0 { // if empty string, return 0
		r.data_val = 0.
		return
	}

	if res, err = strconv.ParseFloat(string(val), 64); err != nil {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CAST_FLOAT_BAD_STRING, rsql.ERROR_BATCH_ABORT, val)
		return
	}

	switch {
	case math.IsNaN(res): // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return

	case math.IsInf(res, 0): // check if +Inf or -Inf
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_INFINITE, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = res
}

func Assign_VARCHAR(r *VARCHAR, a *VARCHAR) *rsql.Error {

	rsql.Assert(r.Data_precision == a.Data_precision)
	rsql.Assert(r.Data_fixlen_flag == a.Data_fixlen_flag)

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	if a.data_error == nil && a.data_NULL_flag == false { // if a is not error nor NULL, copy string
		r.data_val = append(r.data_val[:0], a.data_val...)
	}

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

// Assign_VARCHAR_value is used by VM to assign value to OUT variables, e.g. _@current_db_name..
//
// Fixlen_flag must be false (r must be VARCHAR, not CHAR).
//
func Assign_VARCHAR_value(r *VARCHAR, val string) *rsql.Error {

	rsql.Assert(r.Data_fixlen_flag == false)

	runecount := utf8.RuneCountInString(val)

	if runecount > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, r.Data_precision)
		return r.data_error
	}

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], val...)

	return nil
}

// Assign_VARCHAR_bytes is used during BULK INSERT, to copy field content from file into a VARCHAR working dataslot.
//
// Pass true for normalize_flag only for VARCHAR target datatslots. Other types don't need normalization, as valid field for INT, DATE, etc only contain ascii, or raise a parse error.
//
func Assign_VARCHAR_bytes(r *VARCHAR, val []byte, normalize_flag bool) *rsql.Error {
	var (
		bb        []byte
		runecount int
	)

	// very coarse precision check

	if len(val) > int(r.Data_precision)*utf8.UTFMax {
		runecount := utf8.RuneCount(val)
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, r.Data_precision)
		return r.data_error
	}

	// fill dataslot

	r.data_error = nil
	r.data_NULL_flag = false

	if normalize_flag == true && norm.NFC.IsNormal(val) == false {
		bb = norm.NFC.Append(r.data_val[:0], val...) // normalize to NFC
	} else {
		bb = append(r.data_val[:0], val...)
	}

	runecount = utf8.RuneCount(bb)

	if runecount > int(r.Data_precision) { // check precision
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, r.Data_precision)
		return r.data_error
	}

	if r.Data_fixlen_flag == true && runecount < int(r.Data_precision) {
		padding_count := int(r.Data_precision) - runecount
		string_length := len(bb)
		full_length := len(bb) + padding_count

		if full_length > cap(bb) {
			bb_new := make([]byte, full_length)
			copy(bb_new, bb)
			bb = bb_new
		}

		bb = bb[:full_length]

		for i := string_length; i < full_length; i++ {
			bb[i] = ' '
		}
	}

	r.data_val = bb

	return nil
}

// Assign_VARCHAR_empty_string is used during BULK INSERT, to put an empty string into a VARCHAR working dataslot.
//
func Assign_VARCHAR_empty_string(r *VARCHAR) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = r.data_val[:0]

	if r.Data_fixlen_flag == true {
		if cap(r.data_val) < int(r.Data_precision) {
			r.data_val = make([]byte, r.Data_precision)
		}

		r.data_val = r.data_val[:r.Data_precision]

		for i, _ := range r.data_val {
			r.data_val[i] = ' '
		}
	}
}

func Copy_VARCHAR(r *VARCHAR, a *VARCHAR) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	if r.data_error != nil || r.data_NULL_flag == true {
		return
	}

	r.data_val = append(r.data_val[:0], a.data_val...)
}

func Clone_VARCHAR(kind rsql.Kind_t, a *VARCHAR) *VARCHAR {

	return New_VARCHAR_NULL(kind, a.Data_precision, a.Data_fixlen_flag)
}
