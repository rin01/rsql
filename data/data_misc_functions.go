package data

import (
	"strings"

	"rsql"
	"rsql/cache"
)

func truncate_runes(s string, runes_count int) string {

	if len(s) > runes_count {
		n := 0

		for i, _ := range s {
			n++
			//fmt.Printf("%d  %d  %c\n", i, n, c)
			if n > runes_count {
				return s[:i]
			}
		}
	}

	return s
}

func Sysfunc_atatversion(r *VARCHAR) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], rsql.VERSION...)
}

func Sysfunc_atatservername(r *VARCHAR) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], rsql.G_SERVERNAME...)
}

func Sysfunc_atatservicename(r *VARCHAR) {

	r.data_error = nil
	r.data_NULL_flag = false

	i := strings.LastIndex(rsql.G_SERVERNAME, "/")

	servicename := "RSQLServer"
	if i >= 0 {
		servicename = rsql.G_SERVERNAME[i+1:]
		if servicename == "" {
			servicename = "RSQLServer"
		}
	}

	r.data_val = append(r.data_val[:0], servicename...)
}

func Sysfunc_atatidentity(r *BIGINT, context *rsql.Context) {

	panic("the VM should call SCOPE_IDENTITY() instead of @@IDENTITY ") // for RSQL, @@IDENTITY and SCOPE_IDENTITY() are the same
}

func Sysfunc_atatrowcount(r *BIGINT, context *rsql.Context) {

	wcache := context.Ctx_wcache.(*cache.Wcache)

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = wcache.Wpc_last_rowcount
}

func Sysfunc_ataterror(r *INT) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = 0
}

func Sysfunc_atattrancount(r *INT, context *rsql.Context) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = int64(context.Ctx_trancount)
}

func Sysfunc_scope_identity(r *BIGINT, context *rsql.Context) {

	wcache := context.Ctx_wcache.(*cache.Wcache)

	if wcache.Wpc_last_identity_exists == false {
		r.data_error = nil
		r.data_NULL_flag = true

		return
	}

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = wcache.Wpc_last_identity
}
