package data

import (
	"math"
	"strconv"
	"time"
	"unicode/utf8"

	"rsql"
	"rsql/format"
	"rsql/mdat"
)

func Sysfunc_dateadd_year_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		res_year              int64
		res_day               int
		res_last_day_of_month int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	year, month, day := b.data_val.Date()
	hour, min, sec := b.data_val.Clock()
	ns := b.data_val.Nanosecond()

	res_year = int64(year) + a.data_val

	if res_year >= mdat.YEAR_MAX_SENTINEL || res_year < mdat.YEAR_LOWEST {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	res_day = day
	if month == time.February && day > 28 { // if source date is February 29th, check that it is a valid day for target date. Else, change day to February 28th
		res_last_day_of_month = mdat.Last_day_of_month(int(res_year), month)

		if day > res_last_day_of_month {
			res_day = res_last_day_of_month
		}
	}

	res := time.Date(int(res_year), month, res_day, hour, min, sec, ns, time.UTC)

	r.data_val.Time = res
}

func Sysfunc_dateadd_quarter_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err  *rsql.Error
		res_year  int
		res_month time.Month
		res_day   int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	year, month, day := b.data_val.Date()
	hour, min, sec := b.data_val.Clock()
	ns := b.data_val.Nanosecond()

	if res_year, res_month, res_day, rsql_err = mdat.Add_month_duration(year, month, day, a.data_val*3); rsql_err != nil { // target day is corrected if it is greater than last day of target month
		r.data_error = rsql_err
		return
	}

	res := time.Date(res_year, res_month, res_day, hour, min, sec, ns, time.UTC)

	r.data_val.Time = res
}

func Sysfunc_dateadd_month_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err  *rsql.Error
		res_year  int
		res_month time.Month
		res_day   int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	year, month, day := b.data_val.Date()
	hour, min, sec := b.data_val.Clock()
	ns := b.data_val.Nanosecond()

	if res_year, res_month, res_day, rsql_err = mdat.Add_month_duration(year, month, day, a.data_val); rsql_err != nil { // target day is corrected if it is greater than last day of target month
		r.data_error = rsql_err
		return
	}

	res := time.Date(res_year, res_month, res_day, hour, min, sec, ns, time.UTC)

	r.data_val.Time = res
}

// Sysfunc_dateadd_day_DATETIME adds number of days to date.
//
// This function is called for DATEADD(day, ...), DATEADD(dayofyear, ...), DATEADD(weekday, ...).
// In MS SQL Server docs, it is written that " dayofyear , day, and weekday return the same value.".
//
func Sysfunc_dateadd_day_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_second_duration(b.data_val, a.data_val*mdat.SECONDS_PER_DAY); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

// Sysfunc_datetime_subtract_day_DATETIME subtracts number of days from date (that is, adds -number of days to date).
// This function is almost a copy of Sysfunc_dateadd_day_DATETIME.
//
//       This function is only called for "-" operator in   DATETIME - INT  expression.
//
func Sysfunc_datetime_subtract_day_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_second_duration(b.data_val, -a.data_val*mdat.SECONDS_PER_DAY); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_week_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_second_duration(b.data_val, a.data_val*mdat.SECONDS_PER_WEEK); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_hour_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_second_duration(b.data_val, a.data_val*3600); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_minute_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_second_duration(b.data_val, a.data_val*60); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_second_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_second_duration(b.data_val, a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_millisecond_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_nanosecond_duration(b.data_val, a.data_val*1000000); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_microsecond_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_nanosecond_duration(b.data_val, a.data_val*1000); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_dateadd_nanosecond_DATETIME(r *DATETIME, a *INT, b *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.Add_nanosecond_duration(b.data_val, a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_datediff_year_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = b.data_val.Year() - a.data_val.Year()

	r.data_val = int64(res)
}

func Sysfunc_datediff_quarter_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	b_year, b_month, _ := b.data_val.Date()
	a_year, a_month, _ := a.data_val.Date()

	res = (b_year*12+int(b_month-1))/3 - (a_year*12+int(a_month-1))/3

	r.data_val = int64(res)
}

func Sysfunc_datediff_month_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	b_year, b_month, _ := b.data_val.Date()
	a_year, a_month, _ := a.data_val.Date()

	res = (b_year*12 + int(b_month-1)) - (a_year*12 + int(a_month-1))

	r.data_val = int64(res)
}

// Sysfunc_datediff_day_DATETIME returns number of days between two datetimes.
//
// This function is called for DATEDIFF(day, ...), DATEDIFF(dayofyear, ...).
//
func Sysfunc_datediff_day_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		a_daycount int64
		b_daycount int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	b_daycount = (b.data_val.Unix() - mdat.UNIX_SEC_LOWEST) / mdat.SECONDS_PER_DAY
	a_daycount = (a.data_val.Unix() - mdat.UNIX_SEC_LOWEST) / mdat.SECONDS_PER_DAY

	r.data_val = b_daycount - a_daycount // never oveflow, as max val is 3652058
}

// Sysfunc_datediff_week_DATETIME returns the number of Sundays between a and b.
// It is the same behaviour as MS SQL Server. Specifying SET DATEFIRST has no effect on DATEDIFF, which always uses Sunday as the first day of the week.
//
func Sysfunc_datediff_week_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		a_daycount int64
		b_daycount int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	b_daycount = (b.data_val.Unix()-mdat.UNIX_SEC_LOWEST)/mdat.SECONDS_PER_DAY + 8 // +8 offset because 0001-01-01 is Monday
	a_daycount = (a.data_val.Unix()-mdat.UNIX_SEC_LOWEST)/mdat.SECONDS_PER_DAY + 8

	r.data_val = b_daycount/7 - a_daycount/7 // never oveflow
}

// Sysfunc_datediff_hour_DATETIME returns the real number of hours between two dates.
//
//     NOTE: MS SQL Server counts the number of crossings through the hour boundaries.
//           I think nobody wants or expects this strange behaviour.
//           I decide to return the real hour count between the two dates.
//
func Sysfunc_datediff_hour_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		delta_sec int64
		sign      int
		res       int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta_sec, _, sign = mdat.Diff_sec_nsec_absolute(a.data_val.Time, b.data_val.Time)

	res = delta_sec / 3600

	if sign < 0 {
		res = -res
	}

	r.data_val = res // never overflows, as res max is 87649415
}

// Sysfunc_datediff_minute_DATETIME returns the real number of minutes between two dates.
//
//     NOTE: MS SQL Server counts the number of crossing through the minute boundaries.
//           I think nobody wants or expects this strange behaviour.
//           I decide to return the real minute count between the two dates.
//
func Sysfunc_datediff_minute_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		delta_sec int64
		sign      int
		res       int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta_sec, _, sign = mdat.Diff_sec_nsec_absolute(a.data_val.Time, b.data_val.Time)

	res = delta_sec / 60

	if sign < 0 {
		res = -res
	}

	if res > math.MaxInt32 || res < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATEDIFF_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = res
}

// Sysfunc_datediff_second_DATETIME returns the real number of seconds between two dates.
//
//     NOTE: MS SQL Server counts the number of crossings through the second boundaries.
//           I think nobody wants or expects this strange behaviour.
//           I decide to return the real second count between the two dates.
//
func Sysfunc_datediff_second_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		delta_sec int64
		sign      int
		res       int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta_sec, _, sign = mdat.Diff_sec_nsec_absolute(a.data_val.Time, b.data_val.Time)

	res = delta_sec

	if sign < 0 {
		res = -res
	}

	if res > math.MaxInt32 || res < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATEDIFF_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = res
}

// Sysfunc_datediff_millisecond_DATETIME returns the real number of milliseconds between two dates.
//
//     NOTE: MS SQL Server counts the number of crossings through the millisecond boundaries.
//           I think nobody wants or expects this strange behaviour.
//           I decide to return the real millisecond count between the two dates.
//
func Sysfunc_datediff_millisecond_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		delta_sec  int64
		delta_nsec int64
		sign       int
		res        int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta_sec, delta_nsec, sign = mdat.Diff_sec_nsec_absolute(a.data_val.Time, b.data_val.Time) // Max delta_sec is < 315,537,897,600 seconds. Note: MaxIn64 == 9,223,372,036,854,775,807

	res = delta_sec*1000 + delta_nsec/1000000 // never overflows int64

	if sign < 0 {
		res = -res
	}

	if res > math.MaxInt32 || res < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATEDIFF_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = res
}

// Sysfunc_datediff_microsecond_DATETIME returns the real number of microseconds between two dates.
//
//     NOTE: MS SQL Server counts the number of crossings through the microsecond boundaries.
//           I think nobody wants or expects this strange behaviour.
//           I decide to return the real microsecond count between the two dates.
//
func Sysfunc_datediff_microsecond_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		delta_sec  int64
		delta_nsec int64
		sign       int
		res        int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta_sec, delta_nsec, sign = mdat.Diff_sec_nsec_absolute(a.data_val.Time, b.data_val.Time) // Max delta_sec is < 315,537,897,600 seconds. Note: MaxIn64 == 9,223,372,036,854,775,807

	res = delta_sec*1000000 + delta_nsec/1000 // never overflows int64

	if sign < 0 {
		res = -res
	}

	if res > math.MaxInt32 || res < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATEDIFF_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = res
}

// Sysfunc_datediff_nanosecond_DATETIME returns the real number of nanoseconds between two dates.
//
//     NOTE: MS SQL Server counts the number of crossings through the nanosecond boundaries.
//           I think nobody wants or expects this strange behaviour.
//           I decide to return the real nanosecond count between the two dates.
//
func Sysfunc_datediff_nanosecond_DATETIME(r *INT, a *DATETIME, b *DATETIME) {
	var (
		delta_sec  int64
		delta_nsec int64
		sign       int
		res        int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	delta_sec, delta_nsec, sign = mdat.Diff_sec_nsec_absolute(a.data_val.Time, b.data_val.Time) // Max delta_sec is < 315,537,897,600 seconds. Note: MaxIn64 == 9,223,372,036,854,775,807

	if delta_sec > math.MaxInt64/int64(1e9)-1 { // -1 is needed so that res can be 9,223,372,035,999,999,999
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATEDIFF_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	res = delta_sec*1e9 + delta_nsec

	if sign < 0 {
		res = -res
	}

	if res > math.MaxInt32 || res < math.MinInt32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATEDIFF_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = res
}

func Sysfunc_datepart_year_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Year())

	r.data_val = val
}

func Sysfunc_datepart_quarter_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Month()) // 1 ... 12
	val = ((val - 1) / 3) + 1

	r.data_val = val
}

func Sysfunc_datepart_month_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Month()) // 1 ... 12

	r.data_val = val
}

func Sysfunc_datepart_dayofyear_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.YearDay()) // 1 ... 365,366     (366 for leap years)

	r.data_val = val
}

func Sysfunc_datepart_day_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Day()) // 1 ... 31

	r.data_val = val
}

func Sysfunc_datepart_week_DATETIME(r *INT, a *DATETIME, syslang *SYSLANGUAGE) {
	var (
		year              int
		date_01_01        time.Time
		weekday_01_01     time.Weekday
		delta             int
		yearday_0based    int
		week_count_0based int
		val               int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &syslang.Header) {
		return
	}

	/* operation */

	year = a.data_val.Year()

	date_01_01 = time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC) // January 1st of same year

	weekday_01_01 = date_01_01.Weekday() // 0: Sunday   1: Monday  ...  6: Saturday

	delta = int(weekday_01_01) - syslang.data_firstdayofweek // delta is the number of days before date_01_01 and belonging to the same week
	if delta < 0 {
		delta = delta + 7 // delta is 0...6
	}

	yearday_0based = a.data_val.YearDay() - 1 // 0...364, 365      365 is for leap years

	week_count_0based = (yearday_0based + delta) / 7

	val = int64(week_count_0based + 1) // 1-based

	r.data_val = val
}

func Sysfunc_datepart_weekday_DATETIME(r *INT, a *DATETIME, syslang *SYSLANGUAGE) {
	var (
		weekday time.Weekday
		delta   int
		val     int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &syslang.Header) {
		return
	}

	/* operation */

	weekday = a.data_val.Weekday() // 0: Sunday   1: Monday  ...  6: Saturday

	delta = int(weekday) - syslang.data_firstdayofweek
	if delta < 0 {
		delta = delta + 7
	}

	val = int64(delta + 1) // 1-based

	r.data_val = val
}

func Sysfunc_datepart_hour_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Hour()) // 0 ... 23

	r.data_val = val
}

func Sysfunc_datepart_minute_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Minute()) // 0 ... 59

	r.data_val = val
}

func Sysfunc_datepart_second_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Second()) // 0 ... 59

	r.data_val = val
}

func Sysfunc_datepart_millisecond_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Nanosecond()) / 1000000 // 0 ... 999       MS Sql Server truncates and doesn't round. I do the same, as it is the correct thing to do.

	r.data_val = val
}

func Sysfunc_datepart_microsecond_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Nanosecond()) / 1000 // 0 ... 999999       MS Sql Server truncates and doesn't round. I do the same, as it is the correct thing to do.

	r.data_val = val
}

func Sysfunc_datepart_nanosecond_DATETIME(r *INT, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Nanosecond()) // 0 ... 999999999

	r.data_val = val
}

func Sysfunc_datename_year_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Year())

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_quarter_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Month()) // 1 ... 12
	val = ((val - 1) / 3) + 1

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_month_DATETIME(r *VARCHAR, a *DATETIME, syslang *SYSLANGUAGE) {
	var (
		val     int64
		val_str string
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &syslang.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Month()) // 1 ... 12

	val_str = syslang.data_langinfo.Lng_monthnames[val-1]

	res := append(r.data_val[:0], val_str...)

	r.data_val = res
}

func Sysfunc_datename_dayofyear_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.YearDay()) // 1 ... 365,366     (366 for leap years)

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_day_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Day()) // 1 ... 31

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_week_DATETIME(r *VARCHAR, a *DATETIME, syslang *SYSLANGUAGE) {
	var (
		year              int
		date_01_01        time.Time
		weekday_01_01     time.Weekday
		delta             int
		yearday_0based    int
		week_count_0based int
		val               int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &syslang.Header) {
		return
	}

	/* operation */

	year = a.data_val.Year()

	date_01_01 = time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC) // January 1st of same year

	weekday_01_01 = date_01_01.Weekday() // 0: Sunday   1: Monday  ...  6: Saturday

	delta = int(weekday_01_01) - syslang.data_firstdayofweek // delta is the number of days before date_01_01 and belonging to the same week
	if delta < 0 {
		delta = delta + 7 // delta is 0...6
	}

	yearday_0based = a.data_val.YearDay() - 1 // 0...364, 365      365 is for leap years

	week_count_0based = (yearday_0based + delta) / 7

	val = int64(week_count_0based + 1) // 1-based

	// append to VARCHAR

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_weekday_DATETIME(r *VARCHAR, a *DATETIME, syslang *SYSLANGUAGE) {
	var (
		weekday     time.Weekday
		weekday_str string
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &syslang.Header) {
		return
	}

	/* operation */

	weekday = a.data_val.Weekday() // 0: Sunday   1: Monday  ...  6: Saturday

	weekday_str = syslang.data_langinfo.Lng_daynames[weekday]

	res := append(r.data_val[:0], weekday_str...)

	r.data_val = res
}

func Sysfunc_datename_hour_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Hour()) // 0 ... 23

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_minute_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Minute()) // 0 ... 59

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_second_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Second()) // 0 ... 59

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_millisecond_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Nanosecond()) / 1000000 // 0 ... 999       MS Sql Server truncates and doesn't round. I do the same.

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_microsecond_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Nanosecond()) / 1000 // 0 ... 999999       MS Sql Server truncates and doesn't round. I do the same.

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_datename_nanosecond_DATETIME(r *VARCHAR, a *DATETIME) {
	var (
		val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = int64(a.data_val.Nanosecond()) // 0 ... 999999999

	res := strconv.AppendInt(r.data_val[:0], val, 10)

	r.data_val = res
}

func Sysfunc_eomonth_DATETIME(r *DATETIME, a *DATETIME, month_duration *INT) {
	var (
		year               int
		month              time.Month
		day                int
		month_duration_val int64
		val                time.Time
		rsql_err           *rsql.Error
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &month_duration.Header) {
		return
	}

	/* operation */

	month_duration_val = month_duration.data_val

	year, month, day = a.data_val.Date()

	if year, month, day, rsql_err = mdat.Add_month_duration(year, month, day, month_duration_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	day = mdat.Last_day_of_month(year, month)

	val = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)

	r.data_val.Time = val
}

func Sysfunc_bomonth_DATETIME(r *DATETIME, a *DATETIME, month_duration *INT) {
	var (
		year               int
		month              time.Month
		day                int
		month_duration_val int64
		val                time.Time
		rsql_err           *rsql.Error
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &month_duration.Header) {
		return
	}

	/* operation */

	month_duration_val = month_duration.data_val

	year, month, day = a.data_val.Date()

	if year, month, day, rsql_err = mdat.Add_month_duration(year, month, day, month_duration_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	day = 1

	val = time.Date(year, month, day, 0, 0, 0, 0, time.UTC)

	r.data_val.Time = val
}

func Sysfunc_format_DATETIME(r *VARCHAR, a *DATETIME, b *VARCHAR, syslang *SYSLANGUAGE) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &syslang.Header) {
		return
	}

	/* operation */

	res := format.Append_formatted_date(r.data_val[:0], a.data_val.Time, b.data_val, syslang.data_langinfo)

	// check result length

	if len(res) > int(r.Data_precision) && utf8.RuneCount(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_FORMAT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String(), b.data_val)
		return
	}

	r.data_val = res
}

var (
	DTFORMAT_1  = []byte(`MM\/dd\/yy`)
	DTFORMAT_2  = []byte(`yy\.MM\.dd`)
	DTFORMAT_3  = []byte(`dd\/MM\/yy`)
	DTFORMAT_4  = []byte(`dd\.MM\.yy`)
	DTFORMAT_5  = []byte(`dd\-MM\-yy`)
	DTFORMAT_6  = []byte(`dd MMM yy`)
	DTFORMAT_7  = []byte(`MMM dd\, yy`)
	DTFORMAT_10 = []byte(`MM\-dd\-yy`)
	DTFORMAT_11 = []byte(`yy\/MM\/dd`)
	DTFORMAT_12 = []byte(`yyMMdd`)

	DTFORMAT_0_100  = []byte(`MMM d yyyy h\:mmtt`)
	DTFORMAT_101    = []byte(`MM\/dd\/yyyy`)
	DTFORMAT_102    = []byte(`yyyy\.MM\.dd`)
	DTFORMAT_103    = []byte(`dd\/MM\/yyyy`)
	DTFORMAT_104    = []byte(`dd\.MM\.yyyy`)
	DTFORMAT_105    = []byte(`dd\-MM\-yyyy`)
	DTFORMAT_106    = []byte(`dd MMM yyyy`)
	DTFORMAT_107    = []byte(`MMM dd\, yyyy`)
	DTFORMAT_8_108  = []byte(`HH\:mm\:ss`)
	DTFORMAT_9_109  = []byte(`MMM d yyyy h\:mm\:ss\.ffftt`)
	DTFORMAT_110    = []byte(`MM\-dd\-yyyy`)
	DTFORMAT_111    = []byte(`yyyy\/MM\/dd`)
	DTFORMAT_112    = []byte(`yyyyMMdd`)
	DTFORMAT_13_113 = []byte(`dd MMM yyyy HH:mm\:ss\.fff`)
	DTFORMAT_14_114 = []byte(`HH:mm\:ss\.fff`)
	DTFORMAT_20_120 = []byte(`yyyy\-MM\-dd HH\:mm\:ss`)
	DTFORMAT_21_121 = []byte(`yyyy\-MM\-dd HH\:mm\:ss\.fff`)
	DTFORMAT_126    = []byte(`yyyy\-MM\-dd\THH\:mm\:ss\.fff`)
)

// Sysfunc_convert_DATETIME_to_VARCHAR formats a DATETIME into a VARCHAR, with a format style.
//
//      CONVERT ( data_type [ ( length ) ] , datetime expression [ , style ] )
//
//      NOTE: target may be fixlen, if SQL script was CAST(@mydatetime as char(50)), which has been transformed by the decorator into CONVERT.
//
func Sysfunc_convert_DATETIME_to_VARCHAR(r *VARCHAR, a *DATETIME, b *INT, syslang *SYSLANGUAGE) {
	var (
		format_string []byte
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &syslang.Header) {
		return
	}

	/* operation */

	switch b.data_val {
	case 1:
		format_string = DTFORMAT_1
	case 2:
		format_string = DTFORMAT_2
	case 3:
		format_string = DTFORMAT_3
	case 4:
		format_string = DTFORMAT_4
	case 5:
		format_string = DTFORMAT_5
	case 6:
		format_string = DTFORMAT_6
	case 7:
		format_string = DTFORMAT_7
	case 10:
		format_string = DTFORMAT_10
	case 11:
		format_string = DTFORMAT_11
	case 12:
		format_string = DTFORMAT_12
	case 0, 100:
		format_string = DTFORMAT_0_100
	case 101:
		format_string = DTFORMAT_101
	case 102:
		format_string = DTFORMAT_102
	case 103:
		format_string = DTFORMAT_103
	case 104:
		format_string = DTFORMAT_104
	case 105:
		format_string = DTFORMAT_105
	case 106:
		format_string = DTFORMAT_106
	case 107:
		format_string = DTFORMAT_107
	case 8, 108:
		format_string = DTFORMAT_8_108
	case 9, 109:
		format_string = DTFORMAT_9_109
	case 110:
		format_string = DTFORMAT_110
	case 111:
		format_string = DTFORMAT_111
	case 112:
		format_string = DTFORMAT_112
	case 13, 113:
		format_string = DTFORMAT_13_113
	case 14, 114:
		format_string = DTFORMAT_14_114
	case 20, 120:
		format_string = DTFORMAT_20_120
	case 21, 121:
		format_string = DTFORMAT_21_121
	case 126:
		format_string = DTFORMAT_126

	default:
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_CONVERT_VARCHAR_UNSUPPORTED_STYLE, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	res := format.Append_formatted_date(r.data_val[:0], a.data_val.Time, format_string, syslang.data_langinfo)

	// check result length

	if len(res) > int(r.Data_precision) && utf8.RuneCount(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_CONVERT_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val.String(), format_string)
		return
	}

	r.data_val = res

	if r.Data_fixlen_flag == true && len(r.data_val) < int(r.Data_precision) { // if target is fixlen, the rune count must be equal to target precision. So, we append space padding if needed.
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision)
	}
}

func Sysfunc_datetimefromparts_DATETIME(r *DATETIME, year *INT, month *INT, day *INT, hour *INT, minute *INT, seconds *INT, milliseconds *INT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&year.Header, &month.Header, &day.Header, &hour.Header, &minute.Header, &seconds.Header, &milliseconds.Header) {
		return
	}

	// check values are in proper range

	if (year.data_val >= 1 && year.data_val <= 9999 &&
		month.data_val >= 1 && month.data_val <= 12 &&
		day.data_val >= 1 && day.data_val <= int64(mdat.Last_day_of_month(int(year.data_val), time.Month(month.data_val))) &&
		hour.data_val >= 0 && hour.data_val <= 23 &&
		minute.data_val >= 0 && minute.data_val <= 59 &&
		seconds.data_val >= 0 && seconds.data_val <= 59 &&
		milliseconds.data_val >= 0 && milliseconds.data_val <= 999) == false {

		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATETIMEFROMPARTS_ILLEGAL_ARGS, rsql.ERROR_BATCH_ABORT, year.data_val, month.data_val, day.data_val, hour.data_val, minute.data_val, seconds.data_val, milliseconds.data_val)
		return
	}

	/* operation */

	res := time.Date(int(year.data_val), time.Month(month.data_val), int(day.data_val), int(hour.data_val), int(minute.data_val), int(seconds.data_val), int(milliseconds.data_val*1000000), time.UTC)

	r.data_val.Time = res
}

func Sysfunc_datetime2fromparts_DATETIME(r *DATETIME, year *INT, month *INT, day *INT, hour *INT, minute *INT, seconds *INT, fractions *INT, precision *INT) {
	var (
		nanoseconds int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&year.Header, &month.Header, &day.Header, &hour.Header, &minute.Header, &seconds.Header, &fractions.Header, &precision.Header) {

		if r.data_error == nil && precision.data_error == nil && precision.data_NULL_flag == true { // if no error in arguments, but precision is NULL, raise error
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATETIME2FROMPARTS_NULL_PRECISION, rsql.ERROR_BATCH_ABORT)
		}
		return
	}

	// check values are in proper range

	nanoseconds = mdat.Nanoseconds_from_fractions(fractions.data_val, precision.data_val) // -1 if error

	if (year.data_val >= 1 && year.data_val <= 9999 &&
		month.data_val >= 1 && month.data_val <= 12 &&
		day.data_val >= 1 && day.data_val <= int64(mdat.Last_day_of_month(int(year.data_val), time.Month(month.data_val))) &&
		hour.data_val >= 0 && hour.data_val <= 23 &&
		minute.data_val >= 0 && minute.data_val <= 59 &&
		seconds.data_val >= 0 && seconds.data_val <= 59 &&
		nanoseconds >= 0 && nanoseconds <= 999999999) == false {

		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_DATETIME_DATETIME2FROMPARTS_ILLEGAL_ARGS, rsql.ERROR_BATCH_ABORT, year.data_val, month.data_val, day.data_val, hour.data_val, minute.data_val, seconds.data_val, fractions.data_val, precision.data_val)
		return
	}

	/* operation */

	res := time.Date(int(year.data_val), time.Month(month.data_val), int(day.data_val), int(hour.data_val), int(minute.data_val), int(seconds.data_val), int(nanoseconds), time.UTC)

	r.data_val.Time = res
}

func Sysfunc_getutcdate_DATETIME(r *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	r.data_error = nil
	r.data_NULL_flag = false

	if res, rsql_err = mdat.Get_time_UTC(); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_utc_to_local_DATETIME(r *DATETIME, a *DATETIME) {
	var (
		rsql_err *rsql.Error
		res      time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if res, rsql_err = mdat.UTC_to_local(a.data_val.Time); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val.Time = res
}

func Sysfunc_isnull_DATETIME(r *DATETIME, a *DATETIME, b *DATETIME) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
	}
}

func Sysfunc_iif_DATETIME(r *DATETIME, a *BOOLEAN, b *DATETIME, c *DATETIME) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	r.data_val = c.data_val
}

func Sysfunc_choose_DATETIME(r *DATETIME, a *INT, b_list []*DATETIME) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	r.data_val = elem.data_val
}

func Sysfunc_coalesce_DATETIME(r *DATETIME, a_list []*DATETIME) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = elem.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME, c *DATETIME) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	r_val = !a.data_val.Before(b.data_val.Time) && !a.data_val.After(c.data_val.Time)

	r.data_val = r_val
}

func Sysfunc_not_between_DATETIME(r *BOOLEAN, a *DATETIME, b *DATETIME, c *DATETIME) {

	Sysfunc_between_DATETIME(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_DATETIME(r *VARCHAR, a *DATETIME) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "DATETIME"...)
}

// Sysfunc_aggr_count_DATETIME is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_DATETIME(r *INT, a *DATETIME) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val >= math.MaxInt32 { // check that r.data_val won't go beyond MaxInt32
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_count_big_DATETIME is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_big_DATETIME(r *BIGINT, a *DATETIME) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val == math.MaxInt64 { // check that r.data_val won't go beyond MaxInt64
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_min_DATETIME is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_min_DATETIME(r *DATETIME, a *DATETIME) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val.Before(r.data_val.Time) {
		r.data_val = a.data_val
	}
}

// Sysfunc_aggr_max_DATETIME is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_max_DATETIME(r *DATETIME, a *DATETIME) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val.After(r.data_val.Time) {
		r.data_val = a.data_val
	}
}
