package data

import (
	"bytes"
	"fmt"
	"math"
	"strconv"
	"time"
	"unicode"
	"unicode/utf8"

	"golang.org/x/text/unicode/norm"

	"rsql"
	"rsql/like"
	"rsql/mdat"
	"rsql/mstr"
)

// Sysfunc_compile_VARCHAR_to_REGEXPLIKE compiles a LIKE expression passed as VARCHAR, into a REGEXPLIKE.
// escape_char must contain exaclty one rune or be NULL, else the result will be an error. If escape_char is NULL, there is no escape character.
//
func Sysfunc_compile_VARCHAR_to_REGEXPLIKE(r *REGEXPLIKE, a *VARCHAR, escape_char *VARCHAR) {
	var (
		val  like.Likexp_bundle
		ec   rune
		size int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* check escape char */

	if escape_char.data_error != nil {
		r.data_error = escape_char.data_error
		return
	}

	ec = like.NO_ESCAPE_RUNE // default if escape_char is NULL

	if escape_char.data_NULL_flag == false {
		if ec, size = utf8.DecodeRune(escape_char.data_val); size != len(escape_char.data_val) || len(escape_char.data_val) == 0 {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_ESCAPE_CHAR, rsql.ERROR_BATCH_ABORT, a.data_val)
			return
		}
	}

	/* operation */

	if err := val.Parse_likexp(string(a.data_val), ec); err != nil {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONV_REGEXPLIKE_BAD_STRING, rsql.ERROR_BATCH_ABORT, a.data_val) // LIKE expression too complex (too many % or like.Likexps)
		return
	}

	r.data_val = val
}

// Sysfunc_charlen_VARCHAR returns the length of a string, in runes.
//
// This function returns the total length of the string. It returns the length of the Mystr stored in the Data_VARCHAR, including all trailing blanks.
//
//     Example : datalength('abc   ') = 6
//
// IMPORTANT: this function does not exists in MS SQL Server.
//            With MS SQL Server, you must use LEN(), which excludes trailing spaces, or DATALENGTH(), which returns the number of bytes, not number of characters.
//            CHARLEN() is a much needed function, but incompatible with MS SQL Server.
//            I could have named it STRLEN but CHARLEN sounds better to me.
//
func Sysfunc_charlen_VARCHAR(r *INT, a *VARCHAR) {
	var (
		val       mstr.Mystr
		runecount int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = a.data_val

	if len(val) == 0 {
		r.data_val = 0
		return
	}

	runecount = utf8.RuneCount(val)

	r.data_val = int64(runecount)
}

// Sysfunc_len_VARCHAR returns the length of a string, in runes.
//
// This function returns the length of the string, excluding trailing spaces.
//
//     All trailing spaces are discarded.
//     Example : len('abc   ') = 3
//
func Sysfunc_len_VARCHAR(r *INT, a *VARCHAR) {
	var (
		val       mstr.Mystr
		i         int
		runecount int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = a.data_val

	for i = len(val) - 1; i >= 0; i-- { // skip all blanks at end of string
		if val[i] != ' ' {
			break
		}
	}
	val = val[:i+1]

	if len(val) == 0 {
		r.data_val = 0
		return
	}

	runecount = utf8.RuneCount(val)

	r.data_val = int64(runecount)
}

// Sysfunc_ascii_VARCHAR return the code of the first rune.
// If string is empty, returns NULL.
// Same as Sysfunc_unicode_VARCHAR.
//
func Sysfunc_ascii_VARCHAR(r *INT, a *VARCHAR) {
	var (
		a_val      mstr.Mystr
		code_point rune
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if len(a_val) == 0 {
		r.data_NULL_flag = true // if empty string, return NULL
		return
	}

	code_point, _ = utf8.DecodeRune(a_val)

	r.data_val = int64(code_point)
}

// Sysfunc_unicode_VARCHAR return the code of the first rune.
// If string is empty, returns NULL.
// Same as Sysfunc_ascii_VARCHAR.
//
func Sysfunc_unicode_VARCHAR(r *INT, a *VARCHAR) {
	var (
		a_val      mstr.Mystr
		code_point rune
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if len(a_val) == 0 {
		r.data_NULL_flag = true // if empty string, return NULL
		return
	}

	code_point, _ = utf8.DecodeRune(a_val)

	r.data_val = int64(code_point)
}

// Sysfunc_char_VARCHAR return the character with code point given by argument.
// Same as Sysfunc_nchar_VARCHAR.
//
//       Target: fixlen_flag = false, precision = 1, collation is default.
//
func Sysfunc_char_VARCHAR(r *VARCHAR, a *INT) {
	var (
		a_val    rune
		bytebuff [utf8.UTFMax]byte // one character can take up to 4 bytes in utf8
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = rune(a.data_val)

	if a_val < 0 || a_val > unicode.MaxRune {
		r.data_error = rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_UNICOE_CODEPOINT_INVALID, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	n := utf8.EncodeRune(bytebuff[:], a_val)

	r.data_val = append(r.data_val[:0], bytebuff[:n]...)
}

// Sysfunc_nchar_VARCHAR return the character with code point given by argument.
// Same as Sysfunc_char_VARCHAR.
//
//       Target: fixlen_flag = false, precision = 1, collation is default.
//
func Sysfunc_nchar_VARCHAR(r *VARCHAR, a *INT) {
	var (
		a_val    rune
		bytebuff [utf8.UTFMax]byte // one character can take up to 4 bytes in utf8
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = rune(a.data_val)

	if a_val < 0 || a_val > unicode.MaxRune {
		r.data_error = rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_UNICOE_CODEPOINT_INVALID, rsql.ERROR_BATCH_ABORT, a_val)
		return
	}

	n := utf8.EncodeRune(bytebuff[:], a_val)

	r.data_val = append(r.data_val[:0], bytebuff[:n]...)
}

// Sysfunc_ltrim_VARCHAR discards all leading blanks.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//
func Sysfunc_ltrim_VARCHAR(r *VARCHAR, a *VARCHAR) {
	var (
		val mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = a.data_val

	if len(val) == 0 {
		r.data_val = r.data_val[:0] // if empty string, result is also empty string
		return
	}

	val = bytes.TrimLeftFunc(val, unicode.IsSpace)

	r.data_val = append(r.data_val[:0], val...)
}

// Sysfunc_rtrim_VARCHAR discards all trailing blanks.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//
func Sysfunc_rtrim_VARCHAR(r *VARCHAR, a *VARCHAR) {
	var (
		val mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = a.data_val

	if len(val) == 0 {
		r.data_val = r.data_val[:0] // if empty string, result is also empty string
		return
	}

	val = bytes.TrimRightFunc(val, unicode.IsSpace)

	r.data_val = append(r.data_val[:0], val...)
}

// Sysfunc_trim_VARCHAR discards all leading and trailing blanks.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//
func Sysfunc_trim_VARCHAR(r *VARCHAR, a *VARCHAR) {
	var (
		val mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val = a.data_val

	if len(val) == 0 {
		r.data_val = r.data_val[:0] // if empty string, result is also empty string
		return
	}

	val = bytes.TrimSpace(val) // trim left and right whitespaces

	r.data_val = append(r.data_val[:0], val...)
}

// Sysfunc_lower_VARCHAR converts string to lower case.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//
func Sysfunc_lower_VARCHAR(r *VARCHAR, a *VARCHAR) {
	var (
		res mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res = mstr.Map(r.data_val, a.data_val, unicode.ToLower)

	r.data_val = res
}

// Sysfunc_upper_VARCHAR converts string to upper case.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//
func Sysfunc_upper_VARCHAR(r *VARCHAR, a *VARCHAR) {
	var (
		res mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	res = mstr.Map(r.data_val, a.data_val, unicode.ToUpper)

	r.data_val = res
}

// Sysfunc_space_VARCHAR returns a string with only spaces.
// If a is NULL or a < 0, a NULL string is returned.
//
//      Target: fixlen_flag = false, precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is default.
//              If argument is a literal INT:
//                       - the precision is this value. Precision is adjusted to the range [1...DATATYPE_VARCHAR_PRECISION_MAX].
//                       - if argument is NULL, precision is 1.
//
func Sysfunc_space_VARCHAR(r *VARCHAR, a *INT) {
	var (
		nb            int64
		buff          []byte
		buff_capacity int
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	nb = a.data_val

	if nb < 0 { // if argument is negative, return NULL string
		r.data_NULL_flag = true
		return
	}

	if nb == 0 {
		r.data_val = r.data_val[:0] // if empty string, result is also empty string
		return
	}

	// if nb is too big, silently truncates to target precision

	if nb > int64(r.Data_precision) {
		nb = int64(r.Data_precision)
	}

	// make buff as large as nb

	buff = r.data_val

	buff_capacity = cap(buff)
	if buff_capacity < int(nb) {
		buff_capacity = int(nb)
		buff = make([]byte, buff_capacity)
	}

	buff = buff[:buff_capacity]

	for i := 0; i < int(nb); i++ {
		buff[i] = ' '
	}

	r.data_val = buff[:nb]
}

// Sysfunc_replicate_VARCHAR returns a string copied many times.
// If b is NULL or b < 0, a NULL string is returned.
//
//      Target: fixlen_flag = false, precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is default.
//              If argument 'b' is a literal INT:
//                       - the precision is this value * precision of a. Precision is adjusted to the range [1...DATATYPE_VARCHAR_PRECISION_MAX].
//                       - if argument 'b' is NULL, precision is 1.
//
func Sysfunc_replicate_VARCHAR(r *VARCHAR, a *VARCHAR, b *INT) {
	var (
		res                         mstr.Mystr
		nb                          int
		rune_count_a                int
		rune_count_total            int
		estimated_result_rune_count int
		estimated_result_length     int
		max_result_length           int
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is always variable length VARCHAR

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	nb = int(b.data_val)

	if nb < 0 { // if count is negative, return NULL string
		r.data_NULL_flag = true
		return
	}

	if nb == 0 || len(a.data_val) == 0 {
		r.data_val = r.data_val[:0] // if empty string, result is also empty string
		return
	}

	// make nb copies of string

	rune_count_a = utf8.RuneCount(a.data_val) // never 0, as a.data_val contains at least one byte here

	estimated_result_rune_count = nb * rune_count_a // never 0, as nb > 0 here. Because normalization can occur between all appended segments, estimated_result_rune_count can be a little more or less than real count.
	if estimated_result_rune_count > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
		estimated_result_rune_count = rsql.DATATYPE_VARCHAR_PRECISION_MAX
	}
	estimated_result_length = (estimated_result_rune_count*len(a.data_val))/rune_count_a + 1 // +1 because integer division / truncates the number

	res = r.data_val[:0]
	if cap(res) < estimated_result_length {
		res = make([]byte, 0, estimated_result_length)
	}

	max_result_length = estimated_result_length + 100 // +100 is arbitrary
	for i := 0; i < nb; i++ {
		res = append(res, a.data_val...) // append with no normalization
		if len(res) > max_result_length {
			break
		}
	}

	res = norm.NFC.Bytes(res) // normalize

	rune_count_total = utf8.RuneCount(res)
	if rune_count_total > int(r.Data_precision) { // if string is too big, truncate to target precision
		res.Truncate_to_precision(r.Data_precision)
	}

	r.data_val = res
}

// Sysfunc_left_VARCHAR returns the left substring with 'b' number of runes.
// If b < 0, an error is returned.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//              If argument b is a literal INT:
//                       - the precision is this value. Precision is adjusted to the range [1...precision of 'a'].
//                       - if b is NULL, precision is 1.
//
func Sysfunc_left_VARCHAR(r *VARCHAR, a *VARCHAR, b *INT) {
	var (
		res mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is variable length

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if b.data_val < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_LEFT_RIGHT_BAD_ARGUMENT, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	res = mstr.Left(r.data_val, a.data_val, int(b.data_val))

	r.data_val = res
}

// Sysfunc_right_VARCHAR returns the right substring with 'b' number of runes.
// If b < 0, an error is returned.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//              If argument b is a literal INT:
//                       - the precision is this value. Precision is adjusted to the range [1...precision of 'a'].
//                       - if b is NULL, precision is 1.
//
func Sysfunc_right_VARCHAR(r *VARCHAR, a *VARCHAR, b *INT) {
	var (
		res mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is variable length

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	if b.data_val < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_LEFT_RIGHT_BAD_ARGUMENT, rsql.ERROR_BATCH_ABORT, b.data_val)
		return
	}

	res = mstr.Right(r.data_val, a.data_val, int(b.data_val))

	r.data_val = res
}

// Sysfunc_stuff_VARCHAR returns a copy of string 'a', with section at 'start' and 'length' deleted, replaced by 'repl'.
// If len(a) == 0 or start <= 0 or length < 0, a NULL string is returned.
//
//      Target: fixlen_flag = false, precision is sum of 'a' argument string and 'repl' string. Collation is same as 'a' argument.
//              Precision is sum of precision of a and precision of repl, limited to DATATYPE_VARCHAR_PRECISION_MAX.
//
// 'start' argument is 1-based.
//
//       Note: stuff('hello', 6, 1, 'XXXXXXXXXX') returns NULL. Unfortunately, it is not 'helloXXXXXXXXXX'. I just reproduce the behavior of MS SQL Server.
//
func Sysfunc_stuff_VARCHAR(r *VARCHAR, a *VARCHAR, start *INT, length *INT, repl *VARCHAR) {
	var (
		res           mstr.Mystr
		res_NULL_flag bool
		replacement   mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is variable length

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &start.Header, &length.Header) {
		return
	}

	/* check if repl is error or NULL */

	if repl.data_error != nil {
		r.data_error = repl.data_error
		return
	}

	if repl.data_NULL_flag == false { // if repl is not NULL, replacement is repl string. Else, it is empty string.
		replacement = repl.data_val
	}

	/* operation */

	if len(a.data_val) == 0 || start.data_val <= 0 || length.data_val < 0 {
		r.data_NULL_flag = true
		return
	}

	if res_NULL_flag, res = mstr.Stuff(r.data_val, a.data_val, int(start.data_val), int(length.data_val), replacement); res_NULL_flag == true {
		r.data_NULL_flag = true
		return
	}

	// check for overflow

	if len(res) > rsql.DATATYPE_VARCHAR_PRECISION_MAX { // if precision < DATATYPE_VARCHAR_PRECISION_MAX, it has been defined to always be large enough to receive the result
		if runecount := utf8.RuneCount(res); runecount > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
			return
		}
	}

	r.data_val = res
}

// Sysfunc_substring_VARCHAR returns a copy of a substring of 'a'.
// If 'length' argument < 0, an error is returned.
//
//      Target: fixlen_flag = false, precision and collation are same as 'a' argument.
//              If argument 'length' is a literal INT:
//                       - the precision is this value. Precision is adjusted to the range [1...precision of 'a'].
//
func Sysfunc_substring_VARCHAR(r *VARCHAR, a *VARCHAR, start *INT, length *INT) {
	var (
		res mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is variable length

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &start.Header, &length.Header) {
		return
	}

	/* operation */

	if length.data_val < 0 {
		r.data_error = rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARCHAR_SUBSTRING_BAD_LENGTH, rsql.ERROR_BATCH_ABORT, length.data_val)
		return
	}

	if len(a.data_val) == 0 || start.data_val > int64(len(a.data_val)) || length.data_val == 0 {
		r.data_val = r.data_val[:0]
		return
	}

	res = mstr.Substring(r.data_val, a.data_val, int(start.data_val), int(length.data_val))

	r.data_val = res
}

// Sysfunc_charindex_VARCHAR returns the index (1-based) of the substring to find, or 0 if not found.
//
func Sysfunc_charindex_VARCHAR(r *INT, findstr *VARCHAR, s *VARCHAR, start *INT, coll *SYSCOLLATOR) {
	var (
		found_rune_index int
		rsql_err         *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&findstr.Header, &s.Header, &start.Header, &coll.Header) {
		return
	}

	/* operation */

	if len(s.data_val) == 0 || start.data_val > int64(len(s.data_val)) || len(findstr.data_val) == 0 {
		r.data_val = 0
		return
	}

	_, _, found_rune_index = mstr.Charindex(s.data_val, int(start.data_val), findstr.data_val, -1, coll.data_cached_collator)

	r.data_val = int64(found_rune_index)
}

// Sysfunc_replace_VARCHAR returns a copy of 'a', with all specified substring replaces by another string.
//
//      Target: fixlen_flag = false, precision is DATATYPE_VARCHAR_PRECISION_MAX, collation is same as 'a' argument. Instruction collation encompasses those of 'a' and 'findstr'-
//
func Sysfunc_replace_VARCHAR(r *VARCHAR, a *VARCHAR, findstr *VARCHAR, replacement *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res      mstr.Mystr
		rsql_err *rsql.Error
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is variable length

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &findstr.Header, &replacement.Header, &coll.Header) {
		return
	}

	/* operation */

	if len(a.data_val) == 0 {
		r.data_val = r.data_val[:0]
		return
	}

	if len(findstr.data_val) == 0 {
		r.data_val = append(r.data_val[:0], a.data_val...)
		return
	}

	res = mstr.Replace(r.data_val, a.data_val, findstr.data_val, replacement.data_val, coll.data_cached_collator)

	// check for overflow

	if len(res) > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
		if runecount := utf8.RuneCount(res); runecount > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
			return
		}
	}

	r.data_val = res
}

// Sysfunc_starts_with_VARCHAR returns true if a string starts with prefix.
// Pass the rune count of prefix in prefix_rune_count_hint, or -1 if it is unknown.
//
//       This function is not a SQL Server function, but is used for expression like    country LIKE 'fr%'
//
func Sysfunc_starts_with_VARCHAR(r *BOOLEAN, a *VARCHAR, prefix *VARCHAR, prefix_rune_count_hint *INT, coll *SYSCOLLATOR) {
	var (
		res      bool
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &prefix.Header, &prefix_rune_count_hint.Header, &coll.Header) {
		return
	}

	/* operation */

	res = mstr.Starts_with(a.data_val, prefix.data_val, int(prefix_rune_count_hint.data_val), coll.data_cached_collator)

	r.data_val = res
}

// Sysfunc_concat_VARCHAR returns contatenation of all arguments, with NULL replaced by empty string.
//
//      Target: fixlen_flag = false, precision is sum of all arguments precisions, limited to DATATYPE_VARCHAR_PRECISION_MAX. Collation depends on all argument.
//
func Sysfunc_concat_VARCHAR(r *VARCHAR, a_list []*VARCHAR) {
	var (
		res mstr.Mystr
	)

	rsql.Assert(r.Data_fixlen_flag == false) // result is variable length

	/* if error in operand, returns error. If NULL operand is found, DON'T RETURN !!!!!!!!! */

	for _, a := range a_list {
		if a.data_error != nil {
			r.data_error = a.data_error
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = false

	/* operation */

	res = r.data_val[:0]

	for _, s := range a_list {

		if s.data_NULL_flag == true || len(s.data_val) == 0 { // if NULL or empty string, skip it
			continue
		}

		if len(res) != 0 {
			res = norm.NFC.Append(res, s.data_val...)
		} else {
			res = append(res, s.data_val...)
		}

		if len(res) > rsql.DATATYPE_VARCHAR_PRECISION_MAX*utf8.UTFMax { // result string is getting really too long, stop replacing
			break
		}
	}

	// check for overflow

	if len(res) > rsql.DATATYPE_VARCHAR_PRECISION_MAX { // if precision < DATATYPE_VARCHAR_PRECISION_MAX, it has been defined to always be large enough to receive the result
		if runecount := utf8.RuneCount(res); runecount > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_OVERFLOW, rsql.ERROR_BATCH_ABORT, runecount, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
			return
		}
	}

	r.data_val = res
}

func Sysfunc_convert_VARCHAR_to_DATE(r *DATE, a *VARCHAR, style *INT, syslang *SYSLANGUAGE) {
	var (
		err error
		res time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &style.Header, &syslang.Header) {
		return
	}

	/* operation */

	if res, err = mdat.Parse(a.data_val, syslang.data_DMY, style.data_val, mdat.PARSE_MODE_DATE); err != nil {
		switch err {
		case mdat.Error_style_not_supported:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_DATE_UNSUPPORTED_STYLE, rsql.ERROR_BATCH_ABORT, style.data_val)
		case mdat.Error_wrong_parts_order:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_DATE_WRONG_PARTS_ORDER, rsql.ERROR_BATCH_ABORT, a.data_val)
		default:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_DATE_FAILED, rsql.ERROR_BATCH_ABORT, a.data_val)
		}
		return
	}

	r.data_val.Time = res
}

func Sysfunc_convert_VARCHAR_to_TIME(r *TIME, a *VARCHAR, style *INT, syslang *SYSLANGUAGE) {
	var (
		err error
		res time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &style.Header, &syslang.Header) {
		return
	}

	/* operation */

	if res, err = mdat.Parse(a.data_val, syslang.data_DMY, style.data_val, mdat.PARSE_MODE_TIME); err != nil {
		switch err {
		case mdat.Error_style_not_supported:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_TIME_UNSUPPORTED_STYLE, rsql.ERROR_BATCH_ABORT, style.data_val)
		case mdat.Error_wrong_parts_order:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_TIME_WRONG_PARTS_ORDER, rsql.ERROR_BATCH_ABORT, a.data_val)
		default:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_TIME_FAILED, rsql.ERROR_BATCH_ABORT, a.data_val)
		}
		return
	}

	r.data_val.Time = res
}

func Sysfunc_convert_VARCHAR_to_DATETIME(r *DATETIME, a *VARCHAR, style *INT, syslang *SYSLANGUAGE) {
	var (
		err error
		res time.Time
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &style.Header, &syslang.Header) {
		return
	}

	/* operation */

	if res, err = mdat.Parse(a.data_val, syslang.data_DMY, style.data_val, mdat.PARSE_MODE_DATETIME); err != nil {
		switch err {
		case mdat.Error_style_not_supported:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_DATETIME_UNSUPPORTED_STYLE, rsql.ERROR_BATCH_ABORT, style.data_val)
		case mdat.Error_wrong_parts_order:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_DATETIME_WRONG_PARTS_ORDER, rsql.ERROR_BATCH_ABORT, a.data_val)
		default:
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARCHAR_CONVERT_DATETIME_FAILED, rsql.ERROR_BATCH_ABORT, a.data_val)
		}
		return
	}

	r.data_val.Time = res
}

func Sysfunc_isdate_VARCHAR(r *INT, a *VARCHAR, syslang *SYSLANGUAGE) {
	var (
		err error
		i   int
		res int64
	)

	/* check for error or NULL operand */

	r.data_error = nil
	r.data_NULL_flag = false

	if syslang.data_error != nil {
		r.data_error = syslang.data_error
		return
	}

	if syslang.data_NULL_flag == true {
		r.data_NULL_flag = true
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true { // if NULL, result is false
		r.data_val = 0
		return
	}

	/* operation */

	s := a.data_val

	for i = 0; i < len(s); i++ { // skip blanks
		if s[i] != ' ' {
			break
		}
	}
	s = s[i:]

	if len(s) == 0 { // if empty string, result is false
		r.data_val = 0
		return
	}

	res = 1

	if _, err = mdat.Parse(s, syslang.data_DMY, 0, mdat.PARSE_MODE_DATETIME); err != nil {
		res = 0
	}

	r.data_val = res
}

func Sysfunc_isnumeric_VARCHAR(r *INT, a *VARCHAR) {
	var (
		err error
		val mstr.Mystr
		res float64
	)

	r.data_error = a.data_error
	r.data_NULL_flag = false
	r.data_val = 0

	if a.data_error != nil || a.data_NULL_flag == true {
		return
	}

	val = bytes.TrimSpace(a.data_val)

	if len(val) == 0 { // if empty string, return 0
		return
	}

	if res, err = strconv.ParseFloat(string(val), 64); err != nil {
		return
	}

	if math.IsNaN(res) || math.IsInf(res, 0) { // check if NaN, +Inf or -Inf
		return
	}

	r.data_val = 1
}

func Sysfunc_isnull_VARCHAR(r *VARCHAR, a *VARCHAR, b *VARCHAR) {

	if a.data_error == nil && a.data_NULL_flag == false { // if a not NULL, copy it to r
		r.data_error = nil
		r.data_NULL_flag = false
		r.data_val = append(r.data_val[:0], a.data_val...)
		return
	}

	if a.data_error != nil { // if a is error, r is also error
		r.data_error = a.data_error
		return
	}

	if b.data_error != nil { // if b is error, r is also error
		r.data_error = b.data_error
		return
	}

	r.data_error = nil // else, copy b to r
	r.data_NULL_flag = b.data_NULL_flag
	if b.data_NULL_flag == false {
		r.data_val = append(r.data_val[:0], b.data_val...)
	}
}

func Sysfunc_iif_VARCHAR(r *VARCHAR, a *BOOLEAN, b *VARCHAR, c *VARCHAR) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		if b.data_error == nil && b.data_NULL_flag == false {
			r.data_val = append(r.data_val[:0], b.data_val...)
		}
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	if c.data_error == nil && c.data_NULL_flag == false {
		r.data_val = append(r.data_val[:0], c.data_val...)
	}
}

func Sysfunc_choose_VARCHAR(r *VARCHAR, a *INT, b_list []*VARCHAR) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	if elem.data_error == nil && elem.data_NULL_flag == false {
		r.data_val = append(r.data_val[:0], elem.data_val...)
	}
}

func Sysfunc_coalesce_VARCHAR(r *VARCHAR, a_list []*VARCHAR) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = append(r.data_val[:0], elem.data_val...)
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, c *VARCHAR, coll *SYSCOLLATOR) {
	var (
		res_b    int
		res_c    int
		rsql_err *rsql.Error
	)

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header, &coll.Header) {
		return
	}

	/* operation */

	res_b = mstr.Compare(a.data_val, b.data_val, coll.data_cached_collator) // never fails
	res_c = mstr.Compare(a.data_val, c.data_val, coll.data_cached_collator) // never fails

	r.data_val = (res_b >= 0 && res_c <= 0)
}

func Sysfunc_not_between_VARCHAR(r *BOOLEAN, a *VARCHAR, b *VARCHAR, c *VARCHAR, coll *SYSCOLLATOR) {

	Sysfunc_between_VARCHAR(r, a, b, c, coll)

	r.data_val = !r.data_val
}

// Sysfunc_fit_no_truncate_VARCHAR puts an error in 'r' if 'a' would be truncated because 'r' precision is too small.
// The user cannot call this function. It is generated by the decorator for INSERT INTO and UPDATE statements.
//
func Sysfunc_fit_no_truncate_VARCHAR(r *VARCHAR, a *VARCHAR) {
	var (
		rsql_err *rsql.Error
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	source := a.data_val
	if len(source) > int(r.Data_precision)*utf8.UTFMax {
		source = source[:r.Data_precision*utf8.UTFMax]
	}

	r.data_val = append(r.data_val[:0], source...)

	if r.Data_fixlen_flag == false { // if target is not fixlen and string too long, try to remove some trailing spaces
		if len(r.data_val) > int(r.Data_precision) { // fast but conservative check
			if rsql_err = r.data_val.Fit_to_precision_no_truncate(r.Data_precision); rsql_err != nil {
				r.data_error = rsql_err
				return
			}
		}
	} else {
		if rsql_err = r.data_val.Fit_to_precision_no_truncate(r.Data_precision); rsql_err != nil {
			r.data_error = rsql_err
			return
		}
		r.data_val.Pad_or_truncate_to_precision(r.Data_precision) // if target is fixlen, the rune count must be equal to target precision. So, we append spaces if needed.
	}

}

func Sysfunc_typeof_VARCHAR(r *VARCHAR, a *VARCHAR) {

	r.data_error = nil
	r.data_NULL_flag = false

	datatype_string := "VARCHAR(%d)"
	if a.Data_fixlen_flag {
		datatype_string = "CHAR(%d)"
	}

	r.data_val = append(r.data_val[:0], fmt.Sprintf(datatype_string, a.Data_precision)...)
}

// Sysfunc_aggr_count_VARCHAR is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_VARCHAR(r *INT, a *VARCHAR) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val >= math.MaxInt32 { // check that r.data_val won't go beyond MaxInt32
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_count_big_VARCHAR is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_big_VARCHAR(r *BIGINT, a *VARCHAR) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val == math.MaxInt64 { // check that r.data_val won't go beyond MaxInt64
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_min_VARCHAR is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_min_VARCHAR(r *VARCHAR, a *VARCHAR, coll *SYSCOLLATOR) {
	var rsql_err *rsql.Error

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* process error and NULL */

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = append(r.data_val[:0], a.data_val...)

		return
	}

	if mstr.Compare(a.data_val, r.data_val, coll.data_cached_collator) < 0 {
		r.data_val = append(r.data_val[:0], a.data_val...)
	}
}

// Sysfunc_aggr_max_VARCHAR is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_max_VARCHAR(r *VARCHAR, a *VARCHAR, coll *SYSCOLLATOR) {
	var rsql_err *rsql.Error

	/* create collator lazily if needed */

	if coll.data_error == nil && coll.data_NULL_flag == false && coll.data_cached_collator == nil {
		if coll.data_cached_collator, rsql_err = rsql.Get_collator(coll.data_collation_string); rsql_err != nil {
			coll.data_error = rsql_err
		}
	}

	/* process error and NULL */

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = append(r.data_val[:0], a.data_val...)

		return
	}

	if mstr.Compare(a.data_val, r.data_val, coll.data_cached_collator) > 0 {
		r.data_val = append(r.data_val[:0], a.data_val...)
	}
}
