package data

import (
	"bytes"
	"fmt"
	"math"

	"rsql"
	"rsql/mbin"
)

const MYBIN_CAPACITY_DEFAULT = 30

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *VARBINARY) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_VARBINARY)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	hash += uint32(a.Data_precision)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += rsql.Joaat_hash(a.data_val)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *VARBINARY) String() string {
	const MAX_LEN = 30 // >=2

	typ := "VARBINARY"

	if a.data_error != nil {
		return fmt.Sprintf("%s(%d): error", typ, a.Data_precision)
	}

	if a.data_NULL_flag == true {
		return fmt.Sprintf("%s(%d): null", typ, a.Data_precision)
	}

	if len(a.data_val) > (MAX_LEN-2)/2 {
		return fmt.Sprintf("%s(%d): %x...", typ, a.Data_precision, a.data_val[:(MAX_LEN-2)/2])
	}

	return fmt.Sprintf("%s(%d): %x", typ, a.Data_precision, a.data_val)
}

// New_VARBINARY_NULL creates a new data.VARBINARY initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_VARBINARY_NULL(kind rsql.Kind_t, precision uint16) *VARBINARY {

	rsql.Assert(precision > 0)

	dataslot := &VARBINARY{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VARBINARY,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		Data_precision: precision,
		data_val:       make(mbin.Mybin, 0, MYBIN_CAPACITY_DEFAULT),
	}

	return dataslot
}

// New_literal_VARBINARY_value creates a new data.VARBINARY from a value.
//
func New_literal_VARBINARY_value(val []byte) (*VARBINARY, *rsql.Error) {

	number_of_bytes := len(val)
	if number_of_bytes > rsql.DATATYPE_VARBINARY_PRECISION_MAX {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARBINARY_OVERFLOW, rsql.ERROR_BATCH_ABORT, number_of_bytes, rsql.DATATYPE_VARBINARY_PRECISION_MAX)
	}

	dataslot := &VARBINARY{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VARBINARY,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		Data_precision: uint16(number_of_bytes),
		data_val:       mbin.Mybin(val),
	}

	if dataslot.Data_precision == 0 { // for literal empty binary string, we want at least a precision of 1.
		dataslot.Data_precision = 1
	}

	return dataslot, nil
}

// New_literal_VARBINARY creates a new data.VARBINARY from literal hexastring string.
//
// Lex_type must be prefixed by "0x".
//
func New_literal_VARBINARY(s string) (*VARBINARY, *rsql.Error) {

	rsql.Assert(len(s)%2 == 0)

	number_of_bytes := (len(s) - 2) / 2 // LEX_LITERAL_HEXASTRING has always an even length, 2 hex digits per byte, and has a prefix "0x"
	if number_of_bytes > rsql.DATATYPE_VARBINARY_PRECISION_MAX {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQLDATA_VARBINARY_OVERFLOW, rsql.ERROR_BATCH_ABORT, number_of_bytes, rsql.DATATYPE_VARBINARY_PRECISION_MAX)
	}

	dataslot := &VARBINARY{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VARBINARY,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		Data_precision: uint16(number_of_bytes),
		data_val:       nil,
	}

	if dataslot.Data_precision == 0 { // for literal empty binary string, we want a precision of 1.
		dataslot.Data_precision = 1
	}

	if rsql_err := mbin.Hexa2bin(&dataslot.data_val, []byte(s)); rsql_err != nil { // convert hexastring beginning with "0x" to slice of bytes. Empty byte slice is allowed.
		return nil, rsql_err
	}

	return dataslot, nil
}

func Add_VARBINARY(r *VARBINARY, a *VARBINARY, b *VARBINARY) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* check that string length is <= precision */

	// usually, should not happen, as the target precision is normally big enough.
	// Only concatenation of large strings can return this error.

	if len(a.data_val)+len(b.data_val) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARBINARY_OVERFLOW, rsql.ERROR_BATCH_ABORT, len(a.data_val)+len(b.data_val), r.Data_precision)
		return
	}

	/* concatenate */

	r.data_val = append(r.data_val[:0], a.data_val...)
	r.data_val = append(r.data_val, b.data_val...)
}

func Comp_equal_VARBINARY(r *BOOLEAN, a *VARBINARY, b *VARBINARY) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = bytes.Compare(a.data_val, b.data_val)

	r.data_val = (res == 0)
}

func Comp_greater_VARBINARY(r *BOOLEAN, a *VARBINARY, b *VARBINARY) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = bytes.Compare(a.data_val, b.data_val)

	r.data_val = (res > 0)
}

func Comp_less_VARBINARY(r *BOOLEAN, a *VARBINARY, b *VARBINARY) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = bytes.Compare(a.data_val, b.data_val)

	r.data_val = (res < 0)
}

func Comp_greater_equal_VARBINARY(r *BOOLEAN, a *VARBINARY, b *VARBINARY) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = bytes.Compare(a.data_val, b.data_val)

	r.data_val = (res >= 0)
}

func Comp_less_equal_VARBINARY(r *BOOLEAN, a *VARBINARY, b *VARBINARY) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = bytes.Compare(a.data_val, b.data_val)

	r.data_val = (res <= 0)
}

func Comp_not_equal_VARBINARY(r *BOOLEAN, a *VARBINARY, b *VARBINARY) {
	var (
		res int
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	res = bytes.Compare(a.data_val, b.data_val)

	r.data_val = (res != 0)
}

func Is_null_VARBINARY(r *BOOLEAN, a *VARBINARY) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_VARBINARY(r *BOOLEAN, a *VARBINARY) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_VARBINARY checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_VARBINARY(r *BOOLEAN, a *VARBINARY, b_list []*VARBINARY) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_VARBINARY(r, a, b)

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_VARBINARY(r *BOOLEAN, a *VARBINARY, b_list []*VARBINARY) {

	In_list_VARBINARY(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_VARBINARY(r *VARBINARY, a_list []Case_element_t) {
	var (
		elem_val *VARBINARY
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*VARBINARY)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			if elem_val.data_error == nil && elem_val.data_NULL_flag == false {
				r.data_val = append(r.data_val[:0], elem_val.data_val...)
			}
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VARBINARY_to_VARBINARY(r *VARBINARY, a *VARBINARY) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	val := a.data_val

	if len(val) > int(r.Data_precision) {
		val = a.data_val[:r.Data_precision]
	}

	r.data_val = append(r.data_val[:0], val...)

}

// Cast_VARBINARY_to_BIT casts VARBINARY to BIT.
// If VARBINARY != 0, BIT is set to 1. Else, BIT is set to 0.
//
func Cast_VARBINARY_to_BIT(r *BIT, a *VARBINARY) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	r.data_val = 0

	for _, b := range a.data_val { // if a non 0 byte is found, result is 1
		if b != 0 {
			r.data_val = 1
			break
		}
	}
}

func Cast_VARBINARY_to_TINYINT(r *TINYINT, a *VARBINARY) {
	var (
		rsql_err *rsql.Error
		uval     uint64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if uval, rsql_err = mbin.Bin2uint64(a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	if uval > math.MaxUint8 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARBINARY_CAST_TINYINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val)
		return
	}

	r.data_val = int64(uval)
}

func Cast_VARBINARY_to_SMALLINT(r *SMALLINT, a *VARBINARY) {
	var (
		rsql_err *rsql.Error
		uval     uint64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if uval, rsql_err = mbin.Bin2uint64(a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	if uval > math.MaxUint16 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARBINARY_CAST_SMALLINT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val)
		return
	}

	r.data_val = int64(int16(uval))
}

func Cast_VARBINARY_to_INT(r *INT, a *VARBINARY) {
	var (
		rsql_err *rsql.Error
		uval     uint64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if uval, rsql_err = mbin.Bin2uint64(a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	if uval > math.MaxUint32 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_VARBINARY_CAST_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val)
		return
	}

	r.data_val = int64(int32(uval))
}

func Cast_VARBINARY_to_BIGINT(r *BIGINT, a *VARBINARY) {
	var (
		rsql_err *rsql.Error
		uval     uint64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if uval, rsql_err = mbin.Bin2uint64(a.data_val); rsql_err != nil {
		r.data_error = rsql_err
		return
	}

	r.data_val = int64(uval)
}

func Assign_VARBINARY(r *VARBINARY, a *VARBINARY) *rsql.Error {

	rsql.Assert(r.Data_precision == a.Data_precision)

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	if a.data_error == nil && a.data_NULL_flag == false { // if a is not error nor NULL, copy string
		r.data_val = append(r.data_val[:0], a.data_val...)
	}

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

func Copy_VARBINARY(r *VARBINARY, a *VARBINARY) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	if r.data_error != nil || r.data_NULL_flag == true {
		return
	}

	r.data_val = append(r.data_val[:0], a.data_val...)
}

func Clone_VARBINARY(kind rsql.Kind_t, a *VARBINARY) *VARBINARY {

	return New_VARBINARY_NULL(kind, a.Data_precision)
}
