package data

import (
	"testing"

	"rsql"
)

var (
	samples_BOOLEAN = []tst_sample_t{
		{Logical_unary_not_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_unary_not_BOOLEAN, []interface{}{r_BOOLEAN, nil}, r_BOOLEAN, nil},
		{Logical_unary_not_BOOLEAN, []interface{}{r_BOOLEAN, false}, r_BOOLEAN, true},
		{Logical_unary_not_BOOLEAN, []interface{}{r_BOOLEAN, true}, r_BOOLEAN, false},

		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, false, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, false, nil}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, false, false}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, false, true}, r_BOOLEAN, false},

		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, true, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, true, nil}, r_BOOLEAN, nil},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, true, false}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, true, true}, r_BOOLEAN, true},

		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, false}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, nil, false}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, false, false}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, true, false}, r_BOOLEAN, false},

		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, true}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, nil, true}, r_BOOLEAN, nil},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, false, true}, r_BOOLEAN, false},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, true, true}, r_BOOLEAN, true},

		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_and_BOOLEAN, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},

		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, false, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, false, nil}, r_BOOLEAN, nil},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, false, false}, r_BOOLEAN, false},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, false, true}, r_BOOLEAN, true},

		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, true, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, true},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, true, nil}, r_BOOLEAN, true},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, true, false}, r_BOOLEAN, true},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, true, true}, r_BOOLEAN, true},

		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, false}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, nil, false}, r_BOOLEAN, nil},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, false, false}, r_BOOLEAN, false},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, true, false}, r_BOOLEAN, true},

		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, true}, r_BOOLEAN, true},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, nil, true}, r_BOOLEAN, true},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, false, true}, r_BOOLEAN, true},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, true, true}, r_BOOLEAN, true},

		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, rsql.ERROR_FOR_TESTING, nil}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, nil, rsql.ERROR_FOR_TESTING}, r_BOOLEAN, rsql.ERROR_FOR_TESTING},
		{Logical_or_BOOLEAN, []interface{}{r_BOOLEAN, nil, nil}, r_BOOLEAN, nil},
	}
)

func Test_operators_and_functions_for_BOOLEAN(t *testing.T) {

	// check all the samples

	for i, sample := range samples_BOOLEAN {

		tst_function_tester(sample.function, sample.operands...) // run the function to test

		if tst_function_result_checker(sample.result, sample.result_check) == false { // check result
			t.Fatalf("--------- Failure at samples[%d] for %s -------", i, sample.String())
		}
	}

}
