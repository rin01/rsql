package data

import (
	"math"
	"math/rand"
	"strings"
	"testing"

	"rsql"
)

/*
type tst_sample_t struct {
	function     interface{}   // function to test
	operands     []interface{} // function arguments
	result       interface{}   // argument that receives the result of the function
	result_check interface{}   // expected result
}
*/

func Test_compress_uncompress_int64(t *testing.T) {
	var (
		dest   []byte
		length uint16
		neg    uint8
	)

	for i := 0; i < 100000; i++ {
		a := rand.Int63()

		for k := uint(0); k < 9; k++ {
			b := int64(uint64(a) >> (k * 8))
			if neg != 0 {
				b = -b
			}

			dest, length = append_compressed_int64(dest[:0], b)
			if int(length) != len(dest) {
				panic("impossible")
			}

			b_res := uncompress_int64(dest)

			if b_res != b {
				t.Fatalf("%d != %d", b, b_res)
			}
			//fmt.Printf("%20d %20d  %10d\n", b, b_res, length)
		}

		neg ^= 0xff
	}
}

func serialize_deserialize_VARBINARY(r *VARBINARY, a *VARBINARY) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_VARBINARY(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(len(bbb) == int(length))

	deserialize_to_VARBINARY(r, bbb)
}

func serialize_deserialize_VARCHAR(r *VARCHAR, a *VARCHAR) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_VARCHAR(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(len(bbb) == int(length))

	deserialize_to_VARCHAR(r, bbb)
}

func serialize_deserialize_BIT(r *BIT, a *BIT) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_BIT(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(len(bbb) == 1)

	deserialize_to_BIT(r, bbb)
}

func serialize_deserialize_TINYINT(r *TINYINT, a *TINYINT) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_TINYINT(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(len(bbb) == 1)

	deserialize_to_TINYINT(r, bbb)
}

func serialize_deserialize_SMALLINT(r *SMALLINT, a *SMALLINT) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_SMALLINT(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(length >= 1 && length <= 2 && len(bbb) == int(length))

	deserialize_to_SMALLINT(r, bbb)
}

func serialize_deserialize_INT(r *INT, a *INT) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_INT(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(length >= 1 && length <= 4 && len(bbb) == int(length))

	deserialize_to_INT(r, bbb)
}

func serialize_deserialize_BIGINT(r *BIGINT, a *BIGINT) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_BIGINT(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(length >= 1 && length <= 8 && len(bbb) == int(length))

	deserialize_to_BIGINT(r, bbb)
}

func serialize_deserialize_NUMERIC(r *NUMERIC, a *NUMERIC) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_NUMERIC(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(len(bbb) == int(length))

	deserialize_to_NUMERIC(r, bbb)
}

func serialize_deserialize_FLOAT(r *FLOAT, a *FLOAT) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_FLOAT(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(len(bbb) == 8)

	deserialize_to_FLOAT(r, bbb)
}

func serialize_deserialize_DATE(r *DATE, a *DATE) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_DATE(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(length == 4 && len(bbb) == int(length))

	deserialize_to_DATE(r, bbb)
}

func serialize_deserialize_TIME(r *TIME, a *TIME) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_TIME(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(length >= 1 && length <= 8 && len(bbb) == int(length))

	deserialize_to_TIME(r, bbb)
}

func serialize_deserialize_DATETIME(r *DATETIME, a *DATETIME) {
	var (
		bbb    []byte
		length uint16
	)

	bbb, length = append_serialized_DATETIME(bbb, a)

	if length == 0 {
		r.Set_to_NULL()
		return
	}

	rsql.Assert(length == 12 || length == 8 || length == 4)
	rsql.Assert(len(bbb) == int(length))

	deserialize_to_DATETIME(r, bbb)
}

var (
	samples_serialization = []tst_sample_t{
		{serialize_deserialize_VARBINARY, []interface{}{r_VARBINARY_10, nil}, r_VARBINARY_10, nil},
		{serialize_deserialize_VARBINARY, []interface{}{r_VARBINARY_10, ""}, r_VARBINARY_10, ""},
		{serialize_deserialize_VARBINARY, []interface{}{r_VARBINARY_10, "d"}, r_VARBINARY_10, "d"},
		{serialize_deserialize_VARBINARY, []interface{}{r_VARBINARY_10, "avsfdghezu "}, r_VARBINARY_10, "avsfdghezu "},

		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, nil}, r_VARCHAR_50, nil},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, ""}, r_VARCHAR_50, ""},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, "a"}, r_VARCHAR_50, "a"},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, "a "}, r_VARCHAR_50, "a "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, "a         "}, r_VARCHAR_50, "a         "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, "          "}, r_VARCHAR_50, "          "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, " "}, r_VARCHAR_50, " "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, "東京のおすすめスポット"}, r_VARCHAR_50, "東京のおすすめスポット"},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, "東京のおすすめスポット "}, r_VARCHAR_50, "東京のおすすめスポット "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_50, "東京のおすすめスポット         "}, r_VARCHAR_50, "東京のおすすめスポット         "},

		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, nil}, r_VARCHAR_10_fix, nil},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, ""}, r_VARCHAR_10_fix, "          "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東"}, r_VARCHAR_10_fix, "東         "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東 "}, r_VARCHAR_10_fix, "東         "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東         "}, r_VARCHAR_10_fix, "東         "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東          "}, r_VARCHAR_10_fix, "東         "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東                 "}, r_VARCHAR_10_fix, "東         "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "          "}, r_VARCHAR_10_fix, "          "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, " "}, r_VARCHAR_10_fix, "          "},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東京のおすすめスポット"}, r_VARCHAR_10_fix, "東京のおすすめスポッ"},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東京のおすすめスポット "}, r_VARCHAR_10_fix, "東京のおすすめスポッ"},
		{serialize_deserialize_VARCHAR, []interface{}{r_VARCHAR_10_fix, "東京のおすすめスポット         "}, r_VARCHAR_10_fix, "東京のおすすめスポッ"},

		{serialize_deserialize_BIT, []interface{}{r_BIT, nil}, r_BIT, nil},
		{serialize_deserialize_BIT, []interface{}{r_BIT, 0}, r_BIT, 0},
		{serialize_deserialize_BIT, []interface{}{r_BIT, 1}, r_BIT, 1},

		{serialize_deserialize_TINYINT, []interface{}{r_TINYINT, nil}, r_TINYINT, nil},
		{serialize_deserialize_TINYINT, []interface{}{r_TINYINT, 123}, r_TINYINT, 123},
		{serialize_deserialize_TINYINT, []interface{}{r_TINYINT, 0}, r_TINYINT, 0},
		{serialize_deserialize_TINYINT, []interface{}{r_TINYINT, 255}, r_TINYINT, 255},

		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, nil}, r_SMALLINT, nil},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, 0}, r_SMALLINT, 0},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, -1}, r_SMALLINT, -1},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, 146}, r_SMALLINT, 146},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, -146}, r_SMALLINT, -146},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, 30124}, r_SMALLINT, 30124},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, -30124}, r_SMALLINT, -30124},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, math.MinInt16}, r_SMALLINT, math.MinInt16},
		{serialize_deserialize_SMALLINT, []interface{}{r_SMALLINT, math.MaxInt16}, r_SMALLINT, math.MaxInt16},

		{serialize_deserialize_INT, []interface{}{r_INT, nil}, r_INT, nil},
		{serialize_deserialize_INT, []interface{}{r_INT, 0}, r_INT, 0},
		{serialize_deserialize_INT, []interface{}{r_INT, -1}, r_INT, -1},
		{serialize_deserialize_INT, []interface{}{r_INT, 147}, r_INT, 147},
		{serialize_deserialize_INT, []interface{}{r_INT, 1234567890}, r_INT, 1234567890},
		{serialize_deserialize_INT, []interface{}{r_INT, -1234567890}, r_INT, -1234567890},
		{serialize_deserialize_INT, []interface{}{r_INT, math.MinInt32}, r_INT, math.MinInt32},
		{serialize_deserialize_INT, []interface{}{r_INT, math.MaxInt32}, r_INT, math.MaxInt32},

		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, nil}, r_BIGINT, nil},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, 0}, r_BIGINT, 0},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, -1}, r_BIGINT, -1},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, 147}, r_BIGINT, 147},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, -147}, r_BIGINT, -147},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, 7234567736458767122}, r_BIGINT, 7234567736458767122},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, -1234567736458767122}, r_BIGINT, -1234567736458767122},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, math.MinInt64}, r_BIGINT, math.MinInt64},
		{serialize_deserialize_BIGINT, []interface{}{r_BIGINT, math.MaxInt64}, r_BIGINT, math.MaxInt64},

		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, nil}, r_NUMERIC_12_2, nil},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, "-1"}, r_NUMERIC_12_2, "-1"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, "0"}, r_NUMERIC_12_2, "0"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_34_0, "7234567890123456789012345678901234"}, r_NUMERIC_34_0, "7234567890123456789012345678901234"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_34_0, "9999999999999999999999999999999999"}, r_NUMERIC_34_0, "9999999999999999999999999999999999"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_34_0, "-9999999999999999999999999999999999"}, r_NUMERIC_34_0, "-9999999999999999999999999999999999"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_34_34, "-.9999999999999999999999999999999999"}, r_NUMERIC_34_34, "-0.9999999999999999999999999999999999"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_34_34, "0.9999999999999999999999999999999999"}, r_NUMERIC_34_34, ".9999999999999999999999999999999999"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_34_34, "-.1234567890123456789012345678901234"}, r_NUMERIC_34_34, "-0.1234567890123456789012345678901234"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, "1234567890.12"}, r_NUMERIC_12_2, "1234567890.12"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, "-1234567890.12"}, r_NUMERIC_12_2, "-1234567890.12"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, "-Inf"}, r_NUMERIC_12_2, "-Inf"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, "+Inf"}, r_NUMERIC_12_2, "Inf"},
		{serialize_deserialize_NUMERIC, []interface{}{r_NUMERIC_12_2, "Nan"}, r_NUMERIC_12_2, "Nan"},

		{serialize_deserialize_FLOAT, []interface{}{r_FLOAT, nil}, r_FLOAT, nil},
		{serialize_deserialize_FLOAT, []interface{}{r_FLOAT, 12345678.9012345e27}, r_FLOAT, 12345678.9012345e27},
		{serialize_deserialize_FLOAT, []interface{}{r_FLOAT, -12345678.9012345e-127}, r_FLOAT, -12345678.9012345e-127},
		{serialize_deserialize_FLOAT, []interface{}{r_FLOAT, "-Inf"}, r_FLOAT, "-Inf"},
		{serialize_deserialize_FLOAT, []interface{}{r_FLOAT, "+Inf"}, r_FLOAT, "Inf"},
		{serialize_deserialize_FLOAT, []interface{}{r_FLOAT, "Nan"}, r_FLOAT, "Nan"},

		{serialize_deserialize_DATE, []interface{}{r_DATE, nil}, r_DATE, nil},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "0001-01-01"}, r_DATE, "0001-01-01"},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "0001-12-31"}, r_DATE, "0001-12-31"},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "1955-01-01"}, r_DATE, "1955-01-01"},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "2015-05-11"}, r_DATE, "2015-05-11"},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "5015-01-01"}, r_DATE, "5015-01-01"},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "5015-05-23"}, r_DATE, "5015-05-23"},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "5015-12-31"}, r_DATE, "5015-12-31"},
		{serialize_deserialize_DATE, []interface{}{r_DATE, "9999-12-31"}, r_DATE, "9999-12-31"},

		{serialize_deserialize_TIME, []interface{}{r_TIME, nil}, r_TIME, nil},
		{serialize_deserialize_TIME, []interface{}{r_TIME, "00:00:00"}, r_TIME, "00:00:00"},
		{serialize_deserialize_TIME, []interface{}{r_TIME, "00:00:01.123"}, r_TIME, "00:00:01.123"},
		{serialize_deserialize_TIME, []interface{}{r_TIME, "00:00:01"}, r_TIME, "00:00:01"},
		{serialize_deserialize_TIME, []interface{}{r_TIME, "06:27:51"}, r_TIME, "06:27:51"},
		{serialize_deserialize_TIME, []interface{}{r_TIME, "23:59:59.999999999"}, r_TIME, "23:59:59.999999999"},

		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, nil}, r_DATETIME, nil},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "0001-01-01 00:00:00"}, r_DATETIME, "0001-01-01 00:00:00"},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "0001-01-01 00:00:00.0000001"}, r_DATETIME, "0001-01-01 00:00:00.0000001"},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "0001-01-01 00:00:01.123"}, r_DATETIME, "0001-01-01 00:00:01.123"},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "1955-01-01 00:00:00"}, r_DATETIME, "1955-01-01 00:00:00"},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "1955-01-01 00:00:01"}, r_DATETIME, "1955-01-01 00:00:01"},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "2015-05-11 06:27:51"}, r_DATETIME, "2015-05-11 06:27:51"},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "9999-12-31 23:59:59.999999999"}, r_DATETIME, "9999-12-31 23:59:59.999999999"},
		{serialize_deserialize_DATETIME, []interface{}{r_DATETIME, "9999-12-31 00:00:00"}, r_DATETIME, "9999-12-31 00:00:00"},
	}
)

func Test_serialization(t *testing.T) {

	// check all the samples

	for i, sample := range samples_serialization { // println( sample.String())

		tst_function_tester(sample.function, sample.operands...) // run the function to test

		if tst_function_result_checker(sample.result, sample.result_check) == false { // check result
			t.Fatalf("--------- Failure at samples[%d] for %s -------", i, sample.String())
		}
	}

}

func Test_serialization_row(t *testing.T) {
	var (
		rsql_err *rsql.Error
		row      []rsql.IDataslot
		row_2    []rsql.IDataslot
		result   []interface{}
		tuple    rsql.Tuple
	)

	v_varbinary := []byte("esadfkjr")
	v_varchar := "rdasjhrkjieu "
	v_char := "uehwduejdk" + strings.Repeat(" ", 290)
	v_bit := uint8(1)
	v_tinyint := uint8(121)
	v_smallint := int16(30263)
	v_int := int32(1293746587)
	v_bigint := int64(123456789012345)
	v_money := "-635473"
	v_numeric := "-253647367495847362537485.46573876"
	v_float := 243564.476e-142
	v_date := "20150302"
	v_time := "17:30:28.123456"
	v_datetime := "63541206 13:45:56.987654"

	x_varbinary, rsql_err := New_literal_VARBINARY_value(v_varbinary)
	rsql.Assert(rsql_err == nil)
	x_varchar, rsql_err := New_literal_VARCHAR(v_varchar)
	rsql.Assert(rsql_err == nil)
	x_char, rsql_err := New_literal_VARCHAR(v_char)
	rsql.Assert(rsql_err == nil)
	x_char.Data_fixlen_flag = true
	x_bit := New_literal_BIT_value(v_bit)
	x_tinyint := New_literal_TINYINT_value(v_tinyint)
	x_smallint := New_literal_SMALLINT_value(v_smallint)
	x_int := New_literal_INT_value(v_int)
	x_bigint := New_literal_BIGINT_value(v_bigint)
	x_money, rsql_err := New_literal_MONEY(v_money)
	rsql.Assert(rsql_err == nil)
	x_numeric, rsql_err := New_literal_NUMERIC(v_numeric)
	rsql.Assert(rsql_err == nil)
	x_float, rsql_err := New_literal_FLOAT_value(v_float)
	rsql.Assert(rsql_err == nil)
	x_date, rsql_err := New_literal_DATE_value(v_date)
	rsql.Assert(rsql_err == nil)
	x_time, rsql_err := New_literal_TIME_value(v_time)
	rsql.Assert(rsql_err == nil)
	x_datetime, rsql_err := New_literal_DATETIME_value(v_datetime)
	rsql.Assert(rsql_err == nil)
	x_varchar_NULL := New_VARCHAR_NULL(rsql.KIND_RO_LEAF, 100, false)
	x_int_NULL := New_INT_NULL(rsql.KIND_RO_LEAF)

	x_varbinary_2 := New_VARBINARY_NULL(rsql.KIND_RO_LEAF, 100)
	x_varchar_2 := New_VARCHAR_NULL(rsql.KIND_RO_LEAF, 100, false)
	x_char_2 := New_VARCHAR_NULL(rsql.KIND_RO_LEAF, 300, true)
	x_bit_2 := New_BIT_NULL(rsql.KIND_RO_LEAF)
	x_tinyint_2 := New_TINYINT_NULL(rsql.KIND_RO_LEAF)
	x_smallint_2 := New_SMALLINT_NULL(rsql.KIND_RO_LEAF)
	x_int_2 := New_INT_NULL(rsql.KIND_RO_LEAF)
	x_bigint_2 := New_BIGINT_NULL(rsql.KIND_RO_LEAF)
	x_money_2 := New_MONEY_NULL(rsql.KIND_RO_LEAF)
	x_numeric_2 := New_NUMERIC_NULL(rsql.KIND_RO_LEAF, 34, 9)
	x_float_2 := New_FLOAT_NULL(rsql.KIND_RO_LEAF)
	x_date_2 := New_DATE_NULL(rsql.KIND_RO_LEAF)
	x_time_2 := New_TIME_NULL(rsql.KIND_RO_LEAF)
	x_datetime_2 := New_DATETIME_NULL(rsql.KIND_RO_LEAF)
	x_varchar_NULL_2 := New_VARCHAR_NULL(rsql.KIND_RO_LEAF, 100, false)
	x_int_NULL_2 := New_INT_NULL(rsql.KIND_RO_LEAF)

	row = []rsql.IDataslot{x_varbinary, x_varchar, x_char, x_bit, x_tinyint, x_smallint, x_int, x_bigint, x_money, x_numeric, x_float, x_date, x_time, x_datetime, x_varchar_NULL, x_int_NULL}
	row_2 = []rsql.IDataslot{x_varbinary_2, x_varchar_2, x_char_2, x_bit_2, x_tinyint_2, x_smallint_2, x_int_2, x_bigint_2, x_money_2, x_numeric_2, x_float_2, x_date_2, x_time_2, x_datetime_2, x_varchar_NULL_2, x_int_NULL_2}
	result = []interface{}{v_varbinary, v_varchar, v_char, v_bit, v_tinyint, v_smallint, v_int, v_bigint, v_money, v_numeric, v_float, v_date, v_time, v_datetime, nil, nil}

	tuple, rsql_err = Write_serialized_row(tuple[:0], row)
	rsql.Assert(rsql_err == nil)

	for i, dataslot := range row_2 {
		Deserialize_tuple_field(dataslot, tuple, uint16(i))
	}

	for i, dataslot := range row_2 {
		res := result[i]

		if tst_function_result_checker(dataslot, res) == false {
			t.Fatalf("--------- Failure at row[%d] -------", i)
		}
	}

}
