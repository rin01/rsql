package data

import (
	"math"
	"unicode/utf8"

	"rsql"
	"rsql/format"
)

func Sysfunc_abs_BIGINT(r *BIGINT, a *BIGINT) {
	var (
		a_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	if a_val == math.MinInt64 {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r_val = a_val

	if r_val < 0 {
		r_val = -r_val
	}

	r.data_val = r_val

}

func Sysfunc_ceiling_BIGINT(r *BIGINT, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val
}

func Sysfunc_floor_BIGINT(r *BIGINT, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val
}

func Sysfunc_sign_BIGINT(r *BIGINT, a *BIGINT) {
	var (
		a_val int64
		r_val int64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	a_val = a.data_val

	switch {
	case a_val > 0:
		r_val = 1
	case a_val < 0:
		r_val = -1
	default:
		r_val = 0
	}

	r.data_val = r_val
}

// Sysfunc_power_BIGINT computes the power operation for BIGINT.
// The computation is done by converting operand into float64. The result is thus not precise.
//
func Sysfunc_power_BIGINT(r *BIGINT, a *BIGINT, b *FLOAT) {
	var (
		a_val float64
		b_val float64
		r_val float64
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* operation */

	a_val = float64(a.data_val)
	b_val = b.data_val

	r_val = math.Pow(a_val, b_val) // all digits of a BIGINT ( 19 significant digits ) DON'T FIT in a float64 ( approx. 15 significant digits ). Operands and result are approximate.

	if math.IsNaN(r_val) { // check if NaN
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_FLOAT_NAN, rsql.ERROR_BATCH_ABORT)
		return
	}

	if math.IsInf(r_val, 0) || r_val >= float64(math.MaxInt64) || r_val <= float64(math.MinInt64) { // check if +Inf or -Inf or overflow. NOTE: see Cast_FLOAT_to_BIGINT() for explanations about why '>=' and '<=' are used.
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val = int64(r_val) // truncate fractional part ( which doesn't occur, as r_val should contain no fractional part )
}

// Sysfunc_round_BIGINT rounds or truncate number to given precision.
//
//        ROUND ( numeric_expression, decimal precision [ ,truncate flag ] )
//
func Sysfunc_round_BIGINT(r *BIGINT, a *BIGINT, b *INT, c *INT) {
	var (
		a_val int64
		b_val int64
		r_val int64

		truncate_flag   bool
		divisor         int64
		truncated_a_val int64
		remainder       int64
	)

	/* if error in a or b operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header) {
		return
	}

	/* if c is error, returns error */

	if c.data_error != nil {
		r.data_error = c.data_error
		return
	}

	// If c is not NULL and != 0, truncate. Else, if c is NULL or 0, round.

	if c.data_NULL_flag == false && c.data_val != 0 { // if c is not NULL and != 0
		truncate_flag = true
	}

	/* if rounding length >= 0, just copy the operand and return */

	if b.data_val >= 0 { // for BIGINT, there is no fractional part to round.
		r.data_val = a.data_val
		return
	}

	/* operation */

	a_val = a.data_val

	b_val = -b.data_val // number of least significant digits to set to 0 when truncating. E.g. for b_val=1, 5234 -> 5230       for b_val=2, 5234 -> 5200           Note: if b.data_val == MinInt32, then b_val == MaxInt32+1, but it is ok.

	if b_val >= rsql.DATATYPE_PRECISION_BIGINT { // if all digits must be set to 0
		if truncate_flag == false && b_val == rsql.DATATYPE_PRECISION_BIGINT { // special rounding case, check for overflow
			if a_val >= 5000000000000000000 || a_val <= -5000000000000000000 {
				r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
				return
			}
		}
		r.data_val = 0
		return
	}

	divisor = G_POWER_OF_10[b_val]

	truncated_a_val = (a_val / divisor) * divisor // truncated value of a_val

	if truncate_flag { // if truncate flag, just return this result
		r.data_val = truncated_a_val
		return
	}

	// here, we want to round

	remainder = a_val - truncated_a_val

	rsql.Assert(remainder == a_val%divisor)

	r_val = truncated_a_val

	switch {
	case remainder > 0: // implies that truncated_a_val > 0
		if remainder >= divisor/2 { // 5 50 500 etc
			if truncated_a_val > math.MaxInt64-divisor {
				r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
				return
			}
			r_val = truncated_a_val + divisor
		}

	case remainder < 0: // implies that truncated_a_val < 0
		if remainder <= -divisor/2 { // -5 -50 -500 etc
			if truncated_a_val < math.MinInt64+divisor {
				r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
				return
			}
			r_val = truncated_a_val - divisor
		}
	}

	r.data_val = r_val
}

func Sysfunc_format_BIGINT(r *VARCHAR, a *BIGINT, b *VARCHAR, syslang *SYSLANGUAGE) {
	var (
		err  error
		res  []byte
		nfmt format.Numberfmt
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &syslang.Header) {
		return
	}

	/* operation */

	nfmt.Get_format_info(b.data_val) // 1st parse of format string, to analyze it

	nfmt.Fill_int64(a.data_val) // put string representation of number in nfmt

	nfmt.Set_langinfo(syslang.data_langinfo) // put locale information in nfmt

	if res, err = nfmt.Append_formatted_number(r.data_val[:0], b.data_val); err != nil { // 2nd parse of format string, to output result
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_FORMAT, rsql.ERROR_BATCH_ABORT, a.data_val, b.data_val)
		return
	}

	// check result length

	if len(res) > int(r.Data_precision) && utf8.RuneCount(res) > int(r.Data_precision) {
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_FORMAT_OVERFLOW, rsql.ERROR_BATCH_ABORT, a.data_val, b.data_val)
		return
	}

	r.data_val = res
}

func Sysfunc_isnumeric_BIGINT(r *INT, a *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = 0
	if a.data_NULL_flag == false {
		r.data_val = 1
	}
}

func Sysfunc_isnull_BIGINT(r *BIGINT, a *BIGINT, b *BIGINT) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag
	r.data_val = a.data_val

	if a.data_error == nil && a.data_NULL_flag == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
	}
}

func Sysfunc_iif_BIGINT(r *BIGINT, a *BOOLEAN, b *BIGINT, c *BIGINT) {

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == false && a.data_val == true {
		r.data_error = b.data_error
		r.data_NULL_flag = b.data_NULL_flag
		r.data_val = b.data_val
		return
	}

	// if a is NULL or false

	r.data_error = c.data_error
	r.data_NULL_flag = c.data_NULL_flag
	r.data_val = c.data_val
}

func Sysfunc_choose_BIGINT(r *BIGINT, a *INT, b_list []*BIGINT) {

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	if a.data_val <= 0 || a.data_val > int64(len(b_list)) {
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	elem := b_list[a.data_val-1]

	r.data_error = elem.data_error
	r.data_NULL_flag = elem.data_NULL_flag
	r.data_val = elem.data_val
}

func Sysfunc_coalesce_BIGINT(r *BIGINT, a_list []*BIGINT) {

	for _, elem := range a_list {
		if elem.data_error != nil {
			r.data_error = elem.data_error
			return
		}

		if elem.data_NULL_flag == false {
			r.data_error = elem.data_error
			r.data_NULL_flag = elem.data_NULL_flag
			r.data_val = elem.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Sysfunc_between_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT, c *BIGINT) {
	var (
		r_val bool
	)

	/* if error in operand, returns error. If NULL operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header, &b.Header, &c.Header) {
		return
	}

	/* operation */

	r_val = (a.data_val >= b.data_val) && (a.data_val <= c.data_val)

	r.data_val = r_val
}

func Sysfunc_not_between_BIGINT(r *BOOLEAN, a *BIGINT, b *BIGINT, c *BIGINT) {

	Sysfunc_between_BIGINT(r, a, b, c)

	r.data_val = !r.data_val
}

func Sysfunc_typeof_BIGINT(r *VARCHAR, a *BIGINT) {

	r.data_error = nil
	r.data_NULL_flag = false

	r.data_val = append(r.data_val[:0], "BIGINT"...)
}

// Sysfunc_aggr_count_BIGINT is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_BIGINT(r *INT, a *BIGINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val >= math.MaxInt32 { // check that r.data_val won't go beyond MaxInt32
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_INT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_count_big_BIGINT is an aggregate function.
// It injects 1 if value of 'a' is not NULL into 'r'.
//
func Sysfunc_aggr_count_big_BIGINT(r *BIGINT, a *BIGINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = 1

		return
	}

	if r.data_val == math.MaxInt64 { // check that r.data_val won't go beyond MaxInt64
		r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
		return
	}

	r.data_val++
}

// Sysfunc_aggr_sum_BIGINT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_sum_BIGINT(r *BIGINT, a *BIGINT) {
	var (
		a_val int64
		b_val int64
		r_val int64
	)

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	a_val = a.data_val
	b_val = r.data_val

	switch {
	case b_val > 0: // if b > 0, check that a+b won't go beyond MaxInt64
		if a_val > math.MaxInt64-b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}

	case b_val < 0: // if b < 0, check that a+b won't go below MinInt64
		if a_val < math.MinInt64-b_val {
			r.data_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_BIGINT_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			return
		}
	}

	r_val = a_val + b_val

	r.data_val = r_val
}

// Sysfunc_aggr_min_BIGINT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_min_BIGINT(r *BIGINT, a *BIGINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val < r.data_val {
		r.data_val = a.data_val
	}
}

// Sysfunc_aggr_max_BIGINT is an aggregate function.
// It injects value of 'a' into 'r'.
//
func Sysfunc_aggr_max_BIGINT(r *BIGINT, a *BIGINT) {

	if r.data_error != nil {
		return
	}

	if a.data_error != nil {
		r.data_error = a.data_error
		return
	}

	if a.data_NULL_flag == true {
		return
	}

	/* operation */

	if r.data_NULL_flag == true { // first sum
		r.data_NULL_flag = false
		r.data_val = a.data_val

		return
	}

	if a.data_val > r.data_val {
		r.data_val = a.data_val
	}
}
