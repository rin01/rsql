package data

import (
	"rsql"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *VOID) RO_LEAF_hashval() uint32 {
	var (
		hash uint32
	)

	hash = uint32(rsql.DATATYPE_VOID)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		hash += 63 // utterly arbitrary number
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *VOID) String() string {

	return "VOID: null"
}

// New_VOID_NULL creates a new data.VOID initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_VOID_NULL(kind rsql.Kind_t) *VOID {

	dataslot := &VOID{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VOID,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
	}

	return dataslot
}

// New_literal_VOID_NULL creates a new data.VOID.
//
func New_literal_VOID_NULL() *VOID {

	dataslot := &VOID{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_VOID,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: true,
		},
	}

	return dataslot
}

//========================= NOTE ==========================================================
//
//  All these functions implement SQL operations on literal NULLs.
//
//  E.g.  Add_VOID()          :  NULL + NULL
//        Comp_equal_VOID()   :  NULL = NULL
//        Is_null_VOID()      :  NULL is NULL             ("is NULL" is the operator)
//        Cast_VOID_to_INT()  :  CAST(NULL as INT)
//
//=========================================================================================

func Unary_minus_VOID(r *VOID, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Add_VOID(r *VOID, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Subtract_VOID(r *VOID, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Multiply_VOID(r *VOID, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Divide_VOID(r *VOID, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Modulo_VOID(r *VOID, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Comp_equal_VOID(r *BOOLEAN, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Comp_greater_VOID(r *BOOLEAN, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Comp_less_VOID(r *BOOLEAN, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Comp_greater_equal_VOID(r *BOOLEAN, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Comp_less_equal_VOID(r *BOOLEAN, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Comp_not_equal_VOID(r *BOOLEAN, a *VOID, b *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Is_null_VOID(r *BOOLEAN, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = true
}

func Is_not_null_VOID(r *BOOLEAN, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func In_list_VOID(r *BOOLEAN, a *VOID, b_list []*VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Not_in_list_VOID(r *BOOLEAN, a *VOID, b_list []*VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Case_VOID(r *VOID, a_list []Case_element_t) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_VOID(r *VOID, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_SYSLANGUAGE(r *SYSLANGUAGE, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_VARBINARY(r *VARBINARY, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_VARCHAR(r *VARCHAR, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_REGEXPLIKE(r *REGEXPLIKE, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_BIT(r *BIT, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_TINYINT(r *TINYINT, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_SMALLINT(r *SMALLINT, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_INT(r *INT, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_BIGINT(r *BIGINT, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_MONEY(r *MONEY, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_NUMERIC(r *NUMERIC, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_FLOAT(r *FLOAT, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_DATE(r *DATE, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_TIME(r *TIME, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_VOID_to_DATETIME(r *DATETIME, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Copy_VOID(r *VOID, a *VOID) {

	r.data_error = nil
	r.data_NULL_flag = true
}

func Clone_VOID(kind rsql.Kind_t, a *VOID) *VOID {

	return New_VOID_NULL(kind)
}
