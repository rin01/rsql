package data

import (
	"time"

	"rsql"
	"rsql/mdat"
)

// RO_LEAF_hashval returns a hash of a KIND_RO_LEAF dataslot value.
// It is used during Common Subexpression Elimination.
//
func (a *DATE) RO_LEAF_hashval() uint32 {
	var (
		seconds uint64 // unix seconds
		hash    uint32
	)

	hash = uint32(rsql.DATATYPE_DATE)
	hash += (hash << 10)
	hash ^= (hash >> 6)

	switch {
	case a.data_error != nil:
		hash += (hash << 3)
		hash ^= (hash >> 11)
		hash += (hash << 15)
		hash += uint32(a.data_error.Message_id)
		hash += (hash << 10)
		hash ^= (hash >> 6)

	case a.data_NULL_flag == true:
		// pass

	default:
		seconds = uint64(a.data_val.Unix())

		hash += uint32(seconds)
		hash += (hash << 10)
		hash ^= (hash >> 6)

		hash += uint32(seconds >> 32)
		hash += (hash << 10)
		hash ^= (hash >> 6)
	}

	hash += (hash << 3)
	hash ^= (hash >> 11)
	hash += (hash << 15)

	return hash
}

func (a *DATE) String() string {

	if a.data_error != nil {
		return "DATE: error"
	}

	if a.data_NULL_flag == true {
		return "DATE: null"
	}

	return a.data_val.Format("DATE: 2006-01-02")
}

// New_DATE_NULL creates a new data.DATE initialized to NULL.
//
// Argument 'kind' is kind of node : KIND_RO_LEAF, KIND_VAR_LEAF, etc
//
func New_DATE_NULL(kind rsql.Kind_t) *DATE {

	dataslot := &DATE{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_DATE,
			Data_kind:      kind,
			data_error:     nil,
			data_NULL_flag: true,
		},
		data_val: mdat.MyDatetime{Time: mdat.TIME_1900_01_01},
	}

	return dataslot
}

// New_literal_DATE_value creates a new data.DATE from a value.
//
// Value string must be of the form "1983-04-20".
//
func New_literal_DATE_value(val string) (*DATE, *rsql.Error) {
	var (
		err error
		t   time.Time
	)

	if t, err = mdat.Parse([]byte(val), "YMD", 0, mdat.PARSE_MODE_DATE); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_CONVERSION_STRING_TO_DATE_FAILED, rsql.ERROR_BATCH_ABORT, val)
	}

	dataslot := &DATE{
		Header: Header{
			Data_datatype:  rsql.DATATYPE_DATE,
			Data_kind:      rsql.KIND_RO_LEAF,
			data_error:     nil,
			data_NULL_flag: false,
		},
		data_val: mdat.MyDatetime{Time: t},
	}

	return dataslot, nil
}

func Is_null_DATE(r *BOOLEAN, a *DATE) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = a.data_NULL_flag
}

func Is_not_null_DATE(r *BOOLEAN, a *DATE) {

	r.data_error = a.data_error
	r.data_NULL_flag = false

	r.data_val = !a.data_NULL_flag
}

// In_list_DATE checks if a exists in b_list.
// It returns TRUE, FALSE, or NULL.
//
// b_list can be empty.
//
//      - if left operand is error, result is error
//      - if left operand is NULL, result is NULL
//      - if left operand value is found in list, result is TRUE (even if some elements in the list are error or NULL)
//      - if left operand value is not found in list:
//                      - if error value exists in list, result is error
//                      - if NULL value exists in list, result is NULL
//                      - else, result is FALSE
//
func In_list_DATE(r *BOOLEAN, a *DATE, b_list []*DATE) {
	var (
		NULL_found_flag bool
		error_found     *rsql.Error
	)

	/* if error in left operand, returns error. If NULL left operand, returns NULL */

	if r.Header.process_error_and_NULL(&a.Header) {
		return
	}

	/* operation */

	for _, b := range b_list {
		Comp_equal_DATETIME(r, (*DATETIME)(a), (*DATETIME)(b))

		if r.data_error != nil { // if error detected in comparison result, save it and continue the loop
			if error_found == nil {
				error_found = r.data_error
			}
			continue
		}

		if r.data_NULL_flag {
			NULL_found_flag = true // if comparison result is NULL, flag it and continue the loop
			continue
		}

		if r.data_val == true { // if equality found, r contains TRUE. exit
			return // ===> exit
		}
	}

	// here, no equality has been found in the list.

	if error_found != nil { // if error found, put error into returned value
		r.data_error = error_found
		return
	}

	if NULL_found_flag { // if no error and a NULL result has been found, force result to NULL.
		r.data_error = nil
		r.data_NULL_flag = true
		return
	}

	// else, result is FALSE.

	r.data_error = nil
	r.data_NULL_flag = false
	r.data_val = false
}

func Not_in_list_DATE(r *BOOLEAN, a *DATE, b_list []*DATE) {

	In_list_DATE(r, a, b_list)

	r.data_val = !r.data_val
}

func Case_DATE(r *DATE, a_list []Case_element_t) {
	var (
		elem_val *DATE
	)

	for _, elem := range a_list {
		if elem.Cond.data_error != nil {
			r.data_error = elem.Cond.data_error
			return
		}

		if elem.Cond.data_NULL_flag == false && elem.Cond.data_val == true {
			elem_val = elem.Val.(*DATE)
			r.data_error = elem_val.data_error
			r.data_NULL_flag = elem_val.data_NULL_flag
			r.data_val = elem_val.data_val
			return
		}
	}

	r.data_error = nil
	r.data_NULL_flag = true
}

func Cast_DATE_to_DATE(r *DATE, a *DATE) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Cast_DATE_to_DATETIME(r *DATETIME, a *DATE) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Assign_DATE(r *DATE, a *DATE) *rsql.Error {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val

	if r.data_error != nil {
		return r.data_error
	}
	return nil
}

func Copy_DATE(r *DATE, a *DATE) {

	r.data_error = a.data_error
	r.data_NULL_flag = a.data_NULL_flag

	r.data_val = a.data_val
}

func Clone_DATE(kind rsql.Kind_t, a *DATE) *DATE {

	return New_DATE_NULL(kind)
}
