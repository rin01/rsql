package rsql

import (
	"bytes"
	"fmt"
	"log"
	"sort"
	"strings"

	"golang.org/x/text/collate"
	"golang.org/x/text/language"
	"golang.org/x/text/language/display"
)

type Collstrength_t uint8

const (
	COLLSTRENGTH_0 Collstrength_t = 0 // default collation
	COLLSTRENGTH_1 Collstrength_t = 1 // for collation of table columns
	COLLSTRENGTH_2 Collstrength_t = 2 // COLLATE clause
)

// Normalize_collation returns the normalized version of a collation string.
// For e.g. "French_cs_as", "FR_CS_AS", the string "fr_cs_as" is returned. For "French_CA_ci_ai", "fr_ca_ci_ai" is returned.
//
// Returns error if invalid collation.
//
func Normalize_collation(collation_str string) (string, *Error) {
	var (
		collation_lc            string
		collname                string
		rsql_collname           string
		case_accent_sensitivity string
		ok                      bool
	)

	collation_lc = collation_str // default

	for _, c := range collation_str { // if any non a-z lowercase letter is found, call strings.ToLower. It should never happen if collation_str is an identifier, which is usually passed as lowercase.
		if !((c >= 'a' && c <= 'z') || c == '_') {
			collation_lc = strings.ToLower(collation_str)
			break
		}
	}

	if len(collation_lc) <= 1 || collation_lc[0] == '_' { // check that collation is not empty string, or begins with _
		return "", New_Error(ERROR_SQL_SYNTAX, ERROR_BAD_COLLATION_LOCALE, ERROR_BATCH_ABORT, collation_str)
	}

	if len(collation_lc) < 6 { // check that suffix cs_as, ci_ai, etc exists
		return "", New_Error(ERROR_SQL_SYNTAX, ERROR_BAD_COLLATION_LOCALE_ATTRIBUTES, ERROR_BATCH_ABORT, collation_str)
	}

	case_accent_sensitivity = collation_lc[len(collation_lc)-6:]

	if case_accent_sensitivity != "_ci_ai" && case_accent_sensitivity != "_ci_as" && case_accent_sensitivity != "_cs_as" {
		return "", New_Error(ERROR_SQL_SYNTAX, ERROR_BAD_COLLATION_LOCALE_ATTRIBUTES, ERROR_BATCH_ABORT, collation_str)
	}

	collname = collation_lc[:len(collation_lc)-6] // fr, french, fr_ca, etc

	if rsql_collname, ok = WINDOWS_TO_COLLATION_MAP[collname]; ok == true { // if Windows collation, transform it into rsql collation
		collation_lc = rsql_collname + case_accent_sensitivity
		collname = collation_lc[:len(collation_lc)-6]
	}

	if _, ok = G_COLLATION_MAP[collname]; ok == false { // check if rsql collation exists
		return "", New_Error(ERROR_SQL_SYNTAX, ERROR_BAD_COLLATION_LOCALE, ERROR_BATCH_ABORT, collation_str)
	}

	return collation_lc, nil
}

// get_collator creates a new Collator from string.
//
// collation must be a supported locale, with a suffix like "_cs_as", or an error is returned.
//
// 2014.03.19   text/collate package is still buggy.
//     Normally, with a collator with Strength = colltab.Primary and CaseLevel = true, 'role' == 'rôle', because comparison is accent insensitive and case sensitive.
//     But text/collate.CompareString('role', 'rôle') gives -1.
//
//     That's why we only allow Primary, Secondary and Tertiary Strength. CaseLevel setting is not used, as it is buggy.
//
func Get_collator(collation_string string) (*collate.Collator, *Error) {
	var (
		err      error
		tag      language.Tag
		collator *collate.Collator
	)

	if len(collation_string) < 7 { // suffix must exist
		return nil, New_Error(ERROR_GENERAL, ERROR_SQLDATA_SYSCOLLATOR_INVALID_LOCALE, ERROR_BATCH_ABORT, collation_string)
	}

	// parse accent sensitive and case sensitive flags

	suffix := collation_string[len(collation_string)-6:] // e.g. "_ci_ai", etc

	coll_string := collation_string[:len(collation_string)-6] // e.g. "fr", "fr_ca", "en", etc

	// get tag from locale

	if _, ok := G_COLLATION_MAP[coll_string]; ok == false { // get collator internal tag name, e.g. "fr", "fr-CA", "en", etc
		return nil, New_Error(ERROR_GENERAL, ERROR_SQLDATA_SYSCOLLATOR_INVALID_LOCALE, ERROR_BATCH_ABORT, collation_string)
	}

	if tag, err = language.Parse(coll_string); err != nil { // create tag
		return nil, New_Error(ERROR_GENERAL, ERROR_SQLDATA_SYSCOLLATOR_INVALID_LOCALE, ERROR_BATCH_ABORT, collation_string)
	}

	// create collator from tag, with options

	switch suffix {
	case "_ci_ai":
		collator = collate.New(tag, collate.IgnoreCase, collate.IgnoreDiacritics) // Primary strength

	// case "_cs_ai": // commented out for the moment, because bug in text/collate package
	//      collator = collate.New(tag, collate.IgnoreDiacritics)

	case "_ci_as":
		collator = collate.New(tag, collate.IgnoreCase) // Secondary strength

	case "_cs_as":
		collator = collate.New(tag) // Tertiary strength. In ICU doc http://userguide.icu-project.org/collation/architecture, tertiary is the default: UCOL_TERTIARY (default). It is the same for "collate" package, as in https://github.com/golang/text/blob/master/collate/option.go, we have: colltab.Quaternary: true and colltab.Identity: true in the "ignore" field.

	default:
		return nil, New_Error(ERROR_GENERAL, ERROR_SQLDATA_SYSCOLLATOR_INVALID_SUFFIX, ERROR_BATCH_ABORT, collation_string)
	}

	return collator, nil
}

// rsql collation name to golang collation name.
//
//     NOTE: "bs_cyrl" is mapped to "bs-Cyrl". Notice the underscore, replaced by a hyphen in the mapping. Same for "fr_ca" etc.
//
// This map is created from the list of locales returned by function Supported(), in package "code.google.com/p/go.text/collate".
//
//     VERSION OF COLLATION DATA:
//           In file code.google.com/p/go.text/collate/tables.go, it is written that the collation table was created by running:
//           maketables -root=http://unicode.org/Public/UCA/6.2.0/CollationAuxiliary.zip -cldr=http://www.unicode.org/Public/cldr/23/core.zip
//
//           In 2015, all packages in code.google.com/p/ have been migrated to golang.org/x/.
//
//           In file golang.org/x/text/collate/collate.go, it is written:
//           go:generate go run maketables.go -cldr=23 -unicode=6.2.0
//
//
// If a collation is not listed here, like italian "it", it means it is the same as "en" collation.
//
//     TODO
//     THIS PROBLEM WILL BE SOLVED WHEN TABLES WILL BE CREATED WITH CLDR VERSION 25. WE JUST HAVE TO WAIT.
//     Indeed, in cldr release 25, all missing collations like "it" have been added, with empty body:
//     In http://unicode.org/cldr/trac/browser/tags/release-25/common/collation/it.xml, as it is written "The root collation order is valid for this language".
//
var G_COLLATION_MAP = map[string]string{
	"aa":      "aa",      // Afar
	"af":      "af",      // Afrikaans
	"ar":      "ar",      // Arabic
	"as":      "as",      // Assamese
	"az":      "az",      // Azerbaijani
	"be":      "be",      // Belarusian
	"bg":      "bg",      // Bulgarian
	"bn":      "bn",      // Bengali
	"bs":      "bs",      // Bosnian
	"bs_cyrl": "bs-Cyrl", // Bosnian (Cyrillic)        Notice the underscore and the titlecase for script tag
	"ca":      "ca",      // Catalan
	"cs":      "cs",      // Czech
	"cy":      "cy",      // Welsh
	"da":      "da",      // Danish
	"de":      "de",      // German
	"dz":      "dz",      // Dzongkha
	"ee":      "ee",      // Ewe
	"el":      "el",      // Greek
	"en":      "en",      // English
	"eo":      "eo",      // Esperanto
	"es":      "es",      // Spanish
	"et":      "et",      // Estonian
	"fa":      "fa",      // Persian
	"fa_af":   "fa-AF",   // Persian (Afghanistan)
	"fi":      "fi",      // Finnish
	"fil":     "fil",     // Filipino
	"fo":      "fo",      // Faroese
	"fr":      "fr",      // French
	"fr_ca":   "fr-CA",   // Canadian French
	"gu":      "gu",      // Gujarati
	"ha":      "ha",      // Hausa
	"haw":     "haw",     // Hawaiian
	"he":      "he",      // Hebrew
	"hi":      "hi",      // Hindi
	"hr":      "hr",      // Croatian
	"hu":      "hu",      // Hungarian
	"hy":      "hy",      // Armenian
	"ig":      "ig",      // Igbo
	"is":      "is",      // Icelandic
	"ja":      "ja",      // Japanese
	"kk":      "kk",      // Kazakh
	"kl":      "kl",      // Kalaallisut
	"km":      "km",      // Khmer
	"kn":      "kn",      // Kannada
	"ko":      "ko",      // Korean
	"kok":     "kok",     // Konkani
	"ln":      "ln",      // Lingala
	"lt":      "lt",      // Lithuanian
	"lv":      "lv",      // Latvian
	"mk":      "mk",      // Macedonian
	"ml":      "ml",      // Malayalam
	"mr":      "mr",      // Marathi
	"mt":      "mt",      // Maltese
	"my":      "my",      // Burmese
	"nb":      "nb",      // Norwegian Bokmål
	"nn":      "nn",      // Norwegian Nynorsk
	"nso":     "nso",     // Northern Sotho
	"om":      "om",      // Oromo
	"or":      "or",      // Oriya
	"pa":      "pa",      // Punjabi
	"pl":      "pl",      // Polish
	"ps":      "ps",      // Pashto
	"ro":      "ro",      // Romanian
	"ru":      "ru",      // Russian
	"se":      "se",      // Northern Sami
	"si":      "si",      // Sinhala
	"sk":      "sk",      // Slovak
	"sl":      "sl",      // Slovenian
	"sq":      "sq",      // Albanian
	"sr":      "sr",      // Serbian
	"sr_latn": "sr-Latn", // Serbo-Croatian
	"ssy":     "ssy",     // Saho
	"sv":      "sv",      // Swedish
	"ta":      "ta",      // Tamil
	"te":      "te",      // Telugu
	"th":      "th",      // Thai
	"tn":      "tn",      // Tswana
	"to":      "to",      // Tongan
	"tr":      "tr",      // Turkish
	"uk":      "uk",      // Ukrainian
	"ur":      "ur",      // Urdu
	"vi":      "vi",      // Vietnamese
	"wae":     "wae",     // Walser
	"yo":      "yo",      // Yoruba
	"zh":      "zh",      // Chinese
	"zh_hant": "zh-Hant", // Traditional Chinese
}

// ====== MS SQL Server Windows collation name ======

// collation full names, that are aliases for the short rsql collation names
//
var WINDOWS_TO_COLLATION_MAP = map[string]string{
	"latin1_general":       "en", // general Windows collation name
	"latin1_general_100":   "en", //   same
	"cyrillic_general":     "ru", //   same
	"cyrillic_general_100": "ru", //   same

	"afar":                "aa",      // Afar
	"afrikaans":           "af",      // Afrikaans
	"arabic":              "ar",      // Arabic
	"assamese":            "as",      // Assamese
	"azeri":               "az",      // Azerbaijani
	"belarusian":          "be",      // Belarusian
	"bulgarian":           "bg",      // Bulgarian
	"bengali":             "bn",      // Bengali
	"bosnian_latin":       "bs",      // Bosnian
	"bosnian_cyrillic":    "bs_cyrl", // Bosnian (Cyrillic)
	"catalan":             "ca",      // Catalan
	"czech":               "cs",      // Czech
	"welsh":               "cy",      // Welsh
	"danish":              "da",      // Danish
	"german":              "de",      // German
	"dzongkha":            "dz",      // Dzongkha
	"ewe":                 "ee",      // Ewe
	"greek":               "el",      // Greek
	"english":             "en",      // English
	"esperanto":           "eo",      // Esperanto
	"spanish":             "es",      // Spanish
	"estonian":            "et",      // Estonian
	"persian":             "fa",      // Persian
	"dari":                "fa_af",   // Persian (Afghanistan)
	"finnish":             "fi",      // Finnish
	"filipino":            "fil",     // Filipino
	"faroese":             "fo",      // Faroese
	"french":              "fr",      // French
	"french_ca":           "fr_ca",   // Canadian French
	"gujarati":            "gu",      // Gujarati
	"hausa":               "ha",      // Hausa
	"hawaiian":            "haw",     // Hawaiian
	"hebrew":              "he",      // Hebrew
	"hindi":               "hi",      // Hindi
	"croatian":            "hr",      // Croatian
	"hungarian":           "hu",      // Hungarian
	"armenian":            "hy",      // Armenian
	"igbo":                "ig",      // Igbo
	"icelandic":           "is",      // Icelandic
	"japanese":            "ja",      // Japanese
	"kazakh":              "kk",      // Kazakh
	"kalaallisut":         "kl",      // Kalaallisut
	"khmer":               "km",      // Khmer
	"kannada":             "kn",      // Kannada
	"korean":              "ko",      // Korean
	"konkani":             "kok",     // Konkani
	"lingala":             "ln",      // Lingala
	"lithuanian":          "lt",      // Lithuanian
	"latvian":             "lv",      // Latvian
	"macedonian":          "mk",      // Macedonian
	"malayalam":           "ml",      // Malayalam
	"marathi":             "mr",      // Marathi
	"maltese":             "mt",      // Maltese
	"burmese":             "my",      // Burmese
	"norwegian_bokmål":    "nb",      // Norwegian Bokmål
	"norwegian_nynorsk":   "nn",      // Norwegian Nynorsk
	"northern_sotho":      "nso",     // Northern Sotho
	"oromo":               "om",      // Oromo
	"oriya":               "or",      // Oriya
	"punjabi":             "pa",      // Punjabi
	"polish":              "pl",      // Polish
	"pashto":              "ps",      // Pashto
	"romanian":            "ro",      // Romanian
	"russian":             "ru",      // Russian
	"northern_sami":       "se",      // Northern Sami
	"sinhala":             "si",      // Sinhala
	"slovak":              "sk",      // Slovak
	"slovenian":           "sl",      // Slovenian
	"albanian":            "sq",      // Albanian
	"serbian_cyrillic":    "sr",      // Serbian
	"serbian_latin":       "sr_latn", // Serbo-Croatian
	"saho":                "ssy",     // Saho
	"swedish":             "sv",      // Swedish
	"tamil":               "ta",      // Tamil
	"telugu":              "te",      // Telugu
	"thai":                "th",      // Thai
	"tswana":              "tn",      // Tswana
	"tongan":              "to",      // Tongan
	"turkish":             "tr",      // Turkish
	"ukrainian":           "uk",      // Ukrainian
	"urdu":                "ur",      // Urdu
	"vietnamese":          "vi",      // Vietnamese
	"walser":              "wae",     // Walser
	"yoruba":              "yo",      // Yoruba
	"chinese_prc":         "zh",      // Chinese
	"chinese_traditional": "zh_hant", // Traditional Chinese
}

//=======================================================================
//          init() creates collation list for SHOW COLLATION
//=======================================================================

type collinfo_t struct {
	Name        string
	Aliases     []string
	Description string
}

type byName []collinfo_t

func (a byName) Len() int           { return len(a) }
func (a byName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byName) Less(i, j int) bool { return a[i].Description < a[j].Description }

var G_LIST_OF_RSQL_COLLATIONS_FOR_DISPLAY string

// creates the global variable G_LIST_OF_RSQL_COLLATION_FOR_DISPLAY, containing the list of collations.
// This variable is used by SHOW COLLATIONS.
//
func init() {
	var (
		err                  error
		namer                display.Namer
		supported_collations []collinfo_t
		tag                  language.Tag
		aliases_map          map[string][]string // for each collation short name, put all aliases
		buff                 bytes.Buffer
	)

	// create aliases map

	aliases_map = make(map[string][]string, len(WINDOWS_TO_COLLATION_MAP)) // e.g.  "fr" --> "french"       "ru" --> "cyrillic_general", "cyrillic_general_100", "russian"

	for alias, name := range WINDOWS_TO_COLLATION_MAP {
		aliases_map[name] = append(aliases_map[name], alias)
	}

	for name, aliases := range aliases_map { // for each collation, sort list of aliases
		if len(aliases) > 1 {
			sort.Strings(aliases)
			aliases_map[name] = aliases
		}
	}

	// create rsql collation list

	namer = display.English.Tags()

	supported_collations = make([]collinfo_t, 0, len(G_COLLATION_MAP))

	for name, internalname := range G_COLLATION_MAP {
		if tag, err = language.Parse(internalname); err != nil { // we need the Tag to get a name to display
			log.Fatalf("%s", err)
		}
		collinfo := collinfo_t{Name: name, Aliases: aliases_map[name], Description: namer.Name(tag)}
		supported_collations = append(supported_collations, collinfo)
	}

	sort.Sort(byName(supported_collations)) // sort list

	fmt.Fprintf(&buff, "%-25s %-14s %s\n", "Description", "Collation", "Aliases")
	fmt.Fprintf(&buff, "%-25s %-14s %s\n", "-----------", "---------", "-------")

	for _, collinfo := range supported_collations { // write list
		fmt.Fprintf(&buff, "%-25s %-14s %s\n", collinfo.Description, collinfo.Name, strings.Join(collinfo.Aliases, ", "))
	}

	G_LIST_OF_RSQL_COLLATIONS_FOR_DISPLAY = buff.String() // put result in global variable
}
