package rsql

import (
	"text/template"
)

// Params for BACKUP DATABASE statement.
//
type Backup_params struct {
	Bk_server_default_collation string // sorting order for SHOW output
}

// Params for RESTORE DATABASE statement.
//
type Restore_params struct {
	Rt_replace bool
	Rt_no_user bool
	Rt_verbose bool
}

// Params for SELECT statement
//
type Select_params struct {
	Set_variables_list []IDataslot
	Set_variables_bb   *Basicblock

	TOP_value int64
}

// Params for INSERT INTO ... SELECT
//
type Insert_select_params struct {
	Ins_cursor                     interface{} // always csr.Cursor. But we cannot declare it here, because of circular import.
	Ins_conversion_basicblock      *Basicblock
	Ins_base_row                   []IDataslot // row to insert
	Ins_explicit_IDENTITY_provided bool
}

// Params for BULK INSERT
//
type Bulk_insert_params struct {
	Bi_opt_codepage        string
	Bi_opt_rowterminator   string
	Bi_opt_fieldterminator string
	Bi_opt_firstrow        uint64
	Bi_opt_lastrow         uint64
	Bi_opt_keepidentity    bool
	Bi_opt_rtrim           bool
}

// Params for BULK EXPORT
//
type Bulk_export_params struct {
	Be_opt_codepage        string
	Be_opt_rowterminator   string
	Be_opt_fieldterminator string
	Be_opt_date_format     string // if "", it is the default
	Be_opt_time_format     string // if "", it is the default
	Be_opt_datetime_format string // if "", it is the default
}

// Params for UPDATE
//
type Update_params struct {
	Ud_insertion_base_row []IDataslot          // row to insert. Contains dataslots from target cursor, or SET expressions.
	Ud_cursor             interface{}          // always csr.Cursor. But we cannot declare it here, because of circular import. FROM cursor tree.
	Ud_target_cursor      interface{}          // always csr.Cursor. But we cannot declare it here, because of circular import. Points to target table cursor, inside Ud_cursor tree.
	Ud_basicblock         *Basicblock          // instructions to evaluate SET expressions.
	Ud_indexes_to_update  map[string]*Tabledef // indexes to update, because they contain columns that are updated. If nil, all indexes must be updated.
}

// Params for SHOW
//
type Show_params struct {
	Sh_output_type Sh_output_type_t // NOMRAL, ID, SQL, TEMPLATE
	Sh_template    *template.Template
	Sh_option_ALL  bool
	Sh_option_PERM bool
	Sh_LIKE        bool

	Sh_database_name string
	Sh_schema_name   string
	Sh_object_name   string
	Sh_LIKE_pattern  string
	Sh_flag_unique   bool

	Sh_server_default_collation string // sorting order for SHOW output
}

type Sh_output_type_t uint8

const (
	SH_OUTPUT_NORMAL Sh_output_type_t = 1 << iota
	SH_OUTPUT_ID
	SH_OUTPUT_SQL
	SH_OUTPUT_TEMPLATE
)

func (output_type Sh_output_type_t) String() string {

	switch output_type {
	case SH_OUTPUT_NORMAL:
		return "NORMAL"
	case SH_OUTPUT_ID:
		return "ID"
	case SH_OUTPUT_SQL:
		return "SQL"
	case SH_OUTPUT_TEMPLATE:
		return "TEMPLATE"

	default:
		return "unknown Sh_output_type_t"
	}
}

type Show_info_params struct {
	Shinf_servername               string
	Shinf_server_default_collation string

	Shinf_login            IDataslot
	Shinf_current_user     IDataslot
	Shinf_current_database IDataslot
	Shinf_current_schema   IDataslot
	Shinf_current_language IDataslot
}

type Dump_params struct {
	Dp_database_name   string
	Dp_option_DDL_ONLY bool
	Dp_filename        string

	Dp_server_default_collation string
}
