package mbin

import (
	"rsql"
	"rsql/lex"
)

type Mybin []byte // byte at index 0 is MSB when considered as a number, and byte 0 when considered as stream of bytes. Note: cast(0x0401 as varbinary(1)) = 0x04, and cast(0x0401 as int) = 1025

const HEXA_DIGITS = "0123456789abcdef" // used to convert a byte into hexa digit

// Hexa2bin copies source string made of hexa digits into dest, converting hexa digits into binary bytes.
// Empty binary string "0x" is allowed, and the result is an empty byte slice.
//
// The hexa string must begin with "0x", else, an error is returned.
// "0x" is followed by hexa digits, from most significant bytes (MSB) to least significant bytes (LSB) if considered as number, or first byte to last byte if stream of bytes.
//
// An error is returned if a non-hexa digit is found.
//
func Hexa2bin(dest *Mybin, source []byte) *rsql.Error {
	var (
		rsql_err        *rsql.Error
		number_of_bytes int
		i               int
		k               int
		b_high          uint8
		b_low           uint8
	)

	if len(source) < 2 || source[0] != '0' || source[1] != 'x' {
		return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_HEXASTRING_MUST_START_WITH_0x, rsql.ERROR_BATCH_ABORT)
	}

	source = source[2:] // discard "0x"

	if uint(len(source))&0x0001 != 0 { // length must be an even number
		return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_HEXASTRING_LENGTH_MUST_BE_EVEN, rsql.ERROR_BATCH_ABORT)
	}

	// enlarge dest if too small

	number_of_bytes = len(source) / 2 // two hexa digits make one byte

	if cap(*dest) < number_of_bytes {
		*dest = make(Mybin, 0, number_of_bytes)
	}

	*dest = (*dest)[:number_of_bytes]

	// convert string to hexa string

	i = 0
	k = 0

	for i < len(source) {
		if b_high, rsql_err = lex.Uc_hexa_digit_value(rune(source[i])); rsql_err != nil { // if not hexa digit, returns error.
			return rsql_err
		}

		if b_low, rsql_err = lex.Uc_hexa_digit_value(rune(source[i+1])); rsql_err != nil { // if not hexa digit, returns error.
			return rsql_err
		}

		(*dest)[k] = b_high*16 + b_low // make one byte from two valid hexa digits

		i += 2
		k++
	}

	rsql.Assert(k == number_of_bytes)

	return nil
}

func Bin2uint64(source Mybin) (uint64, *rsql.Error) {
	var (
		total uint64
		b     byte
	)

	if len(source) > 8 {
		return 0, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_HEXASTRING_LENGTH_EXCEEDS_UINT64_SIZE, rsql.ERROR_BATCH_ABORT)
	}

	for _, b = range source {
		total = total<<8 + uint64(b)
	}

	return total, nil
}

// Append_hexastring appends source in hexa notation, into dest string.
//
func Append_hexastring(dest []byte, source Mybin) []byte {
	var (
		b byte
	)

	for _, b = range source {
		dest = append(dest, HEXA_DIGITS[b>>4], HEXA_DIGITS[b&0x0f])
	}

	return dest
}
