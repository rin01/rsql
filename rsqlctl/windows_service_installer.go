

// +build windows


package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"
	"flag"
	"path/filepath"

	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/mgr"
)

func install_service(service_name string, exepath string, description string, start_type string, account string, password string, instance_dir string) error {
	var err error
	var m *mgr.Mgr
	var s *mgr.Service
	var config mgr.Config
	var start_type_value uint32

	// check service_name is ascii

	for _, cc := range service_name {
		if !((cc >= 'a' && cc <= 'z') || (cc >= 'A' && cc <= 'Z') || (cc >= '0' && cc <= '9') || (cc == '_')) {
			return fmt.Errorf("Service name can only contain the characters a..z, A..Z, 0..9 or _")
		}
	}

	if start_type == "" {
		start_type = "manual"
	}

	if len(flag.Args()) != 0 {
		log.Fatalf("Unexpected parameter %s", flag.Args()[0])
	}

	if account == "" {
		// account = "NT AUTHORITY\\LocalService"
		// account is "", Windows will create a service running under the high-privileged LocalSystem account, which can access the instance directory without setting permissions
	}

	if exepath == "" {
		log.Fatalf("Parameter -exepath=<absolute path to executable> is expected.")
	}

	if filepath.IsAbs(exepath) == false {
		log.Fatalf("-exepath argument must be an absolute path.")
	}

	if filepath.IsAbs(instance_dir) == false {
		log.Fatalf("-instance_dir argument must be an absolute path.")
	}

	for _, cc := range instance_dir {
		if cc == ' ' {
			return fmt.Errorf("instance_dir \"%s\" cannot contain blank characters.", instance_dir)
		}
	}

	// connect to service manager

	if m, err = mgr.Connect(); err != nil {
		return err
	}
	defer m.Disconnect()

	// check if service already exists

	if s, err = m.OpenService(service_name); err == nil {
		s.Close()
		return fmt.Errorf("Service %s already exists.", service_name)
	}

	// create service

	switch strings.ToLower(start_type) {
	case "automatic":
		start_type_value = mgr.StartAutomatic
	case "manual":
		start_type_value = mgr.StartManual
	default:
		return fmt.Errorf("start type must be \"automatic\" or \"manual\"")
	}

	config = mgr.Config{
		StartType:        start_type_value,
		BinaryPathName:   exepath,
		ServiceStartName: account, // name of the account under which the service should run
		Password:         password,
		DisplayName:      service_name,
		Description:      description,
	}

	if s, err = m.CreateService(service_name, exepath, config, fmt.Sprintf("-service=%s", service_name), fmt.Sprintf("-dir=%s", instance_dir)); err != nil {
		return err
	}
	defer s.Close()

	return nil
}

func remove_service(service_name string) error {
	var err error
	var m *mgr.Mgr
	var s *mgr.Service

	// connect to service manager

	if m, err = mgr.Connect(); err != nil {
		return err
	}
	defer m.Disconnect()

	// get service

	if s, err = m.OpenService(service_name); err != nil {
		return fmt.Errorf("Cannot access service %s or service not installed. %v", service_name, err)
	}
	defer s.Close()

	// mark service s for deletion from the service manager database.

	if err = s.Delete(); err != nil {
		return err
	}

	return nil
}

func start_service(service_name string) error {
	var err error
	var m *mgr.Mgr
	var s *mgr.Service

	// connect to service manager

	if m, err = mgr.Connect(); err != nil {
		return err
	}
	defer m.Disconnect()

	// get service

	if s, err = m.OpenService(service_name); err != nil {
		return fmt.Errorf("Cannot access service %s or service not installed. %v", service_name, err)
	}
	defer s.Close()

	// start service

	if err = s.Start(); err != nil {
		return fmt.Errorf("Cannot start service %s: %v", service_name, err)
	}

	return nil
}

func query_service(service_name string) error {
	var err error
	var m *mgr.Mgr
	var s *mgr.Service
	var status svc.Status

	// connect to service manager

	if m, err = mgr.Connect(); err != nil {
		return err
	}
	defer m.Disconnect()

	// get service

	if s, err = m.OpenService(service_name); err != nil {
		return fmt.Errorf("Cannot access service %s or service not installed. %v", service_name, err)
	}
	defer s.Close()

	// query service

	if status, err = s.Query(); err != nil {
		return fmt.Errorf("Cannot query service %s: %v", service_name, err)
	}

	fmt.Printf("Service %s is %s\n", service_name, state_string(status.State))

	return nil
}

// send command to service.
func control_service(service_name string, c svc.Cmd, to_state svc.State) error {
	var err error
	var m *mgr.Mgr
	var s *mgr.Service
	var status svc.Status

	// connect to service manager

	if m, err = mgr.Connect(); err != nil {
		return err
	}
	defer m.Disconnect()

	// get service

	if s, err = m.OpenService(service_name); err != nil {
		return fmt.Errorf("Cannot access service %s or service not installed. %v", service_name, err)
	}
	defer s.Close()

	// send control to service

	if status, err = s.Control(c); err != nil {
		return fmt.Errorf("Cannot %s service: %v", command_string(c), err)
	}

	timeout := time.Now().Add(70 * time.Second) // rsql server shutdown takes 60 seconds if workers are still running. With 70 seconds timeout, the stop command should succeed.

	fmt.Print(".")

	for status.State != to_state {
		if timeout.Before(time.Now()) {
			return fmt.Errorf("Timeout waiting for service to %s.", command_string(c))
		}

		time.Sleep(1 * time.Second)

		if status, err = s.Query(); err != nil {
			return fmt.Errorf("Cannot retrieve service status: %v", err)
		}

		fmt.Print(".")
	}

	fmt.Println()

	return nil
}

func command_string(c svc.Cmd) string {

	switch c {
	case svc.Stop:
		return "<Stop>"
	case svc.Pause:
		return "<Pause>"
	case svc.Continue:
		return "<Continue>"
	case svc.Interrogate:
		return "<Interrogate>"
	case svc.Shutdown:
		return "<Shutdown>"
	default:
		return "<unknown command>"
	}
}

func state_string(c svc.State) string {

	switch c {
	case svc.Stopped:
		return "Stopped"
	case svc.StartPending:
		return "StartPending"
	case svc.StopPending:
		return "StopPending>"
	case svc.Running:
		return "Running"
	case svc.ContinuePending:
		return "ContinuePending"
	case svc.PausePending:
		return "PausePending"
	case svc.Paused:
		return "Paused"
	default:
		return "<unknown state>"
	}
}

func Print_usage_and_exit(errmsg string) {

	fmt.Fprintf(os.Stderr, "%s\n\n"+
		"usage: rsqlctl -install -service_name=<service name> -instance_dir=<instance directory> -exepath=<executable path>\n"+
		"       where <service name>       is the name of the service\n" +
		"             <instance directory> is the absolute path to the instance directory\n" +
		"             <executable path>    is the absolute path to the rsql executable\n" +
		"       The following flags are optional:\n" +
		"             -description=<description of the service>\n" +
		"             -start_type=manual or automatic (default is manual)\n" +
		"             -account=<account which runs the service>\n" +
		"             -password=<password of the account>\n" +
		"\n" +
		"       rsqlctl remove <service name>\n" +
		"       rsqlctl start  <service name>\n" +
		"       rsqlctl stop   <service name>\n" +
		"       rsqlctl status <service name>\n",
		errmsg)

	os.Exit(2)
}

func main() {
	var (
		err error

		install_flag bool
		remove_flag bool
		start_flag bool
		stop_flag bool
		status_flag bool
		command string
		service_name string

		exepath string // only for "install" command
		description string // only for "install" command
		start_type string // only for "install" command
		account string // only for "install" command
		password string // only for "install" command
		instance_dir string // only for "install" command
	)

	flag.BoolVar(&install_flag, "install", false, "command to install the service")
	flag.BoolVar(&remove_flag, "remove", false, "command to remove the service")
	flag.BoolVar(&start_flag, "start", false, "command to start the service")
	flag.BoolVar(&stop_flag, "stop", false, "command to stop the service")
	flag.BoolVar(&status_flag, "status", false, "command to display the status of the service")

	flag.StringVar(&service_name, "service_name", "", "name of the service")
	flag.StringVar(&exepath, "exepath", "", "absolute path of the service executable")
	flag.StringVar(&description, "description", "", "description of the service")
	flag.StringVar(&start_type, "start_type", "", "start type of the service \"manual\" or \"automatic\"")
	flag.StringVar(&account, "account", "", "account which runs the service (default LocalSystem)")
	flag.StringVar(&password, "password", "", "password of the account")
	flag.StringVar(&instance_dir, "instance_dir", "", "absolute path of the instance directory")

	flag.Parse()

	command_flag_count := 0
	if install_flag {
		command = "install" // can only be specified as the flag -install
		command_flag_count++
	}
	if remove_flag {
		command = "remove"
		command_flag_count++
	}
	if start_flag {
		command = "start"
		command_flag_count++
	}
	if stop_flag {
		command = "stop"
		command_flag_count++
	}
	if status_flag {
		command = "status"
		command_flag_count++
	}

	switch command_flag_count {
	case 0: // allowed syntax:   rsqlctl <command> <service name>
		if len(os.Args) != 3 {
			Print_usage_and_exit("command and service name are expected")
		}
		command = os.Args[1]
		service_name = os.Args[2]

	case 1: // syntax is:    rsqlctl -<command> <service name>
		if command != "install" {
			if service_name == "" && len(flag.Args()) == 1 {
				service_name = flag.Args()[0]
			}			
		}

	default:
		Print_usage_and_exit("Only one command (-install, remove, start, stop, status) can be specified.")
	}

	if service_name == "" {
		Print_usage_and_exit("service name is expected")
	}

	/*
	fmt.Printf("command      : <%s>\n", command)
	fmt.Printf("service_name : <%s>\n", service_name)
	fmt.Printf("exepath      : <%s>\n", exepath)
	fmt.Printf("description  : <%s>\n", description)
	fmt.Printf("start_type   : <%s>\n", start_type)
	fmt.Printf("account      : <%s>\n", account)
	fmt.Printf("password     : <%s>\n", password)
	fmt.Printf("instance_dir : <%s>\n", instance_dir)
	fmt.Printf("args         : %v\n", flag.Args())
	*/

	// execute command

	switch command {
	case "install":
		err = install_service(service_name, exepath, description, start_type, account, password, instance_dir)

	case "remove":
		err = remove_service(service_name)

	case "start":
		err = start_service(service_name)

	case "stop":
		err = control_service(service_name, svc.Stop, svc.Stopped)

	case "status":
		err = query_service(service_name)

	default:
		Print_usage_and_exit(fmt.Sprintf("invalid command %s", command))
	}

	if err != nil {
		log.Fatalf("Failed to %s %s: %v", command, service_name, err)
	}
}

