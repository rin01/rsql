// +build !windows

package main

import (
	"fmt"
	// "golang.org/x/sys/windows/svc"
)

type Windows_service struct{}

func Service_run(name string, handler interface{}) error {

	return fmt.Errorf("Service_run should never be called in Linux session")
}

func Is_Windows_service_session() bool {

	return false
}
