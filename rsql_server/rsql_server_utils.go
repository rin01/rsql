package main

import (
	"errors"
	"fmt"
	"strings"

	"rsql/msgp"

	"rsql"
)

type Msg_auth struct {
	login_name   string // mandatory
	password     string // mandatory
	database     string // optional (may be empty string)
	opt_showtree bool   // show AST tree
	opt_no_cf    bool   // optional. Disable constant folding, only allowed to 'sa' for debugging
	opt_no_exec  bool   // optional. Compile but don't run the batch.
}

func assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

// read_request_type reads the request type byte sent by client.
// It can be REQTYP_AUTH, REQTYP_BATCH, etc.
// This allows us to read the message content that follows, as we know its structure.
//
func read_request_type(m *msgp.Reader) (rsql.Request_t, error) {
	var (
		err error
		u   uint8
	)

	if u, err = m.ReadUint8(); err != nil {
		return 0, err
	}

	return rsql.Request_t(u), nil
}

// read_auth_message reads authentication message sent by client.
// Options are also included in this message.
//
// This function reads the message following request type REQTYP_AUTH.
//
func read_auth_message(m *msgp.Reader) (msg_auth *Msg_auth, err error) {
	var (
		sz uint32
	)

	// read authentication info

	sz, err = m.ReadMapHeader()
	if err != nil {
		return nil, err
	}

	msg_auth = &Msg_auth{}

	for i := uint32(0); i < sz; i++ {
		var (
			key      string
			val_str  string
			val_bool bool
		)

		key, err = m.ReadString()
		if err != nil {
			return nil, err
		}

		switch key {
		case "login_name":
			val_str, err = m.ReadString()
			if err != nil {
				return nil, err
			}

			msg_auth.login_name = strings.ToLower(val_str)

		case "password":
			val_str, err = m.ReadString()
			if err != nil {
				return nil, err
			}

			msg_auth.password = val_str

		case "database":
			val_str, err = m.ReadString()
			if err != nil {
				return nil, err
			}

			msg_auth.database = strings.ToLower(val_str)

		case "opt_showtree": // option
			val_bool, err = m.ReadBool()
			if err != nil {
				return nil, err
			}

			msg_auth.opt_showtree = val_bool

		case "opt_no_cf": // option
			val_bool, err = m.ReadBool()
			if err != nil {
				return nil, err
			}

			msg_auth.opt_no_cf = val_bool

		case "opt_no_exec": // option
			val_bool, err = m.ReadBool()
			if err != nil {
				return nil, err
			}

			msg_auth.opt_no_exec = val_bool

		default:
			err = fmt.Errorf("msg_auth: unknown auth key \"%s\" received", key)
			return nil, err
		}
	}

	// check

	if msg_auth.login_name == "" { // mandatory
		return nil, errors.New("msg_auth: no login name found")
	}

	if msg_auth.password == "" { // mandatory
		return nil, errors.New("msg_auth: no password found")
	}

	return msg_auth, nil
}

// read_batch_text reads SQL text sent from client.
// If error during read, error is returned.
// If text is too long, *rsql.Error is returned.
//
// This function reads the message following request type REQTYP_BATCH.
//
func read_batch_text(m *msgp.Reader) (text []byte, err error) {
	var (
		sz uint32
		n  int
	)

	if sz, err = m.ReadStringHeader(); err != nil {
		return nil, err
	}

	if int(sz) > rsql.G_BATCH_TEXT_MAX_SIZE { // we must abort the session, because the current batch text is still in the connection and we cannot proceed further
		return nil, rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_BATCH_TOO_LARGE, rsql.ERROR_SESSION_ABORT, sz, rsql.G_BATCH_TEXT_MAX_SIZE)
	}

	text = make([]byte, sz)

	if n, err = m.ReadFull(text); err != nil {
		return nil, err
	}

	assert(n == int(sz))

	return text, nil
}

// write_login_response_type_and_flush sends RESTYP_LOGIN_FAILED or RESTYP_LOGIN_SUCCESS to client.
//
func write_login_response_type_and_flush(mw *msgp.Writer, resptyp rsql.Response_t) (doomed error) {

	mw.WriteUint8(uint8(resptyp))

	if doomed := mw.Flush(); doomed != nil {
		return doomed
	}

	return nil
}

// write_login_response_type_and_info_and_flush sends RESTYP_LOGIN_FAILED or RESTYP_LOGIN_SUCCESS to client, and also some server information.
//
func write_login_response_type_and_info_and_flush(mw *msgp.Writer, resptyp rsql.Response_t) (doomed error) {

	mw.WriteUint8(uint8(resptyp))

	// some server info

	mw.WriteMapHeader(3) // THERE ARE 3 ENTRIES TO SEND. IF YOU SEND MORE OR LESS ENTRIES TO THE CLIENT, YOU MUST CHANGE THIS NUMBER.

	mw.WriteString("server_version_string")
	mw.WriteString(rsql.VERSION)

	mw.WriteString("server_batch_text_max_size")
	mw.WriteInt64(int64(rsql.G_BATCH_TEXT_MAX_SIZE))

	mw.WriteString("server_read_timeout")
	mw.WriteInt64(rsql.Param_read_timeout)

	if doomed := mw.Flush(); doomed != nil {
		return doomed
	}

	return nil
}

// write_rsql_err sends RESTYP_ERROR and error message to client, if error occurred.
//
// This function doesn't flush the write buffer, as it must be followed by write_batch_RC_and_flush, which flushes.
//
func write_rsql_err(mw *msgp.Writer, rsql_err *rsql.Error) (doomed error) {

	mw.WriteUint8(uint8(rsql.RESTYP_ERROR))

	rsql.Write_error_msgp(mw, rsql_err)

	if doomed := mw.Error(); doomed != nil {
		return doomed
	}

	return nil
}

// write_rsql_err sends RESTYP_BATCH_END and return code to client, at end of batch.
//
func write_batch_RC_and_flush(mw *msgp.Writer, rc int64) (doomed error) {

	mw.WriteUint8(uint8(rsql.RESTYP_BATCH_END))

	mw.WriteInt64(rc)

	if doomed := mw.Flush(); doomed != nil {
		return doomed
	}

	return nil
}
