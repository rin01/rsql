// +build windows

package main

import (
	"log"

	"rsql/stmt"

	"golang.org/x/sys/windows/svc"
)

type Windows_service struct{}

func Service_run(name string, handler interface{}) error {

	return svc.Run(name, handler.(svc.Handler))
}

func (m *Windows_service) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	var stopped bool

	changes <- svc.Status{State: svc.StartPending}

	changes <- svc.Status{State: svc.Running, Accepts: svc.AcceptStop | svc.AcceptShutdown}

loop:
	for {
		c := <-r

		switch c.Cmd {
		case svc.Interrogate:
			changes <- c.CurrentStatus

		case svc.Stop, svc.Shutdown:
			stopped = stmt.Windows_service_stop() // if all workers stopped, return true. Else, after 60 seconds timeout, return false as some workers are still running.
			break loop

		default:
			// ignore
		}
	}

	changes <- svc.Status{State: svc.StopPending}

	_ = stopped

	//if stopped == false { // no need to kill the server here, as svc.Run() in rsql_server.go code will terminate
	//	os.Exit(1)
	//}

	return
}

func Is_Windows_service_session() bool {

	res, err := svc.IsAnInteractiveSession()
	if err != nil {
		log.Fatalf("failed to determine if we are running in an interactive session: %v", err)
	}

	return !res
}
