package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof" // profiling
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"sync/atomic"
	"syscall"
	"time"

	"gopkg.in/natefinch/lumberjack.v2"

	"rsql"
	"rsql/ast"
	"rsql/cache"
	"rsql/deco"
	"rsql/dict"
	"rsql/emit"
	"rsql/lk"
	"rsql/msgp"
	"rsql/quad"
	"rsql/stmt"
	"rsql/vm"
)

var g_sigterm_chan chan os.Signal // watch for SIGTERM signal, indicating that server must shutdown

var (
	param_ppage_array_size_total_max_limit        uint64 // count of pages
	param_element_array_size_total_max_soft_limit uint64 // count of elements
)

func main() {
	var (
		//		rsql_err *rsql.Error

		version_flag  bool
		instance_name string

		install_flag             bool
		server_default_collation string // only allowed for '-install'
		server_default_language  string // only allowed for '-install'

		instance_path           string
		server_listener_address string
		logpath                 string
		logging_no_date_flag    bool
		profiler_address        string

		server_global_page_cache_memory_mb uint64

		windows_service_name string // only for Windows when run as a service
	)

	//=== options ===

	flag.BoolVar(&version_flag, "version", false, "displays version.")

	flag.BoolVar(&rsql.G_DEBUG_FLAG, "debug", false, "displays messages for debugging.")

	flag.BoolVar(&install_flag, "install", false, "initializes the instance directory specified by -dir, to make it usable by the database server. -dir, -server_default_collation, -server_default_language options are mandatory, -listener_address is optional.")
	flag.StringVar(&server_default_collation, "server_default_collation", "", "server default collation, e.g. en_ci_ai, latin1_general_ci_ai, fr_cs_as, etc. Only allowed with option -install.")
	flag.StringVar(&server_default_language, "server_default_language", "", "server default language, e.g. en-us, fr-fr, fr-ch, etc. Only allowed with option -install.")

	flag.StringVar(&instance_path, "dir", "", "instance directory (absolute path), where all data will be stored.")
	flag.StringVar(&server_listener_address, "listener_address", "", "listening address, e.g. localhost:7777")

	flag.StringVar(&logpath, "logpath", "", "path of the logging file. If just a filename is given, e.g. \"rsql1.log\", it will be put in the \"logging\" subdirectory of the instance. Else, an absolute path must be given.")
	flag.BoolVar(&logging_no_date_flag, "logging_no_date", false, "do not output current date and time in log messages.")

	flag.Uint64Var(&server_global_page_cache_memory_mb, "global_cache_memory", 0, "size of the global cache memory, in MB. If not specified, the value is read from server parameters.")

	flag.StringVar(&profiler_address, "profiler_address", "", "listening address for the profiler, e.g. localhost:6060.")

	flag.StringVar(&windows_service_name, "service", "", "Windows service name. It is for internal use only.")

	flag.Parse()

	if version_flag {
		fmt.Printf("rsql server: version %s\n", rsql.VERSION)
		os.Exit(0)
	}

	// get instance name from instance_path flag, and set logger prefix

	if instance_path == "" || filepath.IsAbs(instance_path) == false {
		log.Fatal("-dir option is mandatory, and path must be absolute.")
	}

	instance_name = filepath.Base(instance_path)
	if instance_name == "." {
		log.Fatal("-dir option: no valid path found.")
	}

	log.SetFlags(log.LstdFlags)
	log.SetPrefix("rsql:" + instance_name + ": ")

	switch {
	case rsql.G_DEBUG_FLAG == true:
		log.SetFlags(log.LstdFlags | log.Lshortfile) // log prints filename:line no

		log.Println(quad.G_DECQUAD_MACROS)

	case logging_no_date_flag == true: // do not output date and time in log messages
		log.SetFlags(0)
	}

	//===== chdir to instance directory =====

	if err := os.Chdir(instance_path); err != nil {
		time.Sleep(1 * time.Second) // so that systemd has time to collect information (https://bugs.freedesktop.org/show_bug.cgi?id=50184)
		log.Fatalf("%s", err)
	}

	log.Printf("rsql instance directory changed to \"%s\".", instance_path)

	//===== "install" option: create master.db and directories =====

	if install_flag == true {
		if server_default_collation == "" || server_default_language == "" {
			log.Fatal("for '-install' option, '-server_default_collation' and '-server_default_language' options are mandatory.")
		}

		dict.Install_master_db(instance_name, strings.ToLower(server_default_collation), strings.ToLower(server_default_language), server_listener_address) // log.Fatal if error
		log.Println("=== INITIALIZE INSTANCE DIRECTORY: successfully initialized ===")

		return //----> instance directory has been initialized, and installation is finished
	}

	//===== server_default_collation and server_default_language are not allowed =====

	if server_default_collation != "" {
		log.Fatal("if not '-install' option, '-server_default_collation' option is not allowed.")
	}

	if server_default_language != "" {
		log.Fatal("if not '-install' option, '-server_default_language' option is not allowed.")
	}

	//===== start the listener, spawn a new goroutine for each connection, and return only when shutdown received =====

	// Windows service

	if Is_Windows_service_session() {

		go startup_server(instance_path, server_listener_address, logpath, logging_no_date_flag, profiler_address, server_global_page_cache_memory_mb)

		if err := Service_run(windows_service_name, &Windows_service{}); err != nil {
			return
		}

		os.Exit(0) // Windows service terminates
	}

	// normal Linux server, or interactive Windows session on terminal

	startup_server(instance_path, server_listener_address, logpath, logging_no_date_flag, profiler_address, server_global_page_cache_memory_mb)

}

// start the listener, load dictionary from master.db, init the global cache, init collection of journals, init lock manager, spawn a new goroutine for each connection, and return only when shutdown received.
//
func startup_server(instance_path string, server_listener_address string, logpath string, logging_no_date_flag bool, profiler_address string, server_global_page_cache_memory_mb uint64) {

	//===== read listener_address file and create listener =====

	rsql.G_LISTENER_ADDRESS = server_listener_address
	if rsql.G_LISTENER_ADDRESS == "" {
		rsql.G_LISTENER_ADDRESS = strings.TrimSpace(rsql.Must_read_ordinary_file(rsql.LISTENER_ADDRESS_FILENAME))
	}

	ln, err := net.Listen("tcp", rsql.G_LISTENER_ADDRESS)
	if err != nil {
		time.Sleep(1 * time.Second) // so that systemd has time to collect information (https://bugs.freedesktop.org/show_bug.cgi?id=50184)
		log.Fatalf("listener creation failed: %s", err)
	}
	defer ln.Close()

	log.Printf("rsql listener address is %s\n", rsql.G_LISTENER_ADDRESS)

	//===== read version file =====

	version_string := strings.TrimSpace(rsql.Must_read_ordinary_file(rsql.VERSION_FILENAME))

	if rsql.Check_version(version_string) == false {
		time.Sleep(1 * time.Second) // so that systemd has time to collect information (https://bugs.freedesktop.org/show_bug.cgi?id=50184)
		log.Fatalf("The instance directory is version \"%s\", which is incompatible with server version \"%s\".", version_string, rsql.VERSION)
	}

	log.Printf("rsql storage version: %s\n", version_string)
	log.Printf("rsql server  version: %s\n", rsql.VERSION)

	//===== delete all content of tempdir =====

	rsql.Must_remove_directory_content(rsql.DIRECTORY_TEMPDIR)

	//===== open MASTER =====

	dict.MASTER.Open()

	//===== logging file =====

	if logpath != "" {
		if filepath.IsAbs(logpath) == false {
			switch {
			case strings.ContainsRune(logpath, filepath.Separator) == false: // if only filename, logging file is put under the "logging" directory
				logpath = filepath.Join(instance_path, rsql.DIRECTORY_LOGGING, logpath)
			default:
				log.Fatal("option -filepath: the path must be a single file name or an absolute path.")
			}
		}

		if filepath.Ext(logpath) != ".log" {
			log.Fatalf("option -filepath: logging filename \"%s\" must have .log extension.", filepath.Base(logpath))
		}

		logging_max_size_mb, _ := dict.MASTER.Must_load_params_from_master_db(dict.PARAM_SERVER_LOGGING_MAX_SIZE) // read from master.db

		logging_max_count, _ := dict.MASTER.Must_load_params_from_master_db(dict.PARAM_SERVER_LOGGING_MAX_COUNT) // read from master.db

		localtime_flag := false
		if val, _ := dict.MASTER.Must_load_params_from_master_db(dict.PARAM_SERVER_LOGGING_LOCALTIME); val != 0 { // read from master.db
			localtime_flag = true
		}

		lj_logger := &lumberjack.Logger{
			Filename:   logpath,                  // lumnerjack will create the directories in the path
			MaxSize:    int(logging_max_size_mb), // megabytes
			MaxBackups: int(logging_max_count),
			MaxAge:     0, //days
			LocalTime:  localtime_flag,
		}

		log.SetOutput(lj_logger)
	}

	// write some info into logging file

	if logpath != "" {
		if err := log.Output(1, fmt.Sprintf("=== rsql instance \"%s\" is starting ===.", instance_path)); err != nil {
			fmt.Printf("LOGGING FILE ERROR: %s\n", err)
			time.Sleep(1 * time.Second) // so that systemd has time to collect information (https://bugs.freedesktop.org/show_bug.cgi?id=50184)
			os.Exit(1)
		}
		log.Printf("rsql instance directory changed to \"%s\".", instance_path)
		log.Printf("rsql listener address is %s\n", rsql.G_LISTENER_ADDRESS)
		log.Printf("rsql storage version: %s\n", version_string)
		log.Printf("rsql server  version: %s\n", rsql.VERSION)
	}

	//===== load dictionary from master.db =====

	dict.MASTER.Load_dict_from_master_db() // log.Fatal if error. Remove database, tables or indexes that are half-created or half-deleted.

	log.Println("dictionary successfully loaded into memory.")

	//===== read SERVERNAME and server default collation from dict =====

	rsql.G_SERVERNAME = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_SERVERNAME)

	rsql.G_SERVER_DEFAULT_COLLATION = dict.MASTER.Get_sysparameters_string_value(dict.PARAM_SERVER_DEFAULT_COLLATION)

	//===== init global read cache =====

	if server_global_page_cache_memory_mb == 0 {
		server_global_page_cache_memory_mb = uint64(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_GLOBAL_PAGE_CACHE_MEMORY)) // in MB
	}

	cache.GPAGE_CACHE.Init(server_global_page_cache_memory_mb * 1024 * 1024) // memory in bytes. If 0, 16 pages in cache

	log.Println("global read cache successfully initialized.")

	//===== init collection of journals =====

	cache.WORKERS_JOURNALS.Initialize() // rollforward if pending transactions detected

	log.Println("journals initialized.")

	//===== init rsql.Execute_basicblock function pointer =====

	rsql.Execute_basicblock = vm.Execute_basicblock // other packages can execute basic blocks without importing package "rsql/vm", to avoid circular imports

	//===== init lock manager =====

	lock_ticker_interval := time.Duration(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_LOCK_TICKER_INTERVAL)) * time.Millisecond
	lock_timeout_ticks_count := uint64(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_LOCK_TIMEOUT_TICKS_COUNT))

	lk.G_LOCK_MANAGER.Initialize(lock_ticker_interval, lock_timeout_ticks_count)

	//===== get server params =====

	param_ppage_array_size_total_max_limit = uint64(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_WCACHE_MEMORY_MAX)) * 1024 * 1024 / cache.PAGE_SIZE // MB --> count of pages
	if param_ppage_array_size_total_max_limit < cache.WCACHE_PPAGE_ARRAY_SIZE_TOTAL_MAX_LIMIT_LOWEST {
		param_ppage_array_size_total_max_limit = cache.WCACHE_PPAGE_ARRAY_SIZE_TOTAL_MAX_LIMIT_LOWEST
	}

	param_element_array_size_total_max_soft_limit = uint64(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_WCACHE_MODIF_MAX)) * 1024 * 1024 / cache.PAGE_SIZE // MB --> count of pages
	if param_element_array_size_total_max_soft_limit < cache.WCACHE_ELEMENT_ARRAY_SIZE_TOTAL_MAX_SOFT_LIMIT_LOWEST {
		param_element_array_size_total_max_soft_limit = cache.WCACHE_ELEMENT_ARRAY_SIZE_TOTAL_MAX_SOFT_LIMIT_LOWEST
	}

	log.Printf("Init: max size for each private cache == %d pages of %d bytes (%.1f MB)\n", param_ppage_array_size_total_max_limit, cache.PAGE_SIZE, float64(param_ppage_array_size_total_max_limit*cache.PAGE_SIZE)/1024/1024)

	rsql.Param_read_timeout = dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_READ_TIMEOUT) // seconds
	if rsql.Param_read_timeout < dict.SPEC_READ_TIMEOUT_MIN_LIMIT {
		rsql.Param_read_timeout = dict.SPEC_READ_TIMEOUT_MIN_LIMIT
	}

	log.Printf("parameter READ_TIMEOUT = %d seconds", rsql.Param_read_timeout)

	//===== get some param values from dict =====

	rsql.G_BATCH_TEXT_MAX_SIZE = int(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_BATCH_TEXT_MAX_SIZE)) // in bytes
	rsql.G_BATCH_INSERTS_MAX_COUNT = int(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_BATCH_INSERTS_MAX_COUNT))

	//==============================
	//           listen
	//==============================

	// profiler http server. The profiler can run on a production server, as it demands only few resource.

	if profiler_address != "" { // this is only a web server. The profiling is already running, as package "net/http/pprof" has been imported.
		go func() {
			log.Println(http.ListenAndServe(profiler_address, nil))
		}()
	}

	// initialize worker list

	workers_number_max := int(dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_WORKERS_MAX))
	cache.G_WORKER_QUEUE.Initialize(workers_number_max)

	// watch for SIGTERM signal, to halt the server

	g_sigterm_chan = make(chan os.Signal, 1)

	signal.Notify(g_sigterm_chan, syscall.SIGTERM)

	go func() {
		<-g_sigterm_chan // waiting for SIGTERM signal

		stmt.SIGTERM_handler()

		os.Exit(1)
	}()

	// listener listening for requests

	for {
		conn, err := ln.Accept() // accept a new connection
		if err != nil {
			log.Printf("listener Accept() failed: %s", err)
			os.Exit(1)
		}

		if atomic.LoadUint32(&rsql.G_server_shutdown_flag) > 0 {
			conn.Close()
			ln.Close() // close the listener
			break
		}

		worker := cache.G_WORKER_QUEUE.Pop() // get a worker

		go handle_connection(conn, worker) // spawn a goroutine to handle the session
	}

	// shutdown occurred, and an incomming connection has been refused.

	select {} // wait here until the session running the SHUTDOWN command kills the server process
}

func handle_connection(conn net.Conn, worker *cache.Worker) {
	var (
		err           error
		rsql_err_auth *rsql.Error
		writer        *msgp.Writer
		reader        *msgp.Reader
		reqtyp        rsql.Request_t

		batch_channel chan Ch_message
		msg_auth      *Msg_auth
		done_channel  chan bool

		login_name             string
		password               string
		login_force_database   string // client forces this database
		login_id               int64
		login_default_language string
		login_default_database string
		current_database       string
		current_options        ast.Options_t

		journal *cache.Journal // journal for this worker for the whole session (that is, until the connection ends)

		// keep Registration, Wcache and Context for running transaction

		context      *rsql.Context
		registration *lk.Registration
		wcache       *cache.Wcache
	)

	defer cache.G_WORKER_QUEUE.Push(worker)
	if rsql.G_DEBUG_FLAG {
		defer func() {
			pin_count, lru_count, free_count := cache.GPAGE_CACHE.Lists_count() // not thread-safe, but we don't care
			log.Printf("info global cache: pinned %d, lru %d, free %d", pin_count, lru_count, free_count)
		}()

		defer log.Println("DEBUG: CONNECTION CLOSED")
	}
	defer conn.Close()

	writer = msgp.NewWriter(conn)
	reader = msgp.NewReader(conn)

	// read authentication request

	if err = conn.SetReadDeadline(time.Now().Add(time.Duration(rsql.Param_read_timeout) * time.Second)); err != nil {
		log.Printf("server: conn.SetReadDeadline() failed: %s", err) // should not happen
		goto FATAL_COMMUNICATION_FAILURE
	}

	if reqtyp, err = read_request_type(reader); err != nil {
		log.Printf("server: read_request_type() failed: %s", err)
		goto FATAL_COMMUNICATION_FAILURE // connection is broken, or communication protocol is messed up, or login/password is incorrect
	}

	if reqtyp != rsql.REQTYP_AUTH {
		log.Printf("server: REQTYP_AUTH expected, got %s", reqtyp)
		goto FATAL_COMMUNICATION_FAILURE
	}

	if msg_auth, err = read_auth_message(reader); err != nil {
		log.Printf("server: read_auth_message() failed: %s", err)
		goto FATAL_COMMUNICATION_FAILURE
	}

	login_name = msg_auth.login_name         // always exists
	password = msg_auth.password             // always exists
	login_force_database = msg_auth.database // may be emtpy string

	// authenticate

	if login_name, login_id, login_default_language, login_default_database, rsql_err_auth = dict.MASTER.Authenticate_login(login_name, password); rsql_err_auth != nil {
		_ = write_login_response_type_and_flush(writer, rsql.RESTYP_LOGIN_FAILED)
		goto FATAL_COMMUNICATION_FAILURE
	}

	if err = write_login_response_type_and_info_and_flush(writer, rsql.RESTYP_LOGIN_SUCCESS); err != nil { // AUTHENTICATION HAS BEEN SUCCESSFUL, some server info are sent to client
		log.Printf("server: write_login_response_type_and_info_and_flush() failed: %s", err)
		goto FATAL_COMMUNICATION_FAILURE
	}

	current_database = login_default_database

	if login_force_database != "" {
		current_database = login_force_database
	}

	current_options = ast.Get_default_options() // default options for parser

	// fill in information in Worker object

	worker.Lock()

	worker.Wk_remote_address = conn.RemoteAddr().String()
	worker.Wk_start_time = time.Now()
	worker.Wk_login_id = login_id
	worker.Wk_login_name = login_name

	worker.Unlock()

	// create Context

	context = rsql.New_context(login_id, login_name) // this Context object lives until this function terminates

	context.Ctx_worker_id = worker.Wk_id
	context.Ctx_conn = conn
	context.Ctx_mw = writer

	// loop to process incoming batches one after another

	batch_channel = make(chan Ch_message)

	done_channel = make(chan bool) // closing this channel indicates to the batch_reader groutine that it can terminate
	defer close(done_channel)

	go batch_reader(context, batch_channel, done_channel, reader) // goroutine dedicated to reading batches and keepalives from client
	reader = nil                                                  // only batch_reader goroutine reads from connection

	// get the journal for this worker

	journal = cache.WORKERS_JOURNALS.Get_journal(cache.Journal_id_t(worker.Wk_id))

	// defer function to release wcache, journal and locks

	defer func() {
		// release wcache

		if wcache != nil {
			wcache.Filebag().Close_all()
			wcache.Close_flashjournal()
		}

		// release journal

		if journal != nil {
			cache.WORKERS_JOURNALS.Release_journal(journal) // also resets the journal
		}

		// release locks

		if registration != nil {
			lk.G_LOCK_MANAGER.Release(registration)
		}
	}()

	for { // each batch gets a new Decorator
		var (
			batch_text                  []byte
			err                         error
			rsql_err                    *rsql.Error
			decor                       *deco.Decorator
			retval                      int64
			process_next_batch_on_error bool // if error occurs during execution (and not during parsing), process the next batch
			msg                         Ch_message
		)

		// check if server is being shutdown

		if atomic.LoadUint32(&rsql.G_server_shutdown_flag) > 0 {
			return
		}

		// reset some values

		batch_text = nil
		err = nil
		_ = err
		rsql_err = nil
		decor = nil

		if context.Ctx_trancount == 0 {
			assert(registration == nil)
			assert(wcache == nil)
			assert(context.Ctx_wcache == nil)
			context.Clear_for_new_batch() // Ctx_wcache is set to nil
		}

		process_next_batch_on_error = false
		msg = Ch_message{}

		// check dictionary maps in memory, for debugging

		if XDEBUG_DICTIONARY {
			dict.MASTER.Check_dictionary_maps()
		}

		// read batch

		msg = <-batch_channel // wait for batch

		switch msg.M_type {
		case CHMSG_BATCH_READY:
			batch_text = msg.M_batch_text

		case CHMSG_RSQL_ERR_OCCURRED: // batch too large
			rsql_err = msg.M_rsql_err
			rsql_err.Severity_id = rsql.ERROR_SESSION_ABORT
			goto SEND_RSQL_ERR

		case CHMSG_FATAL_COMMUNICATION_FAILURE:
			goto FATAL_COMMUNICATION_FAILURE

		case CHMSG_NO_MORE_BATCH:
			return

		default:
			panic("impossible")
		}

		// init lock manager's Registration object

		if context.Ctx_trancount == 0 {
			assert(registration == nil)
			registration = lk.New_registration(worker.Wk_id)
		}

		// init Parser/Decorator

		if decor, rsql_err = deco.New_decorator(login_name, login_id, login_default_language, current_database, current_options, registration); rsql_err != nil {
			goto RSQL_ERROR_OCCURRED
		}

		if msg_auth.opt_no_cf { // disable Constant-Folding during compilation of the batch
			if login_id != dict.SA_LOGIN_ID { // allowed only for 'sa'
				rsql_err = rsql.New_Error(rsql.ERROR_PERMISSION, rsql.ERROR_NO_CF_ONLY_FOR_SA, rsql.ERROR_BATCH_ABORT)
				goto RSQL_ERROR_OCCURRED
			}

			decor.Set_disable_constant_folding_flag(msg_auth.opt_no_cf)
		}

		if rsql_err = decor.Attach_batch(batch_text); rsql_err != nil {
			goto RSQL_ERROR_OCCURRED
		}

		// parse

		decor.VARIABLES.Update_current_timestamp()

		if rsql_err = decor.Parse_all_AST(); rsql_err != nil { // registration object if filled in during this parsing stage
			goto RSQL_ERROR_OCCURRED
		}
		//println("-------- Compatibility ---------", decor.Compatibilities.String())

		// lock resources

		if rsql_err = lk.G_LOCK_MANAGER.Acquire(registration); rsql_err != nil { // databases and tables are locked, or timeout occurs
			goto RSQL_ERROR_OCCURRED
		}

		if rsql.G_DEBUG_FLAG {
			registration.Debug_print()
		}

		// decorate

		if rsql_err = decor.Wacf_all_AST(decor.Batch_ast_anchor); rsql_err != nil { // decoration stage
			goto RSQL_ERROR_OCCURRED
		}

		// generate instructions

		emit.Instr_walk_all_AST(&decor.LABELS, decor.Batch_ast_anchor, decor.Basicblock_first) // list of instructions, ready to be executed by VM

		// prepare Wcache and Context, needed by VM during execution

		process_next_batch_on_error = true

		//wcache = cache.New_wcache(journal, cache.WCACHE_ELEMENT_ARRAY_SIZE_FIRST_CHUNK, 5000000, cache.WCACHE_PPAGE_ARRAY_SIZE_FIRST_CHUNK, 50) // for test: 10,200,10,50      1000, 5000000, 10, 640

		if context.Ctx_trancount == 0 {
			assert(wcache == nil)
			wcache = cache.New_wcache(journal, cache.WCACHE_ELEMENT_ARRAY_SIZE_FIRST_CHUNK, param_element_array_size_total_max_soft_limit, cache.WCACHE_PPAGE_ARRAY_SIZE_FIRST_CHUNK, param_ppage_array_size_total_max_limit)

			context.Ctx_wcache = wcache
		}

		// print AST if requested

		if msg_auth.opt_showtree {
			if rsql_err = ast.Debug_print_AST(context, decor.Batch_ast_anchor); rsql_err != nil {
				goto RSQL_ERROR_OCCURRED
			}
		}

		// check databases status (DB_STATUS_OFFLINE), mode (DB_MODE_READ_ONLY), and access (DB_ACCESS_RESTRICTED_USER)

		if rsql_err = dict.MASTER.Check_all_databases_status_mode_access(context, registration); rsql_err != nil {
			goto RSQL_ERROR_OCCURRED
		}

		// check permissions on objects

		if rsql_err = dict.MASTER.Check_objects_permissions(context, registration); rsql_err != nil {
			goto RSQL_ERROR_OCCURRED
		}

		// VM executes instructions

		if msg_auth.opt_no_exec == true || decor.Options&(ast.OPTION_PARSEONLY|ast.OPTION_NOEXEC) != 0 {
			goto LABEL_SKIP_EXEC
		}

		if rsql_err = wcache.Create_flashtables(decor.Temptable_bag); rsql_err != nil { // map of all temporary tables
			goto RSQL_ERROR_OCCURRED
		}

		retval, rsql_err = vm.Execute_basicblocks_all(context, decor.Basicblock_first)
		if rsql_err != nil { // error occurred during execution (arithmetic error, duplicate key insertion in table, etc)
			goto RSQL_ERROR_OCCURRED
		}

	LABEL_SKIP_EXEC:
		current_database = decor.VARIABLES.Get_current_db_name() // get current database
		current_options = decor.Options                          // get current options

		//====== normal batch ending =======

		// release locks, wcache

		switch context.Ctx_trancount {
		case 0: // everything has already been committed
			wcache.Filebag().Close_all()
			wcache.Close_flashjournal()
			wcache = nil
			context.Ctx_wcache = nil
			context.Clear_for_new_batch()
			cache.Reset_journal_for_resuse_in_same_session(journal) // in fact, not needed because COMMIT has already reset the journal

			lk.G_LOCK_MANAGER.Release(registration)
			registration = nil

		default: // transaction is pending. wcache, journal and registration must be used in the current state by the next batch.
			wcache.Remove_all_temporary_tables_elements(decor.Temptable_bag) // temporary tables are removed from wcache
			wcache.Close_flashjournal()                                      // journal for temporary tables is closed
		}

		// tech note: the server can successfully send a packet to a client process that doesn't exist any more, because client OS crashed, or network cable was unplugged.
		//            It is a half-open connection, but the server socker can only detect it when packets are sent.
		//            The server can only detect that the client socket not on the first, but on the second packet sending.
		//            That's why the server can think that the batch has successfully terminated because the sending of the rc code doesn't return an error.

		if err := write_batch_RC_and_flush(writer, retval); err != nil { // can succeed even if client socket is down
			goto FATAL_COMMUNICATION_FAILURE
		}

		continue // continue with next batch

		//====== RSQL_ERROR_OCCURRED =======
	RSQL_ERROR_OCCURRED:

		rsql.Assert(rsql_err != nil)

		// get current options and current database

		if decor != nil {
			current_options = decor.Options // get current options

			if decor.VARIABLES.Tbl != nil {
				current_database = decor.VARIABLES.Get_current_db_name() // get current database. May be "" if error occurred during VARIABLES map initialization
			}
		}

		// release wcache and context. When an error occurs, wcache may be in a corrupted state (DRAFT pages may exist), and so must be discarded.

		if wcache != nil { // wcache is nil if error occurred when wcache has not yet been created, in particular for syntax error during parsing
			wcache.Filebag().Close_all()
			wcache.Close_flashjournal()
			wcache = nil
			context.Ctx_wcache = nil
		}

		if journal != nil {
			cache.Reset_journal_for_resuse_in_same_session(journal)
		}

		context.Clear_for_new_batch()

		if registration != nil {
			lk.G_LOCK_MANAGER.Release(registration) // can be called even if Acquire has not been called on Registration object
			registration = nil
		}

	SEND_RSQL_ERR:

		// send error message to client

		switch {
		case process_next_batch_on_error == false:
			rsql_err.State = 127 // State 127 demands that the client stops sending subsequent batches

		case rsql_err.Severity_id == rsql.ERROR_SESSION_ABORT:
			rsql_err.State = 127 // State 127 demands that the client stops sending subsequent batches

		case rsql_err.Severity_id == rsql.ERROR_SERVER_ABORT:
			// pass
		}

		if err := write_rsql_err(writer, rsql_err); err != nil {
			goto FATAL_COMMUNICATION_FAILURE
		}

		// send return code to the client

		if retval == 0 { // never send 0 to client if error occurred.
			retval = -1 // send -1 instead
		}

		if err := write_batch_RC_and_flush(writer, retval); err != nil {
			goto FATAL_COMMUNICATION_FAILURE
		}

		if rsql_err.Severity_id == rsql.ERROR_SERVER_ABORT {
			log.Println("SERVER ABORT") // server terminates abruptly
			os.Exit(1)
		}

		if rsql_err.State == 127 { // if parse error, rsql.ERROR_SESSION_ABORT, or error raised by THROW with state=127, don't process next batch
			return
		}

		continue // continue with next batch
	}

	//====== FATAL_COMMUNICATION_FAILURE =======
FATAL_COMMUNICATION_FAILURE:

	log.Println("server: FATAL_COMMUNICATION_FAILURE, connection closed.")

	return
}

type Ch_message struct {
	M_type       Ch_msg_type_t
	M_batch_text []byte
	M_rsql_err   *rsql.Error
}

type Ch_msg_type_t uint8

const (
	CHMSG_BATCH_READY Ch_msg_type_t = 1 << iota
	CHMSG_RSQL_ERR_OCCURRED
	CHMSG_FATAL_COMMUNICATION_FAILURE
	CHMSG_NO_MORE_BATCH
)

// batch_reader listen to the connection until the connection is closed, or read timeout occurs (that is, no keepalive message has been received).
//
func batch_reader(context *rsql.Context, c chan<- Ch_message, done <-chan bool, reader *msgp.Reader) {
	var (
		err        error
		msg        Ch_msg_type_t
		batch_text []byte
		rsql_err   *rsql.Error
		ok         bool
		reqtyp     rsql.Request_t
		conn       net.Conn
	)

	if rsql.G_DEBUG_FLAG {
		defer log.Println("DEBUG: BATCH READER TERMINATED")
	}

	conn = context.Ctx_conn

	for {
		err = nil
		msg = 0
		batch_text = nil
		rsql_err = nil
		ok = false
		reqtyp = 0

		_ = conn.SetReadDeadline(time.Now().Add(time.Duration(rsql.Param_read_timeout) * time.Second)) // fails if connection is closed, but we don't care

		if reqtyp, err = read_request_type(reader); err != nil {
			if err == io.EOF { // connection closed after last batch teminates. The connection could also have been closed if client crashed.
				if rsql.G_DEBUG_FLAG {
					log.Println("DEBUG: EOF, client closed the connection")
				}

				atomic.CompareAndSwapInt32(&context.Ctx_cancel, 0, rsql.CTX_CANCEL_BY_EOF)
				msg = CHMSG_NO_MORE_BATCH
				goto SEND_MSG
			}

			if operr, ok := err.(*net.OpError); ok == true && operr.Timeout() == true {
				atomic.CompareAndSwapInt32(&context.Ctx_cancel, 0, rsql.CTX_CANCEL_BY_TIMEOUT)
			} else {
				atomic.CompareAndSwapInt32(&context.Ctx_cancel, 0, rsql.CTX_CANCEL_BY_COMMUNICATION_FAILURE)
			}

			log.Printf("server: read_request_type() failed: %s", err)
			msg = CHMSG_FATAL_COMMUNICATION_FAILURE // timeout is a fatal communication error, because it means the client is not alive
			goto SEND_MSG
		}

		switch reqtyp {
		case rsql.REQTYP_KEEPALIVE:
			continue

		case rsql.REQTYP_BATCH:
			_ = conn.SetReadDeadline(time.Now().Add(time.Duration(rsql.Param_read_timeout) * time.Second))

			if batch_text, err = read_batch_text(reader); err != nil {
				rsql_err, ok = err.(*rsql.Error)
				if ok { // must be rsql.ERROR_BATCH_TOO_LARGE
					msg = CHMSG_RSQL_ERR_OCCURRED // we must abort the session, because the current batch text is still in the connection and we cannot proceed further
					goto SEND_MSG
				}

				log.Printf("server: read_batch_text() failed: %s", err)
				atomic.CompareAndSwapInt32(&context.Ctx_cancel, 0, rsql.CTX_CANCEL_BY_COMMUNICATION_FAILURE)
				msg = CHMSG_FATAL_COMMUNICATION_FAILURE
				goto SEND_MSG
			}

			msg = CHMSG_BATCH_READY

		case rsql.REQTYP_CANCEL:
			atomic.CompareAndSwapInt32(&context.Ctx_cancel, 0, rsql.CTX_CANCEL_BY_COMMUNICATION_FAILURE)
			msg = CHMSG_FATAL_COMMUNICATION_FAILURE

		default:
			log.Printf("server: REQTYP_BATCH expected, got %s", reqtyp)
			atomic.CompareAndSwapInt32(&context.Ctx_cancel, 0, rsql.CTX_CANCEL_BY_COMMUNICATION_FAILURE)
			msg = CHMSG_FATAL_COMMUNICATION_FAILURE
		}

	SEND_MSG:
		select {
		case c <- Ch_message{msg, batch_text, rsql_err}: // if CHMSG_BATCH_READY, always continue to loop, because we need to eat keepalive messages
			if msg&(CHMSG_FATAL_COMMUNICATION_FAILURE|CHMSG_NO_MORE_BATCH|CHMSG_RSQL_ERR_OCCURRED) != 0 { // exit only when no more batch is expected
				return
			}

		case <-done: // when the handle_connection goroutine returns, it always closes this channel, which unblocks it
			return
		}
	}

}
