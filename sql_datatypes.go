package rsql

import (
	"fmt"
)

// IDataslot is the interface that can receive all datatypes, e.g. data.VARCHAR, data.INT, data.NUMERIC, data.DATETIME, etc.
//
type IDataslot interface {
	Datatype() Datatype_t
	Kind() Kind_t
	Set_kind(Kind_t)
	Error() *Error
	Set_Error(*Error)
	NULL_flag() bool
	Set_to_NULL()
	RO_LEAF_hashval() uint32
	String() string
}

type AnyINT interface {
	IDataslot

	Data_val() int64
}

// Kind_t is the kind of a dataslot in the AST tree.
// Dataslots may contain constant value, variable value, column value, or temporary value (result of an operator or function computation).
//
type Kind_t uint16

func (kind Kind_t) String() string {

	var s string

	switch kind {
	case KIND_RO_LEAF:
		s = "KIND_RO_LEAF"
	case KIND_VAR_LEAF:
		s = "KIND_VAR_LEAF"
	case KIND_COL_LEAF:
		s = "KIND_COL_LEAF"
	case KIND_TMP:
		s = "KIND_TMP"
	default:
		panic("unknown Kind_t")
	}

	return s
}

// Type of the nodes in the AST tree.
// Power of two.
const (
	KIND_RO_LEAF  Kind_t = 1 // readonly constant. It is a literal number or string. Or after constfolding, token of any type that contains an immutable value.
	KIND_VAR_LEAF Kind_t = 2 // variable
	KIND_COL_LEAF Kind_t = 4 // column of table
	KIND_TMP      Kind_t = 8 // result of operator or function
)

type Datatype_t uint16

// for numbers, precision is the number of significant digits (but '-' sign is not included)
const (
	DATATYPE_PRECISION_BIT      = 1
	DATATYPE_PRECISION_TINYINT  = 3
	DATATYPE_PRECISION_SMALLINT = 5
	DATATYPE_PRECISION_INT      = 10
	DATATYPE_PRECISION_BIGINT   = 19
	DATATYPE_PRECISION_MONEY    = 19 // because in SQL Server, money is stored as bigint
	DATATYPE_SCALE_MONEY        = 4

	DATATYPE_NUMERIC_PRECISION_MAX           = 34                                                                   // must be equal to C.DECQUAD_Pmax (==34).  A check is done in quad.init(), and panics if value mismatch.
	DATATYPE_NUMERIC_RESULT_SCALE_MAX        = 6                                                                    // max scale of numeric result, resulting from an operator or a function.
	DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX = (DATATYPE_NUMERIC_PRECISION_MAX - DATATYPE_NUMERIC_RESULT_SCALE_MAX) // max integral part size of numeric result, resulting from an operator or a function.

	DATATYPE_VARBINARY_PRECISION_MAX = 8000 // max precision for binary and varbinary, in bytes.
	DATATYPE_VARCHAR_PRECISION_MAX   = 8000 // max precision for char and varchar
	DATATYPE_NVARCHAR_PRECISION_MAX  = 4000 // max precision for nchar and nvarchar
)

// 'boolean' datatype does not exist in MS SQL Server, nor 'void' datatype for NULL literal. They are used internally.
// IMPORTANT: The order of these datatypes is important AND SHOULD NOT BE CHANGED, as these values are used when decorating operators to find the "winning" datatype among multiple operands.
//     E.g.    10 + 12.3        When an "int" and a "numeric" are added, the "int" operand is cast to a "numeric" datatype, and the "+" operator will add two "numeric" values.
const (
	DATATYPE_ILLEGAL Datatype_t = iota // illegal datatype of value 0, never used

	DATATYPE_VOID // _void. Used for NULL literal.

	DATATYPE_SYSCOLLATOR // internal datatype, containing information for collation
	DATATYPE_SYSLANGUAGE // internal datatype, containing information for date and number formatting

	DATATYPE_BOOLEAN // _bool. Internal datatype, used for boolean operations.

	DATATYPE_BINARY    // BINARY. Not implemented.
	DATATYPE_VARBINARY // VARBINARY
	DATATYPE_IMAGE     // IMAGE. Not implemented.

	DATATYPE_CHAR    // CHAR. The decorator module will convert CHAR to VARCHAR with fixlen_flag = true.
	DATATYPE_VARCHAR // VARCHAR with fixlen_flag = false.
	DATATYPE_TEXT    // TEXT. Not implemented.

	DATATYPE_NCHAR    // NCHAR. Always converted to CHAR.
	DATATYPE_NVARCHAR // NVARCHAR. Always converted to VARCHAR.
	DATATYPE_NTEXT    // NTEXT. Always converted to TEXT.

	DATATYPE_REGEXPLIKE // internal datatype, containing regular expression. Used by LIKE operator.

	DATATYPE_BIT      // BIT
	DATATYPE_TINYINT  // TINYINT
	DATATYPE_SMALLINT // SMALLINT
	DATATYPE_INT      // INT
	DATATYPE_BIGINT   // BIGINT

	DATATYPE_SMALLMONEY // SMALLMONEY. Always converted to MONEY.
	DATATYPE_MONEY      // MONEY

	DATATYPE_DECIMAL // DECIMAL. Always convered to NUMERIC.
	DATATYPE_NUMERIC // NUMERIC

	DATATYPE_REAL  // REAL. Always converted to FLOAT.
	DATATYPE_FLOAT // FLOAT

	DATATYPE_DATE          // DATE
	DATATYPE_TIME          // TIME
	DATATYPE_SMALLDATETIME // SMALLDATETIME. Always converted to DATETIME.
	DATATYPE_DATETIME      // DATETIME
	DATATYPE_DATETIME2     // DATETIME2. Always converted to DATETIME.

	// DATATYPE_ROWVERSION       rowversion

	// synonyms
	// (note: NATIONAL, VARYING and PRECISION are keywords in the keyword list)

	DATATYPE_CHARACTER // same as CHAR
	DATATYPE_INTEGER   // same as INT
	DATATYPE_DEC       // same as DECIMAL
	DATATYPE_DOUBLE    // same as FLOAT. Must be followed by PRECISION.

	DATATYPE_ARRAYSIZE uint = uint(DATATYPE_DATETIME) + 1 // number of datatypes, used for G_SQL_DATATYPE_CAST_INSTRUCTION_CODE[][] array dimensions. DATETIME is the highest used datatype.
)

// Is_allowed_select_column returns true if datatype is allowed in SELECT column.
//
func (datatype Datatype_t) Is_allowed_select_column() bool {

	if datatype == DATATYPE_VOID ||
		datatype == DATATYPE_VARBINARY ||
		datatype == DATATYPE_VARCHAR ||
		datatype == DATATYPE_BIT ||
		datatype == DATATYPE_TINYINT ||
		datatype == DATATYPE_SMALLINT ||
		datatype == DATATYPE_INT ||
		datatype == DATATYPE_BIGINT ||
		datatype == DATATYPE_MONEY ||
		datatype == DATATYPE_NUMERIC ||
		datatype == DATATYPE_FLOAT ||
		datatype == DATATYPE_DATE ||
		datatype == DATATYPE_TIME ||
		datatype == DATATYPE_DATETIME {
		return true
	}

	return false
}

// G_DATATYPES map contains datatypes, that can be used in DECLARE statement.
//
//      This means that internal datatypes such as DATATYPE_VOID or DATATYPE_BOOLEAN should not be put in this map.
//
var G_DATATYPES = map[string]Datatype_t{
	"binary":    DATATYPE_BINARY,
	"varbinary": DATATYPE_VARBINARY,
	"image":     DATATYPE_IMAGE,

	"char":    DATATYPE_CHAR,
	"varchar": DATATYPE_VARCHAR,
	"text":    DATATYPE_TEXT,

	"nchar":    DATATYPE_NCHAR,
	"nvarchar": DATATYPE_NVARCHAR,
	"ntext":    DATATYPE_NTEXT,

	"bit":      DATATYPE_BIT,
	"tinyint":  DATATYPE_TINYINT,
	"smallint": DATATYPE_SMALLINT,
	"int":      DATATYPE_INT,
	"bigint":   DATATYPE_BIGINT,

	"smallmoney": DATATYPE_SMALLMONEY,
	"money":      DATATYPE_MONEY,

	"decimal": DATATYPE_DECIMAL,
	"numeric": DATATYPE_NUMERIC,

	"real":  DATATYPE_REAL,
	"float": DATATYPE_FLOAT, // C float

	"date":          DATATYPE_DATE,
	"time":          DATATYPE_TIME,
	"smalldatetime": DATATYPE_SMALLDATETIME,
	"datetime":      DATATYPE_DATETIME,
	"datetime2":     DATATYPE_DATETIME2,

	"character": DATATYPE_CHAR,    // synonym for "char"
	"integer":   DATATYPE_INT,     // synonym for "int"
	"dec":       DATATYPE_DECIMAL, // synonym for "decimal"

	"double": DATATYPE_DOUBLE, // for DOUBLE PRECISION
}

// String returns the datatype name.
// It is used to display datatype name when ERROR_INVALID_SYNTAX_CAST_NOT_ALLOWED occurs.
//
func (sql_datatype Datatype_t) String() string {
	var s string

	switch sql_datatype {

	case DATATYPE_VOID:
		s = "VOID"

	case DATATYPE_SYSCOLLATOR:
		s = "SYSCOLLATOR"
	case DATATYPE_SYSLANGUAGE:
		s = "SYSLANGUAGE"

	case DATATYPE_BOOLEAN:
		s = "BOOLEAN"

	case DATATYPE_VARBINARY:
		s = "VARBINARY"

	case DATATYPE_VARCHAR:
		s = "VARCHAR"

	case DATATYPE_REGEXPLIKE:
		s = "REGEXPLIKE"

	case DATATYPE_BIT:
		s = "BIT"
	case DATATYPE_TINYINT:
		s = "TINYINT"
	case DATATYPE_SMALLINT:
		s = "SMALLINT"
	case DATATYPE_INT:
		s = "INT"
	case DATATYPE_BIGINT:
		s = "BIGINT"

	case DATATYPE_MONEY:
		s = "MONEY"
	case DATATYPE_NUMERIC:
		s = "NUMERIC"

	case DATATYPE_FLOAT: // C double
		s = "FLOAT"

	case DATATYPE_DATE:
		s = "DATE"
	case DATATYPE_TIME:
		s = "TIME"
	case DATATYPE_DATETIME:
		s = "DATETIME"

	default:
		panic("datatype not listed")
	}

	return s
}

// Datatype_shortdef returns the datatype string, with precision and scale if needed.
// sql_datatype can be DATATYPE_CHAR (in this case, fixlen_flag is ignored).
//
func Datatype_shortdef(datatype Datatype_t, precision uint16, scale uint16, fixlen_flag bool) string {
	var (
		datatype_display string
	)

	if !(datatype == DATATYPE_VARBINARY || datatype == DATATYPE_CHAR || datatype == DATATYPE_VARCHAR || datatype == DATATYPE_NUMERIC) {
		if !(precision == 0 && scale == 0) {
			panic("impossible")
		}
	}

	switch datatype {
	case DATATYPE_VOID:
		datatype_display = "VOID"

	case DATATYPE_SYSCOLLATOR:
		datatype_display = "SYSCOLLATOR"

	case DATATYPE_SYSLANGUAGE:
		datatype_display = "SYSLANGUAGE"

	case DATATYPE_BOOLEAN:
		datatype_display = "BOOLEAN"

	case DATATYPE_VARBINARY:
		datatype_display = fmt.Sprintf("VARBINARY(%d)", precision)

	case DATATYPE_IMAGE:
		datatype_display = "IMAGE"

	case DATATYPE_CHAR:
		Assert(scale == 0)
		datatype_display = fmt.Sprintf("CHAR(%d)", precision)

	case DATATYPE_VARCHAR:
		//sql_precision may be 0 for empty string
		Assert(scale == 0)

		if fixlen_flag == false {
			datatype_display = fmt.Sprintf("VARCHAR(%d)", precision)
		} else {
			datatype_display = fmt.Sprintf("CHAR(%d)", precision)
		}

	case DATATYPE_TEXT:
		datatype_display = "TEXT"

	case DATATYPE_REGEXPLIKE:
		datatype_display = "REGEXPLIKE"

	case DATATYPE_BIT:
		datatype_display = "BIT"

	case DATATYPE_TINYINT:
		datatype_display = "TINYINT"

	case DATATYPE_SMALLINT:
		datatype_display = "SMALLINT"

	case DATATYPE_INT:
		datatype_display = "INT"

	case DATATYPE_BIGINT:
		datatype_display = "BIGINT"

	case DATATYPE_MONEY:
		datatype_display = "MONEY"

	case DATATYPE_NUMERIC:
		Assert(precision >= 1)
		Assert(scale <= precision)
		datatype_display = fmt.Sprintf("NUMERIC(%d, %d)", precision, scale)

	case DATATYPE_FLOAT:
		datatype_display = "FLOAT"

	case DATATYPE_DATE:
		datatype_display = "DATE"

	case DATATYPE_TIME:
		datatype_display = "TIME"

	case DATATYPE_DATETIME:
		datatype_display = "DATETIME"

	default:
		panic("datatype unknown")
	}

	return datatype_display
}
