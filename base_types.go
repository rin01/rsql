package rsql

import (
	"net"

	"rsql/msgp"
)

var G_server_shutdown_flag uint32 // if >0, normal shutdown, waiting for sessions to finish their work

type Object_qname_t struct {
	Database_name        string
	Schema_name          string
	Object_name          string
	Object_name_original string
}

func (objqname Object_qname_t) String() string {

	if objqname.Database_name == "" {
		if objqname.Schema_name == "" {
			return objqname.Object_name
		}

		return objqname.Schema_name + "." + objqname.Object_name
	}

	return objqname.Database_name + "." + objqname.Schema_name + "." + objqname.Object_name
}

func (objqname Object_qname_t) Is_variable_table() bool {

	if objqname.Database_name != TRASHDB || objqname.Schema_name != "dbo" {
		return false
	}

	if objqname.Object_name[0] == '@' {
		return true
	}

	return false
}

type Permission_t uint16

func (perm Permission_t) String() string {
	var (
		res string
	)

	if perm&PERMISSION_SELECT != 0 {
		res += "S"
	}
	if perm&PERMISSION_UPDATE != 0 {
		res += "U"
	}
	if perm&PERMISSION_INSERT != 0 {
		res += "I"
	}
	if perm&PERMISSION_DELETE != 0 {
		res += "D"
	}
	if perm&PERMISSION_MODIFY_DATABASE != 0 {
		res += "Mdb"
	}
	if perm&PERMISSION_MODIFY_PRINCIPAL != 0 {
		res += "Mpr"
	}
	if perm&PERMISSION_MODIFY_OBJECT != 0 {
		res += "Mobj"
	}
	if perm&PERMISSION_MODIFY_PERMISSIONS != 0 {
		res += "Mperm"
	}

	return res
}

func (perm Permission_t) String_long() string {
	var (
		res string
	)

	if perm == 0 {
		return ""
	}

	if perm&PERMISSION_SELECT != 0 {
		res += "SELECT, "
	}
	if perm&PERMISSION_UPDATE != 0 {
		res += "UPDATE, "
	}
	if perm&PERMISSION_INSERT != 0 {
		res += "INSERT, "
	}
	if perm&PERMISSION_DELETE != 0 {
		res += "DELETE, "
	}

	if perm&PERMISSION_MODIFY_DATABASE != 0 {
		res += "MODIFY_DATABASE, "
	}
	if perm&PERMISSION_MODIFY_PRINCIPAL != 0 {
		res += "MODIFY_PRINCIPAL, "
	}
	if perm&PERMISSION_MODIFY_OBJECT != 0 {
		res += "MODIFY_OBJECT, "
	}
	if perm&PERMISSION_MODIFY_PERMISSIONS != 0 {
		res += "MODIFY_PERMISSIONS, "
	}

	if len(res) != 0 {
		res = res[:len(res)-2]
	}

	return res
}

const (
	PERMISSION_SELECT Permission_t = 1 << iota
	PERMISSION_UPDATE
	PERMISSION_INSERT
	PERMISSION_DELETE

	PERMISSION_MODIFY_DATABASE    // this database permission is used to implement database READ_ONLY mode
	PERMISSION_MODIFY_PRINCIPAL   // this database permission is used to implement database READ_ONLY mode
	PERMISSION_MODIFY_OBJECT      // this database permission is used to implement database READ_ONLY mode
	PERMISSION_MODIFY_PERMISSIONS // this database permission is used to implement database READ_ONLY mode
)

const TABLE_PERM_SUID_MASK Permission_t = PERMISSION_SELECT | PERMISSION_UPDATE | PERMISSION_INSERT | PERMISSION_DELETE

type IWcache interface {
	Set_last_rowcount(int64)
}

// Context allows the session to pass the output stream, error stream, and credential, to the VM and instructions.
//
type Context struct {
	Ctx_login_id            int64  // immutable. During the execution of a batch, the real login can be dropped by the batch itself, or by another batch being executed concurrently.
	Ctx_login_name_for_info string // immutable

	Ctx_worker_id uint32
	Ctx_conn      net.Conn
	Ctx_mw        *msgp.Writer
	Ctx_cancel    int32 // if client closes the connection, or timeout, or communication failure, this field indicates that the session must terminate.

	Ctx_scratch   []byte  // just a scratch buffer used for transient usage by some functions
	Ctx_wcache    IWcache // private write-cache. Stored type is *cache.Wcache. It is stored in an interface to avoid circular import error.
	Ctx_trancount int     // BEGIN TRANSACTION increments this counter. When COMMIT is encountered, if > 1: decrement counter; if 1: make changes permanent and decrement counter; else, raise error.
	Ctx_nocount   bool    // SET NOCOUNT sets this flag. Specifies whether VM sends rowcount info to client.
}

// New_context creates a new Context, to transmit important information to the VM.
//
func New_context(login_id int64, login_name_for_info string) *Context {
	var (
		context *Context
	)

	context = &Context{}

	context.Ctx_login_id = login_id
	context.Ctx_login_name_for_info = login_name_for_info

	context.Ctx_trancount = 0 // 0 is autocommit mode

	return context
}

func (context *Context) Clear_for_new_batch() {

	context.Ctx_scratch = nil
	context.Ctx_wcache = nil
	context.Ctx_trancount = 0 // 0 is autocommit mode
	context.Ctx_nocount = false
}

func (context *Context) Set_last_rowcount(val int64) {

	context.Ctx_wcache.Set_last_rowcount(val)
}

func Assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

func Min_int(a int, b int) int {

	if a <= b {
		return a
	}

	return b
}

func Max_int(a int, b int) int {

	if a >= b {
		return a
	}

	return b
}

func Truncate_string(a string, n int) string {

	if len(a) <= n {
		return a
	}

	return a[:n]
}

const ELLIPSIS_TRUNCATION_LIMIT_DEFAULT = 50 // used for Truncate_string_with_ellipsis() and Truncate_bytes_with_ellipsis()

// Truncate_string_with_ellipsis returns the argument, truncated at n bytes with ellipsis appended.
// You can use ELLIPSIS_TRUNCATION_LIMIT_DEFAULT for n.
//
func Truncate_string_with_ellipsis(a string, n int) string {

	if len(a) <= n {
		return a
	}

	return a[:n] + "..."
}

func Truncate_bytes(a []byte, n int) []byte {

	if len(a) <= n {
		return a
	}

	return a[:n]
}

// Truncate_bytes_with_ellipsis returns the argument, truncated at n bytes with ellipsis appended.
// You can use ELLIPSIS_TRUNCATION_LIMIT_DEFAULT for n.
//
func Truncate_bytes_with_ellipsis(a []byte, n int) []byte {

	if len(a) <= n {
		return a
	}

	var buff = make([]byte, n+3) // + 3 for "..."

	copy(buff[:n], a)
	copy(buff[n:], "...")

	return buff
}
