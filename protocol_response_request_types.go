package rsql

import (
	"fmt"
	"log"
	"sync/atomic"
)

var Param_read_timeout int64 // in seconds

const (
	CTX_CANCEL_BY_EOF = iota + 1
	CTX_CANCEL_BY_TIMEOUT
	CTX_CANCEL_BY_COMMUNICATION_FAILURE
)

//**** ATTENTION: Response_t and Request_t constants are duplicated in the client package "rsql/rsqlib" ****

// message types sent from server to client
//
type Response_t uint8

const (
	RESTYP_LOGIN_FAILED  Response_t = 0
	RESTYP_LOGIN_SUCCESS Response_t = 1

	RESTYP_RECORD_LAYOUT   Response_t = 3
	RESTYP_RECORD          Response_t = 4
	RESTYP_RECORD_FINISHED Response_t = 5 // record count is sent to client

	RESTYP_EXECUTION_FINISHED Response_t = 7  // only statements that affect records, like INSERT, UPDATE, DELETE, send this response, along with number of rows affected. For the moment, other statements succeed silently without sending any response (perhaps it will change, I don't know. In this case, a value or -1 will mean that information about affected row count is not available).
	RESTYP_PRINT              Response_t = 10 // list of values, from PRINT statement

	RESTYP_MESSAGE Response_t = 11 // single string
	RESTYP_ERROR   Response_t = 12 // rsql.Error content

	RESTYP_BATCH_END Response_t = 14 // the return code is sent to client
)

func (resp Response_t) String() string {

	switch resp {
	case RESTYP_LOGIN_FAILED:
		return "RESTYP_LOGIN_FAILED"
	case RESTYP_LOGIN_SUCCESS:
		return "RESTYP_LOGIN_SUCCESS:"

	case RESTYP_RECORD_LAYOUT:
		return "RESTYP_RECORD_LAYOUT"
	case RESTYP_RECORD:
		return "RESTYP_RECORD"
	case RESTYP_RECORD_FINISHED:
		return "RESTYP_RECORD_FINISHED"

	case RESTYP_EXECUTION_FINISHED:
		return "RESTYP_EXECUTION_FINISHED"

	case RESTYP_PRINT:
		return "RESTYP_PRINT"
	case RESTYP_MESSAGE:
		return "RESTYP_MESSAGE"
	case RESTYP_ERROR:
		return "RESTYP_ERROR"

	case RESTYP_BATCH_END:
		return "RESTYP_BATCH_END"

	default:
		return fmt.Sprintf("RESTYP unknown(%d)", resp)
	}
}

// message types sent from client to server
//
type Request_t uint8

const (
	REQTYP_AUTH      Request_t = 20
	REQTYP_BATCH     Request_t = 21
	REQTYP_KEEPALIVE Request_t = 30
	REQTYP_CANCEL    Request_t = 100
)

func (req Request_t) String() string {

	switch req {
	case REQTYP_AUTH:
		return "REQTYP_AUTH"
	case REQTYP_BATCH:
		return "REQTYP_BATCH"
	case REQTYP_KEEPALIVE:
		return "REQTYP_KEEPALIVE"
	case REQTYP_CANCEL:
		return "REQTYP_CANCEL"
	default:
		return fmt.Sprintf("REQTYP unknown(%d)", req)
	}
}

// Writer_error returns the error state of the messagepack Writer.
//
func (context *Context) Writer_error() *Error {

	if err := context.Ctx_mw.Error(); err != nil {
		return New_Error(ERROR_GENERAL, ERROR_CONNECTION_FAILED, ERROR_SESSION_ABORT)
	}

	return nil
}

// Writer_flush flushes the Writer context.Ctx_mw into the connection.
//
func (context *Context) Writer_flush() *Error {

	if err := context.Ctx_mw.Flush(); err != nil {
		return New_Error(ERROR_GENERAL, ERROR_CONNECTION_FAILED, ERROR_SESSION_ABORT)
	}

	return nil
}

// Send_warning sends a string as RESTYP_MESSAGE.
//
// Argument rsql_err must have field Severity_id == ERROR_IS_WARNING.
//
func (context *Context) Send_warning(rsql_err *Error) *Error {
	var (
		message_text string
		s            string
	)

	Assert(rsql_err.Severity_id == ERROR_IS_WARNING)

	// write response type

	context.Ctx_mw.WriteUint8(uint8(RESTYP_MESSAGE))

	// write warning string

	message_text = fmt.Sprintf(rsql_err.Message_fmt_string, rsql_err.Message_args...)

	s = fmt.Sprintf("WARNING %d:%d: %s", rsql_err.Batch_line_no, rsql_err.Batch_line_pos, message_text)

	context.Ctx_mw.WriteString(s)

	// flush data to client

	if rsql_err_2 := context.Writer_flush(); rsql_err_2 != nil {
		return rsql_err_2
	}

	return nil
}

// Send_MESSAGE sends a string as RESTYP_MESSAGE, but doesn't flush.
// You must call context.Writer_flush() yourself.
//
func (context *Context) Send_MESSAGE_no_flush(format string, a ...interface{}) *Error {
	var (
		rsql_err *Error
		s        string
	)

	// write response type

	context.Ctx_mw.WriteUint8(uint8(RESTYP_MESSAGE))

	// write string

	s = fmt.Sprintf(format, a...)

	context.Ctx_mw.WriteString(s)

	// check error

	if rsql_err = context.Writer_error(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

// Send_MESSAGE sends a string as RESTYP_MESSAGE.
//
func (context *Context) Send_MESSAGE(format string, a ...interface{}) *Error {
	var (
		rsql_err *Error
		s        string
	)

	// write response type

	context.Ctx_mw.WriteUint8(uint8(RESTYP_MESSAGE))

	// write string

	s = fmt.Sprintf(format, a...)

	context.Ctx_mw.WriteString(s)

	// flush message to client

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

// Send_MESSAGE_or_panic sends a string as RESTYP_MESSAGE.
// If error occurs because connection failed, panics.
//
//     **** WARNING ****
//     This function is used only in ast/debug_print_ast.go, because wrapping each call with "if err = ...; err != nil { return err }" would make the code less clear.
//     But "panic" must be trapped by "recover", in this case by ast.Debug_print_AST.
//
func (context *Context) Send_MESSAGE_or_panic(format string, a ...interface{}) {
	var (
		rsql_err *Error
		s        string
	)

	// write response type

	context.Ctx_mw.WriteUint8(uint8(RESTYP_MESSAGE))

	// write string

	s = fmt.Sprintf(format, a...)

	context.Ctx_mw.WriteString(s)

	// flush message to client

	if rsql_err = context.Writer_flush(); rsql_err != nil {
		panic(rsql_err)
	}
}

// Check_keepalive checks if keepalive signal has timed out.
//
func (context *Context) Check_keepalive() (rsql_err *Error) {

	if context.Ctx_conn == nil {
		return nil
	}

	// check cancel

	cancel := atomic.LoadInt32(&context.Ctx_cancel)

	if cancel == 0 {
		return
	}

	switch cancel {
	case CTX_CANCEL_BY_EOF: // client has closed prematurely the connection
		return New_Error(ERROR_GENERAL, ERROR_CANCEL_BY_EOF, ERROR_SESSION_ABORT)

	case CTX_CANCEL_BY_TIMEOUT:
		log.Printf("Keepalive timeout expired. Session for \"%s\" terminated.", context.Ctx_login_name_for_info)
		return New_Error(ERROR_GENERAL, ERROR_CANCEL_BY_TIMEOUT, ERROR_SESSION_ABORT, Param_read_timeout)

	case CTX_CANCEL_BY_COMMUNICATION_FAILURE:
		return New_Error(ERROR_GENERAL, ERROR_CANCEL_BY_COMMUNICATION_FAILURE, ERROR_SESSION_ABORT)

	default:
		panic("impossible")
	}

	return nil
}
