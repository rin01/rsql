package ast

import (
	"rsql"
	"rsql/lex"
	"rsql/lk"
)

// Eat_list_of_colnames_for_INSERT eats a list of column names, enclosed in parentheses.
// The list cannot be empty.
//
func (parser *Parser) Eat_list_of_colnames_for_INSERT() ([]string, *rsql.Error) {
	var (
		rsql_err    *rsql.Error
		col_list    []string
		colname     string
		colname_map map[string]struct{} // to check for colname duplicate
	)

	colname_map = make(map[string]struct{})

	// eat column list. Expect left parenthese

	if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil {
		return nil, rsql_err
	}

	// eat list of columns

	col_list = make([]string, 0, SPEC_INSERT_INTO_COL_LIST_DEFAULT_SIZE)

	for {
		if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // === expect column name
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		if len(col_list) >= rsql.SPEC_TABLE_NUMBER_OF_COLUMNS_MAX {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_COLUMNS_IN_LIST, rsql.ERROR_BATCH_ABORT)
		}

		if parser.Current_lexeme.Lex_word == rsql.ROWID {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ROWID_FORBIDDEN_IN_LIST, rsql.ERROR_BATCH_ABORT)
		}

		colname = parser.Current_lexeme.Lex_word // already in lowercase, as it is an identifier

		if _, ok := colname_map[colname]; ok == true { // check for duplicate column name in list
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_COLUMN_NAME_FOUND, rsql.ERROR_BATCH_ABORT, colname)
		}
		colname_map[colname] = struct{}{}

		col_list = append(col_list, colname)

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip column name
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA {
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}
	}

	// expect right parenthese

	if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil {
		return nil, rsql_err
	}

	return col_list, nil
}

// Eat_VALUES_list_of_expressions eats a sequence of VALUES expressions.
//
// If at least one expression (that is, at least one valid term) is not available, a syntax error is returned.
//
// Keyword DEFAULT is allowed. In this case, the pointer to the expression is set to nil.
//
func (parser *Parser) Eat_VALUES_list_of_expressions() ([]*Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		expr     *Token_primary
	)

	expr_list := make([]*Token_primary, 0, 8) // 8 is an arbitrary default capacity, which is good enough for most number of expression in list.

	//=== loop on each expression ===

	for {
		if len(expr_list) >= SPEC_INSERT_INTO_VALUES_EXPRESSIONS_COUNT_MAX {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_EXPRESSIONS_IN_VALUES, rsql.ERROR_BATCH_ABORT)
		}

		switch {
		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_DEFAULT:
			expr = nil

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip DEFAULT
				return nil, rsql_err
			}

		default:
			if expr, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat expression
				return nil, rsql_err
			}
		}

		expr_list = append(expr_list, expr) // append expression to list

		// eat comma

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more expression, exit loop.
			break
		}

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}
	}

	// here, at least one valid expression has been read.

	assert(len(expr_list) > 0)

	return expr_list, nil
}

// parse INSERT INTO statement.
//
//     INSERT [ INTO ] <object> [ ( column_list ) ]
//          {   { VALUES ( { NULL | expression } [ ,...n ] ) [ ,...n ] }
//            | select_clause
//          }
//
func (parser *Parser) get_statement_INSERT_INTO() (*Token_stmt_INSERT_INTO, *rsql.Error) {
	var (
		rsql_err               *rsql.Error
		token_stmt_insert_into *Token_stmt_INSERT_INTO
		qname                  rsql.Object_qname_t
		colname_list           []string
		listexpr               []*Token_primary
		listexpr_count_check   int
		list_of_VALUES         [][]*Token_primary
		xtable                 Xtable
		is_permanent_table     bool
	)

	// current lexeme must be INSERT

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INSERT)

	if rsql_err := parser.Check_compatibility(COMPAT_INSERT); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INSERT
		return nil, rsql_err
	}

	// check if not too many INSERT statements in the batch

	parser.Insert_stmts_count++

	if parser.Insert_stmts_count > rsql.G_BATCH_INSERTS_MAX_COUNT {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_TOO_MANY_INSERTS_IN_BATCH, rsql.ERROR_BATCH_ABORT, rsql.G_BATCH_INSERTS_MAX_COUNT)
	}

	// create Token_stmt_INSERT_INTO

	token_stmt_insert_into = &Token_stmt_INSERT_INTO{}
	parser.token_init(&token_stmt_insert_into.Token, TOK_STMT_INSERT_INTO)

	// eat optional INTO

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INTO {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip optional INTO
			return nil, rsql_err
		}
	}

	// eat table name

	switch parser.Current_lexeme.Lex_type {
	case lex.LEX_VARIABLE: // @vartable
		if qname, rsql_err = parser.Eat_variable_table_qname(); rsql_err != nil {
			return nil, rsql_err
		}

	default: // normal table. If database name or schema name are missing, Eat_object_qname() fills them with current database or schema name.
		if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
			return nil, rsql_err
		}

		is_permanent_table = true
	}

	token_stmt_insert_into.Ins_table_qname = qname

	//=== eat optional column identifier list ===

	listexpr_count_check = -1

	if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN {
		if colname_list, rsql_err = parser.Eat_list_of_colnames_for_INSERT(); rsql_err != nil {
			return nil, rsql_err
		}

		token_stmt_insert_into.Ins_listcol = colname_list
		listexpr_count_check = len(colname_list)
	}

	//=== decide if VALUES or SELECT clause ===

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_VALUES:
		//======== loop on each VALUES ========

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_VALUES); rsql_err != nil { // eat VALUES keyword
			return nil, rsql_err
		}

		for {
			if len(list_of_VALUES) >= SPEC_INSERT_INTO_VALUES_COUNT_MAX {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_VALUES_FOR_INSERT_INTO, rsql.ERROR_BATCH_ABORT, SPEC_INSERT_INTO_VALUES_COUNT_MAX)
			}

			// eat expression list. Expect left parenthese

			if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil {
				return nil, rsql_err
			}

			// eat expression list

			if listexpr, rsql_err = parser.Eat_VALUES_list_of_expressions(); rsql_err != nil { // at least one expression must be available. Element in listexpr is nil for DEFAULT keyword.
				return nil, rsql_err
			}

			if len(listexpr) != listexpr_count_check {
				if listexpr_count_check == -1 { // if listexpr_count_check not initialized yet because no column list is passed, initialize it.
					listexpr_count_check = len(listexpr)
				} else {
					//println(len(listexpr), listexpr_count_check)
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_VALUES_LISTEXPR_COUNT_FOR_INSERT_INTO, rsql.ERROR_BATCH_ABORT)
				}
			}

			// eat column list. Expect right parenthese

			if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil {
				return nil, rsql_err
			}

			// create new Value

			list_of_VALUES = append(list_of_VALUES, listexpr)

			// eat comma

			if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more expression, exit loop.
				break
			}

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
				return nil, rsql_err
			}
		}

		// here, at least one valid VALUES has been read.

		assert(len(list_of_VALUES) > 0)

		token_stmt_insert_into.Ins_values_array = list_of_VALUES
		token_stmt_insert_into.Ins_instruction_code = rsql.INSTR_STMT_INSERT_VALUES

	case lex.LEX_KEYWORD_SELECT:
		//======== eat SELECT clause ========
		var orderby_clause []*Orderby_part

		if xtable, orderby_clause, rsql_err = parser.Eat_xtable_select_or_union(); rsql_err != nil {
			return nil, rsql_err
		}

		if orderby_clause != nil {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_NOT_ALLOWED_IN_INSERT, rsql.ERROR_BATCH_ABORT)
		}

		token_stmt_insert_into.Ins_xtable = xtable
		token_stmt_insert_into.Ins_instruction_code = rsql.INSTR_STMT_INSERT_SELECT

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INSERT_INTO_SOURCE_MISSING, rsql.ERROR_BATCH_ABORT)
	}

	// for permanent table only, put in Registration

	if is_permanent_table {
		parser.Registration.Register_object_qname(token_stmt_insert_into.Ins_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_INSERT)
	}

	return token_stmt_insert_into, nil
}
