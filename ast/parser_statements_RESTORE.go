package ast

import (
	"strings"

	"rsql"
	"rsql/lex"
	"rsql/lk"
)

// parse RESTORE DATABASE statement.
//
//    RESTORE DATABASE database_name [ WITH <option> , ... ]
//      FROM DISK =  'file_name'
//
//    <option> ::= REPLACE | NO_USER | VERBOSE
//
// Example:
//
//    RESTORE DATABASE mydb
//      FROM DISK = '/Backups/mydb.rbak'
//
func (parser *Parser) get_statement_RESTORE() (*Token_stmt_RESTORE, *rsql.Error) {
	var (
		rsql_err      *rsql.Error
		database_name string
		filename      string
		option        string
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_RESTORE)

	if rsql_err := parser.Check_compatibility(COMPAT_RESTORE); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip RESTORE
		return nil, rsql_err
	}

	token_stmt_restore := &Token_stmt_RESTORE{}
	parser.token_init(&token_stmt_restore.Token, TOK_STMT_RESTORE)

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_DATABASE); rsql_err != nil { // eat DATABASE
		return nil, rsql_err
	}

	if database_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat database name
		return nil, rsql_err
	}

	token_stmt_restore.Rt_database_name = database_name

	// eat FROM DISK = 'filename'

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_FROM); rsql_err != nil { // eat FROM
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_DISK); rsql_err != nil { // eat DISK
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // eat '=' operator
		return nil, rsql_err
	}

	if filename, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat 'filename', which is a literal string in original case
		return nil, rsql_err
	}

	filename = strings.TrimSpace(filename)
	if len(filename) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EMPTY_FILENAME, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_restore.Rt_filename = filename

	// eat options if any

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
			return nil, rsql_err
		}

		for {
			if option, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat option
				return nil, rsql_err
			}

			switch lex.Auxword_t(option) {
			case lex.AUXWORD_REPLACE: // REPLACE option
				token_stmt_restore.Rt_option_replace = true

			case lex.AUXWORD_NO_USER: // NO_USER option
				token_stmt_restore.Rt_option_no_user = true

			case lex.AUXWORD_VERBOSE: // VERBOSE option
				token_stmt_restore.Rt_option_verbose = true

			default: // invalid option
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPTION_UNKNOWN, rsql.ERROR_BATCH_ABORT, option)
			}

			if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more option ==> exit loop
				break
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip COMMA
				return nil, rsql_err
			}

		} // end loop
	}

	// put in Registration

	parser.Registration.Register_db(database_name, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_DATABASE) // we want same exclusive lock as ALTER or DROP DATABASE

	return token_stmt_restore, nil
}
