package ast

import (
	"rsql"
	"rsql/lex"
)

//***************************************************************************
//                                                                          *
//                           eat builtin datatype                           *
//                                                                          *
//***************************************************************************

// Eat_builtin_datatype eats a builtin datatype.
//
// E.g. CHAR, VARCHAR(20), VARCHAR(MAX) etc.
//
// The argument 'option' is EAT_DATATYPE_DECLARATION or EAT_DATATYPE_CAST.
//
// All character datatypes used internally by rsql are unicode. So, NATIONAL CHARACTER, NVARCHAR, etc are of no use and always converted to CHAR and VARCHAR.
// So, the datatypes CHAR, VARCHAR or TEXT contain unicode strings.
//
// The current lexeme must be LEX_IDENTPART, or LEX_KEYWORD_NATIONAL followed by LEX_IDENTPART. Else, a syntax error is returned.
//
func (parser *Parser) Eat_builtin_datatype(option eat_datatype_option_t) (sql_datatype rsql.Datatype_t, sql_precision uint16, sql_scale uint16, sql_fixlen_flag bool, rsql_err *rsql.Error) {
	var (
		ok                                     bool
		sql_datatype_national_specifier_exists bool // flag if specifier NATIONAL exists
		sql_datatype_precision_allowed         bool // precision can be explicitely specified in the datatype definition
		sql_datatype_scale_allowed             bool // scale     can be explicitely specified in the datatype definition
		sql_datatype_precision_rparen_expected bool // flag if precision explicitely specified in the datatype definition
		sql_precision_is_identpart_max         bool // 'MAX' found as explicit precision, like in VARCHAR(MAX)
	)

	/* if current lexeme is optional NATIONAL specifier, skip it */

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_NATIONAL { // if NATIONAL is encountered, skip it
		sql_datatype_national_specifier_exists = true

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip specifier NATIONAL
			return 0, 0, 0, false, rsql_err
		}
	}

	/* eat datatype */

	if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // current lexeme must be an identpart
		return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_UNKNOWN, rsql.ERROR_BATCH_ABORT)
	}

	if sql_datatype, ok = rsql.G_DATATYPES[parser.Current_lexeme.Lex_word]; ok == false { // lookup for builtin datatype. If not found, return syntax error.
		return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_UNKNOWN, rsql.ERROR_BATCH_ABORT)
	}

	/* define if the datatype allows precision and scale */

	switch sql_datatype {
	case rsql.DATATYPE_DECIMAL,
		rsql.DATATYPE_NUMERIC:
		sql_datatype_precision_allowed = true
		sql_datatype_scale_allowed = true

	case rsql.DATATYPE_FLOAT: // if no precision specified: C double. Else, can be C double or C float. But in rsql, precision is discarded.
		sql_datatype_precision_allowed = true

	case rsql.DATATYPE_BINARY,
		rsql.DATATYPE_VARBINARY,
		rsql.DATATYPE_CHAR,
		rsql.DATATYPE_VARCHAR,
		rsql.DATATYPE_NCHAR,
		rsql.DATATYPE_NVARCHAR:
		sql_datatype_precision_allowed = true

	case rsql.DATATYPE_DOUBLE: // DOUBLE PRECISION
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip DOUBLE
			return 0, 0, 0, false, rsql_err
		}

		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_PRECISION { // current lexeme must be LEX_KEYWORD_PRECISION
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_UNKNOWN, rsql.ERROR_BATCH_ABORT)
		}

	case rsql.DATATYPE_TIME,
		rsql.DATATYPE_DATETIME2:
		sql_datatype_precision_allowed = true // for TIME and DATETIME2, precision can be given, but its value is discarded.
	}

	/* check if NATIONAL is followed by a permitted character datatype */

	if sql_datatype_national_specifier_exists {
		switch sql_datatype {
		case rsql.DATATYPE_CHAR:
			sql_datatype = rsql.DATATYPE_NCHAR

		case rsql.DATATYPE_VARCHAR:
			sql_datatype = rsql.DATATYPE_NVARCHAR

		case rsql.DATATYPE_TEXT:
			sql_datatype = rsql.DATATYPE_NTEXT

		default:
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_SPECIFIER_NATIONAL, rsql.ERROR_BATCH_ABORT)
		}
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip datatype name
		return 0, 0, 0, false, rsql_err
	}

	/* eat optional VARYING specifier */

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_VARYING { // perhaps VARYING
		switch sql_datatype { // check if VARYING follows a char, nchar or binary datatype
		case rsql.DATATYPE_CHAR:
			sql_datatype = rsql.DATATYPE_VARCHAR

		case rsql.DATATYPE_NCHAR:
			sql_datatype = rsql.DATATYPE_NVARCHAR

		case rsql.DATATYPE_BINARY:
			sql_datatype = rsql.DATATYPE_VARBINARY

		default:
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_SPECIFIER_VARYING, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip specifier VARYING
			return 0, 0, 0, false, rsql_err
		}
	}

	// here, sql_datatype has been almost resolved, but the following transformations can happen :

	//  rsql.DATATYPE_VARCHAR(max)   -> rsql.DATATYPE_TEXT
	//  rsql.DATATYPE_NVARCHAR(max)  -> rsql.DATATYPE_NTEXT
	//  rsql.DATATYPE_VARBINARY(max) -> rsql.DATATYPE_IMAGE
	//
	//  rsql.DATATYPE_NCHAR          -> rsql.DATATYPE_CHAR
	//  rsql.DATATYPE_NVARCHAR       -> rsql.DATATYPE_VARCHAR
	//  rsql.DATATYPE_NTEXT          -> rsql.DATATYPE_TEXT

	//  rsql.DATATYPE_SMALLMONEY     -> rsql.DATATYPE_MONEY

	//  rsql.DATATYPE_DECIMAL        -> rsql.DATATYPE_NUMERIC

	//  rsql.DATATYPE_REAL           -> rsql.DATATYPE_FLOAT
	//  rsql.DATATYPE_FLOAT(1..24)   -> rsql.DATATYPE_FLOAT
	//  rsql.DATATYPE_FLOAT(25..53)  -> rsql.DATATYPE_FLOAT
	//  rsql.DATATYPE_DOUBLE         -> rsql.DATATYPE_FLOAT
	//  rsql.DATATYPE_SMALLDATETIME  -> rsql.DATATYPE_DATETIME
	//  rsql.DATATYPE_DATETIME2      -> rsql.DATATYPE_DATETIME

	/* eat optional explicit (precision, scale) */

	if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN { // explicit precision cannot be 0 (see below)
		sql_datatype_precision_rparen_expected = true

		if sql_datatype_precision_allowed == false { // for this datatype, precision should not be specified
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_PRECISION_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip left paren
			return 0, 0, 0, false, rsql_err
		}

		// === read explicit precision

		switch lexeme := parser.Current_lexeme; {
		case lexeme.Lex_subtype == lex.LEX_LITERAL_NUMBER_INTEGRAL: // read explicit precision
			if sql_precision, rsql_err = lexeme.To_uint16(); rsql_err != nil {
				return 0, 0, 0, false, rsql_err
			}

		case lexeme.Is_identpart_max(): // read explicit 'max'
			sql_precision_is_identpart_max = true

			switch sql_datatype { // 'max' only allowed for varchar, nvarchar and varbinary
			case rsql.DATATYPE_VARCHAR:
				sql_datatype = rsql.DATATYPE_TEXT

			case rsql.DATATYPE_NVARCHAR:
				sql_datatype = rsql.DATATYPE_NTEXT

			case rsql.DATATYPE_VARBINARY:
				sql_datatype = rsql.DATATYPE_IMAGE

			default: // 'max' not allowed for other datatypes
				return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_MAX_PRECISION_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
			}

			sql_datatype_precision_allowed = false
			sql_datatype_scale_allowed = false

		default: // explicit precision not an integer nor 'max'
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_PRECISION, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip precision (integer or 'max')
			return 0, 0, 0, false, rsql_err
		}

		// === read explicit scale if any

		if parser.Current_lexeme.Lex_type == lex.LEX_COMMA { // if comma, read explicit scale
			if sql_datatype_scale_allowed == false { // for this datatype, scale should not be specified
				return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_SCALE_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
				return 0, 0, 0, false, rsql_err
			}

			switch lexeme := parser.Current_lexeme; {
			case lexeme.Lex_subtype == lex.LEX_LITERAL_NUMBER_INTEGRAL:
				if sql_scale, rsql_err = lexeme.To_uint16(); rsql_err != nil { // read explicit scale
					return 0, 0, 0, false, rsql_err
				}

			default: // scale not an integer
				return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_SCALE, rsql.ERROR_BATCH_ABORT)
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip skip explicit scale
				return 0, 0, 0, false, rsql_err
			}
		}

		// === check if explicit precision is valid

		if sql_precision_is_identpart_max == false { // if explicit precision is not 'max',
			if !(sql_precision > 0 && sql_scale <= sql_precision) { // it must be > 0, and scale must be <= precision
				return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION_OR_SCALE, rsql.ERROR_BATCH_ABORT)
			}
		} else {
			assert(sql_precision == 0 && sql_scale == 0)
		}

		// here, the right parenthese has not been eaten yet
	}

	// Here, if no explicit (precision, scale) has been specified :
	//                                                               precision == 0 and scale == 0.

	// If explicit (precision, scale) has been specified :
	//                                                            a) precision > 0 and scale == [0...precision]
	//                                                            b) 'max' --> precision == 0 and scale == 0.

	/* check if precision and scale range are compatible with datatype, and fix them. Promote some datatype like SMALLMONEY to MONEY, etc. */

	switch sql_datatype {
	case rsql.DATATYPE_BINARY:
		return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BINARY_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)

	case rsql.DATATYPE_VARBINARY:
		if sql_precision > rsql.DATATYPE_VARBINARY_PRECISION_MAX {
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION, rsql.ERROR_BATCH_ABORT, sql_precision, rsql.DATATYPE_VARBINARY_PRECISION_MAX)
		}

		if sql_precision == 0 { // default precision is 1 (declaration) or 30 (cast)
			sql_precision = 30
			if option == EAT_DATATYPE_DECLARATION {
				sql_precision = 1
			}
		}

	case rsql.DATATYPE_IMAGE:
		return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_IMAGE_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)

	case rsql.DATATYPE_CHAR, rsql.DATATYPE_VARCHAR:
		if sql_precision > rsql.DATATYPE_VARCHAR_PRECISION_MAX {
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION, rsql.ERROR_BATCH_ABORT, sql_precision, rsql.DATATYPE_VARCHAR_PRECISION_MAX)
		}

		if sql_precision == 0 { // default precision is 1 (declaration) or 30 (cast)
			sql_precision = 30
			if option == EAT_DATATYPE_DECLARATION {
				sql_precision = 1
			}
		}

		if sql_datatype == rsql.DATATYPE_CHAR {
			sql_fixlen_flag = true
		}

		sql_datatype = rsql.DATATYPE_VARCHAR

	case rsql.DATATYPE_NCHAR, rsql.DATATYPE_NVARCHAR:
		if sql_precision > rsql.DATATYPE_NVARCHAR_PRECISION_MAX {
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION, rsql.ERROR_BATCH_ABORT, sql_precision, rsql.DATATYPE_NVARCHAR_PRECISION_MAX)
		}

		if sql_precision == 0 { // default precision is 1 (declaration) or 30 (cast)
			sql_precision = 30
			if option == EAT_DATATYPE_DECLARATION {
				sql_precision = 1
			}
		}

		if sql_datatype == rsql.DATATYPE_NCHAR {
			sql_fixlen_flag = true
		}

		sql_datatype = rsql.DATATYPE_VARCHAR

	case rsql.DATATYPE_TEXT:
		return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_TEXT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)

	case rsql.DATATYPE_NTEXT:
		sql_datatype = rsql.DATATYPE_TEXT
		return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_TEXT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)

	case rsql.DATATYPE_SMALLMONEY:
		sql_datatype = rsql.DATATYPE_MONEY
		sql_precision = 0 // later, DATATYPE_PRECISION_MONEY and DATATYPE_SCALE_MONEY will be put in data.MONEY precision and scale, when MONEY dataslots are created
		sql_scale = 0

	case rsql.DATATYPE_MONEY:
		sql_precision = 0 // later, DATATYPE_PRECISION_MONEY and DATATYPE_SCALE_MONEY will be put in data.MONEY precision and scale, when MONEY dataslots are created
		sql_scale = 0

	case rsql.DATATYPE_DECIMAL,
		rsql.DATATYPE_NUMERIC:
		if sql_precision == 0 { // if no explicit precision,
			sql_precision = 18 // default precision is 18
		}
		if sql_precision > rsql.DATATYPE_NUMERIC_PRECISION_MAX { // 34 (note: in SQL Server, max precision is 38)
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION, rsql.ERROR_BATCH_ABORT, sql_precision, rsql.DATATYPE_NUMERIC_PRECISION_MAX)
		}
		sql_datatype = rsql.DATATYPE_NUMERIC

	case rsql.DATATYPE_REAL:
		sql_datatype = rsql.DATATYPE_FLOAT

	case rsql.DATATYPE_FLOAT:
		if sql_precision > 53 {
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SQL_DATATYPE_BAD_VALUE_FOR_PRECISION, rsql.ERROR_BATCH_ABORT, sql_precision, 53)
		}
		sql_precision = 0
		sql_scale = 0

	case rsql.DATATYPE_DOUBLE:
		sql_datatype = rsql.DATATYPE_FLOAT

	case rsql.DATATYPE_SMALLDATETIME:
		sql_datatype = rsql.DATATYPE_DATETIME

	case rsql.DATATYPE_DATETIME2:
		sql_datatype = rsql.DATATYPE_DATETIME
	}

	/* force precision to 0 for some datatypes */

	switch sql_datatype {
	case rsql.DATATYPE_FLOAT,
		rsql.DATATYPE_DOUBLE,
		rsql.DATATYPE_DATE,
		rsql.DATATYPE_TIME,
		rsql.DATATYPE_DATETIME,
		rsql.DATATYPE_DATETIME2:
		sql_precision = 0
		sql_scale = 0
	}

	/* eat right paren if precision has been explicitely specified in the datatype definition */

	if sql_datatype_precision_rparen_expected {
		if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect right paren
			return 0, 0, 0, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
			return 0, 0, 0, false, rsql_err
		}
	}

	/* return datatype values */

	return sql_datatype, sql_precision, sql_scale, sql_fixlen_flag, nil
}
