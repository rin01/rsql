package ast

import (
	"rsql"
)

// parse SLEEP statement.
// This statement doesn't exist in MS SQL Server.
// It is used for debugging.
//
//    SLEEP n
//
//    n  is sleep duration, in seconds. Can be a fractional number, e.g. 10.5 for 10 seconds and a half.
//
//    Max value is 600 (10 minutes).
//
func (parser *Parser) get_statement_SLEEP() (*Token_stmt_SLEEP, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		duration float64
	)

	token_stmt_sleep := &Token_stmt_SLEEP{}
	parser.token_init(&token_stmt_sleep.Token, TOK_STMT_SLEEP)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip SLEEP
		return nil, rsql_err
	}

	if duration, rsql_err = parser.Eat_literal_positive_float64(); rsql_err != nil { // eat duration in second, as constant float64
		return nil, rsql_err
	}

	if duration > SPEC_SLEEP_MAX_SECONDS {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_SLEEP_TOO_LONG, rsql.ERROR_BATCH_ABORT, SPEC_SLEEP_MAX_SECONDS)
	}

	token_stmt_sleep.Sl_duration = duration

	return token_stmt_sleep, nil
}
