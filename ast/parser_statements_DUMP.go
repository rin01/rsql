package ast

import (
	"strings"

	"rsql"
	"rsql/dict"
	"rsql/lex"
	"rsql/lk"
)

// parse DUMP statement.
// This statement doesn't exist in MS SQL Server.
//
//       DUMP PARAMETERS TO 'filename'
//       DUMP LOGINS TO 'filename'
//       DUMP DATABASE database_name WITH DDL_ONLY TO 'filename'
//
// Generate SQL statements that create logins, databases, users, roles, tables, permissions.
//
// DUMP PARAMETERS               is same as SHOW SQL PARAMETERS.
// DUMP LOGINS                   is same as SHOW SQL LOGINS.
// DUMP DATABASE database_name   doesn't create DDL statements for parameters or logins.
//
func (parser *Parser) get_statement_DUMP() (*Token_stmt_DUMP, *rsql.Error) {
	var (
		rsql_err       *rsql.Error
		database_name  string
		filename       string
		list_of_tables []rsql.Object_qname_t
	)

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip DUMP
		return nil, rsql_err
	}

	token_stmt_dump := &Token_stmt_DUMP{}
	parser.token_init(&token_stmt_dump.Token, TOK_STMT_DUMP)

	token_stmt_dump.Dp_server_default_collation = parser.Server_default_collation

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type == lex.LEX_IDENTPART && lexeme.Lex_word == string(lex.AUXWORD_PARAMETERS):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip PARAMETERS
			return nil, rsql_err
		}

		token_stmt_dump.Dp_instruction_code = rsql.INSTR_STMT_DUMP_PARAMETERS

	case lexeme.Lex_type == lex.LEX_IDENTPART && lexeme.Lex_word == string(lex.AUXWORD_LOGINS):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip LOGINS
			return nil, rsql_err
		}

		token_stmt_dump.Dp_instruction_code = rsql.INSTR_STMT_DUMP_LOGINS

	case lexeme.Lex_subtype == lex.LEX_KEYWORD_DATABASE:

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip DATABASE
			return nil, rsql_err
		}

		if database_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat database name
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
				return nil, rsql_err
			}

			for {
				switch lexeme := parser.Current_lexeme; {
				case lexeme.Lex_type == lex.LEX_IDENTPART && lexeme.Lex_word == string(lex.AUXWORD_DDL_ONLY):
					if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip option
						return nil, rsql_err
					}

					token_stmt_dump.Dp_option_DDL_ONLY = true

				default: // === invalid option ===
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPTION_UNKNOWN, rsql.ERROR_BATCH_ABORT, lexeme.Lex_word)
				}

				if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more option ==> exit loop
					break
				}

				if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip COMMA
					return nil, rsql_err
				}
			}
		}

		token_stmt_dump.Dp_instruction_code = rsql.INSTR_STMT_DUMP_DATABASE
		token_stmt_dump.Dp_database_name = database_name

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DUMP_TYPE_UNKNOWN, rsql.ERROR_BATCH_ABORT)
	}

	// check that DUMP DATABASE has DDL_ONLY option

	if token_stmt_dump.Dp_option_DDL_ONLY == false {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_DUMP_DATABASE_DDL_ONLY_MANDATORY, rsql.ERROR_BATCH_ABORT)
	}

	// eat TO 'filename'

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_TO); rsql_err != nil {
		return nil, rsql_err
	}

	if filename, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat 'filename', which is a literal string in original case
		return nil, rsql_err
	}

	filename = strings.TrimSpace(filename)
	if len(filename) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EMPTY_FILENAME, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_dump.Dp_filename = filename

	// put in Registration

	if list_of_tables, rsql_err = dict.MASTER.Retrieve_list_of_table_names(token_stmt_dump.Dp_database_name); rsql_err != nil {
		return nil, rsql_err
	}

	for _, qname := range list_of_tables {
		parser.Registration.Register_object_qname(qname, lk.LOCK_SHARED, rsql.PERMISSION_SELECT)
	}

	return token_stmt_dump, nil
}
