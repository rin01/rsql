package ast

import (
	"rsql"
	"rsql/lex"
	"rsql/lk"
)

// parse TRUNCATE TABLE statement.
//
//    TRUNCATE TABLE
//       [ database_name . [ schema_name ] . | schema_name . ] table_name
//         [ WITH SHRINK_FILE ]
//
//
// TRUNCATE TABLE table_name WITH SHRINK_FILE   cannot be run inside an explicit transaction (BEGIN TRAN ... COMMIT).
//
func (parser *Parser) get_statement_TRUNCATE_TABLE() (*Token_stmt_TRUNCATE_TABLE, *rsql.Error) {
	var (
		rsql_err                  *rsql.Error
		qname                     rsql.Object_qname_t
		token_stmt_truncate_table *Token_stmt_TRUNCATE_TABLE
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TRUNCATE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip TRUNCATE
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_TABLE); rsql_err != nil { // expect TABLE keyword
		return nil, rsql_err
	}

	// create Token_stmt_TRUNCATE_TABLE

	token_stmt_truncate_table = &Token_stmt_TRUNCATE_TABLE{}
	parser.token_init(&token_stmt_truncate_table.Token, TOK_STMT_TRUNCATE_TABLE)

	// eat table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_truncate_table.Tt_table_qname = qname

	// eat WITH SHRINK_FILE option if any

	compatibility := COMPAT_TRUNCATE

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_auxword(lex.AUXWORD_SHRINK_FILE); rsql_err != nil {
			return nil, rsql_err
		}

		token_stmt_truncate_table.Tt_shrink_file = true

		compatibility = COMPAT_TRUNCATE_WITH_SHRINK_FILE
	}

	if rsql_err := parser.Check_compatibility(compatibility); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// put in Registration

	parser.Registration.Register_object_qname(token_stmt_truncate_table.Tt_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_DELETE)

	return token_stmt_truncate_table, nil
}
