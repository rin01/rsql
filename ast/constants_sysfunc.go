package ast

type (
	Sysfunc_id_t         uint16
	Sysfunc_properties_t uint16
)

type Sysfunc_description struct {
	Sd_id         Sysfunc_id_t
	sd_properties Sysfunc_properties_t
}

const (
	SYSFUNCPROP_IS_NOT_PARENTHESIZED       Sysfunc_properties_t = 1 << iota // for CURRENT_USER, etc
	SYSFUNCPROP_FIRST_ARG_IS_DATEPARTFIELD                                  // for SYSFUNC_DATEADD, SYSFUNC_DATEDIFF, SYSFUNC_DATENAME, SYSFUNC_DATEPART
	SYSFUNCPROP_IS_VOLATILE                                                 // the function should not be constant folded
	SYSFUNCPROP_IS_AGGREGATE                                                // the function should not be constant folded
	SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG
)

func (sysfunc_descr *Sysfunc_description) Is_not_parenthesized() bool {
	return sysfunc_descr.sd_properties&SYSFUNCPROP_IS_NOT_PARENTHESIZED != 0
}

func (sysfunc_descr *Sysfunc_description) First_arg_is_datepartfield() bool {
	return sysfunc_descr.sd_properties&SYSFUNCPROP_FIRST_ARG_IS_DATEPARTFIELD != 0
}

func (sysfunc_descr *Sysfunc_description) Is_volatile() bool {
	return sysfunc_descr.sd_properties&SYSFUNCPROP_IS_VOLATILE != 0
}

func (sysfunc_descr *Sysfunc_description) Is_aggregate() bool {
	return sysfunc_descr.sd_properties&SYSFUNCPROP_IS_AGGREGATE != 0
}

func (sysfunc_descr *Sysfunc_description) Is_volatile_or_aggregate() bool {
	return sysfunc_descr.sd_properties&(SYSFUNCPROP_IS_VOLATILE|SYSFUNCPROP_IS_AGGREGATE) != 0
}

func (sysfunc_descr *Sysfunc_description) Is_sysvar_if_no_arg() bool {
	return sysfunc_descr.sd_properties&SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG != 0
}

func init() {
	var (
		ok bool
	)

	if SYSFUNC_DESCR_CONVERT, ok = G_SYSFUNC_DESCRIPTION_MAP["convert"]; ok == false {
		panic("SYSFUNC_DESCR_CONVERT not in map")
	}
	if SYSFUNC_DESCR_LEFT, ok = G_SYSFUNC_DESCRIPTION_MAP["left"]; ok == false {
		panic("SYSFUNC_DESCR_LEFT not in map")
	}
	if SYSFUNC_DESCR_RIGHT = G_SYSFUNC_DESCRIPTION_MAP["right"]; ok == false {
		panic("SYSFUNC_DESCR_RIGHT not in map")
	}
	if SYSFUNC_DESCR_CURRENT_TIMESTAMP = G_SYSFUNC_DESCRIPTION_MAP["current_timestamp"]; ok == false {
		panic("SYSFUNC_DESCR_CURRENT_TIMESTAMP not in map")
	}
	if SYSFUNC_DESCR_NULLIF, ok = G_SYSFUNC_DESCRIPTION_MAP["nullif"]; ok == false {
		panic("SYSFUNC_DESCR_NULLIF not in map")
	}
	if SYSFUNC_DESCR_IIF, ok = G_SYSFUNC_DESCRIPTION_MAP["iif"]; ok == false {
		panic("SYSFUNC_DESCR_IIF not in map")
	}
	if SYSFUNC_DESCR_DATEPART, ok = G_SYSFUNC_DESCRIPTION_MAP["datepart"]; ok == false {
		panic("SYSFUNC_DESCR_DATEPART not in map")
	}
	if SYSFUNC_DESCR_GETUTCDATE, ok = G_SYSFUNC_DESCRIPTION_MAP["getutcdate"]; ok == false {
		panic("SYSFUNC_DESCR_GETUTCDATE not in map")
	}
	if SYSFUNC_DESCR_AGGR_SUM, ok = G_SYSFUNC_DESCRIPTION_MAP["sum"]; ok == false {
		panic("SYSFUNC_DESCR_AGGR_SUM not in map")
	}
	if SYSFUNC_DESCR_AGGR_COUNT, ok = G_SYSFUNC_DESCRIPTION_MAP["count"]; ok == false {
		panic("SYSFUNC_DESCR_AGGR_COUNT not in map")
	}
}

const (
	SYSFUNC_atatDATEFIRST  Sysfunc_id_t = iota + 1
	SYSFUNC_atatDATEFORMAT              // not a MS SQL function
	SYSFUNC_atatLANGUAGE
	SYSFUNC_atatVERSION
	SYSFUNC_atatSERVERNAME
	SYSFUNC_atatSERVICENAME
	SYSFUNC_atatIDENTITY
	SYSFUNC_atatROWCOUNT
	SYSFUNC_atatERROR
	SYSFUNC_atatTRANCOUNT

	SYSFUNC_SCOPE_IDENTITY

	SYSFUNC_SUSER_NAME
	SYSFUNC_SUSER_ID
	SYSFUNC_DB_NAME
	SYSFUNC_DB_ID
	SYSFUNC_SCHEMA_NAME
	SYSFUNC_SCHEMA_ID
	SYSFUNC_USER_NAME
	SYSFUNC_USER_ID
	SYSFUNC_DATABASE_PRINCIPAL_ID
	SYSFUNC_OBJECT_NAME
	SYSFUNC_OBJECT_ID

	SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE
	SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE
	SYSFUNC_DMY_SYSLANGUAGE
	SYSFUNC_FIT_NO_TRUNCATE_VARBINARY
	SYSFUNC_FIT_NO_TRUNCATE_VARCHAR

	SYSFUNC_DATENAME
	SYSFUNC_DATEPART
	SYSFUNC_DAY
	SYSFUNC_MONTH
	SYSFUNC_YEAR
	SYSFUNC_EOMONTH
	SYSFUNC_BOMONTH
	SYSFUNC_DATEDIFF
	SYSFUNC_DATEADD
	SYSFUNC_DATEFROMPARTS
	SYSFUNC_TIMEFROMPARTS
	SYSFUNC_DATETIMEFROMPARTS
	SYSFUNC_DATETIME2FROMPARTS
	SYSFUNC_GETUTCDATE
	SYSFUNC_UTC_TO_LOCAL // the parser replaces GETDATE by this internal function
	SYSFUNC_SWITCHOFFSET
	SYSFUNC_TODATETIMEOFFSET
	SYSFUNC_SYSDATETIMEOFFSET

	SYSFUNC_ABS
	SYSFUNC_ACOS
	SYSFUNC_ASIN
	SYSFUNC_ATAN
	SYSFUNC_ATN2
	SYSFUNC_CEILING
	SYSFUNC_COS
	SYSFUNC_COT
	SYSFUNC_DEGREES
	SYSFUNC_EXP
	SYSFUNC_FLOOR
	SYSFUNC_LOG
	SYSFUNC_LOG10
	SYSFUNC_PI
	SYSFUNC_POWER
	SYSFUNC_RADIANS
	SYSFUNC_RAND
	SYSFUNC_ROUND
	SYSFUNC_SIGN
	SYSFUNC_SIN
	SYSFUNC_SQRT
	SYSFUNC_SQUARE
	SYSFUNC_TAN

	SYSFUNC_ASCII
	SYSFUNC_CHAR
	SYSFUNC_CHARINDEX
	SYSFUNC_CHARLEN
	SYSFUNC_DATALENGTH
	SYSFUNC_DIFFERENCE
	SYSFUNC_LEFT
	SYSFUNC_LEN
	SYSFUNC_LOWER
	SYSFUNC_LTRIM
	SYSFUNC_NCHAR
	SYSFUNC_PATINDEX
	SYSFUNC_QUOTENAME
	SYSFUNC_REPLACE
	SYSFUNC_REPLICATE
	SYSFUNC_REVERSE
	SYSFUNC_RIGHT
	SYSFUNC_RTRIM
	SYSFUNC_SOUNDEX
	SYSFUNC_SPACE
	SYSFUNC_STR
	SYSFUNC_STUFF
	SYSFUNC_SUBSTRING
	SYSFUNC_TRIM
	SYSFUNC_UNICODE
	SYSFUNC_UPPER
	SYSFUNC_CONCAT

	SYSFUNC_FORMAT
	SYSFUNC_CONVERT

	SYSFUNC_ISDATE
	SYSFUNC_ISNUMERIC
	SYSFUNC_ISNULL
	SYSFUNC_IIF
	SYSFUNC_CHOOSE
	SYSFUNC_COALESCE
	SYSFUNC_TYPEOF

	SYSFUNC_NULLIF // the parser replaces it by iif function

	SYSFUNC_RANDOM_VARCHAR
	SYSFUNC_RANDOM_INT
	SYSFUNC_RANDOM_BIGINT
	SYSFUNC_RANDOM_NUMERIC
	SYSFUNC_RANDOM_FLOAT
	SYSFUNC_RANDOM_DATE

	SYSFUNC_AGGR_COUNT
	SYSFUNC_AGGR_COUNT_BIG
	SYSFUNC_AGGR_SUM
	SYSFUNC_AGGR_MIN
	SYSFUNC_AGGR_MAX
	SYSFUNC_AGGR_AVG
)

// lookup table for function names.
// Internal functions used only by the compiler are not listed here (see SYSFUNC_DESCR_xxx list of constants below).
//
var G_SYSFUNC_DESCRIPTION_MAP = map[string]*Sysfunc_description{

	"@@datefirst":   {SYSFUNC_atatDATEFIRST, SYSFUNCPROP_IS_NOT_PARENTHESIZED},
	"@@dateformat":  {SYSFUNC_atatDATEFORMAT, SYSFUNCPROP_IS_NOT_PARENTHESIZED},
	"@@language":    {SYSFUNC_atatLANGUAGE, SYSFUNCPROP_IS_NOT_PARENTHESIZED},
	"@@version":     {SYSFUNC_atatVERSION, SYSFUNCPROP_IS_NOT_PARENTHESIZED},
	"@@servername":  {SYSFUNC_atatSERVERNAME, SYSFUNCPROP_IS_NOT_PARENTHESIZED},
	"@@servicename": {SYSFUNC_atatSERVICENAME, SYSFUNCPROP_IS_NOT_PARENTHESIZED},
	"@@identity":    {SYSFUNC_atatIDENTITY, SYSFUNCPROP_IS_NOT_PARENTHESIZED | SYSFUNCPROP_IS_VOLATILE},
	"@@rowcount":    {SYSFUNC_atatROWCOUNT, SYSFUNCPROP_IS_NOT_PARENTHESIZED | SYSFUNCPROP_IS_VOLATILE},
	"@@error":       {SYSFUNC_atatERROR, SYSFUNCPROP_IS_NOT_PARENTHESIZED},
	"@@trancount":   {SYSFUNC_atatTRANCOUNT, SYSFUNCPROP_IS_NOT_PARENTHESIZED | SYSFUNCPROP_IS_VOLATILE},

	"scope_identity": {SYSFUNC_SCOPE_IDENTITY, SYSFUNCPROP_IS_VOLATILE},

	"suser_name":            {SYSFUNC_SUSER_NAME, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},  // if no argument, the parser transforms the sysfunc token into the variable _@system_user_name
	"suser_id":              {SYSFUNC_SUSER_ID, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},    // if no argument, the parser transforms the sysfunc token into the variable _@system_user_id
	"db_name":               {SYSFUNC_DB_NAME, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},     // if no argument, the parser transforms the sysfunc token into the variable _@current_db_name
	"db_id":                 {SYSFUNC_DB_ID, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},       // if no argument, the parser transforms the sysfunc token into the variable _@current_db_id
	"schema_name":           {SYSFUNC_SCHEMA_NAME, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG}, // if no argument, the parser transforms the sysfunc token into the variable _@current_schema_name
	"schema_id":             {SYSFUNC_SCHEMA_ID, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},   // if no argument, the parser transforms the sysfunc token into the variable _@current_schema_id
	"user_name":             {SYSFUNC_USER_NAME, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},   // if no argument, the parser transforms the sysfunc token into the variable _@current_user_name
	"user_id":               {SYSFUNC_USER_ID, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},     // if no argument, the parser transforms the sysfunc token into the variable _@current_user_id
	"database_principal_id": {SYSFUNC_USER_ID, SYSFUNCPROP_IS_SYSVAR_IF_NO_ARG},     // if no argument, the parser transforms the sysfunc token into the variable _@current_user_id
	"object_name":           {SYSFUNC_OBJECT_NAME, 0},
	"object_id":             {SYSFUNC_OBJECT_ID, 0},

	"getutcdate":     {SYSFUNC_GETUTCDATE, SYSFUNCPROP_IS_VOLATILE}, // in a SQL batch, getutcdate() and sysutcdatetime() are always transformed into the variable _@current_timestamp.
	"sysutcdatetime": {SYSFUNC_GETUTCDATE, SYSFUNCPROP_IS_VOLATILE}, //     A statement "SET _@current_timestamp = GETUTCDATE()" is appended automatically by the parser after some statements, like SELECT, UPDATE, etc, so that elapsed time can be measured.

	"datename":           {SYSFUNC_DATENAME, SYSFUNCPROP_FIRST_ARG_IS_DATEPARTFIELD},
	"datepart":           {SYSFUNC_DATEPART, SYSFUNCPROP_FIRST_ARG_IS_DATEPARTFIELD},
	"day":                {SYSFUNC_DAY, 0},   // DAY   will be substituted by DATEPART
	"month":              {SYSFUNC_MONTH, 0}, // MONTH will be substituted by DATEPART
	"year":               {SYSFUNC_YEAR, 0},  // YEAR  will be substituted by DATEPART
	"eomonth":            {SYSFUNC_EOMONTH, 0},
	"bomonth":            {SYSFUNC_BOMONTH, 0},
	"datediff":           {SYSFUNC_DATEDIFF, SYSFUNCPROP_FIRST_ARG_IS_DATEPARTFIELD},
	"dateadd":            {SYSFUNC_DATEADD, SYSFUNCPROP_FIRST_ARG_IS_DATEPARTFIELD},
	"datefromparts":      {SYSFUNC_DATEFROMPARTS, 0},
	"timefromparts":      {SYSFUNC_TIMEFROMPARTS, 0},
	"datetimefromparts":  {SYSFUNC_DATETIMEFROMPARTS, 0},
	"datetime2fromparts": {SYSFUNC_DATETIME2FROMPARTS, 0},
	"switchoffset":       {SYSFUNC_SWITCHOFFSET, 0},
	"todatetimeoffset":   {SYSFUNC_TODATETIMEOFFSET, 0},
	"sysdatetimeoffset":  {SYSFUNC_SYSDATETIMEOFFSET, 0},
	"getdate":            {SYSFUNC_UTC_TO_LOCAL, 0},                                // the parser replaces it by the function UTC_TO_LOCAL(_@current_timestamp)
	"sysdatetime":        {SYSFUNC_UTC_TO_LOCAL, 0},                                //    same
	"current_timestamp":  {SYSFUNC_UTC_TO_LOCAL, SYSFUNCPROP_IS_NOT_PARENTHESIZED}, //    same

	"abs":     {SYSFUNC_ABS, 0},
	"acos":    {SYSFUNC_ACOS, 0},
	"asin":    {SYSFUNC_ASIN, 0},
	"atan":    {SYSFUNC_ATAN, 0},
	"atn2":    {SYSFUNC_ATN2, 0},
	"ceiling": {SYSFUNC_CEILING, 0},
	"cos":     {SYSFUNC_COS, 0},
	"cot":     {SYSFUNC_COT, 0},
	"degrees": {SYSFUNC_DEGREES, 0},
	"exp":     {SYSFUNC_EXP, 0},
	"floor":   {SYSFUNC_FLOOR, 0},
	"log":     {SYSFUNC_LOG, 0},
	"log10":   {SYSFUNC_LOG10, 0},
	"pi":      {SYSFUNC_PI, 0},
	"power":   {SYSFUNC_POWER, 0},
	"radians": {SYSFUNC_RADIANS, 0},
	"rand":    {SYSFUNC_RAND, SYSFUNCPROP_IS_VOLATILE},
	"round":   {SYSFUNC_ROUND, 0},
	"sign":    {SYSFUNC_SIGN, 0},
	"sin":     {SYSFUNC_SIN, 0},
	"sqrt":    {SYSFUNC_SQRT, 0},
	"square":  {SYSFUNC_SQUARE, 0},
	"tan":     {SYSFUNC_TAN, 0},

	"ascii":      {SYSFUNC_ASCII, 0},
	"char":       {SYSFUNC_CHAR, 0},
	"charindex":  {SYSFUNC_CHARINDEX, 0},
	"charlen":    {SYSFUNC_CHARLEN, 0},
	"datalength": {SYSFUNC_DATALENGTH, 0},
	"difference": {SYSFUNC_DIFFERENCE, 0},
	"left":       {SYSFUNC_LEFT, 0},
	"len":        {SYSFUNC_LEN, 0},
	"lower":      {SYSFUNC_LOWER, 0},
	"ltrim":      {SYSFUNC_LTRIM, 0},
	"nchar":      {SYSFUNC_NCHAR, 0},
	"patindex":   {SYSFUNC_PATINDEX, 0},
	"quotename":  {SYSFUNC_QUOTENAME, 0},
	"replace":    {SYSFUNC_REPLACE, 0},
	"replicate":  {SYSFUNC_REPLICATE, 0},
	"reverse":    {SYSFUNC_REVERSE, 0},
	"right":      {SYSFUNC_RIGHT, 0},
	"rtrim":      {SYSFUNC_RTRIM, 0},
	"soundex":    {SYSFUNC_SOUNDEX, 0},
	"space":      {SYSFUNC_SPACE, 0},
	"str":        {SYSFUNC_STR, 0},
	"stuff":      {SYSFUNC_STUFF, 0},
	"substring":  {SYSFUNC_SUBSTRING, 0},
	"trim":       {SYSFUNC_TRIM, 0},
	"unicode":    {SYSFUNC_UNICODE, 0},
	"upper":      {SYSFUNC_UPPER, 0},
	"concat":     {SYSFUNC_CONCAT, 0},

	"format":  {SYSFUNC_FORMAT, 0},
	"convert": {SYSFUNC_CONVERT, 0},

	"isdate":    {SYSFUNC_ISDATE, 0},
	"isnumeric": {SYSFUNC_ISNUMERIC, 0},
	"isnull":    {SYSFUNC_ISNULL, 0},
	"iif":       {SYSFUNC_IIF, 0},
	"choose":    {SYSFUNC_CHOOSE, 0},
	"coalesce":  {SYSFUNC_COALESCE, 0},
	"typeof":    {SYSFUNC_TYPEOF, 0},

	"nullif": {SYSFUNC_NULLIF, 0}, // the parser replaces it by iif function

	"random_varchar": {SYSFUNC_RANDOM_VARCHAR, SYSFUNCPROP_IS_VOLATILE},
	"random_int":     {SYSFUNC_RANDOM_INT, SYSFUNCPROP_IS_VOLATILE},
	"random_bigint":  {SYSFUNC_RANDOM_BIGINT, SYSFUNCPROP_IS_VOLATILE},
	"random_numeric": {SYSFUNC_RANDOM_NUMERIC, SYSFUNCPROP_IS_VOLATILE},
	"random_float":   {SYSFUNC_RANDOM_FLOAT, SYSFUNCPROP_IS_VOLATILE},
	"random_date":    {SYSFUNC_RANDOM_DATE, SYSFUNCPROP_IS_VOLATILE},

	"count":     {SYSFUNC_AGGR_COUNT, SYSFUNCPROP_IS_AGGREGATE},
	"count_big": {SYSFUNC_AGGR_COUNT_BIG, SYSFUNCPROP_IS_AGGREGATE},
	"sum":       {SYSFUNC_AGGR_SUM, SYSFUNCPROP_IS_AGGREGATE},
	"min":       {SYSFUNC_AGGR_MIN, SYSFUNCPROP_IS_AGGREGATE},
	"max":       {SYSFUNC_AGGR_MAX, SYSFUNCPROP_IS_AGGREGATE},
	"avg":       {SYSFUNC_AGGR_AVG, SYSFUNCPROP_IS_AGGREGATE}, // the decorator will replace avg(x) by sum(x)/count(x)
}

// constants to *Sysfunc_description for internal functions, or functions that have a name which is a LEX_KEYWORD.
//
var (
	// internal function (only the parser can invoke it, not the SQL batch). Sysfunc_description does not exist in G_SYSFUNC_DESCRIPTION_MAP.
	SYSFUNC_DESCR_COMPILE_VARCHAR_TO_REGEXPLIKE *Sysfunc_description = &Sysfunc_description{SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE, 0}
	SYSFUNC_DESCR_FIRSTDAYOFWEEK_SYSLANGUAGE    *Sysfunc_description = &Sysfunc_description{SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE, 0}
	SYSFUNC_DESCR_DMY_SYSLANGUAGE               *Sysfunc_description = &Sysfunc_description{SYSFUNC_DMY_SYSLANGUAGE, 0}
	SYSFUNC_DESCR_FIT_NO_TRUNCATE_VARBINARY     *Sysfunc_description = &Sysfunc_description{SYSFUNC_FIT_NO_TRUNCATE_VARBINARY, 0}
	SYSFUNC_DESCR_FIT_NO_TRUNCATE_VARCHAR       *Sysfunc_description = &Sysfunc_description{SYSFUNC_FIT_NO_TRUNCATE_VARCHAR, 0}

	// some useful pointers to Sysfunc_description that are used by the parser. Sysfunc_description exists in G_SYSFUNC_DESCRIPTION_MAP.
	SYSFUNC_DESCR_CONVERT           *Sysfunc_description
	SYSFUNC_DESCR_LEFT              *Sysfunc_description // name is keyword
	SYSFUNC_DESCR_RIGHT             *Sysfunc_description // name is keyword
	SYSFUNC_DESCR_CURRENT_TIMESTAMP *Sysfunc_description
	SYSFUNC_DESCR_NULLIF            *Sysfunc_description
	SYSFUNC_DESCR_IIF               *Sysfunc_description
	SYSFUNC_DESCR_DATEPART          *Sysfunc_description
	SYSFUNC_DESCR_GETUTCDATE        *Sysfunc_description
	SYSFUNC_DESCR_AGGR_SUM          *Sysfunc_description
	SYSFUNC_DESCR_AGGR_COUNT        *Sysfunc_description
)

type Datepartfield_t int16

func (d Datepartfield_t) String() string {
	var (
		s string
	)

	switch d {
	case 0:
		s = ""
	case DATEPARTFIELD_YEAR:
		s = "year"
	case DATEPARTFIELD_QUARTER:
		s = "quarter"
	case DATEPARTFIELD_MONTH:
		s = "month"
	case DATEPARTFIELD_DAYOFYEAR:
		s = "dayofyear"
	case DATEPARTFIELD_DAY:
		s = "day"
	case DATEPARTFIELD_WEEK:
		s = "week"
	case DATEPARTFIELD_WEEKDAY:
		s = "weekday"
	case DATEPARTFIELD_HOUR:
		s = "hour"
	case DATEPARTFIELD_MINUTE:
		s = "minute"
	case DATEPARTFIELD_SECOND:
		s = "second"
	case DATEPARTFIELD_MILLISECOND:
		s = "millisecond"
	case DATEPARTFIELD_MICROSECOND:
		s = "microsecond"
	case DATEPARTFIELD_NANOSECOND:
		s = "nanosecond"
	case DATEPARTFIELD_ISO_WEEK:
		s = "iso_week"
	default:
		panic("unknown DATEPARTFIELD")
	}

	return s
}

// first argument for functions DATEADD, DATEDIFF, DATENAME, DATEPART.
//
const (
	DATEPARTFIELD_YEAR Datepartfield_t = iota + 1
	DATEPARTFIELD_QUARTER
	DATEPARTFIELD_MONTH
	DATEPARTFIELD_DAYOFYEAR
	DATEPARTFIELD_DAY
	DATEPARTFIELD_WEEK
	DATEPARTFIELD_WEEKDAY
	DATEPARTFIELD_HOUR
	DATEPARTFIELD_MINUTE
	DATEPARTFIELD_SECOND
	DATEPARTFIELD_MILLISECOND
	DATEPARTFIELD_MICROSECOND
	DATEPARTFIELD_NANOSECOND
	DATEPARTFIELD_ISO_WEEK
)

// first argument for functions DATEADD, DATEDIFF, DATENAME, DATEPART.
//
var G_DATEPARTFIELD_TO_VALUE = map[string]Datepartfield_t{
	"year":        DATEPARTFIELD_YEAR,
	"yyyy":        DATEPARTFIELD_YEAR,
	"yy":          DATEPARTFIELD_YEAR,
	"quarter":     DATEPARTFIELD_QUARTER,
	"qq":          DATEPARTFIELD_QUARTER,
	"q":           DATEPARTFIELD_QUARTER,
	"month":       DATEPARTFIELD_MONTH,
	"mm":          DATEPARTFIELD_MONTH,
	"m":           DATEPARTFIELD_MONTH,
	"dayofyear":   DATEPARTFIELD_DAYOFYEAR,
	"dy":          DATEPARTFIELD_DAYOFYEAR,
	"y":           DATEPARTFIELD_DAYOFYEAR,
	"day":         DATEPARTFIELD_DAY,
	"dd":          DATEPARTFIELD_DAY,
	"d":           DATEPARTFIELD_DAY,
	"week":        DATEPARTFIELD_WEEK,
	"wk":          DATEPARTFIELD_WEEK,
	"ww":          DATEPARTFIELD_WEEK,
	"weekday":     DATEPARTFIELD_WEEKDAY,
	"dw":          DATEPARTFIELD_WEEKDAY,
	"hour":        DATEPARTFIELD_HOUR,
	"hh":          DATEPARTFIELD_HOUR,
	"minute":      DATEPARTFIELD_MINUTE,
	"mi":          DATEPARTFIELD_MINUTE,
	"n":           DATEPARTFIELD_MINUTE,
	"second":      DATEPARTFIELD_SECOND,
	"ss":          DATEPARTFIELD_SECOND,
	"s":           DATEPARTFIELD_SECOND,
	"millisecond": DATEPARTFIELD_MILLISECOND,
	"ms":          DATEPARTFIELD_MILLISECOND,
	"microsecond": DATEPARTFIELD_MICROSECOND,
	"mcs":         DATEPARTFIELD_MICROSECOND,
	"nanosecond":  DATEPARTFIELD_NANOSECOND,
	"ns":          DATEPARTFIELD_NANOSECOND,
	"iso_week":    DATEPARTFIELD_ISO_WEEK,
	"iso_wk":      DATEPARTFIELD_ISO_WEEK,
	"iso_ww":      DATEPARTFIELD_ISO_WEEK,
}
