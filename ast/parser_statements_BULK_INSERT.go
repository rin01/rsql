package ast

import (
	"strconv"
	"strings"

	"rsql"
	"rsql/lex"
	"rsql/lk"
)

// parse BULK INSERT statement.
//
//    BULK INSERT
//       [ database_name . [ schema_name ] . | schema_name . ] table_name
//          FROM 'data_file'
//         [ WITH
//       (
//       [ [ , ] CODEPAGE = { 'ACP' | 'RAW' | 'code_page' } ]
//       [ [ , ] FIELDTERMINATOR = 'field_terminator' ]
//       [ [ , ] ROWTERMINATOR = 'row_terminator' ]
//       [ [ , ] FIRSTROW = first_row ]
//       [ [ , ] LASTROW = last_row ]
//       [ [ , ] KEEPIDENTITY ]
//       [ [ , ] RTRIM ]                                           // note: this option is for rsql only, It is not compatible with MS SQL Server.
//       )]
//
//
//    'data_file'
//          name of data file to import. Empty fileds are converted to NULL, and 0x00 is converted to empty string for VARCHAR or spaces for CHAR.
//
//    CODEPAGE = { 'ACP' | 'code_page' }
//          ACP  is Microsoft windows1252 encoding, which is a superset of iso-8859-1
//          'code_page' is e.g. 'utf8', 'UTF-8', 'iso8859-1', 'iso-8859-1', 'windows1252', etc. code_page is case insensitive and quite lenient about spelling.
//          By default, codepage is utf8.
//
//    FIELDTERMINATOR ='field_terminator'
//          Specifies the field terminator to be used for text data file. By default, it is '\t'
//
//    ROWTERMINATOR ='row_terminator'
//          Specifies the row terminator to be used for text data file. By default, it is '\n', which is a special case as it consider that both \n and \r\n terminate lines.
//
//    FISTROW = first_row
//          Specifies the first line to load. It is 1-based. By default, it is 1.
//
//    LASTROW = last_row
//          Specifies the last line to load. It is 1-based. By default, it is 0, which means that all lines must be loaded.
//
//    KEEPIDENTITY
//          Specifies that values of IDENTITY column must be preserved as read from data file. The field is data file cannot be empty.
//          If KEEPIDENTITY is not specified, the value of IDENTITY column in data file are ignored. The field is data file can be empty. A unique value will be generated during import.
//          For rsql, if the target table contains an IDENTITY column, for safety, it is mandatory to specify either KEEPIDENTITY or NEWIDENTITY.
//
//    NEWIDENTITY
//          This is not a valid MS SQL Server option.
//          For rsql, it specifies that new values for IDENTITY column will be generated automatically, and that the corresponding field in the data file will be ignored.
//          If the target table contains an IDENTITY column, for safety, it is mandatory to specify either KEEPIDENTITY or NEWIDENTITY.
//
//    KEEPNULLS
//          inserts NULL instead of DEFAULT values. Mandatory.
//
//    RTRIM
//          This is not a valid MS SQL Server option.
//          For rsql, it means that all fields from data file must be right trimmed.
//          This is useful if you have a data file with fixed-length columns, and want to convert fields with only spaces into NULL for VARCHAR or CHAR datatypes, instead or keeping the spaces.
//
//
//    Conversion of fields for    BULK INSERT: table <-- text file     or    BULK EXPORT: table --> text file     are as follows:
//
//    table                              text file
//    =====                              =========
//
//    VARCHAR:
//    -------------------
//        NULL                <-->       empty field
//        empty string        <-->       0x00
//        one or morespaces   <-->       one or more spaces
//        hello               <-->       hello
//
//    INT:
//    -------------------
//        NULL                <-->       empty field
//        NULL                <--        one or more spaces
//        0                   <-->       0
//        123                 <-->       123
//
//    DATETIME:
//    -------------------
//        NULL                <-->       empty field
//        NULL                <--        one or more spaces
//        2000.02.03          <-->       2000.02.03
//
//    Note that unlike BULK INSERT that imports NULL, INSERT INTO inserts 0 and 1900.01.01 for empty string or space string when target column is numeral or datetime datatypes.
//
func (parser *Parser) get_statement_BULK_INSERT() (*Token_stmt_BULK_INSERT, *rsql.Error) {
	var (
		err                    error
		rsql_err               *rsql.Error
		qname                  rsql.Object_qname_t
		token_stmt_bulk_insert *Token_stmt_BULK_INSERT
		filename               string
		option                 string
		codepage               string
		fieldterminator        string
		rowterminator          string
		firstrow               int64
		lastrow                int64
		keepidentity           bool
		newidentity            bool
		keepnulls              bool
		rtrim                  bool
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INSERT)

	if rsql_err := parser.Check_compatibility(COMPAT_BULK_INSERT); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_BULK_INSERT

	token_stmt_bulk_insert = &Token_stmt_BULK_INSERT{}
	parser.token_init(&token_stmt_bulk_insert.Token, TOK_STMT_BULK_INSERT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INSERT
		return nil, rsql_err
	}

	// eat table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_bulk_insert.Bi_table_qname = qname

	// eat FROM 'data_file'

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_FROM); rsql_err != nil {
		return nil, rsql_err
	}

	if filename, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat 'data_file', which is a literal string in original case
		return nil, rsql_err
	}

	filename = strings.TrimSpace(filename)
	if len(filename) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EMPTY_FILENAME, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_bulk_insert.Bi_filename = filename

	// eat WITH options, if any

	codepage = "utf8"
	rowterminator = "\\n" // rowterminator must end with \n. Both \n and \r\n are considered to terminate lines.
	fieldterminator = "\\t"
	firstrow = 1 // lines are 1-based
	lastrow = 0  // indicates the last row

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect left parenthese
			return nil, rsql_err
		}

		for {
			if option, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat option
				return nil, rsql_err
			}

			switch lex.Auxword_t(option) { // process option
			case lex.AUXWORD_CODEPAGE: // === CODEPAGE option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if codepage, rsql_err = parser.Eat_literal_stringval_lowercase(); rsql_err != nil { // eat codepage, which is a literal string in lower case
					return nil, rsql_err
				}

			case lex.AUXWORD_ROWTERMINATOR: // === ROWTERMINATOR option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if rowterminator, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat rowterminator, which is a literal string in original case
					return nil, rsql_err
				}

			case lex.AUXWORD_FIELDTERMINATOR: // === FIELDTERMINATOR option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if fieldterminator, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat fieldterminator, which is a literal string in original case
					return nil, rsql_err
				}

			case lex.AUXWORD_FIRSTROW: // === FIRSTROW option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if firstrow, rsql_err = parser.Eat_literal_integral_positive_int64(); rsql_err != nil { // eat firstrow
					return nil, rsql_err
				}

			case lex.AUXWORD_LASTROW: // === LASTROW option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if lastrow, rsql_err = parser.Eat_literal_integral_positive_int64(); rsql_err != nil { // eat lastrow
					return nil, rsql_err
				}

			case lex.AUXWORD_KEEPIDENTITY: // === KEEPIDENTITY option ===
				keepidentity = true

			case lex.AUXWORD_NEWIDENTITY: // === NEWIDENTITY option ===
				newidentity = true

			case lex.AUXWORD_KEEPNULLS: // === KEEPNULLS option ===
				keepnulls = true

			case lex.AUXWORD_RTRIM: // === RTRIM option ===
				rtrim = true

			default: // === invalid option ===
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPTION_UNKNOWN, rsql.ERROR_BATCH_ABORT, option)
			}

			if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more option ==> exit loop
				break
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip COMMA
				return nil, rsql_err
			}
		} // end loop

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
			return nil, rsql_err
		}
	}

	// check options

	if len(fieldterminator) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_FIELDTERMINATOR, rsql.ERROR_BATCH_ABORT, fieldterminator)
	}
	if fieldterminator, err = strconv.Unquote(`"` + fieldterminator + `"`); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_FIELDTERMINATOR, rsql.ERROR_BATCH_ABORT, fieldterminator)
	}

	rowterminator_bak := rowterminator
	if rowterminator, err = strconv.Unquote(`"` + rowterminator + `"`); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_ROWTERMINATOR, rsql.ERROR_BATCH_ABORT, rowterminator_bak)
	}
	if strings.HasSuffix(rowterminator, "\r\n") { // replace ending \r\n by \n
		rowterminator = rowterminator[:len(rowterminator)-2] + "\n"
	}
	if strings.HasSuffix(rowterminator, "\n") == false { // rowterminator must end with \n
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_ROWTERMINATOR_NO_NL, rsql.ERROR_BATCH_ABORT, rowterminator_bak)
	}

	if firstrow == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_FIRSTROW_CANNOT_BE_ZERO, rsql.ERROR_BATCH_ABORT)
	}

	if keepnulls == false {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_KEEPNULLS_IS_MANDATORY, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_bulk_insert.Bi_opt_codepage = codepage
	token_stmt_bulk_insert.Bi_opt_rowterminator = rowterminator
	token_stmt_bulk_insert.Bi_opt_fieldterminator = fieldterminator
	token_stmt_bulk_insert.Bi_opt_firstrow = uint64(firstrow)
	token_stmt_bulk_insert.Bi_opt_lastrow = uint64(lastrow)
	token_stmt_bulk_insert.Bi_opt_keepidentity = keepidentity
	token_stmt_bulk_insert.Bi_opt_newidentity = newidentity
	token_stmt_bulk_insert.Bi_opt_rtrim = rtrim

	// put in Registration

	parser.Registration.Register_object_qname(token_stmt_bulk_insert.Bi_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_INSERT)

	return token_stmt_bulk_insert, nil
}
