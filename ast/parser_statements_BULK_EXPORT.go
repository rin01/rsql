package ast

import (
	"strconv"
	"strings"

	"rsql"
	"rsql/dict"
	"rsql/lex"
	"rsql/lk"
)

// parse BULK EXPORT statement.
//
// This statement doesn't exist in MS SQL Server.
//
//    BULK EXPORT
//       { [ database_name . [ schema_name ] . | schema_name . ] table_name } | { select_expression }
//          TO 'data_file'
//         [ WITH
//       (
//       [ [ , ] CODEPAGE = { 'ACP' | 'RAW' | 'code_page' } ]
//       [ [ , ] FIELDTERMINATOR = 'field_terminator' ]
//       [ [ , ] ROWTERMINATOR = 'row_terminator' ]
//       [ [ , ] DATE_FORMAT = 'date_format' ]
//       [ [ , ] TIME_FORMAT = 'time_format' ]
//       [ [ , ] DATETIME_FORMAT = 'datetime_format' ]
//       )]
//
//
//    'data_file'
//          name of output data file. NULL is converted to empty field. For VARCHAR, empty string is converted to 0x00 (CHAR field is never empty, it always contains spaces).
//
//    CODEPAGE = { 'ACP' | 'code_page' }
//          ACP  is Microsoft windows1252 encoding, which is a superset of iso-8859-1
//          'code_page' is e.g. 'utf8', 'UTF-8', 'iso8859-1', 'iso-8859-1', 'windows1252', etc. code_page is case insensitive and quite lenient about spelling.
//          By default, codepage is utf8.
//
//    FIELDTERMINATOR ='field_terminator'
//          Specifies the field terminator to be used for text data file. By default, it is '\t'.
//
//    ROWTERMINATOR ='row_terminator'
//          Specifies the row terminator to be used for text data file. By default, it is '\n'. Row terminator must be terminated by \n or \r\n.
//
//    DATE_FORMAT ='date_format'
//          Format for DATE. Default is "yyyyMMdd"
//
//    TIME_FORMAT ='time_format'
//          Format for TIME. Default is "HH:mm:ss.FFFFFFFFF"
//
//    DATETIME_FORMAT ='datetime_format'
//          Format for DATETIME. Default is "yyyyMMdd HH:mm:ss.FFFFFFFFF"
//
//    Conversion of fields for    BULD INSERT: table --> text file     or    BULK EXPORT: table --> text file     are as follows:
//
//    table                              text file
//    =====                              =========
//
//    VARCHAR:
//    -------------------
//        NULL                <-->       empty field
//        empty string        <-->       0x00
//        one or morespaces   <-->       one or more spaces
//        hello               <-->       hello
//
//    INT:
//    -------------------
//        NULL                <-->       empty field
//        NULL                <--        one or more spaces
//        0                   <-->       0
//        123                 <-->       123
//
//    DATETIME:
//    -------------------
//        NULL                <-->       empty field
//        NULL                <--        one or more spaces
//        2000.02.03          <-->       2000.02.03
//
func (parser *Parser) get_statement_BULK_EXPORT() (*Token_stmt_BULK_EXPORT, *rsql.Error) {
	var (
		err                    error
		rsql_err               *rsql.Error
		table_qname            rsql.Object_qname_t
		token_stmt_bulk_export *Token_stmt_BULK_EXPORT
		filename               string
		option                 string
		codepage               string
		fieldterminator        string
		rowterminator          string
		date_format            string
		time_format            string
		datetime_format        string
		xtable                 Xtable
		lock_qname_flag        bool
		orderby_clause         []*Orderby_part
	)

	assert(parser.Current_lexeme.Is_auxword(lex.AUXWORD_EXPORT) == true)

	if rsql_err := parser.Check_compatibility(COMPAT_BULK_EXPORT); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_BULK_EXPORT

	token_stmt_bulk_export = &Token_stmt_BULK_EXPORT{}
	parser.token_init(&token_stmt_bulk_export.Token, TOK_STMT_BULK_EXPORT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip EXPORT
		return nil, rsql_err
	}

	// eat SELECT expression or table name

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_SELECT:
		if xtable, orderby_clause, rsql_err = parser.Eat_xtable_select_or_union(); rsql_err != nil {
			return nil, rsql_err
		}

		if TOP_value_exists(xtable) == true {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOP_NOT_ALLOWED_IN_BULK_EXPORT, rsql.ERROR_BATCH_ABORT)
		}

		if orderby_clause != nil {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_NOT_ALLOWED_IN_BULK_EXPORT, rsql.ERROR_BATCH_ABORT)
		}

	default:
		var gtabledef *rsql.GTabledef

		if table_qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
			return nil, rsql_err
		}

		if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(table_qname); rsql_err != nil {
			return nil, rsql_err
		}

		lock_qname_flag = true

		xtable = &Xtable_table{
			Tbl_qname: table_qname,
			Tbl_alias: "",

			Tbl_gtabledef: gtabledef,
		}
	}

	token_stmt_bulk_export.Be_xtable = xtable

	// eat TO 'data_file'

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_TO); rsql_err != nil {
		return nil, rsql_err
	}

	if filename, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat 'data_file', which is a literal string in original case
		return nil, rsql_err
	}

	filename = strings.TrimSpace(filename)
	if len(filename) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EMPTY_FILENAME, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_bulk_export.Be_filename = filename

	// eat WITH options, if any

	codepage = "utf8"
	rowterminator = "\\n" // rowterminator must end with \n. Both \n and \r\n are considered to terminate lines.
	fieldterminator = "\\t"

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect left parenthese
			return nil, rsql_err
		}

		for {
			if option, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat option
				return nil, rsql_err
			}

			switch lex.Auxword_t(option) { // process option
			case lex.AUXWORD_CODEPAGE: // === CODEPAGE option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if codepage, rsql_err = parser.Eat_literal_stringval_lowercase(); rsql_err != nil { // eat codepage, which is a literal string in lower case
					return nil, rsql_err
				}

			case lex.AUXWORD_ROWTERMINATOR: // === ROWTERMINATOR option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if rowterminator, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat rowterminator, which is a literal string in original case
					return nil, rsql_err
				}

			case lex.AUXWORD_FIELDTERMINATOR: // === FIELDTERMINATOR option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if fieldterminator, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat fieldterminator, which is a literal string in original case
					return nil, rsql_err
				}

			case lex.AUXWORD_DATE_FORMAT: // === DATE_FORMAT option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if date_format, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat date_format, which is a literal string in original case
					return nil, rsql_err
				}

			case lex.AUXWORD_TIME_FORMAT: // === TIME_FORMAT option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if time_format, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat time_format, which is a literal string in original case
					return nil, rsql_err
				}

			case lex.AUXWORD_DATETIME_FORMAT: // === DATETIME_FORMAT option ===
				if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
					return nil, rsql_err
				}

				if datetime_format, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat datetime_format, which is a literal string in original case
					return nil, rsql_err
				}

			default: // === invalid option ===
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPTION_UNKNOWN, rsql.ERROR_BATCH_ABORT, option)
			}

			if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more option ==> exit loop
				break
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip COMMA
				return nil, rsql_err
			}
		} // end loop

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
			return nil, rsql_err
		}
	}

	// check options

	if len(fieldterminator) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_FIELDTERMINATOR, rsql.ERROR_BATCH_ABORT, fieldterminator)
	}
	if fieldterminator, err = strconv.Unquote(`"` + fieldterminator + `"`); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_FIELDTERMINATOR, rsql.ERROR_BATCH_ABORT, fieldterminator)
	}

	rowterminator_bak := rowterminator
	if rowterminator, err = strconv.Unquote(`"` + rowterminator + `"`); err != nil {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_ROWTERMINATOR, rsql.ERROR_BATCH_ABORT, rowterminator_bak)
	}
	if strings.HasSuffix(rowterminator, "\r\n") { // replace ending \r\n by \n
		rowterminator = rowterminator[:len(rowterminator)-2] + "\n"
	}
	if strings.HasSuffix(rowterminator, "\n") == false { // rowterminator must end with \n
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_BAD_ROWTERMINATOR_NO_NL, rsql.ERROR_BATCH_ABORT, rowterminator_bak)
	}

	if date_format == "" {
		date_format = "yyyyMMdd"
	}

	if time_format == "" {
		time_format = "HH:mm:ss.FFFFFFFFF"
	}

	if datetime_format == "" {
		datetime_format = "yyyyMMdd HH:mm:ss.FFFFFFFFF"
	}

	token_stmt_bulk_export.Be_opt_codepage = codepage
	token_stmt_bulk_export.Be_opt_rowterminator = rowterminator
	token_stmt_bulk_export.Be_opt_fieldterminator = fieldterminator
	token_stmt_bulk_export.Be_opt_date_format = date_format
	token_stmt_bulk_export.Be_opt_time_format = time_format
	token_stmt_bulk_export.Be_opt_datetime_format = datetime_format

	// put in Registration

	if lock_qname_flag { // if BULK EXPORT table, lock the table
		parser.Registration.Register_object_qname(table_qname, lk.LOCK_SHARED, rsql.PERMISSION_SELECT)
	}

	return token_stmt_bulk_export, nil
}
