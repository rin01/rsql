package ast

import (
	"rsql"
	"rsql/lex"
)

// get_statement_USE parses USE statement.
//
func (parser *Parser) get_statement_USE() (*Token_stmt_USE, *rsql.Error) {
	var (
		rsql_err       *rsql.Error
		database_name  string
		token_stmt_use *Token_stmt_USE
	)

	token_stmt_use = &Token_stmt_USE{}
	parser.token_init(&token_stmt_use.Token, TOK_STMT_USE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip USE
		return nil, rsql_err
	}

	/*==== eat database name ====*/

	if database_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat database name
		return nil, rsql_err
	}

	token_stmt_use.Use_database_name = database_name

	// update parser's current database

	parser.parser_current_default_database = database_name

	return token_stmt_use, nil
}

// get_statement_THROW parses THROW statement.
//
//      THROW 'message', severity, state
//
//            error_number      any INT expression. You can put 50000, as it is a usual value for this parameter.
//            'message'         any string
//            state             any INT expression for your own use.
//
func (parser *Parser) get_statement_THROW() (*Token_stmt_THROW, *rsql.Error) {
	var (
		rsql_err           *rsql.Error
		throw_error_number *Token_primary
		message            *Token_primary
		state              *Token_primary
	)

	token_stmt_throw := &Token_stmt_THROW{}
	parser.token_init(&token_stmt_throw.Token, TOK_STMT_THROW)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip THROW
		return nil, rsql_err
	}

	if throw_error_number, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat error_number
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_punctuation(lex.LEX_COMMA); rsql_err != nil { // expect comma
		return nil, rsql_err
	}

	if message, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat message
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_punctuation(lex.LEX_COMMA); rsql_err != nil { // expect comma
		return nil, rsql_err
	}

	if state, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat state
		return nil, rsql_err
	}

	token_stmt_throw.Th_error_number = throw_error_number
	token_stmt_throw.Th_message = message
	token_stmt_throw.Th_severity = parser.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(16) // like MS SQL Server
	token_stmt_throw.Th_state = state

	return token_stmt_throw, nil
}

func (parser *Parser) get_statement_PRINT() (*Token_stmt_PRINT, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		listexpr []*Token_primary
	)

	token_stmt_print := &Token_stmt_PRINT{}
	parser.token_init(&token_stmt_print.Token, TOK_STMT_PRINT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip PRINT
		return nil, rsql_err
	}

	if listexpr, rsql_err = parser.Eat_list_of_function_arguments(); rsql_err != nil { // eat list of expressions
		return nil, rsql_err
	}

	token_stmt_print.Pr_listexpr = listexpr

	return token_stmt_print, nil
}

// get_statement_SET_LEXER_OR_PARSER_OPTION parses "SET option" statements, e.g. SET PARSEONLY ON, SET PARSEONLY OFF, SET NOEXEC ON, SET QUOTED_IDENTIFIER ON, etc.
//
// This instruction is a pragma for lexer or parser. It is not executed by the VM, no instruction is emitted.
//
//      Available options are :
//          - SET QUOTED_IDENTIFIER ON/OFF    identifier may be quoted by double-quotes
//          - SET ANSI_NULL_DFLT_ON ON/OFF    for CREATE TABLE, column without NULL/NOT NULL clause are NULL.
//          - SET PARSEONLY ON/OFF            only parse the batch. No execution.
//          - SET NOEXEC ON/OFF               same as PARSEONLY
//
func (parser *Parser) get_statement_SET_LEXER_OR_PARSER_OPTION(auxword lex.Auxword_t) (*Token_stmt_SET_LEXER_OR_PARSER_OPTION, *rsql.Error) {
	var (
		rsql_err              *rsql.Error
		token_stmt_set_option *Token_stmt_SET_LEXER_OR_PARSER_OPTION
		option                Options_t
		command               bool
	)

	token_stmt_set_option = &Token_stmt_SET_LEXER_OR_PARSER_OPTION{}
	parser.token_init(&token_stmt_set_option.Token, TOK_STMT_SET_LEXER_OR_PARSER_OPTION)

	// read option

	switch auxword {
	case lex.AUXWORD_QUOTED_IDENTIFIER: // pragma for lexer use only
		option = OPTION_QUOTED_IDENTIFIER

	case lex.AUXWORD_ANSI_NULL_DFLT_ON: // pragma for parser use only
		option = OPTION_ANSI_NULL_DFLT_ON

	case lex.AUXWORD_PARSEONLY: // pragma for caller's use only
		option = OPTION_PARSEONLY

	case lex.AUXWORD_NOEXEC: // pragma for caller's use only
		option = OPTION_NOEXEC

	default:
		panic("unexpected SET option")
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip option PARSEONLY, NOEXEC, QUOTED_IDENTIFIER, etc
		return nil, rsql_err
	}

	// eat ON or OFF

	if command, rsql_err = parser.Eat_keyword_ON_OFF(); rsql_err != nil {
		return nil, rsql_err
	}

	// set option

	if option == OPTION_QUOTED_IDENTIFIER { // option for lexer
		parser.Lexer.Set_option_quoted_identifier(command)
	}

	switch command {
	case true:
		parser.Options |= option

	default:
		parser.Options &^= option
	}

	return token_stmt_set_option, nil
}

func (parser *Parser) get_statement_BEGIN_TRANSACTION() (*Token_stmt_BEGIN_TRANSACTION, *rsql.Error) {

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TRAN || parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TRANSACTION)

	token_stmt_begin_transaction := &Token_stmt_BEGIN_TRANSACTION{}
	parser.token_init(&token_stmt_begin_transaction.Token, TOK_STMT_BEGIN_TRANSACTION)

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip TRAN or TRANSACTION keyword
		return nil, rsql_err
	}

	return token_stmt_begin_transaction, nil
}

func (parser *Parser) get_statement_COMMIT_TRANSACTION() (*Token_stmt_COMMIT_TRANSACTION, *rsql.Error) {

	token_stmt_commit_transaction := &Token_stmt_COMMIT_TRANSACTION{}
	parser.token_init(&token_stmt_commit_transaction.Token, TOK_STMT_COMMIT_TRANSACTION)

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip COMMIT
		return nil, rsql_err
	}

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TRAN || parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TRANSACTION { // skip optional TRAN or TRANSACTION keyword
		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
			return nil, rsql_err
		}
	}

	return token_stmt_commit_transaction, nil
}

func (parser *Parser) get_statement_ROLLBACK_TRANSACTION() (*Token_stmt_ROLLBACK_TRANSACTION, *rsql.Error) {

	token_stmt_rollback_transaction := &Token_stmt_ROLLBACK_TRANSACTION{}
	parser.token_init(&token_stmt_rollback_transaction.Token, TOK_STMT_ROLLBACK_TRANSACTION)

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip COMMIT
		return nil, rsql_err
	}

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TRAN || parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TRANSACTION { // skip optional TRAN or TRANSACTION keyword
		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
			return nil, rsql_err
		}
	}

	return token_stmt_rollback_transaction, nil
}
