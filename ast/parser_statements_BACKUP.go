package ast

import (
	"strings"

	"rsql"
	"rsql/lex"
	"rsql/lk"
)

// parse BACKUP DATABASE statement.
//
//    BACKUP DATABASE database_name
//      TO DISK =  'file_name'
//
// Example:
//
//    BACKUP DATABASE mydb
//      TO DISK = '/Backups/mydb.rbak'
//
func (parser *Parser) get_statement_BACKUP() (*Token_stmt_BACKUP, *rsql.Error) {
	var (
		rsql_err      *rsql.Error
		database_name string
		filename      string
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_BACKUP)

	if rsql_err := parser.Check_compatibility(COMPAT_BACKUP); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip BACKUP
		return nil, rsql_err
	}

	token_stmt_backup := &Token_stmt_BACKUP{}
	parser.token_init(&token_stmt_backup.Token, TOK_STMT_BACKUP)

	token_stmt_backup.Bk_server_default_collation = parser.Server_default_collation

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_DATABASE); rsql_err != nil { // eat DATABASE
		return nil, rsql_err
	}

	if database_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat database name
		return nil, rsql_err
	}

	token_stmt_backup.Bk_database_name = database_name

	// eat TO DISK = 'filename'

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_TO); rsql_err != nil { // eat TO
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_DISK); rsql_err != nil { // eat DISK
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // eat '=' operator
		return nil, rsql_err
	}

	if filename, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat 'filename', which is a literal string in original case
		return nil, rsql_err
	}

	filename = strings.TrimSpace(filename)
	if len(filename) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_EMPTY_FILENAME, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_backup.Bk_filename = filename

	// put in Registration

	parser.Registration.Register_db(database_name, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_DATABASE) // we want same exclusive lock as ALTER or DROP DATABASE

	return token_stmt_backup, nil
}
