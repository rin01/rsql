package ast

import (
	"strconv"
	"strings"

	"rsql"
	"rsql/lang"
	"rsql/lex"
)

func assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

//***************************************************************************
//                                                                          *
//               Token_primary creation and transformation                  *
//                                                                          *
//***************************************************************************

// Create_TOK_PRIM_LITERAL_SYSCOLLATOR creates a literal SYSCOLLATOR token from a collation name.
//
func (parser *Parser) Create_TOK_PRIM_LITERAL_SYSCOLLATOR(collation_name string) *Token_primary {

	lexeme := lex.Create_lexeme(lex.LEX_IDENTPART, lex.LEX_IDENTPART_SUB, collation_name)

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_SYSCOLLATOR, TOK_PRIM_LITERAL_SYSCOLLATOR_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary
}

// Create_TOK_PRIM_LITERAL_NULL creates a 'literal NULL' token.
//
func (parser *Parser) Create_TOK_PRIM_LITERAL_NULL() *Token_primary {

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_NULL, TOK_PRIM_LITERAL_NULL_SUB, lex.LEXEME_KEYWORD_NULL)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary
}

// Create_TOK_PRIM_LITERAL_BOOLEAN creates a 'literal boolean' token from a value.
//
func (parser *Parser) Create_TOK_PRIM_LITERAL_BOOLEAN(val bool) *Token_primary {
	var lexeme lex.Lexeme

	lexeme = lex.LEXEME_DISPLAYWORD_FALSE
	if val == true {
		lexeme = lex.LEXEME_DISPLAYWORD_TRUE
	}

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_BOOLEAN, TOK_PRIM_LITERAL_BOOLEAN_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary
}

// Create_TOK_PRIM_LITERAL_STRING creates a 'literal string' token from a value.
//
func (parser *Parser) Create_TOK_PRIM_LITERAL_STRING(val string) *Token_primary {
	var lexeme lex.Lexeme

	lexeme = lex.Create_lexeme(lex.LEX_LITERAL_STRING, lex.LEX_LITERAL_STRING_SUB, val)

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_STRING, TOK_PRIM_LITERAL_STRING_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary
}

// Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL creates a 'literal number' token from a number.
//
func (parser *Parser) Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(number int32) *Token_primary {
	var lexeme lex.Lexeme

	s := strconv.Itoa(int(number))

	lexeme = lex.Create_lexeme(lex.LEX_LITERAL_NUMBER, lex.LEX_LITERAL_NUMBER_INTEGRAL, s)

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_NUMBER, TOK_PRIM_LITERAL_NUMBER_INTEGRAL, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary
}

// Create_TOK_PRIM_VARIABLE creates a 'variable' token from a lexeme.
// E.g. var_name may be LEXEME_SYSVAR_CURRENT_LANGUAGE, for the "_@current_language" variable.
//
func (parser *Parser) Create_TOK_PRIM_VARIABLE(var_name lex.Lexeme) *Token_primary {
	assert(var_name.Lex_type == lex.LEX_VARIABLE)

	token_primary := parser.New_token_primary(TOK_PRIM_VARIABLE, TOK_PRIM_VARIABLE_SUB, var_name)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary
}

// Create_TOK_PRIM_SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE creates a SYSFUNC token with SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE function.
//
func (parser *Parser) Create_TOK_PRIM_SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE(expression *Token_primary, escape_char *Token_primary) *Token_primary {

	token_primary := parser.New_token_primary(TOK_PRIM_SYSFUNC, TOK_PRIM_SYSFUNC_SUB, lex.LEXEME_DISPLAYWORD_COMPILE_VARCHAR_TO_REGEXPLIKE)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE
	token_primary.Prim_sysfunc_descr = SYSFUNC_DESCR_COMPILE_VARCHAR_TO_REGEXPLIKE

	token_primary.Prim_listexpr = []*Token_primary{expression, escape_char}

	return token_primary
}

// Create_TOK_PRIM_SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE creates a SYSFUNC token with SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE function.
//
func (parser *Parser) Create_TOK_PRIM_SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE(syslanguage *Token_primary, firstdayofweek *Token_primary) *Token_primary {

	token_primary := parser.New_token_primary(TOK_PRIM_SYSFUNC, TOK_PRIM_SYSFUNC_SUB, lex.LEXEME_DISPLAYWORD_FIRSTDAYOFWEEK)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE
	token_primary.Prim_sysfunc_descr = SYSFUNC_DESCR_FIRSTDAYOFWEEK_SYSLANGUAGE

	token_primary.Prim_listexpr = []*Token_primary{syslanguage, firstdayofweek}

	return token_primary
}

// Create_TOK_PRIM_SYSFUNC_DMY_SYSLANGUAGE creates a SYSFUNC token with SYSFUNC_DMY_SYSLANGUAGE function.
//
func (parser *Parser) Create_TOK_PRIM_SYSFUNC_DMY_SYSLANGUAGE(syslanguage *Token_primary, dmy *Token_primary) *Token_primary {

	token_primary := parser.New_token_primary(TOK_PRIM_SYSFUNC, TOK_PRIM_SYSFUNC_SUB, lex.LEXEME_DISPLAYWORD_DMY)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE
	token_primary.Prim_sysfunc_descr = SYSFUNC_DESCR_DMY_SYSLANGUAGE

	token_primary.Prim_listexpr = []*Token_primary{syslanguage, dmy}

	return token_primary
}

// Create_TOK_PRIM_SYSFUNC_GETUTCDATE creates a SYSFUNC token with SYSFUNC_GETUTCDATE function.
//
func (parser *Parser) Create_TOK_PRIM_SYSFUNC_GETUTCDATE() *Token_primary {

	lexeme := lex.Create_lexeme(lex.LEX_IDENTPART, lex.LEX_IDENTPART_SUB, "getutcdate")
	token_primary := parser.New_token_primary(TOK_PRIM_SYSFUNC, TOK_PRIM_SYSFUNC_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE
	token_primary.Prim_sysfunc_descr = SYSFUNC_DESCR_GETUTCDATE

	return token_primary
}

// Create_TOK_PRIM_OPERATOR_COMP creates a TOK_PRIM_OPERATOR_COMP_EQUAL, _GREATER, etc token, with tok_a and tok_b as operands.
//
func (parser *Parser) Create_TOK_PRIM_OPERATOR_COMP(prim_subtype Prim_subtype_t, tok_a *Token_primary, tok_b *Token_primary) *Token_primary {
	var (
		token_op *Token_primary
		lexeme   lex.Lexeme
	)

	switch prim_subtype {
	case TOK_PRIM_OPERATOR_COMP_EQUAL:
		lexeme = lex.LEXEME_OPERATOR_COMP_EQUAL

	case TOK_PRIM_OPERATOR_COMP_GREATER:
		lexeme = lex.LEXEME_OPERATOR_COMP_GREATER

	case TOK_PRIM_OPERATOR_COMP_LESS:
		lexeme = lex.LEXEME_OPERATOR_COMP_LESS

	case TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL:
		lexeme = lex.LEXEME_OPERATOR_COMP_GREATER_EQUAL

	case TOK_PRIM_OPERATOR_COMP_LESS_EQUAL:
		lexeme = lex.LEXEME_OPERATOR_COMP_LESS_EQUAL

	case TOK_PRIM_OPERATOR_COMP_NOT_EQUAL:
		lexeme = lex.LEXEME_OPERATOR_COMP_NOT_EQUAL

	default:
		panic("impossible")
	}

	token_op = parser.New_token_primary(TOK_PRIM_OPERATOR, prim_subtype, lexeme)
	token_op.Tok_batch_line = tok_a.Tok_batch_line
	token_op.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON
	token_op.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT
	token_op.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS

	token_op.Prim_left = tok_a
	token_op.Prim_right = tok_b
	token_op.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_op
}

// transform_YEAR_MONTH_DAY_to_DATEPART transforms YEAR, MONTH, or DAY function into DATEPART, in-place.
//
func (parser *Parser) transform_YEAR_MONTH_DAY_to_DATEPART(tok *Token_primary) {
	var (
		sysfunc_id Sysfunc_id_t
	)

	sysfunc_id = tok.Prim_sysfunc_descr.Sd_id

	switch sysfunc_id {
	case SYSFUNC_YEAR:
		tok.Prim_sql_datepartfield = DATEPARTFIELD_YEAR
	case SYSFUNC_MONTH:
		tok.Prim_sql_datepartfield = DATEPARTFIELD_MONTH
	case SYSFUNC_DAY:
		tok.Prim_sql_datepartfield = DATEPARTFIELD_DAY
	default:
		panic("impossible")
	}

	tok.Prim_name = "datepart"
	tok.Prim_sysfunc_descr = SYSFUNC_DESCR_DATEPART
}

// transform_NULLIF_to_IIF transforms NULLIF into IIF, in-place, when the parser is eating NULLIF function.
//
func (parser *Parser) transform_NULLIF_to_IIF(tok_nullif *Token_primary) *rsql.Error {
	var (
		rsql_err    *rsql.Error
		listexpr    []*Token_primary
		tok_a       *Token_primary
		tok_b       *Token_primary
		tok_equal   *Token_primary
		tok_a_clone *Token_primary
		tok_NULL    *Token_primary
	)

	assert(tok_nullif.Prim_sysfunc_descr == SYSFUNC_DESCR_NULLIF)

	listexpr = tok_nullif.Prim_listexpr

	if len(listexpr) != 2 {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok_nullif.Prim_name, 2)
	}

	tok_a = listexpr[0]
	tok_b = listexpr[1]

	tok_equal = parser.Create_TOK_PRIM_OPERATOR_COMP(TOK_PRIM_OPERATOR_COMP_EQUAL, tok_a, tok_b)

	if tok_a_clone, rsql_err = Clone(tok_a); rsql_err != nil {
		return rsql_err
	}

	tok_NULL = parser.Create_TOK_PRIM_LITERAL_NULL()
	tok_NULL.Tok_batch_line = tok_b.Tok_batch_line

	tok_nullif.Prim_name = "iif"
	tok_nullif.Prim_status = TOK_PRIM_STATUS_COMPLETE
	tok_nullif.Prim_sysfunc_descr = SYSFUNC_DESCR_IIF

	tok_nullif.Prim_listexpr = append(tok_nullif.Prim_listexpr[:0], tok_equal, tok_NULL, tok_a_clone)

	return nil
}

// transform_AVG_to_SUM_div_COUNT transforms AVG(X) into SUM(x)/COUNT(x).
//
func (parser *Parser) transform_AVG_to_SUM_div_COUNT(tok_avg *Token_primary) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		tok_arg_clone *Token_primary
	)

	assert(tok_avg.Prim_sysfunc_descr.Sd_id == SYSFUNC_AGGR_AVG)

	if len(tok_avg.Prim_listexpr) != 1 {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ARGS_COUNT, rsql.ERROR_BATCH_ABORT, tok_avg.Prim_name, 1)
	}

	// create SUM(x) token

	tok_sum := &Token_primary{}
	*tok_sum = *tok_avg
	tok_sum.Prim_name = "sum"
	tok_sum.Prim_status = TOK_PRIM_STATUS_COMPLETE
	tok_sum.Prim_sysfunc_descr = SYSFUNC_DESCR_AGGR_SUM
	tok_sum.Prim_sql_collate = ""

	// create COUNT(x) token

	tok_count := &Token_primary{}
	*tok_count = *tok_avg
	tok_count.Prim_name = "count"
	tok_count.Prim_status = TOK_PRIM_STATUS_COMPLETE
	tok_count.Prim_sysfunc_descr = SYSFUNC_DESCR_AGGR_COUNT
	tok_count.Prim_sql_collate = ""

	if tok_arg_clone, rsql_err = Clone(tok_sum.Prim_listexpr[0]); rsql_err != nil {
		return rsql_err
	}
	tok_count.Prim_listexpr = []*Token_primary{tok_arg_clone}

	// transform AVG token into division token

	tok_avg.Prim_type = TOK_PRIM_OPERATOR
	tok_avg.Prim_subtype = TOK_PRIM_OPERATOR_DIV
	tok_avg.Prim_name = "/"
	tok_avg.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_MULTIPLY
	tok_avg.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT
	tok_avg.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS
	tok_avg.Prim_sysfunc_descr = nil

	tok_avg.Prim_left = tok_sum
	tok_avg.Prim_right = tok_count
	tok_avg.Prim_listexpr = nil
	tok_avg.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return nil
}

// transform_token_primary_to_negative_literal_number transforms a primary token for literal number into a token with negative sign.
//
// NOTE: the current lexeme MUST be TOK_PRIM_LITERAL_NUMBER_FLOAT, TOK_PRIM_LITERAL_NUMBER_NUMERIC or TOK_PRIM_LITERAL_NUMBER_INTEGRAL, but not MONEY which already includes the sign.
//
// This function just add "-" in front of token_primary.Prim_name.
// No new token is created, it is just transformed.
//
// Used by parser.Eat_expression().
//
func (parser *Parser) transform_token_primary_to_negative_literal_number(token_primary *Token_primary) {

	assert(token_primary.Prim_subtype == TOK_PRIM_LITERAL_NUMBER_FLOAT ||
		token_primary.Prim_subtype == TOK_PRIM_LITERAL_NUMBER_NUMERIC ||
		token_primary.Prim_subtype == TOK_PRIM_LITERAL_NUMBER_INTEGRAL)

	assert(token_primary.Prim_name[0] != '-') // negative sign not allowed

	/* substitutes the negative literal number as name for the token */

	token_primary.Prim_name = "-" + token_primary.Prim_name
}

//***************************************************************************
//                                                                          *
//                             eat object name                              *
//                                                                          *
//***************************************************************************

// probe_for_qualified_star returns true if current lexeme and following lexemes can make up a qualified star.
//
// It detects a sequence of '.', optionally preceded by an identpart, and followed by a star.
// This function returns true also for invalid qualified star, e.g. if there are too many parts in the prefix, or the star is followed by a dot.
//
func (parser *Parser) probe_for_qualified_star() (bool, *rsql.Error) {
	var (
		rsql_err   *rsql.Error
		parser_bak Parser
		result     bool
	)

	if parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT) == 0 {
		return false, nil
	}

	if parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && parser.Rune0 != '.' {
		return false, nil
	}

	// save current state of parser (current position, etc)

	parser_bak = *parser

	// eat series of dots, optionally preceded by an identpart

	for {
		if parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip identpart if any
				return false, rsql_err
			}
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_DOT {
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip dot
			return false, rsql_err
		}

		if parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_MULT {
			result = true
			break
		}
	}

	// restore parser state

	*parser = parser_bak

	return result, nil
}

// Eat_prefix reads all lexemes making up the prefix of a qualified name.
//
//     MYDB..CUSTOMERS.NAME  returns the parts [MYDB, "", CUSTOMERS]       nb_prefix_parts == 3
//     ..CUSTOMERS.NAME      returns the parts ["", "", CUSTOMERS]         nb_prefix_parts == 3
//     ...NAME               returns the parts ["", "", ""]                nb_prefix_parts == 3
//     STREET                returns the parts []                          nb_prefix_parts == 0
//     MYTABLE.*             returns the parts [MYTABLE]                   nb_prefix_parts == 1
//     *                     NOT ALLOWED, panics.
//
// After the call, parser.Current_lexeme points to the last part of the qualified name, which must be an identifier or a star '*'.
//
// NOTE: current lexeme must be a LEX_IDENTPART or LEX_DOT.
//
func (parser *Parser) Eat_prefix() (name_3, name_2, name_1 string, nb_prefix_parts int, rsql_err *rsql.Error) {
	var (
		prefix_parts [SPEC_QUALIFIED_NAME_NUMBER_OF_PARTS_MAX - 1]string // db.schema.table. column,   or   db.schema. table,   or   db.schema. function
	)

	assert(parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT) != 0)

	lexeme := parser.Current_lexeme

	/* if no prefix part, return immediately */

	if lexeme.Lex_type == lex.LEX_IDENTPART && parser.Rune0 != '.' {
		return "", "", "", 0, nil
	}

	/* prefix exists. Read first identpart */

	switch {
	case lexeme.Lex_type == lex.LEX_IDENTPART: // === eat first prefix part
		prefix_parts[0] = lexeme.Lex_word
		nb_prefix_parts++
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip first identpart (explicit identpart)
			return "", "", "", 0, rsql_err
		}

	case lexeme.Lex_type == lex.LEX_DOT: // === or read dot (implicit identpart)
		nb_prefix_parts++

	default:
		panic("impossible")
	}

	// At this point, EXACTLY ONE identpart has been read (explicit or implicit identpart).

	/* iterate over pairs of <dot, ident> (ident can be missing and thus be implicit) */

LOOP:
	for {
		// === skip dot

		if parser.Current_lexeme.Lex_type != lex.LEX_DOT { // catch "mydb.4.mytable", as ".4" is a number lexeme, not a dot
			return "", "", "", 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_MALFORMED, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {
			return "", "", "", 0, rsql_err
		}

		lexeme = parser.Current_lexeme // it can be an ident, or a star, or the dot of the next <dot, ident> pair. Else, it is a syntax error.

		// === if lexeme is the last part of the qualified name or star, or number of prefix parts max is reached, break loop

		if (lexeme.Lex_subtype == lex.LEX_IDENTPART_SUB && parser.Rune0 != '.') ||
			(lexeme.Lex_subtype == lex.LEX_OPERATOR_MULT) ||
			(nb_prefix_parts >= SPEC_QUALIFIED_NAME_NUMBER_OF_PARTS_MAX-1) {
			break LOOP
		}

		// === put prefix part into array

		switch lexeme.Lex_subtype {
		case lex.LEX_IDENTPART_SUB: // == here, the lexeme is an explicit prefix part
			prefix_parts[nb_prefix_parts] = lexeme.Lex_word
			nb_prefix_parts++
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip prefix part
				return "", "", "", 0, rsql_err
			}

		case lex.LEX_DOT_SUB: // ==  or the lexeme is a dot (implicit prefix part)
			nb_prefix_parts++ //   we are already at the dot of the next <dot, ident> pair

		default: // == else, return an error
			return "", "", "", 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}
	}

	// === lexeme must be an identpart or a star

	if lexeme.Lex_subtype != lex.LEX_IDENTPART_SUB && lexeme.Lex_subtype != lex.LEX_OPERATOR_MULT {
		return "", "", "", 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	// === next lexeme cannot be a dot

	if parser.Rune0 == '.' {
		return "", "", "", 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_TOO_MANY_PARTS, rsql.ERROR_BATCH_ABORT)
	}

	/* fill out output variables */

	switch nb_prefix_parts {
	case 3:
		name_3 = prefix_parts[0]
		name_2 = prefix_parts[1]
		name_1 = prefix_parts[2]

	case 2:
		name_3 = ""
		name_2 = prefix_parts[0]
		name_1 = prefix_parts[1]

	case 1:
		name_3 = ""
		name_2 = ""
		name_1 = prefix_parts[0]

	default:
		panic("too many parts in prefix.")
	}

	return name_3, name_2, name_1, nb_prefix_parts, nil
}

// Eat_object_qname_parts eats a unqualified or qualified object name (table name, index name, function name, etc).
//
//       An object name is made of the following parts : db.schema.object
//
// The current lexeme must be LEX_IDENTPART or LEX_DOT. Else, an error is returned.
//
//       IMPORTANT: IF DATABASE NAME OR SCHEMA NAME ARE MISSING, THEY ARE EMPTY STRINGS.
//                  Use Eat_object_qname() if you want to set empty database and schema name to default server values.
//
func (parser *Parser) Eat_object_qname_parts() (database_name string, schema_name string, object_name string, object_name_original string, rsql_err *rsql.Error) {
	var (
		dummy           string
		nb_prefix_parts int
	)

	/* eat unqualified or qualified object name */

	if parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT) == 0 {
		return "", "", "", "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OBJECT_NAME_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if dummy, database_name, schema_name, nb_prefix_parts, rsql_err = parser.Eat_prefix(); rsql_err != nil { // eat prefix if any
		return "", "", "", "", rsql_err
	}

	if nb_prefix_parts+1 > SPEC_OBJECT_NAME_NUMBER_OF_PARTS { //   server.db.schema  .object
		return "", "", "", "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OBJECT_TOO_MANY_PARTS, rsql.ERROR_BATCH_ABORT)
	}

	assert(dummy == "")

	if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // last part of identifier must be idenpart
		return "", "", "", "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OBJECT_NAME_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	object_name = parser.Current_lexeme.Lex_word
	object_name_original = parser.Current_lexeme_original.Lex_word

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip object name
		return "", "", "", "", rsql_err
	}

	return database_name, schema_name, object_name, object_name_original, nil
}

// Eat_object_qname eats a unqualified or qualified object name (table name, index name, function name, etc).
//
//       An object name is made of the following parts : db.schema.object
//
// If database name or schema name are missing, they are filled with current database or schema name.
//
// The current lexeme must be LEX_IDENTPART or LEX_DOT. Else, an error is returned.
//
func (parser *Parser) Eat_object_qname() (qname rsql.Object_qname_t, rsql_err *rsql.Error) {
	var (
		database_name        string
		schema_name          string
		object_name          string
		object_name_original string
	)

	if database_name, schema_name, object_name, object_name_original, rsql_err = parser.Eat_object_qname_parts(); rsql_err != nil {
		return rsql.Object_qname_t{}, rsql_err
	}

	if database_name == "" { // if missing, complete database name
		database_name = parser.parser_current_default_database
	}

	if schema_name == "" { // if missing, complete schema name
		schema_name = parser.parser_current_default_schema
	}

	qname = rsql.Object_qname_t{Database_name: database_name, Schema_name: schema_name, Object_name: object_name, Object_name_original: object_name_original}

	return qname, nil
}

// Eat_variable_table_qname_parts eats a @vartable name.
//
func (parser *Parser) Eat_variable_table_qname_parts() (database_name string, schema_name string, object_name string, object_name_original string, rsql_err *rsql.Error) {
	var (
		vartable_name string
	)

	assert(parser.Current_lexeme.Lex_type == lex.LEX_VARIABLE)

	vartable_name = parser.Current_lexeme.Lex_word

	database_name = rsql.TRASHDB
	schema_name = "dbo"

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip @vartable name
		return "", "", "", "", rsql_err
	}

	return database_name, schema_name, vartable_name, vartable_name, nil
}

// Eat_variable_table_qname eats a @vartable name.
//
func (parser *Parser) Eat_variable_table_qname() (qname rsql.Object_qname_t, rsql_err *rsql.Error) {
	var (
		database_name        string
		schema_name          string
		object_name          string
		object_name_original string
	)

	if database_name, schema_name, object_name, object_name_original, rsql_err = parser.Eat_variable_table_qname_parts(); rsql_err != nil {
		return rsql.Object_qname_t{}, rsql_err
	}

	qname = rsql.Object_qname_t{Database_name: database_name, Schema_name: schema_name, Object_name: object_name, Object_name_original: object_name_original}

	return qname, nil
}

//***************************************************************************
//                                                                          *
//        eat punctuation, plain literal string, number, ON/OFF             *
//                                                                          *
//***************************************************************************

func (parser *Parser) Eat_equal_symbol() *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	if parser.Current_lexeme.Lex_subtype != lex.LEX_OPERATOR_COMP_EQUAL {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_EQUAL_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	// skip '='

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func (parser *Parser) Eat_punctuation(lex_type lex.Lex_type_t) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	switch lex_type {
	case lex.LEX_LPAREN:
		if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN { // expect lparen
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_COMMA:
		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // expect comma
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COMMA_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_RPAREN:
		if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_COLON:
		if parser.Current_lexeme.Lex_type != lex.LEX_COLON { // expect colon
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLON_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	default:
		panic("impossible")
	}

	// skip punctuation

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func (parser *Parser) Eat_keyword(lex_subtype lex.Lex_subtype_t) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	switch lex_subtype {
	case lex.LEX_KEYWORD_WITH:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_WITH { // expect WITH
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_WITH_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_NULL:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_NULL { // expect NULL
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_NULL_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_KEY:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_KEY { // expect KEY
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_KEY_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_SET:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_SET { // expect SET
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SET_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_ON:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_ON { // expect ON
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ON_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_TO:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_TO { // expect TO
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TO_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_COLUMN:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_COLUMN { // expect COLUMN
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_VALUES:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_VALUES { // expect VALUES
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_VALUES_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_FROM:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_FROM { // expect FROM
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FROM_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_JOIN:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_JOIN { // expect JOIN
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_JOIN_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_BY:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_BY { // expect BY
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BY_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_TABLE:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_TABLE { // expect TABLE
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_INDEX:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_INDEX { // expect INDEX
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INDEX_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_DATABASE:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_DATABASE { // expect DATABASE
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DATABASE_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_DISK:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_DISK { // expect DISK
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DISK_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_CREATE:
		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_CREATE { // expect CREATE
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CREATE_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	default:
		panic("impossible")
	}

	// skip keyword

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func (parser *Parser) Eat_literal_stringval() (string, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
		value    string
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_type != lex.LEX_LITERAL_STRING {
		return "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_STRING_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	value = lexeme.Lex_word

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return "", rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_literal_stringval_lowercase() (string, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
		value    string
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_type != lex.LEX_LITERAL_STRING {
		return "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_STRING_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	value = strings.ToLower(lexeme.Lex_word)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return "", rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_auxword(auxword lex.Auxword_t) *rsql.Error {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
	)

	lexeme = parser.Current_lexeme

	switch auxword {
	case lex.AUXWORD_MODIFY:
		if lexeme.Lex_word != string(lex.AUXWORD_MODIFY) { // expect MODIFY
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_XXX_AUXWORD_EXPECTED, rsql.ERROR_BATCH_ABORT, strings.ToUpper(string(lex.AUXWORD_MODIFY)))
		}

	case lex.AUXWORD_NAME:
		if lexeme.Lex_word != string(lex.AUXWORD_NAME) { // expect NAME
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_XXX_AUXWORD_EXPECTED, rsql.ERROR_BATCH_ABORT, strings.ToUpper(string(lex.AUXWORD_NAME)))
		}

	case lex.AUXWORD_LOGIN:
		if lexeme.Lex_word != string(lex.AUXWORD_LOGIN) { // expect LOGIN
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_XXX_AUXWORD_EXPECTED, rsql.ERROR_BATCH_ABORT, strings.ToUpper(string(lex.AUXWORD_LOGIN)))
		}

	case lex.AUXWORD_MEMBER:
		if lexeme.Lex_word != string(lex.AUXWORD_MEMBER) { // expect MEMBER
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_XXX_AUXWORD_EXPECTED, rsql.ERROR_BATCH_ABORT, strings.ToUpper(string(lex.AUXWORD_MEMBER)))
		}

	case lex.AUXWORD_SHRINK_FILE:
		if lexeme.Lex_word != string(lex.AUXWORD_SHRINK_FILE) { // expect SHRINK_FILE
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_XXX_AUXWORD_EXPECTED, rsql.ERROR_BATCH_ABORT, strings.ToUpper(string(lex.AUXWORD_SHRINK_FILE)))
		}

	case lex.AUXWORD_NOWAIT:
		if lexeme.Lex_word != string(lex.AUXWORD_NOWAIT) { // expect NOWAIT
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_XXX_AUXWORD_EXPECTED, rsql.ERROR_BATCH_ABORT, strings.ToUpper(string(lex.AUXWORD_NOWAIT)))
		}

	default:
		panic("impossible")
	}

	if lexeme.Lex_type != lex.LEX_IDENTPART {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_IDENTIFIER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	// skip auxword

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {
		return rsql_err
	}

	return nil
}

func (parser *Parser) Eat_identpart() (string, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_type != lex.LEX_IDENTPART {
		return "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_IDENTIFIER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return "", rsql_err
	}

	return lexeme.Lex_word, nil
}

func (parser *Parser) Eat_identpart_original() (ident string, ident_original string, rsql_err *rsql.Error) {
	var (
		lexeme          lex.Lexeme
		lexeme_original lex.Lexeme
	)

	lexeme = parser.Current_lexeme
	lexeme_original = parser.Current_lexeme_original

	if lexeme.Lex_type != lex.LEX_IDENTPART {
		return "", "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_IDENTIFIER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return "", "", rsql_err
	}

	return lexeme.Lex_word, lexeme_original.Lex_word, nil
}

func (parser *Parser) Eat_literal_stringval_or_identpart_lowercase() (string, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
		value    string
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_type&(lex.LEX_LITERAL_STRING|lex.LEX_IDENTPART) == 0 {
		return "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_STRING_OR_IDENTIFIER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	value = strings.ToLower(lexeme.Lex_word)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return "", rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_literal_integral_positive_int64() (int64, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		err      error
		lexeme   lex.Lexeme
		value    int64
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_subtype != lex.LEX_LITERAL_NUMBER_INTEGRAL {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_POSITIVE_INTEGER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if value, err = strconv.ParseInt(lexeme.Lex_word, 10, 64); err != nil {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_POSITIVE_INTEGER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return 0, rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_literal_integral_positive_int64_with_limits(lowest int64, highest int64) (int64, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		err      error
		lexeme   lex.Lexeme
		value    int64
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_subtype != lex.LEX_LITERAL_NUMBER_INTEGRAL {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_POSITIVE_INTEGER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if value, err = strconv.ParseInt(lexeme.Lex_word, 10, 64); err != nil {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_POSITIVE_INTEGER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if value < lowest || value > highest {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_POSITIVE_INTEGER_LIMITS, rsql.ERROR_BATCH_ABORT, lowest, highest)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return 0, rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_literal_integral_int64() (int64, *rsql.Error) {
	var (
		rsql_err           *rsql.Error
		err                error
		lexeme             lex.Lexeme
		negative_sign_flag bool
		value_str          string
		value              int64
	)

	lexeme = parser.Current_lexeme

	// eat '-' sign if any

	if lexeme.Lex_subtype == lex.LEX_OPERATOR_MINUS {
		negative_sign_flag = true

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip '-' sign
			return 0, rsql_err
		}
	}

	// eat number

	lexeme = parser.Current_lexeme

	if lexeme.Lex_subtype != lex.LEX_LITERAL_NUMBER_INTEGRAL {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_INTEGER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	value_str = lexeme.Lex_word
	if negative_sign_flag == true {
		value_str = "-" + value_str
	}

	if value, err = strconv.ParseInt(value_str, 10, 64); err != nil {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_INTEGER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip number
		return 0, rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_literal_positive_float64() (float64, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		err      error
		lexeme   lex.Lexeme
		value    float64
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_subtype != lex.LEX_LITERAL_NUMBER_INTEGRAL && lexeme.Lex_subtype != lex.LEX_LITERAL_NUMBER_NUMERIC && lexeme.Lex_subtype != lex.LEX_LITERAL_NUMBER_FLOAT {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_POSITIVE_FLOAT_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if value, err = strconv.ParseFloat(lexeme.Lex_word, 64); err != nil {
		return 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LITERAL_POSITIVE_FLOAT_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return 0, rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_keyword_ON_OFF() (bool, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
		value    bool
	)

	lexeme = parser.Current_lexeme

	switch lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_ON:
		value = true

	case lex.LEX_KEYWORD_OFF:
		value = false

	default:
		return false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_KEYWORD_ON_OFF_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return false, rsql_err
	}

	return value, nil
}

func (parser *Parser) Eat_keyword_TRUE_FALSE() (bool, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
		value    bool
	)

	lexeme = parser.Current_lexeme

	switch lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_TRUE:
		value = true

	case lex.LEX_KEYWORD_FALSE:
		value = false

	default:
		return false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_KEYWORD_TRUE_FALSE_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return false, rsql_err
	}

	return value, nil
}

// Eat_identpart_collation_normalized returns a normalized collation if collation is supported, or an error.
// Collation must end with "_ci_ai", "_ci_as" or "_cs_as", e.g. "fr_cs_as" or "fr_ca_cs_as".
//
// Supported collations are listed in G_COLLATION_MAP, which contains all collations from http://unicode.org/cldr/trac/browser/tags/release-XX/common/collation?order=name
//
func (parser *Parser) Eat_identpart_collation_normalized() (string, *rsql.Error) {
	var (
		rsql_err  *rsql.Error
		lexeme    lex.Lexeme
		collation string
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_type != lex.LEX_IDENTPART {
		return "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_IDENTIFIER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if collation, rsql_err = rsql.Normalize_collation(lexeme.Lex_word); rsql_err != nil { // normalize collation and check that it is valid. Result is lowercase, e.g. fr_ca_cs_as
		return "", rsql_err
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return "", rsql_err
	}

	return collation, nil
}

// Eat_identpart_language_normalized returns a normalized language if language is supported, or an error.
// Languages are e.g. "fr_ch", "fr_fr", "en_us", etc. "french" is alias for "fr_fr", "english" is alias for "en_us", etc.
//
func (parser *Parser) Eat_identpart_language_normalized() (string, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		lexeme   lex.Lexeme
		language string
	)

	lexeme = parser.Current_lexeme

	if lexeme.Lex_type != lex.LEX_IDENTPART {
		return "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_IDENTIFIER_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if language, rsql_err = lang.Normalize_language(lexeme.Lex_word); rsql_err != nil { // normalize language and check that it is valid
		return "", rsql_err
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip value
		return "", rsql_err
	}

	return language, nil
}

//***************************************************************************
//                                                                          *
//                        operator precedence list                          *
//                                                                          *
//***************************************************************************

// oplist_t is a precedence list used in Eat_expression().
// It stores operators by precedence order. It is used to create the AST tree of an expression.
//
type oplist_t struct {
	array_0 [2]*Token_primary
	array_1 [4]*Token_primary
	array_2 [4]*Token_primary
	array_3 [2]*Token_primary
	array_4 [2]*Token_primary
	array_5 [2]*Token_primary
	array_6 [2]*Token_primary
	array_7 [2]*Token_primary

	lists [TOK_PRIM_OPERATOR_PRECEDENCE_LEVELS][]*Token_primary
}

// init_slice initializes the list for the precedence order.
// It links a default hardcoded array to the requested slice, avoiding allocating memory on the heap when a small number of operators are appended.
//
func (opl *oplist_t) init_slice(precedence Prim_precedence_t) {

	switch precedence {
	case 0:
		opl.lists[0] = opl.array_0[0:0]
	case 1:
		opl.lists[1] = opl.array_1[0:0]
	case 2:
		opl.lists[2] = opl.array_2[0:0]
	case 3:
		opl.lists[3] = opl.array_3[0:0]
	case 4:
		opl.lists[4] = opl.array_4[0:0]
	case 5:
		opl.lists[5] = opl.array_5[0:0]
	case 6:
		opl.lists[6] = opl.array_6[0:0]
	case 7:
		opl.lists[7] = opl.array_7[0:0]
	default:
		panic("unknown precedence")
	}
}

// put_operator inserts Token_primary tok in the precedence list.
// The element is inserted at the beginning (<--- associativity) or end (---> associativity) of the list.
//
// The token passed as argument contains its precedence and associativity information, which are used by this function.
//
//     In function parser.Eat_operator(), we see that :
//        - associativity is <--- for unary  operator. They will be ordered this way in the list 'oplist_t'.
//        - associativity is ---> for binary operator. They will be ordered this way in the list 'oplist_t'.
//
func (opl *oplist_t) put_operator(tok *Token_primary) {

	assert(tok.Prim_type == TOK_PRIM_OPERATOR)

	precedence := tok.prim_precedence
	associativity := tok.prim_associativity

	if opl.lists[precedence] == nil {
		opl.init_slice(precedence)
	}

	// if right associativity append token to the tail of the list

	if associativity == TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT { // right associativity   --->     (most common case)
		opl.lists[precedence] = append(opl.lists[precedence], tok)
		return
	}

	// if left associativity <---, insert token at the head of the list

	assert(associativity == TOK_PRIM_OPERATOR_ASSOCIATIVITY_LEFT)

	lst := opl.lists[precedence]
	lst = append(lst, nil)
	copy(lst[1:], lst)
	lst[0] = tok
	opl.lists[precedence] = lst
}

func (opl *oplist_t) remove(tok *Token_primary) {

	assert(tok.Prim_type == TOK_PRIM_OPERATOR)

	precedence := tok.prim_precedence
	list := opl.lists[precedence]

	for i, t := range list {
		if t == tok {
			opl.lists[precedence] = append(list[:i], list[i+1:]...)
			return
		}
	}

	panic("token to remove not found")
}

//***************************************************************************
//                                                                          *
//                              WHILE nesting list                          *
//                                                                          *
//***************************************************************************

type while_stack_t []*Token_stmt_WHILE

func (wstack *while_stack_t) push(tok *Token_stmt_WHILE) *rsql.Error {

	if len(*wstack) >= SPEC_LOOP_STACK_SIZE {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_NESTED_LOOPS, rsql.ERROR_BATCH_ABORT)
	}

	*wstack = append(*wstack, tok)

	return nil
}

func (wstack *while_stack_t) unpush() {

	length := len(*wstack)

	assert(length > 0)

	(*wstack)[length-1] = nil
	*wstack = (*wstack)[:length-1]
}

func (wstack *while_stack_t) count() int {

	return len(*wstack)
}

func (wstack *while_stack_t) top() *Token_stmt_WHILE {

	return (*wstack)[len(*wstack)-1]
}

//***************************************************************************
//                                                                          *
//                                    misc                                  *
//                                                                          *
//***************************************************************************

// LIKE_is_range checks if s is a LIKE expression of the form 'prefix%'.
// It this is the case, both expressions below are equivalent:
//
//         a LIKE 'prefix%'
//
//         a >= 'prefix' AND a <= 'prefix\U0010FFFD'
//
func LIKE_is_range(s string) (prefix string, ok bool) {
	const (
		STATE_NORMAL = iota + 1
		STATE_INSIDE_BRACKET
		STATE_RBRACKET_EXPECTED
	)

	if len(s) == 0 {
		return "", false
	}

	if s == "%" {
		return "", false
	}

	if s[len(s)-1] != '%' {
		return "", false
	}

	prefix = s[:len(s)-1] // discard terminating '%'

	state := STATE_NORMAL
	for _, r := range prefix {
		switch state {
		case STATE_NORMAL:
			switch r {
			case ']', '_', '%':
				return "", false
			case '[':
				state = STATE_INSIDE_BRACKET
			default:
				// normal character
			}

		case STATE_INSIDE_BRACKET:
			switch r {
			case '_', '%':
				state = STATE_RBRACKET_EXPECTED
			default:
				return "", false
			}

		case STATE_RBRACKET_EXPECTED:
			switch r {
			case ']':
				state = STATE_NORMAL
			default:
				return "", false
			}
		}
	}

	if state != STATE_NORMAL {
		return "", false
	}

	prefix = strings.Replace(prefix, "[_]", "_", -1) // replace all escaped _
	prefix = strings.Replace(prefix, "[%]", "%", -1) // replace all escaped %

	return prefix, true
}
