package ast

import (
	"fmt"
	"strings"

	"rsql"
)

type Compatibility uint64       // category of the statement, to check if it is compatible with other statements of different category in the same batch
const SIZEOF_COMPATIBILITY = 64 // size in bits of Compatibility

const (
	COMPAT_ALTER_SERVER_PARAMETER Compatibility = 1 << iota

	COMPAT_BACKUP
	COMPAT_RESTORE

	COMPAT_CREATE_LOGIN
	COMPAT_ALTER_LOGIN
	COMPAT_DROP_LOGIN

	COMPAT_CREATE_DATABASE
	COMPAT_ALTER_DATABASE
	COMPAT_DROP_DATABASE
	COMPAT_ALTER_AUTHORIZATION

	COMPAT_CREATE_USER
	COMPAT_ALTER_USER
	COMPAT_DROP_USER
	COMPAT_CREATE_ROLE
	COMPAT_ALTER_ROLE
	COMPAT_DROP_ROLE

	COMPAT_CREATE_TABLE
	COMPAT_ALTER_TABLE
	COMPAT_DROP_TABLE

	COMPAT_CREATE_INDEX
	COMPAT_ALTER_INDEX
	COMPAT_DROP_INDEX

	COMPAT_SELECT
	COMPAT_BULK_EXPORT
	COMPAT_INSERT
	COMPAT_BULK_INSERT
	COMPAT_UPDATE
	COMPAT_DELETE
	COMPAT_TRUNCATE
	COMPAT_TRUNCATE_WITH_SHRINK_FILE
	COMPAT_SHRINK
	COMPAT_CREATE_VARIABLE_TABLE
)

const (
	COMPATMASK_SERVER_PARAM_OPS = COMPAT_ALTER_SERVER_PARAMETER                                                                                                                                                                               // mask for statements managing server parameters
	COMPATMASK_BACKUP_OPS       = COMPAT_BACKUP | COMPAT_RESTORE                                                                                                                                                                              // mask for backup and restore statements
	COMPATMASK_LOGIN_OPS        = COMPAT_CREATE_LOGIN | COMPAT_ALTER_LOGIN | COMPAT_DROP_LOGIN                                                                                                                                                // mask for statements managing logins
	COMPATMASK_DATABASE_OPS     = COMPAT_CREATE_DATABASE | COMPAT_ALTER_DATABASE | COMPAT_DROP_DATABASE | COMPAT_ALTER_AUTHORIZATION                                                                                                          // mask for statements managing databases
	COMPATMASK_PRINCIPAL_OPS    = COMPAT_CREATE_USER | COMPAT_ALTER_USER | COMPAT_DROP_USER | COMPAT_CREATE_ROLE | COMPAT_ALTER_ROLE | COMPAT_DROP_ROLE                                                                                       // mask for statements managing principals
	COMPATMASK_TABLE_OPS        = COMPAT_CREATE_TABLE | COMPAT_ALTER_TABLE | COMPAT_DROP_TABLE | COMPAT_CREATE_INDEX | COMPAT_ALTER_INDEX | COMPAT_DROP_INDEX                                                                                 // mask for statements managing table definitions
	COMPATMASK_DATA_OPS         = COMPAT_SELECT | COMPAT_BULK_EXPORT | COMPAT_INSERT | COMPAT_BULK_INSERT | COMPAT_UPDATE | COMPAT_DELETE | COMPAT_TRUNCATE | COMPAT_TRUNCATE_WITH_SHRINK_FILE | COMPAT_SHRINK | COMPAT_CREATE_VARIABLE_TABLE // mask for statements dealing with records

	COMPATMASK_LOGIN_PRINCIPAL_OPS = COMPATMASK_LOGIN_OPS | COMPATMASK_PRINCIPAL_OPS
)

func (compat Compatibility) String() string {
	var (
		list []string
	)

	for i := uint(0); i < SIZEOF_COMPATIBILITY; i++ {
		var c Compatibility

		c = compat & (1 << i)

		if c == 0 {
			continue
		}

		switch c {
		case COMPAT_ALTER_SERVER_PARAMETER:
			list = append(list, "COMPAT_ALTER_SERVER_PARAMETER")

		case COMPAT_BACKUP:
			list = append(list, "COMPAT_BACKUP")
		case COMPAT_RESTORE:
			list = append(list, "COMPAT_RESTORE")

		case COMPAT_CREATE_LOGIN:
			list = append(list, "COMPAT_CREATE_LOGIN")
		case COMPAT_ALTER_LOGIN:
			list = append(list, "COMPAT_ALTER_LOGIN")
		case COMPAT_DROP_LOGIN:
			list = append(list, "COMPAT_DROP_LOGIN")

		case COMPAT_CREATE_DATABASE:
			list = append(list, "COMPAT_CREATE_DATABASE")
		case COMPAT_ALTER_DATABASE:
			list = append(list, "COMPAT_ALTER_DATABASE")
		case COMPAT_DROP_DATABASE:
			list = append(list, "COMPAT_DROP_DATABASE")
		case COMPAT_ALTER_AUTHORIZATION:
			list = append(list, "COMPAT_ALTER_AUTHORIZATION")

		case COMPAT_CREATE_USER:
			list = append(list, "COMPAT_CREATE_USER")
		case COMPAT_ALTER_USER:
			list = append(list, "COMPAT_ALTER_USER")
		case COMPAT_DROP_USER:
			list = append(list, "COMPAT_DROP_USER")
		case COMPAT_CREATE_ROLE:
			list = append(list, "COMPAT_CREATE_ROLE")
		case COMPAT_ALTER_ROLE:
			list = append(list, "COMPAT_ALTER_ROLE")
		case COMPAT_DROP_ROLE:
			list = append(list, "COMPAT_DROP_ROLE")

		case COMPAT_CREATE_TABLE:
			list = append(list, "COMPAT_CREATE_TABLE")
		case COMPAT_ALTER_TABLE:
			list = append(list, "COMPAT_ALTER_TABLE")
		case COMPAT_DROP_TABLE:
			list = append(list, "COMPAT_DROP_TABLE")

		case COMPAT_CREATE_INDEX:
			list = append(list, "COMPAT_CREATE_INDEX")
		case COMPAT_ALTER_INDEX:
			list = append(list, "COMPAT_ALTER_INDEX")
		case COMPAT_DROP_INDEX:
			list = append(list, "COMPAT_DROP_INDEX")

		case COMPAT_SELECT:
			list = append(list, "COMPAT_SELECT")
		case COMPAT_BULK_EXPORT:
			list = append(list, "COMPAT_BULK_EXPORT")
		case COMPAT_INSERT:
			list = append(list, "COMPAT_INSERT")
		case COMPAT_BULK_INSERT:
			list = append(list, "COMPAT_BULK_INSERT")
		case COMPAT_UPDATE:
			list = append(list, "COMPAT_UPDATE")
		case COMPAT_DELETE:
			list = append(list, "COMPAT_DELETE")
		case COMPAT_TRUNCATE:
			list = append(list, "COMPAT_TRUNCATE")
		case COMPAT_TRUNCATE_WITH_SHRINK_FILE:
			list = append(list, "COMPAT_TRUNCATE_WITH_SHRINK_FILE")
		case COMPAT_SHRINK:
			list = append(list, "COMPAT_SHRINK")
		case COMPAT_CREATE_VARIABLE_TABLE:
			list = append(list, "COMPAT_CREATE_VARIABLE_TABLE")
		default:
			list = append(list, fmt.Sprintf("unknown Compatibility %d", compat))
		}
	}

	return strings.Join(list, ", ")
}

func (parser *Parser) Check_compatibility(compat Compatibility) *rsql.Error {
	var (
		compatibilities Compatibility
	)

	compatibilities = parser.Compatibilities

	switch compat {
	case COMPAT_ALTER_SERVER_PARAMETER:
		if compatibilities&^COMPATMASK_SERVER_PARAM_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER SERVER PARAMETER")
		}

	case COMPAT_BACKUP:
		if compatibilities&^COMPATMASK_BACKUP_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "BACKUP DATABASE")
		}
	case COMPAT_RESTORE:
		if compatibilities&^COMPATMASK_BACKUP_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "RESTORE DATABASE")
		}

	case COMPAT_CREATE_LOGIN:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "CREATE LOGIN")
		}

	case COMPAT_ALTER_LOGIN:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER LOGIN")
		}

	case COMPAT_DROP_LOGIN:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DROP LOGIN")
		}

	case COMPAT_CREATE_DATABASE:
		if compatibilities&^COMPATMASK_DATABASE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "CREATE DATABASE")
		}

	case COMPAT_ALTER_DATABASE:
		if compatibilities&^COMPATMASK_DATABASE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER DATABASE")
		}

	case COMPAT_DROP_DATABASE:
		if compatibilities&^COMPATMASK_DATABASE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DROP DATABASE")
		}

	case COMPAT_ALTER_AUTHORIZATION:
		if compatibilities&^COMPATMASK_DATABASE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER AUTHORIZATION")
		}

	case COMPAT_CREATE_USER:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "CREATE USER")
		}

	case COMPAT_ALTER_USER:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER USER")
		}

	case COMPAT_DROP_USER:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DROP USER")
		}

	case COMPAT_CREATE_ROLE:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "CREATE ROLE")
		}

	case COMPAT_ALTER_ROLE:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER ROLE")
		}

	case COMPAT_DROP_ROLE:
		if compatibilities&^COMPATMASK_LOGIN_PRINCIPAL_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DROP ROLE")
		}

	case COMPAT_CREATE_TABLE:
		if compatibilities&^COMPATMASK_TABLE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "CREATE TABLE")
		}

	case COMPAT_ALTER_TABLE:
		if compatibilities&^COMPATMASK_TABLE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER TABLE")
		}

	case COMPAT_DROP_TABLE:
		if compatibilities&^COMPATMASK_TABLE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DROP TABLE")
		}

	case COMPAT_CREATE_INDEX:
		if compatibilities&^COMPATMASK_TABLE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "CREATE INDEX")
		}

	case COMPAT_ALTER_INDEX:
		if compatibilities&^COMPATMASK_TABLE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "ALTER INDEX")
		}

	case COMPAT_DROP_INDEX:
		if compatibilities&^COMPATMASK_TABLE_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DROP INDEX")
		}

	case COMPAT_SELECT: // not only statement, but also SELECT in expressions
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "SELECT")
		}

	case COMPAT_BULK_EXPORT:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "BULK EXPORT")
		}

	case COMPAT_INSERT:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "INSERT")
		}

	case COMPAT_BULK_INSERT:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "BULK INSERT")
		}

	case COMPAT_UPDATE:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "UPDATE")
		}

	case COMPAT_DELETE:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DELETE")
		}

	case COMPAT_TRUNCATE:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "TRUNCATE")
		}

	case COMPAT_TRUNCATE_WITH_SHRINK_FILE:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "TRUNCATE WITH SHRINK_FILE")
		}

	case COMPAT_SHRINK:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "SHRINK")
		}

	case COMPAT_CREATE_VARIABLE_TABLE:
		if compatibilities&^COMPATMASK_DATA_OPS != 0 {
			return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_STATEMENT_INCOMPATIBLE, rsql.ERROR_BATCH_ABORT, "DECLARE @mytable TABLE")
		}

	default:
		panic("impossible")
	}

	parser.Compatibilities |= compat

	return nil
}
