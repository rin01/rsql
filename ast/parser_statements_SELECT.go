package ast

import (
	"rsql"
	"rsql/csr"
	"rsql/data"
	"rsql/dict"
	"rsql/lex"
	"rsql/lk"
)

type Sarginfo struct {
	csr.Sarg

	Tok_a_column *Token_primary // column identifier
	Tok_a        *Token_primary // column identifier (in this case, == Tok_a_column), or cast(col as ...)
	Tok_expr     *Token_primary // expression with which Tok_a will be compared
	Tok_comp     *Token_primary // comparison token
}

type Condinfo struct {
	csr.Cond

	Tok_cond *Token_primary
}

type Orderby_direction_t uint8

const (
	ORDER_BY_ASC Orderby_direction_t = iota + 1
	ORDER_BY_DESC
)

type Orderby_part struct {
	Odr_expression *Token_primary
	Odr_direction  Orderby_direction_t
}

// Column is a column of SELECT.
//
// Col_expression contain the dataslot making up the output values of a SELECT statement.
//
type Column struct {
	Col_label          string // label. Empty string if no label.
	Col_label_original string // same as Col_label, but with original case. For display usage.
	Col_expression     *Token_primary
	Col_no             int
}

type Xt_type_t uint8

const (
	XT_TABLE Xt_type_t = iota + 1
	XT_JOIN
	XT_SINGLE // used for SELECT without FROM clause
	XT_FROM
	XT_SELECT
	XT_UNION
)

func (xt_type Xt_type_t) String() string {

	switch xt_type {
	case XT_TABLE:
		return "XT_TABLE"
	case XT_JOIN:
		return "XT_JOIN"
	case XT_SINGLE:
		return "XT_SINGLE"
	case XT_FROM:
		return "XT_FROM"
	case XT_SELECT:
		return "XT_SELECT"
	case XT_UNION:
		return "XT_UNION"
	default:
		panic("impossible")
	}
}

type Lookup_table_mode_t uint8

const (
	LOOKUP_TABLE_NORMAL    Lookup_table_mode_t = iota + 1 // if table alias exists, it is checked, but table name is not checked. If no alias, table name is checked.
	LOOKUP_TABLE_NAME_ONLY                                // only table name is checked (alias is just ignored if exists)
)

func Coalesce_objname(database_name string, schema_name string, object_name string) string {

	if database_name == "" {
		if schema_name == "" {
			return object_name
		}

		return schema_name + "." + object_name
	}

	return database_name + "." + schema_name + "." + object_name
}

func Coalesce_colname(database_name string, schema_name string, table_name string, col_name string) string {

	if database_name == "" {
		if schema_name == "" {
			if table_name == "" {
				return col_name
			}

			return table_name + "." + col_name
		}

		return schema_name + "." + table_name + "." + col_name
	}

	return database_name + "." + schema_name + "." + table_name + "." + col_name
}

//====================================================================
//                          Xtables
//====================================================================

type Xtable interface {
	Type() Xt_type_t
	Cursor() csr.Cursor
	Col_count() int
	Lookup_name(database_name string, schema_name string, table_name string, column_name string) (xtable_ds Xtable, col_no uint16, dataslot rsql.IDataslot, collation string, rsql_err *rsql.Error)
	Lookup_table(mode Lookup_table_mode_t, database_name string, schema_name string, table_name string) (result *Xtable_table, rsql_err *rsql.Error)
	Append_star_columns(list_columns []*Column, database_name string, schema_name string, table_name string) (list_result []*Column, found bool, rsql_err *rsql.Error)
	Put_in_map(tbl_map Tbl_map) *rsql.Error // check that in Xtable_select, there is no duplicate of table exposed names (that is: aliases, and last part of qualified names if no alias)
}

// this map is used to check that in Xtable_select, the last part of table qualified names and aliases have no duplicate.
//
type Tbl_map map[string]Xtable

// table [ [AS] alias ]
//
type Xtable_table struct {
	Tbl_qname      rsql.Object_qname_t
	Tbl_alias      string
	Tbl_hint_index string

	Tbl_gtabledef *rsql.GTabledef
	Tbl_cursor    *csr.Cursor_table
}

func (xtable_table *Xtable_table) Type() Xt_type_t {
	return XT_TABLE
}

func (xtable_table *Xtable_table) Cursor() csr.Cursor {
	return xtable_table.Tbl_cursor
}

func (xtable_table *Xtable_table) Col_count() int {
	return len(xtable_table.Tbl_cursor.Base_fields)
}

func (xtable_table *Xtable_table) Put_in_map(tbl_map Tbl_map) *rsql.Error {

	objname := xtable_table.Tbl_alias
	if objname == "" {
		objname = xtable_table.Tbl_qname.Object_name
	}

	if _, ok := tbl_map[objname]; ok == true {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_TABLE_NAME, rsql.ERROR_BATCH_ABORT, objname)
	}

	tbl_map[objname] = xtable_table

	return nil
}

// Lookup_name returns the dataslot corresponding to the name.
// Returns nil if not found.
//
// This function lazily creates the dataslot object in cursor (when the cursor was created, dataslots where not created).
//
// This function is used by the decorator.
//
func (xtable_table *Xtable_table) Lookup_name(database_name string, schema_name string, table_name string, column_name string) (xtable_ds Xtable, col_no uint16, dataslot rsql.IDataslot, collation string, rsql_err *rsql.Error) {
	var (
		coldef         *rsql.Coldef
		ok             bool
		col_base_seqno uint16
	)

	switch {
	case xtable_table.Tbl_alias != "": // if table alias name exists
		if table_name != "" && table_name != xtable_table.Tbl_alias {
			return nil, 0, nil, "", nil
		}

		if schema_name != "" || database_name != "" {
			return nil, 0, nil, "", nil
		}

	default: // no table alias name
		if table_name != "" && table_name != xtable_table.Tbl_qname.Object_name {
			return nil, 0, nil, "", nil
		}

		if schema_name != "" && schema_name != xtable_table.Tbl_qname.Schema_name {
			return nil, 0, nil, "", nil
		}

		if database_name != "" && database_name != xtable_table.Tbl_qname.Database_name {
			return nil, 0, nil, "", nil
		}
	}

	if coldef, ok = xtable_table.Tbl_cursor.Gtabledef.Colmap[column_name]; ok == false {
		return nil, 0, nil, "", nil
	}

	col_base_seqno = coldef.Cd_base_seqno

	dataslot = xtable_table.Tbl_cursor.Base_fields[col_base_seqno]

	if dataslot == nil { // create dataslot object lazily
		dataslot = data.Dataslot_XXX_NULL_new(rsql.KIND_COL_LEAF, coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
		xtable_table.Tbl_cursor.Base_fields[col_base_seqno] = dataslot
	}

	rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)

	return xtable_table, col_base_seqno, dataslot, coldef.Cd_collation, nil
}

// Lookup_table returns *Xtable_table corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator, by DELETE statement.
//
// Argument table_name can be an alias name.
//
func (xtable_table *Xtable_table) Lookup_table(mode Lookup_table_mode_t, database_name string, schema_name string, table_name string) (result *Xtable_table, rsql_err *rsql.Error) {

	assert(table_name != "")

	if mode == LOOKUP_TABLE_NORMAL {
		if xtable_table.Tbl_alias != "" { // if alias exists
			if database_name != "" || schema_name != "" || table_name != xtable_table.Tbl_alias {
				return nil, nil
			}

			return xtable_table, nil
		}
	}

	// LOOKUP_TABLE_NORMAL with no alias, or LOOKUP_TABLE_NAME_ONLY

	if table_name != xtable_table.Tbl_qname.Object_name {
		return nil, nil
	}

	if schema_name != "" && schema_name != xtable_table.Tbl_qname.Schema_name {
		return nil, nil
	}

	if database_name != "" && database_name != xtable_table.Tbl_qname.Database_name {
		return nil, nil
	}

	return xtable_table, nil
}

// Append_star_columns appends to list_columns all columns of the table described in arguments.
//
// This function is used by the parser (not by the decorator).
//
func (xtable_table *Xtable_table) Append_star_columns(list_columns []*Column, database_name string, schema_name string, table_name string) (list_result []*Column, found bool, rsql_err *rsql.Error) {
	var (
		gtabledef             *rsql.GTabledef
		unqualified_star_flag bool
	)

	switch {
	case table_name == "" && schema_name == "" && database_name == "": // unqualified '*' star
		unqualified_star_flag = true

	case xtable_table.Tbl_alias != "": // if table alias name exists
		if table_name != "" && table_name != xtable_table.Tbl_alias {
			return list_columns, false, nil
		}

		if schema_name != "" || database_name != "" {
			return list_columns, false, nil
		}

	default: // no table alias name
		if table_name != "" && table_name != xtable_table.Tbl_qname.Object_name {
			return list_columns, false, nil
		}

		if schema_name != "" && schema_name != xtable_table.Tbl_qname.Schema_name {
			return list_columns, false, nil
		}

		if database_name != "" && database_name != xtable_table.Tbl_qname.Database_name {
			return list_columns, false, nil
		}
	}

	// get GTabledef from dict

	gtabledef = xtable_table.Tbl_gtabledef
	assert(gtabledef != nil)

	for _, coldef := range gtabledef.Base.Td_coldefs {
		if coldef.Cd_autonum == rsql.CD_AUTONUM_ROWID { // skip ROWID column
			continue
		}

		colexpr := New_TOK_PRIM_IDENTIFIER(coldef.Cd_colname, lex.Coord_t{}) // no Coord_t for this token, it is useless
		colexpr.Prim_status = TOK_PRIM_STATUS_COMPLETE

		switch {
		case unqualified_star_flag == true:
			switch {
			case xtable_table.Tbl_alias != "": // if table alias name exists
				colexpr.Prim_table_name = xtable_table.Tbl_alias

			default:
				colexpr.Prim_database_name = xtable_table.Tbl_qname.Database_name
				colexpr.Prim_schema_name = xtable_table.Tbl_qname.Schema_name
				colexpr.Prim_table_name = xtable_table.Tbl_qname.Object_name
			}

		default:
			switch {
			case xtable_table.Tbl_alias != "": // if table alias name exists
				colexpr.Prim_table_name = xtable_table.Tbl_alias

			default:
				colexpr.Prim_database_name = database_name
				colexpr.Prim_schema_name = schema_name
				colexpr.Prim_table_name = table_name
			}
		}

		column := &Column{Col_expression: colexpr}

		list_columns = append(list_columns, column)
	}

	return list_columns, true, nil
}

// Xtable JOIN Xtable
//
type Xtable_join struct {
	Jn_type  csr.Join_type_t
	Jn_left  Xtable
	Jn_right Xtable
	Jn_on    *Token_primary // ON is mandatory, except if it is created by reorganizing xtables in FROM list into a tree of JOINs.

	Jn_cursor *csr.Cursor_join
}

func New_Xtable_join_with_cursor(join_type csr.Join_type_t, xtable_left Xtable, xtable_right Xtable, on_expression *Token_primary) *Xtable_join {

	xtable_join := &Xtable_join{
		Jn_type:  join_type,
		Jn_left:  xtable_left,
		Jn_right: xtable_right,
		Jn_on:    on_expression,

		Jn_cursor: csr.New_cursor_join(join_type, xtable_left.Cursor(), xtable_right.Cursor()),
	}

	return xtable_join
}

func (xtable_join *Xtable_join) Type() Xt_type_t {
	return XT_JOIN
}

func (xtable_join *Xtable_join) Cursor() csr.Cursor {
	return xtable_join.Jn_cursor
}

func (xtable_join *Xtable_join) Col_count() int {
	return xtable_join.Jn_left.Col_count() + xtable_join.Jn_right.Col_count()
}

func (xtable_join *Xtable_join) Put_in_map(tbl_map Tbl_map) *rsql.Error {

	if rsql_err := xtable_join.Jn_left.Put_in_map(tbl_map); rsql_err != nil {
		return rsql_err
	}

	if rsql_err := xtable_join.Jn_right.Put_in_map(tbl_map); rsql_err != nil {
		return rsql_err
	}

	return nil
}

// Lookup_name returns the dataslot corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator.
//
func (xtable_join *Xtable_join) Lookup_name(database_name string, schema_name string, table_name string, column_name string) (xtable_ds Xtable, col_no uint16, dataslot rsql.IDataslot, collation string, rsql_err *rsql.Error) {
	var (
		xtable_ds_left  Xtable
		col_no_left     uint16
		dataslot_left   rsql.IDataslot
		collation_left  string
		xtable_ds_right Xtable
		col_no_right    uint16
		dataslot_right  rsql.IDataslot
		collation_right string
	)

	if xtable_ds_left, col_no_left, dataslot_left, collation_left, rsql_err = xtable_join.Jn_left.Lookup_name(database_name, schema_name, table_name, column_name); rsql_err != nil {
		return nil, 0, nil, "", rsql_err
	}

	if xtable_ds_right, col_no_right, dataslot_right, collation_right, rsql_err = xtable_join.Jn_right.Lookup_name(database_name, schema_name, table_name, column_name); rsql_err != nil {
		return nil, 0, nil, "", rsql_err
	}

	switch {
	case dataslot_left == nil && dataslot_right == nil:
		return nil, 0, nil, "", nil

	case dataslot_left != nil && dataslot_right == nil:
		rsql.Assert(xtable_ds_left != nil && dataslot_left.Kind() == rsql.KIND_COL_LEAF)
		return xtable_ds_left, col_no_left, dataslot_left, collation_left, nil

	case dataslot_left == nil && dataslot_right != nil:
		rsql.Assert(xtable_ds_right != nil && dataslot_right.Kind() == rsql.KIND_COL_LEAF)
		return xtable_ds_right, col_no_right, dataslot_right, collation_right, nil

	default:
		assert(dataslot_left != nil && dataslot_right != nil)
		return nil, 0, nil, "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_COLUMN_NAME, rsql.ERROR_BATCH_ABORT, Coalesce_colname(database_name, schema_name, table_name, column_name))
	}
}

// Lookup_table returns *Xtable_table corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator, for DELETE.
//
// Argument table_name can be an alias name.
//
func (xtable_join *Xtable_join) Lookup_table(mode Lookup_table_mode_t, database_name string, schema_name string, table_name string) (result *Xtable_table, rsql_err *rsql.Error) {
	var (
		result_left  *Xtable_table
		result_right *Xtable_table
	)

	if result_left, rsql_err = xtable_join.Jn_left.Lookup_table(mode, database_name, schema_name, table_name); rsql_err != nil {
		return nil, rsql_err
	}

	if result_right, rsql_err = xtable_join.Jn_right.Lookup_table(mode, database_name, schema_name, table_name); rsql_err != nil {
		return nil, rsql_err
	}

	switch {
	case result_left == nil && result_right == nil:
		return nil, nil

	case result_left != nil && result_right == nil:
		return result_left, nil

	case result_left == nil && result_right != nil:
		return result_right, nil

	default:
		assert(result_left != nil && result_right != nil)
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_TARGET_TABLE_NAME, rsql.ERROR_BATCH_ABORT, Coalesce_objname(database_name, schema_name, table_name))
	}
}

// Append_star_columns appends to list_columns all columns of the table described in arguments.
//
// This function is used by the parser (not by the decorator).
//
func (xtable_join *Xtable_join) Append_star_columns(list_columns []*Column, database_name string, schema_name string, table_name string) (list_result []*Column, found bool, rsql_err *rsql.Error) {
	var (
		found_left  bool
		found_right bool
	)

	if list_columns, found_left, rsql_err = xtable_join.Jn_left.Append_star_columns(list_columns, database_name, schema_name, table_name); rsql_err != nil {
		return nil, false, rsql_err
	}

	if list_columns, found_right, rsql_err = xtable_join.Jn_right.Append_star_columns(list_columns, database_name, schema_name, table_name); rsql_err != nil {
		return nil, false, rsql_err
	}

	if found_left || found_right {
		found = true
	}

	return list_columns, found, nil
}

// Xtable_single is a special Xtable, used only for SELECT without FROM clause.
//
type Xtable_single struct {
	Sg_cursor *csr.Cursor_single
}

func (xtable_single *Xtable_single) Type() Xt_type_t {
	return XT_SINGLE
}

func (xtable_single *Xtable_single) Cursor() csr.Cursor {
	return xtable_single.Sg_cursor
}

func (xtable_single *Xtable_single) Col_count() int {
	return 0
}

func (xtable_single *Xtable_single) Put_in_map(tbl_map Tbl_map) *rsql.Error {
	return nil
}

// Lookup_name returns the dataslot corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator.
//
func (xtable_single *Xtable_single) Lookup_name(database_name string, schema_name string, table_name string, column_name string) (xtable_ds Xtable, col_no uint16, dataslot rsql.IDataslot, collation string, rsql_err *rsql.Error) {
	return nil, 0, nil, "", nil
}

// Lookup_table returns *Xtable_table corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator, for DELETE.
//
// Argument table_name can be an alias name.
//
func (xtable_single *Xtable_single) Lookup_table(mode Lookup_table_mode_t, database_name string, schema_name string, table_name string) (result *Xtable_table, rsql_err *rsql.Error) {
	return nil, nil
}

// Append_star_columns appends to list_columns all columns of the table described in arguments.
//
// This function is used by the parser (not by the decorator).
//
func (xtable_single *Xtable_single) Append_star_columns(list_columns []*Column, database_name string, schema_name string, table_name string) (list_result []*Column, found bool, rsql_err *rsql.Error) {
	return list_result, false, nil
}

// FROM clause
//
type Xtable_from struct {
	Fr_list  []Xtable       // list of Xtables. The decorator will reorganize them as a JOIN tree, and Fr_list will then only contain a pointer to the tree root Xtable.
	Fr_where *Token_primary // WHERE clause. Can be nil.

	Fr_cursor *csr.Cursor_from
}

func (xtable_from *Xtable_from) Type() Xt_type_t {
	return XT_FROM
}

func (xtable_from *Xtable_from) Cursor() csr.Cursor {
	return xtable_from.Fr_cursor
}

func (xtable_from *Xtable_from) Col_count() int {
	var count int

	for _, xtbl := range xtable_from.Fr_list {
		count += xtbl.Col_count()
	}

	return count
}

func (xtable_from *Xtable_from) Put_in_map(tbl_map Tbl_map) *rsql.Error {

	for _, xtable := range xtable_from.Fr_list {
		if rsql_err := xtable.Put_in_map(tbl_map); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}

// Lookup_name returns the dataslot corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator.
//
func (xtable_from *Xtable_from) Lookup_name(database_name string, schema_name string, table_name string, column_name string) (xtable_ds Xtable, col_no uint16, dataslot rsql.IDataslot, collation string, rsql_err *rsql.Error) {
	var (
		xtable_ds_aux Xtable
		col_no_aux    uint16
		dataslot_aux  rsql.IDataslot
		collation_aux string
	)

	for _, xtable := range xtable_from.Fr_list {
		if xtable_ds_aux, col_no_aux, dataslot_aux, collation_aux, rsql_err = xtable.Lookup_name(database_name, schema_name, table_name, column_name); rsql_err != nil {
			return nil, 0, nil, "", rsql_err
		}

		if dataslot_aux != nil {
			if dataslot != nil {
				return nil, 0, nil, "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_COLUMN_NAME, rsql.ERROR_BATCH_ABORT, Coalesce_colname(database_name, schema_name, table_name, column_name))
			}

			xtable_ds = xtable_ds_aux
			col_no = col_no_aux
			dataslot = dataslot_aux
			collation = collation_aux
		}
	}

	if dataslot == nil {
		return nil, 0, nil, "", nil
	}

	rsql.Assert(xtable_ds != nil && dataslot.Kind() == rsql.KIND_COL_LEAF)

	return xtable_ds, col_no, dataslot, collation, nil
}

// Lookup_table returns *Xtable_table corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator, for DELETE.
//
// Argument table_name can be an alias name.
//
func (xtable_from *Xtable_from) Lookup_table(mode Lookup_table_mode_t, database_name string, schema_name string, table_name string) (result *Xtable_table, rsql_err *rsql.Error) {
	var (
		result_aux *Xtable_table
	)

	for _, xtable := range xtable_from.Fr_list {
		if result_aux, rsql_err = xtable.Lookup_table(mode, database_name, schema_name, table_name); rsql_err != nil {
			return nil, rsql_err
		}

		if result_aux != nil {
			if result != nil {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_TARGET_TABLE_NAME, rsql.ERROR_BATCH_ABORT, Coalesce_objname(database_name, schema_name, table_name))
			}

			result = result_aux
		}
	}

	return result, nil
}

// Append_star_columns appends to list_columns all columns of the table described in arguments.
//
// This function is used by the parser (not by the decorator).
//
func (xtable_from *Xtable_from) Append_star_columns(list_columns []*Column, database_name string, schema_name string, table_name string) (list_result []*Column, found bool, rsql_err *rsql.Error) {
	var (
		found_aux bool
	)

	for _, xtable := range xtable_from.Fr_list {
		if list_columns, found_aux, rsql_err = xtable.Append_star_columns(list_columns, database_name, schema_name, table_name); rsql_err != nil {
			return nil, false, rsql_err
		}

		if found_aux {
			found = true
		}
	}

	return list_columns, found, nil
}

// SELECT expression
//
type Xtable_select struct {
	Sel_TOP_value int64

	Sel_varlist                []*Token_primary // assignment of @variable in SELECT. Only allowed for top level SELECT.
	Sel_list_cast_to_var       []*Token_primary // tokens that copies Sel_columns to Sel_varlist
	Sel_cast_to_var_basicblock *rsql.Basicblock // CAST intructions to assign SELECT result to @variable

	Sel_columns     []*Column
	Sel_columns_map map[string]*Column // map used for name lookup. If value is nil, it means that column name is ambiguous.

	Sel_from *Xtable_from // FROM clause of the SELECT. For grouping SELECT, a new Xtable_from is created, that reads grouptable.

	Sel_grouping_flag                  bool             // true if GROUP BY or aggregate function detected
	Sel_grouping_explicit_GROUP_BY     bool             // true if GROUP BY clause exists. Else, only aggregate functions exist in the SELECT.
	Sel_grouptable_feeding_from        *Xtable_from     // Xtable that feeds grouptable. It is the original FROM clause of the SELECT.
	Sel_grouptable_feeding_cursor_from *csr.Cursor_from // cursor that feeds grouptable
	Sel_groupby_expressions            []*Token_primary // GROUP BY expressions
	Sel_grouptable_gtabledef           *rsql.GTabledef  // GTabledef of grouptable
	Sel_grouptable_insertion_row       rsql.Row         // insertion row
	Sel_grouptable_upsert_row          rsql.Row         // grouptable row for upsert
	Sel_grouptable_aggrfuncs_to_inject []*Token_primary // aggregate functions to inject in grouptable row
	Sel_grouptable_output_row          rsql.Row         // grouptable row for output
	Sel_grouptable_having              *Token_primary   // HAVING clause

	Sel_orderby           []*Orderby_part
	Sel_orderby_dataslots []rsql.IDataslot
	Sel_cursor_orderby    *csr.Cursor_table // the statement SELECT will insert all records into this table, and then send them to the client

	Sel_alias string

	Sel_cursor *csr.Cursor_select
}

func (xtable_select *Xtable_select) Type() Xt_type_t {
	return XT_SELECT
}

func (xtable_select *Xtable_select) Cursor() csr.Cursor {
	return xtable_select.Sel_cursor
}

func (xtable_select *Xtable_select) Col_count() int {
	return len(xtable_select.Sel_columns)
}

func (xtable_select *Xtable_select) Put_in_map(tbl_map Tbl_map) *rsql.Error {

	objname := xtable_select.Sel_alias

	assert(objname != "")

	if _, ok := tbl_map[objname]; ok == true {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_TABLE_NAME, rsql.ERROR_BATCH_ABORT, objname)
	}

	tbl_map[objname] = xtable_select

	return nil
}

// Lookup_name returns the dataslot corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator.
//
func (xtable_select *Xtable_select) Lookup_name(database_name string, schema_name string, table_name string, column_name string) (xtable_ds Xtable, col_no uint16, dataslot rsql.IDataslot, collation string, rsql_err *rsql.Error) {
	var (
		column *Column
		ok     bool
	)

	// only alias.colname or colname are allowed

	if database_name != "" || schema_name != "" {
		return nil, 0, nil, "", nil
	}

	if table_name != "" && table_name != xtable_select.Sel_alias {
		return nil, 0, nil, "", nil
	}

	// retrieve Column

	if column, ok = xtable_select.Sel_columns_map[column_name]; ok == false {
		return nil, 0, nil, "", nil
	}

	if column == nil {
		return nil, 0, nil, "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_COLUMN_NAME, rsql.ERROR_BATCH_ABORT, Coalesce_colname(database_name, schema_name, table_name, column_name))
	}

	dataslot = xtable_select.Sel_cursor.Cols_facade[column.Col_no]

	rsql.Assert((dataslot.Datatype() == rsql.DATATYPE_VARCHAR) != (column.Col_expression.Prim_collation == ""))
	rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)

	return xtable_select, uint16(column.Col_no), dataslot, column.Col_expression.Prim_collation, nil
}

// Lookup_table returns *Xtable_table corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator, for DELETE.
//
// Argument table_name can be an alias name.
//
func (xtable_select *Xtable_select) Lookup_table(mode Lookup_table_mode_t, database_name string, schema_name string, table_name string) (result *Xtable_table, rsql_err *rsql.Error) {
	return nil, nil
}

// Append_star_columns appends to list_columns all columns of the table described in arguments.
//
// This function is used by the parser (not by the decorator).
//
func (xtable_select *Xtable_select) Append_star_columns(list_columns []*Column, database_name string, schema_name string, table_name string) (list_result []*Column, found bool, rsql_err *rsql.Error) {

	// only alias.colname or colname are allowed

	if database_name != "" || schema_name != "" {
		return list_columns, false, nil
	}

	if table_name != "" && table_name != xtable_select.Sel_alias {
		return list_columns, false, nil
	}

	// append all columns of SELECT

	for i, c := range xtable_select.Sel_columns {
		colname := c.Col_label
		if colname == "" {
			if c.Col_expression.Prim_type != TOK_PRIM_IDENTIFIER {
				return nil, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SELECT_EXPRESSION_WITHOUT_COLUMN_NAME, rsql.ERROR_BATCH_ABORT, i, xtable_select.Sel_alias)
			}

			colname = c.Col_expression.Prim_name
		}

		colexpr := New_TOK_PRIM_IDENTIFIER(colname, lex.Coord_t{}) // no Coord_t for this token, it is useless
		colexpr.Prim_status = TOK_PRIM_STATUS_COMPLETE
		colexpr.Prim_table_name = xtable_select.Sel_alias

		column := &Column{Col_expression: colexpr}

		list_columns = append(list_columns, column)
	}

	return list_columns, true, nil
}

// series of SELECT UNION [ALL] ... SELECT
//
//     a UNION b    is the set of all records of a and b, with duplicates removed.
//
//     Corollary:   In a series of unions, e.g.     a UNION [ALL] b UNION [ALL] c UNION d UNION ALL e
//                                  is same as      a UNION b UNION c UNION d UNION ALL e
//                  Each "UNION" without ALL transforms all preceding "UNION ALL" into "UNION".
//
//                  After this transformation, a series of unioned Xtables is always of the form:
//                  a UNION b UNION c ... UNION p   UNION ALL q UNION ALL r ... UNION ALL z
//
type Xtable_union struct {
	Un_equalized_row []*Token_primary // rows from each SELECT and UNION members are copied here. Column names and collations are same as first SELECT member.

	Un_members []*struct {
		Xtbl                  Xtable           // SELECT or UNION
		ALL_flag              bool             // true if Xtbl is preceded by UNION ALL
		No_duplicate          bool             // see example in comment above: true for  a, b, c, ..., p
		Cast_to_equalized_row []*Token_primary // CAST instuctions to cast or copy each columns from Xtbl to Un_equalized_row. Dataslots of Cast_to_equalized_row are the same as Un_equalized_row.
	} // list of Xtables

	Un_orderby        []*Orderby_part
	Un_cursor_orderby *csr.Cursor_table // the SELECT members will insert all records into this table, and then send them to the client

	Un_alias string

	Un_cursor *csr.Cursor_union
}

func (xtable_union *Xtable_union) Type() Xt_type_t {
	return XT_UNION
}

func (xtable_union *Xtable_union) Cursor() csr.Cursor {
	return xtable_union.Un_cursor
}

func (xtable_union *Xtable_union) Col_count() int {
	return len(xtable_union.Un_equalized_row)
}

func (xtable_union *Xtable_union) Put_in_map(tbl_map Tbl_map) *rsql.Error {

	objname := xtable_union.Un_alias

	if _, ok := tbl_map[objname]; ok == true {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_TABLE_NAME, rsql.ERROR_BATCH_ABORT, objname)
	}

	tbl_map[objname] = xtable_union

	return nil
}

// Lookup_name returns the dataslot corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator.
//
func (xtable_union *Xtable_union) Lookup_name(database_name string, schema_name string, table_name string, column_name string) (xtable_ds Xtable, col_no uint16, dataslot rsql.IDataslot, collation string, rsql_err *rsql.Error) {
	var (
		column *Column
		ok     bool
		xtable Xtable
	)

	// only alias.colname or colname are allowed

	if database_name != "" || schema_name != "" {
		return nil, 0, nil, "", nil
	}

	if table_name != "" && table_name != xtable_union.Un_alias {
		return nil, 0, nil, "", nil
	}

	// retrieve column

	xtable = xtable_union.Un_members[0].Xtbl // retrieve first SELECT

	for xtable.Type() != XT_SELECT {
		rsql.Assert(xtable.Type() == XT_UNION)

		xtable = xtable.(*Xtable_union).Un_members[0].Xtbl
	}

	xtable_select := xtable.(*Xtable_select)

	if column, ok = xtable_select.Sel_columns_map[column_name]; ok == false {
		return nil, 0, nil, "", nil
	}

	if column == nil {
		return nil, 0, nil, "", rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AMBIGUOUS_COLUMN_NAME, rsql.ERROR_BATCH_ABORT, Coalesce_colname(database_name, schema_name, table_name, column_name))
	}

	dataslot = xtable_union.Un_equalized_row[column.Col_no].Prim_dataslot
	collation = xtable_union.Un_equalized_row[column.Col_no].Prim_collation

	rsql.Assert((dataslot.Datatype() == rsql.DATATYPE_VARCHAR) != (collation == ""))
	rsql.Assert(dataslot.Kind() == rsql.KIND_COL_LEAF)

	return xtable_union, uint16(column.Col_no), dataslot, collation, nil
}

// Lookup_table returns *Xtable_table corresponding to the name.
// Returns nil if not found.
//
// This function is used by the decorator, for DELETE.
//
// Argument table_name can be an alias name.
//
func (xtable_union *Xtable_union) Lookup_table(mode Lookup_table_mode_t, database_name string, schema_name string, table_name string) (result *Xtable_table, rsql_err *rsql.Error) {
	return nil, nil
}

// Append_star_columns appends to list_columns all columns of the table described in arguments.
//
// This function is used by the parser (not by the decorator).
//
func (xtable_union *Xtable_union) Append_star_columns(list_columns []*Column, database_name string, schema_name string, table_name string) (list_result []*Column, found bool, rsql_err *rsql.Error) {
	var (
		xtable Xtable
	)

	// only alias.colname or colname are allowed

	if database_name != "" || schema_name != "" {
		return list_columns, false, nil
	}

	if table_name != "" && table_name != xtable_union.Un_alias {
		return list_columns, false, nil
	}

	// retrieve first SELECT

	xtable = xtable_union.Un_members[0].Xtbl

	for xtable.Type() != XT_SELECT {
		rsql.Assert(xtable.Type() == XT_UNION)

		xtable = xtable.(*Xtable_union).Un_members[0].Xtbl
	}

	xtable_select := xtable.(*Xtable_select)

	// append all columns of SELECT

	for i, c := range xtable_select.Sel_columns {
		colname := c.Col_label
		if colname == "" {
			if c.Col_expression.Prim_type != TOK_PRIM_IDENTIFIER {
				return nil, false, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UNION_EXPRESSION_WITHOUT_COLUMN_NAME, rsql.ERROR_BATCH_ABORT, i, xtable_union.Un_alias)
			}

			colname = c.Col_expression.Prim_name
		}

		colexpr := New_TOK_PRIM_IDENTIFIER(colname, lex.Coord_t{}) // no Coord_t for this token, it is useless
		colexpr.Prim_status = TOK_PRIM_STATUS_COMPLETE
		colexpr.Prim_table_name = xtable_union.Un_alias

		column := &Column{Col_expression: colexpr}

		list_columns = append(list_columns, column)
	}

	return list_columns, true, nil
}

//====================================================================
//                      eat Xtables functions
//====================================================================

// eat a unqualified or qualified table name in a FROM clause.
// All parts of the qualified name are filled.
//
// This function is called by Eat_xtable_term().
//
// The current lexeme must be LEX_IDENTPART or LEX_DOT.
//
func (parser *Parser) Eat_table() (*Xtable_table, *rsql.Error) {
	var (
		rsql_err           *rsql.Error
		table_qname        rsql.Object_qname_t
		alias_name         string
		hint_index_name    string
		gtabledef          *rsql.GTabledef
		is_permanent_table bool
	)

	if rsql_err := parser.Check_compatibility(COMPAT_SELECT); rsql_err != nil { // check if SELECT is compatible with previous statements
		return nil, rsql_err
	}

	// eat unqualified or qualified table name

	switch parser.Current_lexeme.Lex_type {
	case lex.LEX_VARIABLE: // @vartable
		if table_qname, rsql_err = parser.Eat_variable_table_qname(); rsql_err != nil {
			return nil, rsql_err
		}

	default: // normal table. If database name or schema name are missing, Eat_object_qname() fills them with current database or schema name.
		if table_qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
			return nil, rsql_err
		}

		is_permanent_table = true
	}

	// eat alias if any

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_AS { // optional AS
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip AS
			return nil, rsql_err
		}

		if !(parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && parser.Rune0 != ':') { // alias expected
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_TABLEALIAS, rsql.ERROR_BATCH_ABORT)
		}
	}

	if parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && parser.Rune0 != ':' { // if table alias exists, eat it. The check about ':' is to eat alias, not label, e.g.       select * from t    followed by      mylabel:
		alias_name = parser.Current_lexeme.Lex_word

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip table alias
			return nil, rsql_err
		}
	}

	// eat optional hint

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect left parenthese
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_INDEX); rsql_err != nil { // expect INDEX keyword
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_COMP_EQUAL {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip =
				return nil, rsql_err
			}
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect left parenthese
			return nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_LITERAL_NUMBER_INTEGRAL:
			var hint_index_int int64

			if hint_index_int, rsql_err = parser.Eat_literal_integral_positive_int64(); rsql_err != nil {
				return nil, rsql_err
			}

			if hint_index_int == 0 {
				hint_index_name = "0"
			}

		case lex.LEX_IDENTPART_SUB:
			if hint_index_name, rsql_err = parser.Eat_identpart(); rsql_err != nil {
				return nil, rsql_err
			}

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_HINT_INDEX_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
			return nil, rsql_err
		}
	}

	// get GTabledef

	switch {
	case table_qname.Is_variable_table():
		if gtabledef, rsql_err = parser.Temptable_bag.Get(table_qname.Object_name); rsql_err != nil {
			return nil, rsql_err
		}

	default:
		if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(table_qname); rsql_err != nil {
			return nil, rsql_err
		}
	}

	// create new Xtable_table

	xtable_table := &Xtable_table{
		Tbl_qname:      table_qname,
		Tbl_alias:      alias_name,
		Tbl_hint_index: hint_index_name,

		Tbl_gtabledef: gtabledef,
	}

	// for permanent table only, put in Registration

	if is_permanent_table {
		parser.Registration.Register_object_qname(table_qname, lk.LOCK_SHARED, rsql.PERMISSION_SELECT)
	}

	return xtable_table, nil
}

// eat a xtable term in a FROM clause.
//
// A xtable term is
//      - a qualified table name (if current lexeme is LEX_IDENTPART)
//      - '(' + xtable_expression + ')'   where xtable_expression is a tree of     xtable_term JOIN xtable_term
//      - '(' + SELECT_or_UNION_expression + ')' [AS] alias
//
// It cannot be a single table name enclosed in parentheses :
//      - '(' + mytable + ')' is not allowed
//
// This function is called by Eat_xtable_join_expression().
//
func (parser *Parser) Eat_xtable_term() (Xtable, *rsql.Error) {
	var (
		rsql_err       *rsql.Error
		xtable         Xtable
		orderby_clause []*Orderby_part
	)

	// === single normal table name, or @vartable name ===

	if parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT|lex.LEX_VARIABLE) != 0 {
		if xtable, rsql_err = parser.Eat_table(); rsql_err != nil { // eat the normal table or @vartable
			return nil, rsql_err
		}

		return xtable, nil // and just return
	}

	// === or xtable term enclosed in parentheses ===

	if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect left parenthese
		return nil, rsql_err
	}

	// === xtable is a xtable join expression with at least one join:   xtable_term JOIN xtable_term ... ===

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_SELECT {
		if xtable, rsql_err = parser.Eat_xtable_join_expression(); rsql_err != nil { // eat the join expression
			return nil, rsql_err
		}

		if xtable.Type() == XT_TABLE { // a single table cannot be enclosed in parentheses. At least one join must exist.
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_PAREN_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
			return nil, rsql_err
		}

		return xtable, nil
	}

	//=== or xtable is a SELECT or UNION expression ===

	if xtable, orderby_clause, rsql_err = parser.Eat_xtable_select_or_union(); rsql_err != nil { // eat the select or union expression
		return nil, rsql_err
	}

	if orderby_clause != nil {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
		return nil, rsql_err
	}

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_AS { // eat optional AS
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip AS
			return nil, rsql_err
		}
	}

	if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // alias is mandatory. Return error if no alias.
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_TABLEALIAS, rsql.ERROR_BATCH_ABORT)
	}

	switch xtable.Type() {
	case XT_SELECT:
		xtable.(*Xtable_select).Sel_alias = parser.Current_lexeme.Lex_word

	case XT_UNION:
		xtable.(*Xtable_union).Un_alias = parser.Current_lexeme.Lex_word

	default:
		panic("impossible")
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip alias
		return nil, rsql_err
	}

	return xtable, nil
}

// eat a xtable join expression.
//
// It is a xtable, optionally joined to other xtables.
//
// A xtable is often a plain table. But it can also be a parenthesed expression which is either (SELECT ...) AS alias   or   (xtable_term JOIN xtable_term ...).
//
//  Examples :
//    mytable
//    mytable JOIN othertable on col1=col2
//    (SELECT ...) as t1 JOIN othertable on col1=col2
//    (t1 JOIN t2 on a=b) JOIN othertable on col1=col2
//
//  Not allowed :
//    (t1)
//
// This function eats exactly this kind of table expression :
//
//   [xtable] [ JOIN [xtable] ON expression ] [ JOIN [xtable] ON expression ] ...
//
//
// This function returns a tree of joined tables found in a FROM clause.
//
// The call tree is as follows :
//
//  - parser.Eat_subquery_one() returns a Token_primary
//    - parser.Eat_xtable_select_or_union()
//
//  - parser.Eat_subquery_many() returns a Token_primary
//    - parser.Eat_xtable_select_or_union()
//
//  - parser.get_statement_select_or_union() returns a Token_stmt_select_or_union
//    - parser.Eat_xtable_select_or_union()
//
//
//  - parser.Eat_xtable_select_or_union() must begin by SELECT. Else, an error is returned.
//    - parser.Eat_xtable_select() asserts that current token is SELECT.
//      - parser.Eat_from_clause()
//        - parser.Eat_xtable_join_expression()  eats   xtable_term [join xtable_term ...]
//          - parser.Eat_xtable_term()     eats 'table'  or  '(...)'  or  '(select...) as alias'
//            - parser.Eat_table()
//            - parser.Eat_xtable_join_expression()
//            - parser.Eat_xtable_select_or_union() <-- only called when '( SELECT' is encountered
//
//
// In the FROM clause, when left parenthese is encountered, the parser always presumes first that it may be a ( xtable_term JOIN xtable_term ... ) expression.
//
// The parser tries hard to create a tree made of Xtables and JOINs.
// Only when '( SELECT' is encountered, is the function parser.Eat_xtable_select_or_union() called.
//
//    E.g.     select * from ((select a from t union all select a from t) union all (select ...
//    The parser will try to parse a series of JOINed tables after the '(', not a UNION expression.
//    So, it will fail, because a top-down parser cannot cope with this, unless backtracking is implemented.
//    It is not worth the effort, as I consider such convoluted SQL as pathological.
//
func (parser *Parser) Eat_xtable_join_expression() (Xtable, *rsql.Error) {
	var (
		rsql_err    *rsql.Error
		left_xtable Xtable

		right_xtable Xtable
		on_expr      *Token_primary
	)

	// read first xtable term

	if parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT|lex.LEX_VARIABLE|lex.LEX_LPAREN) == 0 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_TERM_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if left_xtable, rsql_err = parser.Eat_xtable_term(); rsql_err != nil { // 'table'  or  (... JOIN ... [JOIN ...]...)  or  (SELECT ... [UNION ...]...) AS alias
		return nil, rsql_err
	}

	// read optional joined xtable

	for { // eat sequences of [ JOIN xtable ON expression ]
		// eat INNER, LEFT, JOIN

		var jointype csr.Join_type_t = 0

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_INNER: // == INNER JOIN ==
			jointype = csr.JT_INNER_JOIN

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INNER
				return nil, rsql_err
			}

		case lex.LEX_KEYWORD_JOIN: // == JOIN ==
			jointype = csr.JT_INNER_JOIN

		case lex.LEX_KEYWORD_LEFT: // == LEFT JOIN ==
			jointype = csr.JT_LEFT_JOIN

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip LEFT
				return nil, rsql_err
			}

			if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_OUTER {
				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip useless OUTER
					return nil, rsql_err
				}
			}

		default: // no INNER, LEFT of JOIN found. Just return.
			return left_xtable, nil // ===> return
		}

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_JOIN); rsql_err != nil { // JOIN keyword expected
			return nil, rsql_err
		}

		// eat right xtable

		if parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT|lex.LEX_VARIABLE|lex.LEX_LPAREN) == 0 {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_TERM_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		if right_xtable, rsql_err = parser.Eat_xtable_term(); rsql_err != nil {
			return nil, rsql_err
		}

		// eat ON clause

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_ON); rsql_err != nil { // ON is mandatory
			return nil, rsql_err
		}

		if on_expr, rsql_err = parser.Eat_expression(); rsql_err != nil {
			return nil, rsql_err
		}

		// create Xtable_join

		xtable_join := &Xtable_join{
			Jn_type:  jointype,
			Jn_left:  left_xtable,
			Jn_right: right_xtable,
			Jn_on:    on_expr,
		}

		left_xtable = xtable_join
	}
}

// eat a sequence of xtable join expressions making up a FROM clause and WHERE clause.
//
//      xtable_join_expression [, [xtable_join_expression], [xtable_join_expression], ... ] [WHERE expression]
//
// If current lexeme is not FROM keyword, this function creates a FROM clause containing only a Xtable_single{} object.
//
func (parser *Parser) Eat_xtable_from() (*Xtable_from, *rsql.Error) {
	var (
		rsql_err     *rsql.Error
		list_xtable  []Xtable
		xtable       Xtable
		xtable_from  *Xtable_from
		where_clause *Token_primary
	)

	switch {
	case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_FROM:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip FROM
			return nil, rsql_err
		}

		for { // === loop on each xtable ===
			// eat the xtable join expression

			if xtable, rsql_err = parser.Eat_xtable_join_expression(); rsql_err != nil { // eat the join expression making up the xtable. It can be a single table.
				return nil, rsql_err
			}

			list_xtable = append(list_xtable, xtable) // put it in list

			// eat comma

			if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // === if not comma, exit the loop
				break
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
				return nil, rsql_err
			}
		}

	default: // no FROM clause
		list_xtable = append([]Xtable{}, &Xtable_single{})
	}

	assert(len(list_xtable) > 0) // here, at least one valid xtable has been read

	xtable_from = &Xtable_from{
		Fr_list: list_xtable,
	}

	// ==== eat WHERE clause ====

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WHERE {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WHERE
			return nil, rsql_err
		}

		if where_clause, rsql_err = parser.Eat_expression(); rsql_err != nil {
			return nil, rsql_err
		}

		xtable_from.Fr_where = where_clause
	}

	// ==== check that tables are not ambiguous ====

	tbl_map := make(Tbl_map)

	if rsql_err := xtable_from.Put_in_map(tbl_map); rsql_err != nil {
		return nil, rsql_err
	}

	return xtable_from, nil
}

// eat a SELECT expression.
//
func (parser *Parser) Eat_xtable_select() (*Xtable_select, []*Orderby_part, *rsql.Error) {
	var (
		rsql_err       *rsql.Error
		xtable_select  *Xtable_select
		top_value      int64
		column_list    []*Column
		var_list       []*Token_primary
		xtable_from    *Xtable_from
		no_from_clause bool
		orderby_clause []*Orderby_part
	)

	parser.aggregate_func_allowed_flag = false
	defer func() { parser.aggregate_func_allowed_flag = false }()

	// current lexeme must be SELECT

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_SELECT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip SELECT
		return nil, nil, rsql_err
	}

	// ==== create new Xtable_select ===

	xtable_select = &Xtable_select{}

	// ==== eat TOP clause if exists ====

	top_value = -1 // default: all records are output

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TOP {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip TOP
			return nil, nil, rsql_err
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect left parenthese
			return nil, nil, rsql_err
		}

		if top_value, rsql_err = parser.Eat_literal_integral_positive_int64(); rsql_err != nil { // eat TOP value
			return nil, nil, rsql_err
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
			return nil, nil, rsql_err
		}
	}

	xtable_select.Sel_TOP_value = top_value

	// ==== eat SELECT clause ( eat all columns ) ====

	parser.aggregate_func_allowed_flag = true // allow aggregate function in SELECT expressions
	parser.aggregate_func_detected = false    // reset aggregate function detector

	if column_list, var_list, rsql_err = parser.Eat_column_list(); rsql_err != nil { // unqualified or qualified placeholders '*' may exist in the list
		return nil, nil, rsql_err
	}

	xtable_select.Sel_varlist = var_list
	xtable_select.Sel_columns = column_list

	xtable_select.Sel_grouping_flag = parser.aggregate_func_detected
	parser.aggregate_func_detected = false
	parser.aggregate_func_allowed_flag = false // disallow aggregate function

	// ==== eat FROM clause ====

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_FROM { // no FROM clause
		no_from_clause = true
	}

	if xtable_from, rsql_err = parser.Eat_xtable_from(); rsql_err != nil { // eat sequence of xtables and WHERE clause
		return nil, nil, rsql_err
	}

	xtable_select.Sel_from = xtable_from

	// ==== process '*' placeholders ====

	list_columns := make([]*Column, 0, 40) // 40 is good enough

	for _, column := range xtable_select.Sel_columns {
		parser.Info_line = column.Col_expression.Tok_batch_line

		if column.Col_expression.Prim_subtype != TOK_PRIM_PLACEHOLDER_UNQUALIFIED_STAR && column.Col_expression.Prim_subtype != TOK_PRIM_PLACEHOLDER_QUALIFIED_STAR {
			list_columns = append(list_columns, column)

			if len(list_columns) >= SPEC_SELECT_NUMBER_OF_COLUMNS_MAX {
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_COLUMNS_IN_SELECT, rsql.ERROR_BATCH_ABORT)
			}
			continue
		}

		// '*' placeholder

		if len(xtable_select.Sel_varlist) != 0 {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SELECT_VARIABLE_STAR_FOUND, rsql.ERROR_BATCH_ABORT)
		}

		if no_from_clause == true {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_PLACEHOLDER_WITHOUT_FROM, rsql.ERROR_BATCH_ABORT)
		}

		var found = false // replace unqualified or qualified placeholder '*' by real columns
		if list_columns, found, rsql_err = xtable_from.Append_star_columns(list_columns, column.Col_expression.Prim_database_name, column.Col_expression.Prim_schema_name, column.Col_expression.Prim_table_name); rsql_err != nil {
			return nil, nil, rsql_err
		}

		if found == false { // placeholder table not found
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_PLACEHOLDER_TABLE_NOT_FOUND, rsql.ERROR_BATCH_ABORT, Coalesce_objname(column.Col_expression.Prim_database_name, column.Col_expression.Prim_schema_name, column.Col_expression.Prim_table_name))
		}

		if len(list_columns) >= SPEC_SELECT_NUMBER_OF_COLUMNS_MAX {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_COLUMNS_IN_SELECT, rsql.ERROR_BATCH_ABORT)
		}
	}

	xtable_select.Sel_columns = list_columns

	// create map for column name lookup

	colname_map := make(map[string]*Column, len(xtable_select.Sel_columns))

	for k, column := range xtable_select.Sel_columns {
		column.Col_no = k // 0, 1, 2, ...

		switch {
		case column.Col_label != "": // if column label exists
			if _, ok := colname_map[column.Col_label]; ok == true { // column name already exists. It is ambiguous.
				colname_map[column.Col_label] = nil
			} else {
				colname_map[column.Col_label] = column
			}

		case column.Col_expression.Prim_type == TOK_PRIM_IDENTIFIER: // if column is a plain table column (that is, not an expression)
			col_identname := column.Col_expression.Prim_name
			if _, ok := colname_map[col_identname]; ok == true { // column name already exists. It is ambiguous.
				colname_map[col_identname] = nil
			} else {
				colname_map[col_identname] = column
			}
		}
	}

	xtable_select.Sel_columns_map = colname_map

	// ====  eat GROUP BY clause ====

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_GROUP {
		var groupby_listexpr []*Token_primary

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip GROUP
			return nil, nil, rsql_err
		}

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_BY); rsql_err != nil {
			return nil, nil, rsql_err
		}

		if groupby_listexpr, rsql_err = parser.Eat_list_of_expressions(); rsql_err != nil {
			return nil, nil, rsql_err
		}

		if len(groupby_listexpr) > rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX { // each GROUP BY expression will become a native key field in the grouptable used for grouping
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_GROUP_BY_IN_SELECT, rsql.ERROR_BATCH_ABORT, rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX)
		}

		xtable_select.Sel_groupby_expressions = groupby_listexpr

		xtable_select.Sel_grouping_flag = true
		xtable_select.Sel_grouping_explicit_GROUP_BY = true
	}

	if xtable_select.Sel_grouping_flag == true && len(xtable_select.Sel_groupby_expressions) == 0 { // a grouping SELECT must ALWAYS have a GROUP BY clause. If not exists, assume GROUP BY 1.
		tok_nk := parser.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(1)

		xtable_select.Sel_groupby_expressions = append(xtable_select.Sel_groupby_expressions, tok_nk) // the constant 1 will be used as native key for grouptable
	}

	// ====  eat HAVING clause ====

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_HAVING {
		var having_clause *Token_primary

		if xtable_select.Sel_grouping_flag == false {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_HAVING_WITHOUT_GROUPING, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip HAVING
			return nil, nil, rsql_err
		}

		parser.aggregate_func_allowed_flag = true // allow aggregate function in HAVING expressions
		parser.aggregate_func_detected = false    // reset aggregate function detector

		if having_clause, rsql_err = parser.Eat_expression(); rsql_err != nil {
			return nil, nil, rsql_err
		}

		parser.aggregate_func_detected = false
		parser.aggregate_func_allowed_flag = false // disallow aggregate function

		xtable_select.Sel_grouptable_having = having_clause
	}

	// ==== eat ORDER BY if any ====

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_ORDER { // eat ORDER BY if any
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ORDER
			return nil, nil, rsql_err
		}

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_BY); rsql_err != nil { // skip BY
			return nil, nil, rsql_err
		}

		if xtable_select.Sel_grouping_flag == true {
			parser.aggregate_func_allowed_flag = true // allow aggregate function in ORDER BY expressions for grouping SELECT
			parser.aggregate_func_detected = false    // reset aggregate function detector
		}

		if orderby_clause, rsql_err = parser.Eat_order_by_clause(); rsql_err != nil {
			return nil, nil, rsql_err
		}

		parser.aggregate_func_detected = false
		parser.aggregate_func_allowed_flag = false // disallow aggregate function
	}

	return xtable_select, orderby_clause, nil
}

// eat all columns of a SELECT list.
//
func (parser *Parser) Eat_column_list() (listcol []*Column, listvar []*Token_primary, rsql_err *rsql.Error) {
	var (
		column *Column
		res    bool
	)

	// === loop on each column ===

	// note:  A column alias is normally just a label for the column, nothing more.
	//        A label cannot be used in a WHERE, GROUP BY, HAVING clause.
	//        Only in ORDER BY can label be used. (ORDER BY mylabel)

	for {
		if len(listcol) >= SPEC_SELECT_NUMBER_OF_COLUMNS_MAX {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_COLUMNS_IN_SELECT, rsql.ERROR_BATCH_ABORT)
		}

		// eat 'label =' column label if any.

		var col_label string
		var col_label_original string
		var colexpr *Token_primary
		var token_primary_variable *Token_primary

		switch {
		case parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && parser.Rune0 == '=': // if 'label =' is found, column label exists.
			col_label = parser.Current_lexeme.Lex_word
			col_label_original = parser.Current_lexeme_original.Lex_word

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip label
				return nil, nil, rsql_err
			}

			if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip =
				return nil, nil, rsql_err
			}

		case parser.Current_lexeme.Lex_type == lex.LEX_VARIABLE && parser.Rune0 == '=': // if '@variable =' is found, it is a variable assignment

			if token_primary_variable, rsql_err = parser.Eat_variable(); rsql_err != nil {
				return nil, nil, rsql_err
			}

			if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip =
				return nil, nil, rsql_err
			}

			listvar = append(listvar, token_primary_variable)
		}

		// eat the column expression, qualified star, or unqualified star

		switch {
		case parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_MULT: // column is unqualified star '*'
			if colexpr, rsql_err = parser.Eat_placeholder_UNQUALIFIED_STAR(); rsql_err != nil { // eat *
				return nil, nil, rsql_err
			}

		default:
			if res, rsql_err = parser.probe_for_qualified_star(); rsql_err != nil { // if true, should be a qualified star
				return nil, nil, rsql_err
			}

			switch res {
			case true:
				if colexpr, rsql_err = parser.Eat_placeholder_QUALIFIED_STAR(); rsql_err != nil { // column is qualified star
					return nil, nil, rsql_err
				}

			default:
				if colexpr, rsql_err = parser.Eat_expression(); rsql_err != nil { // column is normal expression
					return nil, nil, rsql_err
				}
			}
		}

		// eat column label if any

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_AS { // optional AS
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip AS
				return nil, nil, rsql_err
			}

			if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // column label expected
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_COL_LABEL, rsql.ERROR_BATCH_ABORT)
			}
		}

		if parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART { // if column label exists, eat it
			if col_label != "" {
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INVALID_COL_LABEL, rsql.ERROR_BATCH_ABORT)
			}

			col_label = parser.Current_lexeme.Lex_word
			col_label_original = parser.Current_lexeme_original.Lex_word

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip column label
				return nil, nil, rsql_err
			}
		}

		// check that label is not on a (un)qualified star column

		if col_label != "" && (colexpr.Prim_subtype == TOK_PRIM_PLACEHOLDER_UNQUALIFIED_STAR || colexpr.Prim_subtype == TOK_PRIM_PLACEHOLDER_QUALIFIED_STAR) {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INVALID_COL_LABEL, rsql.ERROR_BATCH_ABORT)
		}

		// create new struct Column

		column = &Column{
			Col_label:          col_label,
			Col_label_original: col_label_original,
			Col_expression:     colexpr,
		}

		listcol = append(listcol, column)

		// eat comma

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // === if not comma, exit the loop
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, nil, rsql_err
		}
	}

	// here, at least one valid column has been read.

	assert(len(listcol) > 0)

	return listcol, listvar, nil
}

// Eat_xtable_select_or_union parses SELECT statement, or a series of UNIONed SELECTs.
//
// The statement line can begin with SELECT or a left parenthese.
//
func (parser *Parser) Eat_xtable_select_or_union() (Xtable, []*Orderby_part, *rsql.Error) {
	var (
		rsql_err       *rsql.Error
		first_xtable   Xtable
		next_xtable    Xtable
		xtable_union   *Xtable_union
		UNION_ALL_flag bool
		orderby_clause []*Orderby_part
	)

	// === read first xtable ===

	switch {
	case parser.Current_lexeme.Lex_type == lex.LEX_LPAREN:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip left parenthese
			return nil, nil, rsql_err
		}

		if first_xtable, orderby_clause, rsql_err = parser.Eat_xtable_select_or_union(); rsql_err != nil { // Xtable_select or Xtable_union
			return nil, nil, rsql_err
		}

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
			return nil, nil, rsql_err
		}

	case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_SELECT:
		var xtable_select *Xtable_select

		if xtable_select, orderby_clause, rsql_err = parser.Eat_xtable_select(); rsql_err != nil { // Xtable_select
			return nil, nil, rsql_err
		}

		first_xtable = xtable_select

	default:
		return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SELECT_OR_LPAREN_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_UNION {
		return first_xtable, orderby_clause, nil
	}

	// === read all subsequent unioned xtables, if any ===

	xtable_union = &Xtable_union{}

	xtable_union.Un_members = append(xtable_union.Un_members, &struct {
		Xtbl                  Xtable
		ALL_flag              bool
		No_duplicate          bool
		Cast_to_equalized_row []*Token_primary
	}{first_xtable, true, false, nil}) // No_duplicate field will be updated below

	for { // eat sequences of  UNION [xtable]
		// === eat UNION, UNION ALL

		if orderby_clause != nil { // previous UNION member cannot have an ORDER BY clause
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_NOT_ALLOWED_IN_UNION_MEMBER, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip UNION
			return nil, nil, rsql_err
		}

		UNION_ALL_flag = false

		if parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_ALL {
			UNION_ALL_flag = true
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ALL
				return nil, nil, rsql_err
			}
		} else {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UNION_NOT_IMPLEMENTED_YET, rsql.ERROR_BATCH_ABORT) // UNION without ALL parsing works. When it will be implemented, just delete this line
		}

		// === eat next xtable

		switch {
		case parser.Current_lexeme.Lex_type == lex.LEX_LPAREN:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip left parenthese
				return nil, nil, rsql_err
			}

			if next_xtable, orderby_clause, rsql_err = parser.Eat_xtable_select_or_union(); rsql_err != nil { // Xtable_select or Xtable_union
				return nil, nil, rsql_err
			}

			if orderby_clause != nil { // UNION member cannot have an ORDER BY clause
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_NOT_ALLOWED_IN_UNION_MEMBER, rsql.ERROR_BATCH_ABORT)
			}

			if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect right parenthese
				return nil, nil, rsql_err
			}

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_SELECT: // last SELECT can have ORDER BY clause
			if next_xtable, orderby_clause, rsql_err = parser.Eat_xtable_select(); rsql_err != nil { // Xtable_select
				return nil, nil, rsql_err
			}

		default:
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SELECT_OR_LPAREN_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		xtable_union.Un_members = append(xtable_union.Un_members, &struct {
			Xtbl                  Xtable
			ALL_flag              bool
			No_duplicate          bool
			Cast_to_equalized_row []*Token_primary
		}{next_xtable, UNION_ALL_flag, false, nil})

		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_UNION { // if no UNION, break
			break
		}
	}

	// update No_duplicate field

	no_duplicate := false // for all xtables at and before the last UNION, No_duplicate = true

	for i := len(xtable_union.Un_members) - 1; i >= 0; i-- {
		if xtable_union.Un_members[i].ALL_flag == false {
			no_duplicate = true
		}

		if no_duplicate {
			xtable_union.Un_members[i].No_duplicate = true
		}
	}

	// ==== eat ORDER BY if any ====

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_ORDER { // eat ORDER BY if any
		if orderby_clause != nil { // previous UNION member cannot have an ORDER BY clause
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_NOT_ALLOWED_IN_UNION_MEMBER, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ORDER
			return nil, nil, rsql_err
		}

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_BY); rsql_err != nil { // skip BY
			return nil, nil, rsql_err
		}

		if orderby_clause, rsql_err = parser.Eat_order_by_clause(); rsql_err != nil {
			return nil, nil, rsql_err
		}
	}

	// ==== create xtable_union.Un_equalized_row ====

	xtable := xtable_union.Un_members[0].Xtbl // retrieve first SELECT

	for xtable.Type() != XT_SELECT {
		rsql.Assert(xtable.Type() == XT_UNION)

		xtable = xtable.(*Xtable_union).Un_members[0].Xtbl
	}

	xtable_select := xtable.(*Xtable_select)

	xtable_union.Un_equalized_row = make([]*Token_primary, len(xtable_select.Sel_columns))

	for i, _ := range xtable_union.Un_equalized_row { // create all columns of UNION
		colname := "__dummy__" // a dummy name is enough, as the Token_primary is not used

		colexpr := New_TOK_PRIM_IDENTIFIER(colname, lex.Coord_t{}) // no Coord_t for this token, it is useless
		colexpr.Prim_status = TOK_PRIM_STATUS_COMPLETE
		//colexpr.Prim_table_name = ""  // because xtable_union.Un_alias is not known yet, and the Token_primary is not used.

		xtable_union.Un_equalized_row[i] = colexpr
	}

	return xtable_union, orderby_clause, nil
}

// eat list of expressions in a ORDER BY clause.
//
// Return a list of Orderby_parts.
//
func (parser *Parser) Eat_order_by_clause() ([]*Orderby_part, *rsql.Error) {
	var (
		rsql_err       *rsql.Error
		expr           *Token_primary
		direction      Orderby_direction_t
		odr            *Orderby_part
		orderby_clause []*Orderby_part
	)

	// === loop on each ORDER BY part ===

	for {
		if len(orderby_clause) >= SPEC_SELECT_NUMBER_OF_ORDER_BY_MAX {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_ORDER_BY_IN_SELECT, rsql.ERROR_BATCH_ABORT)
		}

		// eat the expression

		if expr, rsql_err = parser.Eat_expression(); rsql_err != nil {
			return nil, rsql_err
		}

		// eat optional ASC DESC

		direction = ORDER_BY_ASC

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_ASC:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip useless ASC
				return nil, rsql_err
			}

		case lex.LEX_KEYWORD_DESC:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ORDER_BY_DESC_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
		}

		// create a new Orderby_part

		odr = &Orderby_part{
			Odr_expression: expr,
			Odr_direction:  direction,
		}

		orderby_clause = append(orderby_clause, odr)

		// eat comma

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // === if not comma, exit the loop
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}
	}

	// here, at least one valid ORDER BY expression has been read.

	assert(len(orderby_clause) > 0)

	return orderby_clause, nil
}

func Get_first_SELECT(xtable Xtable) *Xtable_select {

	for {
		if xtable.Type() == XT_SELECT {
			return xtable.(*Xtable_select)
		}

		xtable = xtable.(*Xtable_union).Un_members[0].Xtbl
	}

	panic("impossible")
}

// get_statement_SELECT_or_UNION parses SELECT statement, or a series of UNIONed SELECTs.
//
// The statement line can begin with SELECT or a left parenthese.
//
func (parser *Parser) get_statement_SELECT_or_UNION() (*Token_stmt_SELECT_or_UNION, *rsql.Error) {
	var (
		rsql_err                   *rsql.Error
		token_stmt_select_or_union *Token_stmt_SELECT_or_UNION
		xtable                     Xtable
		orderby_clause             []*Orderby_part
	)

	// current lexeme must be SELECT or left parenthese

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_SELECT || parser.Current_lexeme.Lex_type == lex.LEX_LPAREN)

	// create Token_stmt_select_or_union

	token_stmt_select_or_union = &Token_stmt_SELECT_or_UNION{}
	parser.token_init(&token_stmt_select_or_union.Token, TOK_STMT_SELECT_OR_UNION)

	if xtable, orderby_clause, rsql_err = parser.Eat_xtable_select_or_union(); rsql_err != nil {
		return nil, rsql_err
	}

	switch xtable := xtable.(type) {
	case *Xtable_select:
		xtable.Sel_orderby = orderby_clause
	case *Xtable_union:
		xtable.Un_orderby = orderby_clause
	default:
		panic("impossible")
	}

	token_stmt_select_or_union.Su_xtable = xtable

	return token_stmt_select_or_union, nil
}

func TOP_value_exists(xtable Xtable) bool {

	switch xtable := xtable.(type) {
	case *Xtable_select:
		if xtable.Sel_TOP_value != -1 {
			return true
		}

		return false

	case *Xtable_union:
		for _, item := range xtable.Un_members {
			if TOP_value_exists(item.Xtbl) == true {
				return true
			}
		}

		return false

	default:
		panic("impossible")
	}
}
