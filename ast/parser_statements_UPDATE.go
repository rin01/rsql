package ast

import (
	"rsql"
	"rsql/dict"
	"rsql/lex"
	"rsql/lk"
)

// get_statement_UPDATE parses UPDATE statement.
//
func (parser *Parser) get_statement_UPDATE() (*Token_stmt_UPDATE, *rsql.Error) {
	var (
		rsql_err             *rsql.Error
		token_stmt_update    *Token_stmt_UPDATE
		target_database_name string
		target_schema_name   string
		target_object_name   string
		xtable_from          *Xtable_from
		target_table         *Xtable_table
		gtabledef            *rsql.GTabledef
		no_from_clause       bool
		set_list             []Set_clause
		colname_map          map[string]struct{}
		is_permanent_table   bool
	)

	//note: UPDATE keyword has already been skipped

	if rsql_err := parser.Check_compatibility(COMPAT_UPDATE); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_update

	token_stmt_update = &Token_stmt_UPDATE{}
	parser.token_init(&token_stmt_update.Token, TOK_STMT_UPDATE)

	// eat target table name, or alias

	switch parser.Current_lexeme.Lex_type {
	case lex.LEX_VARIABLE: // @vartable
		if target_database_name, target_schema_name, target_object_name, _, rsql_err = parser.Eat_variable_table_qname_parts(); rsql_err != nil {
			return nil, rsql_err
		}

	default: // normal table. If database name or schema name are missing, Eat_object_qname() fills them with current database or schema name.
		if target_database_name, target_schema_name, target_object_name, _, rsql_err = parser.Eat_object_qname_parts(); rsql_err != nil { // target_database_name and target_schema_name may be ""
			return nil, rsql_err
		}

		is_permanent_table = true
	}

	// ==== eat SET expressions ====

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_SET); rsql_err != nil { // eat mandatory SET
		return nil, rsql_err
	}

	colname_map = make(map[string]struct{})

	set_list = append(set_list, Set_clause{St_colname: rsql.ROWID, St_expr: parser.Create_TOK_PRIM_LITERAL_NULL()}) // always set ROWID column to NULL, so that ROWID will change for updated rows

	for {
		var colname string
		var colexpr *Token_primary

		if len(set_list) >= rsql.SPEC_TABLE_NUMBER_OF_COLUMNS_MAX {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UPDATE_TOO_MANY_SET_CLAUSES, rsql.ERROR_BATCH_ABORT)
		}

		// eat column name

		if colname, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat column name
			return nil, rsql_err
		}

		if colname == rsql.ROWID {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UPDATE_ROWID_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		if parser.Current_lexeme.Lex_type == lex.LEX_DOT {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UPDATE_QUALIFIED_COLUMN_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		if _, ok := colname_map[colname]; ok == true {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_COLUMN_NAME_FOUND, rsql.ERROR_BATCH_ABORT, colname)
		}

		colname_map[colname] = struct{}{}

		// eat assignment operator and expression

		if colexpr, rsql_err = parser.eat_UPDATE_ASSIGNMENT_operator_and_expression(colname); rsql_err != nil {
			return nil, rsql_err
		}

		// append SET clause to list

		set_clause := Set_clause{St_colname: colname, St_expr: colexpr}

		set_list = append(set_list, set_clause)

		// eat comma

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // === if not comma, exit the loop
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}
	}

	token_stmt_update.Ud_set_list = set_list

	// ==== eat FROM clause ====

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_FROM {
		no_from_clause = true
	}

	if xtable_from, rsql_err = parser.Eat_xtable_from(); rsql_err != nil { // if no FROM clause, a FROM clause containing a Xtable_single{} is created
		return nil, rsql_err
	}

	// lookup target table in FROM clause

	if target_table, rsql_err = xtable_from.Lookup_table(LOOKUP_TABLE_NORMAL, target_database_name, target_schema_name, target_object_name); rsql_err != nil { // normal name lookup, with alias overriding table name
		return nil, rsql_err
	}

	if target_table == nil { // if normal lookup didn't find target table, try looking up with table names only
		if target_table, rsql_err = xtable_from.Lookup_table(LOOKUP_TABLE_NAME_ONLY, target_database_name, target_schema_name, target_object_name); rsql_err != nil { // check table name only (ignore alias)
			return nil, rsql_err
		}
	}

	// target table not found in FROM clause. Create a target Xtable_table and insert it as last element in xtable_from.Fr_list.

	if target_table == nil {
		// create target qname

		if target_database_name == "" {
			target_database_name = parser.parser_current_default_database
		}

		if target_schema_name == "" {
			target_schema_name = parser.parser_current_default_schema
		}

		target_qname := rsql.Object_qname_t{Database_name: target_database_name, Schema_name: target_schema_name, Object_name: target_object_name, Object_name_original: target_object_name}

		// get GTabledef

		switch {
		case target_qname.Is_variable_table():
			if gtabledef, rsql_err = parser.Temptable_bag.Get(target_qname.Object_name); rsql_err != nil {
				return nil, rsql_err
			}

		default:
			if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(target_qname); rsql_err != nil {
				return nil, rsql_err
			}
		}

		// create target Xtabledef

		target_table = &Xtable_table{
			Tbl_qname: target_qname,
			Tbl_alias: "",

			Tbl_gtabledef: gtabledef,
		}

		if no_from_clause == true {
			assert(len(xtable_from.Fr_list) == 1)
			xtable_from.Fr_list[0] = target_table // replace the Xtable_single{} that parser.Eat_xtable_from() put here, by target_table
		} else {
			xtable_from.Fr_list = append(xtable_from.Fr_list, target_table) // insert target_table at last position in list
		}
	}

	token_stmt_update.Ud_from = xtable_from
	token_stmt_update.Ud_target_table = target_table

	// for permanent table only, put in Registration

	if is_permanent_table {
		parser.Registration.Register_object_qname(target_table.Tbl_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_UPDATE)
	}

	return token_stmt_update, nil
}

// Eat_ASSIGNMENT_operator_and_expression is an auxiliary function to parse assignment to UPDATE SET column.
//
// It parses the [assignment operator][expression] sequence.
//
//      It can be a plain assignment :
//        - column = expression
//
//      or a compound assignment :
//        - column += expression
//        - column -= expression
//        - column *= expression
//        - etc
//
// colname is the column identifier to be assigned to.
//
// Returns expression to be assigned to column.
//
func (parser *Parser) eat_UPDATE_ASSIGNMENT_operator_and_expression(colname string) (colexpr *Token_primary, rsql_err *rsql.Error) {
	var (
		prim_subtype_underlying_operator  Prim_subtype_t
		lexeme_underlying_operator        lex.Lexeme     // for compound assignment. Just the lexeme of the underlying operator '+', '-' etc only for info.
		token_expression                  *Token_primary // rvalue, or will be combined with an underlying operator in case of '+=', '*=' etc.
		token_primary_underlying_operand  *Token_primary // for compound assignment. New token which is a copy of the variable or identifier.
		token_primary_underlying_operator *Token_primary // for compound assignment. For example, for '+=' assignment, it will be a new '+' operator token.
	)

	// token_primary_lvalue += token_expression   is transformed into   token_primary_lvalue = token_primary_underlying_operand + token_expression
	// where token_primary_underlying_operand is just a copy of token_primary_lvalue
	// and token_primary_underlying_operator is '+'.

	/* current lexeme must be an assignment operator */

	if prim_subtype_underlying_operator, lexeme_underlying_operator, rsql_err = parser.Eat_compound_assignment_operator(); rsql_err != nil {
		return nil, rsql_err
	}

	info_line := parser.Info_line

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip assignment operator
		return nil, rsql_err
	}

	/* eat expression */

	if token_expression, rsql_err = parser.Eat_expression(); rsql_err != nil {
		return nil, rsql_err
	}

	/* for compound assignment, create a new Token_primary for the column, and a new Token_primary for the underlying operator. */
	/* Then, affect the expression eaten to the underlying operator. */

	if prim_subtype_underlying_operator != TOK_PRIM_OPERATOR_COMP_EQUAL {
		// create a new Token_primary of the column

		lexeme := lex.Create_lexeme(lex.LEX_IDENTPART, lex.LEX_IDENTPART_SUB, colname)

		token_primary_underlying_operand = parser.New_token_primary(TOK_PRIM_IDENTIFIER, TOK_PRIM_IDENTIFIER_SUB, lexeme)
		token_primary_underlying_operand.Prim_status = TOK_PRIM_STATUS_COMPLETE
		token_primary_underlying_operand.Tok_batch_line = info_line

		// create a new Token_primary for underlying operator (operator in the assignment)

		token_primary_underlying_operator = parser.New_token_primary(TOK_PRIM_OPERATOR, prim_subtype_underlying_operator, lexeme_underlying_operator)
		token_primary_underlying_operator.Tok_batch_line = info_line
		// members prim_precedence and prim_associativity are not useful here (they are used only for expression parsing),
		//   so we don't update them.

		token_primary_underlying_operator.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS // used when walking the tree, to walk the children of the token
		token_primary_underlying_operator.Prim_status = TOK_PRIM_STATUS_COMPLETE

		token_primary_underlying_operator.Prim_left = token_primary_underlying_operand
		token_primary_underlying_operator.Prim_right = token_expression

		token_expression = token_primary_underlying_operator
	}

	return token_expression, nil
}
