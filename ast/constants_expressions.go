package ast

type (
	Prim_precedence_t    uint8 // operator precedence
	Prim_associativity_t uint8 // operator associativity (left or right)
	Prim_cardinality_t   uint8 // operator operand specifications
	Prim_status_t        uint8 // used during AST tree creation
)

func (status Prim_status_t) String() string {

	switch status {
	case TOK_PRIM_STATUS_UNCOMPLETE:
		return "STATUS_UNCOMPLETE"
	case TOK_PRIM_STATUS_COMPLETE:
		return "STATUS_COMPLETE"
	}

	panic("impossible")
}

// operator precedence (each precedence level has an associativity left-to-right or right-to-left)
//
const (
	// the precedence levels must be 0, 1, 2 ... because they are indexes in an array of lists of precedence.
	TOK_PRIM_OPERATOR_PRECEDENCE_UNARY_PLUS        Prim_precedence_t = 0   //   + (unary plus), - (unary minus), ~ (bit not)                              associativity  <---
	TOK_PRIM_OPERATOR_PRECEDENCE_MULTIPLY          Prim_precedence_t = 1   //   * (multiply), / (division), % (modulo)                                    associativity  --->
	TOK_PRIM_OPERATOR_PRECEDENCE_ADDITION          Prim_precedence_t = 2   //   + (add), - (substract), & (bit and), ^ (bit xor), | (bit or)              associativity  --->
	TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON        Prim_precedence_t = 3   //   =, >, <, >=, <=, <>, !=, !>, !< (comparison)                              associativity  --->
	TOK_PRIM_OPERATOR_PRECEDENCE_COMP2             Prim_precedence_t = 4   //   IS [NOT] NULL, [NOT] LIKE, [NOT] BETWEEN, [NOT] IN, ALL, ANY, SOME        associativity  --->
	TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_UNARY_NOT Prim_precedence_t = 5   //   NOT (logical not)                                                         associativity  <---
	TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_AND       Prim_precedence_t = 6   //   AND (logical and)                                                         associativity  --->
	TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_OR        Prim_precedence_t = 7   //   OR  (logical or)                                                          associativity  --->
	TOK_PRIM_OPERATOR_PRECEDENCE_LEVELS            Prim_precedence_t = 8   //   number of prededence levels (= highest precedence index + 1)
	TOK_PRIM_OPERATOR_PRECEDENCE_INVALID_VALUE     Prim_precedence_t = 255 //   invalid precedence value, to catch error if we try to use it.

/*  note: FOR MICROSOFT, THE "BIT OR" IS SAME PRECEDENCE AS "BIT AND" AND "BIT XOR", WHICH IS STUPID ! DON'T BLAME ME !
    associativity --->
*/

/*  note: if an exponentiation binary operator would exist ('**', for instance), it would be at the highest precedence level, with an associativity <---
 */
)

const (
	TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT         Prim_associativity_t = 1 << iota // associativity  --->
	TOK_PRIM_OPERATOR_ASSOCIATIVITY_LEFT                                           // associativity  <---
	TOK_PRIM_OPERATOR_ASSOCIATIVITY_INVALID_VALUE Prim_associativity_t = 255       // invalid associativity value, to catch error if we try to use it.
)

const (
	TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND Prim_cardinality_t = 1 << iota
	TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS
	TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL
	TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST
	TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL
	TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE
	TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_BETWEEN
	TOK_PRIM_OPERATOR_CARDINALITY_INVALID_VALUE Prim_cardinality_t = 255 //  invalid cardinality value, to catch error if we try to use it.
)

// During parsing, all terms and operators of an expression are read and put in a linear chain. Operators are also put in one of the precedence lists.
// Then, each operator is processed by precedence order, by unchaining left and/or right token, and putting them as operator operands.
// Thus, tokens are removed from the linear chain, and grafted as operator operands, making an AST tree for the expression.
// In the end, there should remain only one token in the linear chain, which is the root of the expression AST.
const (
	TOK_PRIM_STATUS_UNCOMPLETE Prim_status_t = 1 << iota // initial status for operators. During parsing, as operand tokens are grafted onto an operator, the operator status becomes TOK_PRIM_STATUS_COMPLETE.
	TOK_PRIM_STATUS_COMPLETE                             // during parsing, only tokens with TOK_PRIM_STATUS_COMPLETE can be grafted as operands onto an operator.
)

const (
	EAT_OPERATOR_UNARY     bool = true
	EAT_OPERATOR_NON_UNARY bool = false
)

// used as option for Eat_builtin_datatype().
type eat_datatype_option_t int

// used as option for Eat_builtin_datatype().
const (
	EAT_DATATYPE_CAST        eat_datatype_option_t = 10
	EAT_DATATYPE_DECLARATION eat_datatype_option_t = 20
)
