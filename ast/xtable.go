package ast

type Xtable_stack_t []Xtable

func (stack *Xtable_stack_t) Push(xtable Xtable) {

	*stack = append(*stack, xtable)
}

func (stack *Xtable_stack_t) Pop() Xtable {

	size := len(*stack)

	if size == 0 {
		panic("Cannot Pop(). Xtable_stack is empty.")
	}

	xtable := (*stack)[size-1]

	(*stack)[size-1] = nil

	*stack = (*stack)[:size-1]

	return xtable
}

func (stack *Xtable_stack_t) Top() Xtable {

	size := len(*stack)

	if size == 0 {
		return nil
	}

	return (*stack)[size-1]
}

func (stack *Xtable_stack_t) Is_SELECT_level() bool {

	size := len(*stack)

	if size == 0 {
		return false
	}

	xtable := (*stack)[size-1]

	if xtable.Type() == XT_SELECT {
		return true
	}

	return false
}

func (stack *Xtable_stack_t) Count() int {

	return len(*stack)
}
