package ast

// Prim_type_t and Prim_subtype_t for Token_primary

type (
	Prim_type_t    uint32
	Prim_subtype_t uint32
)

const (
	TOK_PRIMARY_ANCHOR Prim_type_t = 1 << iota // only used by parser.Eat_expression() for the list anchor dummy variable.

	TOK_PRIM_LITERAL_SYSCOLLATOR // leaf node: fr_cs_as
	TOK_PRIM_LITERAL_NULL        // leaf node: NULL
	TOK_PRIM_LITERAL_BOOLEAN     // leaf node: true of false
	TOK_PRIM_LITERAL_HEXASTRING  // leaf node: 0xabcd1234
	TOK_PRIM_LITERAL_STRING      // leaf node: 'bla'
	TOK_PRIM_LITERAL_NUMBER      // leaf node: 123e6  123  123.45 $123.45

	TOK_PRIM_VARIABLE   // leaf node: @myvar
	TOK_PRIM_IDENTIFIER // leaf node: MYDB.sa.CUSTOMERS.NAME
	TOK_PRIM_OPERATOR   // temporary result from operator
	TOK_PRIM_SYSFUNC    // temporary result from builtin function
	TOK_PRIM_UFUNC      // temporary result from user function
	TOK_PRIM_CAST       // temporary result from CAST node

	TOK_PRIM_PLACEHOLDER

	TOK_PRIM_CASE // CASE

	TOK_PRIM_EXISTS // EXISTS

	TOK_PRIM_SUBQUERY // 'select' subquery
)

func (prim_type Prim_type_t) String() string {
	var s string

	switch prim_type {
	case TOK_PRIMARY_ANCHOR:
		s = "TOK_PRIMARY_ANCHOR"

	case TOK_PRIM_LITERAL_SYSCOLLATOR:
		s = "TOK_PRIM_LITERAL_SYSCOLLATOR"
	case TOK_PRIM_LITERAL_NULL:
		s = "TOK_PRIM_LITERAL_NULL"
	case TOK_PRIM_LITERAL_BOOLEAN:
		s = "TOK_PRIM_LITERAL_BOOLEAN"
	case TOK_PRIM_LITERAL_HEXASTRING:
		s = "TOK_PRIM_LITERAL_HEXASTRING"
	case TOK_PRIM_LITERAL_STRING:
		s = "TOK_PRIM_LITERAL_STRING"
	case TOK_PRIM_LITERAL_NUMBER:
		s = "TOK_PRIM_LITERAL_NUMBER"

	case TOK_PRIM_VARIABLE:
		s = "TOK_PRIM_VARIABLE"
	case TOK_PRIM_IDENTIFIER:
		s = "TOK_PRIM_IDENTIFIER"
	case TOK_PRIM_OPERATOR:
		s = "TOK_PRIM_OPERATOR"
	case TOK_PRIM_SYSFUNC:
		s = "TOK_PRIM_SYSFUNC"
	case TOK_PRIM_UFUNC:
		s = "TOK_PRIM_UFUNC"
	case TOK_PRIM_CAST:
		s = "TOK_PRIM_CAST"

	case TOK_PRIM_PLACEHOLDER:
		s = "TOK_PRIM_PLACEHOLDER"

	case TOK_PRIM_CASE:
		s = "TOK_PRIM_CASE"

	case TOK_PRIM_EXISTS:
		s = "TOK_PRIM_EXISTS"

	case TOK_PRIM_SUBQUERY:
		s = "TOK_PRIM_SUBQUERY"

	default:
		panic("Prim_type_t unknown")
	}

	return s
}

const (
	TOK_PRIMARY_ANCHOR_SUB Prim_subtype_t = iota + 1 // only used by parser.Eat_expression() for the list anchor dummy variable.

	TOK_PRIM_LITERAL_SYSCOLLATOR_SUB // fr_cs_as
	TOK_PRIM_LITERAL_NULL_SUB        // NULL
	TOK_PRIM_LITERAL_BOOLEAN_SUB     // true of false
	TOK_PRIM_LITERAL_HEXASTRING_SUB  // 0x1234abef
	TOK_PRIM_LITERAL_STRING_SUB      // 'bla'
	TOK_PRIM_LITERAL_NUMBER_INTEGRAL // 123
	TOK_PRIM_LITERAL_NUMBER_MONEY    // $123.45
	TOK_PRIM_LITERAL_NUMBER_NUMERIC  // 123.45
	TOK_PRIM_LITERAL_NUMBER_FLOAT    // 123.45e3

	TOK_PRIM_VARIABLE_SUB   // @bla
	TOK_PRIM_IDENTIFIER_SUB // MYDB.sa.CUSTOMERS.NAME

	TOK_PRIM_OPERATOR_LOGICAL_AND // temporary result from operator
	TOK_PRIM_OPERATOR_LOGICAL_OR
	TOK_PRIM_OPERATOR_LOGICAL_UNARY_NOT
	TOK_PRIM_OPERATOR_IS_NULL
	TOK_PRIM_OPERATOR_IS_NOT_NULL
	TOK_PRIM_OPERATOR_IN_LIST
	TOK_PRIM_OPERATOR_NOT_IN_LIST
	TOK_PRIM_OPERATOR_LIKE
	TOK_PRIM_OPERATOR_NOT_LIKE
	TOK_PRIM_OPERATOR_ESCAPE // pseudo-operator used for    'a' LIKE 'b' ESCAPE 'c'. This pseudo-operator has no precedence, as it is eaten during processing of LIKE operator.
	TOK_PRIM_OPERATOR_ANY
	TOK_PRIM_OPERATOR_ALL
	TOK_PRIM_OPERATOR_BETWEEN
	TOK_PRIM_OPERATOR_NOT_BETWEEN
	TOK_PRIM_OPERATOR_UNARY_PLUS
	TOK_PRIM_OPERATOR_PLUS
	TOK_PRIM_OPERATOR_UNARY_MINUS
	TOK_PRIM_OPERATOR_MINUS
	TOK_PRIM_OPERATOR_MULT
	TOK_PRIM_OPERATOR_DIV
	TOK_PRIM_OPERATOR_MOD
	TOK_PRIM_OPERATOR_BIT_AND
	TOK_PRIM_OPERATOR_BIT_OR
	TOK_PRIM_OPERATOR_BIT_XOR
	TOK_PRIM_OPERATOR_BIT_UNARY_NOT
	TOK_PRIM_OPERATOR_COMP_EQUAL
	TOK_PRIM_OPERATOR_COMP_GREATER
	TOK_PRIM_OPERATOR_COMP_LESS
	TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL
	TOK_PRIM_OPERATOR_COMP_LESS_EQUAL
	TOK_PRIM_OPERATOR_COMP_NOT_EQUAL

	TOK_PRIM_OPERATOR_ASSIGN_PLUS
	TOK_PRIM_OPERATOR_ASSIGN_MINUS
	TOK_PRIM_OPERATOR_ASSIGN_MULT
	TOK_PRIM_OPERATOR_ASSIGN_DIV
	TOK_PRIM_OPERATOR_ASSIGN_MOD
	TOK_PRIM_OPERATOR_ASSIGN_BIT_AND
	TOK_PRIM_OPERATOR_ASSIGN_BIT_OR
	TOK_PRIM_OPERATOR_ASSIGN_BIT_XOR

	TOK_PRIM_SYSFUNC_SUB

	TOK_PRIM_UFUNC_SUB // temporary result from operator or function

	TOK_PRIM_CAST_UNDEFINED // temporary result from CAST node. This prim_subtype is replaced by TOK_PRIM_CAST_RESTRICT/_ENLARGE/_ADJUST/_COPY during the walking of the ast.
	TOK_PRIM_CAST_RESTRICT  // temporary result from CAST node. Cast datatype to a lower datatype.
	TOK_PRIM_CAST_ENLARGE   // temporary result from CAST node. Cast datatype to a higher datatype.
	TOK_PRIM_CAST_ADJUST    // temporary result from CAST node. Cast datatype to the same datatype, but precision, scale, fixlen_flag or collation is different.
	TOK_PRIM_CAST_COPY      // temporary result from CAST node. Cast datatype to exactly the same datatype and properties. It is simply a copy.
	TOK_PRIM_CAST_ILLEGAL   // temporary result from CAST node. Cast is illegal.

	TOK_PRIM_PLACEHOLDER_DEFAULT          // DEFAULT (in function parameter parsing only)
	TOK_PRIM_PLACEHOLDER_UNQUALIFIED_STAR // unqualified '*'
	TOK_PRIM_PLACEHOLDER_QUALIFIED_STAR   // e.g. MYTABLE.*
	TOK_PRIM_PLACEHOLDER_QUESTIONMARK     // '?' placeholder for prepared statement (ODBC).

	TOK_PRIM_CASE_SUB // CASE

	TOK_PRIM_EXISTS_SUB // EXISTS

	TOK_PRIM_SUBQUERY_MANY // SELECT subquery
	TOK_PRIM_SUBQUERY_ONE  // SELECT subquery
)

func (prim_subtype Prim_subtype_t) String() string {
	var s string

	switch prim_subtype {

	case TOK_PRIMARY_ANCHOR_SUB:
		s = "TOK_PRIMARY_ANCHOR_SUB"

	case TOK_PRIM_LITERAL_SYSCOLLATOR_SUB:
		s = "TOK_PRIM_LITERAL_SYSCOLLATOR_SUB"
	case TOK_PRIM_LITERAL_NULL_SUB:
		s = "TOK_PRIM_LITERAL_NULL_SUB"
	case TOK_PRIM_LITERAL_BOOLEAN_SUB:
		s = "TOK_PRIM_LITERAL_BOOLEAN_SUB"
	case TOK_PRIM_LITERAL_HEXASTRING_SUB:
		s = "TOK_PRIM_LITERAL_HEXASTRING_SUB"
	case TOK_PRIM_LITERAL_STRING_SUB:
		s = "TOK_PRIM_LITERAL_STRING_SUB"
	case TOK_PRIM_LITERAL_NUMBER_INTEGRAL:
		s = "TOK_PRIM_LITERAL_NUMBER_INTEGRAL"
	case TOK_PRIM_LITERAL_NUMBER_MONEY:
		s = "TOK_PRIM_LITERAL_NUMBER_MONEY"
	case TOK_PRIM_LITERAL_NUMBER_NUMERIC:
		s = "TOK_PRIM_LITERAL_NUMBER_NUMERIC"
	case TOK_PRIM_LITERAL_NUMBER_FLOAT:
		s = "TOK_PRIM_LITERAL_NUMBER_FLOAT"

	case TOK_PRIM_VARIABLE_SUB:
		s = "TOK_PRIM_VARIABLE_SUB"
	case TOK_PRIM_IDENTIFIER_SUB:
		s = "TOK_PRIM_IDENTIFIER_SUB"

	case TOK_PRIM_OPERATOR_LOGICAL_AND:
		s = "TOK_PRIM_OPERATOR_LOGICAL_AND"
	case TOK_PRIM_OPERATOR_LOGICAL_OR:
		s = "TOK_PRIM_OPERATOR_LOGICAL_OR"
	case TOK_PRIM_OPERATOR_LOGICAL_UNARY_NOT:
		s = "TOK_PRIM_OPERATOR_LOGICAL_UNARY_NOT"
	case TOK_PRIM_OPERATOR_IS_NULL:
		s = "TOK_PRIM_OPERATOR_IS_NULL"
	case TOK_PRIM_OPERATOR_IS_NOT_NULL:
		s = "TOK_PRIM_OPERATOR_IS_NOT_NULL"
	case TOK_PRIM_OPERATOR_IN_LIST:
		s = "TOK_PRIM_OPERATOR_IN_LIST"
	case TOK_PRIM_OPERATOR_NOT_IN_LIST:
		s = "TOK_PRIM_OPERATOR_NOT_IN_LIST"
	case TOK_PRIM_OPERATOR_LIKE:
		s = "TOK_PRIM_OPERATOR_LIKE"
	case TOK_PRIM_OPERATOR_NOT_LIKE:
		s = "TOK_PRIM_OPERATOR_NOT_LIKE"
	case TOK_PRIM_OPERATOR_ESCAPE:
		s = "TOK_PRIM_OPERATOR_ESCAPE"
	case TOK_PRIM_OPERATOR_ANY:
		s = "TOK_PRIM_OPERATOR_ANY"
	case TOK_PRIM_OPERATOR_ALL:
		s = "TOK_PRIM_OPERATOR_ALL"
	case TOK_PRIM_OPERATOR_BETWEEN:
		s = "TOK_PRIM_OPERATOR_BETWEEN"
	case TOK_PRIM_OPERATOR_NOT_BETWEEN:
		s = "TOK_PRIM_OPERATOR_NOT_BETWEEN"
	case TOK_PRIM_OPERATOR_UNARY_PLUS:
		s = "TOK_PRIM_OPERATOR_UNARY_PLUS"
	case TOK_PRIM_OPERATOR_PLUS:
		s = "TOK_PRIM_OPERATOR_PLUS"
	case TOK_PRIM_OPERATOR_UNARY_MINUS:
		s = "TOK_PRIM_OPERATOR_UNARY_MINUS"
	case TOK_PRIM_OPERATOR_MINUS:
		s = "TOK_PRIM_OPERATOR_MINUS"
	case TOK_PRIM_OPERATOR_MULT:
		s = "TOK_PRIM_OPERATOR_MULT"
	case TOK_PRIM_OPERATOR_DIV:
		s = "TOK_PRIM_OPERATOR_DIV"
	case TOK_PRIM_OPERATOR_MOD:
		s = "TOK_PRIM_OPERATOR_MOD"
	case TOK_PRIM_OPERATOR_BIT_AND:
		s = "TOK_PRIM_OPERATOR_BIT_AND"
	case TOK_PRIM_OPERATOR_BIT_OR:
		s = "TOK_PRIM_OPERATOR_BIT_OR"
	case TOK_PRIM_OPERATOR_BIT_XOR:
		s = "TOK_PRIM_OPERATOR_BIT_XOR"
	case TOK_PRIM_OPERATOR_BIT_UNARY_NOT:
		s = "TOK_PRIM_OPERATOR_BIT_UNARY_NOT"
	case TOK_PRIM_OPERATOR_COMP_EQUAL:
		s = "TOK_PRIM_OPERATOR_COMP_EQUAL"
	case TOK_PRIM_OPERATOR_COMP_GREATER:
		s = "TOK_PRIM_OPERATOR_COMP_GREATER"
	case TOK_PRIM_OPERATOR_COMP_LESS:
		s = "TOK_PRIM_OPERATOR_COMP_LESS"
	case TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL:
		s = "TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL"
	case TOK_PRIM_OPERATOR_COMP_LESS_EQUAL:
		s = "TOK_PRIM_OPERATOR_COMP_LESS_EQUAL"
	case TOK_PRIM_OPERATOR_COMP_NOT_EQUAL:
		s = "TOK_PRIM_OPERATOR_COMP_NOT_EQUAL"

	case TOK_PRIM_OPERATOR_ASSIGN_PLUS:
		s = "TOK_PRIM_OPERATOR_ASSIGN_PLUS"
	case TOK_PRIM_OPERATOR_ASSIGN_MINUS:
		s = "TOK_PRIM_OPERATOR_ASSIGN_MINUS"
	case TOK_PRIM_OPERATOR_ASSIGN_MULT:
		s = "TOK_PRIM_OPERATOR_ASSIGN_MULT"
	case TOK_PRIM_OPERATOR_ASSIGN_DIV:
		s = "TOK_PRIM_OPERATOR_ASSIGN_DIV"
	case TOK_PRIM_OPERATOR_ASSIGN_MOD:
		s = "TOK_PRIM_OPERATOR_ASSIGN_MOD"
	case TOK_PRIM_OPERATOR_ASSIGN_BIT_AND:
		s = "TOK_PRIM_OPERATOR_ASSIGN_BIT_AND"
	case TOK_PRIM_OPERATOR_ASSIGN_BIT_OR:
		s = "TOK_PRIM_OPERATOR_ASSIGN_BIT_OR"
	case TOK_PRIM_OPERATOR_ASSIGN_BIT_XOR:
		s = "TOK_PRIM_OPERATOR_ASSIGN_BIT_XOR"

	case TOK_PRIM_SYSFUNC_SUB:
		s = "TOK_PRIM_SYSFUNC_SUB"

	case TOK_PRIM_UFUNC_SUB:
		s = "TOK_PRIM_UFUNC_SUB"

	case TOK_PRIM_CAST_UNDEFINED:
		s = "TOK_PRIM_CAST_UNDEFINED"
	case TOK_PRIM_CAST_RESTRICT:
		s = "TOK_PRIM_CAST_RESTRICT"
	case TOK_PRIM_CAST_ENLARGE:
		s = "TOK_PRIM_CAST_ENLARGE"
	case TOK_PRIM_CAST_ADJUST:
		s = "TOK_PRIM_CAST_ADJUST"
	case TOK_PRIM_CAST_COPY:
		s = "TOK_PRIM_CAST_COPY"
	case TOK_PRIM_CAST_ILLEGAL:
		s = "TOK_PRIM_CAST_ILLEGAL"

	case TOK_PRIM_PLACEHOLDER_DEFAULT:
		s = "TOK_PRIM_PLACEHOLDER_DEFAULT"
	case TOK_PRIM_PLACEHOLDER_UNQUALIFIED_STAR:
		s = "TOK_PRIM_PLACEHOLDER_UNQUALIFIED_STAR"
	case TOK_PRIM_PLACEHOLDER_QUALIFIED_STAR:
		s = "TOK_PRIM_PLACEHOLDER_QUALIFIED_STAR"
	case TOK_PRIM_PLACEHOLDER_QUESTIONMARK:
		s = "TOK_PRIM_PLACEHOLDER_QUESTIONMARK"

	case TOK_PRIM_CASE_SUB:
		s = "TOK_PRIM_CASE_SUB"

	case TOK_PRIM_EXISTS_SUB:
		s = "TOK_PRIM_EXISTS_SUB"

	case TOK_PRIM_SUBQUERY_MANY:
		s = "TOK_PRIM_SUBQUERY_MANY"
	case TOK_PRIM_SUBQUERY_ONE:
		s = "TOK_PRIM_SUBQUERY_ONE"

	default:
		panic("Prim_subtype_t unknown")
	}

	return s
}
