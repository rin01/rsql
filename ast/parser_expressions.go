package ast

import (
	"rsql"
	"rsql/lex"
)

const (
	TOKEN_TABLE_NUMBER_OF_PARTS             = 3 // db schema table
	TOKEN_COLUMN_NUMBER_OF_PARTS            = 4 // db schema table column
	TOKEN_UFUNC_NUMBER_OF_PARTS             = 3 // server db schema function
	SPEC_QUALIFIED_NAME_NUMBER_OF_PARTS_MAX = 4 // max number of parts of a qualified name, e.g. mydb.dbo.mytable.mycol.
)

// All expressions are made up of Token_primary tokens.
// An expression is a tree of these tokens.
//
type Token_primary struct {
	Token // tok_category is always TOK_PRIMARY

	Prim_type    Prim_type_t
	Prim_subtype Prim_subtype_t

	Prim_database_name string
	Prim_schema_name   string
	Prim_table_name    string // in fact, it is the exposed name. So, it may be an alias name.
	Prim_name          string // for variable name and column name, literal string and number, operator

	Prim_sql_datatype    rsql.Datatype_t // for variable declaration and explicit CAST token. (never contains DATATYPE_CHAR, it is DATATYPE_VARCHAR with fixlen_flag==true instead)
	Prim_sql_precision   uint16          // same
	Prim_sql_scale       uint16          // same
	Prim_sql_fixlen_flag bool            // same
	Prim_sql_collate     string          // same. For COLLATE clause. Note that COLLATE clause for variable declaration (DECLARE statement) is forbidden. A VARCHAR variable has always the server default collation, like MS SQL Server.

	Prim_sql_datepartfield Datepartfield_t // datepartfield argument for DATEPART() and DATENAME() functions, like DATEPARTFIELD_YEAR etc.

	prim_precedence                    Prim_precedence_t    // operator: precedence level
	prim_associativity                 Prim_associativity_t // operator: right or left associativity
	Prim_cardinality                   Prim_cardinality_t   // operator: number of operands. It indicates to parser_eat_expression() how it must reorganize the neighbour tokens.
	Prim_any_all_comp_operator_subtype Prim_subtype_t       // Prim_subtype of the comparison operator for ANY and ALL operators

	Prim_status Prim_status_t // operator: in the expression reorganization, TOK_PRIM_STATUS_UNCOMPLETE, TOK_PRIM_STATUS_COMPLETE.

	Prim_collation_strength rsql.Collstrength_t // used by the decorator, computed from Prim_sql_collate or arguments collations. COLLSTRENGTH_0 (server default collation), COLLSTRENGTH_1 (for column identifier), COLLSTRENGTH_2 (COLLATE clause)
	Prim_collation          string              // same

	Prim_sysfunc_descr *Sysfunc_description // see G_SYSFUNC_DESCRIPTION_MAP
	Prim_left          *Token_primary       // left operand. For TOK_PRIM_EXISTS, points to TOK_PRIM_SUBQUERY_MANY.
	Prim_right         *Token_primary       // right operand. For TOK_PRIM_OPERATOR_IN_SUBQUERY, points to a subquery. For TOK_PRIM_OPERATOR_ALL/ANY, points to the comparison operator token.
	Prim_listexpr      []*Token_primary     // function arguments
	Prim_syscollator   *Token_primary       // supplementary operand. Points to a syscollator token if and only if the operator or function needs it. It must be a literal value or a variable, because the decorator doesn't walk it to emit bytecode.
	Prim_syslanguage   *Token_primary       // supplementary operand. Points to a syslanguage token if and only if the operator or function needs it. It must be a literal value or a variable, because the decorator doesn't walk it to emit bytecode.

	// Prim_identifier_is_external_flag bool  // used when decorating SELECT. True if TOK_PRIM_IDENTIFIER token's dataslot is outside the current SELECT.

	Prim_instruction_code rsql.Instrcode_t // INSTR_ADD_INT, INSTR_SUBSTR_INT, etc

	Prim_xtable_subquery Xtable // for TOK_PRIM_SUBQUERY only.
	//
	// Prim_xtable_namespace Xtable // TOK_PRIM_IDENTIFIER points to the Xtable at the origin of its namespace, which is Xtable_select, Xtable_join, or Xtable_union. (but not Xtable_table).
	//                                                      // RSQL_SYSFUNC_AGGR_COUNT, SUM, etc point to the Xtable_select they belong to.
	//
	//      Token_primary   *prim_clone_working;            // just a working pointer to the cloned token, used by ast_clone_subtree() in the parser to clone a subtree for GROUP BY
	//
	Prim_xtable_ds Xtable // xtable to which Prim_dataslot belongs. Can be a real table, SELECT or UNION.
	Prim_col_no    uint16 // column number of dataslot in real table, SELECT or UNION
	Prim_dataslot  rsql.IDataslot
}

func (tok *Token_primary) clone_shallow() *Token_primary {

	tok_clone := &Token_primary{}
	*tok_clone = *tok

	return tok_clone
}

// New_token_primary is used each time we need a new Token_primary.
//
func (parser *Parser) New_token_primary(prim_type Prim_type_t, prim_subtype Prim_subtype_t, prim_lexeme lex.Lexeme) *Token_primary {

	tok := &Token_primary{}
	parser.token_init(&tok.Token, TOK_PRIMARY)

	tok.Prim_type = prim_type
	tok.Prim_subtype = prim_subtype

	tok.Prim_name = prim_lexeme.Lex_word

	tok.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_INVALID_VALUE       // invalid precedence    to catch bug if a precedence has not been set.
	tok.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_INVALID_VALUE // invalid associativity to catch bug if a precedence has not been set.
	tok.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_INVALID_VALUE     // invalid cardinality   to catch bug if a precedence has not been set.

	tok.Prim_status = TOK_PRIM_STATUS_UNCOMPLETE

	return tok
}

// New_TOK_PRIM_IDENTIFIER is used when parsing SELECT, to create column Token_primary for '*' star placeholder.
//
func New_TOK_PRIM_IDENTIFIER(ident_string string, tok_batch_line lex.Coord_t) *Token_primary {

	tok := &Token_primary{}
	tok.Tok_category = TOK_PRIMARY
	tok.Tok_batch_line = tok_batch_line

	tok.Prim_type = TOK_PRIM_IDENTIFIER
	tok.Prim_subtype = TOK_PRIM_IDENTIFIER_SUB

	tok.Prim_name = ident_string

	tok.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_INVALID_VALUE       // invalid precedence    to catch bug if a precedence has not been set.
	tok.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_INVALID_VALUE // invalid associativity to catch bug if a precedence has not been set.
	tok.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_INVALID_VALUE     // invalid cardinality   to catch bug if a precedence has not been set.

	tok.Prim_status = TOK_PRIM_STATUS_UNCOMPLETE

	return tok
}

// Eat_variable creates a 'variable' token from the current lexeme.
//
// NOTE: the current lexeme MUST be LEX_VARIABLE.
//
func (parser *Parser) Eat_variable() (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_VARIABLE)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_VARIABLE, TOK_PRIM_VARIABLE_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_sysvar creates a 'system variable' token from the current lexeme, like _@system_user_name, _@current_user, etc.
//
// NOTE: pass as argument lex.LEXEME_SYSVAR_SYSTEM_USER_NAME, etc. See static_tables.go file.
//
func (parser *Parser) Eat_sysvar(lexeme lex.Lexeme) (*Token_primary, *rsql.Error) {

	assert(lexeme.Lex_type == lex.LEX_VARIABLE) // _@system_user_name, etc

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_VARIABLE, TOK_PRIM_VARIABLE_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_atfunc creates a system @@function token from the current lexeme.
//
// NOTE: the current lexeme MUST be LEX_ATFUNC.
//
func (parser *Parser) Eat_atfunc() (*Token_primary, *rsql.Error) {
	var (
		ok                  bool
		sysfunc_description *Sysfunc_description
		sysfunc_id          Sysfunc_id_t
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_ATFUNC)

	/* check if it is a @@ builtin function */

	if sysfunc_description, ok = G_SYSFUNC_DESCRIPTION_MAP[lexeme.Lex_word]; ok == false {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ATFUNC_NOT_FOUND, rsql.ERROR_BATCH_ABORT, lexeme.Lex_word)
	}

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_SYSFUNC, TOK_PRIM_SYSFUNC_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	token_primary.Prim_sysfunc_descr = sysfunc_description

	/* special modification for some functions */

	sysfunc_id = sysfunc_description.Sd_id
	if sysfunc_id == SYSFUNC_atatDATEFIRST || sysfunc_id == SYSFUNC_atatDATEFORMAT || sysfunc_id == SYSFUNC_atatLANGUAGE {
		token_primary.Prim_listexpr = []*Token_primary{parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE)}
	}

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_literal_string creates a 'literal string' token from the current lexeme.
//
// NOTE; the current lexeme MUST be LEX_LITERAL_STRING.
//
func (parser *Parser) Eat_literal_string() (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_LITERAL_STRING)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_STRING, TOK_PRIM_LITERAL_STRING_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_literal_hexastring creates a 'literal hexastring' token from the current lexeme.
//
// NOTE; the current lexeme MUST be LEX_LITERAL_HEXASTRING.
//
func (parser *Parser) Eat_literal_hexastring() (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_LITERAL_HEXASTRING)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_HEXASTRING, TOK_PRIM_LITERAL_HEXASTRING_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_literal_number creates a 'literal number' token from the current lexeme.
//
// NOTE; the current lexeme MUST be LEX_LITERAL_NUMBER.
//
func (parser *Parser) Eat_literal_number() (*Token_primary, *rsql.Error) {
	var (
		prim_subtype Prim_subtype_t
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_LITERAL_NUMBER)

	/* create token */

	switch lexeme.Lex_subtype {
	case lex.LEX_LITERAL_NUMBER_FLOAT:
		prim_subtype = TOK_PRIM_LITERAL_NUMBER_FLOAT

	case lex.LEX_LITERAL_NUMBER_NUMERIC:
		prim_subtype = TOK_PRIM_LITERAL_NUMBER_NUMERIC

	case lex.LEX_LITERAL_NUMBER_INTEGRAL:
		prim_subtype = TOK_PRIM_LITERAL_NUMBER_INTEGRAL

	case lex.LEX_LITERAL_NUMBER_MONEY:
		prim_subtype = TOK_PRIM_LITERAL_NUMBER_MONEY

	default:
		panic("lex subtype unknown")
	}

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_NUMBER, prim_subtype, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_literal_NULL creates a 'literal NULL' token from the current lexeme.
//
// NOTE; the current lexeme MUST be LEX_KEYWORD_NULL.
//
func (parser *Parser) Eat_literal_NULL() (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_subtype == lex.LEX_KEYWORD_NULL)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_LITERAL_NULL, TOK_PRIM_LITERAL_NULL_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_placeholder_QUESTIONMARK creates a placeholder '?' token from the current lexeme.
// Used as placeholder for a parameter, in prepared statement for ODBC.
//
// NOTE; the current lexeme MUST be LEX_QUESTIONMARK.
//
func (parser *Parser) Eat_placeholder_QUESTIONMARK() (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_QUESTIONMARK)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_PLACEHOLDER, TOK_PRIM_PLACEHOLDER_QUESTIONMARK, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_placeholder_DEFAULT creates a placeholder 'DEFAULT' token from the current lexeme.
// Used only for INSERT VALUES(DEFAULT) or constraint clause.
//
// NOTE: the current lexeme MUST be LEX_KEYWORD_DEFAULT.
//
func (parser *Parser) Eat_placeholder_DEFAULT() (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_subtype == lex.LEX_KEYWORD_DEFAULT)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_PLACEHOLDER, TOK_PRIM_PLACEHOLDER_DEFAULT, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_placeholder_UNQUALIFIED_STAR creates a placeholder for unqualified star '*' token from the current lexeme.
//
// NOTE: the current lexeme MUST be LEX_OPERATOR_MULT.
//
func (parser *Parser) Eat_placeholder_UNQUALIFIED_STAR() (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_subtype == lex.LEX_OPERATOR_MULT)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_PLACEHOLDER, TOK_PRIM_PLACEHOLDER_UNQUALIFIED_STAR, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE // in fact, status is not used

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_placeholder_QUALIFIED_STAR creates a placeholder for qualified star '*' token from the current lexeme.
//
// NOTE: the current lexeme MUST be LEX_IDENTPART or LEX_DOT.
//
func (parser *Parser) Eat_placeholder_QUALIFIED_STAR() (*Token_primary, *rsql.Error) {
	var (
		rsql_err      *rsql.Error
		database_name string
		schema_name   string
		table_name    string
	)

	assert(parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT) != 0)

	if database_name, schema_name, table_name, _, rsql_err = parser.Eat_prefix(); rsql_err != nil { // entire prefix is eaten.
		return nil, rsql_err
	}

	if table_name == "" {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_PREFIX_WITHOUT_TABLE_NAME, rsql.ERROR_BATCH_ABORT) // like MS Sql Server
	}

	if parser.Current_lexeme.Lex_subtype != lex.LEX_OPERATOR_MULT {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STAR_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_PLACEHOLDER, TOK_PRIM_PLACEHOLDER_QUALIFIED_STAR, parser.Current_lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE // in fact, status is not used

	token_primary.Prim_database_name = database_name
	token_primary.Prim_schema_name = schema_name
	token_primary.Prim_table_name = table_name

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_qualified_identifier creates a qualified identifier token from the current lexeme.
// The prefix has already been eaten and the prefix parts are passed as arguments.
//
// NOTE: the current lexeme MUST be LEX_IDENTPART.
//
func (parser *Parser) Eat_qualified_identifier(database_name, schema_name, table_name string) (*Token_primary, *rsql.Error) {

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_IDENTPART)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_IDENTIFIER, TOK_PRIM_IDENTIFIER_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	token_primary.Prim_database_name = database_name
	token_primary.Prim_schema_name = schema_name
	token_primary.Prim_table_name = table_name

	/* go to next lexeme */

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil {
		return nil, rsql_err
	}

	return token_primary, nil
}

// Eat_ufunc creates a user function token from the current lexeme.
//
// Explicit schema name is mandatory in user function name. If not, a syntax error is raised.
//
// The prefix has already been eaten and the prefix parts are passed as arguments.
//
// NOTE: the current lexeme MUST be LEX_IDENTPART.
//
func (parser *Parser) Eat_ufunc(database_name, schema_name string) (*Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		listexpr []*Token_primary
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_IDENTPART)

	/* create token */

	if schema_name == "" { // schema is mandatory for user function name
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_USER_FUNCTION_MISSING_SCHEMA, rsql.ERROR_BATCH_ABORT, lexeme.Lex_word)
	}

	token_primary := parser.New_token_primary(TOK_PRIM_UFUNC, TOK_PRIM_UFUNC_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	token_primary.Prim_database_name = database_name
	token_primary.Prim_schema_name = schema_name

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip function name
		return nil, rsql_err
	}

	/* eat function arguments */

	if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
		return nil, rsql_err
	}

	if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // eat argument list if not void
		if listexpr, rsql_err = parser.Eat_list_of_function_arguments(); rsql_err != nil { // list of pointers to Token_primaries
			return nil, rsql_err
		}
	}

	if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
		return nil, rsql_err
	}

	token_primary.Prim_listexpr = listexpr // put argument list in Token_primary

	return token_primary, nil
}

// Eat_sysfunc creates a system function token from the current lexeme.
//
// NOTE: the current lexeme MUST be LEX_IDENTPART or LEX_KEYWORD.
//
// This function eats the arguments of the function. The following cases can happen :
//      - lexeme is LEX_IDENTPART : normal system function, like SIN() or GETDATE()
//      - lexeme is LEX_KEYWORD   : LEFT() or RIGHT() normal system function.
//
// This function doesn't eat the special builtin functions @@ERROR, @@ROWCOUNT etc. Use Eat_atfunc() to eat them.
// This function doesn't eat SYSTEM_USER, CURRENT_DATE, etc, because they are not followed by parentheses and are keywords, and thus processed as keywords.
//
func (parser *Parser) Eat_sysfunc(sysfunc_description *Sysfunc_description) (*Token_primary, *rsql.Error) {
	var (
		rsql_err            *rsql.Error
		sysfunc_id          Sysfunc_id_t
		ok                  bool
		datepartfield_value Datepartfield_t
		listexpr            []*Token_primary
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_KEYWORD) != 0) // note:  LEFT() RIGHT() etc are LEX_KEYWORD

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_SYSFUNC, TOK_PRIM_SYSFUNC_SUB, lexeme)
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE
	token_primary.Prim_sysfunc_descr = sysfunc_description

	function_name := lexeme

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip function name
		return nil, rsql_err
	}

	if sysfunc_description.Is_not_parenthesized() { // SYSFUNC_GETDATE when parsing CURRENT_TIMESTAMP, etc has no parentheses and no argument
		goto LABEL_SKIP_ARGUMENT_PARSING // =====> skip argument parsing
	}

	/* eat function arguments */

	if sysfunc_description.Is_aggregate() == true {
		if parser.aggregate_func_allowed_flag == false { // aggregate functions cannot be nested
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_AGGREGATE_FUNCTION_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT, function_name.Lex_word)
		}

		defer func() { parser.aggregate_func_allowed_flag = true }()

		parser.aggregate_func_allowed_flag = false // argument of aggregate function should not contain an aggregate function
		parser.aggregate_func_detected = true
	}

	if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
		return nil, rsql_err
	}

	// if DATEPART() etc, the first arg is a datepart field. E.g. DATEPART(yyyy, '19660712')

	if sysfunc_description.First_arg_is_datepartfield() {
		datepartfield_lexeme := parser.Current_lexeme // e.g. yyyy, month, etc

		if datepartfield_lexeme.Lex_type != lex.LEX_IDENTPART { // current lexeme must be an identpart
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_DATEPARTFIELD, rsql.ERROR_BATCH_ABORT, function_name.Lex_word, datepartfield_lexeme.Lex_word)
		}

		if datepartfield_value, ok = G_DATEPARTFIELD_TO_VALUE[datepartfield_lexeme.Lex_word]; ok == false {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_DATEPARTFIELD, rsql.ERROR_BATCH_ABORT, function_name.Lex_word, datepartfield_lexeme.Lex_word)
		}

		token_primary.Prim_sql_datepartfield = datepartfield_value

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip datepart field
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // expect comma
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SYSFUNC_ARGUMENT_EXPECTED, rsql.ERROR_BATCH_ABORT, function_name.Lex_word)
		}

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type == lex.LEX_RPAREN { // at least one argument is expected after comma
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SYSFUNC_ARGUMENT_EXPECTED, rsql.ERROR_BATCH_ABORT, function_name.Lex_word)
		}
	}

	// if COUNT, it can be COUNT(*). Same for COUNT_BIG.

	if (sysfunc_description.Sd_id == SYSFUNC_AGGR_COUNT || sysfunc_description.Sd_id == SYSFUNC_AGGR_COUNT_BIG) && parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_MULT {
		tok_const_1 := parser.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(1)
		listexpr = append(listexpr, tok_const_1)
		token_primary.Prim_listexpr = listexpr

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip star '*'
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
		}
	}

	// eat remaining arguments

	if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // eat argument list if not empty
		if listexpr, rsql_err = parser.Eat_list_of_function_arguments(); rsql_err != nil { // list of pointers to Token_primaries
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
		}
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
		return nil, rsql_err
	}

	// put argument list in Token_primary

	token_primary.Prim_listexpr = listexpr

	// transform function token for some special cases

LABEL_SKIP_ARGUMENT_PARSING:

	sysfunc_id = sysfunc_description.Sd_id

	switch {
	case sysfunc_id == SYSFUNC_YEAR || sysfunc_id == SYSFUNC_MONTH || sysfunc_id == SYSFUNC_DAY: // if YEAR() or MONTH() or DAY(), transform to DATEPART()
		parser.transform_YEAR_MONTH_DAY_to_DATEPART(token_primary)

	case sysfunc_id == SYSFUNC_NULLIF: // if NULLIF, transform to IIF
		if rsql_err = parser.transform_NULLIF_to_IIF(token_primary); rsql_err != nil {
			return nil, rsql_err
		}

	case sysfunc_id == SYSFUNC_GETUTCDATE: // if GETUTCDATE() or SYSUTCDATETIME(), transform token into system variable
		token_primary.Prim_type = TOK_PRIM_VARIABLE
		token_primary.Prim_subtype = TOK_PRIM_VARIABLE_SUB
		token_primary.Prim_name = lex.LEXEME_SYSVAR_CURRENT_TIMESTAMP.Lex_word
		token_primary.Prim_sysfunc_descr = nil

	case sysfunc_id == SYSFUNC_UTC_TO_LOCAL: // if GETDATE() or SYSDATETIME()
		parameter_token := parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_TIMESTAMP)
		token_primary.Prim_listexpr = append(token_primary.Prim_listexpr, parameter_token)
		token_primary.Prim_name = lex.LEXEME_DISPLAYWORD_UTC_TO_LOCAL.Lex_word

	case sysfunc_description.Is_sysvar_if_no_arg(): // SUSER_NAME(), DB_NAME(), etc are replaced by system variable, e.g. _@system_user_name
		if len(listexpr) == 0 {
			switch sysfunc_id {
			case SYSFUNC_SUSER_NAME:
				lexeme = lex.LEXEME_SYSVAR_SYSTEM_USER_NAME
			case SYSFUNC_SUSER_ID:
				lexeme = lex.LEXEME_SYSVAR_SYSTEM_USER_ID
			case SYSFUNC_DB_NAME:
				lexeme = lex.LEXEME_SYSVAR_CURRENT_DB_NAME
			case SYSFUNC_DB_ID:
				lexeme = lex.LEXEME_SYSVAR_CURRENT_DB_ID
			case SYSFUNC_SCHEMA_NAME:
				lexeme = lex.LEXEME_SYSVAR_CURRENT_SCHEMA_NAME
			case SYSFUNC_SCHEMA_ID:
				lexeme = lex.LEXEME_SYSVAR_CURRENT_SCHEMA_ID
			case SYSFUNC_USER_NAME:
				lexeme = lex.LEXEME_SYSVAR_CURRENT_USER_NAME
			case SYSFUNC_USER_ID:
				lexeme = lex.LEXEME_SYSVAR_CURRENT_USER_ID
			default:
				panic("impossible")
			}

			token_primary.Prim_type = TOK_PRIM_VARIABLE // transform token into system variable
			token_primary.Prim_subtype = TOK_PRIM_VARIABLE_SUB
			token_primary.Prim_name = lexeme.Lex_word
			token_primary.Prim_sysfunc_descr = nil
		}

	case sysfunc_id == SYSFUNC_AGGR_AVG: // transform avg(x) into sum(x)/count(x)
		if rsql_err := parser.transform_AVG_to_SUM_div_COUNT(token_primary); rsql_err != nil {
			return nil, rsql_err
		}
	}

	return token_primary, nil
}

// Eat_CONVERT creates a TOK_PRIM_CAST token from the current lexeme.
//
// NOTE: the current lexeme MUST be LEX_KEYWORD_CONVERT.
//
// This function eats the arguments of the CONVERT function which syntax is CONVERT(datatype, expression [, style ] ).
//
func (parser *Parser) Eat_CONVERT() (*Token_primary, *rsql.Error) {
	var (
		rsql_err        *rsql.Error
		sql_datatype    rsql.Datatype_t
		sql_precision   uint16
		sql_scale       uint16
		sql_fixlen_flag bool

		token_expression *Token_primary
		token_style      *Token_primary
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_subtype == lex.LEX_KEYWORD_CONVERT)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_CAST, TOK_PRIM_CAST_UNDEFINED, lexeme) // note: TOK_PRIM_CAST_UNDEFINED will be redefined into TOK_PRIM_CAST_RESTRICT/_ENLARGE/_ADJUST/_COPY during the decoration of ast

	function_name := lexeme

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip function name
		return nil, rsql_err
	}

	/* === eat arguments ===*/

	if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
		return nil, rsql_err
	}

	/* eat builtin datatype */

	if sql_datatype, sql_precision, sql_scale, sql_fixlen_flag, rsql_err = parser.Eat_builtin_datatype(EAT_DATATYPE_CAST); rsql_err != nil {
		return nil, rsql_err
	}

	token_primary.Prim_sql_datatype = sql_datatype
	token_primary.Prim_sql_precision = sql_precision
	token_primary.Prim_sql_scale = sql_scale
	token_primary.Prim_sql_fixlen_flag = sql_fixlen_flag

	if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // expect comma
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SYSFUNC_ARGUMENT_EXPECTED, rsql.ERROR_BATCH_ABORT, function_name.Lex_word)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
		return nil, rsql_err
	}

	/* eat expression */

	if token_expression, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat expression
		return nil, rsql_err
	}

	/* eat style if any */

	if parser.Current_lexeme.Lex_type == lex.LEX_COMMA { // if comma
		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}

		if token_style, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat style
			return nil, rsql_err
		}
	}

	/* eat rparen */

	if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
		return nil, rsql_err
	}

	/* fill CAST token */

	token_primary.Prim_left = token_expression
	token_primary.Prim_right = token_style

	token_primary.Prim_name = lex.LEXEME_KEYWORD_CAST.Lex_word // instead of LEXEME_KEYWORD_CONVERT

	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary, nil
}

// Eat_CAST creates a CAST token from the current lexemes.
//
// NOTE: the current lexeme MUST be LEX_KEYWORD_CAST.
//
// This function eats the arguments of the CAST function which syntax is CAST(expression AS datatype).
//
func (parser *Parser) Eat_CAST() (*Token_primary, *rsql.Error) {
	var (
		rsql_err        *rsql.Error
		sql_datatype    rsql.Datatype_t
		sql_precision   uint16
		sql_scale       uint16
		sql_fixlen_flag bool

		token_expression *Token_primary
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_subtype == lex.LEX_KEYWORD_CAST)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_CAST, TOK_PRIM_CAST_UNDEFINED, lexeme) // note: TOK_PRIM_CAST_UNDEFINED will be redefined into TOK_PRIM_CAST_RESTRICT/_ENLARGE/_ADJUST/_COPY during the decoration of ast

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip function name
		return nil, rsql_err
	}

	/* === eat arguments ===*/

	if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
		return nil, rsql_err
	}

	/* eat expression */

	if token_expression, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat expression
		return nil, rsql_err
	}

	/* eat mandatory AS */

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_AS { // if AS missing, return error.
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_AS, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip AS
		return nil, rsql_err
	}

	/* eat builtin datatype */

	if sql_datatype, sql_precision, sql_scale, sql_fixlen_flag, rsql_err = parser.Eat_builtin_datatype(EAT_DATATYPE_CAST); rsql_err != nil {
		return nil, rsql_err
	}

	token_primary.Prim_sql_datatype = sql_datatype
	token_primary.Prim_sql_precision = sql_precision
	token_primary.Prim_sql_scale = sql_scale
	token_primary.Prim_sql_fixlen_flag = sql_fixlen_flag

	/* eat rparen */

	if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
		return nil, rsql_err
	}

	/* fill CAST token */

	token_primary.Prim_left = token_expression

	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary, nil
}

// Eat_COLLATE eats a COLLATE clause and returns the collation.
//
// NOTE: the current lexeme MUST be LEX_KEYWORD_COLLATE.
//
// This function eats the collation locale argument of COLLATE.
//
func (parser *Parser) Eat_COLLATE() (collation string, rsql_err *rsql.Error) {

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_COLLATE)

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip COLLATE
		return "", rsql_err
	}

	//==== normalize and check if collation locale is valid, like  fr_cs_as ====

	if collation, rsql_err = parser.Eat_identpart_collation_normalized(); rsql_err != nil {
		return "", rsql_err
	}

	return collation, nil
}

// Eat_operator creates an 'operator' token from the current lexeme.
//
// NOTE: the current lexeme MUST be LEX_OPERATOR.
//
//     If flag_unary is EAT_OPERATOR_UNARY, the operator beeing eaten must be a unary operator  '+'  '-'  '~'  'NOT'. Else, an error is returned.
//
//     Only operators allowed in expressions are eaten.
//     That means that compound assignment operators like '+=', '*=', etc return a syntax error.
//
//     NOTE: An expression is made up of operators and terms. An operator has properties like precedence, associativity, cardinality.
//
//     The attribute 'Prim_status' is TOK_PRIM_STATUS_UNCOMPLETE, so that it cannot be grafted as child of another operator during constructing of AST for the expression.
//     The function parser.Eat_expression() grafts the needed operands (only elements with status TOK_PRIM_STATUS_COMPLETE are graftable) to each operator, which status will then change to TOK_PRIM_STATUS_COMPLETE.
//
//     Argument flag_unary:
//           - EAT_OPERATOR_UNARY      : eat a unary operator. The function returns an error if the operator is not unary.
//           - EAT_OPERATOR_NON_UNARY  : eat a non-unary operator.
//
//     NOTE: There is an interesting problem with IN operator. We can have :
//           - IN ( list of arithmetic expression )
//           - IN ( SELECT... )
//
//             But the case IN ( (select name from employee) ) is difficult to solve.
//               - for MS SQL Server, it is a query returning multiple rows. It uses a bottom-up parser and decides it is a query because the query is not involved in artihmetic operation (e.g. (select name from employee)+'XX'), and there is no other element in the list either.
//               - but for us, it is a subquery that must return one row. Because we use a top-down predictive parser and when parsing the first element, we must decide whether it is a query or an arithmetic expression. We cannot delay the decision.
//                 We do this by checking the first lexeme. If it is a LEX_KEYWORD_SELECT, we choose to call the function that parses a query. Else, as in our example where the first lexeme is a LEX_LPAREN, we call the function that parses a list of expression.
//                 Note that this is the only pathological case, as a parenthesized select expression is normally followed by a UNION or arithmetic operator. The former will return a syntax error (which is better that running silently and wrongly), and the latter is a correct arithmetic expression in the list.
//                 And our pathological example will return an error at execution time if it returns more than one row. So, our SQL Server may return errors either at compile time or run time in this crazy case, but doesn't return false results.
//
//           See also parser.Eat_term() where there is a similar problem.
//
//     IN (subquery) is replaced by equivalent '=ANY' operator.
//     NOT IN (subquery) is replaced by equivalent '<>ALL' operator.
//
//
func (parser *Parser) Eat_operator(flag_unary bool) (*Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error

		prim_subtype                       Prim_subtype_t
		prim_any_all_comp_operator_subtype Prim_subtype_t // only for IN (subquery), ANY, ALL

		prim_precedence    Prim_precedence_t    // parser.Eat_expression() will reorganize the neighbour tokens of operators by precedence order.
		prim_associativity Prim_associativity_t // most operators associativity is --->
		prim_cardinality   Prim_cardinality_t   // number of operands for the operator. It indicates to parser.Eat_expression() how it must reorganize the neighbour tokens.
		// prim_status                          // default TOK_PRIM_STATUS_UNCOMPLETE.

		prim_listexpr []*Token_primary // only for IN operator, list of expressions ( but not for subquery which is stored in Prim_right ).

		NOT_modifier_flag        bool = false // when eating binary operator, NOT can be a modifier to IN, LIKE and BETWEEN.
		comparison_operator_flag bool         // indicates a comparison operator, because we must then check if ANY or ALL follows.
		any_all_subtype          Prim_subtype_t
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_type == lex.LEX_OPERATOR)

	// note: NOT can be a unary operator     : NOT @a > 100
	//       or be part of a binary operator : NOT IN, NOT LIKE, NOT BETWEEN          (for parsing, BETWEEN is considered as a binary operator)

	/* if flag_unary is EAT_OPERATOR_UNARY, return error if operator is not unary */

	switch flag_unary {
	case EAT_OPERATOR_UNARY: // unary operator is expected
		switch lexeme.Lex_subtype {
		case lex.LEX_OPERATOR_PLUS, //  '+'     is unary or binary operator
			lex.LEX_OPERATOR_MINUS,       //  '-'     is unary or binary operator
			lex.LEX_OPERATOR_BIT_NOT,     //  '~'     is only a unary operator
			lex.LEX_OPERATOR_LOGICAL_NOT: //  'NOT'   is a unary operator, or a binary operator modifier
			// pass

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UNARY_OP_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	default: // binary operator is expected
		if lexeme.Lex_subtype == lex.LEX_OPERATOR_LOGICAL_NOT { // NOT can be a modifier to a binary operator like "IN", "LIKE", "BETWEEN" -> "NOT IN", "NOT LIKE", "NOT BETWEEN"
			NOT_modifier_flag = true

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NOT
				return nil, rsql_err
			}

			lexeme = parser.Current_lexeme

			if lexeme.Lex_subtype != lex.LEX_OPERATOR_IN && lexeme.Lex_subtype != lex.LEX_OPERATOR_LIKE && lexeme.Lex_subtype != lex.LEX_OPERATOR_BETWEEN {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BINARY_OP_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}
		}
	}

	/* create operator token */

	token_primary := parser.New_token_primary(TOK_PRIM_OPERATOR, 0, lex.LEXEME_INVALID) // subtype and lexeme fields will be completed herebelow

	/* define operator properties */

	//prim_type           = TOK_PRIM_OPERATOR                 // already defined hereabove
	//prim_subtype                                                // defined below
	//prim_precedence                                             // defined below
	prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT    // provisional default
	prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS // provisional default
	//prim_status         = TOK_PRIM_STATUS_UNCOMPLETE            // provisional default

	comparison_operator_flag = false       // provisional default. Used to check if comparison operator is followed by ANY or ALL.
	prim_any_all_comp_operator_subtype = 0 // provisional default. Used for ANY and ALL.

	switch lexeme.Lex_subtype {
	case lex.LEX_OPERATOR_LOGICAL_AND: /* AND */
		prim_subtype = TOK_PRIM_OPERATOR_LOGICAL_AND
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_AND

	case lex.LEX_OPERATOR_LOGICAL_OR: /* OR */
		prim_subtype = TOK_PRIM_OPERATOR_LOGICAL_OR
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_OR

	case lex.LEX_OPERATOR_LOGICAL_NOT: /* unary NOT  ( left associativity ) */
		prim_subtype = TOK_PRIM_OPERATOR_LOGICAL_UNARY_NOT
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_UNARY_NOT
		prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_LEFT
		prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND

	case lex.LEX_OPERATOR_IS: /* IS (check if it is IS NULL or IS NOT NULL) */
		prim_subtype = TOK_PRIM_OPERATOR_IS_NULL // (provisional) IS NULL
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMP2
		prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL
		lexeme = lex.LEXEME_DISPLAYWORD_IS_NULL // (provisional)

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip IS
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_LOGICAL_NOT {
			prim_subtype = TOK_PRIM_OPERATOR_IS_NOT_NULL // IS NOT NULL
			lexeme = lex.LEXEME_DISPLAYWORD_IS_NOT_NULL
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NOT
				return nil, rsql_err
			}
		}

		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_NULL { // expect NULL
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_IS_NULL, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_OPERATOR_IN: /* IN (expr, expr, ...) or IN (select ...)   see remark above */
		prim_subtype = TOK_PRIM_OPERATOR_IN_LIST // provisional
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMP2
		prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST
		lexeme = lex.LEXEME_DISPLAYWORD_IN_LIST

		if NOT_modifier_flag {
			prim_subtype = TOK_PRIM_OPERATOR_NOT_IN_LIST // provisional
			lexeme = lex.LEXEME_DISPLAYWORD_NOT_IN_LIST
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip IN
			return nil, rsql_err
		}

		// eat list of expressions (at least one expression must exists) or a subquery

		if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN { // expect lparen
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
			return nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype { // it is a IN (subquery) or NOT IN (subquery).
		case lex.LEX_KEYWORD_SELECT:
			prim_subtype = TOK_PRIM_OPERATOR_ANY // 'IN (subquery)' equivalent to '= ANY'
			prim_any_all_comp_operator_subtype = TOK_PRIM_OPERATOR_COMP_EQUAL
			prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL
			lexeme = lex.LEXEME_OPERATOR_ANY

			if NOT_modifier_flag {
				prim_subtype = TOK_PRIM_OPERATOR_ALL // 'NOT IN (subquery)' equivalent to '<> ALL'
				prim_any_all_comp_operator_subtype = TOK_PRIM_OPERATOR_COMP_NOT_EQUAL
				prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL
				lexeme = lex.LEXEME_OPERATOR_ALL
			}

			if token_primary.Prim_right, rsql_err = parser.Eat_subquery_many(); rsql_err != nil { // eat 'select' subquery TOK_PRIM_SUBQUERY_MANY. Return an error if no subquery found.
				return nil, rsql_err
			}

		default: // it is a TOK_PRIM_OPERATOR_IN_LIST or TOK_PRIM_OPERATOR_NOT_IN_LIST
			if prim_listexpr, rsql_err = parser.Eat_list_of_expressions(); rsql_err != nil { // eat sequence of Token_primaries and returns slice of pointers to expressions
				return nil, rsql_err
			}
			token_primary.Prim_listexpr = prim_listexpr
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_OPERATOR_LIKE: /* LIKE */
		prim_subtype = TOK_PRIM_OPERATOR_LIKE
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMP2
		prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE

		if NOT_modifier_flag {
			prim_subtype = TOK_PRIM_OPERATOR_NOT_LIKE
			lexeme = lex.LEXEME_DISPLAYWORD_NOT_LIKE
		}

	case lex.LEX_OPERATOR_ESCAPE: /* pseudo-operator used with LIKE operator */
		prim_subtype = TOK_PRIM_OPERATOR_ESCAPE
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_INVALID_VALUE // no precedence

	case lex.LEX_OPERATOR_BETWEEN: /* BETWEEN */
		prim_subtype = TOK_PRIM_OPERATOR_BETWEEN
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMP2
		prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_BETWEEN

		if NOT_modifier_flag {
			prim_subtype = TOK_PRIM_OPERATOR_NOT_BETWEEN
			lexeme = lex.LEXEME_DISPLAYWORD_NOT_BETWEEN
		}

	case lex.LEX_OPERATOR_PLUS: /*  +  */
		if flag_unary == EAT_OPERATOR_UNARY {
			prim_subtype = TOK_PRIM_OPERATOR_UNARY_PLUS // ( left associativity )
			prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_UNARY_PLUS
			prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_LEFT
			prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND
			break
		}
		prim_subtype = TOK_PRIM_OPERATOR_PLUS
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_ADDITION

	case lex.LEX_OPERATOR_MINUS: /*  -  */
		if flag_unary == EAT_OPERATOR_UNARY {
			prim_subtype = TOK_PRIM_OPERATOR_UNARY_MINUS // ( left associativity )
			prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_UNARY_PLUS
			prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_LEFT
			prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND
			break
		}
		prim_subtype = TOK_PRIM_OPERATOR_MINUS
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_ADDITION

	case lex.LEX_OPERATOR_MULT: /*  *  */
		prim_subtype = TOK_PRIM_OPERATOR_MULT
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_MULTIPLY

	case lex.LEX_OPERATOR_DIV: /*  /  */
		prim_subtype = TOK_PRIM_OPERATOR_DIV
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_MULTIPLY

	case lex.LEX_OPERATOR_MOD: /*  %  */
		prim_subtype = TOK_PRIM_OPERATOR_MOD
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_MULTIPLY

	case lex.LEX_OPERATOR_BIT_AND: /*  &  */
		prim_subtype = TOK_PRIM_OPERATOR_BIT_AND
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_ADDITION

	case lex.LEX_OPERATOR_BIT_OR: /*  |  */
		prim_subtype = TOK_PRIM_OPERATOR_BIT_OR
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_ADDITION

	case lex.LEX_OPERATOR_BIT_XOR: /*  ^  */
		prim_subtype = TOK_PRIM_OPERATOR_BIT_XOR
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_ADDITION

	case lex.LEX_OPERATOR_BIT_NOT: /*  unary ~   ( left associativity )  */
		if flag_unary != EAT_OPERATOR_UNARY {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BINARY_OP_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}
		prim_subtype = TOK_PRIM_OPERATOR_BIT_UNARY_NOT
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_UNARY_PLUS
		prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_LEFT
		prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND

	case lex.LEX_OPERATOR_COMP_EQUAL: /*  =  */
		prim_subtype = TOK_PRIM_OPERATOR_COMP_EQUAL
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON
		comparison_operator_flag = true

	case lex.LEX_OPERATOR_COMP_GREATER: /*  >  */
		prim_subtype = TOK_PRIM_OPERATOR_COMP_GREATER
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON
		comparison_operator_flag = true

	case lex.LEX_OPERATOR_COMP_LESS: /*  <  */
		prim_subtype = TOK_PRIM_OPERATOR_COMP_LESS
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON
		comparison_operator_flag = true

	case lex.LEX_OPERATOR_COMP_GREATER_EQUAL: /*  >=    !< */
		prim_subtype = TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON
		comparison_operator_flag = true

	case lex.LEX_OPERATOR_COMP_LESS_EQUAL: /*  <=    !>*/
		prim_subtype = TOK_PRIM_OPERATOR_COMP_LESS_EQUAL
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON
		comparison_operator_flag = true

	case lex.LEX_OPERATOR_COMP_NOT_EQUAL: /*  <>    != */
		prim_subtype = TOK_PRIM_OPERATOR_COMP_NOT_EQUAL
		prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMPARISON
		comparison_operator_flag = true

	default: // operators not allowed in expression, like LEX_OPERATOR_ASSIGN_PLUS etc, are catched here.
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	/* update operator token */

	token_primary.Prim_subtype = prim_subtype
	token_primary.Prim_name = lexeme.Lex_word

	token_primary.prim_precedence = prim_precedence
	token_primary.prim_associativity = prim_associativity
	token_primary.Prim_cardinality = prim_cardinality
	token_primary.Prim_any_all_comp_operator_subtype = prim_any_all_comp_operator_subtype

	// (operands of IN have already been parsed and grafted into Prim_listexpr)

	/* go to next lexeme */

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip operator
		return nil, rsql_err
	}

	if comparison_operator_flag == false {
		return token_primary, nil // ----- if not a comparison operator, return here.
	}

	/* ====== if comparison operator '=', '>' etc, always check if it is followed by ANY, SOME, ALL ====== */

	if parser.Current_lexeme.Lex_type != lex.LEX_OPERATOR { // it is a plain comparison operator
		return token_primary, nil // ----- if plain comparison operator, return here.
	}

	// here, it may be a ANY or ALL operator.

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_OPERATOR_ALL: // followed by ALL
		any_all_subtype = TOK_PRIM_OPERATOR_ALL
		lexeme = lex.LEXEME_OPERATOR_ALL

	case lex.LEX_OPERATOR_ANY, lex.LEX_OPERATOR_SOME: // followed by ANY or SOME
		any_all_subtype = TOK_PRIM_OPERATOR_ANY
		lexeme = lex.LEXEME_OPERATOR_ANY

	default: // else, it is a plain comparison operator
		return token_primary, nil // ----- if plain comparison operator, return here.
	}

	// here, it is a ANY or ALL operator.

	/* transform comparison token to ANY or ALL token */

	token_primary.Prim_subtype = any_all_subtype
	token_primary.Prim_name = lexeme.Lex_word

	token_primary.Prim_any_all_comp_operator_subtype = prim_subtype // put here the comparison operator subtype.

	token_primary.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_COMP2
	token_primary.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT
	token_primary.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ANY or SOME or ALL
		return nil, rsql_err
	}

	/* eat subquery */

	if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN { // expect lparen
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
		return nil, rsql_err
	}

	if token_primary.Prim_right, rsql_err = parser.Eat_subquery_many(); rsql_err != nil { // eat 'select' subquery TOK_PRIM_SUBQUERY_MANY. Return an error if no subquery found.
		return nil, rsql_err
	}

	if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
		return nil, rsql_err
	}

	// Children for these special operator tokens are as follows :
	//   - TOK_PRIM_OPERATOR_IS_NULL        None
	//   - TOK_PRIM_OPERATOR_NOT_IS_NULL    None
	//   - TOK_PRIM_OPERATOR_BETWEEN        None
	//   - TOK_PRIM_OPERATOR_NOT_BETWEEN    None
	//   - TOK_PRIM_OPERATOR_IN_LIST     ---Prim_listexpr---> slice of pointers to expressions.
	//   - TOK_PRIM_OPERATOR_NOT_IN_LIST ---Prim_listexpr---> slice of pointers to expressions.
	//   - TOK_PRIM_OPERATOR_ALL         ---Prim_right------> TOK_PRIM_SUBQUERY_MANY.
	//   - TOK_PRIM_OPERATOR_ANY         ---Prim_right------> TOK_PRIM_SUBQUERY_MANY.
	//
	// Later, the term situated on their left in the chain will be grafted to these operators as left child.

	return token_primary, nil
}

// Eat_EXISTS creates an EXISTS token.
//
// NOTE: the current lexeme MUST be LEX_KEYWORD_EXISTS.
//
func (parser *Parser) Eat_EXISTS() (*Token_primary, *rsql.Error) {
	var (
		rsql_err      *rsql.Error
		prim_subquery *Token_primary
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_subtype == lex.LEX_KEYWORD_EXISTS)

	/* create token */

	token_primary := parser.New_token_primary(TOK_PRIM_EXISTS, TOK_PRIM_EXISTS_SUB, lexeme)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip EXISTS
		return nil, rsql_err
	}

	/* eat subquery */

	if parser.Current_lexeme.Lex_type != lex.LEX_LPAREN { // expect lparen
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_LPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
		return nil, rsql_err
	}

	if prim_subquery, rsql_err = parser.Eat_subquery_many(); rsql_err != nil { // eat 'select' subquery TOK_PRIM_SUBQUERY_MANY. Return an error if no subquery found.
		return nil, rsql_err
	}

	if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
		return nil, rsql_err
	}

	/* update token */

	token_primary.Prim_left = prim_subquery
	token_primary.Prim_status = TOK_PRIM_STATUS_COMPLETE

	return token_primary, nil
}

// Eat_CASE creates a CASE token.
//
// NOTE: the current lexeme MUST be LEX_KEYWORD_CASE.
//
func (parser *Parser) Eat_CASE() (*Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error

		case_expression   *Token_primary
		when_expression   *Token_primary
		then_expression   *Token_primary
		else_literal_true *Token_primary
		else_expression   *Token_primary

		listexpr []*Token_primary // list of pointers to WHEN and ELSE expressions

		token_equal            *Token_primary // used to transform a CASE switch to CASE normal
		token_when             *Token_primary // same
		token_case_value_clone *Token_primary // same
	)

	lexeme := parser.Current_lexeme
	assert(lexeme.Lex_subtype == lex.LEX_KEYWORD_CASE)

	/* create CASE token */

	token_case := parser.New_token_primary(TOK_PRIM_CASE, TOK_PRIM_CASE_SUB, lexeme)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CASE
		return nil, rsql_err
	}

	/* eat CASE expression if any */

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_WHEN {
		if case_expression, rsql_err = parser.Eat_expression(); rsql_err != nil {
			return nil, rsql_err
		}
	}

	// here, a WHEN clause is expected. If not, return an error.

	/* expect at least one WHEN clause */

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_WHEN {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CASE_WITHOUT_WHEN, rsql.ERROR_BATCH_ABORT)
	}

	/* loop on each  WHEN expr THEN expr */

	for parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WHEN {
		if len(listexpr) >= SPEC_CASE_WHEN_COUNT_MAX*2 {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CASE_WHEN_COUNT_EXCEEDED, rsql.ERROR_BATCH_ABORT)
		}

		//=== WHEN ===

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WHEN
			return nil, rsql_err
		}

		if when_expression, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat WHEN expression
			return nil, rsql_err
		}

		//=== THEN ===

		if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_THEN { // expect THEN
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CASE_WHEN_WITHOUT_THEN, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip THEN
			return nil, rsql_err
		}

		if then_expression, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat THEN expression
			return nil, rsql_err
		}

		//=== append <WHEN, THEN> pair expressions into listexpr

		listexpr = append(listexpr, when_expression, then_expression)
	}

	/* eat ELSE if exists */

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_ELSE:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ELSE
			return nil, rsql_err
		}

		if else_expression, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat ELSE expression
			return nil, rsql_err
		}

	default:
		else_expression = parser.Create_TOK_PRIM_LITERAL_NULL() // if no ELSE clause, create a NULL Token_primary
	}

	// append a literal TRUE condition, followed by ELSE expression into listexpr

	else_literal_true = parser.Create_TOK_PRIM_LITERAL_BOOLEAN(true)

	listexpr = append(listexpr, else_literal_true, else_expression) // condition for ELSE value is always TRUE

	/* eat END */

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_END { // expect END
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CASE_WITHOUT_END, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip END
		return nil, rsql_err
	}

	/* update CASE token */

	token_case.Prim_status = TOK_PRIM_STATUS_COMPLETE
	token_case.Prim_listexpr = listexpr

	if case_expression == nil { // CASE is not a switch, return
		return token_case, nil
	}

	/* if CASE is a switch (that is, if case_expression exists), transform to normal CASE by inserting '=' Token_primaries */

	// === for first 'when' clause ===

	token_when = token_case.Prim_listexpr[0]

	// create a '=' operator token

	token_equal = parser.Create_TOK_PRIM_OPERATOR_COMP(TOK_PRIM_OPERATOR_COMP_EQUAL, case_expression, token_when) // create '=' operator token
	token_equal.Tok_batch_line = token_when.Tok_batch_line                                                        // line/pos

	// insert '=' operator token as CASE 'when' operand, for first 'when' clauses.

	token_case.Prim_listexpr[0] = token_equal

	// === for all subsequent 'when' clauses ===

	for i := 2; i < len(listexpr)-3; i += 2 {
		token_when = token_case.Prim_listexpr[i]

		// create a clone token of the 'case' value

		if token_case_value_clone, rsql_err = Clone(case_expression); rsql_err != nil {
			return nil, rsql_err
		}

		// create a '=' operator token

		token_equal = parser.Create_TOK_PRIM_OPERATOR_COMP(TOK_PRIM_OPERATOR_COMP_EQUAL, token_case_value_clone, token_when) // create '=' operator token
		token_equal.Tok_batch_line = token_when.Tok_batch_line                                                               // line/pos

		// insert '=' operator token as CASE 'when' operand

		token_case.Prim_listexpr[i] = token_equal
	}

	return token_case, nil
}

// Eat_term creates a 'term' token by eating one or more lexemes.
//
// An expression is made up of operators and terms.
// Unlike operators, a term has no precedence, associativity, cardinality, and its status is always TOK_PRIM_STATUS_COMPLETE.
//
// A term can be :
//      - TOK_PRIM_PLACEHOLDER/_QUESTIONMARK
//
//      - TOK_PRIM_LITERAL_NULL
//      - TOK_PRIM_LITERAL_BOOLEAN
//      - TOK_PRIM_LITERAL_HEXASTRING
//      - TOK_PRIM_LITERAL_STRING
//      - TOK_PRIM_LITERAL_NUMBER
//
//      - TOK_PRIM_VARIABLE
//      - TOK_PRIM_IDENTIFIER
//
//      - TOK_PRIM_SYSFUNC
//      - TOK_PRIM_CAST
//      - TOK_PRIM_UFUNC
//
//      - TOK_PRIM_CASE
//      - TOK_PRIM_EXISTS
//
//      - TOK_PRIM_SUBQUERY/_ONE.  It is a 'select' instruction enclosed in ()
//
//      - the root token of an expression enclosed in ().
//
// The following tokens are not terms and are never returned by this function :
//      - TOK_PRIM_OPERATOR is a temporary internal node representing an operator result.
//
// There is the same problem as mentionned for parser.Eat_operator().
//      In the example
//          set @a = ( (select a1 from t1 union all select a2 from t2) union all select a3 from t3 )
//          an error occurs at the second 'union'.
//          Because our parser will consider the content of the second parenthese to be a select subquery (which is the case), and expect that a closing parenthese or an operator follows. Not a 'union'.
//          Our parser is top-down recursive, and must predict if the expression to be eaten is an 'arithmetic' expression or a 'select' subquery. The parser chooses to eat a subquery if the first lexeme is 'select'. Else, it eats an arithmetic expression.
//          MS SQL Server can parse this expression because it is a bottom-up parser.
//          But such particular cases are very very rare, so we don't consider it is a problem.
//          For us, a suquery must begin with the 'SELECT' keyword. Period.
//
func (parser *Parser) Eat_term() (*Token_primary, *rsql.Error) {
	var (
		rsql_err               *rsql.Error
		name_3, name_2, name_1 string
		nb_prefix_parts        int
		token_primary          *Token_primary
		sysfunc_descr          *Sysfunc_description
		ok                     bool
		collation              string
	)

	switch parser.Current_lexeme.Lex_type {
	case lex.LEX_IDENTPART, // === eat identifier or function ===
		lex.LEX_DOT:

		// TOK_PRIM_IDENTIFIER, TOK_PRIM_SYSFUNC, TOK_PRIM_CAST, TOK_PRIM_UFUNC

		// read prefix if any

		if name_3, name_2, name_1, nb_prefix_parts, rsql_err = parser.Eat_prefix(); rsql_err != nil { // entire prefix is eaten.
			return nil, rsql_err
		}

		// here, we expect the current lexeme to be a single identifier or the last idenpart of a qualified identifier. Else, it is a syntax error, in particular if it is a star.

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_IDENTPART_SUB: // == A) if last part of identifier is idenpart ==
			switch {
			case nb_prefix_parts == 0: // == if single name ==
				if sysfunc_descr, ok = G_SYSFUNC_DESCRIPTION_MAP[parser.Current_lexeme.Lex_word]; ok { //  if found in sysfunc table, it is a system function
					token_primary, rsql_err = parser.Eat_sysfunc(sysfunc_descr) // eat system function and arguments (doesn't eat args for SESSION_USER, etc)
				} else {
					token_primary, rsql_err = parser.Eat_qualified_identifier(name_3, name_2, name_1) // else, it is a column name

					if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN {
						return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_NOT_BUILTIN_FUNCTION, rsql.ERROR_BATCH_ABORT, token_primary.Prim_name)
					}
				}

			case parser.Rune0 == '(': // == if prefix exists and current lexeme is followed by lparen, it is a user function ==
				if nb_prefix_parts+1 > TOKEN_UFUNC_NUMBER_OF_PARTS { //   db.schema  .function
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_UFUNC_TOO_MANY_PARTS, rsql.ERROR_BATCH_ABORT)
				}
				token_primary, rsql_err = parser.Eat_ufunc(name_2, name_1) // eat user function and arguments

			default: // == else, it must be an identifier with prefix ==
				if nb_prefix_parts+1 > TOKEN_COLUMN_NUMBER_OF_PARTS { //   db.schema.table  .column
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_TOO_MANY_PARTS, rsql.ERROR_BATCH_ABORT)
				}

				if name_1 == "" { // table name is mandatory in qualified name
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_PREFIX_WITHOUT_TABLE_NAME, rsql.ERROR_BATCH_ABORT) // like MS SQL Server
				}

				token_primary, rsql_err = parser.Eat_qualified_identifier(name_3, name_2, name_1) // db.schema.table.column
			}

		case lex.LEX_OPERATOR_MULT: // == B) it is a qualified star ==
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_QUALIFIED_STAR, rsql.ERROR_BATCH_ABORT)

		default: // == C) syntax error ==
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MALFORMED_EXPRESSION, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_LITERAL_STRING: // eat literal string
		token_primary, rsql_err = parser.Eat_literal_string() // TOK_PRIM_LITERAL_STRING

	case lex.LEX_LITERAL_HEXASTRING: // eat literal hexastring
		token_primary, rsql_err = parser.Eat_literal_hexastring() // TOK_PRIM_LITERAL_HEXASTRING

	case lex.LEX_LITERAL_NUMBER: // eat literal number
		token_primary, rsql_err = parser.Eat_literal_number() // TOK_PRIM_LITERAL_NUMBER

	case lex.LEX_QUESTIONMARK: // eat placeholder QUESTIONMARK (placeholder for prepared statement)
		if parser.questionmark_allowed == false {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ILLEGAL_QUESTIONMARK, rsql.ERROR_BATCH_ABORT)
		}
		token_primary, rsql_err = parser.Eat_placeholder_QUESTIONMARK() // TOK_PRIM_PLACEHOLDER/_QUESTIONMARK

	case lex.LEX_VARIABLE: // eat variable
		token_primary, rsql_err = parser.Eat_variable() // TOK_PRIM_VARIABLE

	case lex.LEX_ATFUNC: // eat @@ builtin function
		token_primary, rsql_err = parser.Eat_atfunc() // TOK_PRIM_SYSFUNC, or TOK_PRIM_VARIABLE

	case lex.LEX_LPAREN: // eat parenthesed subexpression
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
			return nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_SELECT: // TOK_PRIM_SUBQUERY/_ONE
			token_primary, rsql_err = parser.Eat_subquery_one() // eat 'select' subquery
		default:
			token_primary, rsql_err = parser.Eat_expression() // eat expression
		}
		if rsql_err != nil {
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_RPAREN { // expect rparen
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MISSING_RPAREN, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip rparen
			return nil, rsql_err
		}

	case lex.LEX_KEYWORD:
		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_NULL: // eat NULL
			token_primary, rsql_err = parser.Eat_literal_NULL() // TOK_PRIM_LITERAL_NULL

		case lex.LEX_KEYWORD_LEFT: // eat builtin LEFT function
			token_primary, rsql_err = parser.Eat_sysfunc(SYSFUNC_DESCR_LEFT) // TOK_PRIM_SYSFUNC

		case lex.LEX_KEYWORD_RIGHT: // eat builtin RIGHT function
			token_primary, rsql_err = parser.Eat_sysfunc(SYSFUNC_DESCR_RIGHT) // TOK_PRIM_SYSFUNC

		case lex.LEX_KEYWORD_SYSTEM_USER: // eat system variable _@system_user_name
			token_primary, rsql_err = parser.Eat_sysvar(lex.LEXEME_SYSVAR_SYSTEM_USER_NAME) // TOK_PRIM_VARIABLE
			if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LPAREN_NOT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		case lex.LEX_KEYWORD_CURRENT_USER, lex.LEX_KEYWORD_SESSION_USER, lex.LEX_KEYWORD_USER: // eat system variable _@current_user_name
			token_primary, rsql_err = parser.Eat_sysvar(lex.LEXEME_SYSVAR_CURRENT_USER_NAME) // TOK_PRIM_VARIABLE
			if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LPAREN_NOT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		case lex.LEX_KEYWORD_CURRENT_TIMESTAMP: // eat builtin GETDATE function
			token_primary, rsql_err = parser.Eat_sysfunc(SYSFUNC_DESCR_CURRENT_TIMESTAMP) // TOK_PRIM_SYSFUNC
			if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LPAREN_NOT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		case lex.LEX_KEYWORD_CONVERT: // eat builtin CONVERT function
			token_primary, rsql_err = parser.Eat_CONVERT() // TOK_PRIM_CAST

		case lex.LEX_KEYWORD_CAST: // eat builtin CAST function
			token_primary, rsql_err = parser.Eat_CAST() // TOK_PRIM_CAST

		case lex.LEX_KEYWORD_CASE: // eat CASE
			token_primary, rsql_err = parser.Eat_CASE() // TOK_PRIM_CASE

		case lex.LEX_KEYWORD_EXISTS: // eat EXISTS
			token_primary, rsql_err = parser.Eat_EXISTS() // TOK_PRIM_EXISTS

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MALFORMED_EXPRESSION, rsql.ERROR_BATCH_ABORT)
		}

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_MALFORMED_EXPRESSION, rsql.ERROR_BATCH_ABORT) // valid term not found
	}

	if rsql_err != nil {
		return nil, rsql_err
	}

	/* eat COLLATE, if any */

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_COLLATE {
		if collation, rsql_err = parser.Eat_COLLATE(); rsql_err != nil {
			return nil, rsql_err
		}

		token_primary.Prim_sql_collate = collation
	}

	return token_primary, nil
}

// Eat_expression eats a full expression, made up of a sequence of 'terms' and 'operators'.
//
//    [unary op][unary op]...[term] [ [non-unary op][unary op][unary op]...[term] ] ...
//
//    The processing is as follows :
//
//      - All the tokens are read and chained one after another in a main linear list which anchor is 'token_list_dummy'.
//      - Each operator token is also put in an operator list with the proper precedence, and ordered in this list following its associativity direction ( ---> or <--- ).
//      - If a literal integer, float or numeric literal number is preceded by unary minus operator, the operator is removed from main list and from operator precedence list, because the sign belongs to the literal number it prefixes. Then, the token's lexeme is replaced by a lexeme with negative literal number. Unary plus are just discarded.
//      - Then, each operator token of each list (from strongest precedence order to weakest) is processed to graft its left and right operands, and remove them from the main list. This way, the token chain is transformed into an AST.
//      - At the end of this processing, only one token must remain, linked to the list anchor 'token_list_dummy'.
//
//    A token that is grafted to an operator and removed from the chain must be TOK_PRIM_STATUS_COMPLETE, meaning that it has all its operands grafted.
//    If TOK_PRIM_STATUS_UNCOMPLETE, it means that the token has missing operands.
//    In this case, it cannot be grafted to another operator and removed from the chain. It is a mismatch syntax error.
//    Example: "22 + 33 is null + 44".       "22" and "33" are grafted to "+", but "is null" cannot be grafted as left operand of "+" because "is null" is not TOK_PRIM_STATUS_COMPLETE yet.
//
// NOTE: The expression must contain at least one valid term. Else, it returns a syntax error.
//
// The returned token status is TOK_PRIM_STATUS_COMPLETE and is the root of the expression AST tree.
//
func (parser *Parser) Eat_expression() (*Token_primary, *rsql.Error) {
	var (
		rsql_err      *rsql.Error
		info_line_bak lex.Coord_t

		token_list_dummy Token_primary // anchor of double-linked list of tokens making up the expression. Only 'Tok_prev', 'Tok_next', 'Prim_type', 'Prim_subtype' and 'Prim_status' members of this variable are used.

		last_token       *Token_primary // last token in the chain.
		token            *Token_primary // current token.
		token_to_unchain *Token_primary // used when a token must be unchained.
		token_op         *Token_primary
		token_result     *Token_primary

		precedence_lists oplist_t // array of lists of operator tokens for each precedence.
		i                Prim_precedence_t
		preclist         []*Token_primary

		// token for LIKE processing
		token_likexpr *Token_primary
		token_escape  *Token_primary

		// tokens for BETWEEN processing
		token_between_value       *Token_primary
		token_between_limit_low   *Token_primary
		token_between_limit_high  *Token_primary
		token_between_value_clone *Token_primary
		token_between_comp_ge     *Token_primary
		token_between_comp_le     *Token_primary
	)

	/* initialization */

	parser.Info_line_update()

	parser.expression_recursivity_depth++ // increment recursivity counter

	if parser.expression_recursivity_depth > SPEC_EXPRESSION_DEPTH_MAX {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_EXPRESSION_DEPTH_EXCEEDED, rsql.ERROR_BATCH_ABORT)
	}

	token_list_dummy.Tok_next = nil
	token_list_dummy.Tok_prev = nil
	token_list_dummy.Prim_type = TOK_PRIMARY_ANCHOR           // not used.
	token_list_dummy.Prim_subtype = TOK_PRIMARY_ANCHOR_SUB    // because in "eat [ TERM ]" step below, we check for previous_token.Prim_subtype == TOK_PRIM_OPERATOR_UNARY_PLUS/MINUS
	token_list_dummy.Prim_status = TOK_PRIM_STATUS_UNCOMPLETE // because it can never be grafted as operand.

	/* ====== initialization for first sequence   [ UNARY OP ] [ UNARY OP ] ... [ TERM ]   ====== */

	last_token = &token_list_dummy

	/* ====== loop on successive sequences of    [ UNARY OP ] [ UNARY OP ] ... [ TERM ] (exit if end of expression) [ NON-UNARY OP ]  ====== */

	// This step just eats operator and term tokens in a definite sequence, and chain them together.
	// The code must also tell if '+' or '-' operators are unary or binary operators, so that they are put in the proper precedence list.

	for {
		/*** ======= eat sequence of [ UNARY OP ] if any ======= ***/

		for parser.Current_lexeme.Lex_type == lex.LEX_OPERATOR { // eat unary operators
			if token, rsql_err = parser.Eat_operator(EAT_OPERATOR_UNARY); rsql_err != nil { // unary op. An error is returned if operator is not unary.
				return nil, rsql_err
			}

			if token.Prim_subtype == TOK_PRIM_OPERATOR_UNARY_PLUS { // discard 'unary plus' operator and continue.
				continue
			}

			precedence_lists.put_operator(token) // put the token operator in the proper precedence list.

			token_chain(last_token, token)
			last_token = token
		}

		/*** ======= eat exactly one [ TERM ] ======= ***/

		if token, rsql_err = parser.Eat_term(); rsql_err != nil { // eat term, which status is TOK_PRIM_STATUS_COMPLETE. It may be a literal, a variable, an identifier, a function, etc, or a complete expression enclosed in ( )
			return nil, rsql_err // an error is returned by parser.Eat_term() if no valid term found.
		}

		// if current token is literal float, numeric or integer number, and previous_token was unary minus, fusion them together and remove previous_token.

		if (token.Prim_type == TOK_PRIM_LITERAL_NUMBER) &&
			(token.Prim_subtype == TOK_PRIM_LITERAL_NUMBER_INTEGRAL ||
				token.Prim_subtype == TOK_PRIM_LITERAL_NUMBER_NUMERIC ||
				token.Prim_subtype == TOK_PRIM_LITERAL_NUMBER_FLOAT) {

			assert(last_token.Prim_subtype != TOK_PRIM_OPERATOR_UNARY_PLUS)

			if last_token.Prim_subtype == TOK_PRIM_OPERATOR_UNARY_MINUS {
				token_to_unchain = last_token                     // prepare to remove the unary minus operator from the chain
				last_token = last_token.Tok_prev.(*Token_primary) // adjust last_token pointer
				token_unchain(token_to_unchain)                   // remove unary minus operator from the chain
				precedence_lists.remove(token_to_unchain)         // and from the precedence list

				parser.transform_token_primary_to_negative_literal_number(token) // transform token primary to negative lexeme
			}
		}

		// chain term token

		token_chain(last_token, token)
		last_token = token

		/*** ======= eat one [ NON-UNARY OP ]   "+" "-" "*" "/" "IS NULL" "IN (...)" "BETWEEN" etc ======= ***/

	LABEL_NON_UNARY_OP:

		if parser.Current_lexeme.Lex_type != lex.LEX_OPERATOR {
			break // ----------------- exit loop. We have read all the tokens of the expression ----------------
		}

		// eat NON-UNARY operator

		if token, rsql_err = parser.Eat_operator(EAT_OPERATOR_NON_UNARY); rsql_err != nil { // NON-UNARY operator
			return nil, rsql_err
		}
		if token.Prim_subtype != TOK_PRIM_OPERATOR_ESCAPE {
			precedence_lists.put_operator(token)
		}
		token_chain(last_token, token)
		last_token = token

		switch token.Prim_subtype { // special cases for operators that don't have a second operand
		case TOK_PRIM_OPERATOR_IS_NULL, // no operand follows.
			TOK_PRIM_OPERATOR_IS_NOT_NULL, //  same
			TOK_PRIM_OPERATOR_IN_LIST,     // operands have already been eaten by parser.Eat_operator().
			TOK_PRIM_OPERATOR_NOT_IN_LIST, //  same
			TOK_PRIM_OPERATOR_ANY,         //  same
			TOK_PRIM_OPERATOR_ALL:         //  same
			goto LABEL_NON_UNARY_OP // continue loop, but skip the eating of [UNARY OP] ... [TERM] and go directly to eating of [ NON-UNARY OP ]
		}

	} // end loop

	// chain the last token to the token_list_dummy. The list becomes a circular double linked list.
	// This way, a token in the chain points always to another token (the 'Tok_next' pointer is never nil).
	// This is needed for BETWEEN processing, which check the type and unchain many tokens that follows.
	// This renders the next processing step below easier.

	token_chain(last_token, &token_list_dummy)

	/* In the circular chain, we have the following Token_primaries :

	   - token_list_dummy                           : TOK_PRIM_STATUS_UNCOMPLETE

	   - literal string, hexastring, number, NULL   : TOK_PRIM_STATUS_COMPLETE
	   - variable, identifier                       : TOK_PRIM_STATUS_COMPLETE
	   - questionmark placeholder                   : TOK_PRIM_STATUS_COMPLETE
	   - function                                   : TOK_PRIM_STATUS_COMPLETE
	   - subexpression                              : TOK_PRIM_STATUS_COMPLETE
	   - subquery                                   : TOK_PRIM_STATUS_COMPLETE
	   - EXISTS                                     : TOK_PRIM_STATUS_COMPLETE
	   - CASE                                       : TOK_PRIM_STATUS_COMPLETE

	   - operator                                   : TOK_PRIM_STATUS_UNCOMPLETE
	*/

	/* ====== tree reorganization by operator precedences ====== */

	// So far, we have a chain of operators and terms.

	// for each unary operator, graft the next primary as left operand, and remove it from token list.

	// for each binary operator, graft the previous and next primaries as left and right operands, and remove them from token list.

	// for each special operator like [NOT] IN, ANY, ALL, IS [NOT] NULL, graft the previous primary as left operand and remove it from token list.

	// for each special operator BETWEEN, graft the proper primaries as operands and remove them from token list. Transform BETWEEN into AND or OR operator.
	//     note: the 3 operands of 'between' are not cast to a common datatype, but equalized by pairs.
	//     In the example below, @a and @b are equalized to 'varchar', and @a and @c are equalized to 'int'.
	//
	//       declare @a varchar(10) = '2'
	//       declare @b varchar(10) = '100'
	//       declare @c int = 300
	//
	//       if @a between @b and @c
	//           print '''varchar comparison'' of @a and @b, and ''int comparison'' of @a and @c'

	info_line_bak = parser.Info_line // save current Info_line

	for i = 0; i < TOK_PRIM_OPERATOR_PRECEDENCE_LEVELS; i++ { // for each precedence level, from strongest (unary) to weakest
		preclist = precedence_lists.lists[i]

		for _, token_op = range preclist { // for each operator in the precedence list
			parser.Info_line = token_op.Tok_batch_line // change line info for error display

			switch token_op.Prim_cardinality { // reorganize operands
			case TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS: /* OPERATOR WITH TWO OPERANDS */
				token_to_unchain = token_op.Tok_prev.(*Token_primary) // reorganize previous operand and graft as left operand
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_op.Prim_left = token_to_unchain
				token_unchain(token_to_unchain)

				token_to_unchain = token_op.Tok_next.(*Token_primary) // reorganize next operand and graft as right operand
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_op.Prim_right = token_to_unchain
				token_unchain(token_to_unchain)

			case TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND: /* OPERATOR WITH ONE OPERAND */
				token_to_unchain = token_op.Tok_next.(*Token_primary) // reorganize next operand and graft as left operand
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_op.Prim_left = token_to_unchain
				token_unchain(token_to_unchain)

			case TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL, /* IS NULL, IS NOT NULL OPERATOR */
				TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST, /* IN, NOT IN           OPERATOR */
				TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL: /* ANY, ALL             OPERATOR */
				token_to_unchain = token_op.Tok_prev.(*Token_primary) // reorganize previous operand and graft as left operand
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_op.Prim_left = token_to_unchain
				token_unchain(token_to_unchain)

			case TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE: /* LIKE, NOT LIKE OPERATOR */
				token_to_unchain = token_op.Tok_prev.(*Token_primary) // reorganize previous operand and graft as left operand
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_op.Prim_left = token_to_unchain
				token_unchain(token_to_unchain)

				token_to_unchain = token_op.Tok_next.(*Token_primary) // reorganize next operand and graft as right operand
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_likexpr = token_to_unchain
				token_unchain(token_to_unchain)

				escape_clause_exists := false
				token_to_unchain = token_op.Tok_next.(*Token_primary) // check if 'ESCAPE' pseudo-operator exists
				switch token_to_unchain.Prim_subtype {
				case TOK_PRIM_OPERATOR_ESCAPE:
					escape_clause_exists = true
					token_unchain(token_to_unchain) // unchain 'ESCAPE'

					token_to_unchain = token_op.Tok_next.(*Token_primary) // unchain next operand (escape string)
					if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
						return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
					}
					token_escape = token_to_unchain
					token_unchain(token_to_unchain) // unchain escape string

				default:
					token_escape = parser.Create_TOK_PRIM_LITERAL_NULL()  // no ESCAPE clause, create a NULL as escape argument
					token_escape.Tok_batch_line = token_op.Tok_batch_line // LIKE line/pos
				}

				token_op.Prim_right = parser.Create_TOK_PRIM_SYSFUNC_COMPILE_VARCHAR_TO_REGEXPLIKE(token_likexpr, token_escape) // create SYSFUNC token to compile VARCHAR to REGEXPLIKE
				token_op.Prim_right.Tok_batch_line = token_op.Tok_batch_line                                                    // LIKE line/pos

				// if expression is literal string of the form 'prefix%',    a LIKE 'prefix%'    is replaced by    a >= 'prefix' AND a <= 'prefix\U0010FFFD'

				if token_op.Prim_subtype == TOK_PRIM_OPERATOR_LIKE && // only for LIKE. NOT LIKE is not transformed.
					escape_clause_exists == false &&
					token_likexpr.Prim_type == TOK_PRIM_LITERAL_STRING &&
					token_likexpr.Prim_sql_collate == "" { // no transformation in particular for LIKE 'asdf' COLLATE xxx_cs_as

					prefix, ok := LIKE_is_range(token_likexpr.Prim_name)

					if ok == true {
						tok_operand_1 := token_op.Prim_left
						tok_limit_low := parser.Create_TOK_PRIM_LITERAL_STRING(prefix)
						tok_limit_low.Prim_sql_collate = token_likexpr.Prim_sql_collate

						var tok_operand_1_clone *Token_primary
						tok_limit_high := parser.Create_TOK_PRIM_LITERAL_STRING(prefix + "\U0010FFFD") // 10FFFD is the highest unicode character
						tok_limit_high.Prim_sql_collate = token_likexpr.Prim_sql_collate

						tok_comp_ge := parser.Create_TOK_PRIM_OPERATOR_COMP(TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL, tok_operand_1, tok_limit_low)

						if tok_operand_1_clone, rsql_err = Clone(tok_operand_1); rsql_err != nil {
							return nil, rsql_err
						}
						tok_comp_le := parser.Create_TOK_PRIM_OPERATOR_COMP(TOK_PRIM_OPERATOR_COMP_LESS_EQUAL, tok_operand_1_clone, tok_limit_high)

						// transform LIKE token_op, which is still chained, into AND token

						//token_op.Prim_type                       = TOK_PRIM_OPERATOR
						token_op.Prim_subtype = TOK_PRIM_OPERATOR_LOGICAL_AND
						token_op.Prim_name = lex.LEXEME_OPERATOR_LOGICAL_AND.Lex_word

						token_op.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_AND
						token_op.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT
						token_op.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS

						token_op.Prim_left = tok_comp_ge
						token_op.Prim_right = tok_comp_le
					}
				}

			case TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_BETWEEN: /* BETWEEN, NOT BETWEEN OPERATOR */
				// the three operands of BETWEEN have already been processed, as operators like + - * / etc have higher precedence.

				token_to_unchain = token_op.Tok_prev.(*Token_primary) // unchain previous operand
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_between_value = token_to_unchain // value to be compared is unchained
				token_unchain(token_to_unchain)

				token_to_unchain = token_op.Tok_next.(*Token_primary) // unchain next operand (low limit)
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_between_limit_low = token_to_unchain // low limit value is unchained
				token_unchain(token_to_unchain)

				token_to_unchain = token_op.Tok_next.(*Token_primary) // unchain 'AND' operator
				if token_to_unchain.Prim_subtype != TOK_PRIM_OPERATOR_LOGICAL_AND {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BETWEEN_MISSING_AND, rsql.ERROR_BATCH_ABORT)
				}
				assert(token_to_unchain.Prim_status == TOK_PRIM_STATUS_UNCOMPLETE) // the AND operator is always TOK_PRIM_STATUS_UNCOMPLETE, as they have not been processed yet (precedence lower than BETWEEN)
				token_unchain(token_to_unchain)
				precedence_lists.remove(token_to_unchain) // and also removed from the oplist_t

				token_to_unchain = token_op.Tok_next.(*Token_primary) // unchain next operand (high limit)
				if token_to_unchain.Prim_status != TOK_PRIM_STATUS_COMPLETE {
					return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH, rsql.ERROR_BATCH_ABORT)
				}
				token_between_limit_high = token_to_unchain // high limit value is unchained
				token_unchain(token_to_unchain)

				// create token_between_comp_ge, token_between_value_clone, token_between_comp_le

				token_between_comp_ge = parser.Create_TOK_PRIM_OPERATOR_COMP(TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL, token_between_value, token_between_limit_low)

				if token_between_value_clone, rsql_err = Clone(token_between_value); rsql_err != nil {
					return nil, rsql_err
				}
				token_between_comp_le = parser.Create_TOK_PRIM_OPERATOR_COMP(TOK_PRIM_OPERATOR_COMP_LESS_EQUAL, token_between_value_clone, token_between_limit_high)

				// transform BETWEEN token_op, which is still chained, into AND token. (NOT BETWEEN is transformed into OR token)

				switch token_op.Prim_subtype {
				case TOK_PRIM_OPERATOR_BETWEEN: // x BETWEEN a AND b  becomes x>=a AND x<=b
					//token_op.Prim_type                       = TOK_PRIM_OPERATOR
					token_op.Prim_subtype = TOK_PRIM_OPERATOR_LOGICAL_AND
					token_op.Prim_name = lex.LEXEME_OPERATOR_LOGICAL_AND.Lex_word

					token_op.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_AND
					token_op.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT
					token_op.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS

				default: // x NOT BETWEEN a AND b  becomes  x<a OR x>b
					assert(token_op.Prim_subtype == TOK_PRIM_OPERATOR_NOT_BETWEEN)

					//token_op.Prim_type                       = TOK_PRIM_OPERATOR
					token_op.Prim_subtype = TOK_PRIM_OPERATOR_LOGICAL_OR
					token_op.Prim_name = lex.LEXEME_OPERATOR_LOGICAL_OR.Lex_word

					token_op.prim_precedence = TOK_PRIM_OPERATOR_PRECEDENCE_LOGICAL_OR
					token_op.prim_associativity = TOK_PRIM_OPERATOR_ASSOCIATIVITY_RIGHT
					token_op.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS

					token_between_comp_ge.Prim_subtype = TOK_PRIM_OPERATOR_COMP_LESS // transform '>=' into '<'
					token_between_comp_ge.Prim_name = lex.LEXEME_OPERATOR_COMP_LESS.Lex_word

					token_between_comp_le.Prim_subtype = TOK_PRIM_OPERATOR_COMP_GREATER // transform '<=' into '>'
					token_between_comp_le.Prim_name = lex.LEXEME_OPERATOR_COMP_GREATER.Lex_word
				}

				token_op.Prim_left = token_between_comp_ge
				token_op.Prim_right = token_between_comp_le

			default:
				panic("other cardinality")
			}

			token_op.Prim_status = TOK_PRIM_STATUS_COMPLETE // set current operator to TOK_PRIM_STATUS_COMPLETE, so that it becomes graftable.

		} // end of loop (for each operator in a precedence list)

	} // end of loop (for each precedence list)

	/* ====== return root of expression tree as result ====== */

	parser.Info_line = info_line_bak // restore line info for error display

	token_result = token_list_dummy.Tok_next.(*Token_primary) // token to be returned.

	// only one token must remain. All other tokens must have been unchained.
	// Else, an error is returned. E.g. for     'hello' escape 5

	if token_result.Tok_next.(*Token_primary) != &token_list_dummy {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPERATOR_MISMATCH_RESULT_NOT_UNIQUE, rsql.ERROR_BATCH_ABORT)
	}

	assert(token_result.Prim_status == TOK_PRIM_STATUS_COMPLETE) // expression root

	token_unchain(token_result) // unchain from the dummy list

	parser.expression_recursivity_depth-- // decrement recursivity counter

	return token_result, nil
}

// Eat_list_of_expressions eats a sequence of function arguments.
//
// If at least one argument expression (that is, at least one valid term) is not available, a syntax error is returned.
//
func (parser *Parser) Eat_list_of_expressions() ([]*Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		param    *Token_primary
	)

	arg_list := make([]*Token_primary, 0, 8) // 8 is an arbitrary default capacity, which is good enough for most number of expression in list.

	/* === loop on each parameter === */

	for {
		if len(arg_list) >= SPEC_EXPRESSIONS_IN_LIST_COUNT_MAX {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_EXPRESSIONS_IN_LIST, rsql.ERROR_BATCH_ABORT)
		}

		if param, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat parameter expression
			return nil, rsql_err
		}

		arg_list = append(arg_list, param) // append param to list

		// eat comma

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more parameter, exit loop.
			break
		}

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}
	}

	// here, at least one valid expression has been read.

	assert(len(arg_list) > 0)

	return arg_list, nil
}

// Eat_list_of_function_arguments eats a sequence of function arguments.
//
// If at least one argument expression (that is, at least one valid term) is not available, a syntax error is returned.
//
func (parser *Parser) Eat_list_of_function_arguments() ([]*Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		param    *Token_primary
	)

	arg_list := make([]*Token_primary, 0, 6) // 6 is an arbitrary default capacity, which is good enough for most number of arguments.

	/* === loop on each parameter === */

	for {
		if len(arg_list) >= SPEC_FUNCTION_ARGUMENTS_COUNT_MAX {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FUNCTION_ARGUMENTS_COUNT_EXCEEDED, rsql.ERROR_BATCH_ABORT)
		}

		if param, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat parameter expression
			return nil, rsql_err
		}

		arg_list = append(arg_list, param) // append param to list

		// eat comma

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // no more parameter, exit loop.
			break
		}

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}
	}

	// here, at least one valid expression has been read.

	assert(len(arg_list) > 0)

	return arg_list, nil
}

func (parser *Parser) Eat_subquery_many() (*Token_primary, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SUBQUERIES_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT) // TODO
}
func (parser *Parser) Eat_subquery_one() (*Token_primary, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SUBQUERIES_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT) // TODO
}
