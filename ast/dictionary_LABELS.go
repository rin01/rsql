package ast

import (
	"rsql"
)

//======================================================
//                  Dictionary_LABEL                  //
//======================================================

type Dictionary_LABEL struct {
	tbl map[string]*Token_stmt_LABEL
}

func (dictlab *Dictionary_LABEL) Initialize() {

	dictlab.tbl = make(map[string]*Token_stmt_LABEL, 10) // 10 is defaut initial capacity, which is largely good enough
}

func (dictlab *Dictionary_LABEL) Put(label string, tok_label *Token_stmt_LABEL) *rsql.Error {
	var (
		ok bool
	)

	if _, ok = dictlab.tbl[label]; ok == true { // if label already exists
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LABEL_DUPLICATE, rsql.ERROR_BATCH_ABORT, label)
	}

	dictlab.tbl[label] = tok_label

	return nil
}

func (dictlab *Dictionary_LABEL) Check_exists(label string) *rsql.Error {
	var (
		ok bool
	)

	if _, ok = dictlab.tbl[label]; ok == false { // if label does not exist
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LABEL_NOT_FOUND, rsql.ERROR_BATCH_ABORT, label)
	}

	return nil
}

func (dictlab *Dictionary_LABEL) Get(label string) (*Token_stmt_LABEL, *rsql.Error) {
	var (
		tok_label *Token_stmt_LABEL
		ok        bool
	)

	if tok_label, ok = dictlab.tbl[label]; ok == false { // if label does not exist
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_LABEL_NOT_FOUND, rsql.ERROR_BATCH_ABORT, label)
	}

	return tok_label, nil
}
