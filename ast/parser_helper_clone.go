package ast

import (
	"rsql"
)

// Clone clones tok, as well as all its arguments recursively.
//
func Clone(tok *Token_primary) (*Token_primary, *rsql.Error) {
	var (
		rsql_err *rsql.Error
	)

	clone := &Token_primary{}

	*clone = *tok

	// recursively clone all arguments

	if tok.Prim_left != nil {
		if clone.Prim_left, rsql_err = Clone(tok.Prim_left); rsql_err != nil {
			return nil, rsql_err
		}
	}

	if tok.Prim_right != nil {
		if clone.Prim_right, rsql_err = Clone(tok.Prim_right); rsql_err != nil {
			return nil, rsql_err
		}
	}

	if len(tok.Prim_listexpr) != 0 {
		clone.Prim_listexpr = make([]*Token_primary, len(tok.Prim_listexpr))

		for i, arg := range tok.Prim_listexpr {
			if clone.Prim_listexpr[i], rsql_err = Clone(arg); rsql_err != nil {
				return nil, rsql_err
			}
		}
	}

	if tok.Prim_syscollator != nil {
		if clone.Prim_syscollator, rsql_err = Clone(tok.Prim_syscollator); rsql_err != nil {
			return nil, rsql_err
		}
	}

	if tok.Prim_syslanguage != nil {
		if clone.Prim_syslanguage, rsql_err = Clone(tok.Prim_syslanguage); rsql_err != nil {
			return nil, rsql_err
		}
	}

	if tok.Prim_xtable_subquery != nil {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SUBQUERY_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT)
	}

	assert(tok.Prim_dataslot == nil && tok.Prim_xtable_ds == nil) // Clone is only used during parse stage, not during or after the decorating stage

	return clone, nil
}
