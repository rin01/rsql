package ast

const (
	SPEC_EXPRESSION_DEPTH_MAX = 50 // max expression depth

	SPEC_LOOP_STACK_SIZE               = 10  // number of nested WHILE
	SPEC_CASE_WHEN_COUNT_MAX           = 100 // max count of WHEN clauses
	SPEC_FUNCTION_ARGUMENTS_COUNT_MAX  = 100 // for function(arg, arg, arg, ...)
	SPEC_EXPRESSIONS_IN_LIST_COUNT_MAX = 100 // for IN(exp, exp, exp, ...)

	SPEC_OBJECT_NAME_NUMBER_OF_PARTS = 3 // db.schema.object

	SPEC_TABLE_INDEX_KEYLIST_DEFAULT_SIZE = 4

	SPEC_NUMBER_OF_PRINCIPALS_IN_LIST_MAX = 50

	SPEC_INSERT_INTO_COL_LIST_DEFAULT_SIZE        = 30
	SPEC_INSERT_INTO_VALUES_EXPRESSIONS_COUNT_MAX = 200  // max count of expression in VALUES clause, for INSERT INTO table VALUES (exp, exp, exp, ...)
	SPEC_INSERT_INTO_VALUES_COUNT_MAX             = 1000 // max count of VALUES clauses for INSERT INTO

	SPEC_SELECT_NUMBER_OF_COLUMNS_MAX  = 300 // max column count in SELECT clause
	SPEC_SELECT_NUMBER_OF_ORDER_BY_MAX = 20

	PASSWORD_MIN_LENGTH = 6
	PASSWORD_MAX_LENGTH = 128

	SPEC_SLEEP_MAX_SECONDS = 600 // seconds

	SPEC_FLASHTABLE_GENERATOR_START_TBLID = -100 // first value for flash tables (grouptables and sorttables)
)
