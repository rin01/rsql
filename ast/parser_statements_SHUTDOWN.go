package ast

import (
	"rsql"
	"rsql/lex"
)

// parse SHUTDOWN statement.
//
//    SHUTDOWN [ WITH NOWAIT ]
//
// This statement stops RSQL Server. It waits for all sessions to finish. If sessions are still running after 3 minutes, the server is abruptly shutdown.
//
// With option NOWAIT, the server stops abruptly.
//
func (parser *Parser) get_statement_SHUTDOWN() (*Token_stmt_SHUTDOWN, *rsql.Error) {
	var (
		rsql_err *rsql.Error
	)

	token_stmt_shutdown := &Token_stmt_SHUTDOWN{}
	parser.token_init(&token_stmt_shutdown.Token, TOK_STMT_SHUTDOWN)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip SHUTDOWN
		return nil, rsql_err
	}

	// eat WITH NOWAIT option if any

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_auxword(lex.AUXWORD_NOWAIT); rsql_err != nil {
			return nil, rsql_err
		}

		token_stmt_shutdown.Sd_nowait = true
	}

	return token_stmt_shutdown, nil
}
