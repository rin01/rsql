package ast

import (
	"rsql"
	"rsql/lex"
	"rsql/lk"
)

// parse SHRINK TABLE statement.
//
// This statement doesn't exist in MS SQL Server.
//
//    SHRINK TABLE
//       [ database_name . [ schema_name ] . | schema_name . ] table_name
//
// This statement deletes all unused pages at end of the table and index files.
// The user needs DELETE permission, like TRUNCATE TABLE, even though no record is deleted.
//
// SHRINK TABLE cannot be run inside an explicit transaction (BEGIN TRAN ... COMMIT).
//
func (parser *Parser) get_statement_SHRINK_TABLE() (*Token_stmt_SHRINK_TABLE, *rsql.Error) {
	var (
		rsql_err                *rsql.Error
		qname                   rsql.Object_qname_t
		token_stmt_shrink_table *Token_stmt_SHRINK_TABLE
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_SHRINK)

	if rsql_err := parser.Check_compatibility(COMPAT_SHRINK); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip SHRINK
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_TABLE); rsql_err != nil { // expect TABLE keyword
		return nil, rsql_err
	}

	// create Token_stmt_SHRINK_TABLE

	token_stmt_shrink_table = &Token_stmt_SHRINK_TABLE{}
	parser.token_init(&token_stmt_shrink_table.Token, TOK_STMT_SHRINK_TABLE)

	// eat table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_shrink_table.Sk_table_qname = qname

	// put in Registration

	parser.Registration.Register_object_qname(token_stmt_shrink_table.Sk_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_DELETE)

	return token_stmt_shrink_table, nil
}
