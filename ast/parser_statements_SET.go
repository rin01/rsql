package ast

import (
	"rsql"
	"rsql/lang"
	"rsql/lex"
)

// eats a plain or compund assignment operator.
//
//   - if plain assignment '=',         returns      (TOK_PRIM_OPERATOR_COMP_EQUAL, Lexeme{},                 nil)
//   - if compund operator, e.g. '+=' , returns e.g. (TOK_PRIM_OPERATOR_PLUS,       lex.LEXEME_OPERATOR_PLUS, nil)
//
func (parser *Parser) Eat_compound_assignment_operator() (prim_subtype_underlying_operator Prim_subtype_t, lexeme_underlying_operator lex.Lexeme, rsql_err *rsql.Error) {

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_OPERATOR_COMP_EQUAL:
		return TOK_PRIM_OPERATOR_COMP_EQUAL, lex.Lexeme{}, nil // plain assignment '='. The returned lexeme_underlying_operator is invalid.

	case lex.LEX_OPERATOR_ASSIGN_PLUS:
		return TOK_PRIM_OPERATOR_PLUS, lex.LEXEME_OPERATOR_PLUS, nil // compound assignment '+='. A new Token_primary for the operator '+' will be created.

	case lex.LEX_OPERATOR_ASSIGN_MINUS:
		return TOK_PRIM_OPERATOR_MINUS, lex.LEXEME_OPERATOR_MINUS, nil // compound assignment '-='. A new Token_primary for the operator '-' will be created.

	case lex.LEX_OPERATOR_ASSIGN_MULT:
		return TOK_PRIM_OPERATOR_MULT, lex.LEXEME_OPERATOR_MULT, nil // compound assignment '*='. A new Token_primary for the operator '*' will be created.

	case lex.LEX_OPERATOR_ASSIGN_DIV:
		return TOK_PRIM_OPERATOR_DIV, lex.LEXEME_OPERATOR_DIV, nil // compound assignment '/='. A new Token_primary for the operator '/' will be created.

	case lex.LEX_OPERATOR_ASSIGN_MOD:
		return TOK_PRIM_OPERATOR_MOD, lex.LEXEME_OPERATOR_MOD, nil // compound assignment '%='. A new Token_primary for the operator '%' will be created.

	case lex.LEX_OPERATOR_ASSIGN_BIT_AND:
		return TOK_PRIM_OPERATOR_BIT_AND, lex.LEXEME_OPERATOR_BIT_AND, nil // compound assignment '&='. A new Token_primary for the operator '&' will be created.

	case lex.LEX_OPERATOR_ASSIGN_BIT_OR:
		return TOK_PRIM_OPERATOR_BIT_OR, lex.LEXEME_OPERATOR_BIT_OR, nil // compound assignment '|='. A new Token_primary for the operator '|' will be created.

	case lex.LEX_OPERATOR_ASSIGN_BIT_XOR:
		return TOK_PRIM_OPERATOR_BIT_XOR, lex.LEXEME_OPERATOR_BIT_XOR, nil // compound assignment '^='. A new Token_primary for the operator '^' will be created.

	default:
		return 0, lex.Lexeme{}, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BAD_ASSIGNMENT_OPERATOR, rsql.ERROR_BATCH_ABORT)
	}
}

// Eat_ASSIGNMENT_operator_and_expression is an auxiliary function to parse assignment to a variable.
//
// It parses the [assignment operator][expression] sequence.
//
//      It can be a plain assignment :
//        - @variable = expression
//
//      or a compound assignment :
//        - @variable += expression
//        - @variable -= expression
//        - @variable *= expression
//        - etc
//
// token_primary_lvalue is the variable or identifier to be assigned to.
//
// Returns new Token_stmt_ASSIGNMENT token, with token_primary_lvalue as left value, and expression eaten as right value.
//
func (parser *Parser) Eat_ASSIGNMENT_operator_and_expression(token_primary_lvalue *Token_primary) (*Token_stmt_ASSIGNMENT, *rsql.Error) {
	var (
		rsql_err                          *rsql.Error
		prim_subtype_underlying_operator  Prim_subtype_t         // assignment
		lexeme_underlying_operator        lex.Lexeme             // for compound assignment. Just the lexeme of the underlying operator '+', '-' etc only for info.
		token_expression                  *Token_primary         // rvalue, or will be combined with an underlying operator in case of '+=', '*=' etc.
		token_primary_underlying_operand  *Token_primary         // for compound assignment. New token which is a copy of the variable or identifier.
		token_primary_underlying_operator *Token_primary         // for compound assignment. For example, for '+=' assignment, it will be a new '+' operator token.
		token_stmt_assignment             *Token_stmt_ASSIGNMENT // result assignment token
	)

	// token_primary_lvalue += token_expression   is transformed into   token_primary_lvalue = token_primary_underlying_operand + token_expression
	// where token_primary_underlying_operand is just a copy of token_primary_lvalue
	// and token_primary_underlying_operator is '+'.

	/* current lexeme must be an assignment operator */

	if prim_subtype_underlying_operator, lexeme_underlying_operator, rsql_err = parser.Eat_compound_assignment_operator(); rsql_err != nil {
		return nil, rsql_err
	}

	/* create Token_assignment */

	token_stmt_assignment = &Token_stmt_ASSIGNMENT{}
	parser.token_init(&token_stmt_assignment.Token, TOK_STMT_ASSIGNMENT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip assignment operator
		return nil, rsql_err
	}

	/* eat expression */

	if token_expression, rsql_err = parser.Eat_expression(); rsql_err != nil {
		return nil, rsql_err
	}

	/* for compound assignment, create a new Token_primary for the underlying variable or identifier, and a new Token_primary for the operator. */
	/* Then, affect the expression eaten to the underlying operator. */

	if prim_subtype_underlying_operator != TOK_PRIM_OPERATOR_COMP_EQUAL {
		// create a new Token_primary of the underlying variable or identifier (copy of the variable on the left of assignment operator)

		token_primary_underlying_operand = token_primary_lvalue.clone_shallow()

		// create a new Token_primary for underlying operator (operator in the assignment)

		token_primary_underlying_operator = parser.New_token_primary(TOK_PRIM_OPERATOR, prim_subtype_underlying_operator, lexeme_underlying_operator)
		token_primary_underlying_operator.Tok_batch_line = token_stmt_assignment.Tok_batch_line
		// members prim_precedence and prim_associativity are not useful here (they are used only for expression parsing),
		//   so we don't update them.

		token_primary_underlying_operator.Prim_cardinality = TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS // used when walking the tree, to walk the children of the token
		token_primary_underlying_operator.Prim_status = TOK_PRIM_STATUS_COMPLETE

		token_primary_underlying_operator.Prim_left = token_primary_underlying_operand
		token_primary_underlying_operator.Prim_right = token_expression

		token_expression = token_primary_underlying_operator
	}

	/* fill out Token_assignment */

	token_stmt_assignment.Set_lvalue = token_primary_lvalue
	token_stmt_assignment.Set_rvalue = token_expression

	return token_stmt_assignment, nil
}

// get_statement_SET parses the assignment part of a SET statement.
//
//    SET @variable = expression
//
//    SET @variable += expression   etc
//
// The keyword SET has already been eaten. The current lexeme must be the variable to set.
//
func (parser *Parser) get_statement_SET() (*Token_stmt_ASSIGNMENT, *rsql.Error) {
	var (
		rsql_err               *rsql.Error
		token_stmt_assignment  *Token_stmt_ASSIGNMENT
		token_primary_variable *Token_primary
	)

	assert(parser.Current_lexeme.Lex_type == lex.LEX_VARIABLE)

	/* create Token_primary for variable */

	if parser.Current_lexeme.Lex_type != lex.LEX_VARIABLE {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_VARIABLE_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if token_primary_variable, rsql_err = parser.Eat_variable(); rsql_err != nil {
		return nil, rsql_err
	}

	/* eat assignment and expresssion */

	if token_stmt_assignment, rsql_err = parser.Eat_ASSIGNMENT_operator_and_expression(token_primary_variable); rsql_err != nil {
		return nil, rsql_err
	}

	return token_stmt_assignment, nil
}

// get_statement_SET_FULL_SYSLANGUAGE parses the assignment part of a SET LANGUAGE statement.
//
//    SET LANGUAGE [fr-CH]
//    SET LANGUAGE 'fr-CH'
//    SET LANGUAGE @a         with @a being a VARCHAR variable
//
//    These statements are replaced by:
//    SET _@current_language = 'fr_CH'    or
//    SET _@current_language = @a
//
// The keyword SET has already been eaten. The current lexeme must be LANGUAGE.
//
func (parser *Parser) get_statement_SET_FULL_SYSLANGUAGE() (*Token_stmt_ASSIGNMENT, *rsql.Error) {
	var (
		rsql_err              *rsql.Error
		token_stmt_assignment *Token_stmt_ASSIGNMENT
		token_primary_lvalue  *Token_primary
		token_expression      *Token_primary
		langinfo_key          string
	)

	/* create left Token_primary (assignment target) */

	token_primary_lvalue = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE)

	/* create Token_assignment */

	token_stmt_assignment = &Token_stmt_ASSIGNMENT{}
	parser.token_init(&token_stmt_assignment.Token, TOK_STMT_ASSIGNMENT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip LANGUAGE
		return nil, rsql_err
	}

	/* eat expression */

	switch parser.Current_lexeme.Lex_type { // expression must be contain only one term (identifier, literal string, or variable)
	case lex.LEX_IDENTPART,
		lex.LEX_LITERAL_STRING:
		if langinfo_key, rsql_err = lang.Normalize_language(parser.Current_lexeme.Lex_word); rsql_err != nil { // e.g. French->fr-fr    fr_FR->fr-fr
			return nil, rsql_err
		}
		token_expression = parser.Create_TOK_PRIM_LITERAL_STRING(langinfo_key)

	case lex.LEX_VARIABLE:
		token_expression = parser.Create_TOK_PRIM_VARIABLE(parser.Current_lexeme)

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SET_LANGUAGE, rsql.ERROR_BATCH_ABORT)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip lexeme
		return nil, rsql_err
	}

	/* fill out Token_assignment */

	token_stmt_assignment.Set_lvalue = token_primary_lvalue
	token_stmt_assignment.Set_rvalue = token_expression

	return token_stmt_assignment, nil
}

// get_statement_SET_FIRSTDAYOFWEEK_SYSLANGUAGE parses the assignment part of a SET DATEFIRST statement.
//
//    SET DATEFIRST 3
//    SET DATEFIRST @a         with @a being an integer variable
//
//    These statements are replaced by:
//    SET _@current_language = firstdayofweek(_@current_language, 3)    or
//    SET _@current_language = firstdayofweek(_@current_language, @a)
//
// The keyword SET has already been eaten. The current lexeme must be DATEFIRST.
//
func (parser *Parser) get_statement_SET_FIRSTDAYOFWEEK_SYSLANGUAGE() (*Token_stmt_ASSIGNMENT, *rsql.Error) {
	var (
		rsql_err              *rsql.Error
		token_stmt_assignment *Token_stmt_ASSIGNMENT
		token_primary_lvalue  *Token_primary
		token_expression      *Token_primary
		token_sysfunc         *Token_primary
	)

	/* create left Token_primary (assignment target) */

	token_primary_lvalue = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE)

	/* create Token_assignment */

	token_stmt_assignment = &Token_stmt_ASSIGNMENT{}
	parser.token_init(&token_stmt_assignment.Token, TOK_STMT_ASSIGNMENT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip DATEFIRST
		return nil, rsql_err
	}

	/* eat expression */

	switch parser.Current_lexeme.Lex_subtype { // expression must be contain only one term (identifier, or variable)
	case lex.LEX_LITERAL_NUMBER_INTEGRAL:
		if token_expression, rsql_err = parser.Eat_literal_number(); rsql_err != nil {
			return nil, rsql_err
		}

	case lex.LEX_VARIABLE_SUB:
		if token_expression, rsql_err = parser.Eat_variable(); rsql_err != nil {
			return nil, rsql_err
		}

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SET_DATEFIRST, rsql.ERROR_BATCH_ABORT)
	}

	/* create function token */

	token_sysfunc = parser.Create_TOK_PRIM_SYSFUNC_FIRSTDAYOFWEEK_SYSLANGUAGE(token_primary_lvalue.clone_shallow(), token_expression)
	token_sysfunc.Tok_batch_line = token_expression.Tok_batch_line

	/* fill out Token_assignment */

	token_stmt_assignment.Set_lvalue = token_primary_lvalue
	token_stmt_assignment.Set_rvalue = token_sysfunc

	return token_stmt_assignment, nil
}

// get_statement_SET_DMY_SYSLANGUAGE parses the assignment part of a SET DATEFORMAT statement.
//
//    SET DATEFORMAT ymd
//    SET DATEFORMAT 'ymd'
//    SET DATEFORMAT @a         with @a being a VARCHAR variable
//
//    These statements are replaced by:
//    SET _@current_language = dmy(_@current_language, 'ymd')    or
//    SET _@current_language = dmy(_@current_language, @a)
//
// The keyword SET has already been eaten. The current lexeme must be DATEFORMAT.
//
func (parser *Parser) get_statement_SET_DMY_SYSLANGUAGE() (*Token_stmt_ASSIGNMENT, *rsql.Error) {
	var (
		rsql_err              *rsql.Error
		token_stmt_assignment *Token_stmt_ASSIGNMENT
		token_primary_lvalue  *Token_primary
		token_expression      *Token_primary
		token_sysfunc         *Token_primary
	)

	/* create left Token_primary (assignment target) */

	token_primary_lvalue = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE)

	/* create Token_assignment */

	token_stmt_assignment = &Token_stmt_ASSIGNMENT{}
	parser.token_init(&token_stmt_assignment.Token, TOK_STMT_ASSIGNMENT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip DATEFIRST
		return nil, rsql_err
	}

	/* eat expression */

	switch parser.Current_lexeme.Lex_type { // expression must be contain only one term (identifier, literal string, or variable)
	case lex.LEX_IDENTPART:
		token_expression = parser.Create_TOK_PRIM_LITERAL_STRING(parser.Current_lexeme.Lex_word) // we want a literal string instead of an identifier
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {                                // skip IDENTPART
			return nil, rsql_err
		}

	case lex.LEX_LITERAL_STRING:
		if token_expression, rsql_err = parser.Eat_literal_string(); rsql_err != nil {
			return nil, rsql_err
		}

	case lex.LEX_VARIABLE:
		if token_expression, rsql_err = parser.Eat_variable(); rsql_err != nil {
			return nil, rsql_err
		}

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SET_DATEFORMAT, rsql.ERROR_BATCH_ABORT)
	}

	/* create function token */

	token_sysfunc = parser.Create_TOK_PRIM_SYSFUNC_DMY_SYSLANGUAGE(token_primary_lvalue.clone_shallow(), token_expression)
	token_sysfunc.Tok_batch_line = token_expression.Tok_batch_line

	/* fill out Token_assignment */

	token_stmt_assignment.Set_lvalue = token_primary_lvalue
	token_stmt_assignment.Set_rvalue = token_sysfunc

	return token_stmt_assignment, nil
}
