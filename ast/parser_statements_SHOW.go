package ast

import (
	"strings"
	"text/template"

	"rsql"
	"rsql/lex"
)

// parse SHOW INFO statement.
// This statement doesn't exist in MS SQL Server.
//
//       SHOW {I|INFO}
//
// Displays version, server directory, server default collation, current login, current user, current database, current language
//
func (parser *Parser) get_statement_SHOW_INFO() (*Token_stmt_SHOW_INFO, *rsql.Error) {

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip INFO
		return nil, rsql_err
	}

	token_stmt_show_info := &Token_stmt_SHOW_INFO{}
	parser.token_init(&token_stmt_show_info.Token, TOK_STMT_SHOW_INFO)

	token_stmt_show_info.Shinf_servername = parser.Servername
	token_stmt_show_info.Shinf_server_default_collation = parser.Server_default_collation

	token_stmt_show_info.Shinf_login = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_SYSTEM_USER_NAME)
	token_stmt_show_info.Shinf_current_user = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_USER_NAME)
	token_stmt_show_info.Shinf_current_database = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_DB_NAME)
	token_stmt_show_info.Shinf_current_schema = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_SCHEMA_NAME)
	token_stmt_show_info.Shinf_current_language = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_LANGUAGE)

	return token_stmt_show_info, nil
}

// parse SHOW COLLATIONS statement.
// This statement doesn't exist in MS SQL Server.
//
//       SHOW {COLL|COLLATION|COLLATIONS}
//
// Displays a list of all supported collations.
//
func (parser *Parser) get_statement_SHOW_COLLATIONS() (*Token_stmt_SHOW_COLLATIONS, *rsql.Error) {

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip COLLATIONS
		return nil, rsql_err
	}

	token_stmt_show_collations := &Token_stmt_SHOW_COLLATIONS{}
	parser.token_init(&token_stmt_show_collations.Token, TOK_STMT_SHOW_COLLATIONS)

	return token_stmt_show_collations, nil
}

// parse SHOW LANGUAGES statement.
// This statement doesn't exist in MS SQL Server.
//
//       SHOW {LANG|LANGUAGE|LANGUAGES}
//
// Displays a list of all supported languages.
//
func (parser *Parser) get_statement_SHOW_LANGUAGES() (*Token_stmt_SHOW_LANGUAGES, *rsql.Error) {

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip LANGUAGES
		return nil, rsql_err
	}

	token_stmt_show_languages := &Token_stmt_SHOW_LANGUAGES{}
	parser.token_init(&token_stmt_show_languages.Token, TOK_STMT_SHOW_LANGUAGES)

	return token_stmt_show_languages, nil
}

// parse SHOW LOCKS statement.
// This statement doesn't exist in MS SQL Server.
//
//       SHOW {LOCK|LOCKS}
//
// Displays a list of all locks on objects and databases.
//
func (parser *Parser) get_statement_SHOW_LOCKS() (*Token_stmt_SHOW_LOCKS, *rsql.Error) {

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip LOCKS
		return nil, rsql_err
	}

	token_stmt_show_locks := &Token_stmt_SHOW_LOCKS{}
	parser.token_init(&token_stmt_show_locks.Token, TOK_STMT_SHOW_LOCKS)

	return token_stmt_show_locks, nil
}

// parse SHOW WORKERS statement.
// This statement doesn't exist in MS SQL Server.
//
//       SHOW {WORKER|WORKERS}
//
// Displays a list of all workers.
//
func (parser *Parser) get_statement_SHOW_WORKERS() (*Token_stmt_SHOW_WORKERS, *rsql.Error) {

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip WORKERS
		return nil, rsql_err
	}

	token_stmt_show_workers := &Token_stmt_SHOW_WORKERS{}
	parser.token_init(&token_stmt_show_workers.Token, TOK_STMT_SHOW_WORKERS)

	return token_stmt_show_workers, nil
}

// parse SHOW statement.
// This statement doesn't exist in MS SQL Server.
//
// Only "sa" or "dbo" of any database can run this command.
//
//       SHOW <output_mode> {P|PARAM|PARAMS|PARAMETER|PARAMETERS} [parameter_name]
//       SHOW <output_mode> {P|PARAM|PARAMS|PARAMETER|PARAMETERS} [LIKE] 'pattern'
//
//       SHOW <output_mode> {L|LOGIN|LOGINS} [login_name]
//       SHOW <output_mode> {L|LOGIN|LOGINS} [LIKE] 'pattern'
//
//       SHOW <output_mode> {D|DB|DBS|DATABASE|DATABASES} [database_name]
//       SHOW <output_mode> {D|DB|DBS|DATABASE|DATABASES} [LIKE] 'pattern'
//
//       SHOW [ALL] [PERM] <output_mode> {U|USER|USERS} [user_name]
//       SHOW [ALL] [PERM] <output_mode> {U|USER|USERS} [LIKE] 'pattern'
//
//       SHOW [ALL] [PERM] <output_mode> {R|ROLE|ROLES} [role_name]
//       SHOW [ALL] [PERM] <output_mode> {R|ROLE|ROLES} [LIKE] 'pattern'
//
//       SHOW [ALL] [PERM] <output_mode> {T|TABLE|TABLES} [qualified_table_name|unqualified_table_name]
//       SHOW [ALL] [PERM] <output_mode> {T|TABLE|TABLES} [LIKE] 'pattern'
//
//
//       ALL
//          objects in all databases are output. Without ALL, only objects in current database are output.
//          It is only available for USERS, ROLES and TABLES. Else, it is just ignored.
//          ALL is not compatible with SQL output mode.
//
//       PERM
//          displays permissions for USERS, ROLES and TABLES only. Else, it is just ignored.
//          When used with SQL output mode, GRANT and DENY permissions statements are only displayed for TABLE, not for USER nor ROLE:
//                    SHOW SQL PERM TABLES
//
//       <output_mode> ::= NORMAL|ID|SQL|TEMPLATE = 'template_text'
//           NORMAL
//              output is plain text. It is the default output if nothing is specified.
//
//           ID
//              same as NORMAL, but id of the objects are displayed
//
//           SQL
//              output is SQL statement
//
//           TEMPLATE = 'template_text'
//              output is the template text, with placeholders replaced by proper values.
//              Available placeholder names are:
//                  for PARAMETERS: {{.parameter_name}}   {{.parameter_id}}     {{.parameter_value}}
//                  for LOGINS:     {{.login_name}}       {{.login_id}}         {{.default_database_name}}      {{.default_language}}    {{.disabled}}
//                  for DATABASES:  {{.database_name}}    {{.database_id}}      {{.database_owner}}             {{.database_status}}     {{.database_mode}}     {{.database_access}}
//                  for USERS:      {{.user_name}}        {{.user_id}}          {{.database_name}}              {{.member_of}}           {{.login_name}}        {{.disabled}}
//                  for ROLES:      {{.role_name}}        {{.role_id}}          {{.database_name}}              {{.member_of}}
//                  for TABLES:     {{.database_name}}    {{.schema_name}}      {{.table_name}}                 {{.table_id}}
//
//              See Golang Template package for more information about available template syntax: https://golang.org/pkg/text/template/
//
//              In addition to the predefined global functions available in templates, e.g. "printf", "len", "eq", the function "join" is also available (see example below).
//
//              It is very useful if you want to create statements to drop all users, tables, etc (see example below).
//
//
// SHOW displays the list of parameters, logins, databases, users, roles, and tables.
// To display the columns, datatypes and indexes of a table, you must use the SQL output mode:
//          SHOW SQL TABLES
//
//
//  Examples:
//
//       SHOW P
//       SHOW PARAMETERS                        same as SHOW P
//       SHOW SQL P                             output is SQL statements
//
//       SHOW L
//       SHOW LOGINS                            same as SHOW L
//
//       SHOW D
//       SHOW DB                                same as SHOW D
//       SHOW DBS                               same as SHOW D
//       SHOW DATABASE                          same as SHOW D
//       SHOW DATABASES                         same as SHOW D
//
//       SHOW U
//       SHOW USER                              same as SHOW U
//
//       SHOW R
//       SHOW ROLE                              same as SHOW R
//
//       SHOW T
//       SHOW TABLE                             same as SHOW T
//       SHOW TABLES                            same as SHOW T
//       SHOW NORMAL TABLES                     same as SHOW T
//       SHOW TABLE clients
//       SHOW TABLE store..clients
//       SHOW TABLE LIKE 'cli%'
//       SHOW TABLE 'cli%'
//       SHOW SQL TABLE clients                 display table columns as CREATE TABLE statement
//       SHOW SQL PERM TABLE clients            CREATE TABLE statement and GRANT/DENY permission statements
//
//       SHOW ALL TABLE                         display tables in all databases
//       SHOW ALL TABLE clients                 unqualified name expected
//       SHOW ALL TABLE LIKE 'cli%'
//
//       SHOW TEMPLATE = 'Hello, my name is {{.user_name}} and I live in database {{.database_name}}.' USERS
//       SHOW TEMPLATE = '{{.user_name}} is member of {{join .member_of ", "}}' USERS
//       SHOW TEMPLATE = 'drop user {{.user_name}};' USERS
//       SHOW TEMPLATE = '{{if eq .database_name "trashdb"}}TRASHDB is here{{else}}This is database {{.database_name}}{{end}}' DATABASE
//       SHOW TEMPLATE = '{{if eq .default_database_name "customers"}}ALTER LOGIN [{{.login_name}}] WITH DEFAULT_DATABASE=[clients];{{end}}' LOGINS
//
//       Particular case: SHOW TABLE clients      is converted to      SHOW SQL TABLE clients
//           because most probably, you are interested in displaying the table layout.
//
func (parser *Parser) get_statement_SHOW() (*Token_stmt_SHOW, *rsql.Error) {
	var (
		rsql_err       *rsql.Error
		tmpl_text      string
		database_name  string
		schema_name    string
		table_name     string
		login_name     string
		parameter_name string
		principal_name string
		LIKE_pattern   string
		flag_unique    bool
	)

	token_stmt_show := &Token_stmt_SHOW{}
	parser.token_init(&token_stmt_show.Token, TOK_STMT_SHOW)

	// eat NORMAL, ID, SQL, TEMPLATE, and ALL options if any

	token_stmt_show.Sh_output_type = rsql.SH_OUTPUT_NORMAL

LABEL_FOR:
	for {
		switch lexeme := parser.Current_lexeme; {
		case lexeme.Lex_type == lex.LEX_IDENTPART && lexeme.Lex_word == string(lex.AUXWORD_NORMAL):
			token_stmt_show.Sh_output_type = rsql.SH_OUTPUT_NORMAL

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip NORMAL
				return nil, rsql_err
			}

		case lexeme.Lex_type == lex.LEX_IDENTPART && lexeme.Lex_word == string(lex.AUXWORD_ID):
			token_stmt_show.Sh_output_type = rsql.SH_OUTPUT_ID

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip ID
				return nil, rsql_err
			}

		case lexeme.Lex_type == lex.LEX_IDENTPART && lexeme.Lex_word == string(lex.AUXWORD_SQL):
			token_stmt_show.Sh_output_type = rsql.SH_OUTPUT_SQL

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip SQL
				return nil, rsql_err
			}

		case lexeme.Lex_type == lex.LEX_IDENTPART && lexeme.Lex_word == string(lex.AUXWORD_TEMPLATE):
			token_stmt_show.Sh_output_type = rsql.SH_OUTPUT_TEMPLATE

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip TEMPLATE
				return nil, rsql_err
			}

			if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
				return nil, rsql_err
			}

			if tmpl_text, rsql_err = parser.Eat_literal_stringval(); rsql_err != nil { // eat template
				return nil, rsql_err
			}

			var err error
			var fns = template.FuncMap{ // we want to add "join" as a function available in templates
				"join": strings.Join,
			}
			tmpl := template.New("template").Funcs(fns).Option("missingkey=error")
			if tmpl, err = tmpl.Parse(tmpl_text); err != nil {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SHOW_BAD_TEMPLATE, rsql.ERROR_BATCH_ABORT, err)
			}
			token_stmt_show.Sh_template = tmpl

		case lexeme.Lex_subtype == lex.LEX_OPERATOR_ALL:
			token_stmt_show.Sh_option_ALL = true

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip ALL
				return nil, rsql_err
			}

		case lexeme.Lex_type == lex.LEX_IDENTPART && (lexeme.Lex_word == string(lex.AUXWORD_PERM) || lexeme.Lex_word == string(lex.AUXWORD_PERMS) || lexeme.Lex_word == string(lex.AUXWORD_PERMISSION) || lexeme.Lex_word == string(lex.AUXWORD_PERMISSIONS)):
			token_stmt_show.Sh_option_PERM = true

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip PERM
				return nil, rsql_err
			}

		default:
			break LABEL_FOR
		}
	}

	if token_stmt_show.Sh_output_type == rsql.SH_OUTPUT_SQL && token_stmt_show.Sh_option_ALL {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SHOW_ALL_NOT_ALLOWED, rsql.ERROR_BATCH_ABORT)
	}

	// eat object type

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type == lex.LEX_IDENTPART && (lexeme.Lex_word == string(lex.AUXWORD_P) || lexeme.Lex_word == string(lex.AUXWORD_PARAM) || lexeme.Lex_word == string(lex.AUXWORD_PARAMS) || lexeme.Lex_word == string(lex.AUXWORD_PARAMETER) || lexeme.Lex_word == string(lex.AUXWORD_PARAMETERS)):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip PARAMETER, PARAMETERS, P, etc
			return nil, rsql_err
		}

		token_stmt_show.Sh_instruction_code = rsql.INSTR_STMT_SHOW_PARAMETER
		goto LABEL_SHOW_PARAMETER

	case lexeme.Lex_type == lex.LEX_IDENTPART && (lexeme.Lex_word == string(lex.AUXWORD_L) || lexeme.Lex_word == string(lex.AUXWORD_LOGIN) || lexeme.Lex_word == string(lex.AUXWORD_LOGINS)):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip LOGIN, LOGINS, L, etc
			return nil, rsql_err
		}

		token_stmt_show.Sh_instruction_code = rsql.INSTR_STMT_SHOW_LOGIN
		goto LABEL_SHOW_LOGIN

	case lexeme.Lex_subtype == lex.LEX_KEYWORD_DATABASE,
		lexeme.Lex_type == lex.LEX_IDENTPART && (lexeme.Lex_word == string(lex.AUXWORD_D) || lexeme.Lex_word == string(lex.AUXWORD_DB) || lexeme.Lex_word == string(lex.AUXWORD_DBS) || lexeme.Lex_word == string(lex.AUXWORD_DATABASES)):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip DATABASE, DATABASES, D, etc
			return nil, rsql_err
		}

		token_stmt_show.Sh_instruction_code = rsql.INSTR_STMT_SHOW_DATABASE
		goto LABEL_SHOW_DATABASE

	case lexeme.Lex_subtype == lex.LEX_KEYWORD_USER,
		lexeme.Lex_type == lex.LEX_IDENTPART && (lexeme.Lex_word == string(lex.AUXWORD_U) || lexeme.Lex_word == string(lex.AUXWORD_USERS)):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip USER, USERS, U, etc
			return nil, rsql_err
		}

		token_stmt_show.Sh_instruction_code = rsql.INSTR_STMT_SHOW_USER // SHOW USER shows in fact all principals (users and roles)
		goto LABEL_SHOW_USER

	case lexeme.Lex_subtype == lex.LEX_KEYWORD_ROLE,
		lexeme.Lex_type == lex.LEX_IDENTPART && (lexeme.Lex_word == string(lex.AUXWORD_R) || lexeme.Lex_word == string(lex.AUXWORD_ROLES)):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip ROLE, ROLES, R, etc
			return nil, rsql_err
		}

		token_stmt_show.Sh_instruction_code = rsql.INSTR_STMT_SHOW_ROLE
		goto LABEL_SHOW_ROLE

	case lexeme.Lex_subtype == lex.LEX_KEYWORD_TABLE,
		lexeme.Lex_type == lex.LEX_IDENTPART && (lexeme.Lex_word == string(lex.AUXWORD_T) || lexeme.Lex_word == string(lex.AUXWORD_TBL) || lexeme.Lex_word == string(lex.AUXWORD_TBLS) || lexeme.Lex_word == string(lex.AUXWORD_TABLES)):

		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip TABLE, TABLES, T, etc
			return nil, rsql_err
		}

		token_stmt_show.Sh_instruction_code = rsql.INSTR_STMT_SHOW_TABLE
		goto LABEL_SHOW_TABLE

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_SHOW_TYPE_UNKNOWN, rsql.ERROR_BATCH_ABORT)
	}

	//========================================================

LABEL_SHOW_PARAMETER:

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type == lex.LEX_IDENTPART: // eat parameter name
		if parameter_name, rsql_err = parser.Eat_identpart(); rsql_err != nil {
			return nil, rsql_err
		}

	case lexeme.Lex_subtype == lex.LEX_OPERATOR_LIKE, // eat LIKE 'pattern'
		lexeme.Lex_type == lex.LEX_LITERAL_STRING:
		token_stmt_show.Sh_LIKE = true

		if LIKE_pattern, rsql_err = parser.eat_LIKE_pattern(); rsql_err != nil { // eat pattern, which is a literal string in lower case
			return nil, rsql_err
		}

	default:
		// SHOW PARAMETER   (without argument)
	}

	token_stmt_show.Sh_object_name = parameter_name
	token_stmt_show.Sh_LIKE_pattern = LIKE_pattern
	token_stmt_show.Sh_server_default_collation = parser.Server_default_collation

	return token_stmt_show, nil

	//========================================================

LABEL_SHOW_LOGIN:

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type == lex.LEX_IDENTPART: // eat login name
		if login_name, rsql_err = parser.Eat_identpart(); rsql_err != nil {
			return nil, rsql_err
		}

	case lexeme.Lex_subtype == lex.LEX_OPERATOR_LIKE, // eat LIKE 'pattern'
		lexeme.Lex_type == lex.LEX_LITERAL_STRING:
		token_stmt_show.Sh_LIKE = true

		if LIKE_pattern, rsql_err = parser.eat_LIKE_pattern(); rsql_err != nil { // eat pattern, which is a literal string in lower case
			return nil, rsql_err
		}

	default:
		// SHOW LOGIN   (without argument)
	}

	token_stmt_show.Sh_object_name = login_name
	token_stmt_show.Sh_LIKE_pattern = LIKE_pattern
	token_stmt_show.Sh_server_default_collation = parser.Server_default_collation

	return token_stmt_show, nil

	//========================================================

LABEL_SHOW_DATABASE:

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type == lex.LEX_IDENTPART: // eat database name
		if database_name, rsql_err = parser.Eat_identpart(); rsql_err != nil {
			return nil, rsql_err
		}

	case lexeme.Lex_subtype == lex.LEX_OPERATOR_LIKE, // eat LIKE 'pattern'
		lexeme.Lex_type == lex.LEX_LITERAL_STRING:
		token_stmt_show.Sh_LIKE = true

		if LIKE_pattern, rsql_err = parser.eat_LIKE_pattern(); rsql_err != nil { // eat pattern, which is a literal string in lower case
			return nil, rsql_err
		}

	default:
		// SHOW DATABASE   (without argument)
	}

	token_stmt_show.Sh_database_name = database_name
	token_stmt_show.Sh_LIKE_pattern = LIKE_pattern
	token_stmt_show.Sh_server_default_collation = parser.Server_default_collation

	return token_stmt_show, nil

	//========================================================

LABEL_SHOW_USER:

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type == lex.LEX_IDENTPART: // eat principal name
		if principal_name, rsql_err = parser.Eat_identpart(); rsql_err != nil {
			return nil, rsql_err
		}

	case lexeme.Lex_subtype == lex.LEX_OPERATOR_LIKE, // eat LIKE 'pattern'
		lexeme.Lex_type == lex.LEX_LITERAL_STRING:
		token_stmt_show.Sh_LIKE = true

		if LIKE_pattern, rsql_err = parser.eat_LIKE_pattern(); rsql_err != nil { // eat pattern, which is a literal string in lower case
			return nil, rsql_err
		}

	default:
		// SHOW USER   (without argument)
	}

	if token_stmt_show.Sh_option_ALL == false {
		if database_name == "" { // if missing, complete database name
			database_name = parser.parser_current_default_database
		}
	}

	token_stmt_show.Sh_database_name = database_name
	token_stmt_show.Sh_object_name = principal_name
	token_stmt_show.Sh_LIKE_pattern = LIKE_pattern
	token_stmt_show.Sh_server_default_collation = parser.Server_default_collation

	return token_stmt_show, nil

	//========================================================

LABEL_SHOW_ROLE:

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type == lex.LEX_IDENTPART: // eat principal name
		if principal_name, rsql_err = parser.Eat_identpart(); rsql_err != nil {
			return nil, rsql_err
		}

	case lexeme.Lex_subtype == lex.LEX_OPERATOR_LIKE, // eat LIKE 'pattern'
		lexeme.Lex_type == lex.LEX_LITERAL_STRING:
		token_stmt_show.Sh_LIKE = true

		if LIKE_pattern, rsql_err = parser.eat_LIKE_pattern(); rsql_err != nil { // eat pattern, which is a literal string in lower case
			return nil, rsql_err
		}

	default:
		// SHOW ROLE   (without argument)
	}

	if token_stmt_show.Sh_option_ALL == false {
		if database_name == "" { // if missing, complete database name
			database_name = parser.parser_current_default_database
		}
	}

	token_stmt_show.Sh_database_name = database_name
	token_stmt_show.Sh_object_name = principal_name
	token_stmt_show.Sh_LIKE_pattern = LIKE_pattern
	token_stmt_show.Sh_server_default_collation = parser.Server_default_collation

	return token_stmt_show, nil

	//========================================================

LABEL_SHOW_TABLE:

	switch lexeme := parser.Current_lexeme; {
	case lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_DOT) != 0: // eat object name
		if database_name, schema_name, table_name, _, rsql_err = parser.Eat_object_qname_parts(); rsql_err != nil { // database_name and schema_name may be empty string
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_MINUS { // search for exact name
			flag_unique = true

			if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip - symbol
				return nil, rsql_err
			}
		}

	case lexeme.Lex_subtype == lex.LEX_OPERATOR_LIKE, // eat LIKE 'pattern'
		lexeme.Lex_type == lex.LEX_LITERAL_STRING:
		token_stmt_show.Sh_LIKE = true

		if LIKE_pattern, rsql_err = parser.eat_LIKE_pattern(); rsql_err != nil { // eat pattern, which is a literal string in lower case
			return nil, rsql_err
		}

	default:
		// SHOW TABLE   (without argument)
	}

	if token_stmt_show.Sh_option_ALL == false {
		if database_name == "" { // if missing, complete database name
			database_name = parser.parser_current_default_database
		}

		if schema_name == "" { // if missing, complete schema name
			schema_name = parser.parser_current_default_schema
		}
	}

	token_stmt_show.Sh_database_name = database_name
	token_stmt_show.Sh_schema_name = schema_name
	token_stmt_show.Sh_object_name = table_name
	token_stmt_show.Sh_LIKE_pattern = LIKE_pattern
	token_stmt_show.Sh_flag_unique = flag_unique
	token_stmt_show.Sh_server_default_collation = parser.Server_default_collation

	// special case: SHOW mytable     is same as      SHOW SQL mytable

	if token_stmt_show.Sh_option_ALL == false && token_stmt_show.Sh_option_PERM == false && token_stmt_show.Sh_LIKE == false && token_stmt_show.Sh_output_type == rsql.SH_OUTPUT_NORMAL && token_stmt_show.Sh_object_name != "" {
		token_stmt_show.Sh_output_type = rsql.SH_OUTPUT_SQL
	}

	return token_stmt_show, nil
}

// eat_LIKE_pattern is used by get_statement_SHOW(), to eat LIKE string.
// Current lexeme must be "LIKE" operator, or a literal string.
//
func (parser *Parser) eat_LIKE_pattern() (pattern string, rsql_err *rsql.Error) {

	if parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_LIKE { // eat LIKE word
		if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // skip LIKE
			return "", rsql_err
		}
	}

	if pattern, rsql_err = parser.Eat_literal_stringval_lowercase(); rsql_err != nil { // eat pattern, which is a literal string in lower case
		return "", rsql_err
	}

	return pattern, nil
}
