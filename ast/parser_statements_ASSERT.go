package ast

import (
	"rsql"
	"rsql/lex"
)

func (parser *Parser) get_statement_ASSERT_() (*Token_stmt_ASSERT_, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		expr     *Token_primary
	)

	token_stmt_assert := &Token_stmt_ASSERT_{}
	parser.token_init(&token_stmt_assert.Token, TOK_STMT_ASSERT_)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip _ASSERT_
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect lparen
		return nil, rsql_err
	}

	if expr, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat expression
		return nil, rsql_err
	}

	token_stmt_assert.As_expr = expr

	if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect rparen
		return nil, rsql_err
	}

	return token_stmt_assert, nil
}

func (parser *Parser) get_statement_ASSERT_NULL_() (*Token_stmt_ASSERT_NULL_, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		expr     *Token_primary
	)

	token_stmt_assert := &Token_stmt_ASSERT_NULL_{}
	parser.token_init(&token_stmt_assert.Token, TOK_STMT_ASSERT_NULL_)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip _ASSERT_NULL_
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect lparen
		return nil, rsql_err
	}

	if expr, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat expression
		return nil, rsql_err
	}

	token_stmt_assert.As_expr = expr

	if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect rparen
		return nil, rsql_err
	}

	return token_stmt_assert, nil
}

func (parser *Parser) get_statement_ASSERT_ERROR_() (*Token_stmt_ASSERT_ERROR_, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		expr     *Token_primary
	)

	token_stmt_assert := &Token_stmt_ASSERT_ERROR_{}
	parser.token_init(&token_stmt_assert.Token, TOK_STMT_ASSERT_ERROR_)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip _ASSERT_ERROR_
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil { // expect lparen
		return nil, rsql_err
	}

	if expr, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat expression
		return nil, rsql_err
	}

	token_stmt_assert.As_expr = expr

	if rsql_err = parser.Eat_punctuation(lex.LEX_COMMA); rsql_err != nil { // expect comma
		return nil, rsql_err
	}

	if expr, rsql_err = parser.Eat_expression(); rsql_err != nil { // eat suffix expression
		return nil, rsql_err
	}

	token_stmt_assert.As_error_suffix = expr

	if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect rparen
		return nil, rsql_err
	}

	return token_stmt_assert, nil
}
