package ast

import (
	"rsql"
	"rsql/dict"
	"rsql/lex"
	"rsql/lk"
)

// get_statement_DELETE parses DELETE statement.
//
func (parser *Parser) get_statement_DELETE() (*Token_stmt_DELETE, *rsql.Error) {
	var (
		rsql_err             *rsql.Error
		token_stmt_delete    *Token_stmt_DELETE
		target_database_name string
		target_schema_name   string
		target_object_name   string
		xtable_from          *Xtable_from
		target_table         *Xtable_table
		gtabledef            *rsql.GTabledef
		no_from_clause       bool
		is_permanent_table   bool
	)

	// current lexeme must be DELETE

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_DELETE)

	if rsql_err := parser.Check_compatibility(COMPAT_DELETE); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip DELETE
		return nil, rsql_err
	}

	// create Token_stmt_delete

	token_stmt_delete = &Token_stmt_DELETE{}
	parser.token_init(&token_stmt_delete.Token, TOK_STMT_DELETE)

	// ==== skip optional FROM keyword before target table name ====

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_FROM {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip optional FROM
			return nil, rsql_err
		}
	}

	// eat target table name, or alias

	switch parser.Current_lexeme.Lex_type {
	case lex.LEX_VARIABLE: // @vartable
		if target_database_name, target_schema_name, target_object_name, _, rsql_err = parser.Eat_variable_table_qname_parts(); rsql_err != nil {
			return nil, rsql_err
		}

	default: // normal table. If database name or schema name are missing, Eat_object_qname() fills them with current database or schema name.
		if target_database_name, target_schema_name, target_object_name, _, rsql_err = parser.Eat_object_qname_parts(); rsql_err != nil { // target_database_name and target_schema_name may be ""
			return nil, rsql_err
		}

		is_permanent_table = true
	}

	// ==== eat FROM clause ====

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_FROM {
		no_from_clause = true
	}

	if xtable_from, rsql_err = parser.Eat_xtable_from(); rsql_err != nil { // if no FROM clause, a FROM clause containing a Xtable_single{} is created
		return nil, rsql_err
	}

	// lookup target table in FROM clause

	if target_table, rsql_err = xtable_from.Lookup_table(LOOKUP_TABLE_NORMAL, target_database_name, target_schema_name, target_object_name); rsql_err != nil { // normal name lookup, with alias overriding table name
		return nil, rsql_err
	}

	if target_table == nil { // if normal lookup didn't find target table, try looking up with table names only
		if target_table, rsql_err = xtable_from.Lookup_table(LOOKUP_TABLE_NAME_ONLY, target_database_name, target_schema_name, target_object_name); rsql_err != nil { // check table name only (ignore alias)
			return nil, rsql_err
		}
	}

	// target table not found in FROM clause. Create a target Xtable_table and insert it as last element in xtable_from.Fr_list.

	if target_table == nil {
		// create target qname

		if target_database_name == "" {
			target_database_name = parser.parser_current_default_database
			token_stmt_delete.De_target_database_name = target_database_name
		}

		if target_schema_name == "" {
			target_schema_name = parser.parser_current_default_schema
			token_stmt_delete.De_target_schema_name = target_schema_name
		}

		target_qname := rsql.Object_qname_t{Database_name: target_database_name, Schema_name: target_schema_name, Object_name: target_object_name, Object_name_original: target_object_name}

		// get GTabledef

		switch {
		case target_qname.Is_variable_table():
			if gtabledef, rsql_err = parser.Temptable_bag.Get(target_qname.Object_name); rsql_err != nil {
				return nil, rsql_err
			}

		default:
			if gtabledef, rsql_err = dict.MASTER.Get_gtabledef(target_qname); rsql_err != nil {
				return nil, rsql_err
			}
		}

		// create target Xtabledef

		target_table = &Xtable_table{
			Tbl_qname: target_qname,
			Tbl_alias: "",

			Tbl_gtabledef: gtabledef,
		}

		if no_from_clause == true {
			assert(len(xtable_from.Fr_list) == 1)
			xtable_from.Fr_list[0] = target_table // replace the Xtable_single{} that parser.Eat_xtable_from() put here, by target_table
		} else {
			xtable_from.Fr_list = append(xtable_from.Fr_list, target_table) // insert target_table at last position in list
		}
	}

	token_stmt_delete.De_from = xtable_from
	token_stmt_delete.De_target_table = target_table

	token_stmt_delete.De_target_database_name = target_table.Tbl_qname.Database_name
	token_stmt_delete.De_target_schema_name = target_table.Tbl_qname.Schema_name
	token_stmt_delete.De_target_table_name = target_table.Tbl_qname.Object_name

	// for permanent table only, put in Registration

	if is_permanent_table {
		parser.Registration.Register_object_qname(target_table.Tbl_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_DELETE)
	}

	return token_stmt_delete, nil
}
