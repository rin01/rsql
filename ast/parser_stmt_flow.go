package ast

import (
	"rsql"
	"rsql/lex"
)

// create_ASSIGNMENT_for_GETUTCDATE is a helper function, and creates a Token_stmt_ASSIGNMENT token for the statement "SET _@current_timestamp = GETUTCDATE()".
//
func (parser *Parser) create_ASSIGNMENT_for_GETUTCDATE() *Token_stmt_ASSIGNMENT {
	token_stmt_assignment := &Token_stmt_ASSIGNMENT{}
	parser.token_init(&token_stmt_assignment.Token, TOK_STMT_ASSIGNMENT)

	token_stmt_assignment.Set_lvalue = parser.Create_TOK_PRIM_VARIABLE(lex.LEXEME_SYSVAR_CURRENT_TIMESTAMP)
	token_stmt_assignment.Set_rvalue = parser.Create_TOK_PRIM_SYSFUNC_GETUTCDATE()

	return token_stmt_assignment
}

// Eat_sequence_of_statements_up_to_END reads a sequence of statements, up to END keyword, assuming that BEGIN keyword has just been read.
//
// If the BEGIN..END block is empty, an error is returned.
//
// NOTE: We assume that 'BEGIN' HAS BEEN READ !!! This function is called only by parser.get_statement() to continue the parsing of the BEGIN block.
//
func (parser *Parser) Eat_sequence_of_statements_up_to_END() (sequence_first Tokener, sequence_last Tokener, rsql_err *rsql.Error) {
	var (
		token_chain_dummy Token   // use only Tok_next. This chain lists all the statements of the BEGIN..END group.
		token_last        Tokener // pointer to the last token of the chain above.
	)

	/* initialize */

	token_last = &token_chain_dummy

	/* discard all semicolons at beginning of the block */

	for parser.Current_lexeme.Lex_type == lex.LEX_SEMICOLON {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip semicolon
			return nil, nil, rsql_err
		}
	}

	/* read statements until END keyword is reached */

	for parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_END {
		// check if END_OF_BATCH

		if parser.Current_lexeme.Lex_type == lex.LEX_END_OF_BATCH { // END is missing
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_END_MISSING, rsql.ERROR_BATCH_ABORT)
		}

		// read a single statement, a DECLARE/SET group of statements, or a non-empty BEGIN..END group of statements, followed by one optional semicolon.

		if sequence_first, sequence_last, rsql_err = parser.get_statement(); rsql_err != nil { // return at least one statement
			return nil, nil, rsql_err
		}

		token_chain(token_last, sequence_first) // insert sequence into the chain.
		token_last = sequence_last

		// discard all semicolons at end of the statement

		for parser.Current_lexeme.Lex_type == lex.LEX_SEMICOLON {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip semicolon
				return nil, nil, rsql_err
			}
		}

	} // end loop

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip END
		return nil, nil, rsql_err
	}
	parser.Info_line_put_old()

	/* return result */

	if token_last == &token_chain_dummy { // empty BEGIN..END block is invalid syntax.
		return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_EMPTY_BEGIN_END, rsql.ERROR_BATCH_ABORT)
	}

	sequence_first = token_chain_dummy.Next() // always points to a valid statement token
	sequence_first.Set_prev(nil)

	sequence_last = token_last // always points to a valid statement token

	parser.Info_line_update()
	return sequence_first, sequence_last, nil
}

// get_statement_IF parses statement IF..THEN..ELSE
//
func (parser *Parser) get_statement_IF() (*Token_stmt_IF, *rsql.Error) {
	var (
		rsql_err      *rsql.Error
		if_cond       *Token_primary
		if_then       Tokener
		if_else       Tokener
		token_stmt_if *Token_stmt_IF
	)

	/* create Token_stmt_IF */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_IF)

	token_stmt_if = &Token_stmt_IF{}
	parser.token_init(&token_stmt_if.Token, TOK_STMT_IF)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip IF
		return nil, rsql_err
	}

	/* eat condition expression */

	if if_cond, rsql_err = parser.Eat_expression(); rsql_err != nil {
		return nil, rsql_err
	}

	/* eat statement or group of statements for THEN clause */

	if if_then, _, rsql_err = parser.get_statement(); rsql_err != nil { // return one statement, or a list of statements in a BEGIN...END block.
		return nil, rsql_err
	}

	/* check if current lexeme is ELSE */

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_ELSE {

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ELSE
			return nil, rsql_err
		}

		/* eat statement or group of statements for ELSE */

		if if_else, _, rsql_err = parser.get_statement(); rsql_err != nil { // return one statement, or a list of statements in a BEGIN...END block.
			return nil, rsql_err
		}
	}

	/* fill in Token_stmt_IF */

	token_stmt_if.If_cond = if_cond
	token_stmt_if.If_then = if_then
	token_stmt_if.If_else = if_else

	return token_stmt_if, nil
}

// get_statement_WHILE parses statement WHILE
//
func (parser *Parser) get_statement_WHILE() (*Token_stmt_WHILE, *rsql.Error) {
	var (
		rsql_err         *rsql.Error
		while_cond       *Token_primary
		while_body       Tokener
		token_stmt_while *Token_stmt_WHILE
	)

	/* create Token_stmt_WHILE */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WHILE)

	token_stmt_while = &Token_stmt_WHILE{}
	parser.token_init(&token_stmt_while.Token, TOK_STMT_WHILE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WHILE
		return nil, rsql_err
	}

	/* eat condition expression */

	if while_cond, rsql_err = parser.Eat_expression(); rsql_err != nil {
		return nil, rsql_err
	}

	/* register WHILE */

	if rsql_err = parser.while_stack.push(token_stmt_while); rsql_err != nil {
		return nil, rsql_err
	}

	/* eat a statement or a group of statements of the WHILE body */

	if while_body, _, rsql_err = parser.get_statement(); rsql_err != nil { // return one statement, or a list of statements in a BEGIN...END block.
		return nil, rsql_err
	}

	/* fill in Token_stmt_WHILE */

	token_stmt_while.While_cond = while_cond
	token_stmt_while.While_body = while_body

	/* unregister WHILE */

	parser.while_stack.unpush()

	return token_stmt_while, nil
}

// get_statement_BREAK parses statement BREAK
//
func (parser *Parser) get_statement_BREAK() (*Token_stmt_BREAK, *rsql.Error) {
	var (
		rsql_err         *rsql.Error
		token_stmt_break *Token_stmt_BREAK
	)

	/* create Token_stmt_BREAK */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_BREAK)

	token_stmt_break = &Token_stmt_BREAK{}
	parser.token_init(&token_stmt_break.Token, TOK_STMT_BREAK)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip BREAK
		return nil, rsql_err
	}
	parser.Info_line_put_old()

	if parser.while_stack.count() == 0 { // BREAK must be contained in a loop
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_BREAK_WITHOUT_LOOP, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_break.Br_attached_loop = parser.while_stack.top()

	parser.Info_line_update()
	return token_stmt_break, nil
}

// get_statement_CONTINUE parses statement CONTINUE
//
func (parser *Parser) get_statement_CONTINUE() (*Token_stmt_CONTINUE, *rsql.Error) {
	var (
		rsql_err            *rsql.Error
		token_stmt_continue *Token_stmt_CONTINUE
	)

	/* create Token_stmt_CONTINUE */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_CONTINUE)

	token_stmt_continue = &Token_stmt_CONTINUE{}
	parser.token_init(&token_stmt_continue.Token, TOK_STMT_CONTINUE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CONTINUE
		return nil, rsql_err
	}
	parser.Info_line_put_old()

	if parser.while_stack.count() == 0 { // CONTINUE must be contained in a loop
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CONTINUE_WITHOUT_LOOP, rsql.ERROR_BATCH_ABORT)
	}

	token_stmt_continue.Cont_attached_loop = parser.while_stack.top()

	parser.Info_line_update()
	return token_stmt_continue, nil
}

// get_statement_RETURN parses statement RETURN
//
func (parser *Parser) get_statement_RETURN() (*Token_stmt_RETURN, *rsql.Error) {
	var (
		rsql_err          *rsql.Error
		token_stmt_return *Token_stmt_RETURN
		token_expression  *Token_primary
	)

	/* create Token_stmt_RETURN */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_RETURN)

	token_stmt_return = &Token_stmt_RETURN{}
	parser.token_init(&token_stmt_return.Token, TOK_STMT_RETURN)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip RETURN
		return nil, rsql_err
	}
	parser.Info_line_put_old()

	/* eat optional return value */

	switch {
	case parser.Current_lexeme.Lex_type&(lex.LEX_IDENTPART|lex.LEX_KEYWORD) != 0:
		if _, ok := G_SYSFUNC_DESCRIPTION_MAP[parser.Current_lexeme.Lex_word]; ok == true { // if found in sysfunc table, it is a system function. In this case, it is the argument of RETURN.
			if token_expression, rsql_err = parser.Eat_expression(); rsql_err != nil {
				return nil, rsql_err
			}
		}

	case parser.Current_lexeme.Lex_type&(lex.LEX_LITERAL_NUMBER|lex.LEX_LITERAL_HEXASTRING|lex.LEX_LITERAL_STRING|lex.LEX_VARIABLE|lex.LEX_OPERATOR|lex.LEX_LPAREN) != 0:
		if token_expression, rsql_err = parser.Eat_expression(); rsql_err != nil {
			return nil, rsql_err
		}
	}

	if token_expression == nil {
		token_expression = parser.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(0) // create a literal 0 as return value
	}

	token_stmt_return.Ret_expression = token_expression

	parser.Info_line_update()
	return token_stmt_return, nil
}

// get_statement_EXIT parses statement EXIT, synonym of RETURN.
// The returned token is pointer to Token_stmt_RETURN.
//
// Note that "EXIT()" is correct syntax, but "RETURN()" is incorrect.
//
func (parser *Parser) get_statement_EXIT() (*Token_stmt_RETURN, *rsql.Error) {
	var (
		rsql_err          *rsql.Error
		token_stmt_return *Token_stmt_RETURN
		token_expression  *Token_primary
	)

	/* check that EXIT is at beginning of the line */

	if parser.Info_line.Pos != 1 {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_EXIT_NOT_START_OF_LINE, rsql.ERROR_BATCH_ABORT)
	}

	/* create Token_stmt_RETURN */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_EXIT)

	token_stmt_return = &Token_stmt_RETURN{}
	parser.token_init(&token_stmt_return.Token, TOK_STMT_RETURN)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip EXIT
		return nil, rsql_err
	}
	parser.Info_line_put_old()

	/* eat optional return value */

	if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN {
		switch {
		case parser.Rune0 == ')': // EXIT()
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip '('
				return nil, rsql_err
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ')'
				return nil, rsql_err
			}

		default:
			if token_expression, rsql_err = parser.Eat_expression(); rsql_err != nil {
				return nil, rsql_err
			}

			token_stmt_return.Ret_expression = token_expression
		}
	}

	if token_stmt_return.Ret_expression == nil {
		token_expression = parser.Create_TOK_PRIM_LITERAL_NUMBER_INTEGRAL(0) // create a literal 0 as return value
		token_stmt_return.Ret_expression = token_expression
	}

	parser.Info_line_update()
	return token_stmt_return, nil
}

// get_statement_GOTO parses statement GOTO
//
func (parser *Parser) get_statement_GOTO() (*Token_stmt_GOTO, *rsql.Error) {
	var (
		rsql_err        *rsql.Error
		token_stmt_goto *Token_stmt_GOTO
		label           string
	)

	/* create Token_stmt_GOTO */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_GOTO)

	token_stmt_goto = &Token_stmt_GOTO{}
	parser.token_init(&token_stmt_goto.Token, TOK_STMT_GOTO)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip GOTO
		return nil, rsql_err
	}

	/* eat label */

	if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_GOTO_LABEL_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	label = parser.Current_lexeme.Lex_word

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip label
		return nil, rsql_err
	}

	/* fill out Token_stmt_GOTO */

	token_stmt_goto.Gt_label = label
	token_stmt_goto.Gt_all_labels = parser.LABELS // dictionary of all labels in the batch

	return token_stmt_goto, nil
}

// get_statement_LABEL parses statement label.
//
// The label MUST be followed by a colon. Else, it panics.
//
func (parser *Parser) get_statement_LABEL() (*Token_stmt_LABEL, *rsql.Error) {
	var (
		rsql_err         *rsql.Error
		token_stmt_label *Token_stmt_LABEL
		label            string
	)

	/* create Token_stmt_LABEL */
	assert(parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART)

	token_stmt_label = &Token_stmt_LABEL{}
	parser.token_init(&token_stmt_label.Token, TOK_STMT_LABEL)

	/* eat label */

	label = parser.Current_lexeme.Lex_word

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip label
		return nil, rsql_err
	}

	assert(parser.Current_lexeme.Lex_type == lex.LEX_COLON)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip colon
		return nil, rsql_err
	}

	/* fill out Token_stmt_LABEL and register label name */

	token_stmt_label.Lab_label = label

	if rsql_err = parser.LABELS.Put(label, token_stmt_label); rsql_err != nil { // error if duplicate label name
		return nil, rsql_err
	}

	return token_stmt_label, nil
}

// Eat_sequence_of_statements_up_to_LEX_END_OF_BATCH parses a sequence of statements, from current lexeme up to LEX_END_OF_BATCH lexeme.
//
// If sequence is empty, sequence_first and sequence_last are set to nil.
//
// USE [database name] is not only a statement, but also a compiler pragma for parser use.
//
func (parser *Parser) Eat_sequence_of_statements_up_to_LEX_END_OF_BATCH() (sequence_first Tokener, sequence_last Tokener, rsql_err *rsql.Error) {
	var (
		token_chain_dummy Token   // use only Tok_next. This chain lists all the statements.
		token_last        Tokener // pointer to the last token of the chain above.
	)

	/* initialize */

	token_last = &token_chain_dummy

	/* discard all semicolons at beginning */

	for parser.Current_lexeme.Lex_type == lex.LEX_SEMICOLON {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip semicolon
			return nil, nil, rsql_err
		}
	}

	/* read statements until LEX_END_OF_BATCH is reached */

	for parser.Current_lexeme.Lex_type != lex.LEX_END_OF_BATCH {

		// read a single statement, a DECLARE/SET group of statements, or a non-empty BEGIN..END group of statements, followed by one optional semicolon.

		if sequence_first, sequence_last, rsql_err = parser.get_statement(); rsql_err != nil { // return at least one statement
			return nil, nil, rsql_err
		}

		token_chain(token_last, sequence_first) // insert sequence into the chain.
		token_last = sequence_last

		// discard all semicolons at end of the statement

		for parser.Current_lexeme.Lex_type == lex.LEX_SEMICOLON {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip semicolon
				return nil, nil, rsql_err
			}
		}

	} // end loop

	/* return result */

	// if sequence of statements is empty, sequence_first and sequence_last are nil.

	sequence_first = token_chain_dummy.Next() // if list empty, nil

	if sequence_first == nil { // if list empty, set to nil
		sequence_last = nil
	} else {
		sequence_first.Set_prev(nil)
		sequence_last = token_last
	}

	return sequence_first, sequence_last, nil
}

// get_statement eats a single statement, or a sequence of statements generated by a single BEGIN..END block, or by a single DECLARE.
//
// It is used in particular to read a sequence of statements after an IF or WHILE statement.
//
// This function eats exactly one semicolon after the single statement that has been read, or after the BEGIN..END block.
//
// If an empty BEGIN..END block is found, an error is returned.
//
// At least one valid statement is returned.
//
// A syntax error is returned when current lexeme is not a LEX_KEYWORD, like LEX_END_OF_BATCH
//
func (parser *Parser) get_statement() (sequence_first Tokener, sequence_last Tokener, rsql_err *rsql.Error) {
	var (
		statement_single              Tokener // single statement has been read
		auxword                       string
		update_current_timestamp_flag bool // if true, append the statement "SET _@current_timestamp = GETDATE()"
	)

	// ensure that parser.Info_line contains position of current lexeme

	parser.Info_line_update()

	/* call the proper function to parse the statement, or returns an error if statement not recognized. */

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_ALTER:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ALTER
			return nil, nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_AUTHORIZATION:
			statement_single, rsql_err = parser.get_statement_ALTER_AUTHORIZATION()

		case lex.LEX_KEYWORD_DATABASE:
			statement_single, rsql_err = parser.get_statement_ALTER_DATABASE()

		case lex.LEX_KEYWORD_USER:
			statement_single, rsql_err = parser.get_statement_ALTER_USER()

		case lex.LEX_KEYWORD_ROLE:
			statement_single, rsql_err = parser.get_statement_ALTER_ROLE()

		case lex.LEX_KEYWORD_TABLE:
			statement_single, rsql_err = parser.get_statement_ALTER_TABLE()

		case lex.LEX_KEYWORD_INDEX:
			statement_single, rsql_err = parser.get_statement_ALTER_INDEX()

		case lex.LEX_IDENTPART_SUB:
			auxword = parser.Current_lexeme.Lex_word

			switch lex.Auxword_t(auxword) {
			case lex.AUXWORD_LOGIN:
				statement_single, rsql_err = parser.get_statement_ALTER_LOGIN()

			case lex.AUXWORD_SERVER:
				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip SERVER
					return nil, nil, rsql_err
				}

				if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART {
					return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
				}

				auxword = parser.Current_lexeme.Lex_word

				switch lex.Auxword_t(auxword) {
				case lex.AUXWORD_PARAMETER:
					statement_single, rsql_err = parser.get_statement_ALTER_SERVER_PARAMETER()

				default:
					return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
				}

			default:
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		default:
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_BACKUP:
		statement_single, rsql_err = parser.get_statement_BACKUP()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_BULK:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip BULK
			return nil, nil, rsql_err
		}

		switch {
		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INSERT:
			statement_single, rsql_err = parser.get_statement_BULK_INSERT()

		case parser.Current_lexeme.Is_auxword(lex.AUXWORD_EXPORT):
			statement_single, rsql_err = parser.get_statement_BULK_EXPORT()

		default:
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_CHECKPOINT:
		statement_single, rsql_err = parser.get_statement_CHECKPOINT()

	case lex.LEX_KEYWORD_COMMIT:
		statement_single, rsql_err = parser.get_statement_COMMIT_TRANSACTION()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_CREATE:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CREATE
			return nil, nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_CLUSTERED:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CLUSTERED
				return nil, nil, rsql_err
			}
			if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_INDEX { // expect INDEX
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}
			statement_single, rsql_err = parser.get_statement_CREATE_INDEX(rsql.TD_INDEX, rsql.TD_CLUSTERED)

		case lex.LEX_KEYWORD_DATABASE:
			statement_single, rsql_err = parser.get_statement_CREATE_DATABASE()

		case lex.LEX_KEYWORD_DEFAULT:
			statement_single, rsql_err = parser.get_statement_CREATE_DEFAULT()

		case lex.LEX_KEYWORD_INDEX:
			statement_single, rsql_err = parser.get_statement_CREATE_INDEX(rsql.TD_INDEX, rsql.TD_NONCLUSTERED)

		case lex.LEX_KEYWORD_NONCLUSTERED:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NONCLUSTERED
				return nil, nil, rsql_err
			}
			if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_INDEX { // expect INDEX
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}
			statement_single, rsql_err = parser.get_statement_CREATE_INDEX(rsql.TD_INDEX, rsql.TD_NONCLUSTERED)

		case lex.LEX_KEYWORD_PROCEDURE:
			statement_single, rsql_err = parser.get_statement_CREATE_PROCEDURE()

		case lex.LEX_KEYWORD_ROLE:
			statement_single, rsql_err = parser.get_statement_CREATE_ROLE()

		case lex.LEX_KEYWORD_RULE:
			statement_single, rsql_err = parser.get_statement_CREATE_RULE()

		case lex.LEX_KEYWORD_TABLE:
			statement_single, rsql_err = parser.get_statement_CREATE_TABLE(OPT_PERMANENT_TABLE, "") // last argument is not used here (it is for @vartable only)

		case lex.LEX_KEYWORD_TRIGGER:
			statement_single, rsql_err = parser.get_statement_CREATE_TRIGGER()

		case lex.LEX_KEYWORD_UNIQUE: // CREATE UNIQUE [CLUSTERED|NONCLUSTERED] INDEX
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip UNIQUE
				return nil, nil, rsql_err
			}

			switch parser.Current_lexeme.Lex_subtype {
			case lex.LEX_KEYWORD_CLUSTERED:
				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CLUSTERED
					return nil, nil, rsql_err
				}
				if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_INDEX { // expect INDEX
					return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
				}
				statement_single, rsql_err = parser.get_statement_CREATE_INDEX(rsql.TD_UNIQUE, rsql.TD_CLUSTERED)

			case lex.LEX_KEYWORD_NONCLUSTERED:
				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NONCLUSTERED
					return nil, nil, rsql_err
				}
				if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_INDEX { // expect INDEX
					return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
				}
				statement_single, rsql_err = parser.get_statement_CREATE_INDEX(rsql.TD_UNIQUE, rsql.TD_NONCLUSTERED)

			case lex.LEX_KEYWORD_INDEX:
				statement_single, rsql_err = parser.get_statement_CREATE_INDEX(rsql.TD_UNIQUE, rsql.TD_NONCLUSTERED)

			default:
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		case lex.LEX_KEYWORD_USER:
			statement_single, rsql_err = parser.get_statement_CREATE_USER()

		case lex.LEX_KEYWORD_VIEW:
			statement_single, rsql_err = parser.get_statement_CREATE_VIEW()

		case lex.LEX_IDENTPART_SUB:
			auxword = parser.Current_lexeme.Lex_word

			switch lex.Auxword_t(auxword) {
			case lex.AUXWORD_LOGIN:
				statement_single, rsql_err = parser.get_statement_CREATE_LOGIN()

			default:
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		default:
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_DELETE:
		statement_single, rsql_err = parser.get_statement_DELETE()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_DENY:
		statement_single, rsql_err = parser.get_statement_DENY()

	case lex.LEX_KEYWORD_DROP:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip DROP
			return nil, nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_DATABASE:
			statement_single, rsql_err = parser.get_statement_DROP_DATABASE()

		case lex.LEX_KEYWORD_DEFAULT:
			statement_single, rsql_err = parser.get_statement_DROP_DEFAULT()

		case lex.LEX_KEYWORD_INDEX:
			statement_single, rsql_err = parser.get_statement_DROP_INDEX()

		case lex.LEX_KEYWORD_PROCEDURE:
			statement_single, rsql_err = parser.get_statement_DROP_PROCEDURE()

		case lex.LEX_KEYWORD_ROLE:
			statement_single, rsql_err = parser.get_statement_DROP_ROLE()

		case lex.LEX_KEYWORD_RULE:
			statement_single, rsql_err = parser.get_statement_DROP_RULE()

		case lex.LEX_KEYWORD_TABLE:
			statement_single, rsql_err = parser.get_statement_DROP_TABLE()

		case lex.LEX_KEYWORD_TRIGGER:
			statement_single, rsql_err = parser.get_statement_DROP_TRIGGER()

		case lex.LEX_KEYWORD_USER:
			statement_single, rsql_err = parser.get_statement_DROP_USER()

		case lex.LEX_KEYWORD_VIEW:
			statement_single, rsql_err = parser.get_statement_DROP_VIEW()

		case lex.LEX_IDENTPART_SUB:
			auxword = parser.Current_lexeme.Lex_word

			switch lex.Auxword_t(auxword) {
			case lex.AUXWORD_LOGIN:
				statement_single, rsql_err = parser.get_statement_DROP_LOGIN()

			default:
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		default:
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_DUMP:
		statement_single, rsql_err = parser.get_statement_DUMP()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_GRANT:
		statement_single, rsql_err = parser.get_statement_GRANT()

	case lex.LEX_KEYWORD_INSERT:
		statement_single, rsql_err = parser.get_statement_INSERT_INTO()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_THROW:
		statement_single, rsql_err = parser.get_statement_THROW()

	case lex.LEX_KEYWORD_PRINT:
		statement_single, rsql_err = parser.get_statement_PRINT()

	case lex.LEX_KEYWORD_ASSERT_:
		statement_single, rsql_err = parser.get_statement_ASSERT_()

	case lex.LEX_KEYWORD_ASSERT_NULL_:
		statement_single, rsql_err = parser.get_statement_ASSERT_NULL_()

	case lex.LEX_KEYWORD_ASSERT_ERROR_:
		statement_single, rsql_err = parser.get_statement_ASSERT_ERROR_()

	case lex.LEX_KEYWORD_REVOKE:
		statement_single, rsql_err = parser.get_statement_REVOKE()

	case lex.LEX_KEYWORD_RESTORE:
		statement_single, rsql_err = parser.get_statement_RESTORE()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_ROLLBACK:
		statement_single, rsql_err = parser.get_statement_ROLLBACK_TRANSACTION()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_SELECT: // 'SELECT' is a special case. It can be a statement or a subquery expression. Here, it is a statement.
		statement_single, rsql_err = parser.get_statement_SELECT_or_UNION()
		update_current_timestamp_flag = true

	case lex.LEX_LPAREN_SUB: // if lparen is encountered at statement level, it must be a select or union expression.
		if parser.Rune0 != 's' && parser.Rune0 != 'S' && parser.Rune0 != '(' { // this test is quite rough, but it is better than showing ERROR_INVALID_SYNTAX_SELECT_OR_LPAREN_EXPECTED for the line    (hello
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}
		statement_single, rsql_err = parser.get_statement_SELECT_or_UNION()

		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_SHOW:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip SHOW
			return nil, nil, rsql_err
		}

		switch subcommand := parser.Current_lexeme.Lex_word; {
		case parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && (subcommand == string(lex.AUXWORD_I) || subcommand == string(lex.AUXWORD_INFO)):
			statement_single, rsql_err = parser.get_statement_SHOW_INFO()

		case parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && (subcommand == string(lex.AUXWORD_COLL) || subcommand == string(lex.AUXWORD_COLLATION) || subcommand == string(lex.AUXWORD_COLLATIONS)):
			statement_single, rsql_err = parser.get_statement_SHOW_COLLATIONS()

		case parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && (subcommand == string(lex.AUXWORD_LANG) || subcommand == string(lex.AUXWORD_LANGUAGE) || subcommand == string(lex.AUXWORD_LANGUAGES)):
			statement_single, rsql_err = parser.get_statement_SHOW_LANGUAGES()

		case parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && (subcommand == string(lex.AUXWORD_LOCK) || subcommand == string(lex.AUXWORD_LOCKS)):
			statement_single, rsql_err = parser.get_statement_SHOW_LOCKS()

		case parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART && (subcommand == string(lex.AUXWORD_WORKER) || subcommand == string(lex.AUXWORD_WORKERS)):
			statement_single, rsql_err = parser.get_statement_SHOW_WORKERS()

		default:
			statement_single, rsql_err = parser.get_statement_SHOW()
		}

	case lex.LEX_KEYWORD_SLEEP:
		statement_single, rsql_err = parser.get_statement_SLEEP()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_SHUTDOWN:
		statement_single, rsql_err = parser.get_statement_SHUTDOWN()

	case lex.LEX_KEYWORD_TRUNCATE:
		statement_single, rsql_err = parser.get_statement_TRUNCATE_TABLE()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_SHRINK:
		statement_single, rsql_err = parser.get_statement_SHRINK_TABLE()
		update_current_timestamp_flag = true

	case lex.LEX_KEYWORD_UPDATE:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip UPDATE
			return nil, nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_STATISTICS:
			statement_single, rsql_err = parser.get_statement_UPDATE_STATISTICS()

		default:
			statement_single, rsql_err = parser.get_statement_UPDATE()
		}

		update_current_timestamp_flag = true

	// DECLARE, SET, and pragmas

	case lex.LEX_KEYWORD_DECLARE:
		sequence_first, sequence_last, rsql_err = parser.get_statement_DECLARE_and_initializers()

	case lex.LEX_KEYWORD_SET:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip SET
			return nil, nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_VARIABLE_SUB:
			statement_single, rsql_err = parser.get_statement_SET()

		case lex.LEX_IDENTPART_SUB:
			auxword = parser.Current_lexeme.Lex_word

			switch lex.Auxword_t(auxword) {
			case lex.AUXWORD_LANGUAGE:
				statement_single, rsql_err = parser.get_statement_SET_FULL_SYSLANGUAGE()

			case lex.AUXWORD_DATEFORMAT:
				statement_single, rsql_err = parser.get_statement_SET_DMY_SYSLANGUAGE()

			case lex.AUXWORD_DATEFIRST:
				statement_single, rsql_err = parser.get_statement_SET_FIRSTDAYOFWEEK_SYSLANGUAGE()

			case lex.AUXWORD_NOCOUNT:
				statement_single, rsql_err = parser.get_statement_SET_NOCOUNT()

			case lex.AUXWORD_QUOTED_IDENTIFIER, // pragma for lexer  use only
				lex.AUXWORD_ANSI_NULL_DFLT_ON, // pragma for parser  use only
				lex.AUXWORD_PARSEONLY,         // pragma for caller's use only
				lex.AUXWORD_NOEXEC:            // pragma for caller's use only
				statement_single, rsql_err = parser.get_statement_SET_LEXER_OR_PARSER_OPTION(lex.Auxword_t(auxword)) // pragma. No instruction is output for the VM.

			default:
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_OPTION_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

		default:
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

	case lex.LEX_KEYWORD_USE: // pragma for parser use. Also instruction for VM.
		statement_single, rsql_err = parser.get_statement_USE()

	// BEGIN ... END

	case lex.LEX_KEYWORD_BEGIN:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip BEGIN
			return nil, nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_TRAN,
			lex.LEX_KEYWORD_TRANSACTION:
			statement_single, rsql_err = parser.get_statement_BEGIN_TRANSACTION()

		default: // read BEGIN...END sequence of statements
			sequence_first, sequence_last, rsql_err = parser.Eat_sequence_of_statements_up_to_END() // returns an error if empty block
		}

	// flow control

	case lex.LEX_KEYWORD_IF:
		statement_single, rsql_err = parser.get_statement_IF()

	case lex.LEX_KEYWORD_WHILE:
		statement_single, rsql_err = parser.get_statement_WHILE()

	case lex.LEX_KEYWORD_BREAK:
		statement_single, rsql_err = parser.get_statement_BREAK()

	case lex.LEX_KEYWORD_CONTINUE:
		statement_single, rsql_err = parser.get_statement_CONTINUE()

	case lex.LEX_KEYWORD_RETURN:
		statement_single, rsql_err = parser.get_statement_RETURN()

	case lex.LEX_KEYWORD_EXIT:
		statement_single, rsql_err = parser.get_statement_EXIT() // return Token_stmt_RETURN, like RETURN

	case lex.LEX_KEYWORD_GOTO:
		statement_single, rsql_err = parser.get_statement_GOTO()

	case lex.LEX_IDENTPART_SUB: // if identpart is followed by colon, it is a label
		if parser.Rune0 != ':' {
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}
		statement_single, rsql_err = parser.get_statement_LABEL()

	// dangling END and ELSE

	case lex.LEX_KEYWORD_END: // dangling END
		return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_END_WITHOUT_BEGIN, rsql.ERROR_BATCH_ABORT)

	case lex.LEX_KEYWORD_ELSE: // dangling ELSE
		return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ELSE_WITHOUT_IF, rsql.ERROR_BATCH_ABORT)

	default:
		return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	//=== check if error ===

	if rsql_err != nil {
		return nil, nil, rsql_err
	}

	/*===== discard ONLY ONE semicolon following the statement that has been read, or BEGIN..END block that has been read =====*/

	if parser.Current_lexeme.Lex_type == lex.LEX_SEMICOLON {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip semicolon
			return nil, nil, rsql_err
		}
	}

	/*===== append SET _@current_timestamp = GETDATE() if requested =====*/

	if update_current_timestamp_flag == true && parser.disable_current_timestamp_update == false {
		assert(statement_single != nil)

		assignment_getdate := parser.create_ASSIGNMENT_for_GETUTCDATE()

		token_chain(statement_single, assignment_getdate)

		sequence_first = statement_single
		sequence_last = assignment_getdate
		statement_single = nil
	}

	/*===== return first and last token of the chain of statements read =====*/

	if statement_single != nil { // single statement
		sequence_first = statement_single
		sequence_last = statement_single
	}

	return sequence_first, sequence_last, rsql_err
}

// create_ASSIGNMENT_NULL is an auxiliary function to create a new NULL assignment to a variable.
//
// token_primary_lvalue is the variable to be assigned to.
//
// Returns new Token_stmt_ASSIGNMENT token, with token_primary_lvalue as left value, and NULL as right value.
//
func (parser *Parser) create_ASSIGNMENT_NULL(token_primary_lvalue *Token_primary) *Token_stmt_ASSIGNMENT {
	var (
		token_NULL            *Token_primary         // rvalue NULL
		token_stmt_assignment *Token_stmt_ASSIGNMENT // result assignment token
	)

	/* create NULL 'null' Token_primary */

	token_NULL = parser.New_token_primary(TOK_PRIM_LITERAL_NULL, TOK_PRIM_LITERAL_NULL_SUB, lex.LEXEME_KEYWORD_NULL)
	token_NULL.Prim_status = TOK_PRIM_STATUS_COMPLETE

	/* create Token_assignment */

	token_stmt_assignment = &Token_stmt_ASSIGNMENT{}
	parser.token_init(&token_stmt_assignment.Token, TOK_STMT_ASSIGNMENT)

	token_stmt_assignment.Set_lvalue = token_primary_lvalue
	token_stmt_assignment.Set_rvalue = token_NULL

	return token_stmt_assignment
}

// get_statement_DECLARE_and_initializers parses DECLARE statement, and initializers.
//
//      Example :  DECLARE @a int = 123 + 456, @b int, @c int = 789
//
// This function creates a sequence of pairs of new declaration tokens: <Token_stmt_DECLARE_variable, Token_stmt_ASSIGNMENT>.
//
// An assignment token is always created. If no initializer or initializer is "= NULL", an assignment token to NULL is created.
//
func (parser *Parser) get_statement_DECLARE_and_initializers() (sequence_first Tokener, sequence_last Tokener, rsql_err *rsql.Error) {
	var (
		token_stmt_declare_variable *Token_stmt_DECLARE_variable // declaration statement token
		token_primary_variable      *Token_primary
		sql_datatype                rsql.Datatype_t
		sql_precision               uint16
		sql_scale                   uint16
		sql_fixlen_flag             bool

		token_primary_variable_clone *Token_primary
		token_stmt_assignment        *Token_stmt_ASSIGNMENT // assignment token

		token_chain_dummy Token // chain anchor: use only tok_next;
		token_last        Tokener
	)

	/* current lexeme must be DECLARE */

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_DECLARE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip DECLARE
		return nil, nil, rsql_err
	}

	token_last = &token_chain_dummy

	/* loop */

	for {
		/* create Token_primary for variable */

		if parser.Current_lexeme.Lex_type != lex.LEX_VARIABLE { // variable expected
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_VARIABLE_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}
		info_line := parser.Info_line

		if token_primary_variable, rsql_err = parser.Eat_variable(); rsql_err != nil {
			return nil, nil, rsql_err
		}

		/* check if it is a normal variable declaration, or a CURSOR, or a TABLE declaration */

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_AS {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip optional AS
				return nil, nil, rsql_err
			}
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_TABLE:
			stmt, rsql_err := parser.get_statement_CREATE_TABLE(OPT_VARIABLE_TABLE, token_primary_variable.Prim_name)
			if rsql_err != nil {
				return nil, nil, rsql_err
			}

			return stmt, stmt, nil

		case lex.LEX_KEYWORD_CURSOR:
			return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FEATURE_NOT_IMPLEMENTED_YET, rsql.ERROR_BATCH_ABORT)

		default: /* ====== normal variable declaration expected ====== */

			// === create Token_stmt_DECLARE_variable (declaration token for ONE variable)

			token_stmt_declare_variable = &Token_stmt_DECLARE_variable{}
			parser.token_init(&token_stmt_declare_variable.Token, TOK_STMT_DECLARE_VARIABLE, info_line)

			// === eat datatype, fill Token_stmt_DECLARE_variable, and chain it.

			if sql_datatype, sql_precision, sql_scale, sql_fixlen_flag, rsql_err = parser.Eat_builtin_datatype(EAT_DATATYPE_DECLARATION); rsql_err != nil {
				return nil, nil, rsql_err
			}

			token_primary_variable.Prim_sql_datatype = sql_datatype
			token_primary_variable.Prim_sql_precision = sql_precision
			token_primary_variable.Prim_sql_scale = sql_scale
			token_primary_variable.Prim_sql_fixlen_flag = sql_fixlen_flag

			token_stmt_declare_variable.Decl_variable = token_primary_variable

			token_chain(token_last, token_stmt_declare_variable)
			token_last = token_stmt_declare_variable

			// === COLLATE clause is forbidden.

			if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_COLLATE {
				return nil, nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLLATE_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
			}

			// === create Token_stmt_ASSIGNMENT, and chain it.

			token_stmt_assignment = nil

			token_primary_variable_clone = token_primary_variable.clone_shallow() // clone the token for assignment statement
			token_primary_variable_clone.Prim_sql_datatype = 0
			token_primary_variable_clone.Prim_sql_precision = 0
			token_primary_variable_clone.Prim_sql_scale = 0
			token_primary_variable_clone.Prim_sql_fixlen_flag = false
			token_primary_variable_clone.Prim_sql_collate = ""

			switch parser.Current_lexeme.Lex_subtype { // if initialization exists, create assignment token
			case lex.LEX_OPERATOR_COMP_EQUAL:
				if token_stmt_assignment, rsql_err = parser.Eat_ASSIGNMENT_operator_and_expression(token_primary_variable_clone); rsql_err != nil {
					return nil, nil, rsql_err
				}
			default:
				token_stmt_assignment = parser.create_ASSIGNMENT_NULL(token_primary_variable_clone)
			}

			token_chain(token_last, token_stmt_assignment)
			token_last = token_stmt_assignment

		} // end switch

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // if not comma, end of DECLARE statement
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, nil, rsql_err
		}

	} // end while

	/* return result. A sequence of at least one declare statement and one assignment statement is returned. */

	sequence_first = token_chain_dummy.Next() // always points to a valid statement token
	sequence_first.Set_prev(nil)

	sequence_last = token_last // always points to a valid statement token

	return sequence_first, sequence_last, nil
}

// Parse_all_AST parses all AST tree.
//
func (parser *Parser) Parse_all_AST() *rsql.Error {
	var (
		rsql_err      *rsql.Error
		program_chain Tokener
	)

	/* parse */

	if program_chain, _, rsql_err = parser.Eat_sequence_of_statements_up_to_LEX_END_OF_BATCH(); rsql_err != nil { // parse all statements of the batch into an ast tree
		rsql_err.Batch_line_no = parser.Info_line.No
		rsql_err.Batch_line_pos = uint32(parser.Info_line.Pos)
		return rsql_err
	}

	parser.Batch_ast_anchor = program_chain // first token of the AST is attached here

	assert(parser.Xtable_object_stack.Count() == 0)
	assert(parser.xtable_outer_object_stack.Count() == 1)
	assert(parser.while_stack.count() == 0)

	return nil
}
