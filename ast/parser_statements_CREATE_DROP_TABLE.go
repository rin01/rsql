package ast

import (
	"rsql"
	"rsql/dict"
	"rsql/lex"
	"rsql/lk"
)

// get_new_index_sysname creates a new index name, different from all index names in indexmap and base Tabledefs.
//
func (parser *Parser) get_new_index_sysname(gtabledef *rsql.GTabledef) lex.Lexeme {
	var (
		new_name_string string
	)

	new_name_string = dict.Get_new_index_sysname_string(gtabledef)

	new_name := lex.Create_lexeme(lex.LEX_IDENTPART, lex.LEX_IDENTPART_SUB, new_name_string)

	return new_name
}

type Option_kind_table_t uint8

const (
	OPT_PERMANENT_TABLE Option_kind_table_t = 1
	OPT_VARIABLE_TABLE  Option_kind_table_t = 2
	OPT_RESTORING_TABLE Option_kind_table_t = 3 // used by RESTORE, when creating GTabledef from backup file
)

// Get_gtabledef_from_create_table_script creates *GTabledef from a CREATE TABLE script.
// It is used by RESTORE DATABASE statement, to create tables.
//
//
func Get_gtabledef_from_create_table_script(database_name string, script []byte) (schema_name string, gtabledef *rsql.GTabledef, rsql_err *rsql.Error) {
	var (
		parser                          *Parser
		server_default_collation_string string
		options                         Options_t
		token_stmt_create_table         *Token_stmt_CREATE_TABLE
	)

	server_default_collation_string = "en_cs_as" // not used. In backup file, all VARCHAR columns have a COLLATE clause
	options = OPTION_ANSI_NULL_DFLT_ON           // not used. In backup file, NULL or NOT NULL clauses are always specified

	if parser, rsql_err = New_parser(server_default_collation_string, database_name, options, nil); rsql_err != nil {
		return "", nil, rsql_err
	}

	if rsql_err := parser.Attach_batch(script); rsql_err != nil {
		return "", nil, rsql_err
	}

	if rsql_err := parser.Eat_keyword(lex.LEX_KEYWORD_CREATE); rsql_err != nil { // eat CREATE
		return "", nil, rsql_err
	}

	if parser.Current_lexeme.Lex_subtype != lex.LEX_KEYWORD_TABLE { // current lexeme must be TABLE
		return "", nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CREATE_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	if token_stmt_create_table, rsql_err = parser.get_statement_CREATE_TABLE(OPT_RESTORING_TABLE, ""); rsql_err != nil {
		return "", nil, rsql_err
	}

	return token_stmt_create_table.Ct_schema_name, token_stmt_create_table.Ct_gtabledef, nil
}

// parse CREATE TABLE statement or DECLARE @a TABLE.
//
//    CREATE TABLE [ database_name . [ schema_name ] . | schema_name . ] table_name
//    ( {     <column_definition>
//        | [ <table_constraint> ]
//        | [ <table_index> ] [ ,...n ] }
//    )
//
//    <column_definition> ::=
//        column_name <data_type>
//        [ COLLATE collation_name ]
//        [ NULL | NOT NULL ]
//        [ IDENTITY [ ( seed,increment ) ]
//        [ [ CONSTRAINT constraint_name ] { { PRIMARY KEY | UNIQUE } [ CLUSTERED | NONCLUSTERED ] } ]
//        [ INDEX index_name [ CLUSTERED | NONCLUSTERED ] ]
//
//    < table_constraint > ::=
//        [ CONSTRAINT constraint_name ] { PRIMARY KEY | UNIQUE } [ CLUSTERED | NONCLUSTERED ] ( column [ ,...n ] )
//
//    < table_index > ::=
//        INDEX index_name [ CLUSTERED | NONCLUSTERED ] (column [ ,... n ] )
//
//    For variable table (DECLARE @a TABLE), GTabledef is put in parser.Temptable_bag.
//
func (parser *Parser) get_statement_CREATE_TABLE(option Option_kind_table_t, variable_table_name string) (*Token_stmt_CREATE_TABLE, *rsql.Error) {
	var (
		rsql_err                *rsql.Error
		qname                   rsql.Object_qname_t
		gtabledef               *rsql.GTabledef
		tabledef_base           *rsql.Tabledef
		coldef_rowid            *rsql.Coldef
		token_stmt_create_table *Token_stmt_CREATE_TABLE
		indexdef_PK             *rsql.Tabledef
		indexdef_clustered      *rsql.Tabledef
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TABLE)

	switch option {
	case OPT_PERMANENT_TABLE:
		if rsql_err := parser.Check_compatibility(COMPAT_CREATE_TABLE); rsql_err != nil { // check if statement is compatible with previous statements
			return nil, rsql_err
		}

	case OPT_VARIABLE_TABLE:
		if rsql_err := parser.Check_compatibility(COMPAT_CREATE_VARIABLE_TABLE); rsql_err != nil { // check if statement is compatible with previous statements
			return nil, rsql_err
		}

	case OPT_RESTORING_TABLE:
		// pass

	default:
		panic("impossible")
	}

	// create Token_stmt_CREATE_TABLE

	token_stmt_create_table = &Token_stmt_CREATE_TABLE{}
	parser.token_init(&token_stmt_create_table.Token, TOK_STMT_CREATE_TABLE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip TABLE
		return nil, rsql_err
	}

	gtabledef = rsql.New_GTabledef()
	tabledef_base = rsql.New_Tabledef(rsql.TD_TYPE_BASE_TABLE)
	gtabledef.Base = tabledef_base

	switch option {
	case OPT_PERMANENT_TABLE, OPT_RESTORING_TABLE:
		// eat table name

		if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
			return nil, rsql_err
		}

		tabledef_base.Td_durability = rsql.DURABILITY_PERMANENT
		if qname.Object_name[0] == '#' {
			tabledef_base.Td_durability = rsql.DURABILITY_TEMPORARY
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_TEMPORARY_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT, qname.Object_name)
		}

	case OPT_VARIABLE_TABLE:
		qname = rsql.Object_qname_t{Database_name: rsql.TRASHDB, Schema_name: "dbo", Object_name: variable_table_name, Object_name_original: variable_table_name}

		tabledef_base.Td_durability = rsql.DURABILITY_FLASH

	default:
		panic("impossible")
	}

	token_stmt_create_table.Ct_database_name = qname.Database_name
	token_stmt_create_table.Ct_schema_name = qname.Schema_name
	gtabledef.Gtable_name = qname.Object_name
	gtabledef.Gtable_name_original = qname.Object_name_original

	tabledef_base.Td_type = rsql.TD_TYPE_BASE_TABLE
	tabledef_base.Td_cluster_type = rsql.TD_CLUSTERED
	tabledef_base.Td_status = rsql.TD_STATUS_BEING_CREATED

	// expect left parenthese

	if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil {
		return nil, rsql_err
	}

	if parser.Current_lexeme.Lex_type == lex.LEX_RPAREN {
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_NO_COLUMN, rsql.ERROR_BATCH_ABORT, gtabledef.Gtable_name_original)
	}

	// put ROWID column as first column in table

	coldef_rowid = &rsql.Coldef{
		Cd_colname:            rsql.ROWID,
		Cd_colname_original:   rsql.ROWID,
		Cd_datatype:           rsql.DATATYPE_BIGINT,
		Cd_precision:          0,
		Cd_scale:              0,
		Cd_fixlen_flag:        false,
		Cd_collation_strength: 0,
		Cd_collation:          "",
		Cd_autonum:            rsql.CD_AUTONUM_ROWID,
		Cd_NOT_NULL_flag:      true,
		Cd_base_seqno:         0,
	}

	gtabledef.Colmap[rsql.ROWID] = coldef_rowid
	tabledef_base.Td_coldefs = append(tabledef_base.Td_coldefs, coldef_rowid)

	// eat list of Coldefs and constraints UNIQUE, PRIMARY KEY, and INDEX

	if rsql_err = parser.Eat_coldefs_and_constraints(gtabledef); rsql_err != nil { // eat all column definitions and table constraints, and put them into gtabledef
		return nil, rsql_err
	}

	// check that at most one PRIMARY KEY constraint exists

	for _, indexdef := range gtabledef.Indexmap {
		assert(indexdef.Td_type == rsql.TD_TYPE_INDEX_TABLE)

		if indexdef.Td_index_type == rsql.TD_PRIMARY_KEY {
			if indexdef_PK != nil {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_WITH_MANY_PRIMARY_KEYS, rsql.ERROR_BATCH_ABORT)
			}
			indexdef_PK = indexdef
		}
	}

	// check that at most one CLUSTERED specification for indexes exists

	for _, indexdef := range gtabledef.Indexmap {
		if indexdef.Td_cluster_type == rsql.TD_CLUSTERED {
			if indexdef_clustered != nil {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_WITH_MANY_CLUSTERED_INDEXES, rsql.ERROR_BATCH_ABORT)
			}
			indexdef_clustered = indexdef
		}
	}

	// if no CLUSTERED specification, PRIMARY KEY becomes the native key if TD_CLUSTER_TYPE_UNDEFINED is specified. Else, or if no primary key, rowid becomes the native key.

	if indexdef_clustered == nil {
		switch {
		case indexdef_PK != nil && indexdef_PK.Td_cluster_type == rsql.TD_CLUSTER_TYPE_UNDEFINED: // transform PK into TD_CLUSTERED
			indexdef_PK.Td_cluster_type = rsql.TD_CLUSTERED

			indexdef_clustered = indexdef_PK

		default: // create Tabledef for one-column rowid UNIQUE index

			indexdef_rowid := &rsql.Tabledef{}

			new_index_name := parser.get_new_index_sysname(gtabledef) // new generated name

			indexdef_rowid.Td_index_name = new_index_name.Lex_word
			indexdef_rowid.Td_index_name_original = new_index_name.Lex_word
			indexdef_rowid.Td_durability = tabledef_base.Td_durability
			indexdef_rowid.Td_type = rsql.TD_TYPE_INDEX_TABLE
			indexdef_rowid.Td_index_type = rsql.TD_UNIQUE
			indexdef_rowid.Td_cluster_type = rsql.TD_CLUSTERED
			indexdef_rowid.Td_status = rsql.TD_STATUS_BEING_CREATED

			assert(coldef_rowid != nil)
			indexdef_rowid.Td_coldefs = append(indexdef_rowid.Td_coldefs, coldef_rowid)
			indexdef_rowid.Td_nk = append(indexdef_rowid.Td_nk, coldef_rowid)

			if _, ok := gtabledef.Indexmap[indexdef_rowid.Td_index_name]; ok == true {
				panic("impossible")
			}
			gtabledef.Indexmap[indexdef_rowid.Td_index_name] = indexdef_rowid

			indexdef_clustered = indexdef_rowid
		}
	}

	// check primary key if exists

	if indexdef_PK != nil {
		if indexdef_PK.Td_cluster_type == rsql.TD_CLUSTER_TYPE_UNDEFINED {
			indexdef_PK.Td_cluster_type = rsql.TD_NONCLUSTERED
		}

		// check that columns of primary key have NOT NULL clause

		for _, coldef := range indexdef_PK.Td_coldefs {
			if coldef.Cd_NOT_NULL_flag == false {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_PRIMARY_KEY_COLUMNS_MUST_BE_NOT_NULL, rsql.ERROR_BATCH_ABORT, coldef.Cd_colname)
			}
		}
	}

	// here, exactly one index is TD_CLUSTERED, and it becomes the base table native key

	tabledef_base.Td_index_name = indexdef_clustered.Td_index_name // can be the PRIMARY KEY or UNIQUE constraint name, or INDEX name
	tabledef_base.Td_index_name_original = indexdef_clustered.Td_index_name_original
	tabledef_base.Td_index_type = indexdef_clustered.Td_index_type
	// tabledef_base.Td_coldefs =
	tabledef_base.Td_nk = append(tabledef_base.Td_nk[:0], indexdef_clustered.Td_nk...)

	assert(gtabledef.Indexmap[indexdef_clustered.Td_index_name] == indexdef_clustered) // delete clustered index from Indexmap
	delete(gtabledef.Indexmap, indexdef_clustered.Td_index_name)

	// expect right parenthese

	if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil {
		return nil, rsql_err
	}

	// append native key of base table (that is, clustered index) to each indexdef

	for _, indexdef := range gtabledef.Indexmap {
		assert(indexdef.Td_cluster_type == rsql.TD_NONCLUSTERED)

		for _, coldef_nk := range tabledef_base.Td_nk { // for all columns in native key of base table
			indexdef.Td_payload = append(indexdef.Td_payload, coldef_nk) // put it into payload

			if coldef_nk.Exists_in(indexdef.Td_coldefs) == false { // put it into coldefs list, only if not already exists
				indexdef.Td_coldefs = append(indexdef.Td_coldefs, coldef_nk)
			}
		}
	}

	// for permanent table only, skip useless ON <file_group> if any

	if option == OPT_PERMANENT_TABLE && parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_ON {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ON
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FILE_GROUP_OR_PRIMARY_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip skip <file_group>, which is often [PRIMARY]
			return nil, rsql_err
		}
	}

	// fill Token_stmt_create_table

	token_stmt_create_table.Ct_gtabledef = gtabledef

	// for permanent table only, put in Registration

	if option == OPT_PERMANENT_TABLE {
		parser.Registration.Register_object_qname(qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)
	}

	// for variable table only, fill in gtblid etc, and put in Temptable_bag

	if option == OPT_VARIABLE_TABLE {
		// fill in gtblid etc

		gtabledef.Gtblid = parser.Get_flashtable_new_tblid() // negative value
		gtabledef.Gdbid = dict.TRASHDB_DBID
		gtabledef.Gschid = dict.SCHID_DBO

		gtabledef.Base.Td_tblid = gtabledef.Gtblid
		gtabledef.Base.Td_dbid = dict.TRASHDB_DBID
		gtabledef.Base.Td_schid = dict.SCHID_DBO
		gtabledef.Base.Td_base_gtblid = gtabledef.Gtblid
		gtabledef.Base.Td_status = rsql.TD_STATUS_VALID

		for _, indexdef := range gtabledef.Indexmap {
			indexdef.Td_tblid = parser.Get_flashtable_new_tblid() // negative value
			indexdef.Td_dbid = dict.TRASHDB_DBID
			indexdef.Td_schid = dict.SCHID_DBO
			indexdef.Td_base_gtblid = gtabledef.Gtblid
			indexdef.Td_status = rsql.TD_STATUS_VALID
		}

		// put in Temptable_bag

		if rsql_err = parser.Temptable_bag.Put(qname.Object_name, gtabledef); rsql_err != nil {
			return nil, rsql_err
		}
	}

	return token_stmt_create_table, nil
}

// eat all column definitions and table constraints of a CREATE TABLE, and put them in gtabledef.
//
func (parser *Parser) Eat_coldefs_and_constraints(gtabledef *rsql.GTabledef) *rsql.Error {
	var (
		rsql_err           *rsql.Error
		coldef             *rsql.Coldef
		coldef_count       uint16
		identity_seed      int64
		identity_increment int64
		indexdef           *rsql.Tabledef
	)

	// loop through all columns

	coldef_count = uint16(len(gtabledef.Base.Td_coldefs)) // "rowid" has already been inserted as first column
	assert(coldef_count == 1)

	for {
		indexdef = nil
		coldef = nil

		if parser.Current_lexeme.Lex_type == lex.LEX_IDENTPART { // eat column
			if coldef_count >= rsql.SPEC_TABLE_NUMBER_OF_COLUMNS_MAX {
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_COLUMNS_IN_TABLE, rsql.ERROR_BATCH_ABORT, gtabledef.Gtable_name_original)
			}

			if coldef, indexdef, identity_seed, identity_increment, rsql_err = parser.Eat_coldef_and_column_constraints(gtabledef.Base.Td_durability, gtabledef.Colmap); rsql_err != nil { // eat column definition, and all its constraints
				return rsql_err
			}

			coldef.Cd_base_seqno = coldef_count

			if coldef.Cd_autonum == rsql.CD_AUTONUM_IDENTITY {
				if gtabledef.Identity_increment != 0 { // check that no other IDENTITY column exists
					return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_IDENTITY_IN_EXCESS, rsql.ERROR_BATCH_ABORT, coldef.Cd_colname_original)
				}

				assert(coldef.Cd_NOT_NULL_flag == true)

				gtabledef.Identity_seed = identity_seed
				gtabledef.Identity_increment = identity_increment
			}

			// insert new coldef in gtabledef.Colmap and gtabledef.Base.Td_coldefs

			if _, ok := gtabledef.Colmap[coldef.Cd_colname]; ok == true {
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_COLUMN_NAME_FOUND, rsql.ERROR_BATCH_ABORT, coldef.Cd_colname_original)
			}
			gtabledef.Colmap[coldef.Cd_colname] = coldef

			gtabledef.Base.Td_coldefs = append(gtabledef.Base.Td_coldefs, coldef)

			coldef_count++

		} else { // eat table constraint or index
			if indexdef, rsql_err = parser.Eat_table_constraint_or_index(gtabledef.Base.Td_durability, gtabledef.Colmap); rsql_err != nil {
				return rsql_err
			}
		}

		// insert new indexdef in list if any

		if indexdef != nil {
			if len(gtabledef.Indexmap) >= rsql.SPEC_TABLE_NUMBER_OF_INDEXES_MAX {
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_INDEXES, rsql.ERROR_BATCH_ABORT, gtabledef.Gtable_name_original)
			}

			if indexdef.Td_index_name == "" { // PRIMARY KEY or UNIQUE index may have no name, if no constraint name have been given
				new_index_name := parser.get_new_index_sysname(gtabledef) // new generated name
				indexdef.Td_index_name = new_index_name.Lex_word
				indexdef.Td_index_name_original = new_index_name.Lex_word
			}

			if _, ok := gtabledef.Indexmap[indexdef.Td_index_name]; ok == true {
				return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DUPLICATE_INDEX_NAME_FOUND, rsql.ERROR_BATCH_ABORT, indexdef.Td_index_name_original)
			}
			gtabledef.Indexmap[indexdef.Td_index_name] = indexdef
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA { // === if not comma, exit the loop
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return rsql_err
		}

		if parser.Current_lexeme.Lex_type == lex.LEX_RPAREN { // === if right parenthese, exit the loop
			break
		}

	} // end loop

	// table must contain at least one valid column (not counting the ROWID column)

	if coldef_count <= 1 {
		return rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TABLE_NO_COLUMN, rsql.ERROR_BATCH_ABORT, gtabledef.Gtable_name_original)
	}

	return nil
}

// Eat_coldef_and_column_constraints eats column definition and column constraints (NULL, NOT NULL, CHECK, PRIMARY KEY, UNIQUE, iNDEX)
// Note: INDEX clause is in fact not a constraint, but the semantic point is moot.
//
// It always returns a Coldef. If a UNIQUE, PRIMARY KEY, INDEX clause exists, a Tabledef is also returned.
//
//    Returned Tabledef can have Td_cluster_type==TD_CLUSTER_TYPE_UNDEFINED only for PRIMARY KEY.
//    For INDEX, "rowid" is appended to the key list, so that the native key of the index is unique. Native keys of base tables or index tables are always unique.
//
//
//    <column_definition> ::=
//    column_name <data_type>
//        [ COLLATE collation_name ]
//        [ NULL | NOT NULL ]
//        [ IDENTITY [ ( seed,increment ) ]
//
//        [ [ CONSTRAINT constraint_name ] { { PRIMARY KEY | UNIQUE } [ CLUSTERED | NONCLUSTERED ] } ]
//
//        [ INDEX index_name [ CLUSTERED | NONCLUSTERED ] ]
//
//
// We don't care about constraint name, except for PRIMARY KEY and UNIQUE, for which it becomes the index name. INDEX clause has always a name.
//
//      IMPORTANT: the returned *rsql.Tabledef can have no indexdef.Td_index_name and indexdef.Td_index_name_original.
//                 In this case, it is the responsability of the caller to assign proper names to these fields.
//
func (parser *Parser) Eat_coldef_and_column_constraints(durability rsql.Durability_t, colmap map[string]*rsql.Coldef) (coldef *rsql.Coldef, indexdef *rsql.Tabledef, identity_seed int64, identity_increment int64, rsql_err *rsql.Error) {
	var (
		colname          lex.Lexeme
		colname_original lex.Lexeme
		sql_datatype     rsql.Datatype_t
		sql_precision    uint16
		sql_scale        uint16
		sql_fixlen_flag  bool

		index_name                   string
		index_name_original          string
		eat_NULL_clause_flag         bool
		eat_CLUSTERED_clause_flag    bool
		default_column_NOT_NULL_flag bool
		index_type                   rsql.Td_index_type_t
		index_cluster_type           rsql.Td_cluster_type_t
		coldef_rowid                 *rsql.Coldef
	)

	//===== eat column information and create Coldef =====

	if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // expect column name
		return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_EXPECTED, rsql.ERROR_BATCH_ABORT)
	}

	colname = parser.Current_lexeme
	colname_original = parser.Current_lexeme_original // original cased column name

	if colname.Lex_word == rsql.ROWID {
		return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_ROWID_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
	}

	if _, ok := G_SYSFUNC_DESCRIPTION_MAP[colname.Lex_word]; ok { // column name cannot be function name, like MS SQL Server, because in expression, e.g. year will be interpreted as a function
		return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLNAME_CANNOT_BE_FUNCNAME, rsql.ERROR_BATCH_ABORT, colname.Lex_word)
	}

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip column name
		return nil, nil, 0, 0, rsql_err
	}

	if sql_datatype, sql_precision, sql_scale, sql_fixlen_flag, rsql_err = parser.Eat_builtin_datatype(EAT_DATATYPE_DECLARATION); rsql_err != nil { // eat datatype. Returns error if not a builtin datatype
		return nil, nil, 0, 0, rsql_err
	}

	default_column_NOT_NULL_flag = true
	if parser.Options&OPTION_ANSI_NULL_DFLT_ON != 0 { // allows NULL
		default_column_NOT_NULL_flag = false
	}

	coldef = &rsql.Coldef{
		Cd_colname:            colname.Lex_word,
		Cd_colname_original:   colname_original.Lex_word,
		Cd_datatype:           sql_datatype,
		Cd_precision:          sql_precision,
		Cd_scale:              sql_scale,
		Cd_fixlen_flag:        sql_fixlen_flag,
		Cd_collation_strength: 0,  // for VARCHAR, always rsql.COLLSTRENGTH_1
		Cd_collation:          "", // for VARCHAR, never ""
		Cd_autonum:            rsql.CD_AUTONUM_NONE,
		Cd_NOT_NULL_flag:      default_column_NOT_NULL_flag,
		Cd_base_seqno:         0,
	}

	if coldef.Cd_datatype == rsql.DATATYPE_VARCHAR { // default collation for varchar
		coldef.Cd_collation_strength = rsql.COLLSTRENGTH_1
		coldef.Cd_collation = parser.Server_default_collation
	}

	// eat COLLATE clause if any

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_COLLATE {
		var collation string

		if coldef.Cd_datatype != rsql.DATATYPE_VARCHAR {
			return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_COLLATE_FORBIDDEN, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
		}

		if collation, rsql_err = parser.Eat_COLLATE(); rsql_err != nil {
			return nil, nil, 0, 0, rsql_err
		}

		coldef.Cd_collation = collation
	}

	//===== eat other column specification or constraint, and modify coldef as appropriate =====

LABEL_LOOP:
	for { // eat all column constraints, if any

		//=== eat CONSTRAINT constraint_name if any. Note: we don't care about constraint names and discard them, except when they are index name (for PRIMARY KEY and UNIQUE indexes) ===
		var (
			constraint_name          string
			constraint_name_original string
		)

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_CONSTRAINT {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CONSTRAINT
				return nil, nil, 0, 0, rsql_err
			}

			if constraint_name, constraint_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat constraint name
				return nil, nil, 0, 0, rsql_err
			}
		}

		switch {
		//=== eat IDENTITY clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_IDENTITY:
			if constraint_name != "" {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_IDENTITY_CONSTRAINT_NAME_FOBIDDEN, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			if identity_increment != 0 {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_IDENTITY_IN_EXCESS, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			if !(sql_datatype == rsql.DATATYPE_TINYINT || sql_datatype == rsql.DATATYPE_SMALLINT || sql_datatype == rsql.DATATYPE_INT || sql_datatype == rsql.DATATYPE_BIGINT) {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_IDENTITY_FORBIDDEN, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			coldef.Cd_autonum = rsql.CD_AUTONUM_IDENTITY

			identity_seed = 1      // default
			identity_increment = 1 // default

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip IDENTITY
				return nil, nil, 0, 0, rsql_err
			}

			if parser.Current_lexeme.Lex_type == lex.LEX_LPAREN { // if lparen
				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip lparen
					return nil, nil, 0, 0, rsql_err
				}

				if !(parser.Current_lexeme.Lex_subtype == lex.LEX_LITERAL_NUMBER_INTEGRAL || parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_MINUS) {
					return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_IDENTITY_BAD_SEED, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
				}

				if identity_seed, rsql_err = parser.Eat_literal_integral_int64(); rsql_err != nil { // eat seed
					return nil, nil, 0, 0, rsql_err
				}

				if rsql_err = parser.Eat_punctuation(lex.LEX_COMMA); rsql_err != nil { // expect comma
					return nil, nil, 0, 0, rsql_err
				}

				if identity_increment, rsql_err = parser.Eat_literal_integral_int64(); rsql_err != nil { // eat increment
					return nil, nil, 0, 0, rsql_err
				}

				if identity_increment == 0 {
					return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_IDENTITY_BAD_INCREMENT_ZERO, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
				}

				if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil { // expect rparen
					return nil, nil, 0, 0, rsql_err
				}
			}

		//=== eat NULL clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_NULL:
			// constraint name is allowed by SQL Server but we ignore it

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NULL
				return nil, nil, 0, 0, rsql_err
			}

			if eat_NULL_clause_flag == true {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_NULL_CLAUSE_IN_EXCESS, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			eat_NULL_clause_flag = true
			coldef.Cd_NOT_NULL_flag = false

		//=== eat NOT NULL clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_OPERATOR_LOGICAL_NOT:
			// constraint name is allowed by SQL Server but we ignore it

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NOT
				return nil, nil, 0, 0, rsql_err
			}

			if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_NULL); rsql_err != nil { // expect NULL
				return nil, nil, 0, 0, rsql_err
			}

			if eat_NULL_clause_flag == true {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_NULL_CLAUSE_IN_EXCESS, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			eat_NULL_clause_flag = true
			coldef.Cd_NOT_NULL_flag = true

		//=== eat PRIMARY KEY clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_PRIMARY:
			if index_type != 0 {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_INDEXES_IN_EXCESS, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip PRIMARY
				return nil, nil, 0, 0, rsql_err
			}

			if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_KEY); rsql_err != nil { // expect KEY
				return nil, nil, 0, 0, rsql_err
			}

			index_name = constraint_name
			index_name_original = constraint_name_original

			index_type = rsql.TD_PRIMARY_KEY
			index_cluster_type = rsql.TD_CLUSTER_TYPE_UNDEFINED // default
			eat_CLUSTERED_clause_flag = true

		//=== eat UNIQUE clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_UNIQUE:
			if index_type != 0 {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_INDEXES_IN_EXCESS, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip UNIQUE
				return nil, nil, 0, 0, rsql_err
			}

			index_name = constraint_name
			index_name_original = constraint_name_original

			index_type = rsql.TD_UNIQUE
			index_cluster_type = rsql.TD_NONCLUSTERED // default
			eat_CLUSTERED_clause_flag = true

		//=== eat INDEX clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INDEX:
			if constraint_name != "" {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_INDEX_CONSTRAINT_NAME_FOBIDDEN, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			if index_type != 0 {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_INDEXES_IN_EXCESS, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INDEX
				return nil, nil, 0, 0, rsql_err
			}

			if index_name, index_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat index name
				return nil, nil, 0, 0, rsql_err
			}

			index_type = rsql.TD_INDEX
			index_cluster_type = rsql.TD_NONCLUSTERED // default
			eat_CLUSTERED_clause_flag = true

		//=== eat CHECK clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_CHECK:
			return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CHECK_CONSTRAINT_NOT_SUPPORTED, rsql.ERROR_BATCH_ABORT)

		//=== eat DEFAULT clause ===

		case parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_DEFAULT:
			if constraint_name != "" {
				return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_DEFAULT_CONSTRAINT_NAME_FOBIDDEN, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
			}

			return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_DEFAULT_CONSTRAINT_NOT_SUPPORTED, rsql.ERROR_BATCH_ABORT)

		//=== if constraint name, but unknown constraint, return error ===

		case constraint_name != "": // if a constraint name has been read, but unknown constraint clause
			return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_UNKNOWN_CONSTRAINT, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)

		//=== if comma or closing parenthese, no more constraint to be read for this column ===

		case parser.Current_lexeme.Lex_type&(lex.LEX_COMMA|lex.LEX_RPAREN) != 0:
			break LABEL_LOOP //====> exit loop

		//=== else, error ===

		default:
			return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_UNKNOWN_CONSTRAINT, rsql.ERROR_BATCH_ABORT, colname_original.Lex_word)
		}

		// for PRIMARY KEY, UNIQUE, or INDEX, eat optional CLUSTERED or NONCLUSTERED clause

		if eat_CLUSTERED_clause_flag == true {
			assert(index_type != 0)

			switch parser.Current_lexeme.Lex_subtype {
			case lex.LEX_KEYWORD_CLUSTERED:
				index_cluster_type = rsql.TD_CLUSTERED

				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CLUSTERED
					return nil, nil, 0, 0, rsql_err
				}

			case lex.LEX_KEYWORD_NONCLUSTERED:
				index_cluster_type = rsql.TD_NONCLUSTERED

				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NONCLUSTERED
					return nil, nil, 0, 0, rsql_err
				}
			}

			eat_CLUSTERED_clause_flag = false
		}

	} // end loop

	//=== ensure that IDENTITY column is NOT NULL ===

	if coldef.Cd_autonum != 0 {
		if coldef.Cd_NOT_NULL_flag == false {
			return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COL_IDENTITY_NOT_NULL_EXPECTED, rsql.ERROR_BATCH_ABORT, coldef.Cd_colname)
		}
	}

	//== ensure that PRIMARY KEY column is NOT NULL ===

	if index_type == rsql.TD_PRIMARY_KEY {
		if coldef.Cd_NOT_NULL_flag == false {
			return nil, nil, 0, 0, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_PRIMARY_KEY_COLUMNS_MUST_BE_NOT_NULL, rsql.ERROR_BATCH_ABORT, coldef.Cd_colname)
		}
	}

	//=== for PRIMARY KEY, UNIQUE, or INDEX, create indexdef ===

	if index_type != 0 {
		// create Tabledef for one-column index

		indexdef = &rsql.Tabledef{}

		if index_name != "" { // if no constraint name have been provided for PRIMARY KEY or UNIQUE, indexdef.Td_index_name remains empty string
			indexdef.Td_index_name = index_name
			indexdef.Td_index_name_original = index_name_original
		}
		indexdef.Td_durability = durability
		indexdef.Td_type = rsql.TD_TYPE_INDEX_TABLE
		indexdef.Td_index_type = index_type
		indexdef.Td_cluster_type = index_cluster_type
		indexdef.Td_status = rsql.TD_STATUS_BEING_CREATED

		indexdef.Td_coldefs = append(indexdef.Td_coldefs, coldef)
		indexdef.Td_nk = append(indexdef.Td_nk, coldef)
		if index_type == rsql.TD_INDEX { // append ROWID to INDEX column list, because we want all clustered or nonclustered indexes to be unique
			coldef_rowid = colmap[rsql.ROWID]
			indexdef.Td_coldefs = append(indexdef.Td_coldefs, coldef_rowid)
			indexdef.Td_nk = append(indexdef.Td_nk, coldef_rowid)
		}
	}

	return coldef, indexdef, identity_seed, identity_increment, nil
}

// eat table constraint (CHECK, PRIMARY KEY, UNIQUE, INDEX)
//
//    Returned Tabledef can have Td_cluster_type==TD_CLUSTER_TYPE_UNDEFINED only for PRIMARY KEY.
//    For INDEX, "rowid" is appended to the key list, so that the native key of the index is unique. Native keys of base tables or index tables are always unique.
//
//
//      < table_constraint > ::=
//      [ CONSTRAINT constraint_name ] { PRIMARY KEY | UNIQUE } [ CLUSTERED | NONCLUSTERED ] ( column [ ,...n ] )
//
//      < table_index > ::=
//      INDEX index_name [ CLUSTERED | NONCLUSTERED ] (column [ ,... n ] )
//
//
// We don't care about constraint name, except for PRIMARY KEY and UNIQUE, for which it becomes the index name. INDEX clause has always a name.
//
//      IMPORTANT: the returned *rsql.Tabledef can have no indexdef.Td_index_name and indexdef.Td_index_name_original.
//                 In this case, it is the responsability of the caller to assign proper names to these fields.
//
func (parser *Parser) Eat_table_constraint_or_index(durability rsql.Durability_t, colmap map[string]*rsql.Coldef) (*rsql.Tabledef, *rsql.Error) {
	var (
		rsql_err                 *rsql.Error
		ok                       bool
		coldef                   *rsql.Coldef
		coldef_rowid             *rsql.Coldef
		constraint_name          string
		constraint_name_original string
		index_name               string
		index_name_original      string
		indexdef                 *rsql.Tabledef
		index_type               rsql.Td_index_type_t
		index_cluster_type       rsql.Td_cluster_type_t
		keylist                  []*rsql.Coldef
	)

	// eat optional constraint name

	constraint_name = ""
	constraint_name_original = ""

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_CONSTRAINT {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CONSTRAINT
			return nil, rsql_err
		}

		if constraint_name, constraint_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat constraint name
			return nil, rsql_err
		}
	}

	// eat PRIMARY KEY, UNIQUE, INDEX, CHECK

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_PRIMARY:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip PRIMARY
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_KEY); rsql_err != nil { // expect KEY
			return nil, rsql_err
		}

		index_name = constraint_name
		index_name_original = constraint_name_original

		index_type = rsql.TD_PRIMARY_KEY
		index_cluster_type = rsql.TD_CLUSTER_TYPE_UNDEFINED

	case lex.LEX_KEYWORD_UNIQUE:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip UNIQUE
			return nil, rsql_err
		}

		index_name = constraint_name
		index_name_original = constraint_name_original

		index_type = rsql.TD_UNIQUE
		index_cluster_type = rsql.TD_NONCLUSTERED

	case lex.LEX_KEYWORD_INDEX:
		if constraint_name != "" {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TBL_INDEX_CONSTRAINT_NAME_FOBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INDEX
			return nil, rsql_err
		}

		if index_name, index_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat index name
			return nil, rsql_err
		}

		index_type = rsql.TD_INDEX
		index_cluster_type = rsql.TD_NONCLUSTERED

	case lex.LEX_KEYWORD_CHECK:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CHECK_CONSTRAINT_NOT_SUPPORTED, rsql.ERROR_BATCH_ABORT)

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TBL_UNKNOWN_CONSTRAINT, rsql.ERROR_BATCH_ABORT)
	}

	// for PRIMARY KEY, UNIQUE, or INDEX, eat CLUSTERED or NONCLUSTERED, and list of columns

	if index_type != 0 {
		indexdef = &rsql.Tabledef{}

		if index_name != "" { // if no constraint name have been provided for PRIMARY KEY or UNIQUE, indexdef.Td_index_name remains empty string
			indexdef.Td_index_name = index_name
			indexdef.Td_index_name_original = index_name_original
		}
		indexdef.Td_durability = durability
		indexdef.Td_type = rsql.TD_TYPE_INDEX_TABLE
		indexdef.Td_index_type = index_type
		indexdef.Td_cluster_type = index_cluster_type
		indexdef.Td_status = rsql.TD_STATUS_BEING_CREATED

		// eat CLUSTERED or NONCLUSTERED if any

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_CLUSTERED:
			indexdef.Td_cluster_type = rsql.TD_CLUSTERED

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CLUSTERED
				return nil, rsql_err
			}

		case lex.LEX_KEYWORD_NONCLUSTERED:
			indexdef.Td_cluster_type = rsql.TD_NONCLUSTERED

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NONCLUSTERED
				return nil, rsql_err
			}
		}

		// eat column list. Expect left parenthese

		if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil {
			return nil, rsql_err
		}

		// eat list of columns

		keylist = make([]*rsql.Coldef, 0, SPEC_TABLE_INDEX_KEYLIST_DEFAULT_SIZE)

		for {
			if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // === expect column name
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

			if len(keylist) >= rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_INDEX_COLUMNS, rsql.ERROR_BATCH_ABORT)
			}

			if parser.Current_lexeme.Lex_word == rsql.ROWID {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INDEX_ROWID_KEY_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
			}

			if coldef, ok = colmap[parser.Current_lexeme.Lex_word]; ok == false {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INDEX_KEY_NOT_FOUND, rsql.ERROR_BATCH_ABORT, parser.Current_lexeme.Lex_word)
			}

			for coldef.Exists_in(keylist) { // check for duplicate column name in index definition
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INDEX_DUPLICATE_KEY, rsql.ERROR_BATCH_ABORT, parser.Current_lexeme.Lex_word)
			}

			keylist = append(keylist, coldef)

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip column name
				return nil, rsql_err
			}

			switch parser.Current_lexeme.Lex_subtype { // optional ASC or DESC
			case lex.LEX_KEYWORD_ASC: // optional ASC
				if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ASC
					return nil, rsql_err
				}
			case lex.LEX_KEYWORD_DESC: // optional DESC not implemented
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_KEY_DESC_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
			}

			if parser.Current_lexeme.Lex_type != lex.LEX_COMMA {
				break
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
				return nil, rsql_err
			}
		}

		// append keylist into indexdef

		if index_type == rsql.TD_INDEX { // append ROWID to INDEX column list, because we want all indexes to be unique
			coldef_rowid = colmap[rsql.ROWID]
			assert(coldef_rowid.Exists_in(keylist) == false)
			keylist = append(keylist, coldef_rowid)
		}

		indexdef.Td_coldefs = append(indexdef.Td_coldefs[:0], keylist...)
		indexdef.Td_nk = append(indexdef.Td_nk[:0], keylist...)

		// expect right parenthese

		if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil {
			return nil, rsql_err
		}

		// skip useless ON <file_group> if any

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_ON {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ON
				return nil, rsql_err
			}

			if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FILE_GROUP_OR_PRIMARY_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip <file_group>, which is often [PRIMARY]
				return nil, rsql_err
			}
		}
	}

	return indexdef, nil
}

func (parser *Parser) get_statement_DROP_TABLE() (*Token_stmt_DROP_TABLE, *rsql.Error) {
	var (
		rsql_err              *rsql.Error
		qname                 rsql.Object_qname_t
		token_stmt_drop_table *Token_stmt_DROP_TABLE
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TABLE)

	if rsql_err := parser.Check_compatibility(COMPAT_DROP_TABLE); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_DROP_TABLE

	token_stmt_drop_table = &Token_stmt_DROP_TABLE{}
	parser.token_init(&token_stmt_drop_table.Token, TOK_STMT_DROP_TABLE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip TABLE
		return nil, rsql_err
	}

	// eat table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_drop_table.Dt_table_qname = qname

	// put in Registration

	parser.Registration.Register_object_qname(qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

	return token_stmt_drop_table, nil
}

// parse CREATE INDEX statement.
//
//        CREATE [ UNIQUE ] [ CLUSTERED | NONCLUSTERED ] INDEX index_name
//            ON [ database_name. [ schema_name ] . | schema_name. ] table_name ( column [ ,...n ] )
//            [ ON <filegroup> ]
//
func (parser *Parser) get_statement_CREATE_INDEX(index_type rsql.Td_index_type_t, cluster_type rsql.Td_cluster_type_t) (*Token_stmt_CREATE_INDEX, *rsql.Error) {
	var (
		rsql_err                *rsql.Error
		token_stmt_create_index *Token_stmt_CREATE_INDEX
		qname                   rsql.Object_qname_t
		index_name              string
		index_name_original     string
		colname_list            []string
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INDEX)

	if rsql_err := parser.Check_compatibility(COMPAT_CREATE_INDEX); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_CREATE_INDEX

	token_stmt_create_index = &Token_stmt_CREATE_INDEX{}
	parser.token_init(&token_stmt_create_index.Token, TOK_STMT_CREATE_INDEX)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INDEX
		return nil, rsql_err
	}

	token_stmt_create_index.Ci_index_type = index_type
	token_stmt_create_index.Ci_cluster_type = cluster_type

	// eat INDEX name

	if index_name, index_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat index name
		return nil, rsql_err
	}

	token_stmt_create_index.Ci_index_name = index_name
	token_stmt_create_index.Ci_index_name_original = index_name_original

	// eat ON

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_ON); rsql_err != nil {
		return nil, rsql_err
	}

	// eat unqualified or qualified table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_create_index.Ci_table_qname = qname

	// eat column list

	if colname_list, rsql_err = parser.Eat_list_of_colnames_for_index(); rsql_err != nil {
		return nil, rsql_err
	}

	if token_stmt_create_index.Ci_index_type == rsql.TD_INDEX { // for TD_INDEX index, "rowid" is always appended to column list, because all indexes must be unique
		colname_list = append(colname_list, rsql.ROWID)
	}

	token_stmt_create_index.Ci_colname_list = colname_list

	// skip useless ON <file_group> if any

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_ON {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ON
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FILE_GROUP_OR_PRIMARY_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip <file_group>, which is often [PRIMARY]
			return nil, rsql_err
		}
	}

	// put in Registration

	parser.Registration.Register_object_qname(token_stmt_create_index.Ci_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

	return token_stmt_create_index, nil
}

func (parser *Parser) Eat_list_of_colnames_for_index() ([]string, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		keylist  []string
		colname  string
	)

	// eat column list. Expect left parenthese

	if rsql_err = parser.Eat_punctuation(lex.LEX_LPAREN); rsql_err != nil {
		return nil, rsql_err
	}

	// eat list of columns

	keylist = make([]string, 0, SPEC_TABLE_INDEX_KEYLIST_DEFAULT_SIZE)

	for {
		if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART { // === expect column name
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_COLUMN_NAME_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}

		if len(keylist) >= rsql.SPEC_TABLE_NUMBER_OF_INDEX_COLUMN_MAX { // note: '=' is because for TD_INDEX, a rowid column will be appended
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TOO_MANY_INDEX_COLUMNS, rsql.ERROR_BATCH_ABORT)
		}

		if parser.Current_lexeme.Lex_word == rsql.ROWID {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INDEX_ROWID_KEY_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		colname = parser.Current_lexeme.Lex_word // already in lowercase, as it is an identifier

		for _, cn := range keylist { // check for duplicate column name in list
			if cn == colname {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_INDEX_DUPLICATE_KEY, rsql.ERROR_BATCH_ABORT, colname)
			}
		}

		keylist = append(keylist, colname)

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip column name
			return nil, rsql_err
		}

		switch parser.Current_lexeme.Lex_subtype { // optional ASC or DESC
		case lex.LEX_KEYWORD_ASC: // optional ASC
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ASC
				return nil, rsql_err
			}
		case lex.LEX_KEYWORD_DESC: // optional DESC not implemented
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_KEY_DESC_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
		}

		if parser.Current_lexeme.Lex_type != lex.LEX_COMMA {
			break
		}

		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip comma
			return nil, rsql_err
		}
	}

	// expect right parenthese

	if rsql_err = parser.Eat_punctuation(lex.LEX_RPAREN); rsql_err != nil {
		return nil, rsql_err
	}

	return keylist, nil
}

// parse DROP INDEX statement.
//
//      DROP INDEX
//          index_name ON [ database_name. [ schema_name ] . | schema_name. ] table_name
//
func (parser *Parser) get_statement_DROP_INDEX() (*Token_stmt_DROP_INDEX, *rsql.Error) {
	var (
		rsql_err              *rsql.Error
		token_stmt_drop_index *Token_stmt_DROP_INDEX
		qname                 rsql.Object_qname_t
		index_name            string
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INDEX)

	if rsql_err := parser.Check_compatibility(COMPAT_DROP_INDEX); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_DROP_INDEX

	token_stmt_drop_index = &Token_stmt_DROP_INDEX{}
	parser.token_init(&token_stmt_drop_index.Token, TOK_STMT_DROP_INDEX)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INDEX
		return nil, rsql_err
	}

	// eat INDEX name

	if index_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat index name
		return nil, rsql_err
	}

	token_stmt_drop_index.Di_index_name = index_name

	// eat ON

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_ON); rsql_err != nil {
		return nil, rsql_err
	}

	// eat unqualified or qualified table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_drop_index.Di_table_qname = qname

	// put in Registration

	parser.Registration.Register_object_qname(token_stmt_drop_index.Di_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

	return token_stmt_drop_index, nil
}

// parse ALTER INDEX statement.
//
//       ALTER INDEX index_name ON [ database_name. [ schema_name ] . | schema_name. ] table_name
//            WITH NAME = new_index_name
//
//       IMPORTANT: this statement IS NOT COMPATIBLE WITH MS SQL SERVER, but can be used with our database server.
//
func (parser *Parser) get_statement_ALTER_INDEX() (*Token_stmt_ALTER_INDEX, *rsql.Error) {
	var (
		rsql_err                   *rsql.Error
		token_stmt_alter_index     *Token_stmt_ALTER_INDEX
		qname                      rsql.Object_qname_t
		index_name                 string
		change_index_name          string
		change_index_name_original string
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_INDEX)

	if rsql_err := parser.Check_compatibility(COMPAT_ALTER_INDEX); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_ALTER_INDEX

	token_stmt_alter_index = &Token_stmt_ALTER_INDEX{}
	parser.token_init(&token_stmt_alter_index.Token, TOK_STMT_ALTER_INDEX)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip INDEX
		return nil, rsql_err
	}

	// eat INDEX name

	if index_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat index name
		return nil, rsql_err
	}

	token_stmt_alter_index.Ai_index_name = index_name

	// eat ON

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_ON); rsql_err != nil {
		return nil, rsql_err
	}

	// eat unqualified or qualified table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_alter_index.Ai_table_qname = qname

	// eat WITH NAME = new_index_name

	if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_WITH); rsql_err != nil { // skip WITH
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_auxword(lex.AUXWORD_NAME); rsql_err != nil { // skip NAME
		return nil, rsql_err
	}

	if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
		return nil, rsql_err
	}

	if change_index_name, change_index_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat new index name
		return nil, rsql_err
	}

	token_stmt_alter_index.Ai_change_index_name = change_index_name
	token_stmt_alter_index.Ai_change_index_name_original = change_index_name_original

	// put in Registration

	parser.Registration.Register_object_qname(token_stmt_alter_index.Ai_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

	return token_stmt_alter_index, nil
}

// parse ALTER TABLE statement.
//
//      ALTER TABLE [ database_name . [ schema_name ] . | schema_name . ] table_name  WITH NAME = new_table_name
//
//      ALTER TABLE [ database_name . [ schema_name ] . | schema_name . ] table_name [ WITH NOCHECK ]
//      {
//            ADD <table_constraint>
//
//          | DROP [ CONSTRAINT ] constraint_name
//
//          | ALTER COLUMN column_name
//                {
//                   WITH NAME = new_column_name
//                   | { NULL | NOT NULL }
//                }
//
//      }
//
//      <table_constraint> ::=
//            [ CONSTRAINT constraint_name ]
//            {
//                { PRIMARY KEY | UNIQUE }
//                    [ CLUSTERED | NONCLUSTERED ]
//                    (column [ ,...n ] )
//                    [ ON <filegroup> ]
//            }
//
//
// You cannot create a clustered index if a clustered index has already been defined on the table.
// Else, you must drop the table yourself, and create a new table.
//     - Use SHOW SQL TABLE mytable to get the table definition.
//     - BULK EXPORT mytable TO 'myfile.txt' -- use FIELDTERMINATOR or ROWTERMINATOR option if your varchar fields contain \t or \n
//     - DROP mytable
//     - CREATE TABLE mytable with a new definition.
//     - BULK EXPORT mytable FROM 'myfile.txt'
//
//
//      IMPORTANT: the following syntaxes ARE NOT COMPATIBLE WITH MS SQL SERVER, but can be used with our database server:
//
//          - WITH NAME = new_table_name                          to change the table name
//
//          - ALTER COLUMN column_name
//                {
//                   WITH NAME = new_column_name                  to change the column name
//                   | { NULL | NOT NULL }                        to change the column NULL or NOT NULL clause
//                }
//
//
// This function can return *Token_stmt_ALTER_TABLE, *Token_stmt_CREATE_INDEX or *Token_stmt_DROP_INDEX.
//
func (parser *Parser) get_statement_ALTER_TABLE() (Tokener, *rsql.Error) {
	var (
		rsql_err                    *rsql.Error
		token_stmt_alter_table      *Token_stmt_ALTER_TABLE
		qname                       rsql.Object_qname_t
		constraint_name             string
		constraint_name_original    string
		index_name                  string
		index_name_original         string
		index_type                  rsql.Td_index_type_t
		index_cluster_type          rsql.Td_cluster_type_t
		colname_list                []string
		change_table_name           string
		change_table_name_original  string
		column_name                 string
		change_column_name          string
		change_column_name_original string
	)

	assert(parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_TABLE)

	if rsql_err := parser.Check_compatibility(COMPAT_ALTER_TABLE); rsql_err != nil { // check if statement is compatible with previous statements
		return nil, rsql_err
	}

	// create Token_stmt_ALTER_TABLE

	token_stmt_alter_table = &Token_stmt_ALTER_TABLE{}
	parser.token_init(&token_stmt_alter_table.Token, TOK_STMT_ALTER_TABLE)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip TABLE
		return nil, rsql_err
	}

	// eat unqualified or qualified table name

	if qname, rsql_err = parser.Eat_object_qname(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_alter_table.At_table_qname = qname

	// process clause WITH NAME, or skip option WITH NOCHECK

	if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_WITH {
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
			return nil, rsql_err
		}

		if parser.Current_lexeme.Is_auxword(lex.AUXWORD_NAME) { // WITH NAME clause
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NAME
				return nil, rsql_err
			}

			if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
				return nil, rsql_err
			}

			if change_table_name, change_table_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat new table name
				return nil, rsql_err
			}

			token_stmt_alter_table.At_change_table_name = change_table_name
			token_stmt_alter_table.At_change_table_name_original = change_table_name_original

			// put in Registration

			parser.Registration.Register_object_qname(qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)
			parser.Registration.Register_object(qname.Database_name, qname.Schema_name, change_table_name, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

			return token_stmt_alter_table, nil //---> exit
		}

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_NOCHECK:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NOCHECK
				return nil, rsql_err
			}

		case lex.LEX_KEYWORD_CHECK:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_WITH_CHECK_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CHECK_OR_NOCHECK_KEYWORD_EXPECTED, rsql.ERROR_BATCH_ABORT)
		}
	}

	//======== eat subcommand ============

	switch parser.Current_lexeme.Lex_subtype {
	case lex.LEX_KEYWORD_DROP: // is considered as a DROP INDEX statement
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip DROP
			return nil, rsql_err
		}

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_CONSTRAINT { // skip optional CONSTRAINT
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {
				return nil, rsql_err
			}
		}

		if constraint_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat constraint name
			return nil, rsql_err
		}

		// create DROP INDEX statement

		token_stmt_drop_index := &Token_stmt_DROP_INDEX{}
		parser.token_init(&token_stmt_drop_index.Token, TOK_STMT_DROP_INDEX)
		token_stmt_drop_index.Tok_batch_line = token_stmt_alter_table.Tok_batch_line

		token_stmt_drop_index.Di_table_qname = token_stmt_alter_table.At_table_qname
		token_stmt_drop_index.Di_index_name = constraint_name

		// put in Registration

		parser.Registration.Register_object_qname(token_stmt_drop_index.Di_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

		return token_stmt_drop_index, nil //===> return

	case lex.LEX_KEYWORD_ADD: // is considered as a CREATE INDEX statement
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ADD
			return nil, rsql_err
		}

		// eat optional constraint name

		constraint_name = ""
		constraint_name_original = ""

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_CONSTRAINT {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CONSTRAINT
				return nil, rsql_err
			}

			if constraint_name, constraint_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat constraint name
				return nil, rsql_err
			}
		}

		// eat PRIMARY KEY, UNIQUE, CHECK

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_PRIMARY:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip PRIMARY
				return nil, rsql_err
			}

			if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_KEY); rsql_err != nil { // expect KEY
				return nil, rsql_err
			}

			index_name = constraint_name
			index_name_original = constraint_name_original

			index_type = rsql.TD_PRIMARY_KEY
			index_cluster_type = rsql.TD_CLUSTER_TYPE_UNDEFINED

		case lex.LEX_KEYWORD_UNIQUE:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip UNIQUE
				return nil, rsql_err
			}

			index_name = constraint_name
			index_name_original = constraint_name_original

			index_type = rsql.TD_UNIQUE
			index_cluster_type = rsql.TD_NONCLUSTERED

		case lex.LEX_KEYWORD_CHECK:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_CHECK_CONSTRAINT_NOT_SUPPORTED, rsql.ERROR_BATCH_ABORT)

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_TBL_UNKNOWN_CONSTRAINT, rsql.ERROR_BATCH_ABORT)
		}

		// for PRIMARY KEY or UNIQUE, eat CLUSTERED or NONCLUSTERED, and list of columns

		// create CREATE INDEX statement

		token_stmt_create_index := &Token_stmt_CREATE_INDEX{}
		parser.token_init(&token_stmt_create_index.Token, TOK_STMT_CREATE_INDEX)
		token_stmt_create_index.Tok_batch_line = token_stmt_alter_table.Tok_batch_line

		token_stmt_create_index.Ci_table_qname = token_stmt_alter_table.At_table_qname
		token_stmt_create_index.Ci_index_name = index_name // if no constraint name have been provided for PRIMARY KEY or UNIQUE, indexdef.Td_index_name remains empty string
		token_stmt_create_index.Ci_index_name_original = index_name_original
		token_stmt_create_index.Ci_index_type = index_type
		token_stmt_create_index.Ci_cluster_type = index_cluster_type

		// eat CLUSTERED or NONCLUSTERED if any

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_CLUSTERED:
			token_stmt_create_index.Ci_cluster_type = rsql.TD_CLUSTERED

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip CLUSTERED
				return nil, rsql_err
			}

		case lex.LEX_KEYWORD_NONCLUSTERED:
			token_stmt_create_index.Ci_cluster_type = rsql.TD_NONCLUSTERED

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NONCLUSTERED
				return nil, rsql_err
			}
		}

		// eat column list. Expect left parenthese

		if colname_list, rsql_err = parser.Eat_list_of_colnames_for_index(); rsql_err != nil {
			return nil, rsql_err
		}

		if token_stmt_create_index.Ci_index_type == rsql.TD_INDEX { // never happens, as INDEX is not a constraint
			colname_list = append(colname_list, rsql.ROWID)
		}

		token_stmt_create_index.Ci_colname_list = colname_list

		// skip useless ON <file_group> if any

		if parser.Current_lexeme.Lex_subtype == lex.LEX_KEYWORD_ON {
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ON
				return nil, rsql_err
			}

			if parser.Current_lexeme.Lex_type != lex.LEX_IDENTPART {
				return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_FILE_GROUP_OR_PRIMARY_EXPECTED, rsql.ERROR_BATCH_ABORT)
			}

			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip <file_group>, which is often [PRIMARY]
				return nil, rsql_err
			}
		}

		// put in Registration

		parser.Registration.Register_object_qname(token_stmt_create_index.Ci_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

		return token_stmt_create_index, nil //===> return

	case lex.LEX_KEYWORD_ALTER:
		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip ALTER
			return nil, rsql_err
		}

		if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_COLUMN); rsql_err != nil { // skip COLUMN
			return nil, rsql_err
		}

		if column_name, rsql_err = parser.Eat_identpart(); rsql_err != nil { // eat column name
			return nil, rsql_err
		}

		if column_name == rsql.ROWID {
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ALTER_COLUMN_ROWID_FORBIDDEN, rsql.ERROR_BATCH_ABORT)
		}

		token_stmt_alter_table.At_column_name = column_name

		switch parser.Current_lexeme.Lex_subtype {
		case lex.LEX_KEYWORD_NULL:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NULL
				return nil, rsql_err
			}

			token_stmt_alter_table.At_change_column_nullability = dict.ALTER_COLUMN_NULL

		case lex.LEX_OPERATOR_LOGICAL_NOT:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NOT
				return nil, rsql_err
			}

			if rsql_err = parser.Eat_keyword(lex.LEX_KEYWORD_NULL); rsql_err != nil { // skip NULL
				return nil, rsql_err
			}

			token_stmt_alter_table.At_change_column_nullability = dict.ALTER_COLUMN_NOT_NULL

		case lex.LEX_KEYWORD_WITH:
			if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip WITH
				return nil, rsql_err
			}

			if rsql_err = parser.Eat_auxword(lex.AUXWORD_NAME); rsql_err != nil { // skip NAME
				return nil, rsql_err
			}

			if rsql_err = parser.Eat_equal_symbol(); rsql_err != nil { // skip '=' operator
				return nil, rsql_err
			}

			if change_column_name, change_column_name_original, rsql_err = parser.Eat_identpart_original(); rsql_err != nil { // eat new column name
				return nil, rsql_err
			}

			token_stmt_alter_table.At_change_column_name = change_column_name
			token_stmt_alter_table.At_change_column_name_original = change_column_name_original

		default:
			return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ALTER_COLUMN_CLAUSE_UNKNOWN, rsql.ERROR_BATCH_ABORT)
		}

	default:
		return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_ALTER_TABLE_CLAUSE_UNKNOWN, rsql.ERROR_BATCH_ABORT)
	}

	// put in Registration

	parser.Registration.Register_object_qname(token_stmt_alter_table.At_table_qname, lk.LOCK_EXCLUSIVE, rsql.PERMISSION_MODIFY_OBJECT)

	return token_stmt_alter_table, nil
}
