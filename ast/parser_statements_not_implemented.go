package ast

import (
	"rsql"
)

// TODO REMPLACER Tokener PAR LE VRAI TYPE !!!!!!!!!!!!!!!!!!!!

func (parser *Parser) get_statement_CHECKPOINT() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_CREATE_DEFAULT() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_CREATE_PROCEDURE() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_CREATE_RULE() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_CREATE_TRIGGER() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_CREATE_VIEW() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_DROP_DEFAULT() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_DROP_PROCEDURE() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_DROP_RULE() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_DROP_TRIGGER() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_DROP_VIEW() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}

func (parser *Parser) get_statement_UPDATE_STATISTICS() (Tokener, *rsql.Error) {
	return nil, rsql.New_Error(rsql.ERROR_SQL_SYNTAX, rsql.ERROR_INVALID_SYNTAX_STATEMENT_NOT_IMPLEMENTED, rsql.ERROR_BATCH_ABORT)
}
