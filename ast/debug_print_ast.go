package ast

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"unicode/utf8"

	"rsql"
	"rsql/data"
	"rsql/dict"
)

type Margin_buffer_t []byte

const DEBUG_PRINT_AST_MARGIN_CAPACITY_DEFAULT = 100

func address_string(tok *Token_primary) string {

	return fmt.Sprintf("%p", tok)
}

func collation_display(tok *Token_primary) string {

	if tok.Prim_collation != "" {
		return fmt.Sprintf("%s<%d>", tok.Prim_collation, tok.Prim_collation_strength)
	}

	return ""
}

func (margin *Margin_buffer_t) length() int {

	return len(*margin)
}

// set_string appends a string at position i.
// Never fails.
//
func (margin *Margin_buffer_t) set_string(i int, s string) {

	mbuf := *margin
	capacity := cap(mbuf)
	length := len(mbuf)

	switch {
	case i > capacity: // if buffer too small, enlarge it
		mbuf = append(mbuf[:capacity], make([]byte, i-capacity)...)

		mbuf = mbuf[:cap(mbuf)]
		for k := capacity; k < len(mbuf); k++ { // fill extra capacity with blanks
			mbuf[k] = ' '
		}

	case i < length: // if length of the margin buffer is made smaller, erase the extra part
		for k := i; k < length; k++ {
			mbuf[k] = ' '
		}
	}

	// append string

	mbuf = append(mbuf[:i], s...)

	*margin = mbuf
}

func (margin *Margin_buffer_t) debug_print_Token_primary_LITERAL(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type&(TOK_PRIM_LITERAL_SYSCOLLATOR|TOK_PRIM_LITERAL_NULL|TOK_PRIM_LITERAL_BOOLEAN|TOK_PRIM_LITERAL_HEXASTRING|TOK_PRIM_LITERAL_STRING|TOK_PRIM_LITERAL_NUMBER) != 0)

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	//rsql.Assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	prim_name := tok.Prim_name
	if tok.Prim_type == TOK_PRIM_LITERAL_STRING {
		prim_name = "'" + tok.Prim_name + "'"
	}

	context.Send_MESSAGE_or_panic("%s---------  \033[32m%s\033[0m   %s", *margin, prim_name, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_VARIABLE(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_VARIABLE)

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[33m%s\033[0m   %s", *margin, tok.Prim_name, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)

}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_IDENTIFIER(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_IDENTIFIER)

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[1;35m%s.%s.%s.%s\033[0m   %s", *margin, tok.Prim_database_name, tok.Prim_schema_name, tok.Prim_table_name, tok.Prim_name, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)

}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_OPERATOR(context *rsql.Context, margin_idx int, tok *Token_primary) {
	var any_all_comparison string

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_OPERATOR)

	/* walk children */

	switch tok.Prim_cardinality {

	case TOK_PRIM_OPERATOR_CARDINALITY_ONE_OPERAND:
		assert(tok.Prim_left != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left)

		assert(tok.Prim_right == nil)

	case TOK_PRIM_OPERATOR_CARDINALITY_TWO_OPERANDS:
		assert(tok.Prim_left != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left)

		assert(tok.Prim_right != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_right)

	case TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IS_NULL:
		assert(tok.Prim_left != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left)

		assert(tok.Prim_right == nil)

	case TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_LIKE:
		assert(tok.Prim_left != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left)

		assert(tok.Prim_right != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_right)

	case TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_IN_LIST:
		assert(tok.Prim_left != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left)

		assert(tok.Prim_right == nil)

		for _, tok_in_list := range tok.Prim_listexpr {
			margin.debug_print_Token_primary(context, margin_idx+2, tok_in_list)
		}

	case TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL:
		assert(tok.Prim_left != nil)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left)

		assert(tok.Prim_right != nil)
		assert(tok.Prim_right.Prim_subtype == TOK_PRIM_SUBQUERY_MANY)
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_right)

	default:
		panic("cardinality unknown")

	}

	if tok.Prim_syscollator != nil {
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_syscollator)
	}

	if tok.Prim_syslanguage != nil {
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_syslanguage)
	}

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	if tok.Prim_cardinality == TOK_PRIM_OPERATOR_CARDINALITY_SPECIAL_ANY_ALL {
		switch tok.Prim_any_all_comp_operator_subtype {
		case TOK_PRIM_OPERATOR_COMP_EQUAL:
			any_all_comparison = "="

		case TOK_PRIM_OPERATOR_COMP_LESS:
			any_all_comparison = "<"

		case TOK_PRIM_OPERATOR_COMP_GREATER:
			any_all_comparison = ">"

		case TOK_PRIM_OPERATOR_COMP_LESS_EQUAL:
			any_all_comparison = "<="

		case TOK_PRIM_OPERATOR_COMP_GREATER_EQUAL:
			any_all_comparison = ">="

		case TOK_PRIM_OPERATOR_COMP_NOT_EQUAL:
			any_all_comparison = "<>"

		default:
			panic("unknown comparison")
		}
		context.Send_MESSAGE_or_panic("%s---------  \033[1;31m%s %s\033[0m   %s", *margin, tok.Prim_name, any_all_comparison, collation_display(tok))

	} else {
		context.Send_MESSAGE_or_panic("%s---------  \033[1;31m%s\033[0m   %s", *margin, tok.Prim_name, collation_display(tok))
	}

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_SYSFUNC(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_SYSFUNC)

	/* walk children */

	for _, tok_in_list := range tok.Prim_listexpr {
		margin.debug_print_Token_primary(context, margin_idx+2, tok_in_list)
	}

	if tok.Prim_syscollator != nil {
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_syscollator)
	}

	if tok.Prim_syslanguage != nil {
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_syslanguage)
	}

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[34m%s(%s)\033[0m   %s", *margin, tok.Prim_name, tok.Prim_sql_datepartfield, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_UFUNC(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_UFUNC)

	/* walk children */

	for _, tok_in_list := range tok.Prim_listexpr {
		margin.debug_print_Token_primary(context, margin_idx+2, tok_in_list)
	}

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	// tok.Prim_database_name may be nil or not nil
	assert(tok.Prim_schema_name != "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[34m%s.%s.%s()\033[0m   %s", *margin, tok.Prim_database_name, tok.Prim_schema_name, tok.Prim_name, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_primary_CAST(context *rsql.Context, margin_idx int, tok *Token_primary) {
	var sql_datatype_shortdef string

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_CAST)

	/* walk children */

	assert(tok.Prim_left != nil)
	margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left) // expression

	if tok.Prim_right != nil {
		margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_right) // style
	}

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[1;34m%s\033[0m   %s", *margin, tok.Prim_name, collation_display(tok))

	// detail lines

	if tok.Prim_sql_datatype != 0 {
		sql_datatype_shortdef = rsql.Datatype_shortdef(tok.Prim_sql_datatype, tok.Prim_sql_precision, tok.Prim_sql_scale, false) // datatype can be DATATYPE_CHAR (fixlen_flag argument is ignored)
	}

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s cast to : %s", *margin, sql_datatype_shortdef)
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_SUBQUERY(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_subtype == TOK_PRIM_SUBQUERY_ONE || tok.Prim_subtype == TOK_PRIM_SUBQUERY_MANY)

	/* walk subquery */

	assert(tok.Prim_xtable_subquery != nil)
	margin.debug_print_Xtable(context, margin_idx+2, tok.Prim_xtable_subquery, "")

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[33m%s\033[0m   %s", *margin, tok.Prim_name, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_EXISTS(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_EXISTS)

	/* walk subquery */

	assert(tok.Prim_left != nil)
	assert(tok.Prim_left.Prim_subtype == TOK_PRIM_SUBQUERY_MANY)
	margin.debug_print_Token_primary(context, margin_idx+2, tok.Prim_left) // expression

	assert(tok.Prim_right == nil)

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[33m%s\033[0m   %s", *margin, tok.Prim_name, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_primary_VAR_CASE(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)
	assert(tok.Tok_prev == nil)
	assert(tok.Tok_next == nil)
	assert(tok.Prim_type == TOK_PRIM_CASE)

	/* walk subquery */

	assert(tok.Prim_left == nil)
	assert(tok.Prim_right == nil)

	for _, tok_in_list := range tok.Prim_listexpr {
		margin.debug_print_Token_primary(context, margin_idx+2, tok_in_list)
	}

	// +--------- headline

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.

	assert(tok.Prim_database_name == "")
	assert(tok.Prim_schema_name == "")
	assert(tok.Prim_table_name == "")
	assert(tok.Prim_name != "")
	assert(tok.Prim_status == TOK_PRIM_STATUS_COMPLETE)

	context.Send_MESSAGE_or_panic("%s---------  \033[33m%s\033[0m   %s", *margin, tok.Prim_name, collation_display(tok))

	// detail lines

	margin.set_string(margin_idx, "|") // display a vertical bar at left side of the data block.
	context.Send_MESSAGE_or_panic("%s subtype : %s", *margin, tok.Prim_subtype)
	context.Send_MESSAGE_or_panic("%s address : %s", *margin, address_string(tok))
	context.Send_MESSAGE_or_panic("%s", *margin)
}

// print Dataslot properties.
//
func (margin *Margin_buffer_t) debug_print_Dataslot(context *rsql.Context, margin_idx int, fieldname string, cd_NOT_NULL_flag bool, dataslot rsql.IDataslot) {
	var (
		datatype         rsql.Datatype_t
		data_precision   uint16
		data_scale       uint16
		data_fixlen_flag bool
		fieldname_width  int
		datatype_display string
	)

	if fieldname != "" {
		fieldname_width = utf8.RuneCountInString(fieldname)
	}

	datatype = dataslot.Datatype()

	switch datatype {
	case rsql.DATATYPE_VARBINARY:
		data_precision = dataslot.(*data.VARBINARY).Data_precision

	case rsql.DATATYPE_VARCHAR:
		data_precision = dataslot.(*data.VARCHAR).Data_precision
		data_fixlen_flag = dataslot.(*data.VARCHAR).Data_fixlen_flag

	case rsql.DATATYPE_MONEY:
		data_precision = 0
		data_scale = 0

	case rsql.DATATYPE_NUMERIC:
		data_precision = dataslot.(*data.NUMERIC).Data_precision
		data_scale = dataslot.(*data.NUMERIC).Data_scale
	}

	datatype_display = rsql.Datatype_shortdef(datatype, data_precision, data_scale, data_fixlen_flag)

	if dataslot.Kind() == rsql.KIND_RO_LEAF {
		datatype_display += "  \033[32m(RO_LEAF)\033[0m"
	}

	context.Send_MESSAGE_or_panic("%s \033[35m%-*s\033[0mms_data [%p]: %s", *margin, fieldname_width, fieldname, dataslot, datatype_display)
}

// debug_print_Token_primary prints a Token_primary and its children as a tree.
//
func (margin *Margin_buffer_t) debug_print_Token_primary(context *rsql.Context, margin_idx int, tok *Token_primary) {

	assert(tok.Tok_category == TOK_PRIMARY)

	/* print Token_primary */

	switch tok.Prim_type {

	case TOK_PRIM_LITERAL_SYSCOLLATOR, /*========================== literal ===================== */
		TOK_PRIM_LITERAL_NULL,
		TOK_PRIM_LITERAL_BOOLEAN,
		TOK_PRIM_LITERAL_HEXASTRING,
		TOK_PRIM_LITERAL_STRING,
		TOK_PRIM_LITERAL_NUMBER:
		margin.debug_print_Token_primary_LITERAL(context, margin_idx, tok)

	case TOK_PRIM_IDENTIFIER: /*========================== identifier ===================== */
		margin.debug_print_Token_primary_VAR_IDENTIFIER(context, margin_idx, tok)

	case TOK_PRIM_VARIABLE: /*========================== variable ===================== */
		margin.debug_print_Token_primary_VAR_VARIABLE(context, margin_idx, tok)

	case TOK_PRIM_OPERATOR: /*========================== internal node =====================*/
		margin.debug_print_Token_primary_VAR_OPERATOR(context, margin_idx, tok)

	case TOK_PRIM_SYSFUNC: /*========================== sysfunc ===================== */
		margin.debug_print_Token_primary_VAR_SYSFUNC(context, margin_idx, tok)

	case TOK_PRIM_UFUNC: /*========================== ufunc ===================== */
		margin.debug_print_Token_primary_VAR_UFUNC(context, margin_idx, tok)

	case TOK_PRIM_CAST: /*========================== conversion node ===================== */
		margin.debug_print_Token_primary_CAST(context, margin_idx, tok)

	case TOK_PRIM_CASE: /*========================== case  ===================== */
		margin.debug_print_Token_primary_VAR_CASE(context, margin_idx, tok)

	case TOK_PRIM_EXISTS: /*========================== exists  ===================== */
		margin.debug_print_Token_primary_VAR_EXISTS(context, margin_idx, tok)

	case TOK_PRIM_SUBQUERY: /*========================== subquery ===================== */

		switch tok.Prim_subtype {

		case TOK_PRIM_SUBQUERY_ONE,
			TOK_PRIM_SUBQUERY_MANY:
			margin.debug_print_Token_primary_VAR_SUBQUERY(context, margin_idx, tok)

		default:
			panic("cannot print this token_primary. Unknown subtype.")
		}

	default: /*========================== default ===================== */
		panic("cannot print this token_primary. Unknown.")
	}

	/* print collate clause */

	//	if tok.Prim_sql_collate != nil {
	//		context.Send_MESSAGE_or_panic("%s collate: %s", *margin, tok.Prim_sql_collate.Lex_word)
	//	}

	/* print collation */

	//	if tok.Prim_collation != nil {
	//		context.Send_MESSAGE_or_panic("%s collation: %s", *margin, tok.Prim_collation.Lex_word, tok.Prim_collation_strength)
	//	}

	/* print line/pos */

	context.Send_MESSAGE_or_panic("%s line/pos: %d:%d", *margin, tok.Tok_batch_line.No, tok.Tok_batch_line.Pos)

	/* print Dataslot */

	if tok.Prim_dataslot != nil {
		margin.debug_print_Dataslot(context, margin_idx, "", false, tok.Prim_dataslot)
	}
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_IF(context *rsql.Context, margin_idx int, tok *Token_stmt_IF) {

	assert(tok.Tok_category == TOK_STMT_IF)

	/* walk children */

	margin.set_string(margin_idx, "i") // display i at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.If_cond)

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;42m%s\033[0m", *margin, "TOK_STMT_IF")

	margin.set_string(margin_idx, "t")                                       // display t at left side of the data block.
	margin.debug_print_sequence_of_Token(context, margin_idx+3, tok.If_then) // THEN

	if tok.If_else != nil { // if 'else' clause exists
		margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
		context.Send_MESSAGE_or_panic("%s\033[1;33;42m%s\033[0m", *margin, "ELSE")

		margin.set_string(margin_idx, "e") // display e at left side of the data block.
		margin.debug_print_sequence_of_Token(context, margin_idx+3, tok.If_else)
	}

	// detail lines

	switch tok.If_else {
	case nil: // if no 'else' clause
		margin.set_string(margin_idx, "t") // display t at left side of the data block.
	default:
		margin.set_string(margin_idx, "e") // display e at left side of the data block.
	}

	context.Send_MESSAGE_or_panic("%s", *margin)

}

func (margin *Margin_buffer_t) debug_print_Token_stmt_WHILE(context *rsql.Context, margin_idx int, tok *Token_stmt_WHILE) {

	assert(tok.Tok_category == TOK_STMT_WHILE)

	/* walk child */

	margin.set_string(margin_idx, "w") // display w at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.While_cond)

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;42m%s\033[0m", *margin, "TOK_STMT_WHILE")

	margin.set_string(margin_idx, "l")                                          // display l at left side of the data block.
	margin.debug_print_sequence_of_Token(context, margin_idx+3, tok.While_body) // WHILE BODY

	/* detail lines */

	margin.set_string(margin_idx, "l") // display l at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)

}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DECLARE_variable(context *rsql.Context, margin_idx int, tok *Token_stmt_DECLARE_variable) {

	assert(tok.Tok_category == TOK_STMT_DECLARE_VARIABLE)

	/* walk child */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.Decl_variable)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;44m%s\033[0m", *margin, "TOK_STMT_DECLARE_VARIABLE")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ASSIGNMENT(context *rsql.Context, margin_idx int, tok *Token_stmt_ASSIGNMENT) {

	assert(tok.Tok_category == TOK_STMT_ASSIGNMENT)

	/* walk children */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.Set_rvalue)

	margin.debug_print_Token_primary(context, margin_idx+3, tok.Set_lvalue)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_ASSIGNMENT")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)

}

func (margin *Margin_buffer_t) debug_print_sequence_of_Token(context *rsql.Context, margin_idx int, tok Tokener) {

	margin.set_string(margin_idx, ".")

	for tok != nil {

		switch tok.Category() {

		case TOK_STMT_DECLARE_VARIABLE:
			margin.debug_print_Token_stmt_DECLARE_variable(context, margin_idx, tok.(*Token_stmt_DECLARE_variable))

		case TOK_STMT_ASSIGNMENT:
			margin.debug_print_Token_stmt_ASSIGNMENT(context, margin_idx, tok.(*Token_stmt_ASSIGNMENT))

		case TOK_STMT_THROW:
			margin.debug_print_Token_stmt_THROW(context, margin_idx, tok.(*Token_stmt_THROW))

		case TOK_STMT_PRINT:
			margin.debug_print_Token_stmt_PRINT(context, margin_idx, tok.(*Token_stmt_PRINT))

		case TOK_STMT_BEGIN_TRANSACTION:
			margin.debug_print_Token_stmt_BEGIN_TRANSACTION(context, margin_idx, tok.(*Token_stmt_BEGIN_TRANSACTION))

		case TOK_STMT_COMMIT_TRANSACTION:
			margin.debug_print_Token_stmt_COMMIT_TRANSACTION(context, margin_idx, tok.(*Token_stmt_COMMIT_TRANSACTION))

		case TOK_STMT_ROLLBACK_TRANSACTION:
			margin.debug_print_Token_stmt_ROLLBACK_TRANSACTION(context, margin_idx, tok.(*Token_stmt_ROLLBACK_TRANSACTION))

		case TOK_STMT_ASSERT_:
			margin.debug_print_Token_stmt_ASSERT_(context, margin_idx, tok.(*Token_stmt_ASSERT_))

		case TOK_STMT_ASSERT_NULL_:
			margin.debug_print_Token_stmt_ASSERT_NULL_(context, margin_idx, tok.(*Token_stmt_ASSERT_NULL_))

		case TOK_STMT_ASSERT_ERROR_:
			margin.debug_print_Token_stmt_ASSERT_ERROR_(context, margin_idx, tok.(*Token_stmt_ASSERT_ERROR_))

		case TOK_STMT_CREATE_LOGIN:
			margin.debug_print_Token_stmt_CREATE_LOGIN(context, margin_idx, tok.(*Token_stmt_CREATE_LOGIN))

		case TOK_STMT_ALTER_LOGIN:
			margin.debug_print_Token_stmt_ALTER_LOGIN(context, margin_idx, tok.(*Token_stmt_ALTER_LOGIN))

		case TOK_STMT_DROP_LOGIN:
			margin.debug_print_Token_stmt_DROP_LOGIN(context, margin_idx, tok.(*Token_stmt_DROP_LOGIN))

		case TOK_STMT_CREATE_DATABASE:
			margin.debug_print_Token_stmt_CREATE_DATABASE(context, margin_idx, tok.(*Token_stmt_CREATE_DATABASE))

		case TOK_STMT_ALTER_DATABASE:
			margin.debug_print_Token_stmt_ALTER_DATABASE(context, margin_idx, tok.(*Token_stmt_ALTER_DATABASE))

		case TOK_STMT_DROP_DATABASE:
			margin.debug_print_Token_stmt_DROP_DATABASE(context, margin_idx, tok.(*Token_stmt_DROP_DATABASE))

		case TOK_STMT_CREATE_USER:
			margin.debug_print_Token_stmt_CREATE_USER(context, margin_idx, tok.(*Token_stmt_CREATE_USER))

		case TOK_STMT_ALTER_USER:
			margin.debug_print_Token_stmt_ALTER_USER(context, margin_idx, tok.(*Token_stmt_ALTER_USER))

		case TOK_STMT_DROP_USER:
			margin.debug_print_Token_stmt_DROP_USER(context, margin_idx, tok.(*Token_stmt_DROP_USER))

		case TOK_STMT_CREATE_ROLE:
			margin.debug_print_Token_stmt_CREATE_ROLE(context, margin_idx, tok.(*Token_stmt_CREATE_ROLE))

		case TOK_STMT_ALTER_ROLE:
			margin.debug_print_Token_stmt_ALTER_ROLE(context, margin_idx, tok.(*Token_stmt_ALTER_ROLE))

		case TOK_STMT_DROP_ROLE:
			margin.debug_print_Token_stmt_DROP_ROLE(context, margin_idx, tok.(*Token_stmt_DROP_ROLE))

		case TOK_STMT_GRANT:
			margin.debug_print_Token_stmt_GRANT(context, margin_idx, tok.(*Token_stmt_GRANT))

		case TOK_STMT_DENY:
			margin.debug_print_Token_stmt_DENY(context, margin_idx, tok.(*Token_stmt_DENY))

		case TOK_STMT_REVOKE:
			margin.debug_print_Token_stmt_REVOKE(context, margin_idx, tok.(*Token_stmt_REVOKE))

		case TOK_STMT_ALTER_AUTHORIZATION:
			margin.debug_print_Token_stmt_ALTER_AUTHORIZATION(context, margin_idx, tok.(*Token_stmt_ALTER_AUTHORIZATION))

		case TOK_STMT_SHOW_COLLATIONS:
			margin.debug_print_Token_stmt_SHOW_COLLATIONS(context, margin_idx, tok.(*Token_stmt_SHOW_COLLATIONS))

		case TOK_STMT_SHOW_LANGUAGES:
			margin.debug_print_Token_stmt_SHOW_LANGUAGES(context, margin_idx, tok.(*Token_stmt_SHOW_LANGUAGES))

		case TOK_STMT_SHOW_LOCKS:
			margin.debug_print_Token_stmt_SHOW_LOCKS(context, margin_idx, tok.(*Token_stmt_SHOW_LOCKS))

		case TOK_STMT_SHOW_WORKERS:
			margin.debug_print_Token_stmt_SHOW_WORKERS(context, margin_idx, tok.(*Token_stmt_SHOW_WORKERS))

		case TOK_STMT_SHOW_INFO:
			margin.debug_print_Token_stmt_SHOW_INFO(context, margin_idx, tok.(*Token_stmt_SHOW_INFO))

		case TOK_STMT_SHOW:
			margin.debug_print_Token_stmt_SHOW(context, margin_idx, tok.(*Token_stmt_SHOW))

		case TOK_STMT_SLEEP:
			margin.debug_print_Token_stmt_SLEEP(context, margin_idx, tok.(*Token_stmt_SLEEP))

		case TOK_STMT_SHUTDOWN:
			margin.debug_print_Token_stmt_SHUTDOWN(context, margin_idx, tok.(*Token_stmt_SHUTDOWN))

		case TOK_STMT_DUMP:
			margin.debug_print_Token_stmt_DUMP(context, margin_idx, tok.(*Token_stmt_DUMP))

		case TOK_STMT_BACKUP:
			margin.debug_print_Token_stmt_BACKUP(context, margin_idx, tok.(*Token_stmt_BACKUP))

		case TOK_STMT_RESTORE:
			margin.debug_print_Token_stmt_RESTORE(context, margin_idx, tok.(*Token_stmt_RESTORE))

		case TOK_STMT_SET_NOCOUNT:
			margin.debug_print_Token_stmt_SET_NOCOUNT(context, margin_idx, tok.(*Token_stmt_SET_NOCOUNT))

		case TOK_STMT_SET_LEXER_OR_PARSER_OPTION:
			// pass

		case TOK_STMT_ALTER_SERVER_PARAMETER:
			margin.debug_print_Token_stmt_ALTER_SERVER_PARAMETER(context, margin_idx, tok.(*Token_stmt_ALTER_SERVER_PARAMETER))

		case TOK_STMT_IF:
			margin.debug_print_Token_stmt_IF(context, margin_idx, tok.(*Token_stmt_IF))

		case TOK_STMT_WHILE:
			margin.debug_print_Token_stmt_WHILE(context, margin_idx, tok.(*Token_stmt_WHILE))

		case TOK_STMT_BREAK:
			margin.debug_print_Token_stmt_BREAK(context, margin_idx, tok.(*Token_stmt_BREAK))

		case TOK_STMT_CONTINUE:
			margin.debug_print_Token_stmt_CONTINUE(context, margin_idx, tok.(*Token_stmt_CONTINUE))

		case TOK_STMT_RETURN:
			margin.debug_print_Token_stmt_RETURN(context, margin_idx, tok.(*Token_stmt_RETURN))

		case TOK_STMT_GOTO:
			margin.debug_print_Token_stmt_GOTO(context, margin_idx, tok.(*Token_stmt_GOTO))

		case TOK_STMT_LABEL:
			margin.debug_print_Token_stmt_LABEL(context, margin_idx, tok.(*Token_stmt_LABEL))

		case TOK_STMT_USE:
			margin.debug_print_Token_stmt_USE(context, margin_idx, tok.(*Token_stmt_USE))

		case TOK_STMT_CREATE_TABLE:
			margin.debug_print_Token_stmt_CREATE_TABLE(context, margin_idx, tok.(*Token_stmt_CREATE_TABLE))

		case TOK_STMT_ALTER_TABLE:
			margin.debug_print_Token_stmt_ALTER_TABLE(context, margin_idx, tok.(*Token_stmt_ALTER_TABLE))

		case TOK_STMT_DROP_TABLE:
			margin.debug_print_Token_stmt_DROP_TABLE(context, margin_idx, tok.(*Token_stmt_DROP_TABLE))

		case TOK_STMT_CREATE_INDEX:
			margin.debug_print_Token_stmt_CREATE_INDEX(context, margin_idx, tok.(*Token_stmt_CREATE_INDEX))

		case TOK_STMT_ALTER_INDEX:
			margin.debug_print_Token_stmt_ALTER_INDEX(context, margin_idx, tok.(*Token_stmt_ALTER_INDEX))

		case TOK_STMT_DROP_INDEX:
			margin.debug_print_Token_stmt_DROP_INDEX(context, margin_idx, tok.(*Token_stmt_DROP_INDEX))

		case TOK_STMT_INSERT_INTO:
			margin.debug_print_Token_stmt_INSERT_INTO(context, margin_idx, tok.(*Token_stmt_INSERT_INTO))

		case TOK_STMT_BULK_INSERT:
			margin.debug_print_Token_stmt_BULK_INSERT(context, margin_idx, tok.(*Token_stmt_BULK_INSERT))

		case TOK_STMT_BULK_EXPORT:
			margin.debug_print_Token_stmt_BULK_EXPORT(context, margin_idx, tok.(*Token_stmt_BULK_EXPORT))

		case TOK_STMT_SELECT_OR_UNION:
			margin.debug_print_Token_stmt_SELECT_or_UNION(context, margin_idx, tok.(*Token_stmt_SELECT_or_UNION))

		case TOK_STMT_DELETE:
			margin.debug_print_Token_stmt_DELETE(context, margin_idx, tok.(*Token_stmt_DELETE))

		case TOK_STMT_UPDATE:
			margin.debug_print_Token_stmt_UPDATE(context, margin_idx, tok.(*Token_stmt_UPDATE))

		case TOK_STMT_TRUNCATE_TABLE:
			margin.debug_print_Token_stmt_TRUNCATE_TABLE(context, margin_idx, tok.(*Token_stmt_TRUNCATE_TABLE))

		case TOK_STMT_SHRINK_TABLE:
			margin.debug_print_Token_stmt_SHRINK_TABLE(context, margin_idx, tok.(*Token_stmt_SHRINK_TABLE))

		default:
			panic("debug_print_sequence_of_Token unknown tok_subcategory")
		}

		tok = tok.Next()

	} // end loop

	margin.set_string(margin_idx, ".")

}

// print AST tree of the batch.
//
// 'margin_idx' is the number of characters up to and including the vertical bar | or + sign you can see in the tree output.
//
// Each debug_print_xxx() function sets this character at position margin_idx, and prints additional information.
//
func Debug_print_AST(context *rsql.Context, tok Tokener) (rsql_err *rsql.Error) {

	var margin Margin_buffer_t

	// trap panic that occurs if Send_MESSAGE_or_panic fails

	defer func() {
		if r := recover(); r != nil {
			rsql_err = r.(*rsql.Error)
		}
	}()

	// print the AST tree

	margin = make([]byte, DEBUG_PRINT_AST_MARGIN_CAPACITY_DEFAULT)
	for k := 0; k < len(margin); k++ { // fill extra space with blanks
		margin[k] = ' '
	}

	margin = margin[:0]

	margin.debug_print_sequence_of_Token(context, 0, tok)

	return nil
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_THROW(context *rsql.Context, margin_idx int, tok *Token_stmt_THROW) {
	assert(tok.Tok_category == TOK_STMT_THROW)

	/* walk child */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.Th_error_number)

	margin.debug_print_Token_primary(context, margin_idx+3, tok.Th_message)

	margin.debug_print_Token_primary(context, margin_idx+3, tok.Th_severity)

	margin.debug_print_Token_primary(context, margin_idx+3, tok.Th_state)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_THROW")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_PRINT(context *rsql.Context, margin_idx int, tok *Token_stmt_PRINT) {
	assert(tok.Tok_category == TOK_STMT_PRINT)

	/* walk children */

	for _, tok_in_list := range tok.Pr_listexpr {
		margin.debug_print_Token_primary(context, margin_idx+3, tok_in_list)
	}

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_PRINT")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_BEGIN_TRANSACTION(context *rsql.Context, margin_idx int, tok *Token_stmt_BEGIN_TRANSACTION) {
	assert(tok.Tok_category == TOK_STMT_BEGIN_TRANSACTION)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_BEGIN_TRANSACTION")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_COMMIT_TRANSACTION(context *rsql.Context, margin_idx int, tok *Token_stmt_COMMIT_TRANSACTION) {
	assert(tok.Tok_category == TOK_STMT_COMMIT_TRANSACTION)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_COMMIT_TRANSACTION")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ROLLBACK_TRANSACTION(context *rsql.Context, margin_idx int, tok *Token_stmt_ROLLBACK_TRANSACTION) {
	assert(tok.Tok_category == TOK_STMT_ROLLBACK_TRANSACTION)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_ROLLBACK_TRANSACTION")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ASSERT_(context *rsql.Context, margin_idx int, tok *Token_stmt_ASSERT_) {
	assert(tok.Tok_category == TOK_STMT_ASSERT_)

	/* walk child */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.As_expr)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_ASSERT_")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ASSERT_NULL_(context *rsql.Context, margin_idx int, tok *Token_stmt_ASSERT_NULL_) {
	assert(tok.Tok_category == TOK_STMT_ASSERT_NULL_)

	/* walk child */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.As_expr)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_ASSERT_NULL_")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ASSERT_ERROR_(context *rsql.Context, margin_idx int, tok *Token_stmt_ASSERT_ERROR_) {
	assert(tok.Tok_category == TOK_STMT_ASSERT_ERROR_)

	/* walk child */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.As_expr)

	margin.debug_print_Token_primary(context, margin_idx+3, tok.As_error_suffix)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_ASSERT_ERROR_")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_CREATE_LOGIN(context *rsql.Context, margin_idx int, tok *Token_stmt_CREATE_LOGIN) {
	assert(tok.Tok_category == TOK_STMT_CREATE_LOGIN)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s (password hash: \"%s\", default database: \"%s\", default language: \"%s\")", *margin, "TOK_STMT_CREATE_LOGIN", tok.Cl_login_name, tok.Cl_password_hash, tok.Cl_default_database, tok.Cl_default_language)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_LOGIN(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_LOGIN) {
	assert(tok.Tok_category == TOK_STMT_ALTER_LOGIN)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s (password hash: \"%s\", default database: \"%s\", default language: \"%s\", change name: \"%s\", disable option: \"%s\")", *margin, "TOK_STMT_ALTER_LOGIN", tok.Al_login_name, tok.Al_change_password_hash, tok.Al_change_default_database, tok.Al_change_default_language, tok.Al_change_name, tok.Al_change_disable_option)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DROP_LOGIN(context *rsql.Context, margin_idx int, tok *Token_stmt_DROP_LOGIN) {
	assert(tok.Tok_category == TOK_STMT_DROP_LOGIN)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s", *margin, "TOK_STMT_DROP_LOGIN", tok.Dl_login_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_CREATE_DATABASE(context *rsql.Context, margin_idx int, tok *Token_stmt_CREATE_DATABASE) {
	assert(tok.Tok_category == TOK_STMT_CREATE_DATABASE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s", *margin, "TOK_STMT_CREATE_DATABASE", tok.Cd_database_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_DATABASE(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_DATABASE) {
	assert(tok.Tok_category == TOK_STMT_ALTER_DATABASE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s (change name: \"%s\", status: \"%s\", mode: \"%s\", access: \"%s\")", *margin, "TOK_STMT_ALTER_DATABASE", tok.Ad_database_name, tok.Ad_change_name, tok.Ad_status, tok.Ad_mode, tok.Ad_access)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DROP_DATABASE(context *rsql.Context, margin_idx int, tok *Token_stmt_DROP_DATABASE) {
	assert(tok.Tok_category == TOK_STMT_DROP_DATABASE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s", *margin, "TOK_STMT_DROP_DATABASE", tok.Dd_database_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_CREATE_USER(context *rsql.Context, margin_idx int, tok *Token_stmt_CREATE_USER) {
	assert(tok.Tok_category == TOK_STMT_CREATE_USER)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s (login: \"%s\")", *margin, "TOK_STMT_CREATE_USER", tok.Cu_database_name, tok.Cu_user_name, tok.Cu_login_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_USER(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_USER) {
	assert(tok.Tok_category == TOK_STMT_ALTER_USER)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s (change name: \"%s\", change login: \"%s\")", *margin, "TOK_STMT_ALTER_USER", tok.Au_database_name, tok.Au_user_name, tok.Au_change_name, tok.Au_change_login)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DROP_USER(context *rsql.Context, margin_idx int, tok *Token_stmt_DROP_USER) {
	assert(tok.Tok_category == TOK_STMT_DROP_USER)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s", *margin, "TOK_STMT_DROP_USER", tok.Du_database_name, tok.Du_user_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_CREATE_ROLE(context *rsql.Context, margin_idx int, tok *Token_stmt_CREATE_ROLE) {
	assert(tok.Tok_category == TOK_STMT_CREATE_ROLE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s", *margin, "TOK_STMT_CREATE_ROLE", tok.Cr_database_name, tok.Cr_role_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_ROLE(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_ROLE) {
	assert(tok.Tok_category == TOK_STMT_ALTER_ROLE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s (change name: \"%s\", add member: \"%s\", drop member: \"%s\")", *margin, "TOK_STMT_ALTER_ROLE", tok.Ar_database_name, tok.Ar_role_name, tok.Ar_change_name, tok.Ar_add_member_name, tok.Ar_drop_member_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DROP_ROLE(context *rsql.Context, margin_idx int, tok *Token_stmt_DROP_ROLE) {
	assert(tok.Tok_category == TOK_STMT_DROP_ROLE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s", *margin, "TOK_STMT_DROP_ROLE", tok.Dr_database_name, tok.Dr_role_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_GRANT(context *rsql.Context, margin_idx int, tok *Token_stmt_GRANT) {
	assert(tok.Tok_category == TOK_STMT_GRANT)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m <%s> ON %s.%s.%s TO %s", *margin, "TOK_STMT_GRANT", tok.Gr_permission, tok.Gr_object_qname.Database_name, tok.Gr_object_qname.Schema_name, tok.Gr_object_qname.Object_name, tok.Gr_list_of_principal_names)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DENY(context *rsql.Context, margin_idx int, tok *Token_stmt_DENY) {
	assert(tok.Tok_category == TOK_STMT_DENY)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m <%s> ON %s.%s.%s TO %s", *margin, "TOK_STMT_DENY", tok.De_permission, tok.De_object_qname.Database_name, tok.De_object_qname.Schema_name, tok.De_object_qname.Object_name, tok.De_list_of_principal_names)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_REVOKE(context *rsql.Context, margin_idx int, tok *Token_stmt_REVOKE) {
	assert(tok.Tok_category == TOK_STMT_REVOKE)

	/* +--------- headline */

	var principal_list string = fmt.Sprintf("%s", tok.Re_list_of_principal_names)
	if tok.Re_from_all == true {
		principal_list = "ALL"
	}

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m <%s> ON %s.%s.%s FROM %s", *margin, "TOK_STMT_REVOKE", tok.Re_permission, tok.Re_object_qname.Database_name, tok.Re_object_qname.Schema_name, tok.Re_object_qname.Object_name, principal_list)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_AUTHORIZATION(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_AUTHORIZATION) {
	assert(tok.Tok_category == TOK_STMT_ALTER_AUTHORIZATION)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s    (change_owner_login: %s)", *margin, "TOK_STMT_ALTER_AUTHORIZATION", tok.Aa_database_name, tok.Aa_change_owner_login)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SET_NOCOUNT(context *rsql.Context, margin_idx int, tok *Token_stmt_SET_NOCOUNT) {
	assert(tok.Tok_category == TOK_STMT_SET_NOCOUNT)

	/* +--------- headline */

	arg := "OFF"
	if tok.Sc_nocount == true {
		arg = "ON"
	}

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s", *margin, "TOK_STMT_SET_NOCOUNT", arg)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_SERVER_PARAMETER(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_SERVER_PARAMETER) {
	assert(tok.Tok_category == TOK_STMT_ALTER_SERVER_PARAMETER)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m (%s: %d, \"%s\")", *margin, "TOK_STMT_ALTER_SERVER_PARAMETER", tok.Asp_param, tok.Asp_value_int, tok.Asp_value_string)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_BREAK(context *rsql.Context, margin_idx int, tok *Token_stmt_BREAK) {
	assert(tok.Tok_category == TOK_STMT_BREAK)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_BREAK")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_CONTINUE(context *rsql.Context, margin_idx int, tok *Token_stmt_CONTINUE) {
	assert(tok.Tok_category == TOK_STMT_CONTINUE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_CONTINUE")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_RETURN(context *rsql.Context, margin_idx int, tok *Token_stmt_RETURN) {
	assert(tok.Tok_category == TOK_STMT_RETURN)

	/* walk child */

	if tok.Ret_expression != nil {
		margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
		margin.debug_print_Token_primary(context, margin_idx+3, tok.Ret_expression)
	}

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_RETURN")

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_GOTO(context *rsql.Context, margin_idx int, tok *Token_stmt_GOTO) {
	assert(tok.Tok_category == TOK_STMT_GOTO)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m <<%s>>", *margin, "TOK_STMT_GOTO", tok.Gt_label)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_LABEL(context *rsql.Context, margin_idx int, tok *Token_stmt_LABEL) {
	assert(tok.Tok_category == TOK_STMT_LABEL)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m <<%s>>", *margin, "TOK_STMT_LABEL", tok.Lab_label)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_USE(context *rsql.Context, margin_idx int, tok *Token_stmt_USE) {
	assert(tok.Tok_category == TOK_STMT_USE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s", *margin, "TOK_STMT_USE", tok.Use_database_name)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

type option_debug_print_t uint8

const (
	DEBUG_PRINT_TABLEDEF_COLDEFS option_debug_print_t = 1 + iota
	DEBUG_PRINT_TABLEDEF_NK
	DEBUG_PRINT_TABLEDEF_PAYLOAD
	DEBUG_PRINT_TABLEDEF_INDEX_COLDEFS_AFTER_NK
)

func (margin *Margin_buffer_t) debug_print_Token_stmt_CREATE_TABLE(context *rsql.Context, margin_idx int, tok *Token_stmt_CREATE_TABLE) {
	assert(tok.Tok_category == TOK_STMT_CREATE_TABLE)

	/* walk child */

	margin.set_string(margin_idx, ".")
	margin.Debug_print_GTabledef(context, margin_idx+3, tok.Ct_database_name, tok.Ct_schema_name, tok.Ct_gtabledef)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s", *margin, "TOK_STMT_CREATE_TABLE", tok.Ct_database_name, tok.Ct_schema_name, tok.Ct_gtabledef.Gtable_name)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) Debug_print_GTabledef(context *rsql.Context, margin_idx int, database_name string, schema_name string, gtabledef *rsql.GTabledef) {

	/* walk base Tabledefs */

	margin.debug_print_Tabledef(context, margin_idx+3, gtabledef.Base, gtabledef.Identity_seed, gtabledef.Identity_increment)

	/* walk all index Tabledefs */

	for _, tabledef := range gtabledef.Indexmap {
		margin.debug_print_Tabledef(context, margin_idx+3, tabledef, gtabledef.Identity_seed, gtabledef.Identity_increment)
	}

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m %s.%s.%s", *margin, "GTABLEDEF", database_name, schema_name, gtabledef.Gtable_name)

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Tabledef_columns(context *rsql.Context, margin_idx int, tabledef *rsql.Tabledef, identity_seed int64, identity_increment int64, option option_debug_print_t) {
	var (
		listcol    []*rsql.Coldef
		other_info string
	)

	switch option {
	case DEBUG_PRINT_TABLEDEF_COLDEFS:
		listcol = tabledef.Td_coldefs
	case DEBUG_PRINT_TABLEDEF_NK:
		listcol = tabledef.Td_nk
	case DEBUG_PRINT_TABLEDEF_PAYLOAD:
		listcol = tabledef.Td_payload
	case DEBUG_PRINT_TABLEDEF_INDEX_COLDEFS_AFTER_NK:
		listcol = tabledef.Td_coldefs[len(tabledef.Td_nk):]
	default:
		panic("impossible")
	}

	for _, coldef := range listcol {
		margin.set_string(margin_idx, "|")
		display_datatype := rsql.Datatype_shortdef(coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
		display_NULL := "NULL"
		if coldef.Cd_NOT_NULL_flag == true {
			display_NULL = "NOT NULL"
		}
		switch coldef.Cd_autonum {
		case rsql.CD_AUTONUM_ROWID:
			other_info = "AUTONUM_ROWID"
		case rsql.CD_AUTONUM_IDENTITY:
			other_info = "AUTONUM_IDENTITY"
			if tabledef.Td_type == rsql.TD_TYPE_BASE_TABLE {
				other_info = fmt.Sprintf("AUTONUM_IDENTITY(%d, %d)", identity_seed, identity_increment)
			}
		default:
			other_info = coldef.Cd_collation
		}

		if option != DEBUG_PRINT_TABLEDEF_INDEX_COLDEFS_AFTER_NK {
			context.Send_MESSAGE_or_panic("%s \033[34;1m%-20s %3d:\033[0m %-17s %-10s %-s", *margin, coldef.Cd_colname, coldef.Cd_base_seqno, display_datatype, display_NULL, other_info)
		} else {
			context.Send_MESSAGE_or_panic("%s \033[32m%-20s %3d:\033[0m %-17s %-10s %-s", *margin, coldef.Cd_colname, coldef.Cd_base_seqno, display_datatype, display_NULL, other_info)
		}
	}
}

func (margin *Margin_buffer_t) debug_print_Tabledef(context *rsql.Context, margin_idx int, tabledef *rsql.Tabledef, identity_seed int64, identity_increment int64) {

	switch tabledef.Td_type {
	case rsql.TD_TYPE_BASE_TABLE:
		assert(tabledef.Td_cluster_type == rsql.TD_CLUSTERED)

		/* walk columns */

		margin.set_string(margin_idx, "+")
		context.Send_MESSAGE_or_panic("%s------------------------- BASE TABLE --------------------------", *margin)

		margin.debug_print_Tabledef_columns(context, margin_idx, tabledef, identity_seed, identity_increment, DEBUG_PRINT_TABLEDEF_COLDEFS)
		context.Send_MESSAGE_or_panic("%s", *margin)
		context.Send_MESSAGE_or_panic("%s--- Native key (%s) ---", *margin, tabledef.Td_index_type)
		margin.debug_print_Tabledef_columns(context, margin_idx, tabledef, identity_seed, identity_increment, DEBUG_PRINT_TABLEDEF_NK)

	case rsql.TD_TYPE_INDEX_TABLE:
		assert(tabledef.Td_cluster_type == rsql.TD_NONCLUSTERED)

		/* walk columns */

		margin.set_string(margin_idx, "+")
		context.Send_MESSAGE_or_panic("%s-------------------------- INDEX TABLE (%s) -------------------", *margin, tabledef.Td_index_type)

		for i, c := range tabledef.Td_nk {
			if c != tabledef.Td_coldefs[i] {
				log.Panicf("Td_nk[%d] not equal to Td_coldefs[%d] for index %s in table %d", i, i, tabledef.Td_index_name, tabledef.Td_base_gtblid)
			}
		}

		margin.debug_print_Tabledef_columns(context, margin_idx, tabledef, identity_seed, identity_increment, DEBUG_PRINT_TABLEDEF_NK)
		margin.debug_print_Tabledef_columns(context, margin_idx, tabledef, identity_seed, identity_increment, DEBUG_PRINT_TABLEDEF_INDEX_COLDEFS_AFTER_NK)

	default:
		panic("impossible")
	}

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	other_info := ""
	if tabledef.Td_durability != rsql.DURABILITY_PERMANENT {
		other_info = tabledef.Td_durability.String()
	}
	if tabledef.Td_status != rsql.TD_STATUS_VALID {
		if len(other_info) > 0 {
			other_info += ", "
		}
		other_info += tabledef.Td_status.String()
	}

	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m %s", *margin, "TABLEDEF", tabledef.Td_index_name)
	if len(other_info) > 0 {
		margin.set_string(margin_idx, "|")
		context.Send_MESSAGE_or_panic("%s\033[31m  %s\033[0m", *margin, other_info)
	}

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_TABLE(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_TABLE) {
	assert(tok.Tok_category == TOK_STMT_ALTER_TABLE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")

	switch {
	case tok.At_change_table_name != "":
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s    (change_table_name: %s)", *margin, "TOK_STMT_ALTER_TABLE", tok.At_table_qname.Database_name, tok.At_table_qname.Schema_name, tok.At_table_qname.Object_name, tok.At_change_table_name)

	case tok.At_column_name != "":
		switch {
		case tok.At_change_column_name != "":
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s    (column %s new name: %s)", *margin, "TOK_STMT_ALTER_TABLE", tok.At_table_qname.Database_name, tok.At_table_qname.Schema_name, tok.At_table_qname.Object_name, tok.At_column_name, tok.At_change_column_name)

		case tok.At_change_column_nullability != 0:
			switch tok.At_change_column_nullability {
			case dict.ALTER_COLUMN_NULL:
				context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s    (column %s: %s)", *margin, "TOK_STMT_ALTER_TABLE", tok.At_table_qname.Database_name, tok.At_table_qname.Schema_name, tok.At_table_qname.Object_name, tok.At_column_name, "NULL")

			case dict.ALTER_COLUMN_NOT_NULL:
				context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s    (column %s: %s)", *margin, "TOK_STMT_ALTER_TABLE", tok.At_table_qname.Database_name, tok.At_table_qname.Schema_name, tok.At_table_qname.Object_name, tok.At_column_name, "NOT NULL")

				panic("impossible")
			}

		default:
			panic("impossible")
		}

	default:
		panic("impossible")
	}

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DROP_TABLE(context *rsql.Context, margin_idx int, tok *Token_stmt_DROP_TABLE) {
	assert(tok.Tok_category == TOK_STMT_DROP_TABLE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s", *margin, "TOK_STMT_DROP_TABLE", tok.Dt_table_qname.Database_name, tok.Dt_table_qname.Schema_name, tok.Dt_table_qname.Object_name)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_CREATE_INDEX(context *rsql.Context, margin_idx int, tok *Token_stmt_CREATE_INDEX) {
	assert(tok.Tok_category == TOK_STMT_CREATE_INDEX)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s.%s(%s)    (%s, %s)", *margin, "TOK_STMT_CREATE_INDEX", tok.Ci_table_qname.Database_name, tok.Ci_table_qname.Schema_name, tok.Ci_table_qname.Object_name, tok.Ci_index_name, strings.Join(tok.Ci_colname_list, ","), tok.Ci_index_type, tok.Ci_cluster_type)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_ALTER_INDEX(context *rsql.Context, margin_idx int, tok *Token_stmt_ALTER_INDEX) {
	assert(tok.Tok_category == TOK_STMT_ALTER_INDEX)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s.%s    (change_index_name: %s)", *margin, "TOK_STMT_ALTER_INDEX", tok.Ai_table_qname.Database_name, tok.Ai_table_qname.Schema_name, tok.Ai_table_qname.Object_name, tok.Ai_index_name, tok.Ai_change_index_name)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DROP_INDEX(context *rsql.Context, margin_idx int, tok *Token_stmt_DROP_INDEX) {
	assert(tok.Tok_category == TOK_STMT_DROP_INDEX)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s.%s", *margin, "TOK_STMT_DROP_INDEX", tok.Di_table_qname.Database_name, tok.Di_table_qname.Schema_name, tok.Di_table_qname.Object_name, tok.Di_index_name)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_INSERT_INTO(context *rsql.Context, margin_idx int, tok *Token_stmt_INSERT_INTO) {
	assert(tok.Tok_category == TOK_STMT_INSERT_INTO)

	/* walk child */

	switch tok.Ins_instruction_code {
	case rsql.INSTR_STMT_INSERT_VALUES:
		for _, values := range tok.Ins_values_array {
			margin.set_string(margin_idx+3, "+")
			context.Send_MESSAGE_or_panic("%s------------\033[1;33;44m%s\033[0m--------", *margin, "VALUES")
			margin.set_string(margin_idx+3, "|")

			margin.debug_print_VALUES(context, margin_idx+6, tok.Ins_listcol, values)

			margin.set_string(margin_idx+3, "|")
			context.Send_MESSAGE_or_panic("%s", *margin)
		}

	case rsql.INSTR_STMT_INSERT_SELECT:
		margin.set_string(margin_idx, ".")
		margin.debug_print_Xtable(context, margin_idx+3, tok.Ins_xtable, "")

	default:
		panic("impossible")
	}

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s", *margin, "TOK_STMT_INSERT_INTO", tok.Ins_table_qname.Database_name, tok.Ins_table_qname.Schema_name, tok.Ins_table_qname.Object_name)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_VALUES(context *rsql.Context, margin_idx int, listcol []string, values []*Token_primary) {

	for k, val := range values {

		//margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
		margin.debug_print_Token_primary(context, margin_idx+4, val)

		margin.set_string(margin_idx, "+")
		context.Send_MESSAGE_or_panic("%s--\033[1;36m%s\033[0m", *margin, listcol[k])
		margin.set_string(margin_idx, "|")
	}
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_BULK_INSERT(context *rsql.Context, margin_idx int, tok *Token_stmt_BULK_INSERT) {
	assert(tok.Tok_category == TOK_STMT_BULK_INSERT)

	/* walk child */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	margin.debug_print_Token_primary(context, margin_idx+3, tok.Bi_syslanguage)

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s FROM '%s' (codepage: \"%s\", rowterminator: %s, fieldterminator: %s, firstrow: %d, lastrow: %d, keepidentity: %t, rtrim: %t)", *margin, "TOK_STMT_BULK_INSERT", tok.Bi_table_qname.Database_name, tok.Bi_table_qname.Schema_name, tok.Bi_table_qname.Object_name, tok.Bi_filename, tok.Bi_opt_codepage, strconv.Quote(tok.Bi_opt_rowterminator), strconv.Quote(tok.Bi_opt_fieldterminator), tok.Bi_opt_firstrow, tok.Bi_opt_lastrow, tok.Bi_opt_keepidentity, tok.Bi_opt_rtrim)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_BULK_EXPORT(context *rsql.Context, margin_idx int, tok *Token_stmt_BULK_EXPORT) {
	assert(tok.Tok_category == TOK_STMT_BULK_EXPORT)

	/* walk child */

	margin.set_string(margin_idx, ".")
	margin.debug_print_Xtable(context, margin_idx+3, tok.Be_xtable, "")

	/* +--------- headline */

	margin.set_string(margin_idx, "+") // display a + sign at left side of the data block.
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m TO '%s' (codepage: \"%s\", rowterminator: %s, fieldterminator: %s, date_format: %s, time_format: %s, datetime_format: %s)", *margin, "TOK_STMT_BULK_EXPORT", tok.Be_filename, tok.Be_opt_codepage, strconv.Quote(tok.Be_opt_rowterminator), strconv.Quote(tok.Be_opt_fieldterminator), tok.Be_opt_date_format, tok.Be_opt_time_format, tok.Be_opt_datetime_format)

	/* detail lines */

	margin.set_string(margin_idx, ".") // display a dot at left side of the data block.
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SELECT_or_UNION(context *rsql.Context, margin_idx int, tok *Token_stmt_SELECT_or_UNION) {
	assert(tok.Tok_category == TOK_STMT_SELECT_OR_UNION)

	/* walk child */

	margin.set_string(margin_idx, ".")
	margin.debug_print_Xtable(context, margin_idx+3, tok.Su_xtable, "")

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_SELECT_OR_UNION")

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Xtable_table(context *rsql.Context, margin_idx int, xtable_table *Xtable_table) {

	/* print columns */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s------------------------- %s --------------------------", *margin, xtable_table.Tbl_qname)

	margin.set_string(margin_idx, "|")
	base_tabledef := xtable_table.Tbl_cursor.Base_tabledef
	base_fields := xtable_table.Tbl_cursor.Base_fields

	for i, coldef := range base_tabledef.Td_coldefs {
		display_datatype := rsql.Datatype_shortdef(coldef.Cd_datatype, coldef.Cd_precision, coldef.Cd_scale, coldef.Cd_fixlen_flag)
		display_NULL := "NULL"
		if coldef.Cd_NOT_NULL_flag == true {
			display_NULL = "NOT NULL"
		}
		other_info := ""
		switch coldef.Cd_autonum {
		case rsql.CD_AUTONUM_ROWID:
			other_info = "AUTONUM_ROWID"
		case rsql.CD_AUTONUM_IDENTITY:
			other_info = "AUTONUM_IDENTITY"
		default:
			other_info = coldef.Cd_collation
		}

		dataslot_address := ""
		if base_fields[i] != nil {
			dataslot_address = data.Dataslot_address(base_fields[i])
		}

		context.Send_MESSAGE_or_panic("%s \033[34;1m%-20s %3d:\033[0m %-17s %-10s %-20s [%s]", *margin, coldef.Cd_colname, coldef.Cd_base_seqno, display_datatype, display_NULL, other_info, dataslot_address)
	}

	/* +--------- headline */

	exposed_name := xtable_table.Tbl_alias
	if exposed_name == "" {
		exposed_name = xtable_table.Tbl_qname.Object_name
	}

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m %s", *margin, "XTABLE_TABLE", exposed_name)

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Xtable_join(context *rsql.Context, margin_idx int, xtable_join *Xtable_join) {

	/* walk left xtable, right_xtable, and xtable_join */

	margin.debug_print_Xtable(context, margin_idx+3, xtable_join.Jn_left, "")
	margin.debug_print_Xtable(context, margin_idx+3, xtable_join.Jn_right, "")

	if xtable_join.Jn_on != nil {
		context.Send_MESSAGE_or_panic("%s------------------ ON CLAUSE ------------------", *margin)

		margin.debug_print_Token_primary(context, margin_idx+3, xtable_join.Jn_on)
		context.Send_MESSAGE_or_panic("%s", *margin)
	}

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m", *margin, "XTABLE_JOIN")

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Xtable_single(context *rsql.Context, margin_idx int, xtable_single *Xtable_single) {

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m", *margin, "XTABLE_SINGLE")

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Xtable_from(context *rsql.Context, margin_idx int, xtable_from *Xtable_from) {

	/* walk xtable tree */

	assert(len(xtable_from.Fr_list) == 1)

	margin.debug_print_Xtable(context, margin_idx+3, xtable_from.Fr_list[0], "")

	if xtable_from.Fr_where != nil {
		context.Send_MESSAGE_or_panic("%s------------------ WHERE CLAUSE ------------------", *margin)

		margin.debug_print_Token_primary(context, margin_idx+3, xtable_from.Fr_where)
		context.Send_MESSAGE_or_panic("%s", *margin)
	}

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m", *margin, "XTABLE_FROM")

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Xtable_select(context *rsql.Context, margin_idx int, xtable_select *Xtable_select, message string) {

	/* if grouping SELECT */

	if xtable_select.Sel_grouping_flag == true {

		for _, tok := range xtable_select.Sel_groupby_expressions {
			margin.debug_print_Token_primary(context, margin_idx+3, tok)
		}
		context.Send_MESSAGE_or_panic("%s", *margin)

		context.Send_MESSAGE_or_panic("%s------------------ aggregate functions ------------------", *margin)

		for _, tok := range xtable_select.Sel_grouptable_aggrfuncs_to_inject {
			margin.debug_print_Token_primary(context, margin_idx+3, tok)
		}
		context.Send_MESSAGE_or_panic("%s", *margin)

		margin.debug_print_Xtable_from(context, margin_idx+3, xtable_select.Sel_grouptable_feeding_from)
	}

	context.Send_MESSAGE_or_panic("%s", *margin)

	/* walk FROM */

	margin.debug_print_Xtable_from(context, margin_idx+3, xtable_select.Sel_from)

	/* print column expressions */

	context.Send_MESSAGE_or_panic("%s------------------ SELECT COLUMNS ------------------", *margin)

	for _, column := range xtable_select.Sel_columns {
		margin.debug_print_Token_primary(context, margin_idx+3, column.Col_expression)

		colname := column.Col_label
		if colname == "" && column.Col_expression.Prim_type == TOK_PRIM_IDENTIFIER {
			colname = column.Col_expression.Prim_name
		}

		column_facade := xtable_select.Sel_cursor.Cols_facade[column.Col_no]

		context.Send_MESSAGE_or_panic("%s \033[34;1mcol_name: %s\033[0m --> [%s]", *margin, colname, data.Dataslot_address(column_facade))
	}

	context.Send_MESSAGE_or_panic("%s", *margin)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m %s%10s", *margin, "XTABLE_SELECT", xtable_select.Sel_alias, message)

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Xtable_union(context *rsql.Context, margin_idx int, xtable_union *Xtable_union, message string) {

	/* walk members */

	for _, item := range xtable_union.Un_members {
		message := ""
		if item.No_duplicate == true {
			message = "(no row duplicate)"
		}
		margin.debug_print_Xtable(context, margin_idx+3, item.Xtbl, message)
	}

	/* print column expressions */

	context.Send_MESSAGE_or_panic("%s------------------ UNION COLUMNS ------------------", *margin)

	xtable := xtable_union.Un_members[0].Xtbl // retrieve first SELECT

	for xtable.Type() != XT_SELECT {
		rsql.Assert(xtable.Type() == XT_UNION)

		xtable = xtable.(*Xtable_union).Un_members[0].Xtbl
	}

	xtable_select := xtable.(*Xtable_select)

	for i, tok_equalized := range xtable_union.Un_equalized_row {
		margin.debug_print_Token_primary(context, margin_idx+3, tok_equalized)

		column := xtable_select.Sel_columns[i]
		colname := column.Col_label
		if colname == "" && column.Col_expression.Prim_type == TOK_PRIM_IDENTIFIER {
			colname = column.Col_expression.Prim_name
		}

		context.Send_MESSAGE_or_panic("%s \033[34;1mcol_name: %s\033[0m", *margin, colname)
	}

	context.Send_MESSAGE_or_panic("%s", *margin)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[33m%s\033[0m %s%10s", *margin, "XTABLE_UNION", xtable_union.Un_alias, message)

	/* detail lines */

	margin.set_string(margin_idx, "|")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

// debug_print_Xtable prints the tree of Xtables.
//
// Argument message is used by debug_print_Xtable_union(), with messge "(no row duplicate)" for UNIONs that remove duplicates.
//
func (margin *Margin_buffer_t) debug_print_Xtable(context *rsql.Context, margin_idx int, xtable Xtable, message string) {

	switch xtable.Type() {
	case XT_TABLE:
		margin.debug_print_Xtable_table(context, margin_idx, xtable.(*Xtable_table))

	case XT_JOIN:
		margin.debug_print_Xtable_join(context, margin_idx, xtable.(*Xtable_join))

	case XT_SINGLE:
		margin.debug_print_Xtable_single(context, margin_idx, xtable.(*Xtable_single))

	case XT_FROM:
		margin.debug_print_Xtable_from(context, margin_idx, xtable.(*Xtable_from))

	case XT_SELECT:
		margin.debug_print_Xtable_select(context, margin_idx, xtable.(*Xtable_select), message)

	case XT_UNION:
		margin.debug_print_Xtable_union(context, margin_idx, xtable.(*Xtable_union), message)

	default:
		panic("impossible")
	}
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DELETE(context *rsql.Context, margin_idx int, tok *Token_stmt_DELETE) {
	assert(tok.Tok_category == TOK_STMT_DELETE)

	/* walk child */

	margin.set_string(margin_idx, ".")
	margin.debug_print_Xtable_from(context, margin_idx+3, tok.De_from)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	if tok.De_target_table.Tbl_alias != "" {
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s (%s.%s.%s)", *margin, "TOK_STMT_DELETE", tok.De_target_table.Tbl_alias, tok.De_target_table.Tbl_qname.Database_name, tok.De_target_table.Tbl_qname.Schema_name, tok.De_target_table.Tbl_qname.Object_name)
	} else {
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s", *margin, "TOK_STMT_DELETE", tok.De_target_table.Tbl_qname.Database_name, tok.De_target_table.Tbl_qname.Schema_name, tok.De_target_table.Tbl_qname.Object_name)
	}

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_UPDATE(context *rsql.Context, margin_idx int, tok *Token_stmt_UPDATE) {
	assert(tok.Tok_category == TOK_STMT_UPDATE)

	/* walk FROM */

	margin.set_string(margin_idx, ".")
	margin.debug_print_Xtable_from(context, margin_idx+3, tok.Ud_from)

	/* print SET column expressions */

	context.Send_MESSAGE_or_panic("%s------------------ SET clauses ------------------", *margin)

	for _, set_clause := range tok.Ud_set_list {
		margin.debug_print_Token_primary(context, margin_idx+3, set_clause.St_expr)

		context.Send_MESSAGE_or_panic("%s \033[34;1mcol_name: %s\033[0m", *margin, set_clause.St_colname)
	}

	context.Send_MESSAGE_or_panic("%s", *margin)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	if tok.Ud_target_table.Tbl_alias != "" {
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s (%s.%s.%s)", *margin, "TOK_STMT_UPDATE", tok.Ud_target_table.Tbl_alias, tok.Ud_target_table.Tbl_qname.Database_name, tok.Ud_target_table.Tbl_qname.Schema_name, tok.Ud_target_table.Tbl_qname.Object_name)
	} else {
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s", *margin, "TOK_STMT_UPDATE", tok.Ud_target_table.Tbl_qname.Database_name, tok.Ud_target_table.Tbl_qname.Schema_name, tok.Ud_target_table.Tbl_qname.Object_name)
	}

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_TRUNCATE_TABLE(context *rsql.Context, margin_idx int, tok *Token_stmt_TRUNCATE_TABLE) {
	assert(tok.Tok_category == TOK_STMT_TRUNCATE_TABLE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s     shrink file: %t", *margin, "TOK_STMT_TRUNCATE_TABLE", tok.Tt_table_qname.Database_name, tok.Tt_table_qname.Schema_name, tok.Tt_table_qname.Object_name, tok.Tt_shrink_file)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHRINK_TABLE(context *rsql.Context, margin_idx int, tok *Token_stmt_SHRINK_TABLE) {
	assert(tok.Tok_category == TOK_STMT_SHRINK_TABLE)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s.%s.%s", *margin, "TOK_STMT_SHRINK_TABLE", tok.Sk_table_qname.Database_name, tok.Sk_table_qname.Schema_name, tok.Sk_table_qname.Object_name)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHOW_COLLATIONS(context *rsql.Context, margin_idx int, tok *Token_stmt_SHOW_COLLATIONS) {
	assert(tok.Tok_category == TOK_STMT_SHOW_COLLATIONS)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_SHOW_COLLATIONS")

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHOW_LANGUAGES(context *rsql.Context, margin_idx int, tok *Token_stmt_SHOW_LANGUAGES) {
	assert(tok.Tok_category == TOK_STMT_SHOW_LANGUAGES)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_SHOW_LANGUAGES")

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHOW_LOCKS(context *rsql.Context, margin_idx int, tok *Token_stmt_SHOW_LOCKS) {
	assert(tok.Tok_category == TOK_STMT_SHOW_LOCKS)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_SHOW_LOCKS")

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHOW_WORKERS(context *rsql.Context, margin_idx int, tok *Token_stmt_SHOW_WORKERS) {
	assert(tok.Tok_category == TOK_STMT_SHOW_WORKERS)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_SHOW_WORKERS")

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHOW_INFO(context *rsql.Context, margin_idx int, tok *Token_stmt_SHOW_INFO) {
	assert(tok.Tok_category == TOK_STMT_SHOW_INFO)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m", *margin, "TOK_STMT_SHOW_INFO")

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHOW(context *rsql.Context, margin_idx int, tok *Token_stmt_SHOW) {
	var (
		s        string
		showtype string
		target   string
		dbstring string
	)

	assert(tok.Tok_category == TOK_STMT_SHOW)

	if tok.Sh_option_ALL {
		s += "ALL "
	}

	if tok.Sh_option_PERM {
		s += "PERM "
	}

	s += tok.Sh_output_type.String()

	if tok.Sh_database_name != "" {
		dbstring = " IN " + tok.Sh_database_name
	}

	switch tok.Sh_instruction_code {
	case rsql.INSTR_STMT_SHOW_PARAMETER:
		/* +--------- headline */

		showtype = "PARAMETER"
		margin.set_string(margin_idx, "+")
		if tok.Sh_LIKE == false {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s %s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_object_name)
		} else {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s LIKE '%s'", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_LIKE_pattern)
		}

	case rsql.INSTR_STMT_SHOW_LOGIN:
		/* +--------- headline */

		showtype = "LOGIN"
		margin.set_string(margin_idx, "+")
		if tok.Sh_LIKE == false {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s %s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_object_name)
		} else {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s LIKE '%s'", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_LIKE_pattern)
		}

	case rsql.INSTR_STMT_SHOW_DATABASE:
		/* +--------- headline */

		showtype = "DATABASE"
		margin.set_string(margin_idx, "+")
		if tok.Sh_LIKE == false {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s %s%s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_database_name, dbstring)
		} else {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s LIKE '%s'%s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_LIKE_pattern, dbstring)
		}

	case rsql.INSTR_STMT_SHOW_USER:
		/* +--------- headline */

		showtype = "USER"
		margin.set_string(margin_idx, "+")
		if tok.Sh_LIKE == false {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s %s%s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_object_name, dbstring)
		} else {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s LIKE '%s'%s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_LIKE_pattern, dbstring)
		}

	case rsql.INSTR_STMT_SHOW_ROLE:
		/* +--------- headline */

		showtype = "ROLE"
		margin.set_string(margin_idx, "+")
		if tok.Sh_LIKE == false {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s %s%s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_object_name, dbstring)
		} else {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s LIKE '%s'%s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_LIKE_pattern, dbstring)
		}

	case rsql.INSTR_STMT_SHOW_TABLE:
		/* +--------- headline */

		showtype = "TABLE"
		margin.set_string(margin_idx, "+")
		if tok.Sh_LIKE == false {
			target = debug_concat_name_with_dots(tok.Sh_database_name, tok.Sh_schema_name, tok.Sh_object_name)
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s %s%s", *margin, "TOK_STMT_SHOW", s, showtype, target, dbstring)
		} else {
			context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s LIKE '%s'%s", *margin, "TOK_STMT_SHOW", s, showtype, tok.Sh_LIKE_pattern, dbstring)
		}
	}

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SLEEP(context *rsql.Context, margin_idx int, tok *Token_stmt_SLEEP) {
	assert(tok.Tok_category == TOK_STMT_SLEEP)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %.6f", *margin, "TOK_STMT_SLEEP", tok.Sl_duration)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_SHUTDOWN(context *rsql.Context, margin_idx int, tok *Token_stmt_SHUTDOWN) {
	assert(tok.Tok_category == TOK_STMT_SHUTDOWN)

	/* +--------- headline */

	margin.set_string(margin_idx, "+")
	nowait := ""
	if tok.Sd_nowait {
		nowait = "NOWAIT"
	}
	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s", *margin, "TOK_STMT_SHUTDOWN", nowait)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_DUMP(context *rsql.Context, margin_idx int, tok *Token_stmt_DUMP) {

	assert(tok.Tok_category == TOK_STMT_DUMP)

	switch tok.Dp_instruction_code {
	case rsql.INSTR_STMT_DUMP_PARAMETERS:
		dumptype := "PARAMETERS"
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s TO '%s'", *margin, "TOK_STMT_DUMP", dumptype, tok.Dp_filename)

	case rsql.INSTR_STMT_DUMP_LOGINS:
		dumptype := "LOGINS"
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s TO '%s'", *margin, "TOK_STMT_DUMP", dumptype, tok.Dp_filename)

	case rsql.INSTR_STMT_DUMP_DATABASE:
		dumptype := "DATABASE"
		option := ""
		if tok.Dp_option_DDL_ONLY {
			option = "DDL_ONLY"
		}
		context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s %s %s TO '%s'", *margin, "TOK_STMT_DUMP", dumptype, tok.Dp_database_name, option, tok.Dp_filename)

	default:
		panic("impossible")
	}

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_BACKUP(context *rsql.Context, margin_idx int, tok *Token_stmt_BACKUP) {

	assert(tok.Tok_category == TOK_STMT_BACKUP)

	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s TO '%s'", *margin, "TOK_STMT_BACKUP", tok.Bk_database_name, tok.Bk_filename)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func (margin *Margin_buffer_t) debug_print_Token_stmt_RESTORE(context *rsql.Context, margin_idx int, tok *Token_stmt_RESTORE) {

	assert(tok.Tok_category == TOK_STMT_RESTORE)

	context.Send_MESSAGE_or_panic("%s\033[1;33;45m%s\033[0m %s FROM '%s'", *margin, "TOK_STMT_RESTORE", tok.Rt_database_name, tok.Rt_filename)

	/* detail lines */

	margin.set_string(margin_idx, ".")
	context.Send_MESSAGE_or_panic("%s", *margin)
}

func debug_concat_name_with_dots(parts ...string) string {
	var (
		s string
		i int
	)

	for i = 0; i < len(parts); i++ {
		if parts[i] != "" {
			break
		}
	}

	for k := i; k < len(parts); k++ {
		s += "." + parts[k]
	}

	if len(s) > 0 && s[0] == '.' {
		s = s[1:]
	}

	return s
}

// ########### THIS FUNCTION IS JUST FOR MY PRIVATE USE, FOR DEBUGGING. IT SHOULD NOT BE USED FOR ANY OTHER PURPOSE !!!
//
func My_debug_print_Token_primary_tree(tok *Token_primary, i int) {

	assert(tok.Tok_category == TOK_PRIMARY)

	if tok.Prim_left != nil {
		My_debug_print_Token_primary_tree(tok.Prim_left, i+1)
	}

	if tok.Prim_right != nil {
		My_debug_print_Token_primary_tree(tok.Prim_right, i+1)
	}

	fmt.Printf("%d %s\n", i, tok.Prim_name)
	my_debug_print_my_Dataslot(tok.Prim_dataslot)

}

// ########### THIS FUNCTION IS JUST FOR MY PRIVATE USE, FOR DEBUGGING. IT SHOULD NOT BE USED FOR ANY OTHER PURPOSE !!!
//
func my_debug_print_my_Dataslot(dataslot rsql.IDataslot) {
	var (
		datatype         rsql.Datatype_t
		data_precision   uint16
		data_scale       uint16
		data_fixlen_flag bool
		datatype_display string
	)

	datatype = dataslot.Datatype()

	switch datatype {
	case rsql.DATATYPE_VARBINARY:
		data_precision = dataslot.(*data.VARBINARY).Data_precision

	case rsql.DATATYPE_VARCHAR:
		data_precision = dataslot.(*data.VARCHAR).Data_precision
		data_fixlen_flag = dataslot.(*data.VARCHAR).Data_fixlen_flag

	case rsql.DATATYPE_MONEY:
		data_precision = 0
		data_scale = 0

	case rsql.DATATYPE_NUMERIC:
		data_precision = dataslot.(*data.NUMERIC).Data_precision
		data_scale = dataslot.(*data.NUMERIC).Data_scale
	}

	datatype_display = rsql.Datatype_shortdef(datatype, data_precision, data_scale, data_fixlen_flag)

	if dataslot.Kind() == rsql.KIND_RO_LEAF {
		datatype_display += "  \033[32m(RO_LEAF)\033[0m"
	}

	fmt.Printf("    \033[0mms_data [%p]: %s\n", dataslot, datatype_display)
}
