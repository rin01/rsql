package ast

import (
	"fmt"
	//	"io"
	//	"os"
	"testing"

	"rsql"
	"rsql/lang"
)

func Test_parser_eat_qualified_name(t *testing.T) {

	var (
		rsql_err       *rsql.Error
		parser         *Parser
		err_message_id rsql.Message_id_t
	)

	samples := []struct {
		spl_input                    string
		spl_expected_prefix          []string
		spl_expected_np_prefix_parts int
		spl_expected_curr_lexeme     string
		spl_expected_message_id      rsql.Message_id_t
	}{
		{"mydb.dbo.mytable.col", []string{"mydb", "dbo", "mytable"}, 3, "col", 0},
		{"   mydb.dbo.mytable.col   ", []string{"mydb", "dbo", "mytable"}, 3, "col", 0},
		{"mydb.dbo.mytable.col,", []string{"mydb", "dbo", "mytable"}, 3, "col", 0},
		{"mydb.dbo.mytable.col*", []string{"mydb", "dbo", "mytable"}, 3, "col", 0},
		{"  MYDB.  DBO  .  mytable  .col 123", []string{"mydb", "dbo", "mytable"}, 3, "col", 0},
		{"mydb.dbo.mytable", []string{"", "mydb", "dbo"}, 2, "mytable", 0},
		{"dbo.mytable", []string{"", "", "dbo"}, 1, "mytable", 0},
		{"mytable", []string{"", "", ""}, 0, "mytable", 0},
		{"mytable*", []string{"", "", ""}, 0, "mytable", 0},
		{".mytable", []string{"", "", ""}, 1, "mytable", 0},
		{"mytable.", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"dbo.mytable.", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"..mytable.", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"..mytable.5", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_MALFORMED},
		{"..mytable.   5", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"..mytable   .   5", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"...mytable.", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_TOO_MANY_PARTS},
		{"mydb.dbo.mytable.col.asdf", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_TOO_MANY_PARTS},
		{"mydb.dbo.mytable.col.", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_TOO_MANY_PARTS},
		{"mydb..mytable.col,", []string{"mydb", "", "mytable"}, 3, "col", 0},
		{"mydb.dbo..col,", []string{"mydb", "dbo", ""}, 3, "col", 0},
		{"..mytable.col,", []string{"", "", "mytable"}, 3, "col", 0},
		{"...col,", []string{"", "", ""}, 3, "col", 0},
		{"...   ", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"  .  .  .   ", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"  .  .  .  . ", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"mydb.dbo.mytable.*,", []string{"mydb", "dbo", "mytable"}, 3, "*", 0},
		{"dbo.mytable.*,", []string{"", "dbo", "mytable"}, 2, "*", 0},
		{"dbo.mytable.*.", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_TOO_MANY_PARTS},
		{"dbo.mytable.*.asd", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_TOO_MANY_PARTS},
		{"..*.asd", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_TOO_MANY_PARTS},
		{".!.mytable", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"bla..4.mytable", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"bla.4.mytable", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_MALFORMED},
		{"bla.asdf.mytable.!", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"bla.asdf.mytable..5", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_IDENTPART_EXPECTED},
		{"bla.asdf.mytable.5", nil, 0, "", rsql.ERROR_INVALID_SYNTAX_QUALIFIED_NAME_MALFORMED},
	}

	// test all samples

	for i := range samples {

		// init Lexer

		sample := samples[i]

		txt := sample.spl_input

		if parser, rsql_err = New_parser("en_cs_as", "xxx"); rsql_err != nil {
			t.Fatalf("%v", rsql_err)
		}

		rsql_err = parser.Attach_batch([]byte(txt))
		if rsql_err != nil {
			t.Fatalf("<%s>: Attach_batch() should not fail", txt)
		}

		//		if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil {
		//			t.Fatal("Eat_next_lexeme() should not fail")
		//		}

		name_3, name_2, name_1, nb_prefix_parts, rsql_err := parser.Eat_prefix()

		//=== if error occurred, check that it is the expected one ===

		if rsql_err != nil {
			err_message_id = rsql_err.Message_id

			if err_message_id != sample.spl_expected_message_id {
				t.Fatalf("<%s>: Got error %v, expected %v", sample.spl_input, err_message_id, sample.spl_expected_message_id)
			}
			continue // continue testing with next sample
		}

		//=== no error. Check result. ===

		if sample.spl_expected_message_id != 0 { // check that we expect no error
			t.Fatalf("<%s>: Got no error, expected %v", sample.spl_input, sample.spl_expected_message_id)
		}

		if sample.spl_expected_np_prefix_parts != nb_prefix_parts {
			t.Fatalf("<%s>: Got no error, expected %d", sample.spl_input, sample.spl_expected_np_prefix_parts)
		}

		if sample.spl_expected_curr_lexeme != parser.Current_lexeme.Lex_word {
			t.Fatalf("<%s>: Got no error, expected %s", sample.spl_input, sample.spl_expected_curr_lexeme)
		}

		p1 := sample.spl_expected_prefix[2] // lexeme_1

		if name_1 != p1 {
			t.Fatalf("<%s>: Got %s, expected %s", sample.spl_input, name_1, p1)
		}

		p2 := sample.spl_expected_prefix[1] // lexeme_2

		if name_2 != p2 {
			t.Fatalf("<%s>: Got %s, expected %s", sample.spl_input, name_2, p2)
		}

		p3 := sample.spl_expected_prefix[0] // lexeme_3

		if name_3 != p3 {
			t.Fatalf("<%s>: Got %s, expected %s", sample.spl_input, name_3, p3)
		}

	}

}

func Test_parser_eat_expression(t *testing.T) {

	var (
		parser         *Parser
		tok            *Token_primary
		rsql_err       *rsql.Error
		err_message_id rsql.Message_id_t
	)

	samples := []struct {
		spl_input               string
		spl_expected_message_id rsql.Message_id_t
	}{
		{"10", 0},
		{"123 + 456", 0},
		{"'asdf' + 123 - @aaa * 4 + 5*2 ", 0},
		{"'asdf' + 123 - @aaa * (4 + 5*2)", 0},
		{"'asdf' + 123 - @aaa * ((4 + (5-@aa)*2))", 0},
		{"mydb.dbo.mytable.col", 0},
	}

	// test all samples

	for i := range samples {

		// init Lexer

		sample := samples[i]

		txt := sample.spl_input

		if parser, rsql_err = New_parser("en_cs_as", "xxx"); rsql_err != nil {
			t.Fatalf("%v", rsql_err)
		}

		rsql_err = parser.Attach_batch([]byte(txt))
		if rsql_err != nil {
			t.Fatalf("<%s>: Attach_batch() should not fail", txt)
		}

		parser.xtable_object_stack = append(parser.xtable_object_stack, nil) // to allow column identifier

		tok, rsql_err = parser.Eat_expression()

		//=== if error occurred, check that it is the expected one ===

		if rsql_err != nil {
			err_message_id = rsql_err.Message_id
			fmt.Println(rsql_err)

			if err_message_id != sample.spl_expected_message_id {
				t.Fatalf("<%s>: Got error %v, expected %v", sample.spl_input, err_message_id, sample.spl_expected_message_id)
			}
			continue // continue testing with next sample
		}

		//=== no error. ===

		if tok == nil {
			t.Fatalf("<%s>: Got nil token", sample.spl_input)
		}
	}

}

func Test_parser_Fprintf_ERR(t *testing.T) {

	var (
		rsql_err       *rsql.Error
		parser         *Parser
		err_message_id rsql.Message_id_t
	)

	samples := []struct {
		spl_input               string
		spl_expected_message_id rsql.Message_id_t
	}{
		//	{"22 + 33 is null + 44", 0},
		//		{"123 + 456", 0},
		//		{"'asdf' + 123 - @aaa * 4 + 5*-2 + -2 + sin(99) +@@LANguage +0xabc * ( 1 + NULL) + datEadd(YYYY,   99) + cast(88 As varchar(6))", 0},
		//		{"'asdf' + 123 - @aaa * 4 + 5*-2 + -2 + sin(99) +@@LANguage +0xabc * ( 1 + NULL) + bof(5,6)", 0}, // bof est un ident et (5,6) sont ignorés, ce qui est normal
		//				{"4 + - - - -2", 0},
		//		{"'asdf' + 123 - @aaa * (4 + 5*2)", 0},
		//		{"'asdf' + 123 - @aaa * ((4 + (5-@aa)*2))", 0},
		//		{"mydb.dbo.mytable.col", 0},
		// {"1 + case When aa=10 then 'aa' WHEN bb>=20 then 'bb' else 'cc' end", 0},
		// {"@a between 10 + 11 and 20 + 21", 0},
		// {"1 + 2 3 + 4", 0},
		//		{"print 5 + (10+20 + case when (11 + (22+33)) = 44 then 4444 else 555)", 0},
		//		{"if 11 = 22 print 1 + (case when 11 = 44 then 4444 else 555 end) else print 'asdf'  print 9999999", 0},
		{"declare @AA char(8) while 11=22 if 1=2 print 5 + (10+20) else print 5 + (10+20 + case when (11 + (22+33)) = 44 then 4444 else 555 end) + 1111 print 9999", 0},
	}

	// test all samples

	for i := range samples {

		// init Lexer

		sample := samples[i]

		txt := sample.spl_input

		if parser, rsql_err = New_parser("en_CS_AS", "xxx"); rsql_err != nil {
			t.Fatalf("%v", rsql_err)
		}

		rsql_err = parser.Attach_batch([]byte(txt))
		if rsql_err != nil {
			t.Fatalf("<%s>: Attach_batch() should not fail", txt)
		}

		rsql_err = parser.Parse_all_AST()

		//=== if error occurred, check that it is the expected one ===

		if rsql_err != nil {
			err_message_id = rsql_err.Message_id
			fmt.Println(rsql_err)

			if err_message_id != sample.spl_expected_message_id {
				t.Fatalf("<%s>: Got error %v, expected %v", sample.spl_input, err_message_id, sample.spl_expected_message_id)
			}
			continue // continue testing with next sample
		}

		//=== no error. ===

		//  		if tok == nil {
		//  			t.Fatalf("<%s>: Got nil token", sample.spl_input)
		//  		}

		// Fprintf_ERR()
		/*
			var margin Margin_buffer_t
			var w io.Writer = os.Stdout
			margin.Debug_print_AST(w, parser.Batch_ast_anchor)
		*/
	}

}

func Test_parser_collation(t *testing.T) {

	var (
		parser         *Parser
		rsql_err       *rsql.Error
		err_message_id rsql.Message_id_t
		result         string
	)

	samples := []struct {
		spl_input               string
		spl_output              string
		spl_expected_message_id rsql.Message_id_t
	}{
		{"en_cs_as", "en_cs_as", 0},
		{"EN_CS_AS", "en_cs_as", 0},
		{"EN", "", rsql.ERROR_BAD_COLLATION_LOCALE_ATTRIBUTES},
		{"EN_CS", "", rsql.ERROR_BAD_COLLATION_LOCALE_ATTRIBUTES},
		{"ENGLISH", "", rsql.ERROR_BAD_COLLATION_LOCALE_ATTRIBUTES},
		{"ENGLISH_CS_AS", "en_cs_as", 0},
		{"ENGLISH_CI_AI", "en_ci_ai", 0},
		{"english_ci_aI", "en_ci_ai", 0},
		{"fr_CA_ci_aI", "fr_ca_ci_ai", 0},
		{"FRENCH_ci_aI", "fr_ci_ai", 0},
		{"xx_cs_as", "", rsql.ERROR_BAD_COLLATION_LOCALE},
		{"_cs_as", "", rsql.ERROR_BAD_COLLATION_LOCALE},
		{"xxxxxxxx_cs_as", "", rsql.ERROR_BAD_COLLATION_LOCALE},
		{"", "", rsql.ERROR_BAD_COLLATION_LOCALE},
	}

	// test all samples

	for i := range samples {

		// init Lexer

		sample := samples[i]

		if parser, rsql_err = New_parser("en_cs_as", "xxx"); rsql_err != nil {
			t.Fatalf("%v", rsql_err)
		}

		rsql_err = parser.Attach_batch([]byte{})
		if rsql_err != nil {
			t.Fatalf("Attach_batch() should not fail")
		}

		result, rsql_err = rsql.Normalize_collation(sample.spl_input)

		//=== if error occurred, check that it is the expected one ===

		if rsql_err != nil {
			err_message_id = rsql_err.Message_id

			if err_message_id != sample.spl_expected_message_id {
				t.Fatalf("<%s>: Got error %v, expected %v", sample.spl_input, err_message_id, sample.spl_expected_message_id)
			}
			continue // continue testing with next sample
		}

		//=== no error. Check result. ===

		if sample.spl_expected_message_id != 0 { // check that we expect no error
			t.Fatalf("<%s>: Got no error, expected %v", sample.spl_input, sample.spl_expected_message_id)
		}

		if result != sample.spl_output {
			t.Fatalf("<%s>: Got no error, expected %s", sample.spl_input, sample.spl_output)
		}

	}

}

func Test_parser_language(t *testing.T) {

	var (
		parser         *Parser
		rsql_err       *rsql.Error
		err_message_id rsql.Message_id_t
		result         string
	)

	samples := []struct {
		spl_input               string
		spl_output              string
		spl_expected_message_id rsql.Message_id_t
	}{
		{"EN-US", "en-us", 0},
		{"en-us", "en-us", 0},
		{"en_us", "en-us", 0},
		{"US_ENGLISH", "en-us", 0},
		{"us_englisH", "en-us", 0},
		{"us-englisH", "", rsql.ERROR_SQLDATA_SYSLANGUAGE_INVALID_LOCALE},
		{"asdf", "", rsql.ERROR_SQLDATA_SYSLANGUAGE_INVALID_LOCALE},
		{"", "", rsql.ERROR_SQLDATA_SYSLANGUAGE_INVALID_LOCALE},
		{"日本語", "ja-jp", 0},
	}

	// test all samples

	for i := range samples {

		// init Lexer

		sample := samples[i]

		if parser, rsql_err = New_parser("en_cs_as", "xxx"); rsql_err != nil {
			t.Fatalf("%v", rsql_err)
		}

		rsql_err = parser.Attach_batch([]byte{})
		if rsql_err != nil {
			t.Fatalf("Attach_batch() should not fail")
		}

		result, rsql_err = lang.Normalize_language(sample.spl_input)

		//=== if error occurred, check that it is the expected one ===

		if rsql_err != nil {
			err_message_id = rsql_err.Message_id

			if err_message_id != sample.spl_expected_message_id {
				t.Fatalf("<%s>: Got error %v, expected %v", sample.spl_input, err_message_id, sample.spl_expected_message_id)
			}
			continue // continue testing with next sample
		}

		//=== no error. Check result. ===

		if sample.spl_expected_message_id != 0 { // check that we expect no error
			t.Fatalf("<%s>: Got no error, expected %v", sample.spl_input, sample.spl_expected_message_id)
		}

		if result != sample.spl_output {
			t.Fatalf("<%s>: Got no error, expected %s", sample.spl_input, sample.spl_output)
		}

	}

}
