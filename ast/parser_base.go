package ast

import (
	"strings"
	"text/template"

	"rsql"
	"rsql/dict"
	"rsql/lex"
	"rsql/lk"
)

type Options_t uint32

const (
	OPTION_QUOTED_IDENTIFIER Options_t = 1 << iota // option for lexer: identifier may be quoted by double-quotes. The default is read from dict.MASTER server parameters.
	OPTION_ANSI_NULL_DFLT_ON                       // option for parser: for CREATE TABLE, column without NULL/NOT NULL clause are NULL. This option is set by default.
	OPTION_PARSEONLY                               // option for caller's use: only parse the batch. No execution.
	OPTION_NOEXEC                                  // option for caller's use: same as PARSEONLY.
)

func Get_default_options() Options_t {
	var (
		options Options_t
	)

	options |= OPTION_ANSI_NULL_DFLT_ON // options must be ORed

	if dict.MASTER.Get_sysparameters_int_value(dict.PARAM_SERVER_QUOTED_IDENTIFIER) != 0 {
		options |= OPTION_QUOTED_IDENTIFIER
	}

	return options
}

type Parser struct {
	lex.Lexer

	Options Options_t

	Servername                       string
	Server_default_collation         string
	parser_current_default_database  string // used only by the parser during parsing stage
	parser_current_default_schema    string // used only by the parser during parsing stage
	questionmark_allowed             bool
	disable_current_timestamp_update bool // the statement SET _@current_timestamp is appended after some statements like SELECT, UPDATE, etc, only on global level, but not inside function or stored procedure.
	expression_recursivity_depth     int
	aggregate_func_allowed_flag      bool  // allow aggregate functions in SELECT expressions
	aggregate_func_detected          bool  // used to detect aggregate functions in SELECT expressions
	flashtable_tblid_generator       int64 // used to create flashtable tblid (@vartables, grouptables and sorttables). Always negative value.

	while_stack while_stack_t // just used to parse WHILE statements

	LABELS Dictionary_LABEL // contains all labels (used by GOTO) encountered in batch

	Xtable_object_stack       Xtable_stack_t // stack in which SELECT, JOIN, and real tables are stacked, in order for identifiers to lookup the proper namespace during decoration
	xtable_outer_object_stack Xtable_stack_t // used by the parser to make each Xtable point to the outer namespace. At initialization, the first element with nil value must be inserted.

	Batch_ast_anchor Tokener // after parsing, first statement of AST tree is here

	Compatibilities Compatibility       // categories of all statements in the batch, to check if they can coexist in the same batch
	Registration    *lk.Registration    // during parsing stage, all table names are put in Registration, and locks will be acquired before decoration stage
	Temptable_bag   *rsql.Temptable_bag // contains temporary tables (@vartables)

	Insert_stmts_count int // count the number of INSERT statements in a batch. Limiting this number helps avoid long lists of INSERT statements, which may use too much memory for AST tree.
}

// initialize Token.
//
// By default, the line no and pos of the Token is copied from parser.Info_line (see Info_line_update() function).
// But it can be overriden by 'info_line' optional argument.
//
func (parser *Parser) token_init(tok *Token, tok_category Tok_category_t, info_line ...lex.Coord_t) {

	tok.Tok_category = tok_category

	if len(info_line) == 0 {
		tok.Tok_batch_line = parser.Info_line
	} else {
		tok.Tok_batch_line = info_line[0]
	}
}

func (parser *Parser) Option(option Options_t) bool {

	return parser.Options&option != 0
}

func New_parser(server_default_collation_string string, session_default_db_string string, options Options_t, registration *lk.Registration) (*Parser, *rsql.Error) {
	var (
		rsql_err *rsql.Error
	)

	parser := &Parser{}

	if rsql_err = parser.Initialize(server_default_collation_string, session_default_db_string, options, registration); rsql_err != nil {
		return nil, rsql_err
	}

	return parser, nil
}

func (parser *Parser) Initialize(server_default_collation_string string, session_default_db_string string, options Options_t, registration *lk.Registration) *rsql.Error {
	var (
		rsql_err       *rsql.Error
		collation      string
		default_db     string
		default_schema string
	)

	parser.LABELS.Initialize()

	// get server name

	parser.Servername = rsql.G_SERVERNAME

	// set all options

	parser.Options = options

	switch parser.Options & OPTION_QUOTED_IDENTIFIER { // set option for the lexer
	case 0:
		parser.Lexer.Set_option_quoted_identifier(false)
	default:
		parser.Lexer.Set_option_quoted_identifier(true)
	}

	// set server default collation

	collation = strings.ToLower(server_default_collation_string)

	if collation, rsql_err = rsql.Normalize_collation(collation); rsql_err != nil { // collation is normalized and checked here. It is lowercase, e.g. fr_ca_cs_as
		return rsql_err
	}

	parser.Server_default_collation = collation

	// set default db and default schema

	default_db = strings.ToLower(session_default_db_string) // used during parsing stage for uncomplete qualified names. USE statement changes it
	parser.parser_current_default_database = default_db

	default_schema = "dbo" // used during parsing stage for uncomplete qualified names. Always "dbo", never changes
	parser.parser_current_default_schema = default_schema

	// other

	parser.flashtable_tblid_generator = SPEC_FLASHTABLE_GENERATOR_START_TBLID

	parser.xtable_outer_object_stack = append(parser.xtable_outer_object_stack, nil) // first element is always a nil element

	parser.Registration = registration

	parser.Temptable_bag = rsql.New_Temptable_bag()

	return nil
}

func (parser *Parser) Get_flashtable_new_tblid() int64 {

	tblid := parser.flashtable_tblid_generator
	parser.flashtable_tblid_generator--

	return tblid
}

func (parser *Parser) Attach_batch(batch_text []byte) *rsql.Error {

	if rsql_err := parser.Lexer.Attach_batch(batch_text); rsql_err != nil { // parser.Current_lexeme is LEX_START_OF_BATCH
		return rsql_err
	}

	if rsql_err := parser.Eat_next_lexeme(); rsql_err != nil { // parser.Current_lexeme contains first real lexeme, or LEX_END_OF_BATCH
		return rsql_err
	}

	return nil
}

// The Tokener interface contains a pointer to any type of token.
//
type Tokener interface {
	Category() Tok_category_t
	Prev() Tokener
	Next() Tokener
	Set_prev(Tokener)
	Set_next(Tokener)
	Batch_line() lex.Coord_t
	Set_batch_line(lex.Coord_t)
}

// The AST is made up of tokens.
// All tokens have the struct Token as first member.
//
type Token struct {
	Tok_category   Tok_category_t // category, e.g. TOK_STMT_PRINT, TOK_PRIMARY, etc.
	Tok_batch_line lex.Coord_t    // line no (starting from 1) and position (starting from 1) in batch.

	Tok_prev Tokener
	Tok_next Tokener
}

func (tok *Token) Category() Tok_category_t {
	return tok.Tok_category
}

func (tok *Token) Prev() Tokener {
	return tok.Tok_prev
}

func (tok *Token) Next() Tokener {
	return tok.Tok_next
}

func (tok *Token) Set_prev(a Tokener) {
	tok.Tok_prev = a
}

func (tok *Token) Set_next(a Tokener) {
	tok.Tok_next = a
}

func (tok *Token) Batch_line() lex.Coord_t {
	return tok.Tok_batch_line
}

func (tok *Token) Set_batch_line(batch_line lex.Coord_t) {
	tok.Tok_batch_line = batch_line
}

// token_chain appends tok_b as next element of tok_a in the double-linked chain of tokens.
//
// tok must be the last element of the chain.
//
func token_chain(tok_a Tokener, tok_b Tokener) {
	assert(tok_b.Prev() == nil)
	assert(tok_a.Next() == nil)

	tok_b.Set_prev(tok_a)
	tok_a.Set_next(tok_b)
}

// token_unchain removes an element from the double-linked chain of tokens.
// The removed token can be the first, the last, or a token in the middle of the chain.
//
func token_unchain(tok Tokener) {

	//     token A  <-->  tok  <-->  token C

	if tok.Prev() != nil { // if token A exists, make it point to token C
		tok.Prev().Set_next(tok.Next())
	}

	if tok.Next() != nil { // if token C exists, make it point to token A
		tok.Next().Set_prev(tok.Prev())
	}

	tok.Set_prev(nil) // make tok_current point to nowhere
	tok.Set_next(nil)
}

// Token for SET NOCOUNT.
//
type Token_stmt_SET_NOCOUNT struct {
	Token

	Sc_nocount bool
}

// Token for SET LANGUAGE, SET DATEFIRST, SET DATEFORMAT.
// This statement changes the value of variable "_@current_language".
//
type Token_stmt_SET_SYSLANGUAGE struct {
	Token

	Sl_language   string
	Sl_datefirst  int32
	Sl_dateformat string
}

// Token for USE database name.
// This statement changes the value of variables "_@current_db_name", "_@current_db_id", "_@current_user_name", "_@current_user_id".
//
//       compiler pragma: during parsing stage, when the parser eats this statement, it changes parser.current_default_database.
//                        This value is used by the parser to complete the column or table identifiers, if the database part is missing.
//
type Token_stmt_USE struct {
	Token

	Use_database_name string

	Use_database_name_OUT *Token_primary // OUT variable (that is, variable modified directly by the statement)
	Use_database_dbid_OUT *Token_primary //     same
	Use_schema_name_OUT   *Token_primary //     same
	Use_schema_schid_OUT  *Token_primary //     same
	Use_user_name_OUT     *Token_primary //     same
	Use_user_palid_OUT    *Token_primary //     same
}

type Token_stmt_SLEEP struct {
	Token

	Sl_duration float64
}

type Token_stmt_SHUTDOWN struct {
	Token

	Sd_nowait bool
}

// Token_stmt_IF represents the statement IF.
//
type Token_stmt_IF struct {
	Token

	If_cond *Token_primary
	If_then Tokener // points at first token of a list of tokens
	If_else Tokener // points at first token of a list of tokens

	If_basicblock_pre        *rsql.Basicblock
	If_basicblock_then_start *rsql.Basicblock
	If_basicblock_then_end   *rsql.Basicblock
	If_basicblock_else_start *rsql.Basicblock
	If_basicblock_else_end   *rsql.Basicblock
	If_basicblock_post       *rsql.Basicblock
}

// Token_stmt_WHILE represents the statement WHILE.
//
type Token_stmt_WHILE struct {
	Token

	While_cond *Token_primary
	While_body Tokener // points at first token of a list of tokens

	While_basicblock_pre        *rsql.Basicblock
	While_basicblock_cond_start *rsql.Basicblock
	While_basicblock_while_end  *rsql.Basicblock
	While_basicblock_post       *rsql.Basicblock
}

// Token_stmt_BREAK represents the statement BREAK.
//
type Token_stmt_BREAK struct {
	Token

	Br_attached_loop *Token_stmt_WHILE
}

// Token_stmt_CONTINUE represents the statement CONTINUE.
//
type Token_stmt_CONTINUE struct {
	Token

	Cont_attached_loop *Token_stmt_WHILE
}

// Token_stmt_RETURN represents the statement RETURN.
//
type Token_stmt_RETURN struct {
	Token

	Ret_expression *Token_primary
}

// Token_stmt_GOTO represents the statement GOTO.
//
type Token_stmt_GOTO struct {
	Token

	Gt_label string

	Gt_all_labels Dictionary_LABEL // for all Token_stmt_GOTO, this field points to the same collection of labels, parser.LABELS. This is needed by 'emit' package, to generate GOTO instructions.
}

// Token_stmt_LABEL represents the statement LABEL.
//
type Token_stmt_LABEL struct {
	Token

	Lab_label string

	Lab_basicblock_pre   *rsql.Basicblock
	Lab_basicblock_label *rsql.Basicblock
}

// Token for declaration of variable.
// It is not a real statement but a compiler directive, as it won't be executed by the VM.
//
type Token_stmt_DECLARE_variable struct {
	Token

	Decl_variable *Token_primary
}

// Token for assignment of an expression to a variable.
//
type Token_stmt_ASSIGNMENT struct {
	Token

	Set_instruction_code rsql.Instrcode_t

	Set_lvalue *Token_primary
	Set_rvalue *Token_primary
}

// Token for CREATE TABLE.
//
type Token_stmt_CREATE_TABLE struct {
	Token

	Ct_database_name string
	Ct_schema_name   string
	Ct_gtabledef     *rsql.GTabledef
}

// Token for CREATE INDEX.
//
type Token_stmt_CREATE_INDEX struct {
	Token

	Ci_table_qname         rsql.Object_qname_t
	Ci_index_name          string // may be empty string. Index name will be generated automatically during index creation.
	Ci_index_name_original string //   same
	Ci_index_type          rsql.Td_index_type_t
	Ci_cluster_type        rsql.Td_cluster_type_t
	Ci_colname_list        []string
}

// Token for ALTER INDEX.
//
type Token_stmt_ALTER_INDEX struct {
	Token

	Ai_table_qname rsql.Object_qname_t
	Ai_index_name  string

	Ai_change_index_name          string
	Ai_change_index_name_original string
}

// Token for DROP TABLE.
//
type Token_stmt_DROP_TABLE struct {
	Token

	Dt_table_qname rsql.Object_qname_t
}

// Token for DROP INDEX.
// Index can be clustered or nonclustered.
//
type Token_stmt_DROP_INDEX struct {
	Token

	Di_table_qname rsql.Object_qname_t
	Di_index_name  string
}

// Token for ALTER TABLE.
//
type Token_stmt_ALTER_TABLE struct {
	Token

	At_table_qname rsql.Object_qname_t

	At_change_table_name          string
	At_change_table_name_original string

	At_column_name                 string
	At_change_column_name          string
	At_change_column_name_original string
	At_change_column_nullability   dict.Alter_column_nullability_t
}

type Token_stmt_THROW struct {
	Token

	Th_error_number *Token_primary // int
	Th_message      *Token_primary // varchar
	Th_severity     *Token_primary // int
	Th_state        *Token_primary // int
}

// Token for PRINT.
//
type Token_stmt_PRINT struct {
	Token

	Pr_listexpr []*Token_primary
}

// Token for _ASSERT_.
//
// THIS FUNCTION _ASSERT_(expr) IS USED ONLY TO TEST SQL SCRIPTS. THE USER SHOULD NOT USE IT.
//
type Token_stmt_ASSERT_ struct {
	Token

	As_expr *Token_primary // always BOOLEAN
}

// Token for _ASSERT_NULL_.
//
// THIS FUNCTION _ASSERT_NULL_(expr) IS USED ONLY TO TEST SQL SCRIPTS. THE USER SHOULD NOT USE IT.
//
type Token_stmt_ASSERT_NULL_ struct {
	Token

	As_expr *Token_primary // always BOOLEAN
}

// Token for _ASSERT_ERROR_.
//
// THIS FUNCTION _ASSERT_ERROR_(expr, 'error suffix') IS USED ONLY TO TEST SQL SCRIPTS. THE USER SHOULD NOT USE IT.
//
type Token_stmt_ASSERT_ERROR_ struct {
	Token

	As_expr *Token_primary // any type

	As_error_suffix *Token_primary // message error suffix as string
}

// Token for SHOW COLLATIONS
//
type Token_stmt_SHOW_COLLATIONS struct {
	Token
}

// Token for SHOW LANGUAGES
//
type Token_stmt_SHOW_LANGUAGES struct {
	Token
}

// Token for SHOW LOCKS
//
type Token_stmt_SHOW_LOCKS struct {
	Token
}

// Token for SHOW WORKERS
//
type Token_stmt_SHOW_WORKERS struct {
	Token
}

// Token for SHOW INFO
//
type Token_stmt_SHOW_INFO struct {
	Token

	Shinf_version                  string
	Shinf_server_directory         string
	Shinf_servername               string
	Shinf_server_default_collation string

	Shinf_login            *Token_primary
	Shinf_current_user     *Token_primary
	Shinf_current_database *Token_primary
	Shinf_current_schema   *Token_primary
	Shinf_current_language *Token_primary
}

// Token for SHOW
//
type Token_stmt_SHOW struct {
	Token

	Sh_instruction_code rsql.Instrcode_t // INSTR_SHOW_TABLE, etc

	Sh_output_type rsql.Sh_output_type_t
	Sh_template    *template.Template
	Sh_option_ALL  bool // if true, search objects in all databases
	Sh_option_PERM bool // display permissions
	Sh_LIKE        bool

	Sh_database_name string
	Sh_schema_name   string
	Sh_object_name   string
	Sh_LIKE_pattern  string
	Sh_flag_unique   bool

	Sh_server_default_collation string // sorting order for SHOW output
}

// Token for Token_stmt_BACKUP
//
type Token_stmt_BACKUP struct {
	Token

	Bk_database_name string
	Bk_filename      string

	Bk_server_default_collation string // sorting order tables
}

// Token for Token_stmt_RESTORE
//
type Token_stmt_RESTORE struct {
	Token

	Rt_database_name  string
	Rt_filename       string
	Rt_option_replace bool
	Rt_option_no_user bool
	Rt_option_verbose bool
}

// Token for DUMP.
//
type Token_stmt_DUMP struct {
	Token

	Dp_instruction_code rsql.Instrcode_t // INSTR_DUMP_PARAMETERS, INSTR_DUMP_LOGINS, INSTR_DUMP_DATABASE

	Dp_database_name   string
	Dp_option_DDL_ONLY bool
	Dp_filename        string

	Dp_server_default_collation string // sorting order for output
}

// Token for ALTER SERVER_PARAMETER parameters.
//
type Token_stmt_ALTER_SERVER_PARAMETER struct {
	Token

	Asp_param        dict.Server_parameter_t
	Asp_value_int    int64
	Asp_value_string string
}

// Token for SET QUOTED_IDENTIFIER ON (or OFF), SET_PARSEONLY ON, etc.
//
//    It is a dummy token. It is skipped during all processings.
//
type Token_stmt_SET_LEXER_OR_PARSER_OPTION struct {
	Token
}

// Token for CREATE LOGIN.
//
type Token_stmt_CREATE_LOGIN struct {
	Token

	Cl_login_name       string
	Cl_password_hash    string // LEX_LITERAL_STRING
	Cl_default_database string // default is "trashdb"
	Cl_default_language string
}

// Token for ALTER LOGIN.
//
type Token_stmt_ALTER_LOGIN struct {
	Token

	Al_login_name string

	Al_change_name             string
	Al_change_password_hash    string
	Al_change_default_database string
	Al_change_default_language string
	Al_change_disable_option   lex.Auxword_t
}

// Token for DROP LOGIN.
//
type Token_stmt_DROP_LOGIN struct {
	Token

	Dl_login_name string
}

// Token for CREATE DATABASE.
//
type Token_stmt_CREATE_DATABASE struct {
	Token

	Cd_database_name string
}

// Token for ALTER DATABASE.
//
type Token_stmt_ALTER_DATABASE struct {
	Token

	Ad_database_name string

	Ad_change_name string
	Ad_status      lex.Auxword_t
	Ad_mode        lex.Auxword_t
	Ad_access      lex.Auxword_t
}

// Token for DROP DATABASE.
//
type Token_stmt_DROP_DATABASE struct {
	Token

	Dd_database_name string
}

// Token for CREATE USER.
//
type Token_stmt_CREATE_USER struct {
	Token

	Cu_database_name string
	Cu_user_name     string
	Cu_login_name    string
}

// Token for ALTER USER.
//
type Token_stmt_ALTER_USER struct {
	Token

	Au_database_name string
	Au_user_name     string

	Au_change_name  string // may be ""
	Au_change_login string // may be ""
}

// Token for DROP USER.
//
type Token_stmt_DROP_USER struct {
	Token

	Du_database_name string
	Du_user_name     string
}

// Token for CREATE ROLE.
//
type Token_stmt_CREATE_ROLE struct {
	Token

	Cr_database_name string
	Cr_role_name     string
}

// Token for ALTER ROLE.
//
type Token_stmt_ALTER_ROLE struct {
	Token

	Ar_database_name string
	Ar_role_name     string

	Ar_add_member_name  string // only one of these 3 fields is filled with non-empty string
	Ar_drop_member_name string //  see above
	Ar_change_name      string //  see above
}

// Token for DROP ROLE.
//
type Token_stmt_DROP_ROLE struct {
	Token

	Dr_database_name string
	Dr_role_name     string
}

// Token for GRANT permission.
//
type Token_stmt_GRANT struct {
	Token

	Gr_object_qname            rsql.Object_qname_t
	Gr_permission              rsql.Permission_t
	Gr_list_of_principal_names []string
}

// Token for DENY permission.
//
type Token_stmt_DENY struct {
	Token

	De_object_qname            rsql.Object_qname_t
	De_permission              rsql.Permission_t
	De_list_of_principal_names []string
}

// Token for REVOKE permission.
//
type Token_stmt_REVOKE struct {
	Token

	Re_object_qname            rsql.Object_qname_t
	Re_permission              rsql.Permission_t
	Re_list_of_principal_names []string
	Re_from_all                bool
}

// Token for ALTER AUTHORIZATION.
//
type Token_stmt_ALTER_AUTHORIZATION struct {
	Token

	Aa_database_name      string
	Aa_change_owner_login string
}

// Token for INSERT INTO table.
//
// Recods to insert come from a VALUES clause with a list of records, or from a SELECT clause.
//
type Token_stmt_INSERT_INTO struct {
	Token

	Ins_instruction_code rsql.Instrcode_t // INSTR_STMT_INSERT_VALUES or INSTR_STMT_INSERT_SELECT

	Ins_table_qname rsql.Object_qname_t

	Ins_listcol                    []string
	Ins_listcol_base_seqno         []uint16 // base seqno of columns in Ins_listcol
	Ins_explicit_IDENTITY_provided bool     // IDENTITY column name exists in Ins_listcol

	Ins_values_array [][]*Token_primary // VALUES clause if any

	Ins_xtable                Xtable           // SELECT clause if any
	Ins_conversion_tokens     []*Token_primary // for SELECT clause, convert dataslots from SELECT columns into Ins_base_row. Only necessary conversions are appended to this list.
	Ins_conversion_basicblock *rsql.Basicblock // for SELECT clause, instructions generated for Ins_conversion_tokens

	Ins_gtabledef *rsql.GTabledef // as we pass VALUES as rows of base table, the table layout and column datatypes should be identical when the VM will insert them. The lock manager ensures that GTabledef of the table is not modified.
	Ins_base_row  rsql.Row        // Row of base table
}

// Token for BULK INSERT.
//
type Token_stmt_BULK_INSERT struct {
	Token

	Bi_table_qname rsql.Object_qname_t
	Bi_filename    string // file to import

	Bi_opt_codepage        string
	Bi_opt_rowterminator   string
	Bi_opt_fieldterminator string
	Bi_opt_firstrow        uint64
	Bi_opt_lastrow         uint64
	Bi_opt_keepidentity    bool
	Bi_opt_newidentity     bool
	Bi_opt_rtrim           bool

	Bi_gtabledef   *rsql.GTabledef // as we pass VALUES as rows of base table, the table layout and column datatypes should be identical when the VM will insert them. The lock manager ensures that GTabledef of the table is not modified.
	Bi_syslanguage *Token_primary  // used by VM to convert field from file to DATE, TIME or DATETIME.
}

// Token for BULK EXPORT.
//
type Token_stmt_BULK_EXPORT struct {
	Token

	Be_filename string // file to import

	Be_opt_codepage        string
	Be_opt_rowterminator   string
	Be_opt_fieldterminator string
	Be_opt_date_format     string
	Be_opt_time_format     string
	Be_opt_datetime_format string

	Be_xtable Xtable // XT_TABLE, XT_SELECT, or XT_UNION
}

// Token for SELECT, or series of UNIONed SELECTs.
//
type Token_stmt_SELECT_or_UNION struct {
	Token

	Su_xtable Xtable
}

// Token for DELETE.
//
type Token_stmt_DELETE struct {
	Token

	De_target_database_name string
	De_target_schema_name   string
	De_target_table_name    string

	De_from         *Xtable_from
	De_target_table *Xtable_table
}

// Token for UPDATE.
//
type Token_stmt_UPDATE struct {
	Token

	Ud_from         *Xtable_from
	Ud_target_table *Xtable_table // contains rsql.Object_qname_t of target table

	Ud_set_list   []Set_clause     // list of SET clauses
	Ud_basicblock *rsql.Basicblock // basic block containing expressions for SET clauses

	Ud_insertion_base_tokens []*Token_primary // base row to insert. It is a shallow copy of target table row, except for columns that have a SET clause.

	Ud_indexes_to_update map[string]*rsql.Tabledef // indexes to update, because they contain columns that are updated. If nil, all indexes must be updated.
}

type Set_clause struct {
	St_colname string
	St_expr    *Token_primary
}

// Token for TRUNCATE TABLE.
//
type Token_stmt_TRUNCATE_TABLE struct {
	Token

	Tt_table_qname rsql.Object_qname_t
	Tt_shrink_file bool

	Tt_gtabledef *rsql.GTabledef
}

// Token for SHRINK TABLE.
//
type Token_stmt_SHRINK_TABLE struct {
	Token

	Sk_table_qname rsql.Object_qname_t

	Sk_gtabledef *rsql.GTabledef
}

// Token for BEGIN TRANSACTION.
//
type Token_stmt_BEGIN_TRANSACTION struct {
	Token
}

// Token for COMMIT TRANSACTION.
//
type Token_stmt_COMMIT_TRANSACTION struct {
	Token
}

// Token for ROLLBACK TRANSACTION.
//
type Token_stmt_ROLLBACK_TRANSACTION struct {
	Token
}
