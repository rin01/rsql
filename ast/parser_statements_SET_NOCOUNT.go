package ast

import (
	"rsql"
)

// get_statement_SET_NOCOUNT parses SET NOCOUNT statement.
//
//    SET NOCOUNT { ON|OFF }
//
func (parser *Parser) get_statement_SET_NOCOUNT() (*Token_stmt_SET_NOCOUNT, *rsql.Error) {
	var (
		rsql_err               *rsql.Error
		token_stmt_set_nocount *Token_stmt_SET_NOCOUNT
		command                bool
	)

	/* create Token_nocount */

	token_stmt_set_nocount = &Token_stmt_SET_NOCOUNT{}
	parser.token_init(&token_stmt_set_nocount.Token, TOK_STMT_SET_NOCOUNT)

	if rsql_err = parser.Eat_next_lexeme(); rsql_err != nil { // skip NOCOUNT
		return nil, rsql_err
	}

	// eat ON or OFF

	if command, rsql_err = parser.Eat_keyword_ON_OFF(); rsql_err != nil {
		return nil, rsql_err
	}

	token_stmt_set_nocount.Sc_nocount = command

	return token_stmt_set_nocount, nil
}
