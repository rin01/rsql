package rsql

// Row is a record of IDataslots
//
// When we insert a record into a table, a Row is serialized as a Tuple and written to disk.
// When we read a record, a Tuple is read from disk and deserialized into the Row.
//
type Row []IDataslot

// Tuple is a serialized Row, stored on disk as a stream of bytes.
//
//        Its structure is:
//
//             offset of field 0                 uint16    number of fields == offset of field 0 / 2 - 1
//             offset of field 1                 uint16
//             ...
//             offset of field n-1               uint16
//             offset of sentinel field n        uint16    this field n doesn't really exists, This offset value it just one byte past the end of field n-1. It is the length of the tuple, in bytes.
//
//             serialized value of field 0       bunch of bytes, starting at "offset of field 0"
//             ...                               bunch of bytes
//             serialized value of field n-1     bunch of bytes, starting at "offset of field n-1"
//
//
//        The illustration of the layout is:
//              ======================= header of offsets ==================----------------------------- data ------------------------------------
//              | offset 0 | offset 1 | ... | offset n-1 | offset sentinel | serialized field 0 | serialized field 1 | ... | serialized field n-1 |
//              ============================================================-----------------------------------------------------------------------
//
type Tuple []byte

type Tuple_type_t uint8

const (
	TUPLE_TYPE_LEAF Tuple_type_t = iota + 1
	TUPLE_TYPE_NODE
)

const FIELD_OFFSET_SIZE = 2 // offsets in tuple header are uint16

// Field_count returns number of fields in tuple.
//
// See data/data_tuple_serialization.go for serialization format.
//
func (tuple Tuple) Field_count() int {
	var (
		offset_0 uint16
	)

	offset_0 = uint16(tuple[0]) | uint16(tuple[1])<<8

	return int(offset_0/2) - 1 // two bytes per field offset, minus sentinel offset
}

func (tuple Tuple) Get_last_field_uint64() uint64 {
	var (
		offset_0          uint16
		offset_last_field uint16
		last_field        []byte
		val               uint64
	)

	offset_0 = uint16(tuple[0]) | uint16(tuple[1])<<8

	offset_last_field = uint16(tuple[offset_0-4]) | uint16(tuple[offset_0-3])<<8

	last_field = tuple[offset_last_field:]

	Assert(len(last_field) == 8)

	// deserialize last field

	val = uint64(last_field[0]) | uint64(last_field[1])<<8 | uint64(last_field[2])<<16 | uint64(last_field[3])<<24 | uint64(last_field[4])<<32 | uint64(last_field[5])<<40 | uint64(last_field[6])<<48 | uint64(last_field[7])<<56

	return val
}

func (tuple Tuple) Set_last_field_uint64(v uint64) {
	var (
		offset_0          uint16
		offset_last_field uint16
		last_field        []byte
	)

	offset_0 = uint16(tuple[0]) | uint16(tuple[1])<<8

	offset_last_field = uint16(tuple[offset_0-4]) | uint16(tuple[offset_0-3])<<8

	last_field = tuple[offset_last_field:]

	Assert(len(last_field) == 8)

	// serialize last field

	last_field[0] = byte(v)
	last_field[1] = byte(v >> 8)
	last_field[2] = byte(v >> 16)
	last_field[3] = byte(v >> 24)
	last_field[4] = byte(v >> 32)
	last_field[5] = byte(v >> 40)
	last_field[6] = byte(v >> 48)
	last_field[7] = byte(v >> 56)
}

func (tuple Tuple) Get_field_part(i int) []byte {
	var (
		offset_index int
		offset_start uint16
		offset_endx  uint16
		field        []byte
	)

	Assert(i < tuple.Field_count())

	offset_index = i * FIELD_OFFSET_SIZE

	offset_start = uint16(tuple[offset_index]) | uint16(tuple[offset_index+1])<<8
	offset_endx = uint16(tuple[offset_index+2]) | uint16(tuple[offset_index+3])<<8

	field = tuple[offset_start:offset_endx]

	return field
}
