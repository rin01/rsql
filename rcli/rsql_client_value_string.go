package main

import (
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"

	"rsql/rsqlib"
)

const (
	COL_WIDTH_VOID              = 4 // good enough to display "Null"
	COL_WIDTH_BOOLEAN           = 5 // large enough for "false"
	COL_WIDTH_VARBINARY_DEFAULT = 10
	COL_WIDTH_VARCHAR_DEFAULT   = 10
	COL_WIDTH_BIT               = 1
	COL_WIDTH_TINYINT           = 3
	COL_WIDTH_SMALLINT          = 6
	COL_WIDTH_INT               = 11
	COL_WIDTH_BIGINT            = 20
	// WIDTH_MONEY depends on precision
	// WIDTH_NUMERIC depends on precision
	COL_WIDTH_FLOAT = 23 // max width seems to be 13 characters, for %.6g, 19 characters for %.12g, 22 characters for %.15g
	COL_WIDTH_DATE  = len(COL_FMT_DATE)
	// COL_WIDTH_TIME depends on fractional_second
	// COL_WIDTH_DATETIME depends on fractional_second

	COL_FMT_FLOAT = "%*.15g" // 12 is number of digits in coefficient. But the total width must account for the exponent, decimal point and sign. COL_WIDTH_FLOAT: max total width seems to be 22 characters, for a precision of 15.
	COL_FMT_DATE  = "2006-01-02"
)

// stringval_formatted returns a field value, properly formatted.
//
// width_limit is the max width of the returned string for VARBINARY and VARCHAR.
// fractional_second is the number of nanoseconds displayed for TIME and DATETIME.
//
// For a given datatype, all values are displayed in the same width, so that multiple rows can be displayed with columns properly aligned.
//
//          IF YOU MODIFY THIS CODE AND A COLUMN WIDTH CHANGES, YOU MUST ALSO CHANGE THE FUNCTION stringval_col_width().
//
func stringval_formatted(field rsqlib.IField, null_string string, width_limit int, fractional_second int) string {

	switch field := field.(type) {
	case *rsqlib.Void:
		assert(field.Is_Null == true)

		return fmt.Sprintf("%-*.*s", COL_WIDTH_VOID, COL_WIDTH_VOID, null_string)

	case *rsqlib.Boolean:
		if field.Is_Null {
			return fmt.Sprintf("%-*.*s", COL_WIDTH_BOOLEAN, COL_WIDTH_BOOLEAN, null_string)
		}

		s := "false"
		if field.Val == true {
			s = "true"
		}

		return fmt.Sprintf("%-*s", COL_WIDTH_BOOLEAN, s)

	case *rsqlib.Varbinary:
		if width_limit < 4 { // we want min width to be at least 4 characters
			width_limit = 4
		}

		col_width := 2 + int(field.Precision*2) // 0x...

		if col_width > width_limit {
			col_width = width_limit
		}

		if field.Is_Null {
			return fmt.Sprintf("%-*.*s", col_width, col_width, null_string)
		}

		if 2+len(field.Val)*2 <= col_width { // col_width is large enough to display varbinary value "0x..."
			return fmt.Sprintf("0x%-*x", col_width-2, field.Val) // 0x...
		}

		// value 0x... must be truncated

		s := fmt.Sprintf("0x%x~", field.Val[:(col_width-3)/2])

		return fmt.Sprintf("%-*s", col_width, s)

	case *rsqlib.Varchar:
		if width_limit < 1 { // we want min width to be at least 1 character
			width_limit = 1
		}

		col_width := int(field.Precision) // precision in rune count

		if col_width > width_limit {
			col_width = width_limit
		}

		if field.Is_Null {
			return fmt.Sprintf("%-*.*s", col_width, col_width, null_string)
		}

		rune_count := utf8.RuneCount(field.Val)

		// replace trailing spaces and control characters with middle dots

		s := string(field.Val)

		non_graphic_count := 0
		for len(s) > 0 {
			r, size := utf8.DecodeLastRuneInString(s)
			if unicode.IsGraphic(r) && r != ' ' {
				break
			}
			non_graphic_count++
			s = s[:len(s)-size]
		}

		if non_graphic_count > 0 {
			s += strings.Repeat("\u00b7", non_graphic_count) // middle dots
		}

		if rune_count <= col_width { //col_width is large enough to display varchar value
			return fmt.Sprintf("%-*s", col_width, s) // trailing blanks and control characters are replaced by a dot
		}

		// truncate string

		rune_count = 1
		for i, _ := range s {
			if rune_count >= col_width {
				s = s[:i] + "~"
				break // break for
			}
			rune_count++
		}

		assert(utf8.RuneCountInString(s) == col_width)

		return s

	case *rsqlib.Bit:
		if field.Is_Null {
			return fmt.Sprintf("%*.*s", COL_WIDTH_BIT, COL_WIDTH_BIT, null_string)
		}

		return fmt.Sprintf("%*d", COL_WIDTH_BIT, field.Val)

	case *rsqlib.Tinyint:
		if field.Is_Null {
			return fmt.Sprintf("%*.*s", COL_WIDTH_TINYINT, COL_WIDTH_TINYINT, null_string)
		}

		return fmt.Sprintf("%*d", COL_WIDTH_TINYINT, field.Val)

	case *rsqlib.Smallint:
		if field.Is_Null {
			return fmt.Sprintf("%*.*s", COL_WIDTH_SMALLINT, COL_WIDTH_SMALLINT, null_string)
		}

		return fmt.Sprintf("%*d", COL_WIDTH_SMALLINT, field.Val)

	case *rsqlib.Int:
		if field.Is_Null {
			return fmt.Sprintf("%*.*s", COL_WIDTH_INT, COL_WIDTH_INT, null_string)
		}

		return fmt.Sprintf("%*d", COL_WIDTH_INT, field.Val)

	case *rsqlib.Bigint:
		if field.Is_Null {
			return fmt.Sprintf("%*.*s", COL_WIDTH_BIGINT, COL_WIDTH_BIGINT, null_string)
		}

		return fmt.Sprintf("%*d", COL_WIDTH_BIGINT, field.Val)

	case *rsqlib.Money:
		col_width := int(field.Precision) + 2 // +2 for sign and decimal point

		if field.Is_Null {
			return fmt.Sprintf("%*.*s", col_width, col_width, null_string)
		}

		return fmt.Sprintf("%*s", col_width, field.Val)

	case *rsqlib.Numeric:
		col_width := int(field.Precision) + 3 // +3 for sign, 0, decimal point. E.g. -0.123

		if field.Is_Null {
			return fmt.Sprintf("%*.*s", col_width, col_width, null_string)
		}

		return fmt.Sprintf("%*s", col_width, field.Val)

	case *rsqlib.Float:
		if field.Is_Null {
			return fmt.Sprintf("%*.*s", COL_WIDTH_FLOAT, COL_WIDTH_FLOAT, null_string)
		}

		return fmt.Sprintf(COL_FMT_FLOAT, COL_WIDTH_FLOAT, field.Val)

	case *rsqlib.Date:
		if field.Is_Null {
			return fmt.Sprintf("%*.*s", COL_WIDTH_DATE, COL_WIDTH_DATE, null_string)
		}

		return field.Val.Format(COL_FMT_DATE)

	case *rsqlib.Time:
		var fmtstring string

		switch {
		case fractional_second == 0:
			fmtstring = "15:04:05"
		case fractional_second == 1:
			fmtstring = "15:04:05.0"
		case fractional_second == 2:
			fmtstring = "15:04:05.00"
		case fractional_second == 3:
			fmtstring = "15:04:05.000"
		case fractional_second == 4:
			fmtstring = "15:04:05.0000"
		case fractional_second == 5:
			fmtstring = "15:04:05.00000"
		case fractional_second == 6:
			fmtstring = "15:04:05.000000"
		case fractional_second == 7:
			fmtstring = "15:04:05.0000000"
		case fractional_second == 8:
			fmtstring = "15:04:05.00000000"
		default:
			fmtstring = "15:04:05.000000000"
		}

		col_width := len(fmtstring)

		if field.Is_Null {
			return fmt.Sprintf("%*.*s", col_width, col_width, null_string)
		}

		return field.Val.Format(fmtstring)

	case *rsqlib.Datetime:
		var fmtstring string

		switch {
		case fractional_second == 0:
			fmtstring = COL_FMT_DATE + " 15:04:05"
		case fractional_second == 1:
			fmtstring = COL_FMT_DATE + " 15:04:05.0"
		case fractional_second == 2:
			fmtstring = COL_FMT_DATE + " 15:04:05.00"
		case fractional_second == 3:
			fmtstring = COL_FMT_DATE + " 15:04:05.000"
		case fractional_second == 4:
			fmtstring = COL_FMT_DATE + " 15:04:05.0000"
		case fractional_second == 5:
			fmtstring = COL_FMT_DATE + " 15:04:05.00000"
		case fractional_second == 6:
			fmtstring = COL_FMT_DATE + " 15:04:05.000000"
		case fractional_second == 7:
			fmtstring = COL_FMT_DATE + " 15:04:05.0000000"
		case fractional_second == 8:
			fmtstring = COL_FMT_DATE + " 15:04:05.00000000"
		default:
			fmtstring = COL_FMT_DATE + " 15:04:05.000000000"
		}

		col_width := len(fmtstring)

		if field.Is_Null {
			return fmt.Sprintf("%*.*s", col_width, col_width, null_string)
		}

		return field.Val.Format(fmtstring)

	default:
		panic("unknown datatype")
	}
}

// stringval_col_width returns the width of a column formatted by stringval_formatted.
//
func stringval_col_width(field rsqlib.IField, width_limit int, fractional_second int) int {

	switch field := field.(type) {
	case *rsqlib.Void:
		return COL_WIDTH_VOID

	case *rsqlib.Boolean:
		return COL_WIDTH_BOOLEAN

	case *rsqlib.Varbinary:
		if width_limit < 4 { // we want min width to be at least 4 characters
			width_limit = 4
		}

		col_width := 2 + int(field.Precision*2) // 0x...

		if col_width > width_limit {
			col_width = width_limit
		}

		return col_width

	case *rsqlib.Varchar:
		if width_limit < 1 { // we want min width to be at least 1 character
			width_limit = 1
		}

		col_width := int(field.Precision) // precision in rune count

		if col_width > width_limit {
			col_width = width_limit
		}

		return col_width

	case *rsqlib.Bit:
		return COL_WIDTH_BIT

	case *rsqlib.Tinyint:
		return COL_WIDTH_TINYINT

	case *rsqlib.Smallint:
		return COL_WIDTH_SMALLINT

	case *rsqlib.Int:
		return COL_WIDTH_INT

	case *rsqlib.Bigint:
		return COL_WIDTH_BIGINT

	case *rsqlib.Money:
		col_width := int(field.Precision) + 2 // +2 for sign and decimal point
		return col_width

	case *rsqlib.Numeric:
		col_width := int(field.Precision) + 3 // +3 for sign, 0, decimal point
		return col_width

	case *rsqlib.Float:
		return COL_WIDTH_FLOAT

	case *rsqlib.Date:
		return COL_WIDTH_DATE

	case *rsqlib.Time:
		if fractional_second == 0 {
			return len("15:04:05")
		}

		if fractional_second <= 9 {
			return len("15:04:05.") + fractional_second
		}

		return len("15:04:05.000000000")

	case *rsqlib.Datetime:
		if fractional_second == 0 {
			return len(COL_FMT_DATE + " 15:04:05")
		}

		if fractional_second <= 9 {
			return len(COL_FMT_DATE+" 15:04:05.") + fractional_second
		}

		return len(COL_FMT_DATE + " 15:04:05.000000000")

	default:
		panic("unknown datatype")
	}
}
