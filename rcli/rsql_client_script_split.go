package main

import (
	"fmt"
	"strconv"
)

func assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

type Batch_rc_t uint16

const (
	FIND_BATCH_SUCCESS Batch_rc_t = iota // a batch has been successfully found
	FIND_BATCH_EMPTY                     // batch is empty, e.g. GO at beginning of script, or GO 0. Don't send to server.
	FIND_BATCH_NO_MORE                   // no more batch is available in the script

	FIND_BATCH_ERROR_GO_SYNTAX   // GO line has a syntax error
	FIND_BATCH_ERROR_EXIT_SYNTAX // EXIT line has a syntax error
	FIND_BATCH_ERROR_QUIT_SYNTAX // QUIT line has a syntax error
)

type Batch_command_t uint8

const (
	BE_BATCH_GO   Batch_command_t = 1 << iota // batch ends with GO directive, or end-of-file
	BE_BATCH_EXIT                             // batch ends with EXIT directive
	BE_BATCH_QUIT                             // batch ends with QUIT directive
)

func (command Batch_command_t) String() string {
	switch command {
	case BE_BATCH_GO:
		return "BE_BATCH_GO"
	case BE_BATCH_EXIT:
		return "BE_BATCH_EXIT"
	case BE_BATCH_QUIT:
		return "BE_BATCH_QUIT"
	}

	panic("impossible")
}

// a script can be made of several batches.
// A batch can be delimited by a command like GO, EXIT or QUIT.
// Each batch is sent to the server separately, and executed separately, one after another.
//
type Batch_element struct {
	Be_batch_start         int             // byte index in script of start of the batch
	Be_batch_endx          int             // one past end byte index in script of end of the batch
	Be_batch_command       Batch_command_t // BE_BATCH_GO, BE_BATCH_EXIT, BE_BATCH_QUIT
	Be_batch_repeat_number int64           // number after GO command

	Be_next_batch_start int // byte index of start of next batch
}

// Script_find_next_batch returns the next batch, following last_batch.
// For initial value of last_batch, you must pass nil.
//
// If last_batch is GO with count > 0, this function returns a new Batch_element with Be_batch_repeat_number decremented.
//
func Script_find_next_batch(script []byte, last_batch *Batch_element) (rc Batch_rc_t, next_batch *Batch_element) {
	var (
		start_pos int
	)

	// if last batch is GO with Be_batch_repeat_number > 0, decrement it and return same batch

	if last_batch != nil && last_batch.Be_batch_command == BE_BATCH_GO && last_batch.Be_batch_repeat_number > 1 {
		next_batch = &Batch_element{}
		*next_batch = *last_batch
		next_batch.Be_batch_repeat_number--

		return FIND_BATCH_SUCCESS, next_batch
	}

	// if last batch is QUIT or EXIT,

	if last_batch != nil && (last_batch.Be_batch_command&(BE_BATCH_QUIT|BE_BATCH_EXIT) != 0) {
		return FIND_BATCH_NO_MORE, &Batch_element{Be_batch_start: last_batch.Be_next_batch_start}
	}

	// look for next batch

	if last_batch != nil {
		start_pos = last_batch.Be_next_batch_start
	}

	rc, next_batch = Script_find_batch_endx_position(script, start_pos)

	return rc, next_batch
}

func isdigit(c byte) bool {

	if c >= '0' && c <= '9' {
		return true
	}

	return false
}

// skip_blanks returns position of first non-blank character.
// Returned pos is >= p, and can be at terminating 0.
//
func skip_blanks(script []byte, p int) (pos int, blank_found bool) {
	length := len(script) - 1 // excluding terminating 0

	for p < length {
		c := script[p]
		if !(c == ' ' || c == '\t' || c == '\f' || c == '\v' || c == '\r') { // skip all blanks
			return p, blank_found
		}

		blank_found = true
		p++
	}

	return p, blank_found
}

// skip_rest_of_line returns position of \n, or of terminating 0.
//
func skip_rest_of_line(script []byte, p int) (pos int) {
	length := len(script) - 1 // excluding terminating 0

	for p < length {
		c := script[p]
		if c == '\n' { // skip all charaters up to '\n'
			return p
		}

		p++
	}

	return p
}

func lcase(c byte) byte {

	if c >= 'A' && c <= 'Z' {
		return (c - 'A') + 'a'
	}

	return c
}

func parse_int64(script []byte, p int) (pos int, val int64, err error) {
	length := len(script) - 1 // excluding terminating 0

	p_start := p

	for p < length {
		if isdigit(script[p]) == false {
			break
		}

		p++
	}

	// here, script[p] is not a digit

	if val, err = strconv.ParseInt(string(script[p_start:p]), 10, 64); err != nil {
		return p_start, 0, err
	}

	return p, val, nil
}

// Script_find_batch_endx_position looks for the end position of the batch starting at start_pos in the script.
//
//      IMPORTANT: script must end with terminating \0. New_script_splitter takes care of that.
//
// This function looks for the position of the next GO, EXIT or QUIT in the script.
//
// If GO, EXIT or QUIT is not found, it returns the position of the terminating \0.
//
//      GO must be on its own line with no other instruction.
//          - it may be preceded by blank characters
//          - it may be optionally followed by a number (number of times the client will send the batch to the server)
//          - it may be followed by -- comment
//          - else, an error is returned (FIND_BATCH_ERROR_GO_SYNTAX). E.g.   GO abc
//
//      EXIT must be on its own line with no other instruction.
//          - it may be preceded by blank characters
//          - syntax is : "EXIT", "EXIT()" or "EXIT(select"
//          - else, an error is returned (FIND_BATCH_ERROR_EXIT_SYNTAX)
//
//          NOTE: EXIT is considered as a T-SQL statement by rsql server.
//                The batch includes the line containing EXIT, and will be sent to the server, which will check for syntax error.
//
//      QUIT must be on its own line with no other instruction.
//          - it may be preceded by blank characters
//          - it may be followed by -- comment
//          - else, an error is returned (FIND_BATCH_ERROR_QUIT_SYNTAX)
//
//
//      The return value rc is :
//          - FIND_BATCH_SUCCESS (0)
//          - FIND_BATCH_EMPTY               if GO is at start of the script, or GO 0
//          - FIND_BATCH_NO_MORE             if part between start_pos and end of script is made only of blank lines
//          - FIND_BATCH_ERROR_GO_SYNTAX
//          - FIND_BATCH_ERROR_EXIT_SYNTAX
//          - FIND_BATCH_ERROR_QUIT_SYNTAX
//
//      The returned batch_element always contains Be_batch_start of the batch found:
//          - for FIND_BATCH_SUCCESS, all fields of Batch_element are filled.
//          - for FIND_BATCH_EMPTY, only fields Be_batch_start and Be_next_batch_start are filled.
//          - for other rc, only field Be_batch_start is filled.
//
func Script_find_batch_endx_position(script []byte, start_pos int) (rc Batch_rc_t, batch_element *Batch_element) {
	var (
		err                    error
		p_script_end_0         int
		p                      int
		p_batch_endx           int
		c                      byte
		cc                     byte
		start_of_new_line_flag bool // true if current position is preceded on the line only by blank characters
		star_comment_level     int
		batch_command          Batch_command_t
		batch_repeat_number    int64
		blank_found_flag       bool
	)

	// initialization

	p_script_end_0 = len(script) - 1 // points to the ending \0
	assert(script[p_script_end_0] == 0)

	p = start_pos // first position of script

	// skip all blank lines

	for c := script[p]; c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == '\f' || c == '\v'; c = script[p] { // skip all blanks and blank lines
		p++
	}

	if p == p_script_end_0 { // a stream of blank lines terminated by terminating 0 is considered as empty batch. It happens often after the last GO in a script.
		return FIND_BATCH_NO_MORE, &Batch_element{Be_batch_start: start_pos} // return FIND_BATCH_NO_MORE
	}

	// eat all characters until GO, EXIT or QUIT is found

	start_of_new_line_flag = true

	for p < p_script_end_0 { // main loop
		c = script[p]

		switch c {
		case '\'', //=== skip literal string. It can contain any character, even \0, \r or \n ===
			'"':
			p++

			for p < p_script_end_0 && script[p] != c { // while not end of script, skip until ending quote is found
				p++
			}

			if p < p_script_end_0 { // if ending quote, skip it
				p++
			}

			start_of_new_line_flag = false // p is at character following end of string, or at terminating 0
			continue                       // ===> continue main loop

		case '-': //=== -- comment ===
			if script[p+1] != '-' { // if not -- comment, it is a normal character. Break out of switch.
				break
			}

			p += 2
			p = skip_rest_of_line(script, p) // while not end of script, skip until end of line is found

			start_of_new_line_flag = false // p is at terminating 0, or at \n. We don't skip \n, to let it be processed by the main loop.
			continue                       // ===> continue main loop

		case '/': //=== star comment ===
			if script[p+1] != '*' { // if not star comment, it is a normal character. Break out of switch.
				break
			}

			star_comment_level = 1 // start of star comment

			p += 2 // we are at first character inside star comment

			for p < p_script_end_0 { // while not end of file, skip all until end of star comment is found.
				if script[p] == '*' && script[p+1] == '/' { // end of star comment is found. It may be embedded.
					star_comment_level--
					p += 2 // we are at the character following the end of star comment

					if star_comment_level == 0 { // check if it is end of star comment
						break // ==> exit loop
					}

					continue // end of embedded star comment is found. Continue.

				} else if script[p] == '/' && script[p+1] == '*' { // start of embedded star comment is found. Continue.
					star_comment_level++
					p += 2
					continue
				}

				p++ // normal character. Continue.
			}

			start_of_new_line_flag = false // p is at character following end of star comment, or at terminating 0
			continue                       // ===> continue main loop

		case '\n': //=== end of line ===
			p++

			p, _ = skip_blanks(script, p)

			start_of_new_line_flag = true // p is at character following blanks in start of line, or at terminating 0
			continue                      // ===> continue main loop

		default:
			if start_of_new_line_flag == false { // if not start of line, it is a normal character. Break out of switch.
				break
			}

			switch {
			case (c == 'g' || c == 'G') && (script[p+1] == 'o' || script[p+1] == 'O'):
				if cc = script[p+2]; cc == ';' || cc == ' ' || cc == '-' || cc == '\n' || cc == '\r' || cc == '\t' || cc == '\f' || cc == '\v' || cc == 0 {
					batch_command = BE_BATCH_GO
					goto OUT_OF_LOOP // GO has been found. ===> get out of main loop.
				}

			case (c == 'e' || c == 'E') && (script[p+1] == 'x' || script[p+1] == 'X') && (script[p+2] == 'i' || script[p+2] == 'I') && (script[p+3] == 't' || script[p+3] == 'T'):
				if cc = script[p+4]; cc == ';' || cc == ' ' || cc == '-' || cc == '\n' || cc == '\r' || cc == '\t' || cc == '\f' || cc == '\v' || cc == 0 || cc == '(' {
					batch_command = BE_BATCH_EXIT
					goto OUT_OF_LOOP // EXIT has been found. ===> get out of main loop.
				}

			case (c == 'q' || c == 'Q') && (script[p+1] == 'u' || script[p+1] == 'U') && (script[p+2] == 'i' || script[p+2] == 'I') && (script[p+3] == 't' || script[p+3] == 'T'):
				if cc = script[p+4]; cc == ';' || cc == ' ' || cc == '-' || cc == '\n' || cc == '\r' || cc == '\t' || cc == '\f' || cc == '\v' || cc == 0 {
					batch_command = BE_BATCH_QUIT
					goto OUT_OF_LOOP // QUIT has been found. ===> get out of main loop.
				}
			}

			// GO, EXIT of QUIT not found. Current character c is ordinary.
		}

		// skip current character and continue

		start_of_new_line_flag = false

		p++
	}

	// ===== here, p points to terminating 0, or at GO, EXIT or QUIT =====

OUT_OF_LOOP:
	p_batch_endx = p
	batch_repeat_number = 1 // default

	if p_batch_endx == p_script_end_0 { // p is terminating 0. GO, EXIT or QUIT not found
		batch_element = &Batch_element{
			Be_batch_start:         start_pos,
			Be_batch_endx:          p_batch_endx, // index of terminating 0
			Be_batch_command:       BE_BATCH_GO,
			Be_batch_repeat_number: 1,

			Be_next_batch_start: p_batch_endx,
		}

		return FIND_BATCH_SUCCESS, batch_element // ======> return
	}

	// ===== GO has been found =====

	switch batch_command {
	case BE_BATCH_GO:
		p += 2 // points to semicolon, space, \r, \n, \t, \v, \f, or terminating 0
		p, blank_found_flag = skip_blanks(script, p)

		if blank_found_flag && isdigit(script[p]) { // if GO is followed by blanks and at least one digit, eat the positive number and all blanks that follow
			if p, batch_repeat_number, err = parse_int64(script, p); err != nil {
				return FIND_BATCH_ERROR_GO_SYNTAX, &Batch_element{Be_batch_start: start_pos}
			}

			p, _ = skip_blanks(script, p)
		}

		// here, p points to any character after "GO", "GO    ", "GO 123", or "GO    123   ", which may be in particular -, \n, or terminating 0.

		// if -- comment, skip it

		if script[p] == '-' {
			if script[p+1] != '-' { // bad syntax, not a -- comment
				return FIND_BATCH_ERROR_GO_SYNTAX, &Batch_element{Be_batch_start: start_pos}
			}

			p += 2
			p = skip_rest_of_line(script, p)

		} // here, p points to \n or terminating 0

		// check that the whole line has been eaten

		if cc = script[p]; cc != '\n' && cc != 0 {
			return FIND_BATCH_ERROR_GO_SYNTAX, &Batch_element{Be_batch_start: start_pos}
		}

		// ===== EXIT has been found =====

	case BE_BATCH_EXIT: // valid syntax :    EXIT      EXIT()      EXIT(select
		p += 4 // points to semicolon, space, \r, \n, \t, \v, \f, or terminating 0

		p = skip_rest_of_line(script, p) // for EXIT, all the line is included in the batch, because it is considered as a T-SQL statement by rsql

	// ===== QUIT has been found =====

	case BE_BATCH_QUIT:
		p += 4 // points to semicolon, space, \r, \n, \t, \v, \f, or terminating 0
		p, _ = skip_blanks(script, p)

		// if -- comment, skip it

		if script[p] == '-' {
			if script[p+1] != '-' { // bad syntax, not a -- comment
				return FIND_BATCH_ERROR_QUIT_SYNTAX, &Batch_element{Be_batch_start: start_pos}
			}

			p += 2
			p = skip_rest_of_line(script, p)

		} // here, p points to \n or terminating 0

		// check that the whole line has been eaten

		if cc = script[p]; cc != '\n' && cc != 0 {
			return FIND_BATCH_ERROR_QUIT_SYNTAX, &Batch_element{Be_batch_start: start_pos}
		}

	default:
		panic("batch_command unknown")
	}

	// ==== GO, EXIT or QUIT : eat one end of line if any ===

	assert(script[p] == '\n' || script[p] == 0)

	if script[p] == '\n' {
		p++
	}

	if batch_command == BE_BATCH_EXIT {
		p_batch_endx = p
	}

	// here, p points to start of line following the GO, EXIT or QUIT, or to terminating 0

	if batch_command == BE_BATCH_GO && (batch_repeat_number <= 0 || p_batch_endx == start_pos) { // e.g. a "GO" at begining of a script, or "GO 0"
		return FIND_BATCH_EMPTY, &Batch_element{Be_batch_start: start_pos, Be_next_batch_start: p}
	}

	// fill out result

	batch_element = &Batch_element{
		Be_batch_start:         start_pos,           // index of start of batch
		Be_batch_endx:          p_batch_endx,        // index of "GO" or "QUIT". For "EXIT", index of start of next line or terminating 0 that follows. Else, index of terminating 0.
		Be_batch_command:       batch_command,       // BE_BATCH_GO  BE_BATCH_EXIT  BE_BATCH_QUIT
		Be_batch_repeat_number: batch_repeat_number, // 1, or argument of "GO"

		Be_next_batch_start: p, // index of start of the line following the GO, EXIT or QUIT, or index of terminating 0
	}

	return FIND_BATCH_SUCCESS, batch_element // ======> return
}

//=============================================================
//                      Script splitter
//=============================================================

type Script_splitter struct {
	batchname     string // only used for error message. It can be the file path of the batch.
	script_text_0 []byte // script file, terminated by \0, because the splitting function needs it

	last_batch *Batch_element

	batch_start_line_no int
	batch_start_p       int
}

func New_script_splitter(file_text []byte, batchname string) *Script_splitter {

	sp := &Script_splitter{}

	switch {
	case len(file_text) == 0:
		sp.script_text_0 = []byte{0} // script must be terminated by \0

	default:
		if file_text[len(file_text)-1] != 0 {
			file_text = append(file_text, 0) // script must be terminated by \0
		}

		sp.script_text_0 = file_text
	}

	sp.batchname = batchname

	sp.batch_start_line_no = 1

	return sp
}

func (sp *Script_splitter) Next_batch() (batch_text []byte, err error) {
	var (
		batch_rc Batch_rc_t
		batch    *Batch_element
	)

	for {
		batch_rc, batch = Script_find_next_batch(sp.script_text_0, sp.last_batch) // for first call of Next_batch(), sp.last_batch is nil

		sp.last_batch = batch

		sp.Update_batch_start_line_count(batch.Be_batch_start)

		switch batch_rc {
		case FIND_BATCH_SUCCESS:
			batch_text := sp.script_text_0[batch.Be_batch_start:batch.Be_batch_endx]

			return batch_text, nil

		case FIND_BATCH_EMPTY:
			continue // the "for" loop is just to skip empty batches

		case FIND_BATCH_NO_MORE:
			return nil, nil

		case FIND_BATCH_ERROR_GO_SYNTAX:
			return nil, fmt.Errorf("GO syntax error for batch %s:%d", sp.batchname, sp.batch_start_line_no)

		case FIND_BATCH_ERROR_EXIT_SYNTAX:
			return nil, fmt.Errorf("EXIT syntax error for batch %s:%d", sp.batchname, sp.batch_start_line_no)

		case FIND_BATCH_ERROR_QUIT_SYNTAX:
			return nil, fmt.Errorf("QUIT syntax error for batch %s:%d", sp.batchname, sp.batch_start_line_no)

		default:
			panic("impossible")
		}
	}
}

func (sp *Script_splitter) Update_batch_start_line_count(batch_start int) {

	if batch_start <= sp.batch_start_p {
		return
	}

	if batch_start > len(sp.script_text_0) {
		batch_start = len(sp.script_text_0)
	}

	script_section := sp.script_text_0[sp.batch_start_p:batch_start]

	lf_count := 0

	for _, c := range script_section {
		if c == '\n' {
			lf_count++
		}
	}

	sp.batch_start_p = batch_start
	sp.batch_start_line_no += lf_count
}

func (sp *Script_splitter) Current_batch_line_no() int {

	return sp.batch_start_line_no
}
