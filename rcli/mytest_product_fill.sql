/*==============================================
*                                              *
*         fill in table product                *
*                                              *
==============================================*/


USE mytest;

TRUNCATE TABLE product;


-- =========== fill in table product =============

SET NOCOUNT ON
BEGIN TRAN

INSERT INTO product VALUES (500, 'Round steak', '1 kg', 17.85)
INSERT INTO product VALUES (501, 'Sirloin steak', '1 kg', 23.99)
INSERT INTO product VALUES (502, 'Prime rib roast', '1 kg', 31.36)
INSERT INTO product VALUES (503, 'Blade roast', '1 kg', 15.85)
INSERT INTO product VALUES (504, 'Stewing beef', '1 kg', 16.1)
INSERT INTO product VALUES (505, 'Ground beef, regular', '1 kg', 12.4)
INSERT INTO product VALUES (506, 'Pork chops', '1 kg', 12.55)
INSERT INTO product VALUES (507, 'Chicken', '1 kg', 7.46)
INSERT INTO product VALUES (508, 'Bacon', '500 g', 6.86)
INSERT INTO product VALUES (509, 'Wieners', '450 g', 4.31)
INSERT INTO product VALUES (510, 'Canned sockeye salmon', '213 g', 4.37)
INSERT INTO product VALUES (511, 'Homogenized milk', '1 l', 2.46)
INSERT INTO product VALUES (512, 'Partly skimmed milk', '1 l', 2.31)
INSERT INTO product VALUES (513, 'Butter', '454 g', 4.83)
INSERT INTO product VALUES (514, 'Processed cheese food slices', '250 g', 2.69)
INSERT INTO product VALUES (515, 'Evaporated milk', '385 ml', 1.86)
INSERT INTO product VALUES (516, 'Eggs', '1 dz', 3.37)
INSERT INTO product VALUES (517, 'Bread', '675 g', 2.92)
INSERT INTO product VALUES (518, 'Soda crackers', '450 g', 3.09)
INSERT INTO product VALUES (519, 'Macaroni', '500 g', 1.5)
INSERT INTO product VALUES (520, 'Flour', '2.5 kg', 4.88)
INSERT INTO product VALUES (521, 'Corn flakes', '675 g', 4.79)
INSERT INTO product VALUES (522, 'Apples', '1 kg', 4.38)
INSERT INTO product VALUES (523, 'Bananas', '1 kg', 1.59)
INSERT INTO product VALUES (524, 'Grapefruits', '1 kg', 3.88)
INSERT INTO product VALUES (525, 'Oranges', '1 kg', 3.4)
INSERT INTO product VALUES (526, 'Apple juice, canned', '1.36 l', 2.06)
INSERT INTO product VALUES (527, 'Orange juice, tetra-brick', '1 l', 4.01)
INSERT INTO product VALUES (528, 'Carrots', '1 kg', 1.96)
INSERT INTO product VALUES (529, 'Celery', '1 kg', 2.16)
INSERT INTO product VALUES (530, 'Mushrooms', '1 kg', 8.64)
INSERT INTO product VALUES (531, 'Onions', '1 kg', 2.12)
INSERT INTO product VALUES (532, 'Potatoes', '4.54 kg', 6.44)
INSERT INTO product VALUES (533, 'French fried potatoes, frozen', '1 kg', 2.58)
INSERT INTO product VALUES (534, 'Baked beans, canned', '398 ml', 1.27)
INSERT INTO product VALUES (535, 'Tomatoes, canned', '796 ml', 1.58)
INSERT INTO product VALUES (536, 'Tomato juice, canned', '1.36 l', 2.47)
INSERT INTO product VALUES (537, 'Ketchup', '1 l', 3.31)
INSERT INTO product VALUES (538, 'Sugar, white', '2 kg', 2.8)
INSERT INTO product VALUES (539, 'Coffee, roasted', '300 g', 6.21)
INSERT INTO product VALUES (540, 'Coffee, instant', '200 g', 6.68)
INSERT INTO product VALUES (541, 'Tea (bags)', '72', 4.46)
INSERT INTO product VALUES (542, 'Cooking or salad oil', '1 l', 3.96)
INSERT INTO product VALUES (543, 'Soup, canned', '284 ml', 1.11)
INSERT INTO product VALUES (544, 'Baby food', '128 ml', 0.95)
INSERT INTO product VALUES (545, 'Peanut butter', '500 g', 3.42)
INSERT INTO product VALUES (546, 'Fruit flavoured crystals', '2.25 l', 1.9)
INSERT INTO product VALUES (547, 'Soft drinks, cola type', '2 l', 2.04)
INSERT INTO product VALUES (548, 'Soft drinks, lemon-lime type', '2 l', 1.92)
INSERT INTO product VALUES (549, 'Paper towels (rolls)', '2', 2.63)
INSERT INTO product VALUES (550, 'Facial tissue', '200', 2.75)
INSERT INTO product VALUES (551, 'Bathroom tissue (rolls)', '4', 2.53)
INSERT INTO product VALUES (552, 'Shampoo', '300 ml', 3.87)
INSERT INTO product VALUES (553, 'Deodorant', '60 g', 4.49)
INSERT INTO product VALUES (554, 'Toothpaste', '100 ml', 2.68)
INSERT INTO product VALUES (555, 'Cigarettes', '200', 101.78)

COMMIT
SET NOCOUNT OFF


SELECT * FROM product;


