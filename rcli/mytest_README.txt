========================================
*                                      *
*        sample database mytest        *
*                                      *
========================================

mytest is a sample database, containing random data, so that you can play with it.

The tables in mytest are:

  - country
  - product
  - customer
  - orders
  - items


Before running the scripts, you must create the database with the command:

    CREATE DATABASE mytest;


Then, you can run the scripts in the following order:

  - mytest_country_create.sql
  - mytest_country_fill.sql

  - mytest_product_create.sql
  - mytest_product_fill.sql

  - mytest_customer_create.sql
  - mytest_customer_fill.sql

  - mytest_order_create.sql
  - mytest_order_fill.sql

Country and product tables contains only a few records.

By default, 100000 customers are created, with random names, birthdate, etc.
By default, 200000 orders are created, with random data, containing a few random items.




You can read and modify the scripts, to change the number of customers or orders to create.



