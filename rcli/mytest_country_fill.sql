/*==============================================
*                                              *
*         fill in table country                *
*                                              *
==============================================*/


USE mytest;

TRUNCATE TABLE country;


-- =========== fill in table country =============

INSERT INTO country (cid, name) VALUES
    (0, 'USA'),
    (1, 'France'),
    (2, 'Italy'),
    (3, 'Germany'),
    (4, 'Russia'),
    (5, 'Japan'),
    (6, 'China'),
    (7, 'Sweden'),
    (8, 'New Zealand'),
    (9, 'UK')
;




