package main

import (
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path/filepath"

	"github.com/BurntSushi/toml"
)

//======================================================
//               configuration object
//======================================================

// Config contains the configuration values.
//
type Config struct {
	Remote_server Remote_server_t
	Login         Login_t
	Presentation  Presentation_t
	Internal      Internal_t
}

type Remote_server_t struct {
	Address string
}

type Login_t struct {
	Login_name       string
	Password         string
	Default_database string // if empty string, the connection will look up the default database for the user
}

type Presentation_t struct {
	File_encoding     string
	Null_string       string // display this string for NULL values
	Width_limit       int    // column width for VARBINARY and VARCHAR only
	Fractional_second int    // number of fractional second digits, for TIME and DATETIME only
	Print_mode        int    // PRINT mode for multiple values. 0: normal mode, 1: column aligned
	Error_level       int    // 0, 1, 2  more error information are displayed
	Error_wrap        bool   // if false, all error info is printed in one long line. Else, the error text is printed on a new line.
	Color             bool   // if true, error is printed in color.
}

type Internal_t struct {
	Keepalive_interval int // keepalive messages are sent at this rate, in seconds
}

// configuration object, initialized with default values. These values can be overridden by the configuration file, and then by the command line flags.
//
var g_cfg_default = &Config{
	Remote_server: Remote_server_t{
		Address: DEFAULT_REMOTE_SERVER,
	},
	Login: Login_t{
		Login_name:       "",
		Password:         "",
		Default_database: "",
	},
	Presentation: Presentation_t{
		File_encoding:     DEFAULT_FILE_ENCODING,
		Null_string:       DEFAULT_NULL_STRING,
		Width_limit:       DEFAULT_WIDTH_LIMIT,
		Fractional_second: DEFAULT_FRACTIONAL_SECOND,
		Error_level:       DEFAULT_ERROR_LEVEL,
		Error_wrap:        DEFAULT_ERROR_WRAP,
		Color:             DEFAULT_COLOR,
	},
	Internal: Internal_t{
		Keepalive_interval: DEFAULT_KEEPALIVE_INTERVAL,
	},
}

// Get_default_configuration returns a default configuration object.
//
func Get_default_configuration() *Config {

	cfg := &Config{}
	*cfg = *g_cfg_default // copy configuration default

	return cfg
}

// get_home_directory returns the home directory of the current user, in which the config file should be located.
//
func get_home_directory() (string, error) {
	var (
		err      error
		usr      *user.User
		home_dir string
	)

	home_dir = os.Getenv("HOME") // in a Linux system, it is the directory where configuration files are located for the current user

	if home_dir == "" { // if no $HOME environment variable
		if usr, err = user.Current(); err != nil {
			return "", err
		}

		home_dir = usr.HomeDir
	}

	return home_dir, nil
}

// Read_configuration_file reads configuration file.
// If file_path is empty string or relative path, configuration file is looked up in the current directory. If not found, it is looked up in home directory too.
// Else, file_path is absolute path.
//
// This function makes a copy of the default configuration g_cfg_default, and overrides its parameters by those found in config file.
//
func Read_configuration_file(file_path string) (*Config, error) {
	var (
		err      error
		home_dir string
		content  []byte
		md       toml.MetaData
		cfg      *Config
	)

	cfg = &Config{}
	*cfg = *g_cfg_default // copy configuration default

	if file_path == "" {
		file_path = CONFIG_FILE_NAME
	}

	switch {
	case filepath.IsAbs(file_path): // if absolute file_path
		if content, err = ioutil.ReadFile(file_path); err != nil {
			return nil, err
		}

	default: // relative file_path
		if content, err = ioutil.ReadFile(file_path); err != nil {
			if os.IsNotExist(err) == false { // file exists but error occurred
				return nil, err
			}

			// file not exists. Try looking up in home directory.

			if home_dir, err = get_home_directory(); err != nil {
				return nil, err
			}

			file_path = filepath.Join(home_dir, file_path)

			if content, err = ioutil.ReadFile(file_path); err != nil {
				if os.IsNotExist(err) {
					log.Println(err)

					Verboseln("no config file has been loaded")

					return cfg, nil // if file doesn't exist in home, we just return a copy of the default configuration
				}

				return nil, err
			}
		}
	}

	Verbosef("config file %s has been loaded\n", file_path)

	if md, err = toml.Decode(string(content), cfg); err != nil { // parameters found in config file override configuration default values
		return nil, err
	}

	if len(md.Undecoded()) != 0 {
		log.Printf("Undecoded keys in config file %s: %q\n", file_path, md.Undecoded())
	}

	return cfg, nil
}
