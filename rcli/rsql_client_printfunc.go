package main

import (
	"fmt"
	"os"
)

func EPrint(a ...interface{}) (n int, err error) {
	return fmt.Fprint(os.Stderr, a...)
}

func EPrintln(a ...interface{}) (n int, err error) {
	return fmt.Fprintln(os.Stderr, a...)
}

func EPrintf(format string, a ...interface{}) (n int, err error) {
	return fmt.Fprintf(os.Stderr, format, a...)
}

func Verbose(a ...interface{}) {
	if verbose {
		s := fmt.Sprint(a...)
		fmt.Fprintf(os.Stderr, "VERBOSE: %s\n", s)
	}
}

func Verboseln(a ...interface{}) {
	if verbose {
		s := fmt.Sprintln(a...)
		fmt.Fprintf(os.Stderr, "VERBOSE: %s\n", s)
	}
}

func Verbosef(format string, a ...interface{}) {
	if verbose {
		s := fmt.Sprintf(format, a...)
		fmt.Fprintf(os.Stderr, "VERBOSE: %s\n", s)
	}
}
