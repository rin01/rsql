package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"

	"rsql/rsqlib"
)

const LINE_SIZE_DEFAULT = 500 // good default value for size of lines printed out

var verbose bool // verbose. Global variable as we access it inside many functions, and don't want to pass it as argument to each function

func main() {
	var (
		err error

		args []string

		config_file_path       string // directory of file path of the configuration file
		config_none            bool   // no config file should be loaded
		run_files_concurrently bool   // only for 'sa' for debugging. This option can only be passed as command line flag.
		query_string_raw       string // query string
		prints_config_model    bool

		cfg *Config

		opt *rsqlib.Options
	)

	//=== settings and options ===

	// settings passed only from command-line

	flag.StringVar(&config_file_path, "config", "", fmt.Sprintf("if this flag is not set, it is \"%s\". If argument is a relative path and config file is not found, home dir is searched.", CONFIG_FILE_NAME))
	flag.BoolVar(&config_none, "noconfig", false, "no configuration file is loaded. Override \"config\" flag")
	flag.BoolVar(&run_files_concurrently, "concurrent", false, "for 'sa' only: run files concurrently, creating one connection per file")
	flag.BoolVar(&verbose, "v", false, "verbose")
	flag.StringVar(&query_string_raw, "Q", "", "query string")
	flag.BoolVar(&prints_config_model, "config_model", false, "prints a model configuration file for rcli client: rcli -config_model > ~/rcli.conf; chmod 600 ~/rcli.conf")

	// settings that override config files settings

	_ = flag.String("server", DEFAULT_REMOTE_SERVER, "server[:port]")
	_ = flag.String("S", DEFAULT_REMOTE_SERVER, "shortcut for \"server\"")

	_ = flag.String("login", "", "login")
	_ = flag.String("U", "", "shortcut for \"login\"")

	_ = flag.String("password", "", "password")
	_ = flag.String("P", "", "shortcut for \"password\"")

	_ = flag.String("database", "", "database")
	_ = flag.String("d", "", "shortcut for \"database\"")

	_ = flag.String("null", DEFAULT_NULL_STRING, "string displayed by SELECT for NULL values")
	_ = flag.Int("width", DEFAULT_WIDTH_LIMIT, "column width for VARBINARY and VARCHAR only")
	_ = flag.Int("fracsec", DEFAULT_FRACTIONAL_SECOND, "number of fractional second digits, for TIME and DATETIME only")
	_ = flag.Int("print_mode", DEFAULT_PRINT_MODE, "PRINT mode for multiple values. 0: normal mode, 1: column aligned")
	_ = flag.Int("err_level", 0, "0: normal error display, 1: more error info, 2: also print stacktrace")
	_ = flag.Bool("err_wrap", false, "if true, error message is printed in a new line")

	_ = flag.String("encoding", DEFAULT_FILE_ENCODING, "file encoding, e.g. iso8859-1, cp1252. See http://www.w3.org/TR/encoding for list of encoding names")

	_ = flag.Bool("color", false, "errors are displayed in red")

	_ = flag.Int("keepalive_interval", DEFAULT_KEEPALIVE_INTERVAL, "keepalive messages sending rate, in seconds")

	// options to send to server

	opt = &rsqlib.Options{}

	flag.BoolVar(&opt.Showtree, "showtree", false, "show AST tree for debugging")
	flag.BoolVar(&opt.Showtree, "t", false, "shortcut for \"showtree\"")
	flag.BoolVar(&opt.No_cf, "no_cf", false, "for 'sa' only: disable constant folding for debugging")
	flag.BoolVar(&opt.No_exec, "noexec", false, "batches are compiled, but not run")

	flag.Parse()

	if prints_config_model {
		fmt.Println(G_RCLI_CONF)
		os.Exit(0)
	}

	args = flag.Args()

	switch {
	case len(args) == 0 && query_string_raw == "":
		log.Fatalf("no filename passed as argument.")
	case len(args) != 0 && query_string_raw != "":
		log.Fatalf("query string cannot be passed with filename arguments.")
	}

	// fill configuration object from config file

	if config_none {
		Verboseln("no config file has been loaded")

		cfg = Get_default_configuration() // copy configuration default
	} else {
		if cfg, err = Read_configuration_file(config_file_path); err != nil { // read configuration file and return configuration object
			log.Fatal(err)
		}
	}

	fn := func(flg *flag.Flag) { // closure, to override cfg configuration for each flag passed by command line

		//fmt.Printf("xxx  %-14s  %-s\n", flg.Name, flg.Value)

		switch flg.Name {
		case "config",
			"noconfig",
			"concurrent",
			"v",
			"Q",

			"showtree",
			"t",
			"no_cf",
			"noexec":
			// these options can only be passed as command line options, and not from config file

		case "server", "S":
			cfg.Remote_server.Address = flg.Value.(flag.Getter).Get().(string)

		case "login", "U":
			cfg.Login.Login_name = flg.Value.(flag.Getter).Get().(string)

		case "password", "P":
			cfg.Login.Password = flg.Value.(flag.Getter).Get().(string)

		case "database", "d":
			cfg.Login.Default_database = flg.Value.(flag.Getter).Get().(string)

		case "null":
			cfg.Presentation.Null_string = flg.Value.(flag.Getter).Get().(string)

		case "width":
			cfg.Presentation.Width_limit = flg.Value.(flag.Getter).Get().(int)

		case "fracsec":
			cfg.Presentation.Fractional_second = flg.Value.(flag.Getter).Get().(int)

		case "print_mode":
			cfg.Presentation.Print_mode = flg.Value.(flag.Getter).Get().(int)

		case "err_level":
			cfg.Presentation.Error_level = flg.Value.(flag.Getter).Get().(int)

		case "err_wrap":
			cfg.Presentation.Error_wrap = flg.Value.(flag.Getter).Get().(bool)

		case "encoding":
			cfg.Presentation.File_encoding = flg.Value.(flag.Getter).Get().(string)

		case "color":
			cfg.Presentation.Color = flg.Value.(flag.Getter).Get().(bool)

		case "keepalive_interval":
			cfg.Internal.Keepalive_interval = flg.Value.(flag.Getter).Get().(int)

		default:
			panic("unknown flag: " + flg.Name)
		}
	}

	flag.Visit(fn) // override parameters in configuration object cfg with flag values

	// sanitize some parameters

	if strings.ContainsRune(cfg.Remote_server.Address, ':') == false {
		cfg.Remote_server.Address = fmt.Sprintf("%s:%s", cfg.Remote_server.Address, DEFAULT_REMOTE_PORT)
	}

	cfg.Login.Login_name = strings.ToLower(cfg.Login.Login_name)

	cfg.Login.Default_database = strings.ToLower(cfg.Login.Default_database)

	// some options can only be set by 'sa'

	if (opt.No_cf == true || run_files_concurrently == true) && cfg.Login.Login_name != "sa" {
		log.Fatalf("\"no_cf\", \"concurrent\" options can only be set by 'sa'")
	}

	//=== process files ===

	Verbosef("connection address: \"%s\", login: \"%s\", database: \"%s\"", cfg.Remote_server.Address, cfg.Login.Login_name, cfg.Login.Default_database)

	switch {
	case query_string_raw != "": // process one query string
		var (
			err     error
			session *rsqlib.Session
		)

		if strings.Contains(query_string_raw, "$") {
			log.Fatalf("The symbol $ is not allowed in query string, because bash will expand it as a bash variable if you forget to escape it.")
		}

		//=== send login info to server ===

		if session, err = rsqlib.Connect(cfg.Remote_server.Address, cfg.Login.Login_name, cfg.Login.Password, cfg.Login.Default_database, opt, cfg.Internal.Keepalive_interval); err != nil { // expects RESTYP_LOGIN_SUCCESS
			log.Fatalf("login failed for \"%s\"", cfg.Login.Login_name) // because err is just "EOF", as server dropped the connection when login failed
		}
		defer session.Close()

		//=== process the query string ===

		var query_string []byte
		if query_string, err = Read_query_string_in_utf8(query_string_raw, cfg.Presentation.File_encoding); err != nil {
			log.Fatalf("--- %s ---", err)
		}

		if err = process_script(session, cfg, "<query string>", query_string); err != nil { // second argument is in fact file_name, but it is used only to display a batch name in error message
			log.Fatalf("--- %s ---", err)
		}

	case run_files_concurrently == true: // process many files concurrently
		var wg sync.WaitGroup

		for i, file_path := range args {
			wg.Add(1)
			go New_connection_processes_file(&wg, i, cfg, file_path, opt, cfg.Internal.Keepalive_interval) // each goroutine processes one file
		}

		wg.Wait()

	default: // process many files sequentially
		var (
			err     error
			session *rsqlib.Session
		)

		//=== send login info to server ===

		if session, err = rsqlib.Connect(cfg.Remote_server.Address, cfg.Login.Login_name, cfg.Login.Password, cfg.Login.Default_database, opt, cfg.Internal.Keepalive_interval); err != nil { // expects RESTYP_LOGIN_SUCCESS
			log.Fatalf("login failed for \"%s\"", cfg.Login.Login_name) // because err is just "EOF", as server dropped the connection when login failed
		}
		defer session.Close()

		//=== process all files sequentially ===

		for _, file_path := range args {
			if err = process_file(session, cfg, file_path); err != nil {
				log.Fatalf("--- %s ---", err)
			}
		}
	}

}

// New_connection_processes_file creates a new connection, and process a file.
// It must be called as "go New_connection_processes_file()", so that a goroutine runs it.
//
func New_connection_processes_file(wg *sync.WaitGroup, i int, cfg *Config, file_path string, opt *rsqlib.Options, keepalive_interval int) {
	var (
		err     error
		session *rsqlib.Session
	)

	defer wg.Done()

	//=== send login info to server ===

	if session, err = rsqlib.Connect(cfg.Remote_server.Address, cfg.Login.Login_name, cfg.Login.Password, cfg.Login.Default_database, opt, keepalive_interval); err != nil { // expects RESTYP_LOGIN_SUCCESS
		log.Printf("goroutine %d: login failed for \"%s\"", i, cfg.Login.Login_name) // because err is just "EOF", as server dropped the connection when login failed
		return
	}
	defer session.Close()

	//=== process file ===

	if err = process_file(session, cfg, file_path); err != nil {
		log.Printf("goroutine %d: --- %s ---", i, err)
		return
	}
}

func process_file(session *rsqlib.Session, cfg *Config, file_path string) error {
	var (
		err         error
		script_text []byte
	)

	//=== read SQL script file ===

	if script_text, err = Read_file_in_utf8(file_path, cfg.Presentation.File_encoding); err != nil {
		return err
	}

	return process_script(session, cfg, file_path, script_text)
}

func process_script(session *rsqlib.Session, cfg *Config, file_path string, script_text []byte) error {
	var (
		err error

		resp rsqlib.Response_t

		script_splitter *Script_splitter
		batch_text      []byte

		record []rsqlib.IField
	)

	//=== send batches to server ===

	script_splitter = New_script_splitter(script_text, file_path)

	var stop_batch_processing_flag bool

	for {
		if stop_batch_processing_flag == true { // error with state 127 (only THROW or ERROR_SERVER_ABORT can generate it). Don't process subsequent batches.
			return nil
		}

		if batch_text, err = script_splitter.Next_batch(); err != nil { // syntax error in line containing "GO", "EXIT" or "QUIT" followed by illegal characters, e.g. "GO abc"
			return err
		}

		if batch_text == nil { // no more batch
			return nil
		}

		log.Printf("--- executing batch %s:%d ---\n", file_path, script_splitter.Current_batch_line_no())

		if err = session.Send_batch(batch_text); err != nil {
			return err
		}

		Verboseln("batch sent to server")

		//=== read response ===

	LABEL_READ_RESPONSE_LOOP:
		for {
			var (
				rc int64
			)

			if resp, err = session.Read_response_type(); err != nil {
				return err
			}

			switch resp {
			case rsqlib.RESTYP_RECORD_LAYOUT:
				var colname_list []string
				var buff []byte = make([]byte, 0, LINE_SIZE_DEFAULT)
				var linebuff []byte = make([]byte, 0, LINE_SIZE_DEFAULT)

				// create colname list

				if colname_list, err = session.Create_colname_list(); err != nil {
					return err
				}

				// create record

				if record, err = session.Create_row(); err != nil {
					return err
				}

				// print record header

				for i, field := range record {
					col_width := stringval_col_width(field, cfg.Presentation.Width_limit, cfg.Presentation.Fractional_second)
					buff = append(buff, fmt.Sprintf("%-*.*s", col_width, col_width, colname_list[i])...)
					buff = append(buff, '|')

					linebuff = append(linebuff, strings.Repeat("-", col_width)...)
					linebuff = append(linebuff, '+')
				}

				fmt.Printf("%s\n", buff)
				fmt.Printf("%s\n", linebuff)

			case rsqlib.RESTYP_RECORD:
				var buff []byte = make([]byte, 0, LINE_SIZE_DEFAULT)

				// fill record

				if err = session.Fill_row_with_values(record); err != nil {
					return err
				}

				// print record fields

				for _, field := range record {
					buff = append(buff, stringval_formatted(field, cfg.Presentation.Null_string, cfg.Presentation.Width_limit, cfg.Presentation.Fractional_second)...)
					buff = append(buff, '|')
				}

				fmt.Printf("%s\n", buff)

			case rsqlib.RESTYP_RECORD_FINISHED:
				var record_count int64

				if record_count, err = session.Read_int64(); err != nil {
					return err
				}

				fmt.Printf("\n(%d row(s) affected)\n\n", record_count)

				// discard record

				record = nil

			case rsqlib.RESTYP_EXECUTION_FINISHED:
				var record_count int64

				if record_count, err = session.Read_int64(); err != nil {
					return err
				}

				if record_count >= 0 { // NEGATIVE VALUES ARE NOT USED YET (perhaps in the future): if -1, the command doesn't work on rows, like CREATE USER, etc.
					fmt.Printf("(%d row(s) affected)\n\n", record_count)
				}

			case rsqlib.RESTYP_PRINT:
				var row []rsqlib.IField
				var buff []byte = make([]byte, 0, 100)

				// create row

				if row, err = session.Create_row(); err != nil {
					return err
				}

				if err = session.Fill_row_with_values(row); err != nil {
					return err
				}

				// print row values

				if len(row) == 1 {
					fmt.Println(row[0].String()) // print the value completely
					break
				}

				switch cfg.Presentation.Print_mode {
				case 0:
					for _, field := range row {
						buff = append(buff, field.String()...)
						buff = append(buff, ' ')
					}

					if len(row) > 0 {
						buff = buff[:len(buff)-1]
					}

				default:
					for _, field := range row {
						buff = append(buff, stringval_formatted(field, cfg.Presentation.Null_string, cfg.Presentation.Width_limit, cfg.Presentation.Fractional_second)...)
						buff = append(buff, '|')
					}
				}

				fmt.Printf("%s\n", buff)

			case rsqlib.RESTYP_MESSAGE:
				var msg_string string

				if msg_string, err = session.Read_string(); err != nil {
					return err
				}

				fmt.Println(msg_string)

			case rsqlib.RESTYP_ERROR:
				var error_info *rsqlib.Error_info

				if error_info, err = session.Read_Error_info(); err != nil {
					return err
				}

				err_msg := error_info.String_format(cfg.Presentation.Error_level, cfg.Presentation.Error_wrap)
				if cfg.Presentation.Color {
					err_msg = "\033[0;31m" + err_msg + "\033[0m"
				}
				log.Println(err_msg)

				if error_info.State() == 127 { // if state == 127 (only THROW or ERROR_SERVER_ABORT can generate it)
					stop_batch_processing_flag = true // don't run subsequent batches
					log.Printf("error state 127: stop processing subsequent batches in this file.\n")
				}

			case rsqlib.RESTYP_BATCH_END: // batch is finished, no more messages are expected from server for this batch
				if rc, err = session.Read_batch_end_RC(); err != nil {
					return err
				}

				log.Printf("return code: %d\n\n", rc) // prints blank line after the return code

				break LABEL_READ_RESPONSE_LOOP // exit from response loop

			default:
				panic("impossible")
			}
		} // end of response loop

	} // end of batches loop

	return nil
}
