create database dbic
go

declare @i int = 0

while @i<3000000
  begin
  sleep 0.2
  set @i+=1
  end


quit

declare @i int = 0 while @i<3000000  begin  sleep 0.2  set @i+=1  end

--show p

/*
use dbic

drop table PRENEURS_PAYEURS

go


use dbic

CREATE TABLE [dbo].[PRENEURS_PAYEURS] (
	[NO_PERSONNE] [int] NOT NULL ,
	[TITRE_CIVIL] [varchar] (30)  NULL ,
	[NOM] [varchar] (60)  NULL ,
	[PRENOM] [varchar] (30)  NULL ,
	[RUE] [varchar] (40)  NULL ,
	[COMPLEMENT] [varchar] (30)  NULL ,
	[NOM_ABREGE] [varchar] (45)  NULL ,
	[CASE_POSTALE] [varchar] (20)  NULL ,
	[NO_POSTAL] [varchar] (5)  NULL ,
	[LOCALITE] [varchar] (30)  NULL ,
	[CANTON_PAYS] [varchar] (2)  NULL ,
	[TELEPHONE] [varchar] (16)  NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_ENTREE] [datetime] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[TITULARISATION] [varchar] (3)  NULL ,
	[CODE_SECTEUR_ACTIVITE] [varchar] (6)  NULL ,
	[TYPE_PERSONNE] [tinyint] NULL ,
	[NO_COMMUNE] [varchar] (4)  NULL ,
	[CODE_ORGANISATION] [varchar] (2)  NULL ,
	[CODE_SEGMENT_MARCHE] [tinyint] NULL ,
	[NOMBRE_COLLABORATEURS] [smallint] NULL ,
	[NO_AGC] [tinyint] NULL ,
	[NO_AGT] [smallint] NULL ,
	[DATE_DECES] [datetime] NULL 
) ON [PRIMARY]

ALTER TABLE [dbo].[PRENEURS_PAYEURS] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRENEURS_PAYEURS] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 

go

CREATE  UNIQUE  INDEX [IDX_PRENPAY_NOMPREN] ON [dbo].[PRENEURS_PAYEURS]([NOM], [PRENOM], [LOCALITE], [RUE], [NO_PERSONNE], [CODE_SEGMENT_MARCHE]) ON [PRIMARY]

quit
*/

--ALTER SERVER PARAMETER SET SERVER_KEEPALIVE_TIMEOUT = 5
--quit

use dbic

print 1/0

go

print 123

quit


truncate table preneurs_payeurs

bulk insert dbic..preneurs_payeurs FROM '/home/nico/dbic_files/PRENEURS_PAYEURS.txt' WITH ( codepage='acp', NEWIDENTITY, KEEPNULLS );




quit


print 111111111

go

print 2222222222

go

print 33333333333

show sql p



--bulk insert dbic..preneurs_payeurs FROM '/home/nico/dbic_files/PRENEURS_PAYEURS.txt' WITH ( codepage='acp', NEWIDENTITY, KEEPNULLS );


quit

--ALTER SERVER PARAMETER SET SERVER_KEEPALIVE_TIMEOUT = 1
--ALTER SERVER PARAMETER SET SERVER_READ_TIMEOUT = 15
--quit


print 123

declare @i int = 0

while 1=1
  begin
  --print 'ho ' + cast(@i as varchar)
  set @i+=1
  end
  

print 456
quit


--show p
--quit

select * from preneurs_payeurs -- where nom = 'riesch'

truncate table preneurs_payeurs


bulk insert dbic..preneurs_payeurs FROM '/home/nico/dbic_files/PRENEURS_PAYEURS.txt' WITH ( codepage='acp', NEWIDENTITY, KEEPNULLS );

quit


CREATE DATABASE mydb;


go

   USE mydb;

    CREATE TABLE t(id INT NOT NULL IDENTITY, name VARCHAR(20));
    SHOW T;
go

USE mydb;

INSERT INTO t VALUES ('Fred'), ('Maria'), ('Omer');

go







    SET NOCOUNT ON;

    DECLARE @i INT = 0;

    BEGIN TRAN;

    WHILE @i<10
        BEGIN
        INSERT t(name) VALUES ('Row_' + CAST(@i AS VARCHAR));
        SET @i += 1;
        END

    COMMIT;

    SELECT * FROM t;



quit



    IF OBJECT_ID ('mytable', 'U') IS NOT NULL
        DROP TABLE mytable;

    CREATE TABLE mytable (a INT IDENTITY NOT NULL, b VARCHAR(50) NULL);
    GO

    INSERT mytable VALUES ('Row #1'), ('Row #2');
    SELECT * FROM mytable;
    GO

    DROP TABLE mytable;


quit

    IF OBJECT_ID ('mytable2', 'U') IS NOT NULL
            drop table mytable2


    CREATE TABLE mytable2 (
		a INT           NOT NULL IDENTITY,
		b CHAR(10)    NULL,
		c CHAR(10)    NULL,
		d CHAR(6)       NULL,
		e NUMERIC(12,2) NULL,
		f DATETIME      NULL,
		g varbinary(4) null
	    );
go

    PRINT 'Truncate mytable';
    TRUNCATE TABLE mytable2;

    BULK INSERT mytable2 FROM 'mytable_content.txt' WITH ( FIELDTERMINATOR = '|', NEWIDENTITY, KEEPNULLS );

    SELECT *
    FROM mytable2;

quit







    IF OBJECT_ID ('mytable', 'U') IS NULL
	    CREATE TABLE mytable (
		a INT           NOT NULL IDENTITY,
		b VARCHAR(10)    NULL,
		c VARCHAR(10)    NULL,
		d CHAR(6)       NULL,
		e NUMERIC(12,2) NULL,
		f DATETIME      NULL
	    );
    GO

    declare @i int = 0

set nocount on

        TRUNCATE TABLE mytable;

    begin tran

    INSERT INTO mytable(b, c, d, e, f)
        VALUES ('Row #1' , NULL , NULL , NULL   , NULL      ),
               ('Row #2' , ''   , ''   , 123    , '20160201'),
               ('Row #3' , 'ab' , 'cd' , 123.45 , '20160201');

    WHILE @i < 5
      BEGIN
      INSERT INTO mytable(b, c, d, e, f)
          VALUES('Row #' + format(@i, '00000'), 'c_' + cast(@i as varchar), 'd_' + cast(@i as varchar), @i+0.25, dateadd(d, @i, cast('20160101' as date)));
      SET @i+=1
      END


set nocount off

    SELECT *
    FROM mytable;

    BULK EXPORT mytable TO 'mytable_content.txt' WITH ( FIELDTERMINATOR = '|' );

    PRINT 'Truncate mytable';
    TRUNCATE TABLE mytable;

    BULK INSERT mytable FROM 'mytable_content.txt' WITH ( FIELDTERMINATOR = '|', NEWIDENTITY, KEEPNULLS );

    COMMIT

    SELECT *
    FROM mytable where b < 'Row #00010';

quit


update mytable set b = b+'_x'
from mytable

SELECT * FROM mytable
where a between 40000 and 40010;

quit




IF OBJECT_ID ('mytable', 'U') IS NOT NULL
    DROP TABLE mytable;

CREATE TABLE mytable (a INT IDENTITY NOT NULL, b VARCHAR(50) NULL);
GO

SET NOCOUNT ON;

DECLARE @i INT = 0;

BEGIN TRAN;

WHILE @i<50000
    BEGIN
    INSERT mytable VALUES ('Row_' + CAST(@i AS VARCHAR));
    SET @i += 1;
    END

COMMIT;

--SELECT * FROM mytable;

show id t mytable

--UPDATE mytable SET a = 5000;

--SELECT * FROM mytable;

quit










IF OBJECT_ID ('mytable2', 'U') IS NOT NULL
    DROP TABLE mytable2;

CREATE TABLE mytable2 (col1 VARCHAR(100) NULL, col2 NUMERIC(12,2) NULL, col3 DATE NULL);
GO

INSERT INTO mytable2(col1, col2, col3)
SELECT UPPER(b), a*1.5, DATEADD(d, a, CAST('20160101' AS DATE))
FROM mytable
WHERE a<3;

SELECT * FROM mytable2;

quit



if object_id('[employees]') is not null
    begin
    show t employees
    drop table employees
    end


if object_id('employees2') is not null
    begin
    show t employees2
    drop table employees2
    end

go

CREATE TABLE employees (
    id           BIGINT      NOT NULL CONSTRAINT pk PRIMARY KEY nonCLUSTERED,
    name         VARCHAR(30) NOT NULL,
    first_name   VARCHAR(30) NOT NULL,
    birth_date   DATE        NULL index idx_birthdate_col,
    badge_number INT         NULL,

    INDEX idx_name(name, first_name),          -- nonclustered index
    CONSTRAINT uq_badge UNIQUE(badge_number),  -- nonclustered index
)

GO

show t employees
drop table employees

    CREATE TABLE employees (
        id             BIGINT      NULL,
        name           VARCHAR(30) NOT NULL,
        first_name     VARCHAR(30) NOT NULL,
        date_of_birth  DATE        NULL,
        badge_number   INT         NULL,
    )

    ALTER TABLE employees ALTER COLUMN id NOT NULL;

    ALTER TABLE employees ADD CONSTRAINT pk PRIMARY KEY CLUSTERED (id); 

    ALTER TABLE employees ALTER COLUMN date_of_birth WITH NAME = birth_date;

    ALTER TABLE employees ADD CONSTRAINT uq_badge UNIQUE (badge_number);

    CREATE INDEX idx_name ON employees (name, first_name);

    SHOW T employees;


quit


    INSERT INTO employees VALUES (1040, 'PAVLOV',    'Igor',  '19660705', 54700),
                                 (1003, 'SMITH',     'John',  '19700605', 53040),
                                 (1032, 'BOULANGER', 'Jean',  '19901204', 50001),
                                 (1035, 'MULLER',    'Franz', '19550823', 51340),
                                 (1020, 'SANCHEZ',   'Pedro', '19401003', 50033);

go 0

create unique clustered index uq_badge_bis on employees(badge_number asc)

alter table employees alter column badge_number not null
alter table employees alter column id with name = id2
alter table employees alter column name with name = name2
alter table employees alter column first_name with name = first_name2
alter table employees alter column birth_date with name = birth_date2
alter table employees alter column badge_number with name = badge_number2

alter index pk on employees with name = pk2
alter index uq_badge on employees with name = uq_badge2
alter index uq_badge_bis on employees with name = uq_badge_bis2
alter index idx_birthdate_col on employees with name = idx_birthdate_col2
alter index idx_name on employees with name = idx_name2

alter table employees alter column birth_date2 null

alter table employees drop constraint pk2
alter table employees drop constraint uq_badge2
alter table employees drop constraint idx_birthdate_col2
alter table employees drop constraint idx_name2

/*
drop index uq_badge2 on employees
drop index uq_badge_bis2 on employees
drop index idx_birthdate_col2 on employees
drop index idx_name2 on employees
*/


show t employees
--show id t employees


exit



declare @i int = 0
begin tran

while @i<5000
  begin
  insert into employees values(@i+1000, 'x_' + cast(@i as varchar), 'john', null, @i+5000)
  set @i+= 1
  end

commit

/*
    INSERT INTO employees VALUES (1040, 'PAVLOV',    'Igor',  '19660705', 54700),
                                 (1003, 'SMITH',     'John',  '19700605', 53040),
                                 (1032, 'BOULANGER', 'Jean',  '19901204', 50001),
                                 (1035, 'MULLER',    'Franz', '19550823', 51340),
                                 (1020, 'SANCHEZ',   'Pedro', '19401003', 50033);
*/

go


create index idx_birthdate on employees(birth_date);

show t employees
show id t employees

--create unique index idx_birthdate2 on employees(birth_date);








quit

drop table t1

CREATE TABLE t1( a int not null, b varchar(20), c float not null, d datetime, INDEX ix_1 NONCLUSTERED (a)) 

create unique index prout on t1 (c,d)

show t t1

quit






quit



use clients
show perm t


quit

USE aaa; create USER gaiuss for login gaius;
--USE bbb; DROP USER dbo;
USE bof; create USER gaga for login gaius;
USE mydb; create USER gaius for login gaius;

show all u
exit




exit

ALTER SERVER PARAMETER SET SERVER_DEFAULT_DATABASE=pom
go
create login marc with password='marc'



show l

exit


ALTER SERVER PARAMETER SET SERVER_DEFAULT_DATABASE=pom
ALTER SERVER PARAMETER SET SERVER_DEFAULT_LANGUAGE=it_ch
ALTER SERVER PARAMETER SET SERVER_BULK_DIR = '/home/nico/z_go/src/inut/inut_ast/prout/bulkdir'

show p

exit

--create table t(a int, b varchar(20))
print '0000'
ALTER SERVER PARAMETER SET SERVER_QUOTED_IDENTIFIER = off
show p

go

print '1111'
show p
select * from "t"

go

print '22222'
show p
select * from "t"

exit

use trashdb
--drop table t
--drop table u
--go

--    CREATE TABLE t(a int IDENTITY NOT NULL, b int);
--    CREATE TABLE u(n int);
--    GO

----------------- this is a comment ------------------


-- An attempt to create a table with a reserved keyword as a name
-- should fail.

--update t set name += ' World'
--drop table t
--go

--    CREATE TABLE t (a INT NOT NULL IDENTITY PRIMARY KEY, b VARCHAR(20));
--    GO


use mydb

SHOW ALL TABLE trashdb.dbo.clients

quit ----------

select c.name, o.id, b.title, d.quantity, d.unit_price
from ((clients c join orders o on c.id = o.client_id)
       join order_details d on o.id = d.order_id)
       join books b on d.book_id = b.id


quit

insert into order_details values(100000, 5000, 1, 49.90), (100000, 5004, 1, 18.50), (100001, 5005, 1, 22.10), (100002, 5002, 2, 20.00), (100003, 5005, 7, 22.10)

exit

insert into orders values('20160102', 100), ('20160102', 101), ('20160103', 102), ('20160105', 100)

select * from orders


exit


create table clients (id int not null identity(100,1) primary key, name varchar(20), first_name varchar(20), birthdate date);

create table books (id int not null identity(5000,1) primary key, title varchar(30), price numeric(12,2))

create table orders (id int not null identity(100000,1) primary key, date date, client_id int)

create table order_details(order_id int not null, book_id int not null, quantity int, unit_price numeric(12,2))

go

insert into clients values('IVANOV', 'Igor', '19500504'), ('BON', 'Jean', '19700724'), ('MULLER', 'Hans', '19801206')

insert into books values ('Don Quixote', 49.90), ('Anna Karenina', 35), ('War and Peace', 20), ('The Metamorphosis', 24), ('Jane Eyre', 18.50), ('Frankenstein', 22.10)

go
select * from clients

select * from books

quit

print 'GAAAAAAAAAA'

go

print 'UUUUUUUUUUU'

exit



drop table parent;
drop table child;
go


    CREATE TABLE parent (id INT NOT NULL IDENTITY( 100, 1) PRIMARY KEY, first_name VARCHAR(20), name VARCHAR(20));
    CREATE TABLE child  (id INT NOT NULL IDENTITY(5000, 1) PRIMARY KEY, parent_id INT NOT NULL, first_name VARCHAR(20));
    GO

    DECLARE @id BIGINT;

    BEGIN TRAN;
    INSERT INTO parent (first_name, name) VALUES ('Paul', 'SMITH');
    SET @id = SCOPE_IDENTITY();
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'John');
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'Jack');
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'Jill');
    COMMIT;

    BEGIN TRAN;
    INSERT INTO parent (first_name, name) VALUES ('Vladimir', 'POPOV');
    SET @id = SCOPE_IDENTITY();
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'Anastasia');
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'Taysia');
    COMMIT;

    BEGIN TRAN;
    INSERT INTO parent (first_name, name) VALUES ('Jean', 'DUBOIS');
    COMMIT;

    BEGIN TRAN;
    INSERT INTO parent (first_name, name) VALUES ('Mario', 'RAMELLI');
    SET @id = SCOPE_IDENTITY();
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'Isabella');
    INSERT INTO child (parent_id, first_name) VALUES (NULL, 'Anna');  -- error, batch aborts
    COMMIT;

    BEGIN TRAN;
    INSERT INTO parent (first_name, name) VALUES ('Franz', 'MULLER');
    SET @id = SCOPE_IDENTITY();
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'Hans');
    INSERT INTO child (parent_id, first_name) VALUES (@id, 'Greta');
    COMMIT;

    GO

    SELECT * FROM parent;
    SELECT * FROM child;




