package main

import (
	"bufio"
	"io/ioutil"
	"os"
	"strings"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/htmlindex"
	"golang.org/x/text/transform" // Transformer is an interface, having methods Transform() and Reset()
)

// Read_file_in_utf8 returns the content of file, decoding it into utf-8 if necessary.
//
func Read_file_in_utf8(file_path string, encoding_name string) (script_text []byte, err error) {
	var (
		file *os.File
		enc  encoding.Encoding // just an interface, with methods NewDecoder() and NewEncoder() returning a Transformer
		// Real implementation of encoders/decoders are in golang.org/x/text/encoding/charmap package
		decoder         transform.Transformer
		buffered_reader *bufio.Reader
		utf8_reader     *transform.Reader
	)

	// if encoding is already utf-8, just return the file content

	encoding_name = strings.ToLower(encoding_name)

	switch encoding_name {
	case "utf8", "utf-8":
		encoding_name = "utf8"
	case "acp":
		encoding_name = "windows-1252"
	}

	if encoding_name == "utf8" {
		if script_text, err = ioutil.ReadFile(file_path); err != nil {
			return nil, err
		}

		return script_text, nil
	}

	//=== encoding is not utf-8. Open file ===

	if file, err = os.Open(file_path); err != nil {
		return nil, err
	}
	defer file.Close()

	// create buffio.Reader object to buffer the file

	buffered_reader = bufio.NewReader(file)

	// create a decoder from 'encoding_name' to 'utf8'

	if enc, err = htmlindex.Get(encoding_name); err != nil { // get encoding from a usual name, case insensitive. See http://www.w3.org/TR/encoding for the list of encoding names.
		return nil, err

	}

	decoder = enc.NewDecoder() // decodes to utf8

	Verbosef("file encoding is %s", encoding_name)

	// create transform.Reader object

	utf8_reader = transform.NewReader(buffered_reader, decoder) // transform.Reader object implements io.Reader interface

	// read all file content, in utf-8

	if script_text, err = ioutil.ReadAll(utf8_reader); err != nil {
		return nil, err
	}

	return script_text, nil
}

// Read_query_string_in_utf8 returns the query string, decoding it into utf-8 if necessary.
//
func Read_query_string_in_utf8(query_string string, encoding_name string) (script_text []byte, err error) {
	var (
		enc encoding.Encoding // just an interface, with methods NewDecoder() and NewEncoder() returning a Transformer
		// Real implementation of encoders/decoders are in golang.org/x/text/encoding/charmap package
		decoder *encoding.Decoder
	)

	// if encoding is already utf-8, just return the file content

	encoding_name = strings.ToLower(encoding_name)

	switch encoding_name {
	case "utf8", "utf-8":
		encoding_name = "utf8"
	case "acp":
		encoding_name = "windows-1252"
	}

	if encoding_name == "utf8" {
		return []byte(query_string), nil
	}

	//=== encoding is not utf-8, create a decoder from 'encoding_name' to 'utf8' ===

	if enc, err = htmlindex.Get(encoding_name); err != nil { // get encoding from a usual name, case insensitive. See http://www.w3.org/TR/encoding for the list of encoding names.
		return nil, err

	}

	decoder = enc.NewDecoder() // decodes to utf8

	Verbosef("query string encoding is %s", encoding_name)

	// decode

	if script_text, err = decoder.Bytes([]byte(query_string)); err != nil {
		return nil, err
	}

	return script_text, nil
}
