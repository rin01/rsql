/*==============================================
*                                              *
*         create table product                 *
*                                              *
==============================================*/


USE mytest; -- you must have created the database mytest


IF OBJECT_ID ('product', 'U') IS NOT NULL
    BEGIN
    PRINT 'The table product already exists. Delete it.'
    RETURN
    END


-- =========== create table product ===========


-- create table product

CREATE TABLE product (
    prodid    INT            NOT NULL PRIMARY KEY,
    pname     VARCHAR(100)   NOT NULL,
    unit      VARCHAR(20)    NULL,
    price     NUMERIC(12,2)  NULL
)




