// This file is compiled only on windows platform

package main

// default value for options of rcli.
// If you change these values, you should also change the string constant G_RCLI_CONF for the default rcli.conf content.
const (
	CONFIG_FILE_NAME = "rcli.conf"

	DEFAULT_REMOTE_SERVER_NAME = "127.0.0.1" // for local host on Windows, use 127.0.0.1 instead of "localhost" (because "localhost" may be slow on Windows)
	DEFAULT_REMOTE_PORT        = "7777"
	DEFAULT_REMOTE_SERVER      = DEFAULT_REMOTE_SERVER_NAME + ":" + DEFAULT_REMOTE_PORT
	DEFAULT_FILE_ENCODING      = "utf-8"
	DEFAULT_NULL_STRING        = "NULL"
	DEFAULT_WIDTH_LIMIT        = 30
	DEFAULT_FRACTIONAL_SECOND  = 3
	DEFAULT_PRINT_MODE         = 0
	DEFAULT_ERROR_LEVEL        = 0
	DEFAULT_ERROR_WRAP         = true
	DEFAULT_COLOR              = false
	DEFAULT_KEEPALIVE_INTERVAL = 20 // in seconds
)

const G_RCLI_CONF = `
# usually, the name of this file is rcli.conf, located in the user's home directory

# this is the configuration file of the command line rsql client, "rcli", in TOML format.
# for TOML, see https://github.com/toml-lang/toml

[remote_server]
    address            = "127.0.0.1:7777"     # ip and port. For local host on Windows, use 127.0.0.1 instead of "localhost" (because "localhost" may be slow on Windows).


[login]
    login_name         = "sa"                 # login name
    password           = "changeme"           # to ensure that nobody can see your password, you should set the proper permissions on this file.
    # default_database   = "trashdb"            # force the default database for connection. Else, the default database for the login is used.


[presentation]
    file_encoding      = "utf-8"              # encoding of input file
    null_string        = "NULL"               # string displayed by SELECT for NULL values. "␀" is the unicode symbol for Null, which may be a better choice than the string "Null". You can also use some unusual symbol, like "¬", if you like it better.
    width_limit        = 30                   # column width for displaying VARBINARY and VARCHAR
    fractional_second  = 3                    # number of fractional second digits, for displaying TIME and DATETIME
    print_mode         = 0                    # PRINT mode for multiple values. 0: normal mode, 1: column aligned
    error_level        = 0                    # level of details displayed when error is printed. 0, 1, 2
    error_wrap         = true                 # if true, error message is printed on a new line
    color              = false                # if true, error message is printed in color

[internal]
    keepalive_interval = 20                   # keepalive messages are sent to the server at this rate, in seconds. By default, 20 seconds.


`
