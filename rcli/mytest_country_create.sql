/*==============================================
*                                              *
*         create table country                 *
*                                              *
==============================================*/


USE mytest; -- you must have created the database mytest


IF OBJECT_ID ('country', 'U') IS NOT NULL
    BEGIN
    PRINT 'The table country already exists. Delete it.'
    RETURN
    END


-- =========== create table country ===========


-- create table country

CREATE TABLE country (
    cid        INT         NOT NULL PRIMARY KEY,
    name       VARCHAR(20) NOT NULL
)



