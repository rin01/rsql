package btree

import (
	"rsql"
	"rsql/cache"
)

// a list of Shrink_item will be returned to SHRINK_TABLE function, as it is in charge of physically shrinking the files.
//
type Shrink_item struct {
	Sh_tabledef             *rsql.Tabledef
	Sh_highest_used_page_no cache.Page_no_t
}

func Shrink_base_table_and_indexes(wcache *cache.Wcache, gtabledef *rsql.GTabledef) (shrink_list []Shrink_item) {

	assert(wcache.Permanent_tables_count() == 0)

	shrink_item := shrink_table(wcache, gtabledef.Base)
	shrink_list = append(shrink_list, shrink_item)

	for _, tabledef := range gtabledef.Indexmap {
		shrink_item = shrink_table(wcache, tabledef)
		shrink_list = append(shrink_list, shrink_item)
	}

	return shrink_list
}

func shrink_table(wcache *cache.Wcache, tabledef *rsql.Tabledef) Shrink_item {
	var (
		elem_page_info *cache.Page_cache_element
		rootbitmap     []uint8
		ppage_info     *cache.Page
	)

	// get page_info and reinitialize some fields

	elem_page_info = wcache.Require_DRAFT_page_info_and_pin_it(tabledef)
	ppage_info = wcache.Ppage_DRAFT_for_write(elem_page_info)

	highest_zone_no := cache.Zone_no_t(ppage_info.Pinf_zone_bitmaps_count() - 1)
	highest_used_zone_no, highest_used_page_no := get_highest_used_page_no(wcache, tabledef, ppage_info)

	// update page info rootbitmap

	rootbitmap = ppage_info.Pinf_zone_rootbitmap_array()

	for i := highest_used_zone_no + 1; i <= highest_zone_no; i++ {
		cache.Zone_bitmap_set_bit(rootbitmap, uint64(i), 1)
	}

	ppage_info.Set_pinf_zone_bitmaps_count(uint32(highest_used_zone_no + 1))

	wcache.Unpin(elem_page_info) // unpin page info

	// return Shrink_item

	return Shrink_item{Sh_tabledef: tabledef, Sh_highest_used_page_no: highest_used_page_no}
}

func get_highest_used_page_no(wcache *cache.Wcache, tabledef *rsql.Tabledef, ppage_info *cache.Page) (highest_used_zone_no cache.Zone_no_t, highest_used_page_no cache.Page_no_t) {
	var (
		elem_allocator  *cache.Page_cache_element
		ppage_allocator *cache.Page
		highest_zone_no cache.Zone_no_t
		zone_no         int
	)

	highest_zone_no = cache.Zone_no_t(ppage_info.Pinf_zone_bitmaps_count() - 1) // highest zone_no with an allocated allocator page

	for zone_no = int(highest_zone_no); zone_no >= 0; zone_no-- {
		elem_allocator = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, cache.Page_no_t(zone_no*cache.PZONE_NUMBER_OF_PAGES_IN_ZONE))
		ppage_allocator = wcache.Ppage(elem_allocator)
		assert(ppage_allocator.Pg_type() == cache.PAGE_TYPE_ZONE_ALLOCATOR)

		if ppage_allocator.Pzone_is_empty() == false { // allocator for zone 0 is never empty, as it contains info page
			break
		}

		wcache.Unpin(elem_allocator) // unpin zone allocator
	}

	// zone_no contains allocated pages

	assert(zone_no >= 0)

	allocator_page_no := elem_allocator.Pce_page_no()
	assert(allocator_page_no == ppage_allocator.Pg_no())

	delta_page := ppage_allocator.Pzone_highest_allocated_delta_page_no()
	//println("                   ", allocator_page_no, "+", delta_page, "=", allocator_page_no+delta_page)
	assert(delta_page > 0) // allocated page is not the allocator page itself

	highest_used_page_no = allocator_page_no + delta_page

	wcache.Unpin(elem_allocator) // unpin zone allocator

	return cache.Zone_no_t(zone_no), highest_used_page_no
}
