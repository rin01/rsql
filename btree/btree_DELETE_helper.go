package btree

import (
	"rsql"
	"rsql/cache"
)

// delete_one_tuple_from_leaf_page deletes one tuple from leaf page at tuple_index position.
//
// If no tuple at index position, panics.
// Always succeeds.
//
// If page becomes empty, returns true. The caller must discard the page from the btree.
//
func delete_one_tuple_from_leaf_page(page *cache.Page, tuple_index int) (discard_page bool) {
	var (
		tuple_count             int
		tuple_length            int
		tuple_offset            int
		sentinel_offset         int
		upper_tuples_chunk_size int
		i                       int
	)

	tuple_count = page.Pg_tuple_count()

	assert(page.Pg_type() == cache.PAGE_TYPE_LEAF)
	assert(tuple_index >= 0)
	assert(tuple_index < tuple_count)

	if tuple_count == 1 { // the page will be discarded
		return true
	}

	//=== shift upper tuples chunk, and shift and adjust tuple offsets ===

	sentinel_offset = page.Pg_tuple_offset(tuple_count)
	tuple_offset = page.Pg_tuple_offset(tuple_index)
	tuple_length = page.Pg_tuple_length(tuple_index)

	upper_tuples_chunk_size = sentinel_offset - (tuple_offset + tuple_length) // size of upper block that must be moved downwards

	assert(upper_tuples_chunk_size >= 0)

	if upper_tuples_chunk_size > 0 { // if tuples exist after position of the tuple to delete
		copy(page.Pg_canvas[tuple_offset:sentinel_offset-tuple_length], // shift downwards block of tuples after the tuple to delete
			page.Pg_canvas[tuple_offset+tuple_length:sentinel_offset])
	}

	for i = tuple_index + 1; i < tuple_count; i++ { // copy and adjust offsets of tuples after the tuple to delete
		page.Set_pg_tuple_offset(i, uint16(page.Pg_tuple_offset(i+1)-tuple_length))
	}

	//=== finalize page info ===

	page.Add_pg_tuple_count(-1) // decrement tuple count

	return false
}

// delete_one_tuple_from_node_page deletes one tuple from node page at tuple_index position.
// Argument tuple_index can be -1, which is the bottom target page entry.
//
// If no tuple at index position, panics.
// Always succeeds.
//
// If page becomes empty, returns true. The caller must discard the page from the btree.
//
func delete_one_tuple_from_node_page(page *cache.Page, tuple_index int) (discard_page bool) {
	var (
		tuple_count             int
		tuple_length            int
		tuple_offset            int
		sentinel_offset         int
		upper_tuples_chunk_size int
		i                       int
		target_page_no          cache.Page_no_t
	)

	tuple_count = page.Pg_tuple_count()

	assert(page.Pg_type() == cache.PAGE_TYPE_NODE)
	assert(tuple_index >= -1)
	assert(tuple_index < tuple_count)

	//=== if tuple_index == -1 (bottom target) ===

	if tuple_index == -1 {
		if tuple_count == 0 { // node page will be discarded
			return true
		}

		target_page_no = Target_page_no_in_node(page, 0) // bottom target page no is discarded, and replaced by target page of tuple 0
		page.Set_pg_bottom_target_page_no(target_page_no)

		tuple_index = 0 // delete tuple 0
	}

	//=== shift upper tuples chunk, and shift and adjust tuple offsets ===

	sentinel_offset = page.Pg_tuple_offset(tuple_count)
	tuple_offset = page.Pg_tuple_offset(tuple_index)
	tuple_length = page.Pg_tuple_length(tuple_index)

	upper_tuples_chunk_size = sentinel_offset - (tuple_offset + tuple_length) // size of upper block that must be moved downwards

	assert(upper_tuples_chunk_size >= 0)

	if upper_tuples_chunk_size > 0 { // if tuples exist after position of the tuple to delete
		copy(page.Pg_canvas[tuple_offset:sentinel_offset-tuple_length], // shift downwards block of tuples after the tuple to delete
			page.Pg_canvas[tuple_offset+tuple_length:sentinel_offset])
	}

	for i = tuple_index + 1; i < tuple_count; i++ { // copy and adjust offsets of tuples after the tuple to delete
		page.Set_pg_tuple_offset(i, uint16(page.Pg_tuple_offset(i+1)-tuple_length))
	}

	//=== finalize page info ===

	page.Add_pg_tuple_count(-1) // decrement tuple count

	return false
}

// fusion_prev_and_next_pages_of will link the previous and next page of the page passed as argument.
// It happens when a leaf page or node page becomes empty and must be discarded from the btree.
//
func fusion_prev_and_next_pages_of(wcache *cache.Wcache, tabledef *rsql.Tabledef, page *cache.Page) {
	var (
		page_prev_no   cache.Page_no_t
		elem_page_prev *cache.Page_cache_element
		page_prev      *cache.Page
		page_next_no   cache.Page_no_t
		elem_page_next *cache.Page_cache_element
		page_next      *cache.Page
	)

	page_prev_no = page.Pg_prev_no()
	page_next_no = page.Pg_next_no()

	if page_prev_no != cache.PAGE_BOT {
		elem_page_prev = wcache.Require_DRAFT_page_and_pin_it(tabledef, page_prev_no) // previous page
		page_prev = wcache.Ppage_DRAFT_for_write(elem_page_prev)

		page_prev.Set_pg_next_no(page_next_no)

		wcache.Unpin(elem_page_prev)
	}

	if page_next_no != cache.PAGE_EOT {
		elem_page_next = wcache.Require_DRAFT_page_and_pin_it(tabledef, page_next_no) // next page
		page_next = wcache.Ppage_DRAFT_for_write(elem_page_next)

		page_next.Set_pg_prev_no(page_prev_no)

		wcache.Unpin(elem_page_next)
	}
}
