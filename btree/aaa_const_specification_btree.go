// +build !trial

package btree

const (
	XDEBUG_CLEAN_PPAGE_UNUSED_PART bool = false // fill unused part of page.Pg_canvas with a filling value
	XDEBUG_INSPAD_FREE_TUPLE_CLEAR bool = false // function free_tuple() will fill tuple to free with 0xff bytes
)

const INSPAD_NEW_EMPTY_NODE_ENTRY_TUPLE_CAPACITY = 100

const NODE_TUPLE_MAX_SIZE = 1000 // max size of a node entry. In MS SQL Server, this value is 900 bytes.

const DELPAD_NODE_LIST_DEFAULT_CAPACITY = 6 // 6 is good enough. A table with this depth is quite large.
