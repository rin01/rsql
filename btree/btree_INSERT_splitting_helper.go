package btree

import (
	"rsql"
	"rsql/cache"
)

// during splitting, original page is copied into Page_buffer, and the tuple to insert is inserted in it.
// It is easier to find splitting offsets when all tuples are put in this "working" buffer.
// Finally, the split parts are copied back to pages.
//
type Page_buffer struct {
	buff        []byte // tuples are appended in this buffer
	offset_list []int  // tuple offsets + sentinel offset
}

const (
	PAGEBUFF_BUFF_FIXED_CAPACITY          = 2 * cache.PAGE_SIZE              // worst case for leaf pages: tuples from original page + very large tuple. Worst case for node pages: tuples from original page + two entries of size NODE_TUPLE_MAX_SIZE.
	PAGEBUFF_OFFSET_LIST_DEFAULT_CAPACITY = PAGEBUFF_BUFF_FIXED_CAPACITY / 8 // good enough
)

// reset resets pagebuff, which will become empty.
//
func (pagebuff *Page_buffer) reset() {

	if pagebuff.buff == nil {
		pagebuff.buff = make([]byte, PAGEBUFF_BUFF_FIXED_CAPACITY)
	}

	if pagebuff.offset_list == nil {
		pagebuff.offset_list = make([]int, PAGEBUFF_OFFSET_LIST_DEFAULT_CAPACITY)
	}

	pagebuff.buff = pagebuff.buff[:0] // buffer for tuples is now empty

	pagebuff.offset_list = append(pagebuff.offset_list[:0], 0) // sentinel offset existing even if no tuple in page
}

func (pagebuff *Page_buffer) tuple_count() int {

	return len(pagebuff.offset_list) - 1
}

func (pagebuff *Page_buffer) get_tuple(index int) rsql.Tuple {

	tuple_start := pagebuff.offset_list[index]
	tuple_endx := pagebuff.offset_list[index+1]

	return pagebuff.buff[tuple_start:tuple_endx]
}

// consumed_room_by_tuples returns the room consumed by tuples from index_start to index_endx (not including).
// This value is the space consumed by the tuples themselves, plus the space consumed by the offsets.
//
// index_start and index_endx must be >= 0 and <= tuple_count
//
// If index_endx == -1, it is replace by pagebuff.tuple_count().
//
func (pagebuff *Page_buffer) consumed_room_by_tuples(index_start int, index_endx int) int {
	var (
		consumed_room int
	)

	if index_endx == -1 {
		index_endx = pagebuff.tuple_count()
	}

	assert(index_endx >= index_start)

	consumed_room = pagebuff.offset_list[index_endx] - pagebuff.offset_list[index_start] + (index_endx-index_start)*cache.TUPLE_OFFSET_SIZE

	assert(consumed_room >= 0)

	return consumed_room
}

// append_all_tuples_from_page appends all tuples from page_source into pagebuff.
//
func (pagebuff *Page_buffer) append_all_tuples_from_page(page_source *cache.Page) int {
	var (
		pagebuff_tuple_count_0 int
		source_tuple_count     int
		offset                 int
		offset_start           int
	)

	assert(len(pagebuff.offset_list) > 0) // sentinel offset always exists, even if pagebuff contains no tuple

	// copy offsets

	source_tuple_count = page_source.Pg_tuple_count()

	pagebuff_tuple_count_0 = pagebuff.tuple_count()
	offset_start = pagebuff.offset_list[pagebuff_tuple_count_0]

	for i := 1; i <= source_tuple_count; i++ { // copy and shift all offsets, except offset 0, but including sentinel
		offset = page_source.Pg_tuple_offset(i)
		pagebuff.offset_list = append(pagebuff.offset_list, offset+offset_start)
	}

	// copy tuples

	pagebuff.buff = append(pagebuff.buff, page_source.Pg_canvas[:offset]...)

	assert(pagebuff_tuple_count_0+source_tuple_count == pagebuff.tuple_count())
	assert(pagebuff.offset_list[len(pagebuff.offset_list)-1] == len(pagebuff.buff))

	return source_tuple_count
}

// copy_tuples_to_page copies a range of tuples into page_dest, overwriting the old content.
//
// If source_endx == -1, it is replaced by pagebuff.tuple_count().
//
func (pagebuff *Page_buffer) copy_tuples_to_page(page_dest *cache.Page, source_start int, source_endx int) int {
	var (
		tuple_count  int
		offset_start int
		offset_endx  int
	)

	if source_endx == -1 {
		source_endx = pagebuff.tuple_count()
	}

	tuple_count = source_endx - source_start

	assert(tuple_count > 0 || page_dest.Pg_type() == cache.PAGE_TYPE_NODE)

	// copy offsets

	offset_start = pagebuff.offset_list[source_start]
	offset_endx = pagebuff.offset_list[source_endx]

	for i, j := source_start, 0; i <= source_endx; i, j = i+1, j+1 { // all offsets, including sentinel
		offset := pagebuff.offset_list[i]

		page_dest.Set_pg_tuple_offset(j, uint16(offset-offset_start))
	}

	// copy tuples

	copy(page_dest.Pg_canvas[0:offset_endx-offset_start], pagebuff.buff[offset_start:offset_endx])

	page_dest.Set_pg_tuple_count(uint16(tuple_count))

	assert(page_dest.Available_room() >= 0)

	return tuple_count
}

// insert_one_tuple inserts one tuple into pagebuff.
//
func (pagebuff *Page_buffer) insert_one_tuple(tuple_index int, tuple rsql.Tuple) {
	var (
		tuple_count             int
		tuple_length            int
		tuple_offset            int
		sentinel_offset         int
		upper_tuples_chunk_size int
		new_tuple_count         int
	)

	tuple_count = pagebuff.tuple_count()

	sentinel_offset = pagebuff.offset_list[tuple_count]
	tuple_offset = pagebuff.offset_list[tuple_index]

	assert(tuple_index >= 0 && tuple_index <= tuple_count)
	assert(sentinel_offset == len(pagebuff.buff))

	tuple_length = len(tuple)

	//=== insert space for tuple to insert ===

	pagebuff.buff = pagebuff.buff[:sentinel_offset+tuple_length] // increase length. pagebuff.buff underlying array is always big enough.
	pagebuff.offset_list = append(pagebuff.offset_list, 0)       // we append 0, just to increase capacity if necessary

	new_tuple_count = pagebuff.tuple_count()
	assert(new_tuple_count == tuple_count+1)

	//=== shift upper tuples chunk, and shift and adjust tuple offsets ===

	upper_tuples_chunk_size = sentinel_offset - tuple_offset // size of upper block that must be moved upwards
	assert(upper_tuples_chunk_size >= 0)

	if upper_tuples_chunk_size > 0 { // if tuples exist at and after position of the tuple to insert
		copy(pagebuff.buff[tuple_offset+tuple_length:tuple_offset+tuple_length+upper_tuples_chunk_size], // shift upwards block of tuples after the tuple to insert
			pagebuff.buff[tuple_offset:tuple_offset+upper_tuples_chunk_size])
	}

	for i := new_tuple_count; i > tuple_index; i-- { // copy and adjust offsets of tuples after the tuple to insert
		pagebuff.offset_list[i] = pagebuff.offset_list[i-1] + tuple_length
	}

	//=== write insertion tuple ===

	copy(pagebuff.buff[tuple_offset:tuple_offset+tuple_length], tuple) // copy tuple to insert

	assert(pagebuff.offset_list[pagebuff.tuple_count()] == len(pagebuff.buff))
}

// try_to_find_half_consumed_room_splitting_index tries to find splitting index, at which the page is split in two.
// It does so by finding the index at which the original page and split page will contain approximately the same amount of consumed room (which is better than just distribute the half count of tuples in each page).
// But if the inserted row is large, it may fail if consumed room in resulting pages are > PAGE_CANVAS_LENGTH. In this case, -1 is returned.
//
//        pagebuff must contain al least two tuples.
//
//        If -1 is returned, the splitting tentative has failed.
//            You must try another method to find another splitting index. In particular, you can try to split at the index of the record that triggered the page split.
//
func (pagebuff *Page_buffer) try_to_find_half_consumed_room_splitting_index() int {
	var (
		tuple_count int
		split_index int

		t_lower            int
		t_middle           int
		t_upper_x          int
		consumed_room_half int
		consumed_room_0    int
		consumed_room_1    int
	)

	tuple_count = pagebuff.tuple_count()

	assert(tuple_count >= 2) // page must contain at least 2 tuples

	if tuple_count == 2 {
		return 1
	}

	// find the splitting index at which consumed_room >= consumed_room_half

	consumed_room_half = pagebuff.consumed_room_by_tuples(0, tuple_count) / 2

	t_lower = 0
	t_upper_x = tuple_count

	t_middle = (t_lower + t_upper_x) / 2

	for t_middle != t_lower {
		consumed_room := pagebuff.consumed_room_by_tuples(0, t_middle) // consumed room if splitting at t_middle

		if consumed_room < consumed_room_half {
			t_lower = t_middle
		} else {
			t_upper_x = t_middle
		}

		t_middle = (t_lower + t_upper_x) / 2
	}

	assert(t_middle >= 0 && t_middle < tuple_count) // room consumed with splitting at t_middle is < consumed_room_half

	t_middle += 1 // splitting at t_middle, we consume >= consumed_room_half    1 ... tuple_count

	// split_index

	split_index = t_middle

	if split_index >= tuple_count { // happens if last tuple is large and previous tuples are small
		split_index = tuple_count - 1 // only last tuple goes to split page
	}

	if split_index < 1 {
		split_index = 1 // only first tuple remains on original page, the rest goes to split page
	}

	// check if split_index is valid

	consumed_room_0 = pagebuff.consumed_room_by_tuples(0, split_index)
	consumed_room_1 = pagebuff.consumed_room_by_tuples(split_index, -1)

	if consumed_room_0 > cache.PAGE_CANVAS_LENGTH || consumed_room_1 > cache.PAGE_CANVAS_LENGTH {
		split_index = -1
	}

	return split_index // if splitting failed, returns -1. If splitting succeeded, split_index is   1 ... tuple_count-1
}

// split_leaf splits leaf page in two or three pages.
// This function is called when the insertion of a tuple failed, because the page is full. The page must be split in two or three pages.
//
//       argument insertion_index is the index at which the tuple that triggered the splitting has been inserted in pagebuff.
//
//       returned split_index_0 is always a valid index value for splitting.
//       The caller must always move tuples[split_index_0...split_index_1] to split page.
//
//       returned split_index_1 is a valid index value for splitting, or -1 if a second split page is not necessary.
//       If split_index_1 != -1, the caller must move tuples[split_index_1...] to second split page.
//
func (pagebuff *Page_buffer) split_leaf(insertion_index int) (split_index_0 int, split_index_1 int) {
	var (
		tuple_count     int
		consumed_room_0 int
		consumed_room_1 int
		consumed_room_2 int
	)

	tuple_count = pagebuff.tuple_count()
	assert(tuple_count >= 2) // pagebuff must contain at least two tuples. The worst case is one large tuple from original_page + tuple to insert. In this case, split_index will just be 1.
	assert(insertion_index < tuple_count)

	// try to split in half, by splitting total consumed room in two parts

	split_index_0 = pagebuff.try_to_find_half_consumed_room_splitting_index() // -1 is returned if splitting failed. In this case, the inserted record is certainly quite large.
	if split_index_0 != -1 {                                                  // valid splitting index found. One split page needed.
		return split_index_0, -1
	}

	// splitting in half was not successful, because inserted tuple was certainly quite large. We now split at insertion_index.

	split_index_0 = insertion_index

	if split_index_0 == 0 {
		assert(pagebuff.consumed_room_by_tuples(1, -1) <= cache.PAGE_CANVAS_LENGTH)
		return 1, -1 // inserted record remains on original page, the rest on split page
	}

	consumed_room_0 = pagebuff.consumed_room_by_tuples(0, split_index_0)
	consumed_room_1 = pagebuff.consumed_room_by_tuples(split_index_0, -1)

	assert(consumed_room_0 <= cache.PAGE_CANVAS_LENGTH)

	if consumed_room_1 <= cache.PAGE_CANVAS_LENGTH { // one split page needed. Tuples before inserted tuple on original page, inserted tuple and the rest on split page
		return split_index_0, -1
	}

	// two split pages are needed

	assert(split_index_0 < tuple_count-1) // inserted tuple is not the last one

	split_index_1 = split_index_0 + 1 // tuples before inserted tuple remain on original page, inserted tuple alone goes on split page 1, tuples after inserted tuple go on split page 2

	consumed_room_0 = pagebuff.consumed_room_by_tuples(0, split_index_0)
	assert(consumed_room_0 <= cache.PAGE_CANVAS_LENGTH)

	consumed_room_1 = pagebuff.consumed_room_by_tuples(split_index_0, split_index_1)
	assert(consumed_room_1 <= cache.PAGE_CANVAS_LENGTH)

	consumed_room_2 = pagebuff.consumed_room_by_tuples(split_index_1, -1)
	assert(consumed_room_2 <= cache.PAGE_CANVAS_LENGTH)

	return split_index_0, split_index_1
}

// split_node splits node page in two pages.
// It is always possible to split a node in two, because node entry size is limited to approximatively 1 KB (MS SQL Server limits node entry length to 900 bytes).
//
func (pagebuff *Page_buffer) split_node() (split_index int) {
	var (
		tuple_count int
	)

	tuple_count = pagebuff.tuple_count()
	assert(tuple_count >= 2) // pagebuff must contain at least two tuples

	// split in half, by splitting total consumed room in two parts

	split_index = pagebuff.try_to_find_half_consumed_room_splitting_index() // splitting never fails for node page
	assert(split_index != -1)

	return split_index
}
