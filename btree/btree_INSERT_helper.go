package btree

import (
	"fmt"

	"rsql"
	"rsql/cache"
	"rsql/data"
)

func assert(a bool) {
	if a == false {
		panic("assertion failed")
	}
}

/* ============================================================== */
/*                      btree auxiliary functions                 */
/* ============================================================== */

func get_page_no(s []byte) cache.Page_no_t {
	return cache.Page_no_t(uint64(s[0]) | uint64(s[1])<<8 | uint64(s[2])<<16 | uint64(s[3])<<24 |
		uint64(s[4])<<32 | uint64(s[5])<<40 | uint64(s[6])<<48 | uint64(s[7])<<56)
}

// Comp_for_sort_XXX is a multiplexer function to which we can just pass dataslot of any type, and it will compare it for sort order.
//
//    'a' and 'b' are usually the same datatype. But when comparing a SARG column with an expression for SELECT processing, datatypes can be slightly different.
//
//          a       value to compare to reference value
//          b       reference value
//          coll    collator, only used when dataslots are VARCHAR. For other datatype, it is ignored and you can just pass nil.
//
//          result is:
//             -1      a <  b
//              0      a == b
//              1      a >  b
//
//          NOTE:
//               a NULL value is considered as a normal value for sort, which is sorted before 0 or empty string, or any other non-NULL value:
//                      NULL value < any non-NULL value
//                      NULL value == NULL value
//
//          NOTE:
//               if a or b is error, panics.
//
func Comp_for_sort_XXX(a rsql.IDataslot, b rsql.IDataslot, coll *data.SYSCOLLATOR) data.Compsort_t {
	var (
		res data.Compsort_t
	)

	if a.Datatype() != b.Datatype() {
		goto LABEL_DATATYPES_DIFFERENT
	}

	switch a.Datatype() {
	case rsql.DATATYPE_VARBINARY:
		res = data.Comp_for_sort_VARBINARY(a.(*data.VARBINARY), b.(*data.VARBINARY))

	case rsql.DATATYPE_VARCHAR:
		res = data.Comp_for_sort_VARCHAR(a.(*data.VARCHAR), b.(*data.VARCHAR), coll)

	case rsql.DATATYPE_BIT:
		res = data.Comp_for_sort_BIT(a.(*data.BIT), b.(*data.BIT))

	case rsql.DATATYPE_TINYINT:
		res = data.Comp_for_sort_TINYINT(a.(*data.TINYINT), b.(*data.TINYINT))

	case rsql.DATATYPE_SMALLINT:
		res = data.Comp_for_sort_SMALLINT(a.(*data.SMALLINT), b.(*data.SMALLINT))

	case rsql.DATATYPE_INT:
		res = data.Comp_for_sort_INT(a.(*data.INT), b.(*data.INT))

	case rsql.DATATYPE_BIGINT:
		res = data.Comp_for_sort_BIGINT(a.(*data.BIGINT), b.(*data.BIGINT))

	case rsql.DATATYPE_MONEY:
		res = data.Comp_for_sort_NUMERIC((*data.NUMERIC)(a.(*data.MONEY)), (*data.NUMERIC)(b.(*data.MONEY)))

	case rsql.DATATYPE_NUMERIC:
		res = data.Comp_for_sort_NUMERIC(a.(*data.NUMERIC), b.(*data.NUMERIC))

	case rsql.DATATYPE_FLOAT:
		res = data.Comp_for_sort_FLOAT(a.(*data.FLOAT), b.(*data.FLOAT))

	case rsql.DATATYPE_DATE:
		res = data.Comp_for_sort_DATETIME((*data.DATETIME)(a.(*data.DATE)), (*data.DATETIME)(b.(*data.DATE)))

	case rsql.DATATYPE_TIME:
		res = data.Comp_for_sort_DATETIME((*data.DATETIME)(a.(*data.TIME)), (*data.DATETIME)(b.(*data.TIME)))

	case rsql.DATATYPE_DATETIME:
		res = data.Comp_for_sort_DATETIME(a.(*data.DATETIME), b.(*data.DATETIME))

	default:
		panic("unexpected datatype")
	}

	return res

LABEL_DATATYPES_DIFFERENT:

	a_datatype := a.Datatype()
	b_datatype := b.Datatype()

	a_is_anyINT := false
	b_is_anyINT := false

	// xxxINT and yyyINT

	if a_datatype == rsql.DATATYPE_TINYINT || a_datatype == rsql.DATATYPE_SMALLINT || a_datatype == rsql.DATATYPE_INT || a_datatype == rsql.DATATYPE_BIGINT {
		a_is_anyINT = true
	}

	if b_datatype == rsql.DATATYPE_TINYINT || b_datatype == rsql.DATATYPE_SMALLINT || b_datatype == rsql.DATATYPE_INT || b_datatype == rsql.DATATYPE_BIGINT {
		b_is_anyINT = true
	}

	if a_is_anyINT == true && b_is_anyINT == true {
		res = data.Comp_for_sort_xxxINT_yyyINT(a.(rsql.AnyINT), b.(rsql.AnyINT))
		return res
	}

	// MONEY and NUMERIC         DATE and DATETIME

	switch {
	case a_datatype == rsql.DATATYPE_MONEY && b_datatype == rsql.DATATYPE_NUMERIC:
		res = data.Comp_for_sort_NUMERIC((*data.NUMERIC)(a.(*data.MONEY)), b.(*data.NUMERIC))
		return res

	case a_datatype == rsql.DATATYPE_NUMERIC && b_datatype == rsql.DATATYPE_MONEY:
		res = data.Comp_for_sort_NUMERIC(a.(*data.NUMERIC), (*data.NUMERIC)(b.(*data.MONEY)))
		return res

	case a_datatype == rsql.DATATYPE_DATE && b_datatype == rsql.DATATYPE_DATETIME:
		res = data.Comp_for_sort_DATETIME((*data.DATETIME)(a.(*data.DATE)), b.(*data.DATETIME))
		return res

	case a_datatype == rsql.DATATYPE_DATETIME && b_datatype == rsql.DATATYPE_DATE:
		res = data.Comp_for_sort_DATETIME(a.(*data.DATETIME), (*data.DATETIME)(b.(*data.DATE)))
		return res
	}

	panic(fmt.Sprintf("impossible, Comp_for_sort_xxx datatypes illegal: %s %s", a_datatype, b_datatype))
}

// insert_one_tuple_to_page tries to insert one tuple into page at tuple_index position.
//
// If page is empty, the tuple is always inserted, as there is always room enough for a tuple in an empty page.
//
// If page is not empty, and page is not large enough, the tuple is not inserted.
//
//     tuple_index              index at which tuple will be inserted. Value is [0 ... page.Pg_tuple_count()]
//     tuple                    tuple to insert
//
//     returns 1 if tuple has been written into page. Else, 0.
//
func insert_one_tuple_to_page(page *cache.Page, tuple_index int, tuple rsql.Tuple) int {
	var (
		tuple_count             int
		tuple_length            int
		tuple_offset            int
		sentinel_offset         int
		consumed_room_new       int
		upper_tuples_chunk_size int
		i                       int
	)

	tuple_count = page.Pg_tuple_count()

	sentinel_offset = page.Pg_tuple_offset(tuple_count)
	tuple_offset = page.Pg_tuple_offset(tuple_index)

	assert(tuple_index >= 0)
	assert(tuple_index <= tuple_count)

	//=== check if space is available ===

	tuple_length = len(tuple)

	consumed_room_new = page.Consumed_room() + tuple_length + cache.TUPLE_OFFSET_SIZE

	if consumed_room_new > cache.PAGE_CANVAS_LENGTH {
		if tuple_count > 0 {
			return 0 //===> if not enough space for more tuple, just return 0.
		}

		assert(consumed_room_new <= cache.PAGE_CANVAS_LENGTH) // if page is empty, we always want to put at least one tuple
	}

	//=== shift upper tuples chunk, and shift and adjust tuple offsets ===

	upper_tuples_chunk_size = sentinel_offset - tuple_offset // size of upper block that must be moved upwards

	assert(upper_tuples_chunk_size >= 0)

	if upper_tuples_chunk_size > 0 { // if tuples exist at and after position of the tuple to insert
		copy(page.Pg_canvas[tuple_offset+tuple_length:tuple_offset+tuple_length+upper_tuples_chunk_size], // shift upwards block of tuples after the tuple to insert
			page.Pg_canvas[tuple_offset:tuple_offset+upper_tuples_chunk_size])
	}

	for i = tuple_count + 1; i > tuple_index; i-- { // copy and adjust offsets of tuples after the tuple to insert
		page.Set_pg_tuple_offset(i, uint16(page.Pg_tuple_offset(i-1)+tuple_length))
	}

	//=== write insertion tuple data ===

	copy(page.Pg_canvas[tuple_offset:tuple_offset+tuple_length], tuple) // copy tuple to insert

	//=== finalize page info ===

	page.Add_pg_tuple_count(1) // increment tuple count

	assert(page.Available_room() >= 0)

	return 1
}

// write_leaf_page_node_entry appends serialized node entry tuple to dest, pointing to leaf_page.
//
// dest must be an empty buffer.
//
func (inspad *Inspad) write_leaf_page_node_entry(dest rsql.Tuple, tabledef *rsql.Tabledef, leaf_page *cache.Page) (res rsql.Tuple) {
	var (
		tuple        rsql.Tuple
		offset       int
		min_capacity int
		nk_count     int
		base_seqno   uint16
		idx          int
		page_no      cache.Page_no_t
		i            int
		field_part   []byte
	)

	assert(leaf_page.Pg_type() == cache.PAGE_TYPE_LEAF)

	tuple = leaf_page.Pg_tuple(0) // panics if page is empty

	nk_count = len(tabledef.Td_nk)

	// create header space for offsets

	rsql.Assert(len(dest) == 0)

	offset = (nk_count + 2) * rsql.FIELD_OFFSET_SIZE // space for key fields offsets, for target page_no field, and for sentinel field offset. Here, the value of 'offset' is the offset of field 0.

	min_capacity = 4 * offset // 4 times the offset is a good enough starting capacity for dest buffer

	if cap(dest) < min_capacity {
		dest = make([]byte, 0, min_capacity)
	}

	dest = dest[:offset] // header that stores all offsets

	// append all key fields

	for i = 0; i < nk_count; i++ {
		base_seqno = tabledef.Td_nk[i].Cd_base_seqno
		idx = inspad.base_seqno_to_coldefs_pos(tabledef.Td_type, base_seqno)

		field_part = tuple.Get_field_part(idx)

		dest[i*rsql.FIELD_OFFSET_SIZE] = uint8(offset)
		dest[i*rsql.FIELD_OFFSET_SIZE+1] = uint8(offset >> 8)

		dest = append(dest, field_part...)

		offset += len(field_part)
	}

	// append blank target page_no field and sentinel offset

	dest[i*rsql.FIELD_OFFSET_SIZE] = uint8(offset) // offset of target page_no field. Store it in tuple header.
	dest[i*rsql.FIELD_OFFSET_SIZE+1] = uint8(offset >> 8)

	dest = append(dest, 0, 0, 0, 0, 0, 0, 0, 0) // append blank uint64 field

	i++
	offset += 8 // Page_no_t is uint64 (8 bytes)

	dest[i*rsql.FIELD_OFFSET_SIZE] = uint8(offset) // offset of sentinel. Store it in tuple header.
	dest[i*rsql.FIELD_OFFSET_SIZE+1] = uint8(offset >> 8)

	rsql.Assert(offset == len(dest) && i == nk_count+1)

	// write target page_no field

	page_no = leaf_page.Pg_no()

	dest.Set_last_field_uint64(uint64(page_no))

	return dest
}
