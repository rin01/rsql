package btree

import (
	"rsql"
	"rsql/cache"
)

func Truncate_base_table_and_indexes(wcache *cache.Wcache, gtabledef *rsql.GTabledef) (deleted_row_count int64) {

	deleted_row_count = truncate_table(wcache, gtabledef.Base)

	for _, tabledef := range gtabledef.Indexmap {
		truncate_table(wcache, tabledef)
	}

	return deleted_row_count
}

func truncate_table(wcache *cache.Wcache, tabledef *rsql.Tabledef) (deleted_row_count int64) {
	var (
		elem_page_info *cache.Page_cache_element
		rootbitmap     []uint8
		ppage_info     *cache.Page

		elem_zone_allocator            *cache.Page_cache_element
		za_ppage                       *cache.Page
		za_zone_bitmap_array           []uint8
		za_zone_bitmap_free_page_count uint64
	)

	// delete pages

	// allocator bitmaps are not modified, but just ignored, as they are not referenced any more in page info's rootbitmap.
	wcache.Discard_all_TRANSITORY_elements_and_pages_from_wcache(cache.Tblid_t(tabledef.Td_tblid), 2) // all elements in write cache are deleted, excepted allocator page 0 and info page.

	// get page_info and reinitialize some fields

	elem_page_info = wcache.Require_DRAFT_page_info_and_pin_it(tabledef)
	ppage_info = wcache.Ppage_DRAFT_for_write(elem_page_info)

	deleted_row_count = int64(ppage_info.Pinf_total_leaf_tuples_count())

	ppage_info.Set_pinf_root_page_no(0)
	ppage_info.Set_pinf_total_leaf_tuples_count(0)
	ppage_info.Set_pinf_total_leaf_pages_count(0)
	ppage_info.Set_pinf_zone_bitmaps_count(1)

	rootbitmap = ppage_info.Pinf_zone_rootbitmap_array() // clear rootbitmap
	for i, _ := range rootbitmap {
		rootbitmap[i] = 0xFF
	}
	rootbitmap[0] = 0xFE // zone allocator 0 is allocated

	if tabledef.Td_type == rsql.TD_TYPE_BASE_TABLE {
		ppage_info.Set_pinf_next_rowid(0)
		ppage_info.Set_pinf_next_identity(cache.NEXT_IDENTITY_IS_SEED)
	}

	// get zone allocator 0 and clear it

	elem_zone_allocator = wcache.Require_DRAFT_page_and_pin_it(tabledef, 0)
	za_ppage = wcache.Ppage_DRAFT_for_write(elem_zone_allocator)

	za_zone_bitmap_array = za_ppage.Pzone_bitmap() // clear allocation bitmap
	for i, _ := range za_zone_bitmap_array {
		za_zone_bitmap_array[i] = 0
	}

	assert(cache.PAGE_INFO_PAGE_NO < 8)
	za_zone_bitmap_array[0] = 0x01 | (1 << cache.PAGE_INFO_PAGE_NO)          // page 0 is used by this allocator page, and page 1 is for info_page
	za_zone_bitmap_free_page_count = cache.PZONE_NUMBER_OF_PAGES_IN_ZONE - 2 // 2 pages not free
	za_ppage.Set_pzone_free_page_count(uint32(za_zone_bitmap_free_page_count))

	wcache.Unpin(elem_zone_allocator) // unpin zone allocator
	wcache.Unpin(elem_page_info)      // unpin page info

	return deleted_row_count
}
