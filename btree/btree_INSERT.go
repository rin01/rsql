package btree

// DON'T IMPORT "rsql/dict" because insertion functions are used by functions that:
//          - that don't lock the dictionary, like for INSERT INTO
//          - that lock the dictionary, like for CREATE INDEX in dict package.
// So, it means that if you write code that use the dictionary, it must lock the dictionary in some cases, but not in some other cases.
// This is impossible to do, as we have no safe function to check whether the dictionary is already locked or not.
//

import (
	"rsql"
	"rsql/cache"
	"rsql/data"
)

// Inspad is a struct that keeps working variables used by INSERT.
//
type Inspad struct {
	Scratchpad

	table_qname rsql.Object_qname_t

	node_entry_SCRATCH rsql.Row // native key columns, and target page_no as BIGINT. Life duration may span several functions, e.g. the function creates a new node entry, which is written to page later by another function.

	tuple_to_insert rsql.Tuple // just a working buffer used to serialize record, before being inserted into a page

	pagebuff *Page_buffer // buffer used for page splitting

	// the function split_if_necessary_and_insert_child_tuples_in_node_page() does the following:
	//      - inserts child_node_entry_1, and consecutive child_node_entry_2 if any, into node page
	//      - if this causes node page to split, insert new node entry into child_node_entry_1, so that it can be processed by next btree level.
	child_node_entry_1 rsql.Tuple
	child_node_entry_2 rsql.Tuple
	page_depth         uint16 // page level at which child node entries must be inserted. First node level just below LEAF pages is depth 1.

	sql_error *rsql.Error // filled e.g. if duplicate key is found. After insertion, you should check this field.

	new_leaf_pages_count    int // number of new leaf pages for insertion of one record
	total_new_records_count int // total number of records inserted by INSERT statement
}

func New_inspad(tabledef *rsql.Tabledef, table_qname rsql.Object_qname_t) (*Inspad, *rsql.Error) {
	var (
		rsql_err *rsql.Error
	)

	inspad := &Inspad{}

	inspad.tblid = cache.Tblid_t(tabledef.Td_tblid)

	inspad.table_qname = table_qname

	// create node_entry_FLASH and node_entry_SCRATCH node entry rows

	inspad.node_entry_FLASH = data.New_node_row(rsql.KIND_COL_LEAF, tabledef)   // last field is BIGINT target page_no field
	inspad.node_entry_SCRATCH = data.New_node_row(rsql.KIND_COL_LEAF, tabledef) // last field is BIGINT target page_no field

	inspad.node_syscollators = make([]*data.SYSCOLLATOR, len(tabledef.Td_nk))

	for i, coldef := range tabledef.Td_nk {
		if coldef.Cd_datatype == rsql.DATATYPE_VARCHAR {
			if inspad.node_syscollators[i], rsql_err = data.New_literal_SYSCOLLATOR_NO_LAZY(coldef.Cd_collation); rsql_err != nil { // collators are instanciated. If invalid collation, an error is returned.
				return nil, rsql_err
			}
		}
	}

	// create base_seqno_to_coldefs_pos_map, only for TD_TYPE_INDEX_TABLE

	if tabledef.Td_type == rsql.TD_TYPE_INDEX_TABLE {
		inspad.base_seqno_to_coldefs_pos_map = make(map[uint16]int, len(tabledef.Td_coldefs))

		for i, coldef := range tabledef.Td_coldefs {
			inspad.base_seqno_to_coldefs_pos_map[coldef.Cd_base_seqno] = i
		}
	}

	// create pagebuff

	inspad.pagebuff = &Page_buffer{}

	// create child_node_entry_1 and child_node_entry_2

	inspad.child_node_entry_1 = make([]byte, 0, INSPAD_NEW_EMPTY_NODE_ENTRY_TUPLE_CAPACITY)
	inspad.child_node_entry_2 = make([]byte, 0, INSPAD_NEW_EMPTY_NODE_ENTRY_TUPLE_CAPACITY)

	return inspad, nil
}

// insert tuple at the proper position in leaf page, creating new split pages if necessary.
//
//            elem_original_page                 original page, in which we try to insert the tuple. Must be DRAFT and pinned.
//            tabledef                           Tabledef
//            insertion_tuple                    tuple to insert
//            insertion_tuple_index              position in original page at which we want to insert the tuple
//
func (inspad *Inspad) split_if_necessary_and_insert_tuple_in_leaf_page(wcache *cache.Wcache, elem_original_page *cache.Page_cache_element, tabledef *rsql.Tabledef, insertion_tuple rsql.Tuple, insertion_tuple_index int) {
	var (
		original_page        *cache.Page
		original_page_no     cache.Page_no_t
		original_tuple_count int

		pagebuff *Page_buffer
		split_0  int
		split_1  int

		tuples_written int

		elem_split_page *cache.Page_cache_element
		split_page      *cache.Page

		elem_overflow_page *cache.Page_cache_element
		overflow_page      *cache.Page

		last_page *cache.Page

		elem_continuing_page *cache.Page_cache_element
		continuing_page      *cache.Page
		continuing_page_no   cache.Page_no_t
	)

	//====================== try to insert tuple in original page with no split ======================

	original_page = wcache.Ppage_DRAFT_for_write(elem_original_page) // ensure page is DRAFT, pinned, and dirty flag is set

	original_page_no = original_page.Pg_no()

	tuples_written = insert_one_tuple_to_page(original_page, insertion_tuple_index, insertion_tuple) // try to insert tuple

	if tuples_written == 1 {
		return // if success, ===>return
	}

	original_tuple_count = original_page.Pg_tuple_count()
	continuing_page_no = original_page.Pg_next_no()

	// if row to be inserted will be the last row in the table, create a new page for this row

	if continuing_page_no == cache.PAGE_EOT && insertion_tuple_index == original_tuple_count {
		// new blank page

		elem_new_page := wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_LEAF, original_page_no) // last arg is hint page_no
		new_page := wcache.Ppage_DRAFT_for_write(elem_new_page)
		inspad.new_leaf_pages_count++

		new_page.Set_pg_prev_no(original_page_no) // link new page to original page
		original_page.Set_pg_next_no(new_page.Pg_no())

		tuples_written = insert_one_tuple_to_page(new_page, 0, insertion_tuple) // insert tuple
		assert(tuples_written == 1)                                             // inserting a tuple in an empty page always succeeds

		// save new node entries in inspad's list for caller's use

		inspad.child_node_entry_1 = inspad.write_leaf_page_node_entry(inspad.child_node_entry_1[:0], tabledef, new_page)

		wcache.Unpin(elem_new_page) // unpin new page

		return
	}

	//====================== if not space enough in original page, distribute data in one or two split pages ======================

	pagebuff = inspad.pagebuff
	pagebuff.reset()

	pagebuff.append_all_tuples_from_page(original_page) // copy all tuples from original_page to pagebuff
	assert(pagebuff.tuple_count() == original_tuple_count)

	pagebuff.insert_one_tuple(insertion_tuple_index, insertion_tuple) // insert tuple

	split_0, split_1 = pagebuff.split_leaf(insertion_tuple_index) // find splitting indexes
	assert(split_0 > 0)

	pagebuff.copy_tuples_to_page(original_page, 0, split_0) // copy back tuples to original_page
	if XDEBUG_CLEAN_PPAGE_UNUSED_PART == true {
		original_page.Debug_clear_unused_canvas_part()
	}

	// split page

	elem_split_page = wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_LEAF, original_page_no) // last arg is hint page_no
	split_page = wcache.Ppage_DRAFT_for_write(elem_split_page)
	inspad.new_leaf_pages_count++

	split_page.Set_pg_prev_no(original_page_no) // link split page to original page
	original_page.Set_pg_next_no(split_page.Pg_no())

	pagebuff.copy_tuples_to_page(split_page, split_0, split_1) // copy back tuples to split page. split_1 may be -1
	if XDEBUG_CLEAN_PPAGE_UNUSED_PART == true {
		split_page.Debug_clear_unused_canvas_part()
	}

	last_page = split_page

	// second split page

	if split_1 != -1 { // another split page is needed
		elem_overflow_page = wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_LEAF, split_page.Pg_no()) // last arg is hint page_no
		overflow_page = wcache.Ppage_DRAFT_for_write(elem_overflow_page)
		inspad.new_leaf_pages_count++

		overflow_page.Set_pg_prev_no(split_page.Pg_no()) // link overflow_page to split_page
		split_page.Set_pg_next_no(overflow_page.Pg_no())

		pagebuff.copy_tuples_to_page(overflow_page, split_1, -1) // copy back tuples to overflow page
		if XDEBUG_CLEAN_PPAGE_UNUSED_PART == true {
			overflow_page.Debug_clear_unused_canvas_part()
		}

		last_page = overflow_page
	}

	// link last_page to the continuing page

	last_page.Set_pg_next_no(continuing_page_no)

	if continuing_page_no != cache.PAGE_EOT {
		elem_continuing_page = wcache.Require_DRAFT_page_and_pin_it(tabledef, continuing_page_no)
		continuing_page = wcache.Ppage_DRAFT_for_write(elem_continuing_page)

		continuing_page.Set_pg_prev_no(last_page.Pg_no())

		wcache.Unpin(elem_continuing_page)
	}

	// save new node entries in inspad's list for caller's use

	inspad.child_node_entry_1 = inspad.write_leaf_page_node_entry(inspad.child_node_entry_1[:0], tabledef, split_page)

	wcache.Unpin(elem_split_page) // unpin split page

	if elem_overflow_page != nil {
		inspad.child_node_entry_2 = inspad.write_leaf_page_node_entry(inspad.child_node_entry_2[:0], tabledef, overflow_page)

		wcache.Unpin(elem_overflow_page) // unpin overflow page
	}
}

// inserts a row into a leaf page, creating split pages if necessary.
//
//            elem_original_page                 original page, in which we try to insert the tuple. Must be DRAFT and pinned.
//
// If a duplicate key is found (for UNIQUE index or PRIMARY KEY), an error is signaled in inspad.sql_error.
//
func (inspad *Inspad) split_if_necessary_and_insert_row_in_leaf_page(wcache *cache.Wcache, elem_original_page *cache.Page_cache_element, tabledef *rsql.Tabledef, row_to_insert rsql.Row) {
	var (
		tuple_index       int
		original_page     *cache.Page
		nk_equality_found bool
	)

	// get writable ppage

	original_page = wcache.Ppage_DRAFT_for_write(elem_original_page)

	inspad.page_depth = 0 // LEAF pages are depth 0

	// find the position at which the cursor must be inserted

	if tuple_index, nk_equality_found = inspad.lookup_row_in_leaf_page(original_page, tabledef, row_to_insert); nk_equality_found == true { // index of the highest tuple, which is less or equal than row_to_insert
		inspad.child_node_entry_1 = inspad.child_node_entry_1[:0] // no insertion, no split
		inspad.child_node_entry_2 = inspad.child_node_entry_2[:0]

		inspad.sql_error = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_DUPLICATE_KEY_FOUND, rsql.ERROR_BATCH_ABORT, tabledef.Td_index_name_original, inspad.table_qname.String())

		return
	}

	// split page if necessary and insert tuple at tuple_index position

	tuple_index += 1 // new tuple must be inserted just after the highest tuple, which is less or equal than row_to_insert

	inspad.split_if_necessary_and_insert_tuple_in_leaf_page(wcache, elem_original_page, tabledef, inspad.tuple_to_insert, tuple_index) // row_to_insert has already been serialized in inspad.tuple_to_insert, long before calling this function
}

// insert node entry tuple(s) at the proper position in node page, creating a new split page if necessary.
//
//            WHEN CALLING THIS FUNCTION, inspad.child_node_entry_1 MUST EXISTS.
//
//            elem_original_page                 original page, in which we try to insert the tuple. Must be DRAFT and pinned.
//            tabledef                           Tabledef
//            insertion_tuple_index              position in original page at which we want to insert child tuple(s)
//
func (inspad *Inspad) split_if_necessary_and_insert_child_tuples_in_node_page(wcache *cache.Wcache, elem_original_page *cache.Page_cache_element, tabledef *rsql.Tabledef, insertion_tuple_index int) {
	var (
		original_page        *cache.Page
		original_page_no     cache.Page_no_t
		original_tuple_count int

		consumed_room_by_child_tuples int

		pagebuff *Page_buffer
		split_0  int

		tuples_written int

		elem_split_page *cache.Page_cache_element
		split_page      *cache.Page

		elem_continuing_page *cache.Page_cache_element
		continuing_page      *cache.Page
		continuing_page_no   cache.Page_no_t

		new_node_entry        rsql.Tuple
		bottom_target_page_no cache.Page_no_t
	)

	assert(len(inspad.child_node_entry_1) > 0) // child tuple must exist

	//====================== try to insert tuple in original page with no split ======================

	original_page = wcache.Ppage_DRAFT_for_write(elem_original_page) // ensure page is DRAFT, pinned, and dirty flag is set
	assert(original_page.Pg_page_depth() == int(inspad.page_depth))

	original_page_no = original_page.Pg_no()

	consumed_room_by_child_tuples = len(inspad.child_node_entry_1) + cache.TUPLE_OFFSET_SIZE

	if len(inspad.child_node_entry_2) > 0 {
		consumed_room_by_child_tuples += len(inspad.child_node_entry_2) + cache.TUPLE_OFFSET_SIZE
	}

	if original_page.Consumed_room()+consumed_room_by_child_tuples <= cache.PAGE_CANVAS_LENGTH { // if enough room
		tuples_written = insert_one_tuple_to_page(original_page, insertion_tuple_index, inspad.child_node_entry_1) // insert tuple
		assert(tuples_written == 1)
		inspad.child_node_entry_1 = inspad.child_node_entry_1[:0]

		if len(inspad.child_node_entry_2) > 0 {
			tuples_written = insert_one_tuple_to_page(original_page, insertion_tuple_index+1, inspad.child_node_entry_2) // insert tuple
			assert(tuples_written == 1)
			inspad.child_node_entry_2 = inspad.child_node_entry_2[:0]
		}

		return //===>return
	}

	//====================== if not space enough in original page, distribute data in one split page ======================

	original_tuple_count = original_page.Pg_tuple_count()
	continuing_page_no = original_page.Pg_next_no()

	pagebuff = inspad.pagebuff
	pagebuff.reset()

	pagebuff.append_all_tuples_from_page(original_page) // copy all tuples from original_page to pagebuff
	assert(pagebuff.tuple_count() == original_tuple_count)

	pagebuff.insert_one_tuple(insertion_tuple_index, inspad.child_node_entry_1) // insert node entry 1

	if len(inspad.child_node_entry_2) > 0 {
		pagebuff.insert_one_tuple(insertion_tuple_index+1, inspad.child_node_entry_2) // insert node entry 2 if any
	}

	split_0 = pagebuff.split_node() // find splitting index. For node tuple, only one split page is needed, because node entry size is small. Always succeeds.
	assert(split_0 > 0)

	pagebuff.copy_tuples_to_page(original_page, 0, split_0) // copy back tuples to original_page
	if XDEBUG_CLEAN_PPAGE_UNUSED_PART == true {
		original_page.Debug_clear_unused_canvas_part()
	}

	// split_page

	elem_split_page = wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_NODE, original_page_no) // last arg is hint page_no
	split_page = wcache.Ppage_DRAFT_for_write(elem_split_page)

	split_page.Set_pg_page_depth(inspad.page_depth) // set page depth

	split_page.Set_pg_prev_no(original_page_no) // link split page to original page
	original_page.Set_pg_next_no(split_page.Pg_no())

	pagebuff.copy_tuples_to_page(split_page, split_0+1, -1) // copy back remaining tuples to split page, except first tuple. There is always at least one tuple to copy, because node entries are small and we splitted by half of consumed room
	if XDEBUG_CLEAN_PPAGE_UNUSED_PART == true {
		split_page.Debug_clear_unused_canvas_part()
	}

	// link split_page to the continuing page

	split_page.Set_pg_next_no(continuing_page_no)

	if continuing_page_no != cache.PAGE_EOT {
		elem_continuing_page = wcache.Require_DRAFT_page_and_pin_it(tabledef, continuing_page_no)
		continuing_page = wcache.Ppage_DRAFT_for_write(elem_continuing_page)

		continuing_page.Set_pg_prev_no(split_page.Pg_no())

		wcache.Unpin(elem_continuing_page)
	}

	// save split page node entry in inspad.child_node_entry_1 for caller's use

	new_node_entry = pagebuff.get_tuple(split_0)

	bottom_target_page_no = cache.Page_no_t(new_node_entry.Get_last_field_uint64())
	split_page.Set_pg_bottom_target_page_no(bottom_target_page_no)

	inspad.child_node_entry_1 = append(inspad.child_node_entry_1[:0], new_node_entry...)
	inspad.child_node_entry_1.Set_last_field_uint64(uint64(split_page.Pg_no()))

	inspad.child_node_entry_2 = inspad.child_node_entry_2[:0] // only leaf page splitting can create a second node entry

	// unpin split page

	wcache.Unpin(elem_split_page)
}

// check_node_entry_max_length returns an error if inspad.child_node_entry_1 and _2 are too long.
// In this case, these node entries are discarded.
// If no error, the function just returns nil.
//
func (inspad *Inspad) check_node_entry_max_length(tabledef *rsql.Tabledef) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	switch {
	case len(inspad.child_node_entry_1) > NODE_TUPLE_MAX_SIZE:
		table_name := inspad.table_qname.String()
		rsql_err = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_NODE_TUPLE_TOO_LARGE, rsql.ERROR_BATCH_ABORT, tabledef.Td_index_name, table_name, len(inspad.child_node_entry_1), NODE_TUPLE_MAX_SIZE)

	case len(inspad.child_node_entry_2) > NODE_TUPLE_MAX_SIZE:
		table_name := inspad.table_qname.String()
		rsql_err = rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_NODE_TUPLE_TOO_LARGE, rsql.ERROR_BATCH_ABORT, tabledef.Td_index_name, table_name, len(inspad.child_node_entry_2), NODE_TUPLE_MAX_SIZE)
	}

	if rsql_err != nil {
		inspad.child_node_entry_1 = inspad.child_node_entry_1[:0] // child node entries are no more needed
		inspad.child_node_entry_2 = inspad.child_node_entry_2[:0]
	}

	return rsql_err
}

// RECURSIVE FUNCTION FOR ROW INSERTION STARTING FROM A NODE PAGE (argument 'elem_page_node'), BY INSERTING THE ROW IN TARGET LEAF CHILD PAGE, AND NODE ENTRIES IN TARGET NODE CHILD PAGE IF NEEDED, RECURSIVELY.
//
//    Algorithm : current node page is 'elem_page_node'.
//      - if child_target_page is LEAF :
//          - we insert the row into this LEAF child_target_page, generating LEAF split pages if needed.
//          - we receive these LEAF split pages in inspad.child_node_entry_1 and _2.
//          - we insert all these LEAF split page node entries into current 'elem_page_node', creating a new split page if needed, put in inspad.child_node_entry_1.
//      - else, if child_target_page is NODE :
//          - we recursively call this function, which may return a node entry for a split page, if splitting occured.
//          - we receive this node entry in inspad.child_node_entry_1.
//          - we insert this split page node entry into current 'elem_page_node', creating a new split page if needed, put in inspad.child_node_entry_1.
//
//
//    Splitting :
//      - Usually, 'elem_page_node' is left unchanged.
//      - But if the insertion created child node entries, they must be inserted in current 'elem_page_node'.
//      - This can cause 'elem_page_node' be splitted too, creating one new split page node entry, which is put in inspad.child_node_entry_1.
//
//
//    Return     'elem_page_node' or the draftified version of 'elem_page_node'
//
func (inspad *Inspad) walk_and_split_if_necessary_and_insert_row_and_node_entries(wcache *cache.Wcache, elem_page_node *cache.Page_cache_element, tabledef *rsql.Tabledef, row_to_insert rsql.Row) (elem_page_node_modified *cache.Page_cache_element) {
	var (
		node_entry_index       int
		child_target_page_no   cache.Page_no_t
		elem_child_target_page *cache.Page_cache_element
		child_target_page_type cache.Pg_type_t
	)

	//===== lookup tuple (record native key) ---> page_no. The b-tree is walked from root node page until leaf page is reached =====

	node_entry_index, child_target_page_no = inspad.find_target_page_no_in_node_page_for_row_lookup(wcache.Ppage(elem_page_node), tabledef, row_to_insert)

	elem_child_target_page = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, child_target_page_no) // ####   elem_child_target_page is pinned   ####

	child_target_page_type = wcache.Ppage(elem_child_target_page).Pg_type()

	// both functions in the switch below return split pages in inspad.child_node_entry_1 and _2, if splitting has occured

	switch child_target_page_type {
	case cache.PAGE_TYPE_NODE: // recursive on node child target page
		elem_child_target_page = inspad.walk_and_split_if_necessary_and_insert_row_and_node_entries(wcache, elem_child_target_page, tabledef, row_to_insert) // elem_child_target_page MAY HAVE BEEN DRAFTIFIED OR NOT

	case cache.PAGE_TYPE_LEAF: // no recursion on leaf child target page. Row is inserted into elem_child_target_page and split pages are created if needed.
		elem_child_target_page = wcache.DRAFTIFY_pinned_page(tabledef, elem_child_target_page)

		inspad.split_if_necessary_and_insert_row_in_leaf_page(wcache, elem_child_target_page, tabledef, row_to_insert)

	default:
		panic("unexpected pg_type")
	}

	wcache.Unpin(elem_child_target_page) // ####   unpin elem_child_target_page   ####

	// current page depth

	inspad.page_depth += 1 // LEAF pages are depth 0

	// here, if child page has been split, corresponding node entries are available in inspad.child_node_entry_1 and _2

	if len(inspad.child_node_entry_1) == 0 { // if no splitting, just return
		return elem_page_node // original version
	}

	//===== here, one or two pages have been created because child page has been split =====

	// check if node entries are not too long

	if rsql_err := inspad.check_node_entry_max_length(tabledef); rsql_err != nil { // if error, the child node entries are discarded
		inspad.sql_error = rsql_err
		return elem_page_node // original version. The insertion has failed.
	}

	// insert child node entries at current node level, in the current node page or following split page.

	elem_page_node = wcache.DRAFTIFY_pinned_page(tabledef, elem_page_node) // draftify elem_page_node

	inspad.split_if_necessary_and_insert_child_tuples_in_node_page(wcache, elem_page_node, tabledef, node_entry_index+1) // if elem_page_node is split, node entry has been put in inspad.child_node_entry_1

	return elem_page_node // draftified version
}

// MAIN FUNCTION FOR ROW INSERTION INTO A TABLE FILE.
//
// The layout of row_to_insert is as described by tabledef.
//
// All modified pages are DRAFT pages.
//
func (inspad *Inspad) Insert_row_into_table(wcache *cache.Wcache, tabledef *rsql.Tabledef, row_to_insert rsql.Row) *rsql.Error {
	var (
		rsql_err       *rsql.Error
		elem_page_info *cache.Page_cache_element
		page_info      *cache.Page

		elem_root_page *cache.Page_cache_element
		root_page_no   cache.Page_no_t
		root_page_type cache.Pg_type_t

		elem_new_root_page_DRAFT *cache.Page_cache_element
		new_root_page_DRAFT      *cache.Page
	)

	defer func() {
		if elem_new_root_page_DRAFT != nil {
			wcache.Unpin(elem_new_root_page_DRAFT)
		}

		if elem_root_page != nil {
			wcache.Unpin(elem_root_page)
		}

		if elem_page_info != nil {
			wcache.Unpin(elem_page_info)
		}
	}()

	inspad.new_leaf_pages_count = 0 // reset new leaf page counter

	// get page_info and root_page

	elem_page_info = wcache.Require_DRAFT_page_info_and_pin_it(tabledef) // after row has been inserted, page_info is always updated
	page_info = wcache.Ppage_DRAFT_for_write(elem_page_info)

	root_page_no = page_info.Pinf_root_page_no()

	switch root_page_no {
	case 0: // if table is empty, create the first LEAF page
		elem_root_page = wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_LEAF, 0) // hint_page_no == 0

		page_info.Set_pinf_root_page_no(elem_root_page.Pce_page_no())
		inspad.new_leaf_pages_count++

	default:
		elem_root_page = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, root_page_no)
	}

	// serialize row to tuple

	if inspad.tuple_to_insert, rsql_err = data.Write_serialized_row(inspad.tuple_to_insert[:0], row_to_insert); rsql_err != nil {
		return rsql_err
	}

	// walk the btree, and insert record in leaf page and node pages

	inspad.child_node_entry_1 = inspad.child_node_entry_1[:0]
	inspad.child_node_entry_2 = inspad.child_node_entry_2[:0]
	inspad.page_depth = 0

	root_page_type = wcache.Ppage(elem_root_page).Pg_type()

	switch root_page_type {
	case cache.PAGE_TYPE_NODE: // recursive on node child target page
		elem_root_page = inspad.walk_and_split_if_necessary_and_insert_row_and_node_entries(wcache, elem_root_page, tabledef, row_to_insert) // elem_root_page MAY HAVE BEEN DRAFTIFIED OR NOT

	case cache.PAGE_TYPE_LEAF: // no recursion on leaf root page. Row is inserted into elem_root_page and split pages if needed.
		elem_root_page = wcache.DRAFTIFY_pinned_page(tabledef, elem_root_page)

		inspad.split_if_necessary_and_insert_row_in_leaf_page(wcache, elem_root_page, tabledef, row_to_insert)

	default:
		panic("unexpected pg_type")
	}

	// check cache and sql error

	if rsql_err = wcache.Check_error(); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = inspad.sql_error; rsql_err != nil {
		return rsql_err
	}

	// update page_info

	page_info.Add_pinf_total_leaf_pages_count(inspad.new_leaf_pages_count)
	page_info.Add_pinf_total_leaf_tuples_count(1)

	// root page has not been split ==> return

	if len(inspad.child_node_entry_1) == 0 {
		return nil
	}

	//============ current root page has been split. Create new root page. ============

	// check if node entries are not too long

	if rsql_err := inspad.check_node_entry_max_length(tabledef); rsql_err != nil { // if error, the child node entries are discarded
		inspad.sql_error = rsql_err
		return rsql_err
	}

	// create new root page

	elem_new_root_page_DRAFT = wcache.Fetch_BLANK_DRAFT_page_and_pin_it(tabledef, cache.PAGE_TYPE_NODE, 0) // hint_page_no == 0
	new_root_page_DRAFT = wcache.Ppage_DRAFT_for_write(elem_new_root_page_DRAFT)

	inspad.page_depth++                                      // one more depth level
	new_root_page_DRAFT.Set_pg_page_depth(inspad.page_depth) // set page depth

	// insert bottom target page in new root page, and child node entries

	new_root_page_DRAFT.Set_pg_bottom_target_page_no(root_page_no)

	tuples_written := insert_one_tuple_to_page(new_root_page_DRAFT, 0, inspad.child_node_entry_1) // insert first child node entry at first position
	assert(tuples_written == 1)

	if len(inspad.child_node_entry_2) != 0 {
		tuples_written := insert_one_tuple_to_page(new_root_page_DRAFT, 1, inspad.child_node_entry_2) // insert second child node entry
		assert(tuples_written == 1)
	}

	inspad.child_node_entry_1 = inspad.child_node_entry_1[:0] // child node entries are no more needed
	inspad.child_node_entry_2 = inspad.child_node_entry_2[:0]

	// update pointer to root page

	page_info.Set_pinf_root_page_no(elem_new_root_page_DRAFT.Pce_page_no())

	// check cache and sql error

	if rsql_err = wcache.Check_error(); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = inspad.sql_error; rsql_err != nil {
		return rsql_err
	}

	return nil
}

func Check_dataslot_error_and_NOT_NULL_and_update_ROWID_and_IDENTITY_columns(wcache *cache.Wcache, gtabledef *rsql.GTabledef, base_row_to_insert rsql.Row, explicit_IDENTITY_provided bool) (rsql_err *rsql.Error) {
	var (
		elem_page_info *cache.Page_cache_element
		page_info      *cache.Page
		base_tabledef  *rsql.Tabledef
		base_coldefs   []*rsql.Coldef

		dataslot rsql.IDataslot
		i        int

		column_ROWID   *rsql.Coldef
		dataslot_ROWID rsql.IDataslot
		rowid_value    int64

		column_identity_seqno int
		dataslot_identity     rsql.IDataslot
		identity_value        int64
		increment             int64
		update_next_identity  bool
	)

	base_tabledef = gtabledef.Base
	base_coldefs = base_tabledef.Td_coldefs

	elem_page_info = wcache.Require_DRAFT_page_info_and_pin_it(base_tabledef) // page_info is always updated
	page_info = wcache.Ppage_DRAFT_for_write(elem_page_info)

	defer func() {
		wcache.Unpin(elem_page_info)

	}()

	// check no error in any column in row_to_insert, and find if IDENTITY column exists

	column_identity_seqno = -1

	for i, dataslot = range base_row_to_insert {
		if rsql_err = dataslot.Error(); rsql_err != nil {
			return rsql_err
		}

		if base_coldefs[i].Cd_autonum == rsql.CD_AUTONUM_IDENTITY {
			if column_identity_seqno != -1 {
				panic("impossible: another column with IDENTITY property")
			}

			column_identity_seqno = i

			if explicit_IDENTITY_provided == true && dataslot.NULL_flag() == true { // when an explicit value for IDENTITY is provided, it cannot be NULL
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TABLE_COLUMN_DOESNT_ALLOW_NULL, rsql.ERROR_BATCH_ABORT, base_coldefs[i].Cd_colname)
			}
		}

		if base_coldefs[i].Cd_NOT_NULL_flag == true { // check clause NOT NULL
			if base_coldefs[i].Cd_autonum == 0 && dataslot.NULL_flag() == true { // if ordinary column with NULL value
				return rsql.New_Error(rsql.ERROR_GENERAL, rsql.ERROR_TABLE_COLUMN_DOESNT_ALLOW_NULL, rsql.ERROR_BATCH_ABORT, base_coldefs[i].Cd_colname)
			}
		}
	}

	// fill in ROWID column with incremental value

	column_ROWID = base_coldefs[0] // always exists
	assert(column_ROWID.Cd_autonum == rsql.CD_AUTONUM_ROWID && column_ROWID.Cd_base_seqno == 0)

	dataslot_ROWID = base_row_to_insert[0]

	rowid_value = page_info.Pinf_next_rowid()

	if rowid_value == rsql.MAX_INT64 { // we consider MAX_INT64 to be invalid as ROWID value
		return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_ROWID_COLUMN_OVERFLOW, rsql.ERROR_BATCH_ABORT)
	}

	data.Set_BIGINT_value_from_int64(dataslot_ROWID.(*data.BIGINT), rowid_value)

	rowid_value++
	page_info.Set_pinf_next_rowid(rowid_value)

	// fill in column IDENTITY

	wcache.Wpc_last_identity_exists = false // @@IDENTITY value is discarded
	wcache.Wpc_last_identity = 0

	if column_identity_seqno != -1 { // if column IDENTITY exists
		dataslot_identity = base_row_to_insert[column_identity_seqno]

		identity_value = page_info.Pinf_next_identity()
		if identity_value == cache.NEXT_IDENTITY_IS_SEED { // initial IDENTITY value
			identity_value = gtabledef.Identity_seed
		}

		switch {
		case explicit_IDENTITY_provided == true: // explicit column for IDENTITY is provided. We must update page_info.Pg_next_identity field if necessary.
			explicit_identity_value := data.Get_value_from_xxxINT(dataslot_identity)

			switch {
			case gtabledef.Identity_increment >= 0:
				if explicit_identity_value > identity_value {
					identity_value = explicit_identity_value

					update_next_identity = true
				}

			default:
				if explicit_identity_value < identity_value {
					identity_value = explicit_identity_value

					update_next_identity = true
				}
			}

			wcache.Wpc_last_identity_exists = true // store value for @@IDENTITY
			wcache.Wpc_last_identity = explicit_identity_value

		default: // no explicit column for IDENTITY is provided, we must compute a value for it.
			if identity_value == cache.NEXT_IDENTITY_OVERFLOW {
				return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_IDENTITY_xxxINT_COLUMN_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			}

			if rsql_err = data.Set_value_to_xxxINT(dataslot_identity, identity_value); rsql_err != nil { // dataslot_identity is not referenced by any other expression, so we can safely update its value
				return rsql.New_Error(rsql.ERROR_ARITH, rsql.ERROR_SQLDATA_IDENTITY_xxxINT_COLUMN_OVERFLOW, rsql.ERROR_BATCH_ABORT)
			}

			update_next_identity = true

			wcache.Wpc_last_identity_exists = true // store value for @@IDENTITY
			wcache.Wpc_last_identity = identity_value
		}

		// compute subsequent IDENTITY value

		if update_next_identity == true {
			increment = gtabledef.Identity_increment

			next_identity_value := identity_value + increment

			switch {
			case increment > 0: // if increment > 0, check that identity_value+increment won't go beyond MaxInt64
				if identity_value > rsql.MAX_INT64-increment {
					next_identity_value = cache.NEXT_IDENTITY_OVERFLOW
				}

			case increment < 0: // if increment < 0, check that identity_value+increment is not <= cache.NEXT_IDENTITY_SPECIAL_LIMIT (a little above MinInt64)
				if identity_value <= cache.NEXT_IDENTITY_SPECIAL_LIMIT-increment {
					next_identity_value = cache.NEXT_IDENTITY_OVERFLOW
				}

			default:
				panic("increment cannot be 0")
			}

			page_info.Set_pinf_next_identity(next_identity_value) // next_identity_value can be cache.NEXT_IDENTITY_OVERFLOW
		}
	}

	return nil
}

// Insert_row_into_base_table_and_indexes inserts row into base table and indexes.
//
//       If list_of_indexes is nil, row is inserted in all indexes. This is the normal case for INSERT statement.
//       If list_of_indexes is not nil, row is only inserted in indexes from the list. This is the case for UPDATE statement.
//
func Insert_row_into_base_table_and_indexes(wcache *cache.Wcache, inspad_bag *Inspad_bag, gtabledef *rsql.GTabledef, base_row_to_insert rsql.Row, list_of_indexes map[string]*rsql.Tabledef, explicit_IDENTITY_provided bool) *rsql.Error {
	var (
		rsql_err      *rsql.Error
		base_tabledef *rsql.Tabledef
		inspad        *Inspad
	)

	base_tabledef = gtabledef.Base

	if rsql_err = Check_dataslot_error_and_NOT_NULL_and_update_ROWID_and_IDENTITY_columns(wcache, gtabledef, base_row_to_insert, explicit_IDENTITY_provided); rsql_err != nil { // check that no dataslot contains error. Update value of ROWID, and IDENTITY column if not explicitly provided.
		return rsql_err
	}

	// insert row into base table

	//println("ROWID: ", data.Get_value_from_xxxINT(base_row_to_insert[0]))

	if inspad, rsql_err = inspad_bag.Get_inspad(base_tabledef); rsql_err != nil {
		return rsql_err
	}

	if rsql_err = inspad.Insert_row_into_table(wcache, base_tabledef, base_row_to_insert); rsql_err != nil {
		return rsql_err
	}

	// insert row into index tables

	if list_of_indexes == nil { // if INSERT statement (that is, not UPDATE)
		list_of_indexes = gtabledef.Indexmap // all indexes
	}

	for _, tabledef := range list_of_indexes {
		if inspad, rsql_err = inspad_bag.Get_inspad(tabledef); rsql_err != nil {
			return rsql_err
		}

		index_row := Create_shallow_index_row_from_pool(tabledef, base_row_to_insert)

		if rsql_err = inspad.Insert_row_into_table(wcache, tabledef, index_row); rsql_err != nil {
			return rsql_err
		}

		Put_row_into_pool(index_row)
	}

	return nil
}

// Inspad_bag is just a cache of Scratchbags, which can be reused when many records are inserted.
// This way, we don't need to create a new inspad for each insertion.
//
type Inspad_bag struct {
	table_qname rsql.Object_qname_t
	list        []*Inspad // cache of inspads
}

func (inspad_bag *Inspad_bag) Initialize(table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef) {

	inspad_bag.table_qname = table_qname

	inspad_bag.list = make([]*Inspad, 0, len(gtabledef.Indexmap)+1)
}

func (inspad_bag *Inspad_bag) Get_inspad(tabledef *rsql.Tabledef) (*Inspad, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		inspad   *Inspad
	)

	// look up in cache of inspads

	for _, inspad := range inspad_bag.list {
		if inspad.tblid == cache.Tblid_t(tabledef.Td_tblid) {
			return inspad, nil
		}
	}

	// inspad not found, create a new one

	if inspad, rsql_err = New_inspad(tabledef, inspad_bag.table_qname); rsql_err != nil {
		return nil, rsql_err
	}

	inspad_bag.list = append(inspad_bag.list, inspad)

	return inspad, nil
}

func Insert_series_of_rows(wcache *cache.Wcache, inspad_bag *Inspad_bag, gtabledef *rsql.GTabledef, rows_to_insert []rsql.Row, explicit_IDENTITY_provided bool) *rsql.Error {
	var (
		rsql_err *rsql.Error
	)

	for _, row := range rows_to_insert {

		if rsql_err = Insert_row_into_base_table_and_indexes(wcache, inspad_bag, gtabledef, row, nil, explicit_IDENTITY_provided); rsql_err != nil {
			return rsql_err
		}
	}

	return nil
}
