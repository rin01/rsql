package btree

import (
	"rsql"
	"rsql/cache"
	"rsql/data"
)

// Selpad is a struct that keeps working variables used by SELECT.
//
type Selpad struct {
	Scratchpad
}

// tabledef argument is the table or index, the native key of which will be used by the lookup functions.
//
func New_selpad(tabledef *rsql.Tabledef) (*Selpad, *rsql.Error) {
	var (
		rsql_err *rsql.Error
	)

	selpad := &Selpad{}

	selpad.tblid = cache.Tblid_t(tabledef.Td_tblid)

	// create node_entry_FLASH node entry row

	selpad.node_entry_FLASH = data.New_node_row(rsql.KIND_COL_LEAF, tabledef) // nk fields of tabledef, and last field is BIGINT target page_no field

	selpad.node_syscollators = make([]*data.SYSCOLLATOR, len(tabledef.Td_nk))

	for i, coldef := range tabledef.Td_nk {
		if coldef.Cd_datatype == rsql.DATATYPE_VARCHAR {
			if selpad.node_syscollators[i], rsql_err = data.New_literal_SYSCOLLATOR_NO_LAZY(coldef.Cd_collation); rsql_err != nil { // collators are instanciated. If invalid collation, an error is returned.
				return nil, rsql_err
			}
		}
	}

	// create base_seqno_to_coldefs_pos_map, only for TD_TYPE_INDEX_TABLE

	if tabledef.Td_type == rsql.TD_TYPE_INDEX_TABLE {
		selpad.base_seqno_to_coldefs_pos_map = make(map[uint16]int, len(tabledef.Td_coldefs))

		for i, coldef := range tabledef.Td_coldefs {
			selpad.base_seqno_to_coldefs_pos_map[coldef.Cd_base_seqno] = i
		}
	}

	return selpad, nil
}

// Lookup_nk_starting_row_and_load_record_equal_or_greater looks up the highest row in table that is equal to nk_start_limit; or if not found, the subsequent row, which is greater than nk_start_limit.
//
// Note that even if the result row is GREATER than nk_start_limit, the leading nk fields specified in the query can be EQUAL to the leading nk fields of nk_start_limit.
// Then, the corresponding record is loaded in index_leaf_row.
//
// If the SELECT statement has specified Sargs only for a leading part of the nk fields, the trailing nk fields point to NULL dataslots.
// The index lookup is always performed using all nk fields.
//
// Returns 0 if record not found, and 1 if record has been successfully found.
//
// If record found, it is loaded in index_leaf_row.
//
// Returned tuple_index is >= 0.
//
func (selpad *Selpad) Lookup_nk_starting_row_and_load_record_equal_or_greater(wcache *cache.Wcache, tabledef *rsql.Tabledef, nk_start_limit rsql.Row, index_leaf_row rsql.Row) (count int, page_no cache.Page_no_t, tuple_index int) {
	var (
		elem_page_info *cache.Page_cache_element
		page_info      *cache.Page

		root_page_no cache.Page_no_t

		elem_page            *cache.Page_cache_element
		ppage                *cache.Page
		elem_page_no         cache.Page_no_t
		child_target_page_no cache.Page_no_t
		nk_equality_found    bool
	)

	defer func() {
		if elem_page != nil {
			wcache.Unpin(elem_page)
		}
	}()

	// get page_info and root_page

	elem_page_info = wcache.Fetch_TRANSITORY_or_PUBLIC_page_info_and_pin_it(tabledef)
	page_info = wcache.Ppage(elem_page_info)

	root_page_no = page_info.Pinf_root_page_no()

	wcache.Unpin(elem_page_info)
	elem_page_info = nil
	page_info = nil

	if root_page_no == 0 { // if table is empty, return
		return 0, 0, 0
	}

	//===== lookup tuple (record native key) ---> page_no. The b-tree is walked from root node page until leaf page is reached =====

	elem_page_no = root_page_no

	for { // start from root page, walk through node pages, to finally reach leaf page
		elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, elem_page_no)
		ppage = wcache.Ppage(elem_page)

		if ppage.Pg_type() == cache.PAGE_TYPE_LEAF {
			break
		}

		_, child_target_page_no = selpad.find_target_page_no_in_node_page_for_row_lookup(ppage, tabledef, nk_start_limit) // find child target page

		wcache.Unpin(elem_page)

		elem_page_no = child_target_page_no
	}

	// lookup row from leaf page

	tuple_index, nk_equality_found = selpad.lookup_row_in_leaf_page(ppage, tabledef, nk_start_limit) // index of the highest tuple, which is less or equal than nk_start_limit. NOTE: tuple_index may be -1

	// if the row is not equal, go to the next row

	if nk_equality_found == false { // this means that index is the highest tuple, which is LESS than nk_start_limit
		tuple_index++ // index of next tuple, which is GREATER than nk_start_limit

		if tuple_index == ppage.Pg_tuple_count() { // tuple is the first tuple in the next page
			tuple_index = 0

			elem_page_no = ppage.Pg_next_no()

			if elem_page_no == cache.PAGE_EOT {
				return 0, 0, 0 // end of table has been reached
			}

			wcache.Unpin(elem_page)
			elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, elem_page_no)
			ppage = wcache.Ppage(elem_page)
		}
	}

	// read record

	data.Read_row_from_leaf_page(index_leaf_row, tabledef, ppage, tuple_index)

	return 1, elem_page.Pce_page_no(), tuple_index
}

// Walk_base_table_for_native_key_and_load_record lookup the record in base table and fill base_leaf_row_target dataslots.
// The lookup uses the values of the nk fields in base_leaf_row.
//
// Returns count == 0 if record not found.
//
func (selpad *Selpad) Walk_base_table_for_native_key_and_load_record(wcache *cache.Wcache, base_tabledef *rsql.Tabledef, base_leaf_row rsql.Row, base_leaf_row_target rsql.Row) (count int, page_no cache.Page_no_t, tuple_index int) {
	var (
		elem_page_info *cache.Page_cache_element
		page_info      *cache.Page

		root_page_no cache.Page_no_t

		elem_page            *cache.Page_cache_element
		ppage                *cache.Page
		elem_page_no         cache.Page_no_t
		child_target_page_no cache.Page_no_t
		nk_equality_found    bool
	)

	defer func() {
		if elem_page != nil {
			wcache.Unpin(elem_page)
		}
	}()

	rsql.Assert(base_tabledef.Td_type == rsql.TD_TYPE_BASE_TABLE)

	// get page_info and root_page

	elem_page_info = wcache.Fetch_TRANSITORY_or_PUBLIC_page_info_and_pin_it(base_tabledef)
	page_info = wcache.Ppage(elem_page_info)

	root_page_no = page_info.Pinf_root_page_no()

	wcache.Unpin(elem_page_info)
	elem_page_info = nil
	page_info = nil

	if root_page_no == 0 { // if table is empty, return
		return 0, 0, 0
	}

	//===== lookup tuple (record native key) ---> page_no. The b-tree is walked from root node page until leaf page is reached =====

	elem_page_no = root_page_no

	for { // start from root page, walk through node pages, to finally reach leaf page
		elem_page = wcache.Fetch_TRANSITORY_or_PUBLIC_page_and_pin_it(base_tabledef, elem_page_no)
		ppage = wcache.Ppage(elem_page)

		if ppage.Pg_type() == cache.PAGE_TYPE_LEAF {
			break
		}

		_, child_target_page_no = selpad.find_target_page_no_in_node_page_for_row_lookup(ppage, base_tabledef, base_leaf_row) // find child target page

		wcache.Unpin(elem_page)

		elem_page_no = child_target_page_no
	}

	// lookup row from leaf page

	tuple_index, nk_equality_found = selpad.lookup_row_in_leaf_page(ppage, base_tabledef, base_leaf_row) // index of the highest tuple, which is less or equal than native_key. NOTE: tuple_index may be -1

	if nk_equality_found == false { // record with specified native_key is not found
		return 0, 0, 0
	}

	// read record into base_leaf_row_target

	data.Read_row_from_leaf_page(base_leaf_row_target, base_tabledef, ppage, tuple_index)

	return 1, elem_page.Pce_page_no(), tuple_index
}

// Walk_base_table_for_native_key_and_load_record_for_grouptable lookup the record in base table and fill base_leaf_row_target dataslots.
//
// IT IS EXACTLY THE SAME AS Walk_base_table_for_native_key_and_load_record, but it also lookups DRAFT pages. IT IS USED FOR grouptable.
//
// The lookup uses the values of the nk fields in base_leaf_row.
//
// Returns count == 0 if record not found.
//
func (selpad *Selpad) Walk_base_table_for_native_key_and_load_record_for_grouptable(wcache *cache.Wcache, base_tabledef *rsql.Tabledef, base_leaf_row rsql.Row, base_leaf_row_target rsql.Row) (count int, page_no cache.Page_no_t, tuple_index int) {
	var (
		elem_page_info *cache.Page_cache_element
		page_info      *cache.Page

		root_page_no cache.Page_no_t

		elem_page            *cache.Page_cache_element
		ppage                *cache.Page
		elem_page_no         cache.Page_no_t
		child_target_page_no cache.Page_no_t
		nk_equality_found    bool
	)

	defer func() {
		if elem_page != nil {
			wcache.Unpin(elem_page)
		}
	}()

	rsql.Assert(base_tabledef.Td_type == rsql.TD_TYPE_BASE_TABLE)

	// get page_info and root_page

	elem_page_info = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_info_and_pin_it(base_tabledef)
	page_info = wcache.Ppage(elem_page_info)

	root_page_no = page_info.Pinf_root_page_no()

	wcache.Unpin(elem_page_info)
	elem_page_info = nil
	page_info = nil

	if root_page_no == 0 { // if table is empty, return
		return 0, 0, 0
	}

	//===== lookup tuple (record native key) ---> page_no. The b-tree is walked from root node page until leaf page is reached =====

	elem_page_no = root_page_no

	for { // start from root page, walk through node pages, to finally reach leaf page
		elem_page = wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(base_tabledef, elem_page_no)
		ppage = wcache.Ppage(elem_page)

		if ppage.Pg_type() == cache.PAGE_TYPE_LEAF {
			break
		}

		_, child_target_page_no = selpad.find_target_page_no_in_node_page_for_row_lookup(ppage, base_tabledef, base_leaf_row) // find child target page

		wcache.Unpin(elem_page)

		elem_page_no = child_target_page_no
	}

	// lookup row from leaf page

	tuple_index, nk_equality_found = selpad.lookup_row_in_leaf_page(ppage, base_tabledef, base_leaf_row) // index of the highest tuple, which is less or equal than native_key. NOTE: tuple_index may be -1

	if nk_equality_found == false { // record with specified native_key is not found
		return 0, 0, 0
	}

	// read record into base_leaf_row_target

	data.Read_row_from_leaf_page(base_leaf_row_target, base_tabledef, ppage, tuple_index)

	return 1, elem_page.Pce_page_no(), tuple_index
}
