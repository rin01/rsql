package btree

import (
	"rsql"
	"rsql/cache"
	"rsql/data"
)

// Delpad is a struct that keeps working variables used by DELETE (the same way Inspad is used by INSERT).
//
type Delpad struct {
	Scratchpad

	node_list     []Node_item       // list of nodes walked through, leading to the target leaf page
	deletion_list []cache.Page_no_t // list of pages (leaf page, and possibly node pages) to discard from cache and from logfile, because they became empty

	number_of_deleted_leaf_pages  int
	number_of_deleted_leaf_tuples int
}

type Node_item struct {
	elem_node        *cache.Page_cache_element // node page in cache, pinned
	node_entry_index int                       // index of the entry in the node page, pointing to the next node page to search, or to target leaf page
}

func New_delpad(tabledef *rsql.Tabledef) (*Delpad, *rsql.Error) {
	var (
		rsql_err *rsql.Error
	)

	delpad := &Delpad{}

	delpad.tblid = cache.Tblid_t(tabledef.Td_tblid)

	// create node_entry_FLASH node entry rows

	delpad.node_entry_FLASH = data.New_node_row(rsql.KIND_COL_LEAF, tabledef) // last field is BIGINT target page_no field

	delpad.node_syscollators = make([]*data.SYSCOLLATOR, len(tabledef.Td_nk))

	for i, coldef := range tabledef.Td_nk {
		if coldef.Cd_datatype == rsql.DATATYPE_VARCHAR {
			if delpad.node_syscollators[i], rsql_err = data.New_literal_SYSCOLLATOR_NO_LAZY(coldef.Cd_collation); rsql_err != nil { // collators are instanciated. If invalid collation, an error is returned.
				return nil, rsql_err
			}
		}
	}

	// create base_seqno_to_coldefs_pos_map, only for TD_TYPE_INDEX_TABLE

	if tabledef.Td_type == rsql.TD_TYPE_INDEX_TABLE {
		delpad.base_seqno_to_coldefs_pos_map = make(map[uint16]int, len(tabledef.Td_coldefs))

		for i, coldef := range tabledef.Td_coldefs {
			delpad.base_seqno_to_coldefs_pos_map[coldef.Cd_base_seqno] = i
		}
	}

	// create node_list

	delpad.node_list = make([]Node_item, 0, DELPAD_NODE_LIST_DEFAULT_CAPACITY)
	delpad.deletion_list = make([]cache.Page_no_t, 0, DELPAD_NODE_LIST_DEFAULT_CAPACITY)

	return delpad, nil
}

// Delpad_bag is just a cache of Delpads, which can be reused when many records are deleted.
// This way, we don't need to create a new delpad for each insertion.
//
type Delpad_bag struct {
	list []*Delpad // cache of Delpads
}

func (delpad_bag *Delpad_bag) Initialize(gtabledef *rsql.GTabledef) {

	delpad_bag.list = make([]*Delpad, 0, len(gtabledef.Indexmap)+1)
}

func (delpad_bag *Delpad_bag) Get_delpad(tabledef *rsql.Tabledef) (*Delpad, *rsql.Error) {
	var (
		rsql_err *rsql.Error
		delpad   *Delpad
	)

	// look up in cache of delpads

	for _, delpad := range delpad_bag.list {
		if delpad.tblid == cache.Tblid_t(tabledef.Td_tblid) {
			return delpad, nil
		}
	}

	// delpad not found, create a new one

	if delpad, rsql_err = New_delpad(tabledef); rsql_err != nil {
		return nil, rsql_err
	}

	delpad_bag.list = append(delpad_bag.list, delpad)

	return delpad, nil
}

// Delete_row deletes records from base table and all indexes, matching native key of base_row.
//
// Argument base_row is row with fields from base table.
// Only fields belonging to a native key (of base table or indexes) contain a Dataslot, others are nil.
//
//       If list_of_indexes is nil, row is deleted from all indexes. This is the normal case for DELETE statement.
//       If list_of_indexes is not nil, row is only deleted from indexes in the list. This is the case for UPDATE statement.
//
func Delete_row(wcache *cache.Wcache, delpad_bag *Delpad_bag, gtabledef *rsql.GTabledef, base_row rsql.Row, list_of_indexes map[string]*rsql.Tabledef) (count int, rsql_err *rsql.Error) {
	var (
		base_tabledef *rsql.Tabledef
		delpad        *Delpad
	)

	base_tabledef = gtabledef.Base

	// delete row from base table

	if delpad, rsql_err = delpad_bag.Get_delpad(base_tabledef); rsql_err != nil {
		return 0, rsql_err
	}

	count = delpad.Delete_row_from_table(wcache, base_tabledef, base_row) // only fields belonging to a native key (of base table or index) contain a Dataslot, others are nil
	if count == 0 {
		return 0, nil // no record has been found. It should not exist in indexes too.
	}

	// record to delete exists. Delete row from index tables

	if list_of_indexes == nil { // if DELETE statement (that is, not UPDATE)
		list_of_indexes = gtabledef.Indexmap // all indexes
	}

	for _, tabledef := range list_of_indexes {
		if delpad, rsql_err = delpad_bag.Get_delpad(tabledef); rsql_err != nil {
			return 0, rsql_err
		}

		index_row := Create_shallow_index_row_from_pool(tabledef, base_row)

		count2 := delpad.Delete_row_from_table(wcache, tabledef, index_row)
		assert(count2 == 1)

		Put_row_into_pool(index_row)
	}

	return 1, nil
}

// Delete_row_from_table deletes a row from table.
//
// Returns 0 if record not found, and 1 if record has been successfully deleted.
//
func (delpad *Delpad) Delete_row_from_table(wcache *cache.Wcache, tabledef *rsql.Tabledef, row_to_delete rsql.Row) (count int) {
	var (
		elem_page_info *cache.Page_cache_element
		page_info      *cache.Page

		root_page_no cache.Page_no_t

		elem_page_no         cache.Page_no_t
		node_entry_index     int
		child_target_page_no cache.Page_no_t
		elem_leaf_page       *cache.Page_cache_element
		leaf_ppage           *cache.Page

		tuple_index       int
		nk_equality_found bool
	)

	defer func() {
		if elem_leaf_page != nil {
			wcache.Unpin(elem_leaf_page)
		}

		for i := len(delpad.node_list) - 1; i >= 0; i-- {
			wcache.Unpin(delpad.node_list[i].elem_node)
		}
		delpad.node_list = delpad.node_list[:0]

		if elem_page_info != nil {
			wcache.Unpin(elem_page_info)
		}

		for _, page_no := range delpad.deletion_list {
			wcache.Delete_DRAFT_element_and_page(tabledef, page_no)
		}
		delpad.deletion_list = delpad.deletion_list[:0]
	}()

	delpad.node_list = delpad.node_list[:0]
	delpad.deletion_list = delpad.deletion_list[:0]

	// get page_info and root_page

	elem_page_info = wcache.Require_DRAFT_page_info_and_pin_it(tabledef) // after row has been deleted, page_info is always updated
	page_info = wcache.Ppage_DRAFT_for_write(elem_page_info)

	root_page_no = page_info.Pinf_root_page_no()

	if root_page_no == 0 { // if table is empty, return
		return 0
	}

	//===== lookup tuple (record native key) ---> page_no. The b-tree is walked from root node page until leaf page is reached =====

	elem_page_no = root_page_no

	for { // start from root page, walk through node pages, to finally reach leaf page
		elem_page := wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, elem_page_no)
		ppage := wcache.Ppage(elem_page)

		if ppage.Pg_type() == cache.PAGE_TYPE_LEAF {
			elem_leaf_page = elem_page
			elem_page = nil
			ppage = nil
			break
		}

		node_entry_index, child_target_page_no = delpad.find_target_page_no_in_node_page_for_row_lookup(ppage, tabledef, row_to_delete) // find child target page

		delpad.node_list = append(delpad.node_list, Node_item{elem_node: elem_page, node_entry_index: node_entry_index}) // each node is appended to delpad.node_list

		elem_page_no = child_target_page_no
	}

	// delete row from leaf page

	elem_leaf_page = wcache.DRAFTIFY_pinned_page(tabledef, elem_leaf_page)
	leaf_ppage = wcache.Ppage_DRAFT_for_write(elem_leaf_page)

	if tuple_index, nk_equality_found = delpad.lookup_row_in_leaf_page(leaf_ppage, tabledef, row_to_delete); nk_equality_found == false { // index of the highest tuple, which is less or equal than row_to_delete
		return 0 // record not found
	}

	page_info.Add_pinf_total_leaf_tuples_count(-1) // update page_info

	if discard_page := delete_one_tuple_from_leaf_page(leaf_ppage, tuple_index); discard_page == false { // delete tuple in leaf page
		return 1 // leaf page doesn't need to be discarded, as it is not empty
	}

	// leaf page is empty and must be discarded. Delete entry in node page, and discard node pages if empty.

	fusion_prev_and_next_pages_of(wcache, tabledef, leaf_ppage)

	delpad.deletion_list = append(delpad.deletion_list, elem_leaf_page.Pce_page_no()) // put leaf page in deletion list
	page_info.Add_pinf_total_leaf_pages_count(-1)                                     // update page_info

	if len(delpad.node_list) == 0 { // no node page. Table became empty.
		assert(leaf_ppage.Pg_no() == root_page_no)

		page_info.Set_pinf_root_page_no(0)

		return 1
	}

	for i := len(delpad.node_list) - 1; i >= 0; i-- { // node pages exist
		node_item := delpad.node_list[i]
		elem_node_page := node_item.elem_node
		node_entry_index := node_item.node_entry_index

		elem_node_page = wcache.DRAFTIFY_pinned_page(tabledef, elem_node_page)
		delpad.node_list[i].elem_node = elem_node_page
		node_ppage := wcache.Ppage_DRAFT_for_write(elem_node_page)

		if discard_page := delete_one_tuple_from_node_page(node_ppage, node_entry_index); discard_page == false { // delete entry from current node page, and check if current node page must be discarded too
			if i > 0 {
				return 1 // an entry has been deleted from non-root node page, but it is not discardable
			}

			break // an entry has been deleted from root page. If it contains only the bottom target page entry, it must be discarded.
		}

		assert(i != 0) // root node is never empty, because it is already discarded if it contains one entry

		fusion_prev_and_next_pages_of(wcache, tabledef, node_ppage)

		delpad.deletion_list = append(delpad.deletion_list, elem_node_page.Pce_page_no()) // put node page in deletion list
	}

	// an entry has been deleted from root page. If it has no entry, except bottom target page entry, discard it, as well as child nodes with one entry.

	elem_page_no = root_page_no

	for {
		elem_page := wcache.Fetch_DRAFT_or_TRANSITORY_or_PUBLIC_page_and_pin_it(tabledef, elem_page_no)
		ppage := wcache.Ppage(elem_page)

		if ppage.Pg_tuple_count() > 0 { // node page or leaf page
			wcache.Unpin(elem_page)
			return 1 //---> exit
		}

		target_page_no := ppage.Pg_bottom_target_page_no()
		page_info.Set_pinf_root_page_no(target_page_no)

		delpad.deletion_list = append(delpad.deletion_list, elem_page.Pce_page_no())
		wcache.Unpin(elem_page)

		elem_page_no = target_page_no
	}
}
