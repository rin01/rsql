package btree

import (
	"rsql"
	"rsql/cache"
	"rsql/data"
)

// Udpad_bag is just a cache of Delpads and Inspads, which can be reused when many records are updated.
// This way, we don't need to create a new Delpad and Inspad for each update.
//
type Udpad_bag struct {
	Delpad_bag
	Inspad_bag
}

func (udpad_bag *Udpad_bag) Initialize(table_qname rsql.Object_qname_t, gtabledef *rsql.GTabledef) {

	udpad_bag.Delpad_bag.Initialize(gtabledef)
	udpad_bag.Inspad_bag.Initialize(table_qname, gtabledef)
}

// replace_one_tuple_in_page_in_place tries to replace one tuple in page in-place at tuple_index position.
//
// If page contains one tuple, the tuple is always replaced, as there is always room enough for a tuple in a page.
//
// If page is not large enough because tuple is too large, the tuple is not inserted.
//
//     tuple_index              index of the tuple to replace. Value is [0 ... page.Pg_tuple_count()-1]
//     tuple                    tuple to replace
//
//     returns 1 if tuple has been written in page. Else, 0.
//
// For the moment, this function is only used for GROUP BY SELECT, for record in grouptable. UPDATE statement doesn't use it yet.
//
func replace_one_tuple_in_page_in_place(page *cache.Page, tuple_index int, tuple rsql.Tuple) int {
	var (
		tuple_count             int
		old_tuple_length        int
		tuple_length            int
		tuple_offset            int
		next_tuple_offset       int
		sentinel_offset         int
		consumed_room_new       int
		upper_tuples_chunk_size int
		i                       int
		delta                   int
	)

	tuple_count = page.Pg_tuple_count()

	assert(tuple_index >= 0)
	assert(tuple_index < tuple_count)

	tuple_offset = page.Pg_tuple_offset(tuple_index)

	old_tuple_length = page.Pg_tuple_length(tuple_index)
	tuple_length = len(tuple)

	// if tuple length is same as old tuple length, just overwrite the old tuple with the new tuple

	if tuple_length == old_tuple_length {
		copy(page.Pg_canvas[tuple_offset:tuple_offset+tuple_length], tuple) // overwrite old tuple with new one

		return 1
	}

	//=== if tuple length is different, check if space is available ===

	consumed_room_new = page.Consumed_room() - old_tuple_length + tuple_length

	if consumed_room_new > cache.PAGE_CANVAS_LENGTH {
		return 0 //===> if not enough space for the new tuple, just return 0.
	}

	// shift upper tuples chunk, and shift and adjust tuple offsets

	next_tuple_offset = page.Pg_tuple_offset(tuple_index + 1)
	sentinel_offset = page.Pg_tuple_offset(tuple_count)

	upper_tuples_chunk_size = sentinel_offset - next_tuple_offset // size of upper block that must be moved upwards or downwards

	assert(upper_tuples_chunk_size >= 0)

	if upper_tuples_chunk_size > 0 { // if tuples exist after position of the tuple to insert
		copy(page.Pg_canvas[tuple_offset+tuple_length:tuple_offset+tuple_length+upper_tuples_chunk_size], // shift upwards or downwards block of tuples after the tuple to insert
			page.Pg_canvas[next_tuple_offset:next_tuple_offset+upper_tuples_chunk_size])
	}

	delta = tuple_length - old_tuple_length // negative or positive

	for i = tuple_index + 1; i <= tuple_count; i++ { // copy and adjust offsets of tuples after the tuple to insert
		page.Set_pg_tuple_offset(i, uint16(page.Pg_tuple_offset(i)+delta))
	}

	// write insertion tuple data

	copy(page.Pg_canvas[tuple_offset:tuple_offset+tuple_length], tuple) // copy tuple to insert

	// finalize page info

	assert(page.Available_room() >= 0)

	return 1
}

// Replace_one_row_in_page_in_place tries to replace one row in page in-place at tuple_index position.
//
// If page contains one tuple, the tuple is always replaced, as there is always room enough for a tuple in a page.
//
// If page is not large enough because tuple is too large, the tuple is not inserted.
//
//     tuple_index              index of the tuple to replace. Value is [0 ... page.Pg_tuple_count()-1]
//     row                      row to replace
//
//     returns 1 if tuple has been written in page. Else, 0.
//
// For the moment, this function is only used for GROUP BY SELECT, for record in grouptable. UPDATE statement doesn't use it yet.
//
func (inspad *Inspad) Replace_one_row_in_page_in_place(wcache *cache.Wcache, tabledef *rsql.Tabledef, page_no cache.Page_no_t, tuple_index int, row rsql.Row) (count int, rsql_err *rsql.Error) {
	var (
		elem_page *cache.Page_cache_element
		page      *cache.Page
	)

	elem_page = wcache.Require_DRAFT_page_and_pin_it(tabledef, page_no)
	defer wcache.Unpin(elem_page)

	page = wcache.Ppage_DRAFT_for_write(elem_page)

	if inspad.tuple_to_insert, rsql_err = data.Write_serialized_row(inspad.tuple_to_insert[:0], row); rsql_err != nil {
		return 0, rsql_err
	}

	count = replace_one_tuple_in_page_in_place(page, tuple_index, inspad.tuple_to_insert)

	return count, nil
}
