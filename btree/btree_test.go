package btree

import (
	"testing"

	"rsql"
	"rsql/cache"
	"rsql/data"
)

func new_sample_tabledef() *rsql.Tabledef {

	tabledef := &rsql.Tabledef{}

	coldef0 := &rsql.Coldef{
		Cd_datatype:   rsql.DATATYPE_INT,
		Cd_base_seqno: 0,
	}

	coldef1 := &rsql.Coldef{
		Cd_datatype:           rsql.DATATYPE_VARCHAR,
		Cd_precision:          1000,
		Cd_scale:              0,
		Cd_fixlen_flag:        false,
		Cd_collation_strength: rsql.COLLSTRENGTH_1,
		Cd_collation:          "fr_cs_as",

		Cd_base_seqno: 1,
	}

	coldef2 := &rsql.Coldef{
		Cd_datatype:   rsql.DATATYPE_SMALLINT,
		Cd_base_seqno: 2,
	}

	coldef3 := &rsql.Coldef{
		Cd_datatype:   rsql.DATATYPE_INT,
		Cd_base_seqno: 3,
	}

	tabledef.Td_coldefs = []*rsql.Coldef{coldef0, coldef1, coldef2, coldef3}
	tabledef.Td_nk = []*rsql.Coldef{coldef2, coldef1}

	return tabledef
}

func new_row_with_value(tabledef *rsql.Tabledef, values ...interface{}) rsql.Row {
	var (
		rsql_err *rsql.Error
		dataslot rsql.IDataslot
	)

	row := make([]rsql.IDataslot, len(tabledef.Td_coldefs))

	for i, coldef := range tabledef.Td_coldefs {
		switch coldef.Cd_datatype {
		case rsql.DATATYPE_VARCHAR:
			if values[i] == nil {
				dataslot = data.New_VARCHAR_NULL(rsql.KIND_RO_LEAF, 300, false)
			} else {
				if dataslot, rsql_err = data.New_literal_VARCHAR_value(values[i].(string)); rsql_err != nil {
					panic("bad value for VARCHAR")
				}
			}
		case rsql.DATATYPE_SMALLINT:
			if values[i] == nil {
				dataslot = data.New_SMALLINT_NULL(rsql.KIND_RO_LEAF)
			} else {
				dataslot = data.New_literal_SMALLINT_value(values[i].(int16))
			}
		case rsql.DATATYPE_INT:
			if values[i] == nil {
				dataslot = data.New_INT_NULL(rsql.KIND_RO_LEAF)
			} else {
				dataslot = data.New_literal_INT_value(values[i].(int32))
			}
		default:
			panic("you shoud add a 'case' for this datatype in this function")
		}

		row[i] = dataslot
	}

	return row
}

func Test_1(t *testing.T) {
	var (
		rsql_err *rsql.Error
	)

	page := &cache.Page{}
	page.Set_pg_type(cache.PAGE_TYPE_LEAF)

	tabledef := new_sample_tabledef()
	inspad, rsql_err := New_inspad(tabledef)
	assert(rsql_err == nil)

	pagebuff := &Page_buffer{}
	pagebuff.reset()

	// row 0

	//	row := new_row_with_value(tabledef, int32(0x01010101), "helloo", int16(10), int32(0x10101010)) // INT, VARCHAR, SMALLINT, INT
	row := new_row_with_value(tabledef, nil, "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq", nil, nil) // INT, VARCHAR, SMALLINT, INT

	if inspad.tuple, rsql_err = data.Write_serialized_row(inspad.tuple[:0], row); rsql_err != nil {
		t.Fatal("failed")
	}

	count := insert_one_tuple_to_page(page, 0, inspad.tuple)
	//println("qqqqqqq", inspad.tuple.Field_count())
	assert(count == 1)

	// row 1

	row = new_row_with_value(tabledef, int32(0x01010101), "helloo", int16(14), int32(0x11111111)) // INT, VARCHAR, SMALLINT, INT

	if inspad.tuple, rsql_err = data.Write_serialized_row(inspad.tuple[:0], row); rsql_err != nil {
		t.Fatal("failed")
	}

	//	count = insert_one_tuple_to_page(page, 1, inspad.tuple)
	assert(count == 1)

	// row 2

	row = new_row_with_value(tabledef, int32(0x01010101), "helloo", int16(16), int32(0x12121212)) // INT, VARCHAR, SMALLINT, INT

	if inspad.tuple, rsql_err = data.Write_serialized_row(inspad.tuple[:0], row); rsql_err != nil {
		t.Fatal("failed")
	}

	//	count = insert_one_tuple_to_page(page, 2, inspad.tuple)
	assert(count == 1)

	//println(cache.PAGE_CANVAS_LENGTH)

	//rsql.Printf("%v\n", page.Pg_canvas)

	//============= copy to pagebuff ====

	nn := pagebuff.append_all_tuples_from_page(page)
	nn += pagebuff.append_all_tuples_from_page(page)
	nn += pagebuff.append_all_tuples_from_page(page)
	nn += pagebuff.append_all_tuples_from_page(page)
	nn += pagebuff.append_all_tuples_from_page(page)
	nn += pagebuff.append_all_tuples_from_page(page)
	//println(nn, pagebuff.tuple_count())

	//	row = new_row_with_value(tabledef, int32(0x02020202), "helloo", int16(14), int32(0x7fffffff)) // INT, VARCHAR, SMALLINT, INT
	row = new_row_with_value(tabledef, nil, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", nil, nil) // INT, VARCHAR, SMALLINT, INT
	if inspad.tuple, rsql_err = data.Write_serialized_row(inspad.tuple[:0], row); rsql_err != nil {
		t.Fatal("failed")
	}

	pagebuff.insert_one_tuple(1, inspad.tuple)

	row = new_row_with_value(tabledef, int32(0x02020202), "heloooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooohelooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooohelooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooohelooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooohelooooooooooo", int16(14), int32(0x7fffffff)) // INT, VARCHAR, SMALLINT, INT
	if inspad.tuple, rsql_err = data.Write_serialized_row(inspad.tuple[:0], row); rsql_err != nil {
		t.Fatal(rsql_err) //"failed")
	}

	pagebuff.insert_one_tuple(3, inspad.tuple)

	split_0, split_1 := pagebuff.split_leaf(3)

	for i := 0; i < pagebuff.tuple_count(); i++ {
		if i == split_0 {
			//rsql.Println("------ split 0 ------")
		}
		if i == split_1 {
			//rsql.Println("------ split 1 ------")
		}
		//rsql.Println(i, "---", pagebuff.get_tuple(i))
	}

	//rsql.Println("consumed room", pagebuff.consumed_room_by_tuples(0, split_0), pagebuff.consumed_room_by_tuples(split_0, split_1))

	/*
		//========== export to page_dest

		page_dest := &cache.Page{}
		page_dest.Set_pg_type(cache.PAGE_TYPE_LEAF)

		nnn := pagebuff.copy_tuples_to_page(page_dest, 0, -1)
		println("nnn", nnn)

		rsql.Printf("%v\n", page_dest.Pg_canvas)


		println("--------", pagebuff.consumed_room_by_tuples(0,3), pagebuff.consumed_room_by_tuples(0,6), pagebuff.consumed_room_by_tuples(2,5), "page", page.Consumed_room(), "page_dest", page_dest.Consumed_room())
	*/

}
