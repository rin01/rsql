package btree

import (
	"sync"

	"rsql"
	"rsql/cache"
	"rsql/data"
)

const DEFAULT_ROW_CAPACITY_IN_POOL = 50

// global pool of []rsql.IDataslot rows used by Create_shallow_index_row_from_pool
//
var g_pool_of_rows = sync.Pool{
	New: func() interface{} {
		return make([]rsql.IDataslot, DEFAULT_ROW_CAPACITY_IN_POOL)
	},
}

type Scratchpad struct {
	tblid cache.Tblid_t // a scratchpad can only be used with the Tabledef used for its creation

	node_entry_FLASH  rsql.Row            // native key columns, and target page_no as BIGINT. Used when a function needs to read a tuple from disk. Life duration is limited to the function. E.g. compare_leaf_row_native_key_with_tuple_for_sort().
	node_syscollators []*data.SYSCOLLATOR // if node_entry_FLASH[i] column is VARCHAR, node_syscollators[i] contains the collator used for comparison. Else, node_syscollators[i] is nil.

	base_seqno_to_coldefs_pos_map map[uint16]int // map that converts seqno to position in tabledef.Td_coldefs slice, only for TD_TYPE_INDEX_TABLE. For TD_TYPE_BASE_TABLE, we know that position == seqno.
}

func (scratchpad *Scratchpad) base_seqno_to_coldefs_pos(td_type rsql.Td_type_t, base_seqno uint16) int {
	var (
		pos int
		ok  bool
	)

	if td_type == rsql.TD_TYPE_BASE_TABLE {
		return int(base_seqno)
	}

	if pos, ok = scratchpad.base_seqno_to_coldefs_pos_map[base_seqno]; ok == false {
		panic("impossible")
	}

	return pos
}

// Create_shallow_index_row_from_pool creates a Row for index table (TD_TYPE_INDEX_TABLE).
// The dataslots point to dataslots in base_row (shallow copy).
//
// We use a pool, because lots of Rows are used and discarded when we INSERT or DELETE lots of records.
//
func Create_shallow_index_row_from_pool(tabledef *rsql.Tabledef, base_row rsql.Row) rsql.Row {
	var (
		coldefs   []*rsql.Coldef
		index_row rsql.Row
	)

	coldefs = tabledef.Td_coldefs

	index_row = g_pool_of_rows.Get().([]rsql.IDataslot) // get []rsql.IDataslot from pool

	if len(coldefs) <= cap(index_row) { // check if large enough
		index_row = index_row[:len(coldefs)]
	} else {
		index_row = make(rsql.Row, len(coldefs))
	}

	for i, coldef := range coldefs {
		base_seqno := coldefs[i].Cd_base_seqno

		dataslot := base_row[base_seqno]
		assert(dataslot.Datatype() == coldef.Cd_datatype)

		index_row[i] = dataslot
	}

	return index_row
}

// Put_row_into_pool puts a unused row into pool.
// This function sets all fields of Row to nil, so that when it is fetched again, it is in a clean state.
//
func Put_row_into_pool(row []rsql.IDataslot) {

	for i, _ := range row { // reset all fields
		row[i] = nil
	}

	row = row[:0]

	g_pool_of_rows.Put(row) // put the row into the pool
}

// find_target_page_no_in_node_page_for_row_lookup returns the target page_no in a node page, for the row_to_lookup.
//
// row_to_lookup must be a leaf row, in the order specified by tabledef.Td_coldefs[].
//
// This function is used to walk the btree, to find the leaf page in which the row is located or must be inserted.
//
//      The node page can be empty. In this case, the function returns the bottom target page no.
//
//      The returned target page_no belongs to the entry in the node page which is the highest tuple less or equal than row_to_lookup.
//      If row_to_lookup is smaller than the first entry, return pg_bottom_target_page_no.
//
func (scratchpad *Scratchpad) find_target_page_no_in_node_page_for_row_lookup(page *cache.Page, tabledef *rsql.Tabledef, row_to_lookup rsql.Row) (node_entry_index int, target_page_no cache.Page_no_t) {
	var (
		t_lower     int // tuple at t_lower is always <= row_to_lookup
		t_middle    int // t_middle is always > t_lower and < t_upper_x, except during the last iteration, where t_middle == t_lower
		t_upper_x   int // tuple at t_upper_x is always > row_to_lookup. Initial value of t_upper_x is index of sentinel tuple.
		node_entry  rsql.Tuple
		comp_result data.Compsort_t
	)

	assert(page.Pg_type() == cache.PAGE_TYPE_NODE)

	// find the tuple that is the highest tuple, less or equal than the entry to lookup. Only native key fields are compared

	t_lower = -1
	t_upper_x = page.Pg_tuple_count()

	if t_upper_x == 0 { // if page empty, ===> return bottom target page no
		return -1, page.Pg_bottom_target_page_no()
	}

	t_middle = t_upper_x - 1 // the first occurence of the loop will check if the row to lookup is greater or equal than the top tuple in the page.

	for t_middle != t_lower {
		node_entry = page.Pg_tuple(t_middle)

		comp_result = scratchpad.compare_leaf_row_native_key_with_tuple_for_sort(tabledef, row_to_lookup, node_entry, rsql.TUPLE_TYPE_NODE)

		if comp_result >= 0 {
			t_lower = t_middle
		} else {
			t_upper_x = t_middle
		}

		t_middle = (t_lower+t_upper_x+2)/2 - 1 // +2 and -1 are to floor the result, when t_lower==-1 and t_upper_x==0 (in this case, we want t_middle==-1, and not 0)
	}

	// t_middle is the highest tuple that is less or equal than the row to lookup. That is:    row to lookup >= tuple at t_middle

	// if t_middle == -1, it means that the row must be looked up or inserted in page.Pg_bottom_target_page_no().

	assert(t_middle >= -1)

	// read the page_no field of the t_middle tuple

	if t_middle == -1 { // return bottom target page no
		return -1, page.Pg_bottom_target_page_no()
	}

	node_entry = page.Pg_tuple(t_middle)

	target_page_no = cache.Page_no_t(node_entry.Get_last_field_uint64())

	return t_middle, target_page_no
}

// compare_leaf_row_native_key_with_tuple_for_sort compares a record native key with a leaf tuple or node tuple for sort.
//
//     scratchpad                     scratchpad must have been initialized. To perform the comparison, this function uses the dataslots of scratchpad, as scratch working memory.
//     tabledef                       Tabledef. It is used by the function to get the columns making up the native key.
//     leaf_row                       row to compare to the tuple. It must be a leaf row, in the order specified by tabledef.Td_coldefs[]. Native key columns must contain a dataslot.
//     tuple                          tuple, to which the row will be compared.
//                                                If type is TUPLE_TYPE_NODE, 'tuple' match exactly the fields of scratchpad.node_entry_FLASH.
//                                                If type is TUPLE_TYPE_LEAF, the native key fields of 'tuple' match exactly the fields of scratchpad.node_entry_FLASH.
//     tuple_type                     TUPLE_TYPE_LEAF or TUPLE_TYPE_NODE.
//                                                If TUPLE_TYPE_NODE, 'tuple' contains only the native key fields, and a BIGINT target page_no field, which is not used here.
//                                                If TUPLE_TYPE_LEAF, 'tuple' contains all fields of tabledef.Td_coldefs[].
//
func (scratchpad *Scratchpad) compare_leaf_row_native_key_with_tuple_for_sort(tabledef *rsql.Tabledef, leaf_row rsql.Row, tuple rsql.Tuple, tuple_type rsql.Tuple_type_t) data.Compsort_t {
	var (
		coldef         *rsql.Coldef
		i              int
		idx            int
		tuple_field_no uint16
		dataslot_a     rsql.IDataslot
		dataslot_b     rsql.IDataslot
		syscollator    *data.SYSCOLLATOR
		comp_result    data.Compsort_t
	)

	assert(scratchpad.tblid == cache.Tblid_t(tabledef.Td_tblid))

	for i, coldef = range tabledef.Td_nk { // for each column in native key

		//===== dataslot_a from leaf row to compare =====

		idx = scratchpad.base_seqno_to_coldefs_pos(tabledef.Td_type, coldef.Cd_base_seqno)

		dataslot_a = leaf_row[idx] // each field making up the native key

		//===== dataslot_b from tuple field, filled using scratchpad dataslots =====

		dataslot_b = scratchpad.node_entry_FLASH[i] // recipient for the value of native key fields from tuple

		tuple_field_no = uint16(idx)            // position of native key fields for leaf tuple
		if tuple_type == rsql.TUPLE_TYPE_NODE { // position of native key fields for node tuple
			tuple_field_no = uint16(i)
		}

		data.Deserialize_tuple_field(dataslot_b, tuple, tuple_field_no) // put tuple field value into dataslot_b

		//===== compare =====

		syscollator = scratchpad.node_syscollators[i]

		comp_result = Comp_for_sort_XXX(dataslot_a, dataslot_b, syscollator) // for sort, a non-NULL value is greater than NULL

		if comp_result != 0 { // as soon as one field is greater or less, the whole record is also greater or less
			return comp_result //===> exit
		}

		// here, fields are equal. We must continue comparison with next field
	}

	assert(comp_result == 0) // all native key fields are equal

	return comp_result
}

// lookup_row_in_leaf_page finds the position at which row_to_lookup exists, in leaf page.
// If row doesn't exist, the retuned position is the index of the highest tuple, which is less than row_to_lookup.
//
// The native key fields of the row are compared to the native key fields of the tuples in leaf page.
//
//         The tuple returned is the highest tuple, which is less or equal than the row_to_lookup.
//
//         Returns position    -1 ... page.Pg_tuple_count()-1
//
// If you use this function to find the index at which a new record must be inserted, it must be inserted at tuple_index+1
// If t_middle == -1, it means that the row must be inserted at first position in the page.
//
func (scratchpad *Scratchpad) lookup_row_in_leaf_page(page *cache.Page, tabledef *rsql.Tabledef, row_to_lookup rsql.Row) (tuple_index int, nk_equality_found bool) {
	var (
		t_lower     int // tuple at t_lower is always <= row_to_lookup
		t_middle    int // t_middle is always > t_lower and < t_upper_x, except during the last iteration, where t_middle == t_lower
		t_upper_x   int // tuple at t_upper_x is always > row_to_lookup. Initial value of t_upper_x is index of sentinel tuple.
		tuple_leaf  rsql.Tuple
		comp_result data.Compsort_t
	)

	assert(page.Pg_type() == cache.PAGE_TYPE_LEAF)

	// find the tuple that is the highest tuple, less or equal than the row to lookup

	t_lower = -1
	t_upper_x = page.Pg_tuple_count()

	if t_upper_x == 0 { // if page empty, ===> return. This should never happen, as leaf pages always contain at least one record.
		return -1, false // TODO replace this line by panic() ?
	}

	t_middle = t_upper_x - 1 // the first occurence of the loop will check if the row to lookup is greater or equal than the top tuple in the page.

	for t_middle != t_lower {
		tuple_leaf = page.Pg_tuple(t_middle)

		comp_result = scratchpad.compare_leaf_row_native_key_with_tuple_for_sort(tabledef, row_to_lookup, tuple_leaf, rsql.TUPLE_TYPE_LEAF)

		if comp_result >= 0 {
			t_lower = t_middle
		} else {
			t_upper_x = t_middle
		}

		if comp_result == 0 { // native key equality found
			nk_equality_found = true
		}

		t_middle = (t_lower+t_upper_x+2)/2 - 1 // +2 and -1 are to floor the result, when t_lower==-1 and t_upper_x==0 (in this case, we want t_middle==-1, and not 0)
	}

	// t_middle is the highest tuple that is less or equal than the row to lookup.

	assert(t_middle >= -1)

	return t_middle, nk_equality_found
}

func Target_page_no_in_node(node_page *cache.Page, entry_no int) (target_page_no cache.Page_no_t) {
	var (
		node_entry rsql.Tuple
	)

	assert(node_page.Pg_type() == cache.PAGE_TYPE_NODE)

	if entry_no == -1 {
		return node_page.Pg_bottom_target_page_no()
	}

	node_entry = node_page.Pg_tuple(entry_no)

	target_page_no = cache.Page_no_t(node_entry.Get_last_field_uint64())

	return target_page_no
}

// Equality_leaf_rows_native_key_for_sort is used by GROUP BY, for grouptable, to check if grouptable_row is same group as insertion_row.
// As for sort, a non-NULL value is not equal to NULL, and NULL == NULL.
// If dataslots contain an error, panics.
//
func (scratchpad *Scratchpad) Equality_leaf_rows_native_key_for_sort(tabledef *rsql.Tabledef, leaf_row_a rsql.Row, leaf_row_b rsql.Row) bool {
	var (
		coldef      *rsql.Coldef
		dataslot_a  rsql.IDataslot
		dataslot_b  rsql.IDataslot
		syscollator *data.SYSCOLLATOR
		comp_result data.Compsort_t
	)

	assert(scratchpad.tblid == cache.Tblid_t(tabledef.Td_tblid))

	for i := len(tabledef.Td_nk) - 1; i >= 0; i-- { // for each column in native key. Reverse order because an inequality is more likely to occur for the last fields.
		coldef = tabledef.Td_nk[i]
		dataslot_a = leaf_row_a[coldef.Cd_base_seqno] // each field making up the native key
		dataslot_b = leaf_row_b[coldef.Cd_base_seqno] // each field making up the native key

		// compare

		syscollator = scratchpad.node_syscollators[i]

		comp_result = Comp_for_sort_XXX(dataslot_a, dataslot_b, syscollator) // for sort, a non-NULL value is greater than NULL

		if comp_result != 0 { // inequality found
			return false //===> exit
		}

		// here, fields are equal. We must continue comparison with next field
	}

	assert(comp_result == 0) // all native key fields are equal

	return true
}
