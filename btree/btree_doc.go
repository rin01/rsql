// package btree implements a btree on disk.
//
//       A SQL table is made of:
//            - a base table
//            - optionally one or mode index tables
//
//       Each base table or index table is stored in its own file.
//       The Tabledef structures describe the layout of these files.
//       Each file is made up of pages, which contain tuples (which are serialized records).
//
//       The btree implementation uses heavily package rsql/cache, to manage the pages making up the btrees.
//       It also uses package rsql/data, to make comparison between tuples.
//
//       http://www.cs.usfca.edu/~galles/visualization/BPlusTree.html        <------ MUST SEE ;-)))
//       http://en.wikipedia.org/wiki/B%2B_tree
//       https://www.percona.com/files/presentations/percona-live/london-2011/PLUK2011-b-tree-indexes-and-innodb.pdf
//
package btree
