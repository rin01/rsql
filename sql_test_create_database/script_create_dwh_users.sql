
--drop database dwh
create database dwh


use [DWH]


create user CHA0666 for login [EZCORP\CHA0666]
create user CHA0747 for login [EZCORP\CHA0747]
create user CHA4601 for login [EZCORP\CHA4601]
create user CHA7068 for login [EZCORP\CHA7068]
create user CHA8459 for login [EZCORP\CHA8459]
create user CHIGOHE for login [EZCORP\CHIGOHE]
create user CHK2423 for login [EZCORP\CHK2423]
create user CHL4579 for login [EZCORP\CHL4579]
create user CHL4628 for login [EZCORP\CHL4628]
create user CHL4780 for login [EZCORP\CHL4780]
create user CHL4794 for login [EZCORP\CHL4794]
create user chl4806 for login [EZCORP\CHL4806]
create user CHL4871 for login [EZCORP\CHL4871]
create user CHV2541 for login [EZCORP\CHV2541]
create user CHV3755 for login [EZCORP\CHV3755]
create user CHV7113 for login [EZCORP\CHV7113]
create user CHVWALS for login [EZCORP\CHVWALS]
create user CHY0787 for login [EZCORP\CHY0787]
create user CHY7358 for login [EZCORP\CHY7358]
create user CHZ9690 for login [EZCORP\CHZ9690]
create user cha8751 for login [EZCORP\cha8751]
create user chl4708 for login [EZCORP\chl4708]
create user guest
create user user_alibaba
create user walserw for login [CH\walserw]
create user zh_compta
create user zh_compta2


create role autres
create role controlling


alter role autres add member user_alibaba
alter role controlling add member CHA0666
alter role controlling add member CHA0747
alter role controlling add member CHA4601
alter role controlling add member CHA8459
alter role controlling add member CHIGOHE
alter role controlling add member CHK2423
alter role controlling add member CHL4579
alter role controlling add member CHL4628
alter role controlling add member CHL4871
alter role controlling add member CHV2541
alter role controlling add member CHV3755
alter role controlling add member CHV7113
alter role controlling add member CHVWALS
alter role controlling add member CHY0787
alter role controlling add member CHY7358
alter role controlling add member CHZ9690
alter role controlling add member cha8751
alter role controlling add member chl4708
alter role controlling add member walserw
alter role controlling add member zh_compta
alter role controlling add member zh_compta2




