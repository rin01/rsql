print 'start'


--drop database dbic
create database dbic

use [DBIC]


print 'users and roles creating'
create user CHA0666 for login [EZCORP\CHA0666]
create user CHA0747 for login [EZCORP\CHA0747]
create user CHA4601 for login [EZCORP\CHA4601]
create user CHA5690 for login [EZCORP\CHA5690]
create user CHA7068 for login [EZCORP\CHA7068]
create user CHA7074 for login [EZCORP\CHA7074]
create user CHA7077 for login [EZCORP\CHA7077]
create user CHA8459 for login [EZCORP\CHA8459]
create user CHA9690 for login [EZCORP\CHA9690]
create user CHIGOHE for login [EZCORP\CHIGOHE]
create user CHK2024 for login [EZCORP\CHK2024]
create user CHK2423 for login [EZCORP\CHK2423]
create user CHL4579 for login [EZCORP\CHL4579]
create user CHL4628 for login [EZCORP\CHL4628]
create user CHL4636 for login [EZCORP\CHL4636]
create user CHL4649 for login [EZCORP\CHL4649]
create user CHL4689 for login [EZCORP\CHL4689]
create user CHL4711 for login [EZCORP\CHL4711]
create user CHL4713 for login [EZCORP\CHL4713]
create user CHL4749 for login [EZCORP\CHL4749]
create user CHL4780 for login [EZCORP\CHL4780]
create user CHL4794 for login [EZCORP\CHL4794]
create user CHL4806 for login [EZCORP\CHL4806]
create user CHL4815 for login [EZCORP\CHL4815]
create user CHL4819 for login [EZCORP\CHL4819]
create user CHL4871 for login [EZCORP\CHL4871]
create user CHL4873 for login [EZCORP\CHL4873]
create user CHV2541 for login [EZCORP\CHV2541]
create user CHV2716 for login [EZCORP\CHV2716]
create user CHV2721 for login [EZCORP\CHV2721]
create user CHV2752 for login [EZCORP\CHV2752]
create user CHV3755 for login [EZCORP\CHV3755]
create user CHV7113 for login [EZCORP\CHV7113]
create user CHVHUBL for login [EZCORP\CHVHUBL]
create user CHVSPA1 for login [EZCORP\CHVSPA1]
create user CHVWALS for login [EZCORP\CHVWALS]
create user CHY0787 for login [EZCORP\CHY0787]
create user CHY0790 for login [EZCORP\CHY0790]
create user CHY7358 for login [EZCORP\CHY7358]
create user CHZ7202 for login [EZCORP\CHZ7202]
create user CHZ9690 for login [EZCORP\CHZ9690]
create user STATO
create user ZZVC0
create user cha8751 for login [EZCORP\cha8751]
create user chl4708 for login [EZCORP\chl4708]
create user chl4830 for login [EZCORP\chl4830]
create user dlzco
create user dlzdi
create user dlzle
create user dlzmd
create user guest
create user user_alibaba
create user walserw for login [CH\walserw]
create user zh_compta
create user zh_compta2

create role secu_siege
create role secu_agences
create role secu_agents
create role gr_autres
print 'users and roles have been created'


print 'roles membership start'
alter role gr_autres add member STATO
alter role gr_autres add member ZZVC0
alter role gr_autres add member dlzdi
alter role gr_autres add member dlzle
alter role secu_siege add member CHA0666
alter role secu_siege add member CHA0747
alter role secu_siege add member CHA4601
alter role secu_siege add member CHA5690
alter role secu_siege add member CHA7068
alter role secu_siege add member CHA7074
alter role secu_siege add member CHA7077
alter role secu_siege add member CHA8459
alter role secu_siege add member CHA9690
alter role secu_siege add member CHIGOHE
alter role secu_siege add member CHK2024
alter role secu_siege add member CHK2423
alter role secu_siege add member CHL4579
alter role secu_siege add member CHL4628
alter role secu_siege add member CHL4636
alter role secu_siege add member CHL4649
alter role secu_siege add member CHL4689
alter role secu_siege add member CHL4711
alter role secu_siege add member CHL4713
alter role secu_siege add member CHL4749
alter role secu_siege add member CHL4780
alter role secu_siege add member CHL4794
alter role secu_siege add member CHL4806
alter role secu_siege add member CHL4815
alter role secu_siege add member CHL4819
alter role secu_siege add member CHL4871
alter role secu_siege add member CHL4873
alter role secu_siege add member CHV2541
alter role secu_siege add member CHV2716
alter role secu_siege add member CHV2721
alter role secu_siege add member CHV2752
alter role secu_siege add member CHV3755
alter role secu_siege add member CHV7113
alter role secu_siege add member CHVWALS
alter role secu_siege add member CHY0787
alter role secu_siege add member CHY0790
alter role secu_siege add member CHY7358
alter role secu_siege add member CHZ7202
alter role secu_siege add member CHZ9690
alter role secu_siege add member cha8751
alter role secu_siege add member chl4708
alter role secu_siege add member chl4830
alter role secu_siege add member user_alibaba
alter role secu_siege add member walserw
alter role secu_siege add member zh_compta
alter role secu_siege add member zh_compta2
print 'roles membership end'



