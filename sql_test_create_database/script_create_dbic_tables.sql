SET QUOTED_IDENTIFIER ON 
SET ANSI_NULL_DFLT_ON on


use DBIC

print 'start'

CREATE TABLE [dbo].[ADRESSES] (
	[NO_PERSONNE] [int] NOT NULL ,
	[ADRESSE_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRESSE_2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRESSE_3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRESSE_4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRESSE_5] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRESSE_6] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRESSE_7] [varchar] (30) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[AGENCES] (
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_PERSONNE] [int] NOT NULL ,
	[TITRE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM_LONG] [varchar] (50) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRESSE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[COMPLEMENT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[CASE_POSTALE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CODE_ZONE] [tinyint] NULL ,
	[LANGUE_AGENCE] [tinyint] NULL ,
	[CODE_CANTON] [tinyint] NULL ,
	[EMAIL] [varchar] (100) COLLATE fr_cs_as NULL ,
	[CODE_ZONE_ZH] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[AGENTS] (
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[TITRE_CIVIL] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[LIGNE_ADRESSE_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[LIGNE_ADRESSE_2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[CODE_CANTON] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_HIERARCHIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MUTATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_GENRE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_SOUSGENRE] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[AIC_CODIF1] (
	[ID] [char] (10) COLLATE fr_cs_as NOT NULL ,
	[TAB_NAME] [varchar] (50) COLLATE fr_cs_as NULL ,
	[COL_NAME] [varchar] (50) COLLATE fr_cs_as NULL ,
	[COL_DESCR] [varchar] (200) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[AIC_CODIF2] (
	[ID] [char] (10) COLLATE fr_cs_as NOT NULL ,
	[CODE] [char] (15) COLLATE fr_cs_as NOT NULL ,
	[CODE_DESCR] [varchar] (200) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[AIC_SYSTYPES] (
	[xtype] [tinyint] NOT NULL ,
	[name] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[AS400_NATURES_CODES] (
	[NATURE_OPERATION] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[LIBELLE] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[AS400_VIVC] (
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TYPE_POLICE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [int] NULL ,
	[DATE_COMPTABLE] [datetime] NULL ,
	[DATE_VALEUR] [datetime] NULL ,
	[NATURE_OPERATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[TYPE_MONTANT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT] [numeric](12, 2) NULL ,
	[REFERENCE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[CODE_FONDATION] [varchar] (20) COLLATE fr_cs_as NULL ,
	[CODE_CANTON] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[FRACTIONNEMENT] [tinyint] NULL ,
	[DATE_EXERCICE] [datetime] NULL ,
	[NO_COMPTE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LIBELLE_COMPTE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[LIBELLE_ECRITURE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[CODE_MONNAIE_ISO] [varchar] (12) COLLATE fr_cs_as NULL ,
	[ANN_ANT_CRT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FONDATION_DWH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NATURE_PP] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_REASSUREUR] [int] NULL ,
	[CODE_GESTION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[REM] [varchar] (30) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ASSURES_AI_PERSONNES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_ASSURE] [tinyint] NOT NULL ,
	[NOM_ASSURE] [char] (32) COLLATE fr_cs_as NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_SEXE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ASSURES_MALADIE_COLLECTIVE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [smallint] NOT NULL ,
	[NO_PLAN] [smallint] NOT NULL ,
	[DATE_ENTREE] [datetime] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[TYPE_ASSURE] [tinyint] NULL ,
	[SALAIRE_CONVENTIONNEL] [numeric](7, 0) NULL ,
	[DEDUCTION_SALAIRE] [numeric](7, 0) NULL ,
	[INDEMNITE_FIXE] [numeric](7, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ASSURES_VIE_COLLECTIVE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[NO_PERSONNE] [int] NULL ,
	[NOM_ASSURE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[PRENOM_ASSURE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_LANGUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[DATE_DEBUT_ASSURANCE] [datetime] NULL ,
	[DATE_LIMITE_FACTURATION] [datetime] NULL ,
	[DATE_EFFET_MUTATION] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[DATE_SINISTRE] [datetime] NULL ,
	[DATE_DEBUT_LIBERATION] [datetime] NULL ,
	[DATE_DEBUT_VERSEMENT] [datetime] NULL ,
	[DATE_CALCUL] [datetime] NULL ,
	[CODE_CATEGORIE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ETAT_ASSURE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RENTE_EN_COURS] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_RENTE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MISE_EN_GAGE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_DROIT_OPTION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_ACTIVITE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MUTATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_ECHEANCE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_USER] [varchar] (6) COLLATE fr_cs_as NULL ,
	[TAUX_ACTIVITE] [numeric](5, 2) NULL ,
	[TAUX_INVALIDITE] [numeric](5, 2) NULL ,
	[PRIME_EPARGNE] [numeric](12, 2) NULL ,
	[PRIME_RISQUE] [numeric](12, 2) NULL ,
	[PRIME_NIVELEE] [numeric](12, 2) NULL ,
	[PRIME_RENCHERISSEMENT] [numeric](12, 2) NULL ,
	[PRIME_MESURES_SPECIALES] [numeric](12, 2) NULL ,
	[PRIME_FONDS_GARANTIE] [numeric](12, 2) NULL ,
	[PRIME_FRAIS_GESTION] [numeric](12, 2) NULL ,
	[PRIME_COMPL_INTERET] [numeric](12, 2) NULL ,
	[PRIME_COMPL_LONGEVITE] [numeric](12, 2) NULL ,
	[PRIME_INVALIDITE] [numeric](12, 2) NULL ,
	[PRIME_DECES] [numeric](12, 2) NULL ,
	[PRIME_COORDINATION_LAA] [numeric](12, 2) NULL ,
	[SALAIRE_AVS] [numeric](12, 2) NULL ,
	[SALAIRE_LPP] [numeric](12, 2) NULL ,
	[SALAIRE_ASSURE] [numeric](12, 2) NULL ,
	[RESERVE_LPP_31_12] [numeric](12, 2) NULL ,
	[RESERVE_TOTALE_31_12] [numeric](12, 2) NULL ,
	[RESERVE_INVALIDITE] [numeric](12, 2) NULL ,
	[RESERVE_RENTIERS] [numeric](12, 2) NULL ,
	[AVOIR_LPP_EFFET] [numeric](12, 2) NULL ,
	[AVOIR_TOTAL_EFFET] [numeric](12, 2) NULL ,
	[AVOIR_LPP_PROJETE_AVEC_INT] [numeric](12, 2) NULL ,
	[AVOIR_TOTAL_PROJETE_AVEC_INT] [numeric](12, 2) NULL ,
	[PRESTATION_SORTIE_TOTAL_50ANS] [numeric](12, 2) NULL ,
	[PRESTATION_SORTIE_LPP_50ANS] [numeric](12, 2) NULL ,
	[PRESTATION_SORTIE_MARIAGE] [numeric](12, 2) NULL ,
	[CUMUL_VERSEMENT_LEP_LPP] [numeric](12, 2) NULL ,
	[CUMUL_VERSEMENT_LEP_HORS_LPP] [numeric](12, 2) NULL ,
	[ANCIEN_VERSEMENT_LEP] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[BRANCHES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[CENTRE_ACTIVITES] [varchar] (3) COLLATE fr_cs_as NULL ,
	[LIBELLE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[LIBELLE_COURT] [varchar] (20) COLLATE fr_cs_as NULL ,
	[REGROUP] [varchar] (30) COLLATE fr_cs_as NULL ,
	[LIBELLE_D] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CANTONS_ANNUAIRES] (
	[NO_ANNUAIRE] [tinyint] NOT NULL ,
	[CANTON] [varchar] (30) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CAPITAUX_PRODUCTION] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_SEQUENCE] [tinyint] NOT NULL ,
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[MONTANT_CAPITAL] [numeric](12, 2) NULL ,
	[NOM_PRENEUR] [varchar] (15) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CARTE_PROD_13] (
	[NO_POLICE] [int] NOT NULL ,
	[BR] [tinyint] NOT NULL ,
	[AGCE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[AGT1] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[AGT2] [smallint] NOT NULL ,
	[MOIS] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOD] [tinyint] NOT NULL ,
	[DT_EFF] [varchar] (20) COLLATE fr_cs_as NULL ,
	[CREAT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NOM] [varchar] (50) COLLATE fr_cs_as NULL ,
	[EX] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[CANT] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[BASE_COMM] [numeric](9, 0) NULL ,
	[CF] [tinyint] NULL ,
	[CAP_COMM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[DT_COMM] [varchar] (20) COLLATE fr_cs_as NULL ,
	[CC] [tinyint] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CENTRES_COUTS] (
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_CENTRE_COUTS] [varchar] (10) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_CAUSES_SINISTRES] (
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[CAUSE_SINISTRE] [varchar] (70) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_MONNAIES] (
	[CODE_MONNAIE] [tinyint] NOT NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[LIBELLE_COURT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[LIBELLE_LONG] [varchar] (30) COLLATE fr_cs_as NULL ,
	[TAUX] [numeric](10, 9) NULL ,
	[TAUX2] [numeric](10, 9) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_MONNAIES_ISO] (
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[CODE_NUM] [smallint] NOT NULL ,
	[PAYS] [varchar] (20) COLLATE fr_cs_as NULL ,
	[MONNAIE] [varchar] (30) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_NATURES_ECRITURES] (
	[TYPE_POLICE] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[LIBELLE_COURT] [varchar] (255) COLLATE fr_cs_as NULL ,
	[LIBELLE] [varchar] (255) COLLATE fr_cs_as NULL ,
	[NATURE_PREST_DWH] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_NATURES_ECRITURES_TMP] (
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[LIBELLE_COURT] [varchar] (255) COLLATE fr_cs_as NULL ,
	[LIBELLE] [varchar] (255) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_NATURES_PAIEMENTS] (
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NATURE_PAIEMENT] [varchar] (70) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_PAYS] (
	[NO_PAYS_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_PAYS_ISO3] [char] (3) COLLATE fr_cs_as NOT NULL ,
	[C_PAYS_ISO2] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[T_PAYS_ISO] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODES_REGION] (
	[ZONE] [tinyint] NOT NULL ,
	[NOM_ZONE] [varchar] (30) COLLATE fr_cs_as NOT NULL ,
	[NOM_ZONE_D] [varchar] (30) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODIF_AGENTS_GE_ZH] (
	[ID_EMP] [char] (5) COLLATE fr_cs_as NOT NULL ,
	[NO_AGT_ZH] [int] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODIF_BONUS_MALUS] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[CODE_BONUS_MALUS] [tinyint] NOT NULL ,
	[TAUX] [numeric](4, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODIF_MALADIE_COLLECTIVE] (
	[NO_SECTEUR_ECONOMIQUE] [char] (6) COLLATE fr_cs_as NOT NULL ,
	[CLASSE_RISQUE] [smallint] NULL ,
	[GENRE_ENTREPRISE] [tinyint] NULL ,
	[LIBELLE_FRANCAIS] [varchar] (60) COLLATE fr_cs_as NULL ,
	[LIBELLE_ALLEMAND] [varchar] (60) COLLATE fr_cs_as NULL ,
	[LIBELLE_ITALIEN] [varchar] (60) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODIF_NOGA_2002] (
	[C_NOGA] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[C_LANG] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[T_LIB_NOGA] [varchar] (150) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODIF_RISQUES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[LIBELLE_RISQUE] [varchar] (100) COLLATE fr_cs_as NULL ,
	[LOB_RISQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[LIBELLE_SINISTRES] [varchar] (100) COLLATE fr_cs_as NULL ,
	[LOB_SINISTRES] [varchar] (5) COLLATE fr_cs_as NULL ,
	[LIBELLE_STATPROD] [varchar] (100) COLLATE fr_cs_as NULL ,
	[LOB_STATPROD] [varchar] (5) COLLATE fr_cs_as NULL ,
	[REMARQUES] [varchar] (50) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_COMPAGNIE] [int] NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LIBERATION_TAXE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[LOB2] [varchar] (5) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CODIF_RISQUES_CORMIS] (
	[SEQ_NO] [char] (3) COLLATE fr_cs_as NOT NULL ,
	[BUSINESS_SEGMENT] [char] (5) COLLATE fr_cs_as NOT NULL ,
	[LIBELLE] [varchar] (60) COLLATE fr_cs_as NULL ,
	[REMARQUES] [varchar] (60) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COMM_ACQUISITIONS] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_SEQUENCE] [tinyint] NOT NULL ,
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[GENRE_COMMISSION] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[MONTANT_COMMISSION] [numeric](12, 2) NULL ,
	[PRIME_BASE] [numeric](12, 2) NULL ,
	[COMMISSION_A_RECEVOIR_ZH] [numeric](12, 2) NULL ,
	[NOM_PRENEUR] [varchar] (15) COLLATE fr_cs_as NULL ,
	[TAUX_AGT_VI_PU] [numeric](6, 3) NULL ,
	[TAUX_AGT_VI_PP] [numeric](6, 3) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COMM_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[DATE_DEBUT_RISQUE] [datetime] NOT NULL ,
	[CODE_LIAISON_DEBIT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[CODE_NO_DROIT] [smallint] NOT NULL ,
	[CODE_NO_ORDRE_DROIT] [smallint] NOT NULL ,
	[TAUX_AGENT] [smallint] NULL ,
	[DATE_DEBUT_DROIT] [datetime] NULL ,
	[CODE_DUREE_RISQUE] [smallint] NULL ,
	[DATE_FIN_DROIT] [datetime] NULL ,
	[MONTANT_CAPITAL_COM] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COMPTES_OUVERTS] (
	[DATE_EXTRAC] [datetime] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[DATE_VALEUR] [datetime] NOT NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[R_POL] [int] NULL ,
	[C_TYP_REF] [char] (4) COLLATE fr_cs_as NULL ,
	[M_MNT] [numeric](12, 2) NULL ,
	[M_NONRGL] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[CONVERSION_ACTIVITE] (
	[SECT_75] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[SECT_85] [varchar] (4) COLLATE fr_cs_as NULL ,
	[AUX] [varchar] (4) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COURS_MONNAIES] (
	[MOIS_CHANGE] [tinyint] NOT NULL ,
	[ANNEE_CHANGE] [smallint] NOT NULL ,
	[CODE_MONNAIE] [tinyint] NOT NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[LIBELLE_COURT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[LIBELLE_LONG] [varchar] (30) COLLATE fr_cs_as NULL ,
	[TAUX] [numeric](10, 9) NULL ,
	[TAUX2] [numeric](10, 9) NULL ,
	[TAUX_BILAN] [numeric](10, 9) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COUV_MALADIE_COLLECTIVE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_PLAN] [smallint] NOT NULL ,
	[NO_COUCHE] [tinyint] NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[DELAI_ATTENTE] [smallint] NOT NULL ,
	[GENRE_DELAI_ATTENTE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[PERIODE_PRESTATION] [smallint] NULL ,
	[DUREE_PRESTATION] [smallint] NULL ,
	[TAUX_INDEMNITE] [smallint] NULL ,
	[MONTANT_PRESTATION] [numeric](9, 0) NULL ,
	[CLASSE_RISQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ETENDUE_TAUX_HOMME] [tinyint] NULL ,
	[TAUX_BASE_HOMME] [numeric](5, 2) NULL ,
	[MONTANT_PRIME_FIXE_HOMME_10] [numeric](9, 2) NULL ,
	[ETENDUE_TAUX_FEMME] [tinyint] NULL ,
	[TAUX_BASE_FEMME] [numeric](5, 2) NULL ,
	[MONTANT_PRIME_FIXE_FEMME_10] [numeric](9, 2) NULL ,
	[ETENDUE_TAUX_GLOBAL] [tinyint] NULL ,
	[TAUX_BASE_GLOBAL] [numeric](5, 2) NULL ,
	[MONTANT_PRIME_FIXE_GLOBAL_10] [numeric](9, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COUV_MALADIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[GENRE_COUVERTURE] [smallint] NOT NULL ,
	[NO_COUVERTURE] [tinyint] NOT NULL ,
	[AGE_ENTREE] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[DELAI_ATTENTE] [smallint] NULL ,
	[INDEMNITE_VERSEE] [numeric](12, 2) NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[AGE_TERME] [tinyint] NULL ,
	[EDITION_TARIFAIRE] [smallint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[MONTANT_PRIME_BASE] [numeric](12, 2) NULL ,
	[MONTANT_ACC_NORMAL] [numeric](12, 2) NULL ,
	[MONTANT_ACC_HORS_CLASSE] [numeric](12, 2) NULL ,
	[MONTANT_SUP_AGGRAVATION] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_VEIL] [numeric](12, 2) NULL ,
	[MONTANT_CURE] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_FAMILIAL] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_SPECIAL] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_TOTALE] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COUV_MALADIE_INDIVIDUELLE_200704] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[GENRE_COUVERTURE] [smallint] NOT NULL ,
	[NO_COUVERTURE] [tinyint] NOT NULL ,
	[AGE_ENTREE] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[DELAI_ATTENTE] [smallint] NULL ,
	[INDEMNITE_VERSEE] [numeric](12, 2) NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[AGE_TERME] [tinyint] NULL ,
	[EDITION_TARIFAIRE] [smallint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[MONTANT_PRIME_BASE] [numeric](12, 2) NULL ,
	[MONTANT_ACC_NORMAL] [numeric](12, 2) NULL ,
	[MONTANT_ACC_HORS_CLASSE] [numeric](12, 2) NULL ,
	[MONTANT_SUP_AGGRAVATION] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_VEIL] [numeric](12, 2) NULL ,
	[MONTANT_CURE] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_FAMILIAL] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_SPECIAL] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_TOTALE] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[COUV_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[DATE_INTERVENTION] [datetime] NULL ,
	[NO_MOUVEMENT_CATAL] [smallint] NULL ,
	[DATE_DEBUT_ETAT] [datetime] NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [int] NULL ,
	[CODE_NATURE_COUVERTURE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RISQUE_COUVERTURE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_CLASSE_RISQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DELAI_EXONERATION] [smallint] NULL ,
	[DELAI_ATTENTE] [smallint] NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[DATE_FIN_TECHNIQUE] [datetime] NULL ,
	[DATE_FIN_PRIME] [datetime] NULL ,
	[CODE_REASSURANCE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RABAIS_SURPRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RABAIS_SOMME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_SITUATION_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FISCAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MODE_FRACT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MODE_PARTICIPATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MONTANT_TECHNIQUE_COUV] [numeric](12, 2) NULL ,
	[MONTANT_COUV_ACTUEL] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_TARIF] [numeric](12, 2) NULL ,
	[MONTANT_PU_SPECIALE] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_SOMME] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_MAJORATION] [numeric](12, 2) NULL ,
	[MONTANT_RABAIS_SPECIAL] [numeric](12, 2) NULL ,
	[MONTANT_SURPRIME_PER] [numeric](12, 2) NULL ,
	[MONTANT_SURPRIME_UNI] [numeric](12, 2) NULL ,
	[NO_ASSURE_1] [tinyint] NULL ,
	[CODE_SEXE_1] [tinyint] NULL ,
	[DATE_NAISSANCE_1] [datetime] NULL ,
	[CODE_RISQUE_AGGRAVE_1] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_ASSURE_2] [tinyint] NULL ,
	[CODE_SEXE_2] [tinyint] NULL ,
	[DATE_NAISSANCE_2] [datetime] NULL ,
	[CODE_RISQUE_AGGRAVE_2] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_ASSURE_3] [tinyint] NULL ,
	[CODE_SEXE_3] [tinyint] NULL ,
	[DATE_NAISSANCE_3] [datetime] NULL ,
	[CODE_RISQUE_AGGRAVE_3] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[DEBITS_VIE_INDIVIDUELLE] (
	[CODE_MONNAIE] [tinyint] NOT NULL ,
	[DATE_COMPTABLE] [smallint] NOT NULL ,
	[CODE_BRANCHE] [tinyint] NOT NULL ,
	[EDITION_TARIFAIRE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[DEBIT_PERIODIQUE] [numeric](12, 2) NULL ,
	[DEBIT_UNIQUE] [numeric](12, 2) NULL ,
	[DEBIT_PARTICIPATION] [numeric](12, 2) NULL ,
	[DEBIT_TOTAL] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[EIS_STATPRODGROUP] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[STATPRODGROUP] [varchar] (50) COLLATE fr_cs_as NULL ,
	[STATPRODGROUP_LIBELLE] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[EIS_TABLEAU_BORD_ENT] (
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[BRANCHE] [varchar] (12) COLLATE fr_cs_as NOT NULL ,
	[ORDRE] [tinyint] NULL ,
	[INIT_NBPOL] [smallint] NULL ,
	[INIT_PRIMES] [numeric](12, 2) NULL ,
	[NEW_NBPOL_1] [smallint] NULL ,
	[NEW_PRIMES_1] [numeric](12, 2) NULL ,
	[NEW_NBPOL_2] [smallint] NULL ,
	[NEW_PRIMES_2] [numeric](12, 2) NULL ,
	[MUT_NBPOL] [smallint] NULL ,
	[MUT_PRIMES] [numeric](12, 2) NULL ,
	[ANN_NBPOL] [smallint] NULL ,
	[ANN_PRIMES] [numeric](12, 2) NULL ,
	[FINPER_NBPOL] [smallint] NULL ,
	[FINPER_PRIMES] [numeric](12, 2) NULL ,
	[REN_INIT_NBPOL] [numeric](12, 2) NULL ,
	[REN_INIT_PRIMES] [numeric](12, 2) NULL ,
	[RENOUV_NBPOL] [numeric](12, 2) NULL ,
	[RENOUV_PRIMES] [numeric](12, 2) NULL ,
	[SIN_PRIMES] [numeric](12, 2) NULL ,
	[SIN_CHARGES] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ENTREPRISES] (
	[NO_ANNUAIRE] [tinyint] NULL ,
	[NOM] [varchar] (64) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (25) COLLATE fr_cs_as NULL ,
	[NOM_JEUNE_FILLE] [varchar] (25) COLLATE fr_cs_as NULL ,
	[PROFESSION] [varchar] (50) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (33) COLLATE fr_cs_as NULL ,
	[NUMERO_RUE] [varchar] (7) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[FAX] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (4) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (25) COLLATE fr_cs_as NULL ,
	[BRANCHE] [varchar] (70) COLLATE fr_cs_as NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_ETAT_CIVIL] [tinyint] NULL ,
	[CODE_NOUVEAU] [tinyint] NULL ,
	[CASE_POSTALE] [varchar] (33) COLLATE fr_cs_as NULL ,
	[NO_POSTAL_CASE_POSTALE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[LOCALITE_CASE_POSTALE] [varchar] (25) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[GARANTIES_VEHICULE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[TYPE_OBJET] [tinyint] NOT NULL ,
	[NO_OBJET] [tinyint] NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[NO_GARANTIE] [tinyint] NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[STATUT_GARANTIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TARIF_1] [smallint] NULL ,
	[CODE_TARIF_2] [smallint] NULL ,
	[CODE_VALEUR_A] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_B] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_GARANTIE] [int] NULL ,
	[CODE_TYPE_FRANCHISE] [tinyint] NULL ,
	[MONTANT_FRANCHISE] [smallint] NULL ,
	[TAUX_FRANCHISE] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[GARANTIES_VEHICULE_TMP] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[TYPE_OBJET] [tinyint] NOT NULL ,
	[NO_OBJET] [tinyint] NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[NO_GARANTIE] [tinyint] NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[STATUT_GARANTIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TARIF_1] [smallint] NULL ,
	[CODE_TARIF_2] [smallint] NULL ,
	[CODE_VALEUR_A] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_B] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_GARANTIE] [int] NULL ,
	[CODE_TYPE_FRANCHISE] [tinyint] NULL ,
	[MONTANT_FRANCHISE] [smallint] NULL ,
	[TAUX_FRANCHISE] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[GENEVOISE_VC04_DECODIF] (
	[NO_POLICE_GE] [int] NOT NULL ,
	[NO_POLICE] [int] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[GENEVOISE_VC04_RESERVES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [int] NULL ,
	[NO_ASSURE] [smallint] NULL ,
	[MONTANT_RESERVE] [numeric](9, 2) NULL ,
	[REPORT_PRIME] [numeric](9, 2) NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[NO_POLICE_GE] [int] NULL ,
	[NO_ASSURE_GE] [smallint] NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[CODE_TARIF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[BASE_TECHNIQUE] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HELP_LISTE_COLONNES] (
	[TABLE_NOM] [varchar] (100) COLLATE fr_cs_as NOT NULL ,
	[CLE_PRIMAIRE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[COLONNE_NOM] [varchar] (100) COLLATE fr_cs_as NOT NULL ,
	[NO_ORDRE] [smallint] NULL ,
	[TYPE_DATA] [varchar] (30) COLLATE fr_cs_as NULL ,
	[LONGUEUR] [smallint] NULL ,
	[TAILLE_VARIABLE] [smallint] NULL ,
	[NULL_ALLOWED] [smallint] NULL ,
	[AUTO_INCREMENT] [smallint] NULL ,
	[DESCRIPTION] [varchar] (255) COLLATE fr_cs_as NULL ,
	[LINK_TO] [varchar] (255) COLLATE fr_cs_as NULL ,
	[STATUS] [varchar] (3) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HELP_LISTE_COLVALEURS] (
	[GROUPE_ID] [varchar] (25) COLLATE fr_cs_as NOT NULL ,
	[VALEUR] [varchar] (100) COLLATE fr_cs_as NOT NULL ,
	[LIBELLE] [varchar] (255) COLLATE fr_cs_as NULL ,
	[NO_ORDRE] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HIST_COMM_ACQUIS_IARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_COMMISSION] [int] IDENTITY (1, 1) NOT NULL ,
	[DATE_TRAITEMENT] [datetime] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[CODE_GROUPE_1] [tinyint] NULL ,
	[CODE_GROUPE_2] [tinyint] NULL ,
	[CODE_GROUPE_3] [tinyint] NULL ,
	[CODE_GROUPE_4] [tinyint] NULL ,
	[CODE_GROUPE_5] [tinyint] NULL ,
	[CODE_GROUPE_6] [tinyint] NULL ,
	[MONTANT_COMMISSION] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HIST_INVENTAIRE_VI03] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [smallint] NOT NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[STATUT] [tinyint] NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_PRIME_FRACT] [tinyint] NULL ,
	[DUREE_PRIME] [smallint] NULL ,
	[CODE_RENTE_FRACT] [tinyint] NULL ,
	[DUREE_RENTE] [smallint] NULL ,
	[GARANTIE_RENTE] [smallint] NULL ,
	[PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[MONTANT_RENTE] [numeric](12, 2) NULL ,
	[PRIME_RESERVE] [numeric](12, 2) NULL ,
	[RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[RESERVE_FIN] [numeric](12, 2) NULL ,
	[RESERVE_INT] [numeric](12, 2) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[NB] [tinyint] NULL ,
	[SEX] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HIST_INV_TRIM_GENLOC] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[DATE_FIN_TECHNIQUE] [datetime] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[COUVERTURE_DECES] [numeric](12, 2) NULL ,
	[COUVERTURE_VIE] [numeric](12, 2) NULL ,
	[NB_ACTIONS] [int] NULL ,
	[RESERVE_FRAIS_DEBUT] [numeric](9, 2) NULL ,
	[RESERVE_FRAIS_FIN] [numeric](9, 2) NULL ,
	[RESERVE_FRAIS_INT] [numeric](9, 2) NULL ,
	[RESERVE_RISQUE_DEBUT] [numeric](9, 2) NULL ,
	[RESERVE_RISQUE_FIN] [numeric](9, 2) NULL ,
	[RESERVE_RISQUE_INT] [numeric](9, 2) NULL ,
	[VALEUR_ACTIONS] [numeric](12, 2) NULL ,
	[COTATION_INVENTAIRE] [numeric](12, 4) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HIST_LIAISONS] (
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[STATUT_LIEN] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PRENEUR] [int] NOT NULL ,
	[NO_PAYEUR] [int] NOT NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[PRIME_PERIODIQUE] [numeric](12, 2) NULL ,
	[PRIMES_UNIQUES] [numeric](12, 2) NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[SEGMENT_MARCHE] [tinyint] NULL ,
	[GENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ZONE] [tinyint] NULL ,
	[PP_CHF] [numeric](12, 2) NULL ,
	[PU_CHF] [numeric](12, 2) NULL ,
	[NEW] [numeric](12, 2) NULL ,
	[MODIF] [numeric](12, 2) NULL ,
	[ANNUL] [numeric](12, 2) NULL ,
	[S_NEW] [numeric](12, 2) NULL ,
	[S_MODIF] [numeric](12, 2) NULL ,
	[S_MODIF2] [numeric](12, 2) NULL ,
	[S_ANNUL] [numeric](12, 2) NULL ,
	[SOUS_TYPE] [varchar] (6) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HIST_PAIEMENTS_MALADIE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[GENRE_COUVERTURE] [tinyint] NOT NULL ,
	[NO_COUVERTURE] [tinyint] NOT NULL ,
	[NO_SIN] [tinyint] NOT NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[CODE_LIQUIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[COLL_INDIV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_MOUVMT] [tinyint] NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[SEXE] [tinyint] NULL ,
	[GR_TARIF] [tinyint] NULL ,
	[CODE_TARIF] [tinyint] NULL ,
	[ANNEE_TARIF] [tinyint] NULL ,
	[C_CGA_AA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_AGE_ACT] [tinyint] NULL ,
	[AGE_ENTREE] [tinyint] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[C_AGENT] [smallint] NULL ,
	[DELAI_ATTENTE] [smallint] NULL ,
	[CODE_FRANCHISE] [smallint] NULL ,
	[CODE_AGGRAVATION] [tinyint] NULL ,
	[CODE_ACCOUCHEMENT] [tinyint] NULL ,
	[CODE_ACCIDENT] [tinyint] NULL ,
	[CODE_CURE] [tinyint] NULL ,
	[DELAI_ATTENTE_2] [smallint] NULL ,
	[Z_TX_INCAP] [smallint] NULL ,
	[C_TRANS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MONTANT_SINISTRE] [numeric](12, 0) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 0) NULL ,
	[MONTANT_FRAIS_CHARGE] [numeric](12, 0) NULL ,
	[MONTANT_FRAIS_RECUP] [numeric](12, 0) NULL ,
	[MONTANT_PRIME_RECUP] [numeric](12, 0) NULL ,
	[MONTANT_QPART_INTRAS] [numeric](12, 0) NULL ,
	[MONTANT_RESERVE_MATH] [numeric](12, 0) NULL ,
	[N_DECOMPTES] [varchar] (10) COLLATE fr_cs_as NULL ,
	[N_CHAMP_9] [varchar] (10) COLLATE fr_cs_as NULL ,
	[DATE_DEBUT_PRESTATION] [datetime] NULL ,
	[DATE_EFFET_COUVERTURE] [datetime] NULL ,
	[DATE_LIQUIDATION] [datetime] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[NO_ASSURE] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HIST_PE_1995] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_DEBIT] [int] IDENTITY (1, 1) NOT NULL ,
	[PRIMES_EMISES] [numeric](12, 2) NULL ,
	[PRIMES_UNIQUES] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[HIST_PE_1995_DETAILS] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[CODE_SITUATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[BRANCHE] [tinyint] NULL ,
	[SEQUENCE] [tinyint] NULL ,
	[CODE_ANNULATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_QCQT] [tinyint] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MONTANT_PRIME] [numeric](9, 2) NULL ,
	[MONTANT_BONUS_MALUS] [numeric](9, 2) NULL ,
	[MONTANT_FRAIS] [numeric](9, 2) NULL ,
	[MONTANT_TAXES] [numeric](9, 2) NULL ,
	[TOTAL_PRIME] [numeric](9, 2) NULL ,
	[DATE_MUTATION] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[IC_USERS] (
	[ID_EMP] [char] (5) COLLATE fr_cs_as NOT NULL ,
	[ID_AGC] [tinyint] NOT NULL ,
	[ID_AGT] [smallint] NOT NULL ,
	[COMPART] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[IC_USERS_MODIF] (
	[ID_EMP] [char] (5) COLLATE fr_cs_as NOT NULL ,
	[COMPART] [tinyint] NULL ,
	[MODIF] [varchar] (10) COLLATE fr_cs_as NULL ,
	[COMPART_OLD] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INFO_GENFLEX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_MOUVEMENT_CATALOGUE] [smallint] NOT NULL ,
	[DATE_CATALOGUE] [datetime] NULL ,
	[DATE_DEBUT_ETAT] [datetime] NULL ,
	[CODE_MASQUE] [tinyint] NULL ,
	[TYPE_EVOLUTION_PRIME] [tinyint] NULL ,
	[DUREE_FINANCEMENT_EPARGNE] [smallint] NULL ,
	[PRIME_EPARGNE_SUIVANTE] [numeric](12, 2) NULL ,
	[PRIME_BASE_COMMISSION] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INVENTAIRE_MENSUEL_IARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR_COUCHE] [tinyint] NOT NULL ,
	[NO_INVENTAIRE] [int] IDENTITY (1, 1) NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_SITUATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[ANNEE_CATALOGUE] [smallint] NULL ,
	[MOIS_CATALOGUE] [tinyint] NULL ,
	[CODE_GROUPE_1] [tinyint] NULL ,
	[CODE_GROUPE_2] [tinyint] NULL ,
	[CODE_GROUPE_3] [tinyint] NULL ,
	[CODE_GROUPE_4] [tinyint] NULL ,
	[CODE_GROUPE_5] [tinyint] NULL ,
	[CODE_GROUPE_6] [tinyint] NULL ,
	[MOUVEMENT_PRIME] [numeric](12, 2) NULL ,
	[FILLER1] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INVENTAIRE_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_TARIF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MODE_PARTICIPATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MODE_FRACT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[DATE_FIN_PRIME] [datetime] NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_COUV_DECES] [numeric](12, 2) NULL ,
	[MONTANT_COUV_VIE] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_TARIF] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_RESERVE] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_FIN] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_INT] [numeric](12, 2) NULL ,
	[MONTANT_REPORT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INV_TRIM_VI03] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[STATUT] [tinyint] NULL ,
	[MODE_PARTICIPATION] [tinyint] NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_PRIME_FRACT] [tinyint] NULL ,
	[DUREE_PRIME] [smallint] NULL ,
	[CODE_RENTE_FRACT] [tinyint] NULL ,
	[DUREE_RENTE] [smallint] NULL ,
	[GARANTIE_RENTE] [tinyint] NULL ,
	[PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[MONTANT_RENTE] [numeric](12, 2) NULL ,
	[PRIME_RESERVE] [numeric](12, 2) NULL ,
	[RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[RESERVE_FIN] [numeric](12, 2) NULL ,
	[RESERVE_INT] [numeric](12, 2) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[CODE_MONNAIE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INV_TRIM_VI03_200506] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[STATUT] [tinyint] NULL ,
	[MODE_PARTICIPATION] [tinyint] NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_PRIME_FRACT] [tinyint] NULL ,
	[DUREE_PRIME] [smallint] NULL ,
	[CODE_RENTE_FRACT] [tinyint] NULL ,
	[DUREE_RENTE] [smallint] NULL ,
	[GARANTIE_RENTE] [tinyint] NULL ,
	[PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[MONTANT_RENTE] [numeric](12, 2) NULL ,
	[PRIME_RESERVE] [numeric](12, 2) NULL ,
	[RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[RESERVE_FIN] [numeric](12, 2) NULL ,
	[RESERVE_INT] [numeric](12, 2) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[CODE_MONNAIE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INV_TRIM_VI03_RENF] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[STATUT] [tinyint] NULL ,
	[MODE_PARTICIPATION] [tinyint] NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_PRIME_FRACT] [tinyint] NULL ,
	[DUREE_PRIME] [smallint] NULL ,
	[CODE_RENTE_FRACT] [tinyint] NULL ,
	[DUREE_RENTE] [smallint] NULL ,
	[GARANTIE_RENTE] [tinyint] NULL ,
	[PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[MONTANT_RENTE] [numeric](12, 2) NULL ,
	[PRIME_RESERVE] [numeric](12, 2) NULL ,
	[RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[RESERVE_FIN] [numeric](12, 2) NULL ,
	[RESERVE_INT] [numeric](12, 2) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[CODE_MONNAIE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INV_TRIM_VI07] (
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_DEBUT_TECHNIQUE] [char] (7) COLLATE fr_cs_as NOT NULL ,
	[DATE_FIN_TECHNIQUE] [char] (7) COLLATE fr_cs_as NOT NULL ,
	[AGE] [tinyint] NOT NULL ,
	[CODE_SEXE] [tinyint] NOT NULL ,
	[COUVERTURE_DECES] [numeric](9, 0) NULL ,
	[RESERVE_RISQUE_DEBUT] [numeric](9, 0) NULL ,
	[RESERVE_RISQUE_FIN] [numeric](9, 0) NULL ,
	[RESERVE_RISQUE_INT] [numeric](9, 0) NULL ,
	[RESERVE_FRAIS_DEBUT] [numeric](9, 0) NULL ,
	[RESERVE_FRAIS_FIN] [numeric](9, 0) NULL ,
	[RESERVE_FRAIS_INT] [numeric](9, 0) NULL ,
	[PRIME_EPARGNE] [numeric](9, 0) NULL ,
	[RESERVE_EPARGNE] [numeric](9, 0) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INV_TRIM_VIE_INDIVIDUELLE] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_TARIF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MODE_PARTICIPATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MODE_FRACT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[DATE_FIN_PRIME] [datetime] NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_COUV_DECES] [numeric](12, 2) NULL ,
	[MONTANT_COUV_VIE] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_TARIF] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_RESERVE] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_FIN] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_INT] [numeric](12, 2) NULL ,
	[MONTANT_REPORT] [numeric](12, 2) NULL ,
	[NO_TRIMESTRE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[INV_TRIM_VIRAC] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [smallint] NULL ,
	[VALEUR_RACHAT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[LATTMAN2] (
	[D_TRAIT] [datetime] NOT NULL ,
	[ADRE1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_PAYS] [varchar] (3) COLLATE fr_cs_as NULL ,
	[LIB_PAYS] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_INSPECT] [int] NULL ,
	[LANGUE] [tinyint] NULL ,
	[BRANCHE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[T_ETAT] [char] (1) COLLATE fr_cs_as NULL ,
	[M_SOLDE_CPT_PRIME] [numeric](12, 2) NULL ,
	[NOM_ASSURE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[PRENOM_ASSURE] [varchar] (18) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[LATTMANN] (
	[D_TRAIT] [datetime] NOT NULL ,
	[ADRE1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_PAYS] [varchar] (3) COLLATE fr_cs_as NULL ,
	[LIB_PAYS] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_INSPECT] [int] NULL ,
	[LANGUE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[BRANCHE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[D_EFFET] [datetime] NULL ,
	[D_FIN] [datetime] NULL ,
	[M_COUV] [numeric](12, 2) NULL ,
	[M_PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[M_PRIME_PERIODIQUE] [numeric](12, 2) NULL ,
	[M_RACHAT] [numeric](12, 2) NULL ,
	[M_DECES] [numeric](12, 2) NULL ,
	[DUREE_DIFFERE] [tinyint] NULL ,
	[STAT_RENTE] [tinyint] NULL ,
	[D_FIRST_RENTE] [datetime] NULL ,
	[M_PRET] [numeric](12, 2) NULL ,
	[Z_TAUX_PRET] [numeric](12, 6) NULL ,
	[M_INTERET] [numeric](12, 2) NULL ,
	[NOM_ASSURE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[PRENOM_ASSURE] [varchar] (18) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[LIAISONS] (
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[STATUT_LIEN] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PRENEUR] [int] NOT NULL ,
	[NO_PAYEUR] [int] NOT NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[PRIME_PERIODIQUE] [numeric](12, 2) NULL ,
	[PRIMES_UNIQUES] [numeric](12, 2) NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[SEGMENT_MARCHE] [tinyint] NULL ,
	[GENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ZONE] [tinyint] NULL ,
	[PP_CHF] [numeric](12, 2) NULL ,
	[PU_CHF] [numeric](12, 2) NULL ,
	[NEW] [numeric](12, 2) NULL ,
	[MODIF] [numeric](12, 2) NULL ,
	[ANNUL] [numeric](12, 2) NULL ,
	[S_NEW] [numeric](12, 2) NULL ,
	[S_MODIF] [numeric](12, 2) NULL ,
	[S_MODIF2] [numeric](12, 2) NULL ,
	[S_ANNUL] [numeric](12, 2) NULL ,
	[SOUS_TYPE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[MODE_PMT] [tinyint] NULL ,
	[TAUX_COASS_MAX] [numeric](7, 2) NULL ,
	[PRIME_EPARGNE_POOL] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[LIAISONS_SEGMENT] (
	[NO_PERSONNE] [int] NOT NULL ,
	[SEGMENT] [tinyint] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[LOCALITE_COMMUNE] (
	[NO_POSTAL] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[LOCALITE_COMPLETE] [varchar] (39) COLLATE fr_cs_as NOT NULL ,
	[LOCALITE_ABREGEE] [varchar] (18) COLLATE fr_cs_as NULL ,
	[NO_COMMUNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NOM_COMMUNE] [varchar] (18) COLLATE fr_cs_as NULL ,
	[CANTON] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MC06_ASSURES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[DATE_CREATION_ASS_SIN] [datetime] NOT NULL ,
	[DATE_ANN_ASS_SIN] [datetime] NOT NULL ,
	[CODE_PAIEMENT_SINISTRE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[DATE_DEBUT_EMPL] [datetime] NOT NULL ,
	[DATE_DEBUT_SAL] [datetime] NOT NULL ,
	[DATE_FIN_SAL] [datetime] NOT NULL ,
	[MONTANT_SAL] [numeric](12, 2) NOT NULL ,
	[MONTANT_SAL_ANNUEL] [numeric](12, 2) NOT NULL ,
	[MONTANT_SAL_MENSUEL] [numeric](9, 2) NOT NULL ,
	[MONTANT_SAL_SUPPL] [numeric](9, 2) NOT NULL ,
	[N_H_JOUR] [numeric](5, 2) NOT NULL ,
	[N_H_SEM] [numeric](5, 2) NOT NULL ,
	[N_H_JOUR_SEM] [numeric](5, 2) NOT NULL ,
	[N_H_JOUR_MOIS] [numeric](5, 2) NOT NULL ,
	[MONTANT_SAL_H] [numeric](6, 2) NOT NULL ,
	[CODE_GNR_SAL] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MC06_FRAIS] (
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_PAIEMENT_SINISTRE] [int] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[NO_PRST_SIN] [int] NOT NULL ,
	[DATE_PMNT_SIN] [datetime] NOT NULL ,
	[MONTANT_PMNT_SIN] [numeric](12, 2) NOT NULL ,
	[CODE_MODE_PMNT_SIN] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[VISA_PMNT_SIN] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[CODE_NAT_PMNT_SIN] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[CODE_ENV_PMNT_SIN] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[DATE_PMNT_PRST] [datetime] NOT NULL ,
	[CODE_ENV_PRST] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_SINISTRE] [int] NOT NULL ,
	[CODE_GNR_FRAIS] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[DATE_FACT_FRAIS] [datetime] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MC06_PRESTATIONS] (
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_PAIEMENT_SINISTRE] [int] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[NO_PRST_SIN] [int] NOT NULL ,
	[DATE_PMNT_SIN] [datetime] NOT NULL ,
	[MONTANT_PMNT_SIN] [numeric](12, 2) NOT NULL ,
	[CODE_MODE_PMNT_SIN] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[VISA_PMNT_SIN] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[CODE_NAT_PMNT_SIN] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[CODE_ENV_PMNT_SIN] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[DATE_PMNT_PRST] [datetime] NOT NULL ,
	[CODE_ENV_PRST] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_SINISTRE] [int] NOT NULL ,
	[NO_RES] [int] NOT NULL ,
	[CODE_GNR_PRST_RSQ] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[DATE_DEB_DRT_PRST] [datetime] NOT NULL ,
	[DATE_FIN_DRT_PRST] [datetime] NOT NULL ,
	[N_J_INDM] [int] NULL ,
	[N_J_INDM_VIR] [numeric](6, 2) NULL ,
	[N_J_ATT] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MC06_SINISTRES] (
	[NO_SINISTRE] [int] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_PLAN] [smallint] NOT NULL ,
	[NO_COUCHE] [tinyint] NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[N_DELAI_ATTENTE] [smallint] NOT NULL ,
	[NO_AVNT_MC] [int] NOT NULL ,
	[CODE_GNR_DELAI] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_PER_PRST] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DUREE_PER_PRST] [smallint] NULL ,
	[MONTANT_PRST_ANN] [numeric](12, 2) NULL ,
	[Z_PRST_INV] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_FRANCHISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_FRANCHISE] [int] NULL ,
	[MONTANT_MAX_PRST] [int] NULL ,
	[CODE_SUPR_DEL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_GNR_RISQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_CLS_RISQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SAL_CNVL_ANN] [numeric](12, 2) NULL ,
	[DATE_ANN_ASS_SIN] [datetime] NULL ,
	[DATE_ANN] [datetime] NULL ,
	[ANN_CGA_COUV] [smallint] NULL ,
	[ANN_TAR_COUV] [smallint] NULL ,
	[AGE_LIM_HOMME] [tinyint] NULL ,
	[AGE_LIM_FEMME] [tinyint] NULL ,
	[CODE_GNR_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_NAT_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_NAT_CTRA] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_ENT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_RES] [int] NULL ,
	[DATE_RES] [datetime] NULL ,
	[MONTANT_RES] [numeric](12, 2) NULL ,
	[NO_ASSURE] [int] NULL ,
	[NO_GRP_SIN] [int] NULL ,
	[DATE_DEBUT_SIN] [datetime] NULL ,
	[DATE_FIN_SIN] [datetime] NULL ,
	[N_J_LP] [smallint] NULL ,
	[N_J_PRLG_COUV] [smallint] NULL ,
	[CODE_DGNS] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CODE_EVNT] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MI05_SAP_PREST] (
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_SINISTRE] [tinyint] NOT NULL ,
	[COUCHE] [tinyint] NOT NULL ,
	[EDITION] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[MW] [varchar] (1) COLLATE fr_cs_as NULL ,
	[KMU] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ANNEE_DEBUT] [smallint] NULL ,
	[NO_ECRITURE] [int] NOT NULL ,
	[DATE_COMPTABLE] [datetime] NOT NULL ,
	[BS] [tinyint] NOT NULL ,
	[CODE_MONNAIE_ISO] [char] (3) COLLATE fr_cs_as NOT NULL ,
	[MONTANT] [numeric](12, 2) NOT NULL ,
	[COMMENTAIRE] [varchar] (200) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MVT_CATAL_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[TYPE_CAPITAL] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[DATE_CATALOGUE] [datetime] NOT NULL ,
	[CODE_MUTATION] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NOMBRE_POLICES] [smallint] NULL ,
	[MVT_COUVERTURE] [numeric](12, 2) NULL ,
	[MVT_PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[MVT_PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[MVT_RESERVE] [numeric](12, 2) NULL ,
	[MVT_TRANSFERT] [numeric](12, 2) NULL ,
	[MVT_PRESTATION] [numeric](12, 2) NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[CODE_MONNAIE] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MVT_EPARGNE_VIE_IND] (
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[MUTATION] [varchar] (20) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [smallint] NULL ,
	[DATE_CATALOGUE] [datetime] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[MONTANT_SOLDE_INITIAL] [numeric](12, 2) NULL ,
	[MVT_CAPITALISATION] [numeric](12, 2) NULL ,
	[MVT_INTERETS] [numeric](12, 2) NULL ,
	[MVT_TOTAL] [numeric](12, 2) NULL ,
	[MONTANT_SOLDE_FINAL] [numeric](12, 2) NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[MZ_RENTES_MIGREES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_SINISTRE] [char] (19) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[NOUVELLES_AFFAIRES_VC04] (
	[ANNEE_REF] [smallint] NOT NULL ,
	[MOIS_REF] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[PA_EP] [numeric](12, 2) NULL ,
	[PA_RF] [numeric](12, 2) NULL ,
	[PU_GENEVOISE] [numeric](12, 2) NULL ,
	[PU_PROGRESSA] [numeric](12, 2) NULL ,
	[CODE_TYPE_CONTRAT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[TYPE_CONTRAT_2] [varchar] (1) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[OBJETS_VEHICULE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[TYPE_OBJET] [tinyint] NOT NULL ,
	[NO_OBJET] [tinyint] NOT NULL ,
	[STATUT_OBJET] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_MARQUE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[TEXTE_MARQUE] [varchar] (25) COLLATE fr_cs_as NULL ,
	[CODE_GENRE_VEHICULE] [tinyint] NULL ,
	[CODE_TYPE_VEHICULE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[DATE_IMMATRICULATION] [datetime] NULL ,
	[PRIX_VEHICULE] [int] NULL ,
	[CANTON_IMMATRICULATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_IMMATRICULATION] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CYLINDREE] [varchar] (10) COLLATE fr_cs_as NULL ,
	[PUISSANCE] [numeric](7, 2) NULL ,
	[POIDS] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[OBJETS_VEHICULE_TMP] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[TYPE_OBJET] [tinyint] NOT NULL ,
	[NO_OBJET] [tinyint] NOT NULL ,
	[STATUT_OBJET] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_MARQUE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[TEXTE_MARQUE] [varchar] (25) COLLATE fr_cs_as NULL ,
	[CODE_GENRE_VEHICULE] [tinyint] NULL ,
	[CODE_TYPE_VEHICULE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[DATE_IMMATRICULATION] [datetime] NULL ,
	[PRIX_VEHICULE] [int] NULL ,
	[CANTON_IMMATRICULATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_IMMATRICULATION] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CYLINDREE] [varchar] (10) COLLATE fr_cs_as NULL ,
	[PUISSANCE] [numeric](7, 2) NULL ,
	[POIDS] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_ACLA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_AC_AUTRES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_AI_CIRCULATION] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_AI_PERSONNES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_AI_VOYAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_BATIMENT_ENT] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_BATIMENT_IND] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_CASCO_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_CASCO_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_IARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[NO_SINISTRE] [char] (19) COLLATE fr_cs_as NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT] [numeric](12, 4) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_INV_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_INV_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_LAA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_MALADIE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_SIN] [tinyint] NOT NULL ,
	[CODE_COUCHE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_NO_ETAT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_NO_TRAV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_TRAIT_AA] [tinyint] NULL ,
	[D_TRAIT_MM] [tinyint] NULL ,
	[C_ENR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[GROUPE_ASS] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[GROUPE_TARIF] [tinyint] NULL ,
	[CODE_TARIF] [tinyint] NULL ,
	[ANNEE_TARIF] [tinyint] NULL ,
	[AGE_ACTUEL] [tinyint] NULL ,
	[ANN_ANT_CRT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_PRIME] [tinyint] NULL ,
	[NO_COLL_IND] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_ASSURE_COLL] [smallint] NULL ,
	[CODE_CANTON] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANC_AGCE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_COLL_IND_BAT] [tinyint] NULL ,
	[C_BR_REELLE] [tinyint] NULL ,
	[CODE_PAIEMENT] [tinyint] NULL ,
	[C_SIGNE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[PLAN_COLL] [smallint] NULL ,
	[DELAI_ATT_1] [smallint] NULL ,
	[FRANCHISE] [smallint] NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_FRAIS_A_CHARGE] [numeric](12, 2) NULL ,
	[MONTANT_FRAIS_A_RECUP] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_A_RECUP] [numeric](12, 2) NULL ,
	[MONTANT_QPART_INTRAS] [numeric](12, 2) NULL ,
	[MONTANT_GARANTIE_PAIEMENT] [numeric](12, 2) NULL ,
	[MONTANT_TOTAL_SINISTRE] [numeric](12, 2) NULL ,
	[MONTANT_PRESTATION_REASS] [numeric](12, 2) NULL ,
	[NB_JOURS_INDEMN_1] [numeric](12, 2) NULL ,
	[NB_JOURS_HOSP] [numeric](12, 2) NULL ,
	[NB_JOURS_PERTE_GAIN] [numeric](12, 2) NULL ,
	[NB_JOURS_INDEMN_2] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_MATH] [numeric](12, 2) NULL ,
	[NB_DECOMPTES] [numeric](12, 2) NULL ,
	[NB_CAS_SINISTRES] [numeric](12, 2) NULL ,
	[NB_CAS_ACCIDENTS] [numeric](12, 2) NULL ,
	[NB_CAS_ACCOUCHEMENTS] [numeric](12, 2) NULL ,
	[NB_JOURS_INDEMN_3] [numeric](12, 2) NULL ,
	[NB_JOURS_ACCIDENTS] [numeric](12, 2) NULL ,
	[NB_JOURS_ACCOUCHEMENTS] [numeric](12, 2) NULL ,
	[NB_JOURS_CURE] [numeric](12, 2) NULL ,
	[C_ACCOUCH] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_TYPE_MVT] [tinyint] NULL ,
	[ANNEE_CGA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[AGE_ENTREE] [tinyint] NULL ,
	[D_PAIEMENT_ANN] [tinyint] NULL ,
	[C_TRANS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[Z_TX_INCAPACITE] [smallint] NULL ,
	[C_SUFFIXE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_MANUEL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_ACCIDENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_AGGRAV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_CURE_SIN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_COUV_COLL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DELAI_ATT_2] [smallint] NULL ,
	[ANNEE_DEBUT_SIN] [tinyint] NULL ,
	[GROUPE_PAIEMENT] [tinyint] NULL ,
	[ANNEE_PAIEMENT] [tinyint] NULL ,
	[MOIS_PAIEMENT] [tinyint] NULL ,
	[DATE_DEBUT_PRESTATION] [datetime] NULL ,
	[DATE_FIN_PRESTATION] [datetime] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[DATE_EFFET_COUVERTURE] [datetime] NULL ,
	[DATE_ORDINATEUR] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_OBJET_VALEUR] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_RC_AGRICOLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_RC_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_RC_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_RC_IMMEUBLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_RC_PRIVEE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_RC_PROFESSIONNELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PAIEMENTS_VEHICULE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT] [smallint] NOT NULL ,
	[VISA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PARTENAIRES] (
	[NO_PERSONNE] [int] NOT NULL ,
	[TITRE_CIVIL] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (60) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[COMPLEMENT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM_ABREGE] [varchar] (45) COLLATE fr_cs_as NULL ,
	[CASE_POSTALE] [varchar] (25) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (5) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (24) COLLATE fr_cs_as NULL ,
	[CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [int] NULL ,
	[DATE_ENTREE] [datetime] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[TITULARISATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_SECTEUR_ACTIVITE] [smallint] NULL ,
	[TYPE_PERSONNE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_COMMUNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_ORGANISATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SEGMENT_MARCHE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PARTICULIERS] (
	[NO_ANNUAIRE] [tinyint] NULL ,
	[NOM] [varchar] (64) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (25) COLLATE fr_cs_as NULL ,
	[NOM_JEUNE_FILLE] [varchar] (25) COLLATE fr_cs_as NULL ,
	[PROFESSION] [varchar] (50) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (33) COLLATE fr_cs_as NULL ,
	[NUMERO_RUE] [varchar] (7) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[FAX] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (4) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (25) COLLATE fr_cs_as NULL ,
	[BRANCHE] [varchar] (70) COLLATE fr_cs_as NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_ETAT_CIVIL] [tinyint] NULL ,
	[CODE_NOUVEAU] [tinyint] NULL ,
	[CASE_POSTALE] [varchar] (33) COLLATE fr_cs_as NULL ,
	[NO_POSTAL_CASE_POSTALE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[LOCALITE_CASE_POSTALE] [varchar] (25) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PART_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TYPE_DROIT] [tinyint] NOT NULL ,
	[MODE_PARTICIPATION] [tinyint] NOT NULL ,
	[DATE_DEBUT_PART] [datetime] NOT NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NOT NULL ,
	[MONTANT_DROIT_PART] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_ACLA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[TAUX_COASSURANCE] [numeric](7, 3) NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_SUSPENSION] [datetime] NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[DATE_MUTATION] [datetime] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[SECTEUR_ACTIVITE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[NPA_RISQUE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SORTIR_POLICE] [tinyint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL ,
	[CODE_ETAT_QUARTET] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_QUARTET] [varchar] (3) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_AC_AUTRES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_SUPPRESSION] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_AI_CIRCULATION] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_AI_CIRCULATION_200702] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_AI_PERSONNES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_SORTIR_POLICE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_AI_PERSONNES_200702] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_SORTIR_POLICE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_AI_VOYAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_AI_VOYAGE_200702] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_BATIMENT_ENT] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_SEGMENT] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_BATIMENT_IND] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_SEGMENT] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_CASCO_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_CASCO_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_INV_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_SEGMENT] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_INV_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_OBJET_ASSURE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_LAA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[TAUX_COASSURANCE] [numeric](7, 3) NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_SUSPENSION] [datetime] NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[DATE_MUTATION] [datetime] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[SECTEUR_ACTIVITE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[NPA_RISQUE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SORTIR_POLICE] [tinyint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL ,
	[CODE_ETAT_QUARTET] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_QUARTET] [varchar] (3) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_MALADIE_COLLECTIVE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[NO_SECTEUR_ECONOMIQUE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[DUREE_CONTRAT] [tinyint] NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_DEBUT_PARTICIPATION] [datetime] NULL ,
	[MODE_PARTICIPATION] [tinyint] NULL ,
	[TAUX_PRIME_PARTICIPATION] [tinyint] NULL ,
	[TAUX_SINISTRE_PARTICIPATION] [tinyint] NULL ,
	[NATURE_CONTRAT] [tinyint] NULL ,
	[NATURE_PRIME] [tinyint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[CODE_INDEPENDANT] [tinyint] NULL ,
	[NOMBRE_HOMMES_ASSURES] [smallint] NULL ,
	[NOMBRE_FEMMES_ASSURES] [smallint] NULL ,
	[NOMBRE_TOTAL_ASSURES] [smallint] NULL ,
	[SALAIRE_HOMMES] [numeric](12, 0) NULL ,
	[SALAIRE_FEMMES] [numeric](12, 0) NULL ,
	[TOTAL_SALAIRES] [numeric](12, 0) NULL ,
	[NO_GROUPEMENT] [smallint] NULL ,
	[DATE_RENOUV_TAUX] [datetime] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[CODE_ETAT_QUARTET] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_QUARTET] [varchar] (3) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [numeric](7, 2) NULL ,
	[LAST_USER] [varchar] (8) COLLATE fr_cs_as NULL ,
	[DATE_RENOUV_CONTRAT] [datetime] NULL ,
	[DATE_RENOUV_TAUX2] [datetime] NULL ,
	[SIN_OUVERT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CAUSE_RESILIATION] [smallint] NULL ,
	[C_NOGA] [varchar] (6) COLLATE fr_cs_as NULL ,
	[C_SEQ_NOGA] [tinyint] NULL ,
	[M_FRCH_CTRA] [int] NULL ,
	[GENRE_GESTION] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_MALADIE_COLL_MIGREES_GIRARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[PERIODE_LOADED] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_MALADIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[NOM] [varchar] (22) COLLATE fr_cs_as NULL ,
	[NO_COLLECTIVE_IND] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DUREE_CONTRAT] [tinyint] NULL ,
	[AGE_FINAL] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_EXCEPTION] [tinyint] NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[CODE_AGGRAVATION] [tinyint] NULL ,
	[CODE_ACCIDENT_COUV] [tinyint] NULL ,
	[CODE_CLASSE_ACC] [tinyint] NULL ,
	[CODE_CALCUL_SPECIAL] [tinyint] NULL ,
	[CODE_RABAIS_FRAIS_GUERISON] [tinyint] NULL ,
	[CODE_RABAIS_CURE] [tinyint] NULL ,
	[CODE_RABAIS_FAMILIAL] [tinyint] NULL ,
	[RESERVE_VEILLESSE] [numeric](12, 2) NULL ,
	[TRAITEMENT_AMBULATOIRE] [numeric](12, 2) NULL ,
	[SUP_ACC_HORS_CLASSE] [numeric](12, 2) NULL ,
	[SUPPL_MALADIE] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL ,
	[PARTICIPATION_EXCEDENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_MALADIE_INDIVIDUELLE_200704] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[NOM] [varchar] (22) COLLATE fr_cs_as NULL ,
	[NO_COLLECTIVE_IND] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DUREE_CONTRAT] [tinyint] NULL ,
	[AGE_FINAL] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_EXCEPTION] [tinyint] NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[CODE_AGGRAVATION] [tinyint] NULL ,
	[CODE_ACCIDENT_COUV] [tinyint] NULL ,
	[CODE_CLASSE_ACC] [tinyint] NULL ,
	[CODE_CALCUL_SPECIAL] [tinyint] NULL ,
	[CODE_RABAIS_FRAIS_GUERISON] [tinyint] NULL ,
	[CODE_RABAIS_CURE] [tinyint] NULL ,
	[CODE_RABAIS_FAMILIAL] [tinyint] NULL ,
	[RESERVE_VEILLESSE] [numeric](12, 2) NULL ,
	[TRAITEMENT_AMBULATOIRE] [numeric](12, 2) NULL ,
	[SUP_ACC_HORS_CLASSE] [numeric](12, 2) NULL ,
	[SUPPL_MALADIE] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL ,
	[PARTICIPATION_EXCEDENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_MALADIE_IND_MIGREES_GIRARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[PERIODE_LOADED] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_OBJET_VALEUR] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_RC_AGRICOLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_RC_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_RC_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_RC_IMMEUBLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_RC_PRIVEE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_RC_PROFESSIONNELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_CANTON_RISQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[RABAIS_SUPPLEMENTAIRE] [tinyint] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_VC04_MIGREES_GIRARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[PERIODE_LOADED] [int] NULL ,
	[POL_DATE_ANNULATION] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_VEHICULE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[CODE_SORTIR_POLICE] [tinyint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_VEHICULE_TMP] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [tinyint] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[CODE_SORTIR_POLICE] [tinyint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_VIE_COLLECTIVE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[MOIS_EFFET] [tinyint] NULL ,
	[DATE_EFFET_CONTRAT] [datetime] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[DATE_LIMITE_FACTURATION] [datetime] NULL ,
	[DATE_EFFET_MUTATION] [datetime] NULL ,
	[DATE_REPARTITION_EXCEDENTS] [datetime] NULL ,
	[CODE_FONDATION] [smallint] NULL ,
	[CODE_TYPE_JURIDIQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ETAT_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CAUSE_RESILIATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_RABAIS_GRAND_GROUPE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[UTILISATION_RABAIS] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RABAIS_FRAIS_GESTION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[REASSURANCE_TRAITE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANTON] [tinyint] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[MODE_REPARTITION_EXC] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_BON_SANTE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DROIT_OPTION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FACTURATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RENCHERISSEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MESURES_SPECIALES] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FONDS_GARANTIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FRAIS_GESTION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_GENRE_COUVERTURE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_APPLICATION_TAUX] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_DROIT_SUBSIDES] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_PLAN_EXCEDENTS] [smallint] NULL ,
	[TEAM_GESTION_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DECENTRALISATION_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TEAM_DOCUMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SECTEUR_ACTIVITE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CLASSE_RISQUE_DECES] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CLASSE_RISQUE_INV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[FACTEUR_RISQUE_MAN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_ACTIVITE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TAUX_RABAIS_GRAND_GROUPE] [numeric](5, 2) NULL ,
	[TAUX_RABAIS_FRAIS_GESTION] [numeric](5, 2) NULL ,
	[FACTEUR_RISQUE_DECES] [numeric](6, 3) NULL ,
	[FACTEUR_RISQUE_INV] [numeric](6, 3) NULL ,
	[TAUX_COASSURANCE] [numeric](7, 2) NULL ,
	[PRIME_RISQUE] [numeric](12, 2) NULL ,
	[PRIME_EPARGNE] [numeric](12, 2) NULL ,
	[PRIME_EPARGNE_POOL] [numeric](12, 2) NULL ,
	[PRIME_NIVELEE] [numeric](12, 2) NULL ,
	[PRIME_RENCHERISSEMENT] [numeric](12, 2) NULL ,
	[PRIME_MESURES_SPECIALES] [numeric](12, 2) NULL ,
	[PRIME_FONDS_GARANTIE] [numeric](12, 2) NULL ,
	[PRIME_FRAIS_GESTION] [numeric](12, 2) NULL ,
	[PRIME_ANNUELLE_TOTALE] [numeric](12, 2) NULL ,
	[RESERVE_TOTALE] [numeric](12, 2) NULL ,
	[RABAIS_GRAND_GROUPE] [numeric](12, 2) NULL ,
	[RABAIS_FRAIS_GESTION] [numeric](12, 2) NULL ,
	[MONTANT_EXCEDENTS] [numeric](12, 2) NULL ,
	[NOMBRE_ASSURES_ACTIFS] [smallint] NULL ,
	[NOMBRE_RENTIERS] [smallint] NULL ,
	[NOMBRE_INVALIDES] [smallint] NULL ,
	[TOTAL_SALAIRES_AVS] [numeric](12, 2) NULL ,
	[TOTAL_SALAIRES_LPP] [numeric](12, 2) NULL ,
	[TOTAL_SALAIRES_ASSURES] [numeric](12, 2) NULL ,
	[TAUX_INTERET_LPP] [numeric](5, 3) NULL ,
	[TAUX_INTERET_HORS_LPP] [numeric](5, 3) NULL ,
	[TAUX_PROJECTION_LPP] [numeric](5, 3) NULL ,
	[TAUX_PROJECTION_HORS_LPP] [numeric](5, 3) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_VIE_COLLECTIVEbof] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[MOIS_EFFET] [tinyint] NULL ,
	[DATE_EFFET_CONTRAT] [datetime] NULL ,
	[DATE_EXPIRATION] [datetime] NULL ,
	[DATE_LIMITE_FACTURATION] [datetime] NULL ,
	[DATE_EFFET_MUTATION] [datetime] NULL ,
	[DATE_REPARTITION_EXCEDENTS] [datetime] NULL ,
	[CODE_FONDATION] [smallint] NULL ,
	[CODE_TYPE_JURIDIQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ETAT_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CAUSE_RESILIATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_RABAIS_GRAND_GROUPE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[UTILISATION_RABAIS] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RABAIS_FRAIS_GESTION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[REASSURANCE_TRAITE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANTON] [tinyint] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[MODE_REPARTITION_EXC] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_BON_SANTE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DROIT_OPTION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FACTURATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_RENCHERISSEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MESURES_SPECIALES] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FONDS_GARANTIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FRAIS_GESTION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_GENRE_COUVERTURE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_APPLICATION_TAUX] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_DROIT_SUBSIDES] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_PLAN_EXCEDENTS] [smallint] NULL ,
	[TEAM_GESTION_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DECENTRALISATION_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TEAM_DOCUMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SECTEUR_ACTIVITE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CLASSE_RISQUE_DECES] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CLASSE_RISQUE_INV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[FACTEUR_RISQUE_MAN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_ACTIVITE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TAUX_RABAIS_GRAND_GROUPE] [numeric](5, 2) NULL ,
	[TAUX_RABAIS_FRAIS_GESTION] [numeric](5, 2) NULL ,
	[FACTEUR_RISQUE_DECES] [numeric](6, 3) NULL ,
	[FACTEUR_RISQUE_INV] [numeric](6, 3) NULL ,
	[TAUX_COASSURANCE] [numeric](7, 2) NULL ,
	[PRIME_RISQUE] [numeric](12, 2) NULL ,
	[PRIME_EPARGNE] [numeric](12, 2) NULL ,
	[PRIME_NIVELEE] [numeric](12, 2) NULL ,
	[PRIME_RENCHERISSEMENT] [numeric](12, 2) NULL ,
	[PRIME_MESURES_SPECIALES] [numeric](12, 2) NULL ,
	[PRIME_FONDS_GARANTIE] [numeric](12, 2) NULL ,
	[PRIME_FRAIS_GESTION] [numeric](12, 2) NULL ,
	[PRIME_ANNUELLE_TOTALE] [numeric](12, 2) NULL ,
	[RESERVE_TOTALE] [numeric](12, 2) NULL ,
	[RABAIS_GRAND_GROUPE] [numeric](12, 2) NULL ,
	[RABAIS_FRAIS_GESTION] [numeric](12, 2) NULL ,
	[MONTANT_EXCEDENTS] [numeric](12, 2) NULL ,
	[NOMBRE_ASSURES_ACTIFS] [smallint] NULL ,
	[NOMBRE_RENTIERS] [smallint] NULL ,
	[NOMBRE_INVALIDES] [smallint] NULL ,
	[TOTAL_SALAIRES_AVS] [numeric](12, 2) NULL ,
	[TOTAL_SALAIRES_LPP] [numeric](12, 2) NULL ,
	[TOTAL_SALAIRES_ASSURES] [numeric](12, 2) NULL ,
	[TAUX_INTERET_LPP] [numeric](5, 3) NULL ,
	[TAUX_INTERET_HORS_LPP] [numeric](5, 3) NULL ,
	[TAUX_PROJECTION_LPP] [numeric](5, 3) NULL ,
	[TAUX_PROJECTION_HORS_LPP] [numeric](5, 3) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[POLICES_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_VALIDATION] [datetime] NULL ,
	[CODE_ANNULATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PREVOYANCE_LIEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_SALAIRE_INDEPENDANT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_POLICE_GAGEE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_PREST_EN_COURS] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_GENRE_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_GV8_REVAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_CLAUSE_BENEF] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_GARANTI_ASSUR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_PERIODE_GARANTIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_DERNIERE_GARANTIE] [datetime] NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[MONTANT_PRIME_PERIODIQUE] [numeric](12, 2) NULL ,
	[TOTAL_PRIMES_UNIQUES] [numeric](12, 2) NULL ,
	[DATE_DEBUT_PRET] [datetime] NULL ,
	[DATE_FIN_PRET] [datetime] NULL ,
	[CODE_MODE_FRACT] [tinyint] NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[TAUX_ANNUEL_PRET] [numeric](7, 6) NULL ,
	[JOUR_ANNIVERSAIRE] [tinyint] NULL ,
	[MOIS_ANNIVERSAIRE] [tinyint] NULL ,
	[MONTANT_PRET] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PORTEFEUILLE_1995] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_PRENEUR] [int] NULL ,
	[NOUVEAU] [varchar] (1) COLLATE fr_cs_as NULL ,
	[GES_CODE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[NAME] [varchar] (30) COLLATE fr_cs_as NULL ,
	[VORNAME] [varchar] (30) COLLATE fr_cs_as NULL ,
	[STR_HAUS] [varchar] (35) COLLATE fr_cs_as NULL ,
	[PLZ] [varchar] (5) COLLATE fr_cs_as NULL ,
	[ORT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[BES_PP] [numeric](12, 2) NULL ,
	[BES_EE] [numeric](12, 2) NULL ,
	[BES_PV] [numeric](12, 2) NULL ,
	[BES_MF] [numeric](12, 2) NULL ,
	[BES_SV] [numeric](12, 2) NULL ,
	[BES_VV] [numeric](12, 2) NULL ,
	[NEU_PP] [numeric](12, 2) NULL ,
	[NEU_EE] [numeric](12, 2) NULL ,
	[NEU_PV] [numeric](12, 2) NULL ,
	[NEU_MF] [numeric](12, 2) NULL ,
	[NEU_SV] [numeric](12, 2) NULL ,
	[NEU_VV] [numeric](12, 2) NULL ,
	[SBU] [varchar] (1) COLLATE fr_cs_as NULL ,
	[VST_BEZ] [varchar] (30) COLLATE fr_cs_as NULL ,
	[VKST_PLZ] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PERS_NAM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[PERS_NR] [varchar] (9) COLLATE fr_cs_as NULL ,
	[KUNDE_NR] [varchar] (9) COLLATE fr_cs_as NULL ,
	[VERTRAG_NR] [varchar] (11) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRENEURS_PAYEURS] (
	[NO_PERSONNE] [int] NOT NULL ,
	[TITRE_CIVIL] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (60) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (40) COLLATE fr_cs_as NULL ,
	[COMPLEMENT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM_ABREGE] [varchar] (45) COLLATE fr_cs_as NULL ,
	[CASE_POSTALE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (5) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_ENTREE] [datetime] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[TITULARISATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_SECTEUR_ACTIVITE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[TYPE_PERSONNE] [tinyint] NULL ,
	[NO_COMMUNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_ORGANISATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SEGMENT_MARCHE] [tinyint] NULL ,
	[NOMBRE_COLLABORATEURS] [smallint] NULL ,
	[NO_AGC] [tinyint] NULL ,
	[NO_AGT] [smallint] NULL ,
	[DATE_DECES] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRENEURS_PAYEURS_ADRESSES] (
	[NO_PERSONNE] [int] NOT NULL ,
	[C_NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[TYPE_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[TITRE_CIVIL] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (60) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (40) COLLATE fr_cs_as NULL ,
	[COMPLEMENT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM_ABREGE] [varchar] (45) COLLATE fr_cs_as NULL ,
	[CASE_POSTALE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (5) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[TYPE_PERSONNE] [tinyint] NULL ,
	[NO_COMMUNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CONFIDENTIEL] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TABLE_ADRESSE] [varchar] (3) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRENEURS_PAYEURS_CONFID] (
	[NO_PERSONNE] [int] NOT NULL ,
	[TITRE_CIVIL] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (60) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[COMPLEMENT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM_ABREGE] [varchar] (45) COLLATE fr_cs_as NULL ,
	[CASE_POSTALE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (5) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (24) COLLATE fr_cs_as NULL ,
	[CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_ENTREE] [datetime] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[TITULARISATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_SECTEUR_ACTIVITE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[TYPE_PERSONNE] [tinyint] NULL ,
	[NO_COMMUNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_ORGANISATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SEGMENT_MARCHE] [tinyint] NULL ,
	[NOMBRE_COLLABORATEURS] [smallint] NULL ,
	[NO_AGC] [tinyint] NULL ,
	[NO_AGT] [smallint] NULL ,
	[DATE_DECES] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRENEURS_PAYEURS_TEST] (
	[NO_PERSONNE] [int] NOT NULL ,
	[TITRE_CIVIL] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (60) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (40) COLLATE fr_cs_as NULL ,
	[COMPLEMENT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NOM_ABREGE] [varchar] (45) COLLATE fr_cs_as NULL ,
	[CASE_POSTALE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (5) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_ENTREE] [datetime] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[CODE_LANGUE] [tinyint] NULL ,
	[TITULARISATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_SECTEUR_ACTIVITE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[TYPE_PERSONNE] [tinyint] NULL ,
	[NO_COMMUNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_ORGANISATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SEGMENT_MARCHE] [tinyint] NULL ,
	[NOMBRE_COLLABORATEURS] [smallint] NULL ,
	[NO_AGC] [tinyint] NULL ,
	[NO_AGT] [smallint] NULL ,
	[DATE_DECES] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_0819_0820] (
	[ANNEE_EMISSION] [smallint] NOT NULL ,
	[MOIS_EMISSION] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_PRD] [smallint] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_MN_ISO] [smallint] NOT NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[M_ECR] [numeric](12, 2) NOT NULL ,
	[D_VAL_ECR] [datetime] NOT NULL ,
	[C_PERIODE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[NO_PERS] [int] NOT NULL ,
	[T_NOM_PREN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[K_FONDATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_NAT_DWH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[NO_ASSURE_IC] [int] NULL ,
	[NO_REGUL] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_AI_CIRCULATION] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_PRESTATION] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[MONTANT_FRANCHISE] [smallint] NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_AI_PERSONNES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_PRESTATION] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[MONTANT_FRANCHISE] [smallint] NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_AI_VOYAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_PRESTATION] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[MONTANT_FRANCHISE] [smallint] NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_ZP] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[NO_SIN_BR] [int] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NAT_DWH] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[D_TRMT_ECR] [datetime] NOT NULL ,
	[D_VAL_ECR] [datetime] NOT NULL ,
	[D_CMPT_ECR] [datetime] NOT NULL ,
	[MOIS_EMISSION] [tinyint] NOT NULL ,
	[ANNEE_EMISSION] [smallint] NOT NULL ,
	[NO_CPT_VIE] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[D_EDIT_TAR_SSAA] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_SOC] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_COASS] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[M_ECR] [numeric](12, 2) NOT NULL ,
	[L_ATT_AN] [char] (1) COLLATE fr_cs_as NULL ,
	[C_ATT_CMPT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_REF_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[C_MET_OPE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_SEGT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_TBL] [varchar] (3) COLLATE fr_cs_as NULL ,
	[K_FONDATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL ,
	[CCPT] [int] NULL ,
	[CCTYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_NAT_CMPT] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_NAT_DWH_CPV] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_TYP_AFF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CPT_SAP] [varchar] (20) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_ZP_MELAN] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[NO_SIN_BR] [int] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NAT_DWH] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[D_TRMT_ECR] [datetime] NOT NULL ,
	[D_VAL_ECR] [datetime] NOT NULL ,
	[D_CMPT_ECR] [datetime] NOT NULL ,
	[MOIS_EMISSION] [tinyint] NOT NULL ,
	[ANNEE_EMISSION] [smallint] NOT NULL ,
	[NO_CPT_VIE] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[D_EDIT_TAR_SSAA] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_SOC] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_COASS] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[M_ECR] [numeric](12, 2) NOT NULL ,
	[L_ATT_AN] [char] (1) COLLATE fr_cs_as NULL ,
	[C_ATT_CMPT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_REF_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[C_MET_OPE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_SEGT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_TBL] [varchar] (3) COLLATE fr_cs_as NULL ,
	[K_FONDATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL ,
	[CCPT] [int] NULL ,
	[CCTYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_NAT_CMPT] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_NAT_DWH_CPV] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_TYP_AFF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CPT_SAP] [varchar] (20) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_ZS] (
	[D_DEB] [datetime] NOT NULL ,
	[D_FIN] [datetime] NOT NULL ,
	[D_JOUR] [datetime] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[D_TRMT_ECR] [datetime] NOT NULL ,
	[D_VAL_ECR] [datetime] NOT NULL ,
	[D_CMPT_ECR] [datetime] NOT NULL ,
	[C_GNR_TRMT_ECR] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[C_AFLT_CMPT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[C_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[NO_PERS] [int] NOT NULL ,
	[C_PERIODE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_DOC_EMIS] [int] NOT NULL ,
	[C_TYP_DOC] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[D_TRMT_CLTR] [datetime] NOT NULL ,
	[D_DOC_EMIS] [datetime] NOT NULL ,
	[D_ANN_DOC] [datetime] NOT NULL ,
	[M_EMIS] [numeric](12, 2) NOT NULL ,
	[DOC_REFAIT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[DC] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_DET] [int] NOT NULL ,
	[NO_REGUL] [int] NOT NULL ,
	[M_DET] [numeric](12, 2) NOT NULL ,
	[M_RGL] [numeric](12, 2) NOT NULL ,
	[M_NONRGL] [numeric](12, 2) NOT NULL ,
	[M_RGL_BL] [numeric](12, 2) NOT NULL ,
	[M_NONRGL_BL] [numeric](12, 2) NOT NULL ,
	[M_CONF] [numeric](12, 2) NOT NULL ,
	[CCPT] [int] NULL ,
	[CCTYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[D_PMNT_AVNC] [datetime] NULL ,
	[D_BAL_ECR] [datetime] NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_POL] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRESTATIONS_ZS_MELAN] (
	[D_DEB] [datetime] NOT NULL ,
	[D_FIN] [datetime] NOT NULL ,
	[D_JOUR] [datetime] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[D_TRMT_ECR] [datetime] NOT NULL ,
	[D_VAL_ECR] [datetime] NOT NULL ,
	[D_CMPT_ECR] [datetime] NOT NULL ,
	[C_GNR_TRMT_ECR] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[C_AFLT_CMPT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[C_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[NO_PERS] [int] NOT NULL ,
	[C_PERIODE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_DOC_EMIS] [int] NOT NULL ,
	[C_TYP_DOC] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[D_TRMT_CLTR] [datetime] NOT NULL ,
	[D_DOC_EMIS] [datetime] NOT NULL ,
	[D_ANN_DOC] [datetime] NOT NULL ,
	[M_EMIS] [numeric](12, 2) NOT NULL ,
	[DOC_REFAIT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[DC] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_DET] [int] NOT NULL ,
	[NO_REGUL] [int] NOT NULL ,
	[M_DET] [numeric](12, 2) NOT NULL ,
	[M_RGL] [numeric](12, 2) NOT NULL ,
	[M_NONRGL] [numeric](12, 2) NOT NULL ,
	[M_RGL_BL] [numeric](12, 2) NOT NULL ,
	[M_NONRGL_BL] [numeric](12, 2) NOT NULL ,
	[M_CONF] [numeric](12, 2) NOT NULL ,
	[CCPT] [int] NULL ,
	[CCTYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[D_PMNT_AVNC] [datetime] NULL ,
	[D_BAL_ECR] [datetime] NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_POL] [int] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PREST_CAP_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[NO_MVT_CATAL] [smallint] NOT NULL ,
	[CODE_GENRE_PRESTATION] [smallint] NOT NULL ,
	[CODE_MOTIF_PRESTATION] [smallint] NULL ,
	[DATE_ECH_PRESTATION] [datetime] NULL ,
	[DATE_CATALOGUE] [datetime] NULL ,
	[MONTANT_PRESTATION] [numeric](11, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PREST_VIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[DATE_DEBUT_PRESTATION] [datetime] NOT NULL ,
	[DATE_FIN_PRESTATION] [datetime] NULL ,
	[TAUX_PRESTATION] [numeric](7, 3) NULL ,
	[CODE_MODE_FRACT] [tinyint] NULL ,
	[MONTANT_PRESTATION_AN] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRIMES_EMISES_GL] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_DEBIT] [datetime] NOT NULL ,
	[NO_MOUVEMENT] [int] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE_DEBIT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_DEBIT] [tinyint] NULL ,
	[NO_COLLECTIVE_IND] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL ,
	[DATE_DEBUT] [datetime] NULL ,
	[DATE_FIN] [datetime] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRIMES_EMISES_GL_MELAN] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_DEBIT] [datetime] NOT NULL ,
	[NO_MOUVEMENT] [int] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NULL ,
	[ANNEE_COMPTABLE] [smallint] NULL ,
	[CODE_NATURE_DEBIT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_DEBIT] [tinyint] NULL ,
	[NO_COLLECTIVE_IND] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL ,
	[DATE_DEBUT] [datetime] NULL ,
	[DATE_FIN] [datetime] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRIMES_EMISES_IARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[DATE_DEBIT] [datetime] NOT NULL ,
	[CODE_GROUPE_1] [tinyint] NOT NULL ,
	[CODE_GROUPE_2] [tinyint] NOT NULL ,
	[CODE_GROUPE_3] [tinyint] NOT NULL ,
	[CODE_GROUPE_4] [tinyint] NOT NULL ,
	[CODE_GROUPE_5] [tinyint] NOT NULL ,
	[CODE_GROUPE_6] [tinyint] NOT NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[CODE_EVENEMENT] [tinyint] NOT NULL ,
	[CODE_DECOMPTE_COASS] [tinyint] NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[TAUX_COASSURANCE] [numeric](7, 2) NULL ,
	[TAUX_ADMINISTRATION] [numeric](9, 2) NULL ,
	[MONTANT_ADMINISTRATION] [numeric](9, 2) NULL ,
	[MONTANT_PRIME] [numeric](9, 2) NULL ,
	[MONTANT_BONUS_MALUS] [numeric](9, 2) NULL ,
	[MONTANT_FRAIS] [numeric](9, 2) NULL ,
	[MONTANT_TAXES] [numeric](9, 2) NULL ,
	[MONTANT_TF_PRIME] [numeric](9, 2) NULL ,
	[MONTANT_TF_FRAIS] [numeric](9, 2) NULL ,
	[MONTANT_PA] [numeric](9, 2) NULL ,
	[MONTANT_ART74] [numeric](9, 2) NULL ,
	[MONTANT_ART76] [numeric](9, 2) NULL ,
	[DATE_DEBUT] [datetime] NULL ,
	[DATE_FIN] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRIMES_RISQUE_VC04] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[ANNEE] [smallint] NOT NULL ,
	[PRIME_RISQUE] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRIMES_ZP] (
	[D_DEB] [datetime] NOT NULL ,
	[D_FIN] [datetime] NOT NULL ,
	[D_JOUR] [datetime] NOT NULL ,
	[F_RGL] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[C_TBL] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_TYP_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[R_POL] [int] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[C_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[D_CLTR_CPT] [datetime] NULL ,
	[C_PERIODE] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_GNR_TRMT_ECR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[D_TRMT_ECR] [datetime] NULL ,
	[D_VAL_ECR] [datetime] NULL ,
	[D_VAL_FIN_ECR] [datetime] NULL ,
	[D_CMPT_ECR] [datetime] NULL ,
	[NO_CMPT] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_SOC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_ALFT_CMPT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CCPT] [int] NULL ,
	[CCTYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_ATT_AN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_ATT_CMPT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_REF_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[C_MET_OPE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_DOC_EMIS] [int] NULL ,
	[C_TYP_DOC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_TRMT_CLTR] [datetime] NULL ,
	[D_DOC_EMIS] [datetime] NULL ,
	[D_ANN_DOC] [datetime] NULL ,
	[M_EMIS] [numeric](12, 2) NULL ,
	[DOC_REFAIT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DC] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[M_ECR] [numeric](12, 2) NULL ,
	[M_HNONRGL_BL] [numeric](12, 2) NULL ,
	[M_RGL] [numeric](12, 2) NULL ,
	[M_NONRGL] [numeric](12, 2) NULL ,
	[M_RGL_BL] [numeric](12, 2) NULL ,
	[M_NONRGL_BL] [numeric](12, 2) NULL ,
	[M_DEDUC] [numeric](12, 2) NULL ,
	[NO_PERS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[C_SEGT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[R_AGCE_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_AGNT_ZC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[R_AGCE_ZA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_AGNT_ZA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_CANTON] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PAYS_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_ANNU_SIT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_BR_VS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_ASMB] [int] NULL ,
	[C_GNR_ASMB] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_STAT_CTR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_BROK] [varchar] (1) COLLATE fr_cs_as NULL ,
	[K_FONDATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL ,
	[C_TBL_LIEN] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_GENCOUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[C_NAT_CMPT] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_NAT_DWH_CPV] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_TYP_AFF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CPT_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_CANTON_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PAYS_ISO_AET] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_CMNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NOM_CMNE] [varchar] (18) COLLATE fr_cs_as NULL ,
	[NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PRD_LPC] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRIMES_ZP_MELAN] (
	[D_DEB] [datetime] NOT NULL ,
	[D_FIN] [datetime] NOT NULL ,
	[D_JOUR] [datetime] NOT NULL ,
	[F_RGL] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[C_TBL] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_TYP_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[R_POL] [int] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[C_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[D_CLTR_CPT] [datetime] NULL ,
	[C_PERIODE] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_GNR_TRMT_ECR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[D_TRMT_ECR] [datetime] NULL ,
	[D_VAL_ECR] [datetime] NULL ,
	[D_VAL_FIN_ECR] [datetime] NULL ,
	[D_CMPT_ECR] [datetime] NULL ,
	[NO_CMPT] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_SOC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_ALFT_CMPT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CCPT] [int] NULL ,
	[CCTYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_ATT_AN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_ATT_CMPT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_REF_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[C_MET_OPE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_DOC_EMIS] [int] NULL ,
	[C_TYP_DOC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_TRMT_CLTR] [datetime] NULL ,
	[D_DOC_EMIS] [datetime] NULL ,
	[D_ANN_DOC] [datetime] NULL ,
	[M_EMIS] [numeric](12, 2) NULL ,
	[DOC_REFAIT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DC] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[M_ECR] [numeric](12, 2) NULL ,
	[M_HNONRGL_BL] [numeric](12, 2) NULL ,
	[M_RGL] [numeric](12, 2) NULL ,
	[M_NONRGL] [numeric](12, 2) NULL ,
	[M_RGL_BL] [numeric](12, 2) NULL ,
	[M_NONRGL_BL] [numeric](12, 2) NULL ,
	[M_DEDUC] [numeric](12, 2) NULL ,
	[NO_PERS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[C_SEGT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[R_AGCE_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_AGNT_ZC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[R_AGCE_ZA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_AGNT_ZA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_CANTON] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PAYS_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_ANNU_SIT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_BR_VS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_ASMB] [int] NULL ,
	[C_GNR_ASMB] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_STAT_CTR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_BROK] [varchar] (1) COLLATE fr_cs_as NULL ,
	[K_FONDATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL ,
	[C_TBL_LIEN] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_GENCOUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[C_NAT_CMPT] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_NAT_DWH_CPV] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_TYP_AFF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CPT_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_CANTON_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PAYS_ISO_AET] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_CMNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NOM_CMNE] [varchar] (18) COLLATE fr_cs_as NULL ,
	[NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PRD_LPC] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[PRIMES_ZP_PEPU_FIXED] (
	[D_DEB] [datetime] NOT NULL ,
	[D_FIN] [datetime] NOT NULL ,
	[D_JOUR] [datetime] NOT NULL ,
	[F_RGL] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[C_TBL] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_TYP_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[R_POL] [int] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[C_MN_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[D_CLTR_CPT] [datetime] NULL ,
	[C_PERIODE] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_GNR_TRMT_ECR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[D_TRMT_ECR] [datetime] NULL ,
	[D_VAL_ECR] [datetime] NULL ,
	[D_VAL_FIN_ECR] [datetime] NULL ,
	[D_CMPT_ECR] [datetime] NULL ,
	[NO_CMPT] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_SOC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_BR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_ALFT_CMPT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CCPT] [int] NULL ,
	[CCTYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_ATT_AN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_ATT_CMPT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_REF_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[C_MET_OPE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_DOC_EMIS] [int] NULL ,
	[C_TYP_DOC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_TRMT_CLTR] [datetime] NULL ,
	[D_DOC_EMIS] [datetime] NULL ,
	[D_ANN_DOC] [datetime] NULL ,
	[M_EMIS] [numeric](12, 2) NULL ,
	[DOC_REFAIT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DC] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[M_ECR] [numeric](12, 2) NULL ,
	[M_HNONRGL_BL] [numeric](12, 2) NULL ,
	[M_RGL] [numeric](12, 2) NULL ,
	[M_NONRGL] [numeric](12, 2) NULL ,
	[M_RGL_BL] [numeric](12, 2) NULL ,
	[M_NONRGL_BL] [numeric](12, 2) NULL ,
	[M_DEDUC] [numeric](12, 2) NULL ,
	[NO_PERS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[C_SEGT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[R_AGCE_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_AGNT_ZC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[R_AGCE_ZA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[R_AGNT_ZA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_CANTON] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PAYS_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_ANNU_SIT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_BR_VS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_ASMB] [int] NULL ,
	[C_GNR_ASMB] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_STAT_CTR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_BROK] [varchar] (1) COLLATE fr_cs_as NULL ,
	[K_FONDATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL ,
	[C_TBL_LIEN] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_GENCOUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[C_NAT_CMPT] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_NAT_DWH_CPV] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_TYP_AFF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CPT_SAP] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_CANTON_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PAYS_ISO_AET] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_CMNE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NOM_CMNE] [varchar] (18) COLLATE fr_cs_as NULL ,
	[NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_PRD_LPC] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[QUARTETS] (
	[NO_QUARTET] [int] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[STATUT_QUARTET] [char] (1) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[REMUNERATION_AGENTS] (
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[DATE_COMPTABLE] [datetime] NOT NULL ,
	[CODE_HIERARCHIQUE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_COMPAGNIE] [smallint] NOT NULL ,
	[COMMISSIONS_ACQUISITION] [numeric](12, 2) NULL ,
	[COMMISSIONS_PARTICIPATION] [numeric](12, 2) NULL ,
	[INDEMNITES_PERTE_GAIN] [numeric](9, 2) NULL ,
	[INDEMNITES_ADMINISTRATION] [numeric](9, 2) NULL ,
	[INDEMNITES_DIVERSES] [numeric](9, 2) NULL ,
	[MONTANT_RAPPELS] [numeric](9, 2) NULL ,
	[MONTANT_FIXE] [numeric](12, 2) NULL ,
	[MONTANT_FRAIS] [numeric](12, 2) NULL ,
	[MONTANT_AVANCES] [numeric](12, 2) NULL ,
	[MONTANT_GARANTIES] [numeric](12, 2) NULL ,
	[EXCEDENT_GARANTIES] [numeric](12, 2) NULL ,
	[CORRECTION_GARANTIES] [numeric](12, 2) NULL ,
	[COMPTE_GARANTIES] [numeric](12, 2) NULL ,
	[TRANSFERT_GARANTIES] [numeric](12, 2) NULL ,
	[BONIFICATIONS_COMPLEMENTAIRES] [numeric](9, 2) NULL ,
	[FONDS_PREVOYANCE] [numeric](9, 2) NULL ,
	[MONTANT_AVS] [numeric](9, 2) NULL ,
	[MONTANT_AC_1] [numeric](9, 2) NULL ,
	[MONTANT_AC_2] [numeric](9, 2) NULL ,
	[CAISSE_MALADIE] [numeric](9, 2) NULL ,
	[ALLOCATIONS_FAMILIALES] [numeric](9, 2) NULL ,
	[MONTANT_CAUTION] [numeric](9, 2) NULL ,
	[MONTANT_PRET] [numeric](9, 2) NULL ,
	[TOTAL_NET] [numeric](12, 2) NULL ,
	[MONTANTS_DIVERS] [numeric](12, 2) NULL ,
	[COMM_ZURICH] [numeric](12, 2) NULL ,
	[COMM_ALPINA] [numeric](12, 2) NULL ,
	[COMM_ORION] [numeric](12, 2) NULL ,
	[COMM_RETROCEDEE] [numeric](12, 2) NULL ,
	[COMM_INTRAS] [numeric](12, 2) NULL ,
	[COMM_LOMBARD] [numeric](12, 2) NULL ,
	[SOLDE_GARANTIE] [numeric](9, 2) NULL ,
	[TRANSF_GENERALE] [numeric](12, 2) NULL ,
	[TOTAL_BRUT] [numeric](12, 2) NULL ,
	[M_SCUDDER] [numeric](12, 2) NULL ,
	[M_ZH_INVEST] [numeric](12, 2) NULL ,
	[M_GE_INVEST] [numeric](12, 2) NULL ,
	[EMMENTALISCHE] [numeric](12, 2) NULL ,
	[AXA] [numeric](12, 2) NULL ,
	[POOL] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[REMUNERATION_VIE_IND] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ORDRE] [smallint] NOT NULL ,
	[DATE_COMPTABLE] [datetime] NOT NULL ,
	[TYPE_MOUVEMENT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[MONTANT_REMUNERATION] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RENDEMENT_MALADIE_COLLECTIVE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[ANNEE_RENDEMENT] [smallint] NOT NULL ,
	[TYPE_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL ,
	[MONTANT_SINISTRES] [numeric](12, 2) NULL ,
	[MONTANT_PARTICIPATION] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RENDEMENT_POLICES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[ANNEE] [smallint] NOT NULL ,
	[RENDEMENT] [int] NULL ,
	[TOTAL_PRIMES] [numeric](12, 2) NULL ,
	[NB_SINISTRES] [smallint] NULL ,
	[TOTAL_SINISTRES] [numeric](12, 2) NULL ,
	[TOTAL_RESERVES] [numeric](12, 2) NULL ,
	[TOTAL_PARTICIPATIONS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RENTES_VIVC] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [smallint] NOT NULL ,
	[NATURE_RENTE_DWH] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[MONTANT_EMIS] [numeric](9, 2) NULL ,
	[MOIS_EMISSION] [tinyint] NOT NULL ,
	[ANNEE_EMISSION] [smallint] NOT NULL ,
	[CODE_MONNAIE] [tinyint] NOT NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[REMARQUE] [varchar] (20) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RENTES_VIVC_HIST] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [smallint] NOT NULL ,
	[NATURE_RENTE_DWH] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[MONTANT_EMIS] [numeric](9, 2) NULL ,
	[IMPOT] [numeric](7, 2) NULL ,
	[DIVIDENDE] [numeric](7, 2) NULL ,
	[MOIS_EMISSION] [tinyint] NOT NULL ,
	[ANNEE_EMISSION] [smallint] NOT NULL ,
	[CODE_MODE_FRACT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[MOIS_DEBUT_RENTE] [tinyint] NULL ,
	[ANNEE_DEBUT_RENTE] [smallint] NULL ,
	[ANNEE_TARIF] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RENTES_VIVC_intermed] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [smallint] NOT NULL ,
	[NATURE_RENTE_DWH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[MONTANT_EMIS] [numeric](9, 2) NULL ,
	[MOIS_EMISSION] [tinyint] NOT NULL ,
	[ANNEE_EMISSION] [smallint] NOT NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RENTES_VIVC_intermed_original] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [smallint] NOT NULL ,
	[NATURE_RENTE_DWH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_NAT_ECR] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[MONTANT_EMIS] [numeric](9, 2) NULL ,
	[MOIS_EMISSION] [tinyint] NOT NULL ,
	[ANNEE_EMISSION] [smallint] NOT NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MOIS_ECHEANCE] [tinyint] NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RENTES_VI_PERMNONPERM] (
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NOM] [varchar] (50) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (50) COLLATE fr_cs_as NULL ,
	[DEBUT_PREST] [datetime] NULL ,
	[TAUX] [float] NULL ,
	[M_RENTE] [numeric](12, 2) NOT NULL ,
	[PERM_NONPERM] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[COMPTEUR] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RESERVES_PREST_INVALIDITE] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[CIE_REASS] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [smallint] NOT NULL ,
	[AGE_SURVENANCE] [tinyint] NOT NULL ,
	[AGE_TERME] [tinyint] NOT NULL ,
	[DELAI] [tinyint] NULL ,
	[DUREE] [numeric](3, 1) NULL ,
	[TAUX_PRESTATION] [numeric](4, 1) NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_SURVENANCE] [datetime] NULL ,
	[MONTANT_COUVERTURE] [numeric](12, 1) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 1) NULL ,
	[MONTANT_RESERVE] [numeric](12, 1) NULL ,
	[CODE_MONNAIE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RESERVES_VIE_COLLECTIVE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[DATE_COMPTABLE] [datetime] NOT NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[TAUX_COASSURANCE] [numeric](5, 2) NULL ,
	[RESERVE_MATHEMATIQUE] [numeric](12, 0) NULL ,
	[RESERVE_MATHEMATIQUE_POOL] [numeric](12, 0) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[REPORT_PRIME_POOL] [numeric](12, 2) NULL ,
	[CODE_TYPE_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RES_RI_VIE_COLLECTIVE] (
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[C_SEXE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[D_NAISS] [datetime] NOT NULL ,
	[N_TAUX_ACT] [numeric](7, 2) NULL ,
	[N_TAUX_INV] [numeric](7, 2) NOT NULL ,
	[C_TRAITE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[K_GENRE_ASS] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[DELAI_ATT] [tinyint] NULL ,
	[N_MTRENTE] [numeric](12, 2) NOT NULL ,
	[D_SIN] [datetime] NULL ,
	[D_DEBPREST] [datetime] NOT NULL ,
	[D_ECHPREST] [datetime] NOT NULL ,
	[N_RESMATH] [numeric](12, 2) NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RISQUES_VEHICULE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[TYPE_OBJET] [tinyint] NOT NULL ,
	[NO_OBJET] [tinyint] NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[STATUT_RISQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_BONUS_MALUS] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[RISQUES_VEHICULE_TMP] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[TYPE_OBJET] [tinyint] NOT NULL ,
	[NO_OBJET] [tinyint] NOT NULL ,
	[NO_RISQUE] [tinyint] NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[STATUT_RISQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_BONUS_MALUS] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SAP_PRESTATIONS_VI] (
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[NO_COMPTE] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[DATE_VALEUR] [datetime] NOT NULL ,
	[NATURE_SINISTRE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[MONTANT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SC_Année_inventaire] (
	[Année] [smallint] NOT NULL ,
	[Mois] [tinyint] NOT NULL ,
	[date_fin] [char] (6) COLLATE fr_cs_as NOT NULL ,
	[Date_inventaire] [datetime] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SC_T_Date_limite] (
	[Année] [smallint] NOT NULL ,
	[Mois] [tinyint] NOT NULL ,
	[Date_limite] [datetime] NULL ,
	[Mois_limite] [char] (6) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SC_as7i_inv_result] (
	[TYP_MVT] [char] (1) COLLATE fr_cs_as NULL ,
	[NO_FNDS] [int] NULL ,
	[NO_SIT_FNDS] [int] NULL ,
	[NO_MVT_FNDS] [int] NOT NULL ,
	[NO_VAL_FNDS] [char] (10) COLLATE fr_cs_as NULL ,
	[T_NOM_FNDS] [varchar] (60) COLLATE fr_cs_as NULL ,
	[T_MN_COT_FNDS] [char] (3) COLLATE fr_cs_as NULL ,
	[D_CAT_MVT_FNDS] [datetime] NULL ,
	[N_PART_FNDS] [numeric](11, 5) NULL ,
	[Z_ACH_FNDS] [numeric](9, 4) NULL ,
	[M_ACH_FNDS] [numeric](11, 2) NULL ,
	[Z_CHGE_ACH] [numeric](15, 9) NULL ,
	[NO_SIT_FAA] [int] NULL ,
	[R_AGCE_N] [int] NULL ,
	[R_AGNT] [int] NULL ,
	[D_EFF_FAA] [datetime] NULL ,
	[NO_PLAN_INVS] [int] NULL ,
	[D_ADHS_PLAN] [datetime] NULL ,
	[NO_AFFR_FRNS] [varchar] (20) COLLATE fr_cs_as NULL ,
	[Z_CHGE_CEMS] [numeric](15, 9) NULL ,
	[M_COT_FNDS] [numeric](11, 2) NULL ,
	[M_CHF_FNDS] [numeric](11, 2) NULL ,
	[NO_FNDS_SCUD] [char] (3) COLLATE fr_cs_as NULL ,
	[NO_FNDS_ZURICH] [char] (3) COLLATE fr_cs_as NULL ,
	[Année_catal] [int] NULL ,
	[Mois_catal] [int] NULL ,
	[Année] [int] NULL ,
	[Mois] [int] NULL ,
	[MOIS_INV] [char] (6) COLLATE fr_cs_as NULL ,
	[MOIS_D_CATAL] [char] (6) COLLATE fr_cs_as NULL ,
	[LIBELLE_MVT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[Taux] [numeric](15, 9) NULL ,
	[C_FACT_NGTN] [char] (1) COLLATE fr_cs_as NULL ,
	[D_VAL_MVT_FNDS] [datetime] NULL ,
	[TAUX_VAL_FNDS] [numeric](15, 9) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SC_as7i_mvt_result] (
	[TYP_MVT] [char] (1) COLLATE fr_cs_as NULL ,
	[NO_MVT_FNDS] [int] NOT NULL ,
	[NO_AFFR_FRNS] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_FNDS] [int] NULL ,
	[D_ACQS_FNDS] [datetime] NULL ,
	[NO_VAL_FNDS] [char] (10) COLLATE fr_cs_as NULL ,
	[NO_SIT_FNDS] [int] NULL ,
	[D_ADHS_PLAN] [datetime] NULL ,
	[NO_PLAN_INVS] [int] NULL ,
	[D_CAT_MVT_FNDS] [datetime] NULL ,
	[D_VAL_MVT_FNDS] [datetime] NULL ,
	[N_PART_FNDS] [numeric](11, 5) NULL ,
	[M_COT_FNDS] [numeric](11, 2) NULL ,
	[T_MN_COT_FNDS] [char] (3) COLLATE fr_cs_as NULL ,
	[M_FRAI_VNT] [numeric](5, 2) NULL ,
	[M_CHF_FNDS] [numeric](11, 2) NULL ,
	[NO_SIT_FAA] [int] NULL ,
	[D_EFF_FAA] [datetime] NULL ,
	[M_BASE_CEMS] [numeric](11, 2) NULL ,
	[M_CEMS] [numeric](11, 2) NULL ,
	[D_REMU_CEMS] [datetime] NULL ,
	[R_AGCE_N] [int] NULL ,
	[R_AGNT] [int] NULL ,
	[Mois_catal] [int] NULL ,
	[Année_catal] [int] NULL ,
	[NO_FNDS_SCUD] [char] (3) COLLATE fr_cs_as NULL ,
	[NO_FNDS_ZURICH] [char] (3) COLLATE fr_cs_as NULL ,
	[Mois_inv] [char] (6) COLLATE fr_cs_as NULL ,
	[Mois_d_catal] [char] (6) COLLATE fr_cs_as NULL ,
	[Mois_d_val] [char] (6) COLLATE fr_cs_as NULL ,
	[NO_MAA] [int] NULL ,
	[C_FACT_NGTN] [char] (2) COLLATE fr_cs_as NULL ,
	[Z_CHGE_CEMS] [numeric](15, 9) NULL ,
	[Date_limite] [datetime] NULL ,
	[T_MN_INVS] [char] (3) COLLATE fr_cs_as NULL ,
	[Date_inventaire] [datetime] NULL ,
	[M_BASE_INVS] [numeric](11, 2) NULL ,
	[Z_CHGE_INVS] [numeric](15, 9) NULL ,
	[M_INVS_COMM] [numeric](11, 2) NULL ,
	[D_REMU_INVS] [datetime] NULL ,
	[M_DRT_TBR] [numeric](11, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SC_as7i_mvt_result_stat_rohrer] (
	[TYP_MVT] [char] (1) COLLATE fr_cs_as NULL ,
	[NO_MVT_FNDS] [int] NOT NULL ,
	[Région] [char] (2) COLLATE fr_cs_as NULL ,
	[NO_FETA] [varchar] (10) COLLATE fr_cs_as NULL ,
	[NO_FNDS] [int] NULL ,
	[NO_VAL_FNDS] [char] (10) COLLATE fr_cs_as NULL ,
	[NO_PLAN_INVS] [int] NULL ,
	[PROG_INVEST] [char] (4) COLLATE fr_cs_as NULL ,
	[D_VAL_MVT_FNDS] [datetime] NULL ,
	[N_PART_FNDS] [numeric](11, 5) NULL ,
	[M_COT_FNDS] [numeric](11, 2) NULL ,
	[T_MN_COT_FNDS] [char] (3) COLLATE fr_cs_as NULL ,
	[M_CHF_FNDS_UNIQUE] [numeric](11, 2) NULL ,
	[M_CHF_FNDS_PERIODIQUE] [numeric](11, 2) NULL ,
	[M_BASE_CEMS] [numeric](11, 2) NULL ,
	[M_CEMS] [numeric](11, 2) NULL ,
	[Taux_prime] [numeric](6, 3) NULL ,
	[D_REMU_CEMS] [datetime] NULL ,
	[R_AGCE_N] [int] NULL ,
	[Nom_Agence] [varchar] (12) COLLATE fr_cs_as NULL ,
	[R_AGNT] [int] NULL ,
	[Nom_agent] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_MAA] [int] NULL ,
	[Z_CHGE_CEMS] [numeric](15, 9) NULL ,
	[C_FACT_NGTN] [char] (1) COLLATE fr_cs_as NULL ,
	[Date_limite_catal] [datetime] NULL ,
	[Mois_inv] [char] (6) COLLATE fr_cs_as NULL ,
	[Mois_d_catal] [char] (6) COLLATE fr_cs_as NULL ,
	[T_MN_INVS] [char] (3) COLLATE fr_cs_as NULL ,
	[T_NOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PNOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[Type] [char] (1) COLLATE fr_cs_as NULL ,
	[Année_catal] [int] NULL ,
	[Mois_catal] [int] NULL ,
	[C_NO_POSTAL] [char] (5) COLLATE fr_cs_as NULL ,
	[T_RSN_SOC_PRINC] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_RSN_SOC_SECD] [varchar] (30) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SC_t_mvt_stat_zurich] (
	[TYP_MVT] [char] (1) COLLATE fr_cs_as NULL ,
	[NO_MVT_FNDS] [int] NOT NULL ,
	[NO_AFFR_FRNS] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_FNDS] [int] NULL ,
	[D_ACQS_FNDS] [datetime] NULL ,
	[NO_VAL_FNDS] [char] (10) COLLATE fr_cs_as NULL ,
	[NO_SIT_FNDS] [int] NULL ,
	[D_ADHS_PLAN] [datetime] NULL ,
	[NO_PLAN_INVS] [int] NULL ,
	[D_CAT_MVT_FNDS] [datetime] NULL ,
	[D_VAL_MVT_FNDS] [datetime] NULL ,
	[N_PART_FNDS] [numeric](11, 5) NULL ,
	[M_COT_FNDS] [numeric](11, 2) NULL ,
	[T_MN_COT_FNDS] [char] (3) COLLATE fr_cs_as NULL ,
	[M_FRAI_VNT] [numeric](5, 2) NULL ,
	[M_CHF_FNDS] [numeric](11, 2) NULL ,
	[NO_SIT_FAA] [int] NULL ,
	[D_EFF_FAA] [datetime] NULL ,
	[M_BASE_CEMS] [numeric](11, 2) NULL ,
	[M_CEMS] [numeric](11, 2) NULL ,
	[D_REMU_CEMS] [datetime] NULL ,
	[R_AGCE_N] [int] NULL ,
	[R_AGNT] [int] NULL ,
	[Mois_catal] [int] NULL ,
	[Année_catal] [int] NULL ,
	[NO_FNDS_SCUD] [char] (3) COLLATE fr_cs_as NULL ,
	[NO_FNDS_ZURICH] [char] (3) COLLATE fr_cs_as NULL ,
	[Mois_inv] [char] (6) COLLATE fr_cs_as NULL ,
	[Mois_d_catal] [char] (6) COLLATE fr_cs_as NULL ,
	[Mois_d_val] [char] (6) COLLATE fr_cs_as NULL ,
	[NO_MAA] [int] NULL ,
	[C_FACT_NGTN] [char] (2) COLLATE fr_cs_as NULL ,
	[Z_CHGE_CEMS] [numeric](15, 9) NULL ,
	[Date_limite] [datetime] NULL ,
	[T_MN_INVS] [char] (3) COLLATE fr_cs_as NULL ,
	[Date_inventaire] [datetime] NULL ,
	[M_BASE_INVS] [numeric](11, 2) NULL ,
	[Z_CHGE_INVS] [numeric](15, 9) NULL ,
	[M_INVS_COMM] [numeric](11, 2) NULL ,
	[D_REMU_INVS] [datetime] NULL ,
	[M_DRT_TBR] [numeric](11, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_ACLA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[CODE_SEXE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_AC_AUTRES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_AI_CIRCULATION] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[CODE_SEXE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_AI_PERSONNES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[CODE_SEXE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_AI_VOYAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[CODE_SEXE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_BATIMENT_ENT] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_BATIMENT_IND] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_CASCO_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[CODE_PENALITE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_CASCO_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_IARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[NO_SINISTRE] [char] (19) COLLATE fr_cs_as NOT NULL ,
	[MOIS_EXERCICE] [tinyint] NOT NULL ,
	[ANNEE_EXERCICE] [smallint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 4) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 4) NULL ,
	[CODE_DIVERS] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_INV_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_INV_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_LAA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[CODE_SEXE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_MC06_MIGRES_WIDMER] (
	[FALL] [int] NOT NULL ,
	[SNR_ZUR] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[SNR_GEN] [varchar] (30) COLLATE fr_cs_as NOT NULL ,
	[POLICE_ZUR] [int] NULL ,
	[VN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[ASSURE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[ZUST_PERS] [varchar] (50) COLLATE fr_cs_as NULL ,
	[ERF_DAT] [datetime] NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[NO_SINISTRE] [int] NOT NULL ,
	[NO_SINISTRE_IC] [char] (19) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_OBJET_VALEUR] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_RC_AGRICOLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_RC_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_RC_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_RC_IMMEUBLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_RC_PRIVEE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_RC_PROFESSIONNELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SINISTRES_VEHICULE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[NO_CONTRAT] [tinyint] NOT NULL ,
	[BRANCHE] [tinyint] NOT NULL ,
	[DATE_SURVENANCE] [datetime] NOT NULL ,
	[NO_GESTIONNAIRE] [tinyint] NOT NULL ,
	[CODE_LIQUIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[MOIS_LIQUIDATION] [tinyint] NULL ,
	[ANNEE_LIQUIDATION] [smallint] NULL ,
	[MOIS_EXERCICE] [tinyint] NULL ,
	[ANNEE_EXERCICE] [smallint] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[CODE_REASSURANCE] [tinyint] NULL ,
	[TAUX_REASSURANCE] [tinyint] NULL ,
	[RESERVES] [numeric](12, 2) NULL ,
	[NOMBRE_PAIEMENTS] [smallint] NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[CODE_PENALITE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SOLDES] (
	[DATE_EXTRAC] [datetime] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[D_CMPT_ECR] [datetime] NOT NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[R_POL] [int] NULL ,
	[C_TYP_REF] [char] (4) COLLATE fr_cs_as NULL ,
	[M_MNT] [numeric](12, 2) NULL ,
	[M_NONRGL] [numeric](12, 2) NULL ,
	[L_FONDATION] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL ,
	[C_PR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_VALEUR] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SOLDES_COMPTES] (
	[DATE_SOLDE] [datetime] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[R_POL] [int] NULL ,
	[C_TYP_REF] [char] (4) COLLATE fr_cs_as NULL ,
	[M_SOLDE] [numeric](12, 2) NULL ,
	[L_FONDATION] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[SOLDES_MELAN] (
	[DATE_EXTRAC] [datetime] NOT NULL ,
	[NO_CPT] [int] NOT NULL ,
	[NO_ECR] [int] NOT NULL ,
	[D_CMPT_ECR] [datetime] NOT NULL ,
	[C_TYP_CPT] [varchar] (3) COLLATE fr_cs_as NULL ,
	[T_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[R_POL] [int] NULL ,
	[C_TYP_REF] [char] (4) COLLATE fr_cs_as NULL ,
	[M_MNT] [numeric](12, 2) NULL ,
	[M_NONRGL] [numeric](12, 2) NULL ,
	[L_FONDATION] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_PRD] [smallint] NULL ,
	[C_PR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_VALEUR] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[STATISTIQUE_PRODUCTION] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR_COUCHE] [smallint] NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_SITUATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[ANNEE_CATALOGUE] [smallint] NULL ,
	[MOIS_CATALOGUE] [tinyint] NULL ,
	[CODE_GROUPE_1] [tinyint] NULL ,
	[CODE_GROUPE_2] [tinyint] NULL ,
	[CODE_GROUPE_3] [tinyint] NULL ,
	[CODE_GROUPE_4] [tinyint] NULL ,
	[CODE_GROUPE_5] [tinyint] NULL ,
	[CODE_GROUPE_6] [tinyint] NULL ,
	[MOUVEMENT_PRIME] [numeric](12, 2) NULL ,
	[CODE_STAT] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[STATISTIQUE_PRODUCTION_VC04] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR_COUCHE] [smallint] NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_SITUATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[ANNEE_CATALOGUE] [smallint] NULL ,
	[MOIS_CATALOGUE] [tinyint] NULL ,
	[CODE_GROUPE_1] [tinyint] NULL ,
	[CODE_GROUPE_2] [tinyint] NULL ,
	[CODE_GROUPE_3] [tinyint] NULL ,
	[CODE_GROUPE_4] [tinyint] NULL ,
	[CODE_GROUPE_5] [tinyint] NULL ,
	[CODE_GROUPE_6] [tinyint] NULL ,
	[MOUVEMENT_PRIME] [numeric](12, 2) NULL ,
	[MOUVEMENT_PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[CODE_STAT] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[STAT_ACT_MUTATIONS] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[CODE_MUTATION] [smallint] NOT NULL ,
	[LIBELLE_COURT] [varchar] (12) COLLATE fr_cs_as NULL ,
	[LIBELLE_LONG] [varchar] (50) COLLATE fr_cs_as NULL ,
	[CODE_RECAP_IV] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CODE_RECAP_ACT] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CODE_PRODUCTION] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[STAT_PROD_BRANCHES] (
	[CODE_GROUPE_1] [tinyint] NOT NULL ,
	[CODE_GROUPE_2] [tinyint] NOT NULL ,
	[CODE_GROUPE_3] [tinyint] NOT NULL ,
	[CODE_GROUPE_4] [tinyint] NOT NULL ,
	[CODE_GROUPE_5] [tinyint] NOT NULL ,
	[BRANCHE] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CATEGORIE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[ORDRE] [tinyint] NULL ,
	[LIBELLE] [varchar] (30) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[STAT_PROD_LIBELLES] (
	[CODE_STATISTIQUE] [tinyint] NOT NULL ,
	[LIBELLE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[CODE_DWH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[LIBELLE_DWH] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[STAT_PROD_MUTATIONS] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[CODE_MUTATION] [smallint] NOT NULL ,
	[CODE_STATISTIQUE] [tinyint] NULL ,
	[LIBELLE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[ZCAUSE_ANNULATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_RESIL_VC] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[STRUCT_PORTEFEUILLE_ZH] (
	[DATNEU] [int] NOT NULL ,
	[POL_SGE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[VERTRIEBSK] [smallint] NULL ,
	[MARKTG] [int] NULL ,
	[GESCHAFTSST] [varchar] (10) COLLATE fr_cs_as NULL ,
	[AGENTURNR] [int] NULL ,
	[EVST] [int] NULL ,
	[ADM] [int] NULL ,
	[NAME_ADM] [varchar] (100) COLLATE fr_cs_as NULL ,
	[CB] [varchar] (10) COLLATE fr_cs_as NULL ,
	[LOB0] [varchar] (10) COLLATE fr_cs_as NULL ,
	[VZ2] [varchar] (10) COLLATE fr_cs_as NULL ,
	[VERTRIEBSKANAL] [int] NULL ,
	[POLICENNR] [varchar] (100) COLLATE fr_cs_as NOT NULL ,
	[POLICEN_NAMEN] [varchar] (150) COLLATE fr_cs_as NULL ,
	[KUNDE] [varchar] (150) COLLATE fr_cs_as NULL ,
	[PLZ] [varchar] (150) COLLATE fr_cs_as NULL ,
	[ORT] [varchar] (150) COLLATE fr_cs_as NULL ,
	[ABLAUF_DATUM] [varchar] (12) COLLATE fr_cs_as NULL ,
	[TARPR] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_AGENCES] (
	[AGENCE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[LAGENCE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ZONE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CGEO] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_BRANCHES] (
	[TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[L_BRANCHE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_SOC] [varchar] (1) COLLATE fr_cs_as NULL ,
	[GERE] [bit] NOT NULL ,
	[TAUX_COMM] [numeric](7, 4) NULL ,
	[E01] [bit] NOT NULL ,
	[E02] [bit] NOT NULL ,
	[E03] [bit] NOT NULL ,
	[E04] [bit] NOT NULL ,
	[E05] [bit] NOT NULL ,
	[E06] [bit] NOT NULL ,
	[E07] [bit] NOT NULL ,
	[E08] [bit] NOT NULL ,
	[E09] [bit] NOT NULL ,
	[E10] [bit] NOT NULL ,
	[E11] [bit] NOT NULL ,
	[E12] [bit] NOT NULL ,
	[E13] [bit] NOT NULL ,
	[E14] [bit] NOT NULL ,
	[E15] [bit] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_COMM_ADMIN_T08] (
	[C_NO_EXTRAT] [int] NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NO_REF] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[C_AGCE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_AGNT] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_HIERAR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[Z_MONTANT] [real] NULL ,
	[Z_REGUL] [real] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_COMM_COURTAGE_MAN] (
	[C_NO_EXTRAT] [int] NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NO_REF] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[C_MOUV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_MOUV] [datetime] NOT NULL ,
	[C_SEQ] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[Z_MONTANT] [real] NULL ,
	[C_AGCE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_AGNT] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_COURT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[Z_TAUX] [numeric](6, 3) NULL ,
	[Z_COM] [numeric](12, 2) NULL ,
	[Z_COM_VER] [real] NULL ,
	[T_NOM_AGNT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PRENOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_USER] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_COMM_COURTAGE_MAN_20080624_BAK] (
	[C_NO_EXTRAT] [int] NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NO_REF] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[C_MOUV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_MOUV] [datetime] NOT NULL ,
	[C_SEQ] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[Z_MONTANT] [real] NULL ,
	[C_AGCE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_AGNT] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_COURT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[Z_TAUX] [real] NULL ,
	[Z_COM] [real] NULL ,
	[Z_COM_VER] [real] NULL ,
	[T_NOM_AGNT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PRENOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_USER] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_COMM_COURTAGE_T14] (
	[C_NO_EXTRAT] [int] NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NO_REF] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[C_MOUV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_MOUV] [datetime] NOT NULL ,
	[C_SEQ] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[Z_MONTANT] [real] NULL ,
	[C_AGCE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_AGNT] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_COURT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[Z_TAUX] [real] NULL ,
	[Z_COM] [real] NULL ,
	[Z_COM_VER] [real] NULL ,
	[T_NOM_AGNT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PRENOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_USER] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_COMM_COURTAGE_T14_MELAN] (
	[C_NO_EXTRAT] [int] NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NO_REF] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[C_MOUV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_MOUV] [datetime] NOT NULL ,
	[C_SEQ] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[Z_MONTANT] [real] NULL ,
	[C_AGCE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_AGNT] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_COURT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[Z_TAUX] [real] NULL ,
	[Z_COM] [real] NULL ,
	[Z_COM_VER] [real] NULL ,
	[T_NOM_AGNT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_NOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PRENOM_PREN] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_USER] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_COURTIERS_T14] (
	[NO_AGENCE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[LAGENCE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_AGT1] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_AGT2] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[T_NOM_AGNT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[VIE_IND] [real] NULL ,
	[MAL_IND] [real] NULL ,
	[MAL_COLL] [real] NULL ,
	[ACC_IND] [real] NULL ,
	[LAA] [real] NULL ,
	[RC_GN] [real] NULL ,
	[CHOSES] [real] NULL ,
	[VC_E1] [real] NULL ,
	[VC_E2] [real] NULL ,
	[VC_E3] [real] NULL ,
	[VC_E4] [real] NULL ,
	[VC_R1] [real] NULL ,
	[VC_R2] [real] NULL ,
	[VC_R3] [real] NULL ,
	[VC_R4] [real] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_DATES_DISPONIBLES] (
	[C_NO_EXTRAT] [int] NOT NULL ,
	[L_DATE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_DATE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[DISPONIBLE] [bit] NOT NULL ,
	[D_DEB] [datetime] NULL ,
	[D_FIN] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_DETMI05_T11] (
	[C_NO_EXTRAT] [int] NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NO_REF] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[D_MOUV] [datetime] NOT NULL ,
	[C_SEQ] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_GROUPE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_COUV] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_TAR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[D_TAR_AA] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[Z_PRIM_BASE] [real] NULL ,
	[Z_SUPPL_ACC] [real] NULL ,
	[Z_SUPPL_ACC_HCL] [real] NULL ,
	[Z_SUPPL_AGGR] [real] NULL ,
	[Z_RAB_CURE] [real] NULL ,
	[Z_RAB_FAMI] [real] NULL ,
	[Z_RAB_COLL] [real] NULL ,
	[Z_SUPPL_FRAC] [real] NULL ,
	[R_ECR] [smallint] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_MVTS_T00] (
	[C_MOIS] [int] NOT NULL ,
	[F_DEB] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[F_CRE] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[F_IMP] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[F_AVC] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[R_POL] [varchar] (7) COLLATE fr_cs_as NOT NULL ,
	[D_MVT] [datetime] NOT NULL ,
	[C_SEQ] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_MVT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[D_CPT] [datetime] NULL ,
	[C_MOUV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_TYPE_MVT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_GR_MI] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_SOC] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_SEG] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_DEVISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_TYP_CPT] [varchar] (5) COLLATE fr_cs_as NULL ,
	[C_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_POL_VS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_CANT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_ED_TAR] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_TYP_ANNUL] [varchar] (3) COLLATE fr_cs_as NULL ,
	[R_NO_PREN] [varchar] (9) COLLATE fr_cs_as NULL ,
	[C_AGCE_ZA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_AGNT_ZA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_AGCE_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_AGNT_ZC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[Z_MONTANT] [numeric](12, 2) NULL ,
	[Z_OLD_IMP] [numeric](12, 2) NULL ,
	[Z_OLD_AVC] [numeric](12, 2) NULL ,
	[Z_DEB] [numeric](12, 2) NULL ,
	[Z_CRE] [numeric](12, 2) NULL ,
	[Z_SOLDE] [numeric](12, 2) NULL ,
	[Z_NEW_IMP] [numeric](12, 2) NULL ,
	[Z_NEW_AVC] [numeric](12, 2) NULL ,
	[C_QUARTET] [varchar] (1) COLLATE fr_cs_as NULL ,
	[D_DEB_FACT] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_MVTS_T00_MELAN] (
	[C_MOIS] [int] NOT NULL ,
	[F_DEB] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[F_CRE] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[F_IMP] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[F_AVC] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[C_TYPE_REF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[R_POL] [varchar] (7) COLLATE fr_cs_as NOT NULL ,
	[D_MVT] [datetime] NOT NULL ,
	[C_SEQ] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[C_MVT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[D_CPT] [datetime] NULL ,
	[C_MOUV] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_TYPE_MVT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_GR_MI] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_SOC] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_SEG] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_DEVISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_TYP_CPT] [varchar] (5) COLLATE fr_cs_as NULL ,
	[C_MN_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[C_POL_VS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_CANT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_ED_TAR] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_TYP_ANNUL] [varchar] (3) COLLATE fr_cs_as NULL ,
	[R_NO_PREN] [varchar] (9) COLLATE fr_cs_as NULL ,
	[C_AGCE_ZA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_AGNT_ZA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_AGCE_ZC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_AGNT_ZC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[Z_MONTANT] [numeric](12, 2) NULL ,
	[Z_OLD_IMP] [numeric](12, 2) NULL ,
	[Z_OLD_AVC] [numeric](12, 2) NULL ,
	[Z_DEB] [numeric](12, 2) NULL ,
	[Z_CRE] [numeric](12, 2) NULL ,
	[Z_SOLDE] [numeric](12, 2) NULL ,
	[Z_NEW_IMP] [numeric](12, 2) NULL ,
	[Z_NEW_AVC] [numeric](12, 2) NULL ,
	[C_QUARTET] [varchar] (1) COLLATE fr_cs_as NULL ,
	[D_DEB_FACT] [datetime] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_NATURE_MOUVEMENTS] (
	[C_NATURE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[LIB_COURT] [varchar] (12) COLLATE fr_cs_as NULL ,
	[LIB_LONG] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_TYPE_MOUVEMENTS] (
	[TYPE_MVT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[LIB_COURT] [varchar] (12) COLLATE fr_cs_as NULL ,
	[LIB_LONG] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[S_VcComBroker] (
	[AN] [smallint] NOT NULL ,
	[D_LOAD] [datetime] NOT NULL ,
	[R_AGCE_ZA] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[LAGENCE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[R_AGNT_ZA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[T_NOM_AGNT] [varchar] (50) COLLATE fr_cs_as NULL ,
	[R_POL] [int] NOT NULL ,
	[GENCOUV] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[PREN] [varchar] (50) COLLATE fr_cs_as NULL ,
	[MNT_FPA] [numeric](12, 2) NOT NULL ,
	[SIGNE] [smallint] NOT NULL ,
	[MNT_ABS] [numeric](12, 2) NOT NULL ,
	[TAUX] [numeric](6, 4) NOT NULL ,
	[MNT_A_COM] [numeric](12, 2) NOT NULL ,
	[COM] [numeric](12, 2) NOT NULL ,
	[COMPTEUR] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[TEMP_INV_TRIM_VI03] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[STATUT] [tinyint] NULL ,
	[MODE_PARTICIPATION] [tinyint] NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_PRIME_FRACT] [tinyint] NULL ,
	[DUREE_PRIME] [smallint] NULL ,
	[CODE_RENTE_FRACT] [tinyint] NULL ,
	[DUREE_RENTE] [smallint] NULL ,
	[GARANTIE_RENTE] [tinyint] NULL ,
	[PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[MONTANT_RENTE] [numeric](12, 2) NULL ,
	[PRIME_RESERVE] [numeric](12, 2) NULL ,
	[RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[RESERVE_FIN] [numeric](12, 2) NULL ,
	[RESERVE_INT] [numeric](12, 2) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[CODE_MONNAIE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[TEMP_INV_TRIM_VI03_RENF] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[STATUT] [tinyint] NULL ,
	[MODE_PARTICIPATION] [tinyint] NULL ,
	[DATE_DEBUT_TECHNIQUE] [datetime] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[CODE_PRIME_FRACT] [tinyint] NULL ,
	[DUREE_PRIME] [smallint] NULL ,
	[CODE_RENTE_FRACT] [tinyint] NULL ,
	[DUREE_RENTE] [smallint] NULL ,
	[GARANTIE_RENTE] [tinyint] NULL ,
	[PRIME_ANNUELLE] [numeric](12, 2) NULL ,
	[PRIME_UNIQUE] [numeric](12, 2) NULL ,
	[MONTANT_RENTE] [numeric](12, 2) NULL ,
	[PRIME_RESERVE] [numeric](12, 2) NULL ,
	[RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[RESERVE_FIN] [numeric](12, 2) NULL ,
	[RESERVE_INT] [numeric](12, 2) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[CODE_MONNAIE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[TEMP_INV_TRIM_VI07] (
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_DEBUT_TECHNIQUE] [char] (7) COLLATE fr_cs_as NOT NULL ,
	[DATE_FIN_TECHNIQUE] [char] (7) COLLATE fr_cs_as NOT NULL ,
	[AGE] [tinyint] NOT NULL ,
	[CODE_SEXE] [tinyint] NOT NULL ,
	[COUVERTURE_DECES] [numeric](9, 0) NULL ,
	[RESERVE_RISQUE_DEBUT] [numeric](9, 0) NULL ,
	[RESERVE_RISQUE_FIN] [numeric](9, 0) NULL ,
	[RESERVE_RISQUE_INT] [numeric](9, 0) NULL ,
	[RESERVE_FRAIS_DEBUT] [numeric](9, 0) NULL ,
	[RESERVE_FRAIS_FIN] [numeric](9, 0) NULL ,
	[RESERVE_FRAIS_INT] [numeric](9, 0) NULL ,
	[PRIME_EPARGNE] [numeric](9, 0) NULL ,
	[RESERVE_EPARGNE] [numeric](9, 0) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[TEMP_INV_TRIM_VIE_INDIVIDUELLE] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_TARIF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MODE_PARTICIPATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_MODE_FRACT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[DATE_FIN_PRIME] [datetime] NULL ,
	[CODE_MONNAIE] [tinyint] NULL ,
	[AGE] [tinyint] NULL ,
	[CODE_SEXE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_COUV_DECES] [numeric](12, 2) NULL ,
	[MONTANT_COUV_VIE] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_TARIF] [numeric](12, 2) NULL ,
	[MONTANT_PRIME_RESERVE] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_DEBUT] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_FIN] [numeric](12, 2) NULL ,
	[MONTANT_RESERVE_INT] [numeric](12, 2) NULL ,
	[MONTANT_REPORT] [numeric](12, 2) NULL ,
	[NO_TRIMESTRE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[TEMP_INV_TRIM_VIRAC] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [smallint] NULL ,
	[VALEUR_RACHAT] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[TEMP_RESERVES_PREST_INVALIDITE] (
	[MOIS_TRIMESTRE] [tinyint] NOT NULL ,
	[ANNEE_TRIMESTRE] [smallint] NOT NULL ,
	[CIE_REASS] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[EDITION_TARIFAIRE] [smallint] NOT NULL ,
	[AGE_SURVENANCE] [tinyint] NOT NULL ,
	[AGE_TERME] [tinyint] NOT NULL ,
	[DELAI] [tinyint] NULL ,
	[DUREE] [numeric](3, 1) NULL ,
	[TAUX_PRESTATION] [numeric](4, 1) NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_SURVENANCE] [datetime] NULL ,
	[MONTANT_COUVERTURE] [numeric](12, 1) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 1) NULL ,
	[MONTANT_RESERVE] [numeric](12, 1) NULL ,
	[CODE_MONNAIE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[TEXTES_MALADIE_INDIVIDUELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_LIGNE] [tinyint] NOT NULL ,
	[TEXTE] [varchar] (63) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_ACLA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[ETAT_GARANTIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[TAUX_COASSURANCE] [numeric](7, 3) NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[NO_RISQUE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CLASSE_RISQUE] [smallint] NULL ,
	[DEGRE_RISQUE] [smallint] NULL ,
	[CODE_EXCEPTION] [tinyint] NULL ,
	[NOMBRE_PERSONNES] [smallint] NULL ,
	[SALAIRE_TOTAL] [int] NULL ,
	[TAUX_TOTAL] [numeric](7, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_AC_AUTRES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_ECHEANCE] [smallint] NULL ,
	[NOMBRE_PERSONNES] [smallint] NULL ,
	[SALAIRE_TOTAL] [int] NULL ,
	[TAUX_TOTAL] [numeric](7, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_AI_CIRCULATION] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[NOMBRE_ASSURES] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN_COMPL] [smallint] NULL ,
	[CODE_TARIF] [smallint] NULL ,
	[CODE_INVALIDITE] [smallint] NULL ,
	[CODE_FRAIS_GUERISON] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_AI_CIRCULATION_200702] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[NOMBRE_ASSURES] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN_COMPL] [smallint] NULL ,
	[CODE_TARIF] [smallint] NULL ,
	[CODE_INVALIDITE] [smallint] NULL ,
	[CODE_FRAIS_GUERISON] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_AI_PERSONNES] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[NOMBRE_ASSURES] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN_COMPL] [smallint] NULL ,
	[CODE_TARIF] [smallint] NULL ,
	[CODE_INVALIDITE] [smallint] NULL ,
	[CODE_FRAIS_GUERISON] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_AI_PERSONNES_200702] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[NOMBRE_ASSURES] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN_COMPL] [smallint] NULL ,
	[CODE_TARIF] [smallint] NULL ,
	[CODE_INVALIDITE] [smallint] NULL ,
	[CODE_FRAIS_GUERISON] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_AI_VOYAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[NOMBRE_ASSURES] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN_COMPL] [smallint] NULL ,
	[CODE_TARIF] [smallint] NULL ,
	[CODE_INVALIDITE] [smallint] NULL ,
	[CODE_FRAIS_GUERISON] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_AI_VOYAGE_200702] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [tinyint] NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[NOMBRE_ASSURES] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN] [smallint] NULL ,
	[CODE_ALLOCATION_JOURN_COMPL] [smallint] NULL ,
	[CODE_TARIF] [smallint] NULL ,
	[CODE_INVALIDITE] [smallint] NULL ,
	[CODE_FRAIS_GUERISON] [smallint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_BATIMENT_ENT] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_EVOLUTIVE] [tinyint] NULL ,
	[SOMME_ASSUREE] [int] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_BATIMENT_IND] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_EVOLUTIVE] [tinyint] NULL ,
	[SOMME_ASSUREE] [int] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_CASCO_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_UR] [tinyint] NULL ,
	[PRIX_CATALOGUE] [numeric](12, 2) NULL ,
	[DATE_MISE_CIRCULATION] [smallint] NULL ,
	[DEGRE_BONUS_MALUS] [tinyint] NULL ,
	[POSITION_TARIF] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_PLAQUE] [varchar] (9) COLLATE fr_cs_as NULL ,
	[MARQUE] [varchar] (24) COLLATE fr_cs_as NULL ,
	[CODE_MARQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_TYPE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_VEHICULE] [tinyint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_CASCO_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_EVOLUTIVE] [tinyint] NULL ,
	[SOMME_ASSUREE] [int] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_IARD] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[DATE_CREATION_UR] [datetime] NULL ,
	[STATUT_UR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MUTATION_UR] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA_UR] [varchar] (4) COLLATE fr_cs_as NULL ,
	[SOMME_ASSURANCE_UR] [numeric](12, 0) NULL ,
	[MONTANT_PRIME_UR] [numeric](12, 2) NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MODE_PAIEMENT_POLICE] [tinyint] NULL ,
	[MONTANT_PRIME_POLICE] [numeric](12, 2) NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_INV_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_EVOLUTIVE] [tinyint] NULL ,
	[SOMME_ASSUREE] [int] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_INV_MENAGE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_EVOLUTIVE] [tinyint] NULL ,
	[SOMME_ASSUREE] [int] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_LAA] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[ETAT_GARANTIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_COASSURANCE] [tinyint] NULL ,
	[TAUX_COASSURANCE] [numeric](7, 3) NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[CODE_MUTATION] [smallint] NULL ,
	[NO_RISQUE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CLASSE_RISQUE] [smallint] NULL ,
	[DEGRE_RISQUE] [smallint] NULL ,
	[CODE_EXCEPTION] [tinyint] NULL ,
	[NOMBRE_PERSONNES] [smallint] NULL ,
	[SALAIRE_TOTAL] [int] NULL ,
	[TAUX_TOTAL] [numeric](7, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_OBJET_VALEUR] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [smallint] NULL ,
	[ANNEE_CGA] [smallint] NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_VALEUR_EVOLUTIVE] [tinyint] NULL ,
	[SOMME_ASSUREE] [int] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_RC_AGRICOLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DECOMPTE_DEFINITIF] [tinyint] NULL ,
	[NO_RISQUE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_PARTICIPATION] [tinyint] NULL ,
	[CODE_TYPE_FRANCHISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_FRANCHISE] [numeric](12, 2) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_RC_BATEAUX] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_TYPE_UR] [tinyint] NULL ,
	[PRIX_CATALOGUE] [int] NULL ,
	[DATE_MISE_CIRCULATION] [smallint] NULL ,
	[DEGRE_BONUS_MALUS] [tinyint] NULL ,
	[POSITION_TARIF] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_PLAQUE] [varchar] (9) COLLATE fr_cs_as NULL ,
	[MARQUE] [varchar] (24) COLLATE fr_cs_as NULL ,
	[CODE_MARQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_TYPE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_VEHICULE] [tinyint] NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_RC_ENTREPRISE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DECOMPTE_DEFINITIF] [tinyint] NULL ,
	[CODE_TYPE_DECOMPTE] [tinyint] NULL ,
	[MASSE_SALARIALE] [int] NULL ,
	[NO_RISQUE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_PARTICIPATION] [tinyint] NULL ,
	[CODE_TYPE_FRANCHISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_FRANCHISE] [numeric](12, 2) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_RC_IMMEUBLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DECOMPTE_DEFINITIF] [tinyint] NULL ,
	[NO_RISQUE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_PARTICIPATION] [tinyint] NULL ,
	[CODE_TYPE_FRANCHISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_FRANCHISE] [numeric](12, 2) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_RC_PRIVEE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DECOMPTE_DEFINITIF] [tinyint] NULL ,
	[NO_RISQUE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_PARTICIPATION] [tinyint] NULL ,
	[CODE_TYPE_FRANCHISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_FRANCHISE] [numeric](12, 2) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[UR_RC_PROFESSIONNELLE] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_UR] [tinyint] NOT NULL ,
	[CODE_MUTATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TAUX_COASSURANCE] [tinyint] NULL ,
	[ANNEE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[ANNEE_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DATE_EFFET] [datetime] NULL ,
	[CODE_GENRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DECOMPTE_DEFINITIF] [tinyint] NULL ,
	[NO_RISQUE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_PARTICIPATION] [tinyint] NULL ,
	[CODE_TYPE_FRANCHISE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_FRANCHISE] [numeric](12, 2) NULL ,
	[MONTANT_PRESTATION] [numeric](12, 2) NULL ,
	[MONTANT_PRIME] [numeric](12, 2) NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VC04_PEPU_manuelle_200702] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_ASSURE] [int] NOT NULL ,
	[N_MTPU2] [numeric](12, 2) NOT NULL ,
	[C_MUTATION] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[D_MUTB] [datetime] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VC04_PEPU_manuelle_200702_regroup] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[N_MTPU2] [numeric](12, 2) NOT NULL ,
	[C_MUTATION] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[D_MUTB] [datetime] NOT NULL ,
	[NB_ASS] [int] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VICOM] (
	[ANNEE] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_AGENCE] [tinyint] NULL ,
	[NO_AGENT] [smallint] NULL ,
	[D_DEBUT_RISQUE] [datetime] NULL ,
	[M_TOT_COMMISSION] [numeric](12, 2) NULL ,
	[T_REGUL] [varchar] (5) COLLATE fr_cs_as NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VIM015_LETTRE_PRENEUR] (
	[R_POL] [varchar] (7) COLLATE fr_cs_as NOT NULL ,
	[C_LANGUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_SEGMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[LIB_ASSURE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[T_FORMULE] [varchar] (60) COLLATE fr_cs_as NULL ,
	[ADRE1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE5] [varchar] (30) COLLATE fr_cs_as NULL ,
	[MOIS_ANNIV] [varchar] (13) COLLATE fr_cs_as NULL ,
	[ANNEE_ANNIV] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NUM_AGENCE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[AGENCE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[INDC_TEL_AG] [varchar] (6) COLLATE fr_cs_as NULL ,
	[TEL_AG] [varchar] (10) COLLATE fr_cs_as NULL ,
	[ANNEE_D_JOURS] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MOIS_D_JOURS] [varchar] (12) COLLATE fr_cs_as NULL ,
	[JOURS_D_JOURS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[PRIME_TOT] [varchar] (15) COLLATE fr_cs_as NULL ,
	[PRIME_RISQUE] [varchar] (15) COLLATE fr_cs_as NULL ,
	[PRIME_EPRGNE] [varchar] (15) COLLATE fr_cs_as NULL ,
	[SOLDE_COMPTE] [varchar] (15) COLLATE fr_cs_as NULL ,
	[MONTANT_BVR] [varchar] (15) COLLATE fr_cs_as NULL ,
	[PARAG_AGENCE] [varchar] (255) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VIM016] (
	[R_POL] [varchar] (7) COLLATE fr_cs_as NOT NULL ,
	[C_LANGUE] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[CSEGMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[AUTRE_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[LIB_ASSURE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[TFORMULE] [varchar] (50) COLLATE fr_cs_as NULL ,
	[ADRE1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE5] [varchar] (30) COLLATE fr_cs_as NULL ,
	[TFORMULE_1] [varchar] (50) COLLATE fr_cs_as NULL ,
	[ADRE1_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE2_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE3_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE4_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ADRE5_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[MOISANNIVXX] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MOISANNIV] [varchar] (13) COLLATE fr_cs_as NULL ,
	[ANNEEANNIV] [varchar] (4) COLLATE fr_cs_as NULL ,
	[C_NUM_AGC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[AGENCE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[INDCTELAG] [varchar] (4) COLLATE fr_cs_as NULL ,
	[TELAG] [varchar] (10) COLLATE fr_cs_as NULL ,
	[ANNEEDJOURS] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MOISDJOURS] [varchar] (13) COLLATE fr_cs_as NULL ,
	[JOURSDJOURS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANNEEDTRAIT] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MOISDTRAIT] [varchar] (13) COLLATE fr_cs_as NULL ,
	[JOURSDTRAIT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[PRIMPRETOT] [varchar] (13) COLLATE fr_cs_as NULL ,
	[PRIMPRERISQU] [varchar] (13) COLLATE fr_cs_as NULL ,
	[PRIMPREEPGN] [varchar] (13) COLLATE fr_cs_as NULL ,
	[SOLDECOMPTE] [varchar] (13) COLLATE fr_cs_as NULL ,
	[CAPITVIE] [varchar] (13) COLLATE fr_cs_as NULL ,
	[D_FIN_VIE] [varchar] (10) COLLATE fr_cs_as NULL ,
	[CAPITDECES] [varchar] (13) COLLATE fr_cs_as NULL ,
	[D_FIN_DECES] [varchar] (10) COLLATE fr_cs_as NULL ,
	[DELAI_EXO] [varchar] (2) COLLATE fr_cs_as NULL ,
	[PREST_EXO] [varchar] (13) COLLATE fr_cs_as NULL ,
	[D_FIN_EXO] [varchar] (10) COLLATE fr_cs_as NULL ,
	[PART_ATTRIB] [varchar] (13) COLLATE fr_cs_as NULL ,
	[M_SOLDE_PART] [varchar] (13) COLLATE fr_cs_as NULL ,
	[D_SOLDE_PART] [varchar] (10) COLLATE fr_cs_as NULL ,
	[M_INT_ATT] [varchar] (13) COLLATE fr_cs_as NULL ,
	[D_FIN_INT] [varchar] (10) COLLATE fr_cs_as NULL ,
	[M_TOT_PART] [varchar] (13) COLLATE fr_cs_as NULL ,
	[PRIM_EPGN_SUIV] [varchar] (13) COLLATE fr_cs_as NULL ,
	[PRIM_TOT_SUIV] [varchar] (40) COLLATE fr_cs_as NULL ,
	[C_PARG_AG] [varchar] (255) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VI_EPARGNE_ATTRIBUTIONS] (
	[MOIS_REF] [tinyint] NOT NULL ,
	[ANNEE_REF] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[NO_MOUVEMENT_CATAL] [int] NOT NULL ,
	[CODE_MODE_PARTICIPATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[DATE_ATTRIBUTION_PE] [datetime] NULL ,
	[CODE_MASQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PART_ATTRIBUTION] [numeric](12, 2) NULL ,
	[DATE_CATALOGUE] [datetime] NULL ,
	[C_DEVISE] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DEBUT_TECHNIQUE_COUV] [datetime] NULL ,
	[SITUATION_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VI_EPARGNE_COMPTES] (
	[MOIS_REF] [tinyint] NOT NULL ,
	[ANNEE_REF] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[C_DEVISE] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[MONTANT_SOLDE_INITIAL] [numeric](12, 2) NULL ,
	[MONTANT_ATTRIBUTION] [numeric](12, 2) NULL ,
	[MONTANT_ATT_INT] [numeric](12, 2) NULL ,
	[MONTANT_PRELEV] [numeric](12, 2) NULL ,
	[MONTANT_SOLDE_FINAL] [numeric](12, 2) NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VI_PRODUCTION] (
	[PERIODE] [int] NOT NULL ,
	[R_POL] [int] NOT NULL ,
	[C_ETAT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[BRANCHE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[C_DEVISE] [smallint] NOT NULL ,
	[C_PREV_LIEE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[R_AGC] [tinyint] NULL ,
	[R_AGT] [smallint] NULL ,
	[D_DEB_INTV_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_DEB_ETAT_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[D_EDIT_TAR_SSAA] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[D_EDIT_CGA_AAMM] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NAT_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_RISQUE_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_DELAI_EXONER] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_DELAI] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_DEB_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_PRIM_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_SITUA_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_FRACT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_PART] [varchar] (2) COLLATE fr_cs_as NULL ,
	[M_COUV_ACTUEL] [numeric](12, 2) NULL ,
	[M_PRI_PERIO] [numeric](12, 2) NULL ,
	[M_PRI_UNIQUE] [numeric](12, 2) NULL ,
	[M_PU_SPECIALE] [numeric](12, 2) NULL ,
	[M_RAB_SOM] [numeric](12, 2) NULL ,
	[M_MAJORATION] [numeric](12, 2) NULL ,
	[M_SURP_PERIO] [numeric](12, 2) NULL ,
	[M_SURP_UNIQ] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PP] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PU] [numeric](12, 2) NULL ,
	[T_PA_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_5] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_6] [varchar] (30) COLLATE fr_cs_as NULL ,
	[R_POL_REINV] [varchar] (7) COLLATE fr_cs_as NULL ,
	[GENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SOUSGENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VI_PRODUCTION_2006] (
	[PERIODE] [int] NOT NULL ,
	[R_POL] [int] NOT NULL ,
	[C_ETAT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[BRANCHE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[C_DEVISE] [smallint] NOT NULL ,
	[C_PREV_LIEE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[R_AGC] [tinyint] NULL ,
	[R_AGT] [smallint] NULL ,
	[D_DEB_INTV_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_DEB_ETAT_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[D_EDIT_TAR_SSAA] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[D_EDIT_CGA_AAMM] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NAT_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_RISQUE_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_DELAI_EXONER] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_DELAI] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_DEB_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_PRIM_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_SITUA_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_FRACT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_PART] [varchar] (2) COLLATE fr_cs_as NULL ,
	[M_COUV_ACTUEL] [numeric](12, 2) NULL ,
	[M_PRI_PERIO] [numeric](12, 2) NULL ,
	[M_PRI_UNIQUE] [numeric](12, 2) NULL ,
	[M_PU_SPECIALE] [numeric](12, 2) NULL ,
	[M_RAB_SOM] [numeric](12, 2) NULL ,
	[M_MAJORATION] [numeric](12, 2) NULL ,
	[M_SURP_PERIO] [numeric](12, 2) NULL ,
	[M_SURP_UNIQ] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PP] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PU] [numeric](12, 2) NULL ,
	[R_POL_REINV] [varchar] (7) COLLATE fr_cs_as NULL ,
	[GENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SOUSGENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VI_PRODUCTION_20090304] (
	[PERIODE] [int] NOT NULL ,
	[R_POL] [int] NOT NULL ,
	[C_ETAT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[BRANCHE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[C_DEVISE] [smallint] NOT NULL ,
	[C_PREV_LIEE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[R_AGC] [tinyint] NULL ,
	[R_AGT] [smallint] NULL ,
	[D_DEB_INTV_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_DEB_ETAT_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[D_EDIT_TAR_SSAA] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[D_EDIT_CGA_AAMM] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NAT_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_RISQUE_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_DELAI_EXONER] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_DELAI] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_DEB_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_PRIM_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_SITUA_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_FRACT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_PART] [varchar] (2) COLLATE fr_cs_as NULL ,
	[M_COUV_ACTUEL] [numeric](12, 2) NULL ,
	[M_PRI_PERIO] [numeric](12, 2) NULL ,
	[M_PRI_UNIQUE] [numeric](12, 2) NULL ,
	[M_PU_SPECIALE] [numeric](12, 2) NULL ,
	[M_RAB_SOM] [numeric](12, 2) NULL ,
	[M_MAJORATION] [numeric](12, 2) NULL ,
	[M_SURP_PERIO] [numeric](12, 2) NULL ,
	[M_SURP_UNIQ] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PP] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PU] [numeric](12, 2) NULL ,
	[R_POL_REINV] [varchar] (7) COLLATE fr_cs_as NULL ,
	[GENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SOUSGENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[VI_PRODUCTION_MAJ_ADRESSE] (
	[PERIODE] [int] NOT NULL ,
	[R_POL] [int] NOT NULL ,
	[C_ETAT] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[BRANCHE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[C_DEVISE] [smallint] NOT NULL ,
	[C_PREV_LIEE] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[R_AGC] [tinyint] NULL ,
	[R_AGT] [smallint] NULL ,
	[D_DEB_INTV_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_DEB_ETAT_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[D_EDIT_TAR_SSAA] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[D_EDIT_CGA_AAMM] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NAT_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_RISQUE_COUV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_DELAI_EXONER] [varchar] (2) COLLATE fr_cs_as NULL ,
	[C_DELAI] [varchar] (2) COLLATE fr_cs_as NULL ,
	[D_DEB_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_TECH_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[D_FIN_PRIM_COUV] [char] (8) COLLATE fr_cs_as NOT NULL ,
	[C_SITUA_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_FRACT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[C_MODE_PART] [varchar] (2) COLLATE fr_cs_as NULL ,
	[M_COUV_ACTUEL] [numeric](12, 2) NULL ,
	[M_PRI_PERIO] [numeric](12, 2) NULL ,
	[M_PRI_UNIQUE] [numeric](12, 2) NULL ,
	[M_PU_SPECIALE] [numeric](12, 2) NULL ,
	[M_RAB_SOM] [numeric](12, 2) NULL ,
	[M_MAJORATION] [numeric](12, 2) NULL ,
	[M_SURP_PERIO] [numeric](12, 2) NULL ,
	[M_SURP_UNIQ] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PP] [numeric](12, 2) NULL ,
	[M_PRODUCTION_PU] [numeric](12, 2) NULL ,
	[T_PA_1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_5] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_PA_6] [varchar] (30) COLLATE fr_cs_as NULL ,
	[R_POL_REINV] [varchar] (7) COLLATE fr_cs_as NULL ,
	[GENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SOUSGENRE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_ACH] (
	[NO_PERS] [int] NOT NULL ,
	[C_NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_RUE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_RUE] [varchar] (7) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_ADR] (
	[NO_PERS] [int] NOT NULL ,
	[C_NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_TYPE_ADRESSE] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[C_CONFIDENTIEL] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[T_NOM_RUE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_NO_POSTAL] [varchar] (5) COLLATE fr_cs_as NULL ,
	[T_FILLER1] [varchar] (1) COLLATE fr_cs_as NULL ,
	[T_LOCALITE] [varchar] (24) COLLATE fr_cs_as NULL ,
	[C_COMPL_ADR] [varchar] (1) COLLATE fr_cs_as NULL ,
	[T_COMPL_ADR] [varchar] (30) COLLATE fr_cs_as NULL ,
	[C_COMPL_NPA] [varchar] (2) COLLATE fr_cs_as NULL ,
	[T_CASE_POSTALE] [varchar] (20) COLLATE fr_cs_as NULL ,
	[C_CANTON_PAYS] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_AET] (
	[NO_PERS] [int] NOT NULL ,
	[C_NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_LANG] [tinyint] NOT NULL ,
	[NO_PAYS_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[T_RUE_MIX_ETR] [varchar] (30) COLLATE fr_cs_as NULL ,
	[T_LIEU_ETR] [varchar] (30) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_CAD] (
	[NO_PERS] [int] NOT NULL ,
	[C_NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_CONF] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_CPL] (
	[NO_PERS] [int] NOT NULL ,
	[C_NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[T_CP] [varchar] (20) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_LCA] (
	[NO_PERS] [int] NOT NULL ,
	[C_NO_ORD_ADR] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_RUE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_RUE] [varchar] (7) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_NPA] (
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_LANG] [tinyint] NOT NULL ,
	[NO_OFCE_POURS] [int] NULL ,
	[C_DIS] [varchar] (1) COLLATE fr_cs_as NULL ,
	[T_LOC_18_CH] [varchar] (18) COLLATE fr_cs_as NULL ,
	[T_AGLM] [varchar] (18) COLLATE fr_cs_as NULL ,
	[C_CANTON] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_RCH] (
	[C_RUE] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[NO_PSTL_CH] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_PSTL_C_CH] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[T_RUE_MAJ] [varchar] (28) COLLATE fr_cs_as NULL ,
	[T_RUE_MIX] [varchar] (28) COLLATE fr_cs_as NULL ,
	[T_RUE_RCDL] [varchar] (10) COLLATE fr_cs_as NOT NULL ,
	[NO_MASN_MIN_PAIR] [smallint] NULL ,
	[NO_MASN_MAX_PAIR] [smallint] NULL ,
	[NO_MASN_MIN_IMPA] [smallint] NULL ,
	[NO_MASN_MAX_IMPA] [smallint] NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[ZC_SUP] (
	[NO_PERS] [int] NOT NULL ,
	[NO_PERS_LIEE] [int] NOT NULL ,
	[C_TYPE_RELATION] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[C_NO_ORD] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[C_NO_ORD_LIEE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[T_TXTE_RELATION] [varchar] (20) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[_ACCES_AGCAGT] (
	[TACRO] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[NO_AGENCE] [tinyint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[_ACCES_DROITS] (
	[tAcro] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[Cod_Parametre] [varchar] (1000) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[_bof_VI_EPARGNE_ATTRIBUTIONS] (
	[MOIS_REF] [tinyint] NOT NULL ,
	[ANNEE_REF] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_COUVERTURE] [smallint] NOT NULL ,
	[NO_MOUVEMENT_CATAL] [int] NOT NULL ,
	[CODE_MODE_PARTICIPATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[DATE_ATTRIBUTION_PE] [datetime] NULL ,
	[CODE_MASQUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_PART_ATTRIBUTION] [numeric](12, 2) NULL ,
	[DATE_CATALOGUE] [datetime] NULL ,
	[C_DEVISE] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[CODE_TARIF] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_TARIFAIRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[EDITION_CGA] [varchar] (4) COLLATE fr_cs_as NULL ,
	[DEBUT_TECHNIQUE_COUV] [datetime] NULL ,
	[SITUATION_PRIME] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL 
) ON [PRIMARY]


CREATE TABLE [dbo].[_essaibof] (
	[n] [int] NULL ,
	[d] [datetime] NULL 
) ON [PRIMARY]


ALTER TABLE [dbo].[ADRESSES] WITH NOCHECK ADD 
	CONSTRAINT [PK_ADRESSES] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[AGENCES] WITH NOCHECK ADD 
	CONSTRAINT [PK_AGENCES] PRIMARY KEY  CLUSTERED 
	(
		[NO_AGENCE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[AGENTS] WITH NOCHECK ADD 
	CONSTRAINT [PK_AGENTS] PRIMARY KEY  CLUSTERED 
	(
		[NO_AGENCE],
		[NO_AGENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[AIC_CODIF1] WITH NOCHECK ADD 
	CONSTRAINT [PK_AIC_CODIF1] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[AIC_CODIF2] WITH NOCHECK ADD 
	CONSTRAINT [PK_AIC_CODIF2] PRIMARY KEY  CLUSTERED 
	(
		[ID],
		[CODE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[AIC_SYSTYPES] WITH NOCHECK ADD 
	CONSTRAINT [PK_AIC_SYSTYPES] PRIMARY KEY  CLUSTERED 
	(
		[xtype]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[AS400_NATURES_CODES] WITH NOCHECK ADD 
	CONSTRAINT [PK_ASCOD] PRIMARY KEY  CLUSTERED 
	(
		[NATURE_OPERATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ASSURES_AI_PERSONNES] WITH NOCHECK ADD 
	CONSTRAINT [PK_ASSURES_AI_PERSONNES] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_ASSURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ASSURES_MALADIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_ASSURES_MALADIE_COLLEC] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[NO_PLAN]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ASSURES_VIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_ASSURES_VIE_COLLECTIVE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[BRANCHES] WITH NOCHECK ADD 
	CONSTRAINT [PK_BRANCHES] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CANTONS_ANNUAIRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_CANTONS_ANNUAIRES] PRIMARY KEY  CLUSTERED 
	(
		[NO_ANNUAIRE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CARTE_PROD_13] WITH NOCHECK ADD 
	CONSTRAINT [PK_CARTE_PROD_13] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE],
		[NO_POLICE],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CENTRES_COUTS] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_AGENCE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_CAUSES_SINISTRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODES_CAUSES_SINISTRES] PRIMARY KEY  CLUSTERED 
	(
		[CODE_CAUSE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_MONNAIES] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODES_MONNAIES2] PRIMARY KEY  CLUSTERED 
	(
		[CODE_MONNAIE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_MONNAIES_ISO] WITH NOCHECK ADD 
	CONSTRAINT [PK___2__11] PRIMARY KEY  CLUSTERED 
	(
		[CODE_MONNAIE_ISO]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_NATURES_ECRITURES] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODES_PREST] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[C_NAT_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_NATURES_ECRITURES_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODES_NATURES_ECRITURES_TMP] PRIMARY KEY  CLUSTERED 
	(
		[C_NAT_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_NATURES_PAIEMENTS] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODES_NATURES_PAIEMENT] PRIMARY KEY  CLUSTERED 
	(
		[CODE_NATURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_PAYS] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODES_PAYS] PRIMARY KEY  CLUSTERED 
	(
		[C_PAYS_ISO3]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODES_REGION] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[ZONE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODIF_AGENTS_GE_ZH] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[ID_EMP]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODIF_BONUS_MALUS] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODIF_BONUS_MALUS] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_RISQUE],
		[CODE_BONUS_MALUS]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODIF_MALADIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODIF_MALADIE_COLLEC] PRIMARY KEY  CLUSTERED 
	(
		[NO_SECTEUR_ECONOMIQUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODIF_NOGA_2002] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODIF_NOGA] PRIMARY KEY  CLUSTERED 
	(
		[C_NOGA],
		[C_LANG]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODIF_RISQUES] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODIF_RISQUES_IARD] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_RISQUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CODIF_RISQUES_CORMIS] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODIF_RISQUES_CORMIS] PRIMARY KEY  CLUSTERED 
	(
		[SEQ_NO],
		[BUSINESS_SEGMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[COMM_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_COMM_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[DATE_DEBUT_RISQUE],
		[CODE_LIAISON_DEBIT],
		[CODE_NO_DROIT],
		[CODE_NO_ORDRE_DROIT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[COMPTES_OUVERTS] WITH NOCHECK ADD 
	CONSTRAINT [COMPTES_OUVERTS_PK] PRIMARY KEY  CLUSTERED 
	(
		[DATE_EXTRAC],
		[NO_CPT],
		[NO_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[CONVERSION_ACTIVITE] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODIF_SECT_ACTIVITE] PRIMARY KEY  CLUSTERED 
	(
		[SECT_75]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[COURS_MONNAIES] WITH NOCHECK ADD 
	CONSTRAINT [PK_COURS_MONNAIES] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_CHANGE],
		[MOIS_CHANGE],
		[CODE_MONNAIE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[COUV_MALADIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_COUV_MALADIE_COLLECTIVE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_PLAN],
		[NO_COUCHE],
		[NO_RISQUE],
		[DELAI_ATTENTE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[COUV_MALADIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_COUV_MALADIE_IND] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[GENRE_COUVERTURE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[COUV_MALADIE_INDIVIDUELLE_200704] WITH NOCHECK ADD 
	CONSTRAINT [PK_COUV_MALADIE_IND_200704] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[GENRE_COUVERTURE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[COUV_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_COUV_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[EIS_STATPRODGROUP] WITH NOCHECK ADD 
	CONSTRAINT [PK_EIS_STATPRODGROUP] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[EIS_TABLEAU_BORD_ENT] WITH NOCHECK ADD 
	CONSTRAINT [PK_EIS_TABLEAU_BORD_ENT] PRIMARY KEY  CLUSTERED 
	(
		[NO_AGENCE],
		[NO_AGENT],
		[BRANCHE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[GARANTIES_VEHICULE] WITH NOCHECK ADD 
	CONSTRAINT [PK_GARANTIES_VEHICULE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[TYPE_OBJET],
		[NO_OBJET],
		[NO_RISQUE],
		[NO_GARANTIE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[GARANTIES_VEHICULE_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_GARANTIES_VEHICULE_TMP] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[TYPE_OBJET],
		[NO_OBJET],
		[NO_RISQUE],
		[NO_GARANTIE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[GENEVOISE_VC04_DECODIF] WITH NOCHECK ADD 
	CONSTRAINT [PK_GEVC04_DECODIF] PRIMARY KEY  CLUSTERED 
	(
		[NO_POLICE_GE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[HELP_LISTE_COLONNES] WITH NOCHECK ADD 
	CONSTRAINT [PK_HELPCOL] PRIMARY KEY  CLUSTERED 
	(
		[TABLE_NOM],
		[COLONNE_NOM]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[HELP_LISTE_COLVALEURS] WITH NOCHECK ADD 
	CONSTRAINT [PK_HELPVAL] PRIMARY KEY  CLUSTERED 
	(
		[GROUPE_ID],
		[VALEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[HIST_INVENTAIRE_VI03] WITH NOCHECK ADD 
	CONSTRAINT [PK_HIST_INVENTAIRE_VI03] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[CODE_TARIF],
		[EDITION_TARIFAIRE],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[HIST_INV_TRIM_GENLOC] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_GENLOC] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[HIST_LIAISONS] WITH NOCHECK ADD 
	CONSTRAINT [PK_HIST_LIAISONS] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[HIST_PAIEMENTS_MALADIE] WITH NOCHECK ADD 
	CONSTRAINT [PK_SINISTRES_MALADIE_HIST] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[GENRE_COUVERTURE],
		[NO_COUVERTURE],
		[NO_SIN],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[HIST_PE_1995] WITH NOCHECK ADD 
	CONSTRAINT [PK_HIST_PE_1995] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_DEBIT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[IC_USERS] WITH NOCHECK ADD 
	CONSTRAINT [PK_IC_USERS] PRIMARY KEY  CLUSTERED 
	(
		[ID_EMP]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[IC_USERS_MODIF] WITH NOCHECK ADD 
	CONSTRAINT [PK_IC_USERS_MODIF] PRIMARY KEY  CLUSTERED 
	(
		[ID_EMP]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INFO_GENFLEX] WITH NOCHECK ADD 
	CONSTRAINT [PK_INFO_GENFLEX] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_MOUVEMENT_CATALOGUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INVENTAIRE_MENSUEL_IARD] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_MENSUEL_IARD] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR_COUCHE],
		[NO_INVENTAIRE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INVENTAIRE_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_VIE_IND] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[CODE_TARIF]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INV_TRIM_VI03] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_VI03] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INV_TRIM_VI03_200506] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_VI03_200506] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INV_TRIM_VI03_RENF] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_VI03_RENF] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INV_TRIM_VI07] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_GENLOC_11] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[CODE_TARIF]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INV_TRIM_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_INV_TRIM_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[CODE_TARIF]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[INV_TRIM_VIRAC] WITH NOCHECK ADD 
	CONSTRAINT [PK_INVENTAIRE_VIRAC] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[LATTMAN2] WITH NOCHECK ADD 
	CONSTRAINT [PK_LATTMAN2] PRIMARY KEY  CLUSTERED 
	(
		[BRANCHE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[LATTMANN] WITH NOCHECK ADD 
	CONSTRAINT [PK_LATTMANN] PRIMARY KEY  CLUSTERED 
	(
		[BRANCHE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[LIAISONS] WITH NOCHECK ADD 
	CONSTRAINT [PK_LIAISONS] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[LIAISONS_SEGMENT] WITH NOCHECK ADD 
	CONSTRAINT [PK___3__11] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[LOCALITE_COMMUNE] WITH NOCHECK ADD 
	CONSTRAINT [PK_LOCALITE_COMMUNE] PRIMARY KEY  CLUSTERED 
	(
		[NO_POSTAL],
		[LOCALITE_COMPLETE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[MC06_ASSURES] WITH NOCHECK ADD 
	CONSTRAINT [PK__MC06_ASSURES] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[DATE_DEBUT_EMPL],
		[DATE_DEBUT_SAL],
		[DATE_FIN_SAL]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[MI05_SAP_PREST] WITH NOCHECK ADD 
	CONSTRAINT [MI05_SAP_PREST_PK] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_SINISTRE],
		[NO_ECRITURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[MVT_CATAL_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_MVT_CATAL_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[DATE_CATALOGUE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[TYPE_CAPITAL],
		[CODE_MUTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[MVT_EPARGNE_VIE_IND] WITH NOCHECK ADD 
	CONSTRAINT [PK_MVT_EPARGNE_VIE_IND1] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[MZ_RENTES_MIGREES] WITH NOCHECK ADD 
	CONSTRAINT [PK_MZ_RENTES_MIGREES] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_SINISTRE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[NOUVELLES_AFFAIRES_VC04] WITH NOCHECK ADD 
	CONSTRAINT [PK_NOUV_AFF_VC04] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_REF],
		[MOIS_REF],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[OBJETS_VEHICULE] WITH NOCHECK ADD 
	CONSTRAINT [PK_OBJETS_VEHICULE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[TYPE_OBJET],
		[NO_OBJET]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[OBJETS_VEHICULE_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_OBJETS_VEHICULE_TMP] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[TYPE_OBJET],
		[NO_OBJET]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_ACLA] WITH NOCHECK ADD 
	CONSTRAINT [P11] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_AC_AUTRES] WITH NOCHECK ADD 
	CONSTRAINT [P54] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_AI_CIRCULATION] WITH NOCHECK ADD 
	CONSTRAINT [P53] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_AI_PERSONNES] WITH NOCHECK ADD 
	CONSTRAINT [P51] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_AI_VOYAGE] WITH NOCHECK ADD 
	CONSTRAINT [P52] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_BATIMENT_ENT] WITH NOCHECK ADD 
	CONSTRAINT [P87] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_BATIMENT_IND] WITH NOCHECK ADD 
	CONSTRAINT [P90] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_CASCO_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [P74] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_CASCO_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [P88] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_INV_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [P86] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_INV_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [P85] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_LAA] WITH NOCHECK ADD 
	CONSTRAINT [P10] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_OBJET_VALEUR] WITH NOCHECK ADD 
	CONSTRAINT [P89] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_RC_AGRICOLE] WITH NOCHECK ADD 
	CONSTRAINT [P63] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_RC_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [P66] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_RC_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [P60] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_RC_IMMEUBLE] WITH NOCHECK ADD 
	CONSTRAINT [P64] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_RC_PRIVEE] WITH NOCHECK ADD 
	CONSTRAINT [P62] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_RC_PROFESSIONNELLE] WITH NOCHECK ADD 
	CONSTRAINT [P61] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PARTENAIRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_PARTENAIRES] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PART_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_PART_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[CODE_TYPE_DROIT],
		[MODE_PARTICIPATION],
		[DATE_DEBUT_PART]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_ACLA] WITH NOCHECK ADD 
	CONSTRAINT [PK_P11] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_AC_AUTRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_P54] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_AI_CIRCULATION] WITH NOCHECK ADD 
	CONSTRAINT [PK_P53] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_AI_CIRCULATION_200702] WITH NOCHECK ADD 
	CONSTRAINT [PK_P53_200702] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_AI_PERSONNES] WITH NOCHECK ADD 
	CONSTRAINT [PK_P51] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_AI_PERSONNES_200702] WITH NOCHECK ADD 
	CONSTRAINT [PK_P51_200702] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_AI_VOYAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P52] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_AI_VOYAGE_200702] WITH NOCHECK ADD 
	CONSTRAINT [PK_P52_200702] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_BATIMENT_ENT] WITH NOCHECK ADD 
	CONSTRAINT [PK_P87] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_BATIMENT_IND] WITH NOCHECK ADD 
	CONSTRAINT [PK_P90] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_CASCO_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [PK_P74] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_CASCO_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P88] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_INV_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P86] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_INV_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P85] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_LAA] WITH NOCHECK ADD 
	CONSTRAINT [PK_P10] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_MALADIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P06] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_MALADIE_COLL_MIGREES_GIRARD] WITH NOCHECK ADD 
	CONSTRAINT [PK_P056] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_MALADIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P05] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_MALADIE_INDIVIDUELLE_200704] WITH NOCHECK ADD 
	CONSTRAINT [PK_P05_200704] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_MALADIE_IND_MIGREES_GIRARD] WITH NOCHECK ADD 
	CONSTRAINT [PK_P055] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_OBJET_VALEUR] WITH NOCHECK ADD 
	CONSTRAINT [PK_P89] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_RC_AGRICOLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P63] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_RC_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [PK_P66] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_RC_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P60] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_RC_IMMEUBLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P64] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_RC_PRIVEE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P62] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_RC_PROFESSIONNELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P61] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_VC04_MIGREES_GIRARD] WITH NOCHECK ADD 
	CONSTRAINT [PK_P0567] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_VEHICULE] WITH NOCHECK ADD 
	CONSTRAINT [PK_POLICES_VEHICULE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_VEHICULE_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_POLICES_VEHICULE_TMP] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_VIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P04] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_VIE_COLLECTIVEbof] WITH NOCHECK ADD 
	CONSTRAINT [PK_P04bof] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[POLICES_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_P01] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PORTEFEUILLE_1995] WITH NOCHECK ADD 
	CONSTRAINT [PK_PORTEFEUILLE_1995] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRENEURS_PAYEURS] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRENEURS_PAYEURS] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRENEURS_PAYEURS_ADRESSES] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRENEURS_PAYEURS_ADRESSES] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE],
		[C_NO_ORD_ADR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRENEURS_PAYEURS_CONFID] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRENEURS_PAYEURS22] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRENEURS_PAYEURS_TEST] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRENEURS_PAYEURS_TEST] PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRESTATIONS_0819_0820] WITH NOCHECK ADD 
	CONSTRAINT [PREST_0819_0820_PK] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_EMISSION],
		[MOIS_EMISSION],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_PRD],
		[NO_ECR],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRESTATIONS_AI_CIRCULATION] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRESTATIONS_AI_CIRCULATION] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[CODE_PRESTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRESTATIONS_AI_PERSONNES] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRESTATIONS_AI_PERSONNES] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[CODE_PRESTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRESTATIONS_AI_VOYAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRESTATIONS_AI_VOYAGE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[CODE_PRESTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRESTATIONS_ZS] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRESTATIONS_ZS_1__10] PRIMARY KEY  CLUSTERED 
	(
		[D_DEB],
		[D_FIN],
		[NO_ECR],
		[NO_CPT],
		[C_MN_ISO],
		[C_PERIODE],
		[C_TYP_DOC],
		[D_DOC_EMIS],
		[DC],
		[NO_DET]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRESTATIONS_ZS_MELAN] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRESTATIONS_ZS_MELAN_1] PRIMARY KEY  CLUSTERED 
	(
		[D_DEB],
		[D_FIN],
		[NO_ECR],
		[NO_CPT],
		[C_MN_ISO],
		[C_PERIODE],
		[C_TYP_DOC],
		[D_DOC_EMIS],
		[DC],
		[NO_DET]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PREST_CAP_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_PREST_CAP_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[NO_MVT_CATAL],
		[CODE_GENRE_PRESTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PREST_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_PREST_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[DATE_DEBUT_PRESTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRIMES_RISQUE_VC04] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRIMES_RISQUE_VC04] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[ANNEE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRIMES_ZP] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRIMES_ZP11] PRIMARY KEY  CLUSTERED 
	(
		[D_DEB],
		[C_TBL],
		[NO_CPT],
		[C_PERIODE],
		[NO_ECR],
		[DC]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRIMES_ZP_MELAN] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRIMES_ZP_MELAN11] PRIMARY KEY  CLUSTERED 
	(
		[D_DEB],
		[C_TBL],
		[NO_CPT],
		[C_PERIODE],
		[NO_ECR],
		[DC]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRIMES_ZP_PEPU_FIXED] WITH NOCHECK ADD 
	CONSTRAINT [PK_PRIMES_ZP_PEPU] PRIMARY KEY  CLUSTERED 
	(
		[D_DEB],
		[C_TBL],
		[NO_CPT],
		[C_PERIODE],
		[NO_ECR],
		[DC]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[QUARTETS] WITH NOCHECK ADD 
	CONSTRAINT [PK___1__11] PRIMARY KEY  CLUSTERED 
	(
		[NO_QUARTET],
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[REMUNERATION_VIE_IND] WITH NOCHECK ADD 
	CONSTRAINT [PK_REMUNERATION_VIE_IND] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ORDRE],
		[DATE_COMPTABLE],
		[TYPE_MOUVEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RENDEMENT_MALADIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_RENDEMENT_MALADIE_COLL] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[ANNEE_RENDEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RENDEMENT_POLICES] WITH NOCHECK ADD 
	CONSTRAINT [PK_RENDEMENT_POLICES] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[ANNEE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RENTES_VIVC] WITH NOCHECK ADD 
	CONSTRAINT [PK_RENTES_VIVC_2] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_EMISSION],
		[MOIS_EMISSION],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[NATURE_RENTE_DWH],
		[CODE_MONNAIE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RENTES_VIVC_HIST] WITH NOCHECK ADD 
	CONSTRAINT [PK_RENTES_VIVC] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_EMISSION],
		[MOIS_EMISSION],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[NATURE_RENTE_DWH]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RENTES_VIVC_intermed] WITH NOCHECK ADD 
	CONSTRAINT [PK_RENTES_VIVC_intermed] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_EMISSION],
		[MOIS_EMISSION],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[C_NAT_ECR],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RENTES_VIVC_intermed_original] WITH NOCHECK ADD 
	CONSTRAINT [PK_RENTES_VIVC_intermed_orig] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_EMISSION],
		[MOIS_EMISSION],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[C_NAT_ECR],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RENTES_VI_PERMNONPERM] WITH NOCHECK ADD 
	CONSTRAINT [PK__RENTES_VI_PERMNO__5AD97420] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE],
		[TYPE_POLICE],
		[NO_POLICE],
		[COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RESERVES_PREST_INVALIDITE] WITH NOCHECK ADD 
	CONSTRAINT [PK_RESERVES_PREST_INVALIDITE2] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[CIE_REASS],
		[CODE_TARIF],
		[EDITION_TARIFAIRE],
		[AGE_SURVENANCE],
		[AGE_TERME]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RESERVES_VIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_RESERVES_VIE_COLLECTIVE] PRIMARY KEY  CLUSTERED 
	(
		[DATE_COMPTABLE],
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RES_RI_VIE_COLLECTIVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_RES_RI_VIE_COLLECTIVE_1__11] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[K_GENRE_ASS]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RISQUES_VEHICULE] WITH NOCHECK ADD 
	CONSTRAINT [PK_RISQUES_VEHICULE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[TYPE_OBJET],
		[NO_OBJET],
		[NO_RISQUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[RISQUES_VEHICULE_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_RISQUES_VEHICULE_TMP] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[TYPE_OBJET],
		[NO_OBJET],
		[NO_RISQUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SAP_PRESTATIONS_VI] WITH NOCHECK ADD 
	CONSTRAINT [PK_SAP_PAIEMENTS] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SC_Année_inventaire] WITH NOCHECK ADD 
	CONSTRAINT [PK_SC_Annee_inventaire_1__11] PRIMARY KEY  CLUSTERED 
	(
		[Année],
		[Mois]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SC_T_Date_limite] WITH NOCHECK ADD 
	CONSTRAINT [PK_SC_T_Date_limite] PRIMARY KEY  CLUSTERED 
	(
		[Année],
		[Mois]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SC_as7i_inv_result] WITH NOCHECK ADD 
	CONSTRAINT [PK_SC_as7i_inv_result_1__11] PRIMARY KEY  CLUSTERED 
	(
		[NO_MVT_FNDS]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SC_as7i_mvt_result] WITH NOCHECK ADD 
	CONSTRAINT [PK_SC_as7i_mvt_result] PRIMARY KEY  CLUSTERED 
	(
		[NO_MVT_FNDS]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SC_as7i_mvt_result_stat_rohrer] WITH NOCHECK ADD 
	CONSTRAINT [PK_SC_as7i_mvt_result_sta1__11] PRIMARY KEY  CLUSTERED 
	(
		[NO_MVT_FNDS]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SC_t_mvt_stat_zurich] WITH NOCHECK ADD 
	CONSTRAINT [PK_SC_t_mvt_stat_zurich] PRIMARY KEY  CLUSTERED 
	(
		[NO_MVT_FNDS]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_ACLA] WITH NOCHECK ADD 
	CONSTRAINT [PK_S11] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_AC_AUTRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_S54] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_AI_CIRCULATION] WITH NOCHECK ADD 
	CONSTRAINT [PK_S53] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_AI_PERSONNES] WITH NOCHECK ADD 
	CONSTRAINT [PK_S51] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_AI_VOYAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S52] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_BATIMENT_ENT] WITH NOCHECK ADD 
	CONSTRAINT [PK_S87] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_BATIMENT_IND] WITH NOCHECK ADD 
	CONSTRAINT [PK_S90] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_CASCO_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [PK_S74] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_CASCO_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S88] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_INV_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S86] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_INV_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S85] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_LAA] WITH NOCHECK ADD 
	CONSTRAINT [PK_S10] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_MC06_MIGRES_WIDMER] WITH NOCHECK ADD 
	CONSTRAINT [PK_SIN_MC06_MIGRES_WIDMER] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[NO_SINISTRE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_OBJET_VALEUR] WITH NOCHECK ADD 
	CONSTRAINT [PK_S89] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_RC_AGRICOLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S63] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_RC_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [PK_S66] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_RC_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S60] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_RC_IMMEUBLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S64] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_RC_PRIVEE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S62] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_RC_PROFESSIONNELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_S61] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_CONTRAT],
		[BRANCHE],
		[DATE_SURVENANCE],
		[NO_GESTIONNAIRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SOLDES] WITH NOCHECK ADD 
	CONSTRAINT [SOLDES_PK] PRIMARY KEY  CLUSTERED 
	(
		[DATE_EXTRAC],
		[NO_CPT],
		[NO_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SOLDES_COMPTES] WITH NOCHECK ADD 
	CONSTRAINT [SOLDES_COMPTES_PK] PRIMARY KEY  CLUSTERED 
	(
		[DATE_SOLDE],
		[NO_CPT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SOLDES_MELAN] WITH NOCHECK ADD 
	CONSTRAINT [SOLDES_PK_MELAN] PRIMARY KEY  CLUSTERED 
	(
		[DATE_EXTRAC],
		[NO_CPT],
		[NO_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[STAT_ACT_MUTATIONS] WITH NOCHECK ADD 
	CONSTRAINT [PK_STAT_ACT_MUTATIONS] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[CODE_MUTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[STAT_PROD_BRANCHES] WITH NOCHECK ADD 
	CONSTRAINT [PK_STAT_PROD_BRANCHES] PRIMARY KEY  CLUSTERED 
	(
		[CODE_GROUPE_1],
		[CODE_GROUPE_2],
		[CODE_GROUPE_3],
		[CODE_GROUPE_4],
		[CODE_GROUPE_5]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[STAT_PROD_LIBELLES] WITH NOCHECK ADD 
	CONSTRAINT [PK_STAT_PROD_LIBELLES] PRIMARY KEY  CLUSTERED 
	(
		[CODE_STATISTIQUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[STAT_PROD_MUTATIONS] WITH NOCHECK ADD 
	CONSTRAINT [PK_STAT_PROD_MUTATIONS] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[CODE_MUTATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[STRUCT_PORTEFEUILLE_ZH] WITH NOCHECK ADD 
	CONSTRAINT [PK_STRUCT_ZH] PRIMARY KEY  CLUSTERED 
	(
		[DATNEU],
		[POLICENNR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_AGENCES] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_AGENCES] PRIMARY KEY  CLUSTERED 
	(
		[AGENCE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_BRANCHES] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_BRANCHES] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_REF]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_COMM_ADMIN_T08] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_COMM_ADMIN_T08] PRIMARY KEY  CLUSTERED 
	(
		[C_NO_EXTRAT],
		[C_TYPE_REF],
		[C_NO_REF],
		[C_AGCE],
		[C_AGNT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_COMM_COURTAGE_MAN] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_COMM_COURTAGE_MAN] PRIMARY KEY  CLUSTERED 
	(
		[C_NO_EXTRAT],
		[C_TYPE_REF],
		[C_NO_REF],
		[D_MOUV],
		[C_SEQ],
		[C_AGCE],
		[C_AGNT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_COMM_COURTAGE_MAN_20080624_BAK] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_COMM_COURTAGE_MAN_20080624_BAK] PRIMARY KEY  CLUSTERED 
	(
		[C_NO_EXTRAT],
		[C_TYPE_REF],
		[C_NO_REF],
		[D_MOUV],
		[C_SEQ],
		[C_AGCE],
		[C_AGNT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_COMM_COURTAGE_T14] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_COMM_COURTAGE_T14] PRIMARY KEY  CLUSTERED 
	(
		[C_NO_EXTRAT],
		[C_TYPE_REF],
		[C_NO_REF],
		[D_MOUV],
		[C_SEQ],
		[C_AGCE],
		[C_AGNT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_COMM_COURTAGE_T14_MELAN] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_COMM_COURTAGE_T14_MELAN] PRIMARY KEY  CLUSTERED 
	(
		[C_NO_EXTRAT],
		[C_TYPE_REF],
		[C_NO_REF],
		[D_MOUV],
		[C_SEQ],
		[C_AGCE],
		[C_AGNT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_COURTIERS_T14] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_COURTIERS_T14] PRIMARY KEY  CLUSTERED 
	(
		[NO_AGENCE],
		[NO_AGT1],
		[NO_AGT2]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_DATES_DISPONIBLES] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_DATES_DISPONIBLES] PRIMARY KEY  CLUSTERED 
	(
		[C_NO_EXTRAT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_DETMI05_T11] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_DETMI05_T11] PRIMARY KEY  CLUSTERED 
	(
		[C_NO_EXTRAT],
		[C_TYPE_REF],
		[C_NO_REF],
		[D_MOUV],
		[C_SEQ],
		[R_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_MVTS_T00] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_MVTS_T00] PRIMARY KEY  CLUSTERED 
	(
		[C_MOIS],
		[F_DEB],
		[F_CRE],
		[F_IMP],
		[F_AVC],
		[C_TYPE_REF],
		[R_POL],
		[D_MVT],
		[C_SEQ],
		[C_MVT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_MVTS_T00_MELAN] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_MVTS_T00_MELAN] PRIMARY KEY  CLUSTERED 
	(
		[C_MOIS],
		[F_DEB],
		[F_CRE],
		[F_IMP],
		[F_AVC],
		[C_TYPE_REF],
		[R_POL],
		[D_MVT],
		[C_SEQ],
		[C_MVT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_NATURE_MOUVEMENTS] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_NATURE_MOUVEMENTS] PRIMARY KEY  CLUSTERED 
	(
		[C_NATURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_TYPE_MOUVEMENTS] WITH NOCHECK ADD 
	CONSTRAINT [PK_S_TYPE_MOUVEMENTS] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_MVT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[S_VcComBroker] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[AN],
		[R_AGCE_ZA],
		[R_POL],
		[COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[TEMP_INV_TRIM_VI03] WITH NOCHECK ADD 
	CONSTRAINT [PK_TEMP_INVENTAIRE_VI03] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[TEMP_INV_TRIM_VI03_RENF] WITH NOCHECK ADD 
	CONSTRAINT [PK_TEMP_INVENTAIRE_VI03_RENF] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[TEMP_INV_TRIM_VI07] WITH NOCHECK ADD 
	CONSTRAINT [PK_TEMP_INVENTAIRE_GENLOC_11] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[CODE_TARIF]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[TEMP_INV_TRIM_VIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_TEMP_INV_TRIM_VIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[CODE_TARIF]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[TEMP_INV_TRIM_VIRAC] WITH NOCHECK ADD 
	CONSTRAINT [PK_TEMP_INVENTAIRE_VIRAC] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[TEMP_RESERVES_PREST_INVALIDITE] WITH NOCHECK ADD 
	CONSTRAINT [PK_TEMP_RESERVES_PREST_INVALIDITE2] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_TRIMESTRE],
		[MOIS_TRIMESTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[CIE_REASS],
		[CODE_TARIF],
		[EDITION_TARIFAIRE],
		[AGE_SURVENANCE],
		[AGE_TERME]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[TEXTES_MALADIE_INDIVIDUELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_TEXTES_MALADIE_INDIVIDUELLE] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_LIGNE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_ACLA] WITH NOCHECK ADD 
	CONSTRAINT [PK_U11] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_AC_AUTRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_U54] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_AI_CIRCULATION] WITH NOCHECK ADD 
	CONSTRAINT [PK_U53] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_AI_CIRCULATION_200702] WITH NOCHECK ADD 
	CONSTRAINT [PK_U53_200702] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_AI_PERSONNES] WITH NOCHECK ADD 
	CONSTRAINT [PK_U51] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_AI_PERSONNES_200702] WITH NOCHECK ADD 
	CONSTRAINT [PK_U51_200702] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_AI_VOYAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U52] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_AI_VOYAGE_200702] WITH NOCHECK ADD 
	CONSTRAINT [PK_U52_200702] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_BATIMENT_ENT] WITH NOCHECK ADD 
	CONSTRAINT [PK_U87] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_BATIMENT_IND] WITH NOCHECK ADD 
	CONSTRAINT [PK_U90] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_CASCO_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [PK_U74] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_CASCO_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U88] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_IARD] WITH NOCHECK ADD 
	CONSTRAINT [PK_URIARD] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_INV_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U86] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_INV_MENAGE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U85] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_LAA] WITH NOCHECK ADD 
	CONSTRAINT [PK_U10] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_OBJET_VALEUR] WITH NOCHECK ADD 
	CONSTRAINT [PK_U89] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_RC_AGRICOLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U63] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_RC_BATEAUX] WITH NOCHECK ADD 
	CONSTRAINT [PK_U66] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_RC_ENTREPRISE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U60] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_RC_IMMEUBLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U64] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_RC_PRIVEE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U62] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[UR_RC_PROFESSIONNELLE] WITH NOCHECK ADD 
	CONSTRAINT [PK_U61] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[VC04_PEPU_manuelle_200702_regroup] WITH NOCHECK ADD 
	CONSTRAINT [PK_VC04_PEPU_regroup] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[N_MTPU2],
		[D_MUTB]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[VICOM] WITH NOCHECK ADD 
	CONSTRAINT [PK_VICOM] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[VIM015_LETTRE_PRENEUR] WITH NOCHECK ADD 
	CONSTRAINT [PK_VIM015_LETTRE_PRENEUR_1__10] PRIMARY KEY  CLUSTERED 
	(
		[R_POL]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[VIM016] WITH NOCHECK ADD 
	CONSTRAINT [PK_VIM016_1__10] PRIMARY KEY  CLUSTERED 
	(
		[R_POL],
		[C_LANGUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[VI_EPARGNE_ATTRIBUTIONS] WITH NOCHECK ADD 
	CONSTRAINT [VI_EPARGNE_ATTRIB_PK2] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_REF],
		[MOIS_REF],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_COUVERTURE],
		[NO_MOUVEMENT_CATAL]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[VI_EPARGNE_COMPTES] WITH NOCHECK ADD 
	CONSTRAINT [VI_EPARGNE_COMPTES_PK1] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_REF],
		[MOIS_REF],
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_ACH] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERS],
		[C_NO_ORD_ADR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_ADR] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERS],
		[C_NO_ORD_ADR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_AET] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERS],
		[C_NO_ORD_ADR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_CAD] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERS],
		[C_NO_ORD_ADR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_CPL] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERS],
		[C_NO_ORD_ADR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_LCA] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERS],
		[C_NO_ORD_ADR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_NPA] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PSTL_CH],
		[NO_PSTL_C_CH]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_RCH] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[C_RUE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[ZC_SUP] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERS],
		[NO_PERS_LIEE],
		[C_TYPE_RELATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[_ACCES_AGCAGT] WITH NOCHECK ADD 
	CONSTRAINT [PK_ACCES_AGCAGT] PRIMARY KEY  CLUSTERED 
	(
		[TACRO],
		[NO_AGENCE],
		[NO_AGENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[_ACCES_DROITS] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[tAcro]
	)  ON [PRIMARY] 


 CREATE  CLUSTERED  INDEX [IDX_AS400_DATE] ON [dbo].[AS400_VIVC]([DATE_COMPTABLE], [TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_COMMACQ_ANNEE] ON [dbo].[COMM_ACQUISITIONS]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [NO_AGENCE], [NO_AGENT], [TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_DEBVIE_DATE] ON [dbo].[DEBITS_VIE_INDIVIDUELLE]([DATE_COMPTABLE], [CODE_BRANCHE], [EDITION_TARIFAIRE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_GENEVC04RES_ANNEE] ON [dbo].[GENEVOISE_VC04_RESERVES]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [TYPE_POLICE], [NO_POLICE], [NO_ASSURE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_HISTCOMMACQUIARD_ANNEE] ON [dbo].[HIST_COMM_ACQUIS_IARD]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [TYPE_POLICE], [NO_UR], [NO_COMMISSION]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_HISTPE1995DETAILS_ANNEE] ON [dbo].[HIST_PE_1995_DETAILS]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [TYPE_POLICE], [NO_POLICE], [NO_UR]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [MC06_FRAIS_IDX] ON [dbo].[MC06_FRAIS]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [TYPE_POLICE], [NO_POLICE], [NO_ASSURE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [MC06_PREST_IDX] ON [dbo].[MC06_PRESTATIONS]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [TYPE_POLICE], [NO_POLICE], [NO_ASSURE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [MC06_SIN_IDX] ON [dbo].[MC06_SINISTRES]([TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_PAIE_TYPOL] ON [dbo].[PAIEMENTS_IARD]([TYPE_POLICE], [NO_POLICE], [NO_UR], [DATE_SURVENANCE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_PAIE_MAL_ANNEE] ON [dbo].[PAIEMENTS_MALADIE]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [TYPE_POLICE], [NO_POLICE], [NO_SIN]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_PAI_VE08] ON [dbo].[PAIEMENTS_VEHICULE]([TYPE_POLICE], [NO_POLICE], [NO_UR], [NO_CONTRAT], [BRANCHE], [DATE_SURVENANCE], [NO_GESTIONNAIRE], [CODE_LIQUIDATION], [NO_PAIEMENT], [ANNEE_COMPTABLE], [MOIS_COMPTABLE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_ANNEE_MOIS] ON [dbo].[PRESTATIONS_ZP]([ANNEE_EMISSION], [MOIS_EMISSION], [TYPE_POLICE], [NO_POLICE], [NO_ASSURE], [NO_ECR]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_ANNEE_MOIS] ON [dbo].[PRESTATIONS_ZP_MELAN]([ANNEE_EMISSION], [MOIS_EMISSION], [TYPE_POLICE], [NO_POLICE], [NO_ASSURE], [NO_ECR]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_PRIEMGL_ANNEE] ON [dbo].[PRIMES_EMISES_GL]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [NO_AGENCE], [NO_AGENT], [TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_PRIEMGL_ANNEE] ON [dbo].[PRIMES_EMISES_GL_MELAN]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [NO_AGENCE], [NO_AGENT], [TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_PRIEMIS_ANNEE] ON [dbo].[PRIMES_EMISES_IARD]([ANNEE_COMPTABLE], [MOIS_COMPTABLE], [NO_AGENCE], [NO_AGENT], [TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_REMUAGT_DATE] ON [dbo].[REMUNERATION_AGENTS]([DATE_COMPTABLE], [NO_AGENCE], [NO_AGENT]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_SIN_TYPOL] ON [dbo].[SINISTRES_IARD]([TYPE_POLICE], [NO_POLICE], [NO_UR], [DATE_SURVENANCE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_SIN_VE08] ON [dbo].[SINISTRES_VEHICULE]([TYPE_POLICE], [NO_POLICE], [NO_UR], [NO_CONTRAT], [BRANCHE], [DATE_SURVENANCE], [NO_GESTIONNAIRE], [CODE_LIQUIDATION], [ANNEE_LIQUIDATION]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_STATPROD_ANNEE_AGCT] ON [dbo].[STATISTIQUE_PRODUCTION]([ANNEE_CATALOGUE], [NO_AGENCE], [NO_AGENT], [MOIS_CATALOGUE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_STATPRODVC04_ANNEE_AGCT] ON [dbo].[STATISTIQUE_PRODUCTION_VC04]([ANNEE_CATALOGUE], [NO_AGENCE], [NO_AGENT], [MOIS_CATALOGUE]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_VI_PRODUCTION] ON [dbo].[VI_PRODUCTION]([PERIODE], [R_POL]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_VI_PRODUCTION_2006] ON [dbo].[VI_PRODUCTION_2006]([PERIODE], [R_POL]) ON [PRIMARY]


 CREATE  CLUSTERED  INDEX [IDX_VI_PRODUCTION_20090304] ON [dbo].[VI_PRODUCTION_20090304]([PERIODE], [R_POL]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_AGENCES_NO_ZONE] ON [dbo].[AGENCES]([NO_AGENCE], [CODE_ZONE]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_AGENTS_NO_GENRE] ON [dbo].[AGENTS]([NO_AGENCE], [NO_AGENT], [CODE_GENRE], [CODE_SOUSGENRE]) ON [PRIMARY]


ALTER TABLE [dbo].[AS400_VIVC] ADD 
	CONSTRAINT [PK_AS400] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


 CREATE  INDEX [IDX_AS400_TYPE_NO] ON [dbo].[AS400_VIVC]([TYPE_POLICE], [NO_POLICE], [DATE_COMPTABLE]) ON [PRIMARY]


ALTER TABLE [dbo].[CAPITAUX_PRODUCTION] ADD 
	CONSTRAINT [PK_CAPITAUXPROD] PRIMARY KEY  NONCLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE],
		[NO_AGENCE],
		[NO_AGENT],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_SEQUENCE]
	)  ON [PRIMARY] 


 CREATE  UNIQUE  INDEX [IDX_CODES_MONNAIES_ISO] ON [dbo].[CODES_MONNAIES_ISO]([CODE_NUM]) ON [PRIMARY]


 CREATE  INDEX [IDX_CODES_PAYS] ON [dbo].[CODES_PAYS]([CANTON_PAYS]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_CODIFRISQUES_TYP_DWH] ON [dbo].[CODIF_RISQUES]([TYPE_POLICE], [NO_RISQUE], [RISQUE_DWH], [LIBERATION_TAXE]) ON [PRIMARY]


ALTER TABLE [dbo].[COMM_ACQUISITIONS] ADD 
	CONSTRAINT [PK_COMMACQ] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_SEQUENCE],
		[NO_AGENCE],
		[NO_AGENT],
		[GENRE_COMMISSION],
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE]
	)  ON [PRIMARY] 


 CREATE  INDEX [IDX_COMMACQ_AGCT] ON [dbo].[COMM_ACQUISITIONS]([NO_AGENCE], [NO_AGENT], [ANNEE_COMPTABLE], [MOIS_COMPTABLE]) ON [PRIMARY]


ALTER TABLE [dbo].[DEBITS_VIE_INDIVIDUELLE] ADD 
	CONSTRAINT [PK_DEBVIE] PRIMARY KEY  NONCLUSTERED 
	(
		[CODE_MONNAIE],
		[DATE_COMPTABLE],
		[CODE_BRANCHE],
		[EDITION_TARIFAIRE],
		[CODE_TARIF]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[GENEVOISE_VC04_RESERVES] ADD 
	CONSTRAINT [PK_GENEVOISE_VC04_RESERVES] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


 CREATE  INDEX [IDX_GENEVC04RES_TYPENO] ON [dbo].[GENEVOISE_VC04_RESERVES]([TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


ALTER TABLE [dbo].[HIST_COMM_ACQUIS_IARD] ADD 
	CONSTRAINT [PK_HIST_COMM_ACQUIS_IARD] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_UR],
		[NO_COMMISSION]
	)  ON [PRIMARY] 


 CREATE  INDEX [IDX_INVGENLOC_TYPENO] ON [dbo].[HIST_INV_TRIM_GENLOC]([TYPE_POLICE], [NO_POLICE]) ON [PRIMARY]


 CREATE  INDEX [IDX_HLI_AGENCT] ON [dbo].[HIST_LIAISONS]([NO_AGENCE], [NO_AGENT]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_HLI_NOPOLSEG] ON [dbo].[HIST_LIAISONS]([NO_POLICE], [TYPE_POLICE], [SEGMENT_MARCHE]) ON [PRIMARY]


 CREATE  INDEX [IDX_HLI_PRENEUR] ON [dbo].[HIST_LIAISONS]([NO_PRENEUR]) ON [PRIMARY]


ALTER TABLE [dbo].[HIST_PE_1995_DETAILS] ADD 
	CONSTRAINT [PK_HIST_PE_1995_DETAILS] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


 CREATE  INDEX [IDX_INVVI03_TYPENO] ON [dbo].[INV_TRIM_VI03]([TYPE_POLICE], [NO_POLICE], [NO_COUVERTURE]) ON [PRIMARY]


 CREATE  INDEX [IDX_INVVI03_TYPENO_200506] ON [dbo].[INV_TRIM_VI03_200506]([TYPE_POLICE], [NO_POLICE], [NO_COUVERTURE]) ON [PRIMARY]


 CREATE  INDEX [IDX_INVTRIMVIEIND_TYPENO] ON [dbo].[INV_TRIM_VIE_INDIVIDUELLE]([TYPE_POLICE], [NO_POLICE], [NO_COUVERTURE]) ON [PRIMARY]


ALTER TABLE [dbo].[MC06_FRAIS] ADD 
	CONSTRAINT [PK__MC06_FRAIS__73A521EA] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_PAIEMENT_SINISTRE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[MC06_PRESTATIONS] ADD 
	CONSTRAINT [PK__MC06_PRESTATIONS__70C8B53F] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_PAIEMENT_SINISTRE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[MC06_SINISTRES] ADD 
	CONSTRAINT [PK__MC06_SINISTRES] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_SINISTRE],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_PLAN],
		[NO_COUCHE],
		[NO_RISQUE],
		[N_DELAI_ATTENTE],
		[NO_AVNT_MC]
	)  ON [PRIMARY] 


 CREATE  INDEX [IDX_MVT_EPAVIEIND1_TYPENO] ON [dbo].[MVT_EPARGNE_VIE_IND]([TYPE_POLICE], [NO_POLICE], [ANNEE_COMPTABLE]) ON [PRIMARY]


ALTER TABLE [dbo].[PAIEMENTS_IARD] ADD 
	CONSTRAINT [PK_PIARD] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_SINISTRE],
		[CODE_LIQUIDATION],
		[NO_PAIEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PAIEMENTS_MALADIE] ADD 
	CONSTRAINT [PK_PAIE_MAL] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_SIN],
		[CODE_COUCHE],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 


 CREATE  UNIQUE  INDEX [IDX_PRENPAY_NO_SEG] ON [dbo].[PRENEURS_PAYEURS]([NO_PERSONNE], [CODE_SEGMENT_MARCHE]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_PRENPAY_NO_NOMPRENADRSEG] ON [dbo].[PRENEURS_PAYEURS]([NO_PERSONNE], [NOM], [PRENOM], [LOCALITE], [RUE], [CODE_SEGMENT_MARCHE]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_PRENPAY_NOMPREN] ON [dbo].[PRENEURS_PAYEURS]([NOM], [PRENOM], [LOCALITE], [RUE], [NO_PERSONNE], [CODE_SEGMENT_MARCHE]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_PRENPAY_NO_SECTACT] ON [dbo].[PRENEURS_PAYEURS]([NO_PERSONNE], [CODE_SECTEUR_ACTIVITE]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_PRENPAY_NO_NBCOLL] ON [dbo].[PRENEURS_PAYEURS]([NO_PERSONNE], [NOMBRE_COLLABORATEURS]) ON [PRIMARY]


 CREATE  UNIQUE  INDEX [IDX_PRENPAY_AGCAGT] ON [dbo].[PRENEURS_PAYEURS]([NO_AGC], [NO_AGT], [NO_PERSONNE], [NOM], [PRENOM], [LOCALITE], [RUE], [CODE_SEGMENT_MARCHE]) ON [PRIMARY]


ALTER TABLE [dbo].[PRESTATIONS_ZP] ADD 
	CONSTRAINT [PK_PRESTATIONS_ZP] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[NO_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRESTATIONS_ZP_MELAN] ADD 
	CONSTRAINT [PK_PRESTATIONS_ZP_MELAN] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_ASSURE],
		[NO_ECR]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRIMES_EMISES_GL] ADD 
	CONSTRAINT [PK_PRIMES_EMISES_GL] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[DATE_DEBIT],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRIMES_EMISES_GL_MELAN] ADD 
	CONSTRAINT [PK_PRIMES_EMISES_GL_MELAN] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[DATE_DEBIT],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[PRIMES_EMISES_IARD] ADD 
	CONSTRAINT [PK_PRIMES_EMISES_IARD] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[REMUNERATION_AGENTS] ADD 
	CONSTRAINT [PK_REMUNERATION_AGENTS] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_AGENCE],
		[NO_AGENT],
		[DATE_COMPTABLE],
		[CODE_HIERARCHIQUE],
		[CODE_COMPAGNIE]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[SINISTRES_IARD] ADD 
	CONSTRAINT [PK_SIARD] PRIMARY KEY  NONCLUSTERED 
	(
		[NO_SINISTRE],
		[CODE_LIQUIDATION]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[STATISTIQUE_PRODUCTION] ADD 
	CONSTRAINT [PK_STATISTIQUE_PRODUCTION] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR_COUCHE],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


ALTER TABLE [dbo].[STATISTIQUE_PRODUCTION_VC04] ADD 
	CONSTRAINT [PK_STATISTIQUE_PRODUCTION_VC04] PRIMARY KEY  NONCLUSTERED 
	(
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_UR_COUCHE],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 


 CREATE  UNIQUE  INDEX [IDX_STATPRODMUT] ON [dbo].[STAT_PROD_MUTATIONS]([TYPE_POLICE], [CODE_MUTATION], [CODE_STATISTIQUE]) ON [PRIMARY]


 CREATE  INDEX [IDX_TEMP_INVVI03_TYPENO] ON [dbo].[TEMP_INV_TRIM_VI03]([TYPE_POLICE], [NO_POLICE], [NO_COUVERTURE]) ON [PRIMARY]


 CREATE  INDEX [IDX_TEMP_INVTRIMVIEIND_TYPENO] ON [dbo].[TEMP_INV_TRIM_VIE_INDIVIDUELLE]([TYPE_POLICE], [NO_POLICE], [NO_COUVERTURE]) ON [PRIMARY]


GRANT  SELECT  ON [dbo].[ADRESSES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[AGENCES]  TO [public]


GRANT  SELECT  ON [dbo].[AGENCES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[AGENCES]  TO [STATO]


GRANT  SELECT  ON [dbo].[AGENCES]  TO [ZZVC0]


GRANT  SELECT  ON [dbo].[AGENCES]  TO [dlzco]


GRANT  SELECT  ON [dbo].[AGENCES]  TO [dlzdi]


GRANT  SELECT  ON [dbo].[AGENCES]  TO [dlzle]


GRANT  SELECT  ON [dbo].[AGENTS]  TO [public]


GRANT  SELECT  ON [dbo].[AGENTS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[AGENTS]  TO [STATO]


GRANT  SELECT  ON [dbo].[AGENTS]  TO [ZZVC0]


GRANT  SELECT  ON [dbo].[AGENTS]  TO [dlzco]


GRANT  SELECT  ON [dbo].[AGENTS]  TO [dlzdi]


GRANT  SELECT  ON [dbo].[AGENTS]  TO [dlzle]


GRANT  SELECT  ON [dbo].[AIC_CODIF1]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[AIC_CODIF1]  TO [user_alibaba]


GRANT  SELECT  ON [dbo].[AIC_CODIF2]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[AIC_CODIF2]  TO [user_alibaba]


GRANT  SELECT  ON [dbo].[AIC_SYSTYPES]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[AIC_SYSTYPES]  TO [user_alibaba]


GRANT  SELECT  ON [dbo].[AS400_NATURES_CODES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[AS400_VIVC]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[ASSURES_AI_PERSONNES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[ASSURES_AI_PERSONNES]  TO [user_alibaba]


GRANT  SELECT  ON [dbo].[ASSURES_MALADIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[ASSURES_VIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[BRANCHES]  TO [public]


GRANT  SELECT  ON [dbo].[BRANCHES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CANTONS_ANNUAIRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHA0747]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHA4601]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHIGOHE]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHL4579]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHL4628]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHV3755]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHV7113]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHVWALS]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [CHZ9690]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [cha8751]


GRANT  SELECT  ON [dbo].[CAPITAUX_PRODUCTION]  TO [walserw]


GRANT  SELECT  ON [dbo].[CARTE_PROD_13]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[CARTE_PROD_13]  TO [cha8751]


GRANT  SELECT  ON [dbo].[CENTRES_COUTS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_CAUSES_SINISTRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_MONNAIES]  TO [public]


GRANT  SELECT  ON [dbo].[CODES_MONNAIES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_MONNAIES_ISO]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_MONNAIES_ISO]  TO [secu_agences]


GRANT  SELECT  ON [dbo].[CODES_MONNAIES_ISO]  TO [secu_agents]


GRANT  SELECT  ON [dbo].[CODES_NATURES_ECRITURES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_NATURES_PAIEMENTS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_PAYS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_PAYS]  TO [secu_agences]


GRANT  SELECT  ON [dbo].[CODES_PAYS]  TO [secu_agents]


GRANT  SELECT  ON [dbo].[CODES_REGION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODES_REGION]  TO [secu_agences]


GRANT  SELECT  ON [dbo].[CODES_REGION]  TO [secu_agents]


GRANT  SELECT  ON [dbo].[CODIF_AGENTS_GE_ZH]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODIF_BONUS_MALUS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODIF_MALADIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODIF_NOGA_2002]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODIF_NOGA_2002]  TO [secu_agences]


GRANT  SELECT  ON [dbo].[CODIF_NOGA_2002]  TO [secu_agents]


GRANT  SELECT  ON [dbo].[CODIF_RISQUES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[CODIF_RISQUES_CORMIS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHA0747]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHA4601]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHA7068]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHIGOHE]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHK2423]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHL4579]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHL4628]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHV3755]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHV7113]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHVWALS]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [CHZ9690]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [cha8751]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [walserw]


GRANT  SELECT  ON [dbo].[COMM_ACQUISITIONS]  TO [zh_compta]


GRANT  SELECT  ON [dbo].[COMM_VIE_INDIVIDUELLE]  TO [CHL4579]


GRANT  SELECT  ON [dbo].[COMM_VIE_INDIVIDUELLE]  TO [CHL4794]


GRANT  SELECT  ON [dbo].[COMM_VIE_INDIVIDUELLE]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[COMM_VIE_INDIVIDUELLE]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[COMM_VIE_INDIVIDUELLE]  TO [cha8751]


GRANT  SELECT  ON [dbo].[COMM_VIE_INDIVIDUELLE]  TO [zh_compta]


GRANT  SELECT  ON [dbo].[COMPTES_OUVERTS]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[COMPTES_OUVERTS]  TO [STATO]


GRANT  SELECT  ON [dbo].[CONVERSION_ACTIVITE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[COURS_MONNAIES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[COURS_MONNAIES]  TO [CHA0747]


GRANT  SELECT  ON [dbo].[COUV_MALADIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[COUV_MALADIE_COLLECTIVE]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[COUV_MALADIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[COUV_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[DEBITS_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[EIS_STATPRODGROUP]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[ENTREPRISES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[GARANTIES_VEHICULE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[GENEVOISE_VC04_DECODIF]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[GENEVOISE_VC04_RESERVES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[HELP_LISTE_COLONNES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[HELP_LISTE_COLVALEURS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[HIST_INVENTAIRE_VI03]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[HIST_INV_TRIM_GENLOC]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[HIST_LIAISONS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[HIST_PAIEMENTS_MALADIE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[IC_USERS]  TO [public]


GRANT  SELECT  ON [dbo].[IC_USERS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[IC_USERS]  TO [secu_agents]


GRANT  SELECT  ON [dbo].[IC_USERS_MODIF]  TO [public]


GRANT  SELECT  ON [dbo].[IC_USERS_MODIF]  TO [secu_agents]


GRANT  SELECT  ON [dbo].[INFO_GENFLEX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INVENTAIRE_MENSUEL_IARD]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INVENTAIRE_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INV_TRIM_VI03]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INV_TRIM_VI03_200506]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INV_TRIM_VI03_RENF]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INV_TRIM_VI07]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INV_TRIM_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[INV_TRIM_VIRAC]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[LATTMAN2]  TO [CHA7068]


GRANT  SELECT  ON [dbo].[LATTMAN2]  TO [CHA7074]


GRANT  SELECT  ON [dbo].[LATTMAN2]  TO [CHK2423]


GRANT  SELECT  ON [dbo].[LATTMAN2]  TO [CHL4649]


GRANT  SELECT  ON [dbo].[LATTMAN2]  TO [CHL4794]


GRANT  SELECT  ON [dbo].[LATTMAN2]  TO [CHL4815]


GRANT  SELECT  ON [dbo].[LATTMAN2]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[LATTMANN]  TO [CHA7068]


GRANT  SELECT  ON [dbo].[LATTMANN]  TO [CHA7074]


GRANT  SELECT  ON [dbo].[LATTMANN]  TO [CHK2423]


GRANT  SELECT  ON [dbo].[LATTMANN]  TO [CHL4649]


GRANT  SELECT  ON [dbo].[LATTMANN]  TO [CHL4794]


GRANT  SELECT  ON [dbo].[LATTMANN]  TO [CHL4815]


GRANT  SELECT  ON [dbo].[LATTMANN]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[LIAISONS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[LIAISONS]  TO [STATO]


GRANT  SELECT  ON [dbo].[LIAISONS]  TO [ZZVC0]


GRANT  SELECT  ON [dbo].[LOCALITE_COMMUNE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MC06_ASSURES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MC06_FRAIS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MC06_PRESTATIONS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MC06_SINISTRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MI05_SAP_PREST]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MVT_CATAL_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MVT_EPARGNE_VIE_IND]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[MZ_RENTES_MIGREES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[NOUVELLES_AFFAIRES_VC04]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[OBJETS_VEHICULE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_ACLA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_AC_AUTRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_AI_CIRCULATION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_AI_PERSONNES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_AI_VOYAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_BATIMENT_ENT]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_BATIMENT_IND]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_CASCO_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_CASCO_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_IARD]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_INV_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_INV_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_LAA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_MALADIE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_OBJET_VALEUR]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_RC_AGRICOLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_RC_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_RC_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_RC_IMMEUBLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_RC_PRIVEE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_RC_PROFESSIONNELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PAIEMENTS_VEHICULE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PARTENAIRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PARTICULIERS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PART_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_ACLA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_AC_AUTRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_AI_CIRCULATION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_AI_PERSONNES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_AI_VOYAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_BATIMENT_ENT]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_BATIMENT_IND]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_CASCO_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_CASCO_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_INV_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_INV_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_LAA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_MALADIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_MALADIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_OBJET_VALEUR]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_RC_AGRICOLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_RC_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_RC_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_RC_IMMEUBLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_RC_PRIVEE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_RC_PROFESSIONNELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_VEHICULE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_VIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[POLICES_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRENEURS_PAYEURS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRENEURS_PAYEURS]  TO [STATO]


GRANT  SELECT  ON [dbo].[PRENEURS_PAYEURS]  TO [ZZVC0]


GRANT  SELECT  ON [dbo].[PRESTATIONS_AI_CIRCULATION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRESTATIONS_AI_PERSONNES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRESTATIONS_AI_VOYAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZP]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZP]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRESTATIONS_ZP]  TO [STATO]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZP_MELAN]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZP_MELAN]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRESTATIONS_ZP_MELAN]  TO [STATO]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZS]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRESTATIONS_ZS]  TO [STATO]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZS_MELAN]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRESTATIONS_ZS_MELAN]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRESTATIONS_ZS_MELAN]  TO [STATO]


GRANT  SELECT  ON [dbo].[PREST_CAP_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PREST_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRIMES_EMISES_GL]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRIMES_EMISES_GL_MELAN]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRIMES_EMISES_IARD]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRIMES_EMISES_IARD]  TO [STATO]


GRANT  SELECT  ON [dbo].[PRIMES_RISQUE_VC04]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[PRIMES_ZP]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRIMES_ZP]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRIMES_ZP]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[PRIMES_ZP]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRIMES_ZP]  TO [STATO]


GRANT  SELECT  ON [dbo].[PRIMES_ZP_MELAN]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRIMES_ZP_MELAN]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRIMES_ZP_MELAN]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[PRIMES_ZP_MELAN]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[PRIMES_ZP_MELAN]  TO [STATO]


GRANT  SELECT  ON [dbo].[QUARTETS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[REMUNERATION_AGENTS]  TO [CHL4579]


GRANT  SELECT  ON [dbo].[REMUNERATION_AGENTS]  TO [CHV2541]


GRANT  SELECT  ON [dbo].[REMUNERATION_AGENTS]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[REMUNERATION_AGENTS]  TO [cha8751]


GRANT  SELECT  ON [dbo].[RENDEMENT_MALADIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[RENDEMENT_POLICES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[RENTES_VIVC]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[RENTES_VI_PERMNONPERM]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[RESERVES_PREST_INVALIDITE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[RESERVES_VIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[RES_RI_VIE_COLLECTIVE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[RISQUES_VEHICULE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SAP_PRESTATIONS_VI]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SC_Année_inventaire]  TO [CHA7068]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_Année_inventaire]  TO [CHL4649]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_Année_inventaire]  TO [CHL4815]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_Année_inventaire]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[SC_T_Date_limite]  TO [CHA7068]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_T_Date_limite]  TO [CHL4649]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_T_Date_limite]  TO [CHL4815]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_T_Date_limite]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[SC_as7i_inv_result]  TO [CHA7068]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_inv_result]  TO [CHL4649]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_inv_result]  TO [CHL4815]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_inv_result]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[SC_as7i_mvt_result]  TO [CHA7068]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_mvt_result]  TO [CHL4649]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_mvt_result]  TO [CHL4815]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_mvt_result]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[SC_as7i_mvt_result_stat_rohrer]  TO [CHA7068]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_mvt_result_stat_rohrer]  TO [CHL4649]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_mvt_result_stat_rohrer]  TO [CHL4815]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_as7i_mvt_result_stat_rohrer]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[SC_t_mvt_stat_zurich]  TO [CHA7068]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_t_mvt_stat_zurich]  TO [CHL4649]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_t_mvt_stat_zurich]  TO [CHL4815]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SC_t_mvt_stat_zurich]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[SINISTRES_ACLA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_AC_AUTRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_AI_CIRCULATION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_AI_PERSONNES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_AI_VOYAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_BATIMENT_ENT]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_BATIMENT_IND]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_CASCO_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_CASCO_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_IARD]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_INV_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_INV_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_LAA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_OBJET_VALEUR]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_RC_AGRICOLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_RC_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_RC_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_RC_IMMEUBLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_RC_PRIVEE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_RC_PROFESSIONNELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SINISTRES_VEHICULE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[SOLDES]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SOLDES]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SOLDES]  TO [STATO]


GRANT  SELECT  ON [dbo].[SOLDES_COMPTES]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SOLDES_COMPTES]  TO [STATO]


GRANT  SELECT  ON [dbo].[SOLDES_MELAN]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SOLDES_MELAN]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[SOLDES_MELAN]  TO [STATO]


GRANT  SELECT  ON [dbo].[STATISTIQUE_PRODUCTION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[STATISTIQUE_PRODUCTION_VC04]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[STAT_ACT_MUTATIONS]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[STAT_PROD_BRANCHES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[STAT_PROD_BRANCHES]  TO [STATO]


GRANT  SELECT  ON [dbo].[STAT_PROD_LIBELLES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[STAT_PROD_MUTATIONS]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[STRUCT_PORTEFEUILLE_ZH]  TO [CHL4579]


GRANT  SELECT  ON [dbo].[STRUCT_PORTEFEUILLE_ZH]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[STRUCT_PORTEFEUILLE_ZH]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_AGENCES]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_AGENCES]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_AGENCES]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_AGENCES]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_AGENCES]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_AGENCES]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_AGENCES]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_BRANCHES]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_BRANCHES]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_BRANCHES]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_BRANCHES]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_BRANCHES]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_BRANCHES]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_BRANCHES]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_COMM_ADMIN_T08]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_ADMIN_T08]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_ADMIN_T08]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_COMM_ADMIN_T08]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_COMM_ADMIN_T08]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_ADMIN_T08]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_COMM_ADMIN_T08]  TO [cha8751]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_MAN]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_MAN]  TO [CHL4713]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_MAN]  TO [STATO]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_MAN]  TO [cha8751]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_MAN_20080624_BAK]  TO [CHL4713]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_T14]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_T14]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_COMM_COURTAGE_T14]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_COMM_COURTAGE_T14]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_T14]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_COMM_COURTAGE_T14]  TO [cha8751]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_T14_MELAN]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_T14_MELAN]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_COMM_COURTAGE_T14_MELAN]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_COMM_COURTAGE_T14_MELAN]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COMM_COURTAGE_T14_MELAN]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_COMM_COURTAGE_T14_MELAN]  TO [cha8751]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COURTIERS_T14]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COURTIERS_T14]  TO [CHL4713]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COURTIERS_T14]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_COURTIERS_T14]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COURTIERS_T14]  TO [STATO]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_COURTIERS_T14]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_DATES_DISPONIBLES]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_DATES_DISPONIBLES]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_DATES_DISPONIBLES]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_DATES_DISPONIBLES]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_DATES_DISPONIBLES]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_DATES_DISPONIBLES]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_DATES_DISPONIBLES]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_DETMI05_T11]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_DETMI05_T11]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_DETMI05_T11]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_DETMI05_T11]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_DETMI05_T11]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_DETMI05_T11]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_MVTS_T00]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_MVTS_T00]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_MVTS_T00]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_MVTS_T00]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_MVTS_T00]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_MVTS_T00]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_MVTS_T00]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_MVTS_T00_MELAN]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_MVTS_T00_MELAN]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_MVTS_T00_MELAN]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_MVTS_T00_MELAN]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_MVTS_T00_MELAN]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_MVTS_T00_MELAN]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_MVTS_T00_MELAN]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_NATURE_MOUVEMENTS]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_NATURE_MOUVEMENTS]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_NATURE_MOUVEMENTS]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_NATURE_MOUVEMENTS]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_NATURE_MOUVEMENTS]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_NATURE_MOUVEMENTS]  TO [cha8751]


GRANT  SELECT  ON [dbo].[S_TYPE_MOUVEMENTS]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_TYPE_MOUVEMENTS]  TO [CHL4713]


GRANT  SELECT  ON [dbo].[S_TYPE_MOUVEMENTS]  TO [CHV2716]


GRANT  SELECT  ON [dbo].[S_TYPE_MOUVEMENTS]  TO [CHY0787]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_TYPE_MOUVEMENTS]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_TYPE_MOUVEMENTS]  TO [cha8751]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_VcComBroker]  TO [CHK2024]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_VcComBroker]  TO [CHL4713]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[S_VcComBroker]  TO [STATO]


GRANT  SELECT  ON [dbo].[S_VcComBroker]  TO [cha8751]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI03]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI03]  TO [CHVSPA1]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI03]  TO [CHVHUBL]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI03_RENF]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI03_RENF]  TO [CHVSPA1]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI03_RENF]  TO [CHVHUBL]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI07]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI07]  TO [CHVSPA1]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VI07]  TO [CHVHUBL]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VIE_INDIVIDUELLE]  TO [CHVSPA1]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VIE_INDIVIDUELLE]  TO [CHVHUBL]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VIRAC]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VIRAC]  TO [CHVSPA1]


GRANT  SELECT  ON [dbo].[TEMP_INV_TRIM_VIRAC]  TO [CHVHUBL]


GRANT  SELECT  ON [dbo].[TEMP_RESERVES_PREST_INVALIDITE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[TEMP_RESERVES_PREST_INVALIDITE]  TO [CHVSPA1]


GRANT  SELECT  ON [dbo].[TEMP_RESERVES_PREST_INVALIDITE]  TO [CHVHUBL]


GRANT  SELECT  ON [dbo].[TEXTES_MALADIE_INDIVIDUELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_ACLA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_AC_AUTRES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_AI_CIRCULATION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_AI_PERSONNES]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_AI_VOYAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_BATIMENT_ENT]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_BATIMENT_IND]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_CASCO_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_CASCO_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_IARD]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_INV_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_INV_MENAGE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_LAA]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_OBJET_VALEUR]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_RC_AGRICOLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_RC_BATEAUX]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_RC_ENTREPRISE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_RC_IMMEUBLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_RC_PRIVEE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[UR_RC_PROFESSIONNELLE]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[VC04_PEPU_manuelle_200702]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[VC04_PEPU_manuelle_200702_regroup]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[VICOM]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[VIM015_LETTRE_PRENEUR]  TO [CHL4649]


GRANT  SELECT  ON [dbo].[VIM016]  TO [CHL4649]


GRANT  SELECT  ON [dbo].[VI_EPARGNE_ATTRIBUTIONS]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[VI_EPARGNE_ATTRIBUTIONS]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[VI_EPARGNE_COMPTES]  TO [secu_siege]


GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[VI_EPARGNE_COMPTES]  TO [CHL4873]


GRANT  SELECT  ON [dbo].[VI_PRODUCTION]  TO [secu_siege]


GRANT  SELECT  ON [dbo].[VI_PRODUCTION_2006]  TO [secu_siege]




