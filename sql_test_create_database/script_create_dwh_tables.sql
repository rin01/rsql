use DWH

CREATE TABLE [dbo].[DWH_ARTEMIS_INV] (
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[MONAT] [char] (6) COLLATE fr_cs_as NOT NULL ,
	[HERK] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[BUKR] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[SGE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[VST] [int] NULL ,
	[PB] [int] NULL ,
	[RISIKO] [int] NULL ,
	[RA] [smallint] NULL ,
	[VZ4] [char] (8) COLLATE fr_cs_as NULL ,
	[VERTRAGSNUM] [int] NOT NULL ,
	[NAME] [varchar] (30) COLLATE fr_cs_as NULL ,
	[VORNAME] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ZUSATZ] [varchar] (30) COLLATE fr_cs_as NULL ,
	[STRASSE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ORT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[DEPOT_TYP] [varchar] (3) COLLATE fr_cs_as NULL ,
	[PLAN_ID] [varchar] (3) COLLATE fr_cs_as NULL ,
	[VALOREN_NUM] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[FONDSLIEFERANT] [varchar] (7) COLLATE fr_cs_as NULL ,
	[TRANSCODE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[STORNO] [varchar] (1) COLLATE fr_cs_as NULL ,
	[PREISTAG] [datetime] NULL ,
	[VORZEICHEN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NEUGELD_FW] [numeric](18, 2) NULL ,
	[WRG_CODE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NEUGELD_CHF] [numeric](18, 2) NULL ,
	[AUFTRAGSCODE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[AUSGABEKOMM] [numeric](18, 2) NULL ,
	[WRG_AK] [varchar] (3) COLLATE fr_cs_as NULL ,
	[AUSGABEKOMM_CHF] [numeric](18, 2) NULL ,
	[PROD_PRAEMIE] [numeric](18, 2) NULL ,
	[ANZAHL_ANTEILE] [numeric](18, 3) NULL ,
	[AUSGABE_RUCKNAHMEKURS] [numeric](17, 5) NULL ,
	[WAHRUNGSKURS] [numeric](18, 5) NULL ,
	[AUM] [numeric](18, 2) NULL ,
	[AUM_CHF] [numeric](18, 2) NULL ,
	[BESTANDEKOMM] [numeric](18, 2) NULL ,
	[VERARB_DATUM] [datetime] NULL ,
	[RECORDTYP] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[PLAN_CODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DEPOTNUMMER] [int] NOT NULL ,
	[POSTLEITZAHL_CODE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[POSTLEITZAHL] [smallint] NULL ,
	[GA_NUMMER] [varchar] (5) COLLATE fr_cs_as NULL ,
	[UAT] [int] NULL ,
	[NOGA_CODE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[MS_ZIELGRUPPE] [int] NULL ,
	[ABSATZKANAL_CO] [varchar] (2) COLLATE fr_cs_as NULL ,
	[EINSTIEGSKOMM] [numeric](18, 2) NULL ,
	[WRG_EK] [varchar] (3) COLLATE fr_cs_as NULL ,
	[EINSTIEGSKOMM_CHF] [numeric](18, 2) NULL ,
	[GEBUHR1] [numeric](18, 2) NULL ,
	[GEBUHR2] [numeric](18, 2) NULL ,
	[GEBUHR3] [numeric](18, 2) NULL ,
	[ZINSERTRAG] [numeric](18, 2) NULL ,
	[ERTRAG_KOMM] [numeric](18, 2) NULL ,
	[ERTRAG_HANDEL] [numeric](18, 2) NULL ,
	[AUFWAND_KOMM] [numeric](18, 2) NULL ,
	[AUFWAND_ZINS] [numeric](18, 2) NULL ,
	[BELEG_NUM] [varchar] (15) COLLATE fr_cs_as NULL ,
	[AUFHEBUNGSGRND] [varchar] (2) COLLATE fr_cs_as NULL ,
	[STUFE_1] [int] NULL ,
	[STUFE_2] [int] NULL ,
	[STUFE_3] [int] NULL ,
	[STUFE_4] [int] NULL ,
	[STUFE_5] [int] NULL ,
	[NO_PERS_DWH_IC] [int] NULL ,
	[NO_VAL_DWH_IC] [varchar] (20) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_ARTEMIS_MVT] (
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[NO_MAA_IC] [int] NOT NULL ,
	[NO_MFN_IC] [int] NOT NULL ,
	[MONAT] [char] (6) COLLATE fr_cs_as NOT NULL ,
	[HERK] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[BUKR] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[SGE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[VST] [int] NULL ,
	[PB] [int] NULL ,
	[RISIKO] [int] NULL ,
	[RA] [smallint] NULL ,
	[VZ4] [char] (8) COLLATE fr_cs_as NULL ,
	[VERTRAGSNUM] [int] NOT NULL ,
	[NAME] [varchar] (30) COLLATE fr_cs_as NULL ,
	[VORNAME] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ZUSATZ] [varchar] (30) COLLATE fr_cs_as NULL ,
	[STRASSE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[ORT] [varchar] (30) COLLATE fr_cs_as NULL ,
	[DEPOT_TYP] [varchar] (3) COLLATE fr_cs_as NULL ,
	[PLAN_ID] [varchar] (3) COLLATE fr_cs_as NULL ,
	[VALOREN_NUM] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[FONDSLIEFERANT] [varchar] (7) COLLATE fr_cs_as NULL ,
	[TRANSCODE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[STORNO] [varchar] (1) COLLATE fr_cs_as NULL ,
	[PREISTAG] [datetime] NULL ,
	[VORZEICHEN] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NEUGELD_FW] [numeric](18, 2) NULL ,
	[WRG_CODE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NEUGELD_CHF] [numeric](18, 2) NULL ,
	[AUFTRAGSCODE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[AUSGABEKOMM] [numeric](18, 2) NULL ,
	[WRG_AK] [varchar] (3) COLLATE fr_cs_as NULL ,
	[AUSGABEKOMM_CHF] [numeric](18, 2) NULL ,
	[PROD_PRAEMIE] [numeric](18, 2) NULL ,
	[ANZAHL_ANTEILE] [numeric](18, 3) NULL ,
	[AUSGABE_RUCKNAHMEKURS] [numeric](17, 5) NULL ,
	[WAHRUNGSKURS] [numeric](18, 5) NULL ,
	[AUM] [numeric](18, 2) NULL ,
	[AUM_CHF] [numeric](18, 2) NULL ,
	[BESTANDEKOMM] [numeric](18, 2) NULL ,
	[VERARB_DATUM] [datetime] NULL ,
	[RECORDTYP] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[PLAN_CODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DEPOTNUMMER] [int] NOT NULL ,
	[POSTLEITZAHL_CODE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[POSTLEITZAHL] [smallint] NULL ,
	[GA_NUMMER] [varchar] (5) COLLATE fr_cs_as NULL ,
	[UAT] [int] NULL ,
	[NOGA_CODE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[MS_ZIELGRUPPE] [int] NULL ,
	[ABSATZKANAL_CO] [varchar] (2) COLLATE fr_cs_as NULL ,
	[EINSTIEGSKOMM] [numeric](18, 2) NULL ,
	[WRG_EK] [varchar] (3) COLLATE fr_cs_as NULL ,
	[EINSTIEGSKOMM_CHF] [numeric](18, 2) NULL ,
	[GEBUHR1] [numeric](18, 2) NULL ,
	[GEBUHR2] [numeric](18, 2) NULL ,
	[GEBUHR3] [numeric](18, 2) NULL ,
	[ZINSERTRAG] [numeric](18, 2) NULL ,
	[ERTRAG_KOMM] [numeric](18, 2) NULL ,
	[ERTRAG_HANDEL] [numeric](18, 2) NULL ,
	[AUFWAND_KOMM] [numeric](18, 2) NULL ,
	[AUFWAND_ZINS] [numeric](18, 2) NULL ,
	[BELEG_NUM] [varchar] (15) COLLATE fr_cs_as NULL ,
	[AUFHEBUNGSGRND] [varchar] (2) COLLATE fr_cs_as NULL ,
	[STUFE_1] [int] NULL ,
	[STUFE_2] [int] NULL ,
	[STUFE_3] [int] NULL ,
	[STUFE_4] [int] NULL ,
	[STUFE_5] [int] NULL ,
	[NO_PERS_DWH_IC] [int] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_CLIENTS] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[PERIODE_LIVRAISON] [char] (6) COLLATE fr_cs_as NOT NULL ,
	[ENTITE_JURIDIQUE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [smallint] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [char] (17) COLLATE fr_cs_as NOT NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[SBU_CLIENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[GEPU_NO_PERSONNE] [varchar] (26) COLLATE fr_cs_as NULL ,
	[NOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[PRENOM] [varchar] (30) COLLATE fr_cs_as NULL ,
	[TYPE_PERSONNE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_PROFESSION] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PROFESSION] [varchar] (30) COLLATE fr_cs_as NULL ,
	[DOMAINE_ACTIVITE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[STATUT_PROF] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ETAT_CIVIL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TITRE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SEXE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[ZNO_PERSONNE] [varchar] (7) COLLATE fr_cs_as NULL ,
	[NATIONALITE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_LANGUE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[PUBLICITE_STOP] [varchar] (1) COLLATE fr_cs_as NULL ,
	[RUE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[NO_MAISON] [varchar] (5) COLLATE fr_cs_as NULL ,
	[NO_POSTAL] [varchar] (6) COLLATE fr_cs_as NULL ,
	[LOCALITE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[CODE_CANTON] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TELEPHONE] [varchar] (15) COLLATE fr_cs_as NULL ,
	[CODE_PAYS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CATEGORIE_CLIENT_SBS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NOTE_SBS] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NOMBRE_COLLABORATEURS] [int] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_CODIF_COMPTES_SAP] (
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[TYPE_RESERVE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[CONSOLIDATION] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[COMPTE_SAP_BILAN] [int] NOT NULL ,
	[COMPTE_SAP_PP] [int] NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_CODIF_LOB] (
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[LOB] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[LOB2] [varchar] (5) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_CODIF_LOB2_VZ4] (
	[LOB2] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_CODIF_PC] (
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[PC] [varchar] (5) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_CODIF_SOURCE_IC] (
	[TBL] [varchar] (40) COLLATE fr_cs_as NOT NULL ,
	[SOURCE_IC] [varchar] (10) COLLATE fr_cs_as NOT NULL ,
	[TBL_SOURCE] [varchar] (30) COLLATE fr_cs_as NOT NULL ,
	[LIBELLE] [varchar] (500) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_COMMISSIONS] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[NO_SEQUENCE_IC] [int] NOT NULL ,
	[NO_AGENCE_IC] [smallint] NOT NULL ,
	[NO_AGENT_IC] [smallint] NOT NULL ,
	[GENRE_COMMISSION_IC] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[NO_COMMISSIONNE] [varchar] (9) COLLATE fr_cs_as NULL ,
	[NO_RISQUE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[ALTERNATIVE_RISQUE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[SOCIETE_GROUPE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_AGENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CROSS_SELLING] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CODE_FONCTION_PB] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_FONCTION_KUBE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_KUBE] [varchar] (10) COLLATE fr_cs_as NULL ,
	[NO_CENTRE_COUT] [varchar] (10) COLLATE fr_cs_as NULL ,
	[CODE_AFF_VIE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_CC_COURTIER] [varchar] (10) COLLATE fr_cs_as NULL ,
	[NO_COMPTE_COURTIER] [varchar] (10) COLLATE fr_cs_as NULL ,
	[COMM_ACQUISITION] [numeric](13, 2) NULL ,
	[COMM_RENOUVELLEMENT] [numeric](13, 2) NULL ,
	[COMM_GESTION] [numeric](13, 2) NULL ,
	[COMM_FORFAITAIRE] [numeric](13, 2) NULL ,
	[COMM_PASSAGE] [numeric](13, 2) NULL ,
	[COMM_SPECIALE] [numeric](13, 2) NULL ,
	[MONTANT_NOUV_PROD_LT] [numeric](13, 2) NULL ,
	[MONTANT_NOUV_PROD_CT] [numeric](13, 2) NULL ,
	[MONTANT_MODIF_PROD] [numeric](13, 2) NULL ,
	[MONTANT_RENOUV] [numeric](13, 2) NULL ,
	[NOMBRE_RISQUE_LT] [int] NULL ,
	[NOMBRE_RISQUE_CT] [int] NULL ,
	[NOMBRE_RISQUE_MODIF_PROD] [int] NULL ,
	[NOMBRE_RISQUE_RENOUV] [int] NULL ,
	[COMM_GESTION_COURTIER] [numeric](13, 2) NULL ,
	[COMM_PARTICIPATION_COURTIER] [numeric](13, 2) NULL ,
	[SOURCE_IC] [varchar] (5) COLLATE fr_cs_as NULL ,
	[FIBU_RELEVANT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[LEB_ART_C] [varchar] (2) COLLATE fr_cs_as NULL ,
	[IC_CODE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[RV_CODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[GESTION_SAP_IC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COMPTE_SAP_IC] [varchar] (6) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_DESCRIPTION_EXPORT_FILES] (
	[GEfile] [varchar] (10) COLLATE fr_cs_as NOT NULL ,
	[ZHout_col_no] [smallint] NOT NULL ,
	[ZHout_col_name] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[ZHout_col_type] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[ZHout_col_defval0] [varchar] (200) COLLATE fr_cs_as NULL ,
	[ZHout_col_length] [smallint] NOT NULL ,
	[GEfile_col_no] [smallint] NULL ,
	[GEfile_col_name] [varchar] (50) COLLATE fr_cs_as NULL ,
	[GEfile_col_length] [smallint] NULL ,
	[ZHdoc_nr] [varchar] (5) COLLATE fr_cs_as NULL ,
	[ZHdoc_datenelement] [varchar] (200) COLLATE fr_cs_as NULL ,
	[ZHdoc_type] [varchar] (20) COLLATE fr_cs_as NULL ,
	[remarques] [varchar] (250) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_EXTERNAL_COMM] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[DATE_COMPTABLE] [datetime] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[NO_SEQUENCE_IC] [int] IDENTITY (1, 1) NOT NULL ,
	[NO_AGENCE] [smallint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[SEGMENT] [tinyint] NOT NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[COMM_ACQUISITION] [numeric](12, 2) NULL ,
	[COMM_SPECIALE] [numeric](12, 2) NULL ,
	[COMM_GESTION_COURTIER] [numeric](12, 2) NULL ,
	[NO_IDENTIFICATION] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[COMM_GESTION] [numeric](12, 2) NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_EXTERNAL_COMM_REF] (
	[NO_COMPAGNIE] [smallint] NOT NULL ,
	[NOM_COMPAGNIE] [varchar] (20) COLLATE fr_cs_as NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_IDENTIFICATION] [int] NOT NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[COMPTE_SAP] [varchar] (12) COLLATE fr_cs_as NOT NULL ,
	[NO_AGENCE] [smallint] NOT NULL ,
	[NO_AGENT] [smallint] NOT NULL ,
	[SEGMENT] [tinyint] NOT NULL ,
	[NO_CIE_REASS] [int] NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NOM_INTERNATIONAL] [varchar] (10) COLLATE fr_cs_as NULL ,
	[NO_CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NOM_CONSOLIDATION] [varchar] (20) COLLATE fr_cs_as NULL ,
	[NO_TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NOM_TYPE_AFFAIRE] [varchar] (30) COLLATE fr_cs_as NULL ,
	[DESCRIPTION] [varchar] (255) COLLATE fr_cs_as NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_TYPE_COMM] [smallint] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_EXTERNAL_DATA] (
	[ID] [int] NOT NULL ,
	[TBL] [varchar] (30) COLLATE fr_cs_as NOT NULL ,
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NULL ,
	[NO_UR] [tinyint] NULL ,
	[NO_SEQUENCE] [smallint] NULL ,
	[NO_SINISTRE] [varchar] (19) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CROSS_SELLING] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT] [numeric](12, 2) NULL ,
	[MONTANT_2] [numeric](12, 2) NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NATURE_SINISTRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_SURVENANCE] [datetime] NULL ,
	[COMM_GESTION_COURTIER] [numeric](13, 2) NULL ,
	[DATE_OUVERTURE] [datetime] NULL ,
	[SEGMENT] [tinyint] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[TYPE_MONTANT] [smallint] NULL ,
	[MONTANT_3] [numeric](12, 2) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_EXTERNAL_DATA_BUFFER] (
	[ID] [int] NOT NULL ,
	[TBL] [varchar] (30) COLLATE fr_cs_as NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NULL ,
	[NO_UR] [tinyint] NULL ,
	[NO_SEQUENCE] [smallint] NULL ,
	[NO_SINISTRE] [varchar] (19) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CROSS_SELLING] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT] [varchar] (14) COLLATE fr_cs_as NULL ,
	[MONTANT_2] [varchar] (14) COLLATE fr_cs_as NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NATURE_SINISTRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_SURVENANCE] [datetime] NULL ,
	[COMM_GESTION_COURTIER] [varchar] (15) COLLATE fr_cs_as NULL ,
	[DATE_OUVERTURE] [datetime] NULL ,
	[SEGMENT] [tinyint] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[TYPE_MONTANT] [smallint] NULL ,
	[MONTANT_3] [numeric](12, 2) NULL ,
	[DESCRIPTION] [varchar] (255) COLLATE fr_cs_as NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_INTERFACE_SAP_TMP] (
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[NO_COMPAGNIE_IC] [int] NOT NULL ,
	[BUK] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[TYPE_RESERVE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[RISQUE_DWH] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[CIE_COASS_REASS] [int] NOT NULL ,
	[SEGMENT] [tinyint] NOT NULL ,
	[MONTANT_RESERVE] [numeric](12, 2) NULL ,
	[ALT_DUMMY_PK] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_INTERFACE_SAP_TMP2] (
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[NO_COMPAGNIE_IC] [int] NOT NULL ,
	[BUK] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_MONNAIE_ISO] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[CIE_COASS_REASS] [int] NOT NULL ,
	[SEGMENT] [tinyint] NOT NULL ,
	[COMPTE_SAP_BILAN] [int] NOT NULL ,
	[COMPTE_SAP_PP] [int] NOT NULL ,
	[LOB2] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[PC] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[MONTANT_RESERVE] [numeric](12, 2) NOT NULL ,
	[MOUVEMENT] [numeric](12, 2) NOT NULL ,
	[GAC] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[ALT_DUMMY_PK] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_INVENTAIRE] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [int] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[DATE_MUTATION] [datetime] NULL ,
	[CODE_EVENEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_STATUT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ZCAUSE_ANNULATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CAUSE_ANNULATION] [varchar] (23) COLLATE fr_cs_as NULL ,
	[PROLONGATION_CONTRAT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[FLAG_AFF_REMPLACEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[PRIME_TARIF] [numeric](12, 2) NULL ,
	[BONUS_MALUS] [varchar] (3) COLLATE fr_cs_as NULL ,
	[MONTANT_BONUS_MALUS] [numeric](12, 2) NULL ,
	[MODE_PAIEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_COMMISSIONNE] [varchar] (9) COLLATE fr_cs_as NULL ,
	[DATE_CGA] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CODE_TARIF] [varchar] (9) COLLATE fr_cs_as NULL ,
	[GROUPE_TARIF] [varchar] (7) COLLATE fr_cs_as NULL ,
	[TYPE_PRIME] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ETAT_PRIME] [numeric](12, 2) NULL ,
	[SOMME_ASSURANCE] [numeric](12, 2) NULL ,
	[RESERVE_BILAN] [numeric](12, 2) NULL ,
	[REPORT_PRIME] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (5) COLLATE fr_cs_as NULL ,
	[SEGMENT_COURANT_IC] [tinyint] NULL ,
	[IC_CODE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[RV_CODE] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_KONTIERUNG_GA] (
	[NUMERO] [int] IDENTITY (1, 1) NOT NULL ,
	[BET] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[GAC] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[RV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NAT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[VZ4] [varchar] (8) COLLATE fr_cs_as NULL ,
	[BUK] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[KONTO] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[KONTO_BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[DLK] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[DLK_BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_KONTIERUNG_GL] (
	[NUMERO] [int] IDENTITY (1, 1) NOT NULL ,
	[BET] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[SCHADENART] [varchar] (2) COLLATE fr_cs_as NULL ,
	[BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[GAC] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[RV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NAT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[VZ4] [varchar] (8) COLLATE fr_cs_as NULL ,
	[BUK] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[KONTO] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[KONTO_BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[DLK] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[DLK_BUK] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[DLK_BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_PAIEMENTS] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[NO_SINISTRE_IC] [char] (19) COLLATE fr_cs_as NOT NULL ,
	[CODE_LIQUIDATION_IC] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[NO_PAIEMENT_IC] [int] NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[NO_SINISTRE] [varchar] (19) COLLATE fr_cs_as NULL ,
	[NO_PAIEMENT] [smallint] NULL ,
	[INDEX_PAIEMENT] [smallint] NULL ,
	[NO_PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[EVENTUALITE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ZTYPE_DEDOMMAGEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[TYPE_DEDOMMAGEMENT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[FILLER1] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NATURE_RECUPERATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[MONTANT_PAIEMENT] [numeric](12, 2) NULL ,
	[MONTANT_RENTE] [numeric](12, 2) NULL ,
	[COMPL_RENCHERISSEMENT] [numeric](12, 2) NULL ,
	[FRAIS_GESTION_INTERSIN] [numeric](12, 2) NULL ,
	[MODE_PAIEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[DATE_PAIEMENT] [datetime] NULL ,
	[FLAG_PAIEMENT_LIBRE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_CONTRACTANT] [int] NULL ,
	[UNTER_VA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[RENTE_IDENTIFICATION] [varchar] (17) COLLATE fr_cs_as NULL ,
	[TAUX_JOURNALIER] [numeric](12, 2) NULL ,
	[TAUX_HORAIRE] [numeric](12, 2) NULL ,
	[NOMBRE_JOURS] [int] NULL ,
	[NOMBRE_HEURES] [int] NULL ,
	[ARTICLE_REDUCTION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[POURCENT_REDUCTION] [smallint] NULL ,
	[MONTANT_REDUCTION] [numeric](12, 2) NULL ,
	[MONTANT_RETENUE] [numeric](12, 2) NULL ,
	[TAUX_INCAPACITE_TRAVAIL] [smallint] NULL ,
	[INDEMNITE] [numeric](12, 2) NULL ,
	[NOMBRE_JOUR_PAYES] [smallint] NULL ,
	[NOMBRE_CONSULTATIONS] [smallint] NULL ,
	[TYPE_PRESTATION] [varchar] (4) COLLATE fr_cs_as NULL ,
	[SOMME_ASSURANCE] [numeric](12, 2) NULL ,
	[DELAI_ATTENTE] [smallint] NULL ,
	[FRANCHISE] [numeric](12, 2) NULL ,
	[DUREE_PRESTATION] [smallint] NULL ,
	[DATE_FIN] [datetime] NULL ,
	[NO_COMPTEUR] [int] IDENTITY (1, 1) NOT NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[RISQUE_IC] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NATURE_SINISTRE_IC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT_IC] [tinyint] NULL ,
	[NO_AGENCE_IC] [smallint] NULL ,
	[NO_AGENT_IC] [smallint] NULL ,
	[NO_CPTE_IC] [varchar] (8) COLLATE fr_cs_as NULL ,
	[INTERNATIONAL_IC] [varchar] (1) COLLATE fr_cs_as NULL ,
	[EMPFANGER_Z1] [varchar] (30) COLLATE fr_cs_as NULL ,
	[EMPFANGER_Z2] [varchar] (30) COLLATE fr_cs_as NULL ,
	[EMPFANGER_Z3] [varchar] (30) COLLATE fr_cs_as NULL ,
	[EMPFANGER_Z4] [varchar] (30) COLLATE fr_cs_as NULL ,
	[VISEUR] [varchar] (8) COLLATE fr_cs_as NULL ,
	[ERFASSER] [varchar] (8) COLLATE fr_cs_as NULL ,
	[IC_CODE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[RV_CODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DATE_VAL_ECR_IC] [datetime] NULL ,
	[EDITION_TARIF_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_FONDATION_IC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[GESTION_SAP_IC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COMPTE_SAP_IC] [varchar] (6) COLLATE fr_cs_as NULL ,
	[SBU_IC] [varchar] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_PARAMETRES] (
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[ANNEE_COMPTABLE] [smallint] NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_POLICES] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[DATE_SUSPENSION] [datetime] NULL ,
	[CAUSE_ANNULATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[ZCAUSE_ANNULATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NOTE_SBS] [int] NULL ,
	[AGENCE_SBS] [varchar] (8) COLLATE fr_cs_as NULL ,
	[ID_PERSONNEL_SBS] [varchar] (8) COLLATE fr_cs_as NULL ,
	[CODE_COUVERTURE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_COASS] [varchar] (1) COLLATE fr_cs_as NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[BRANCHENKENNUNG] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_POLICES_COMPLEMENT] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[SYSTEME_BRANCHE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[NB_ASSURES] [int] NULL ,
	[RESERVE_BILAN] [numeric](12, 2) NULL ,
	[PRIME_TARIF] [numeric](12, 2) NULL ,
	[MODE_PAIEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[PROLONGATION_TACITE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[POLICE_REMPLACEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_POL_REMPLACEE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ASSUREUR_PRECEDENT] [varchar] (5) COLLATE fr_cs_as NULL ,
	[TAUX_COASS_ZGROUP] [numeric](6, 3) NULL ,
	[SOMME_ASSUREE] [numeric](15, 2) NULL ,
	[DATE_EINV_FZ] [datetime] NULL ,
	[U_BEG_D] [datetime] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_POLICES_COMPLEMENT_MISSING_TMP] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[SYSTEME_BRANCHE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[DATE_CREATION] [datetime] NULL ,
	[DATE_ECHEANCE] [datetime] NULL ,
	[NB_ASSURES] [int] NULL ,
	[RESERVE_BILAN] [numeric](12, 2) NULL ,
	[PRIME_TARIF] [numeric](12, 2) NULL ,
	[MODE_PAIEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[PROLONGATION_TACITE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[POLICE_REMPLACEMENT] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_POL_REMPLACEE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ASSUREUR_PRECEDENT] [varchar] (5) COLLATE fr_cs_as NULL ,
	[TAUX_COASS_ZGROUP] [numeric](6, 3) NULL ,
	[SOMME_ASSUREE] [numeric](15, 2) NULL ,
	[DATE_EINV_FZ] [datetime] NULL ,
	[U_BEG_D] [datetime] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_POLICES_GCIE_SPLITT] (
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NOGA_C] [varchar] (5) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_POLICES_GCIE_SPLITT_MC06] (
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NOGA_C] [varchar] (5) COLLATE fr_cs_as NULL ,
	[NO_CENTRE_COUT] [varchar] (10) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_POLICES_MISSING_TMP] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[DATE_ANNULATION] [datetime] NULL ,
	[DATE_SUSPENSION] [datetime] NULL ,
	[CAUSE_ANNULATION] [varchar] (3) COLLATE fr_cs_as NULL ,
	[ZCAUSE_ANNULATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NOTE_SBS] [int] NULL ,
	[AGENCE_SBS] [varchar] (8) COLLATE fr_cs_as NULL ,
	[ID_PERSONNEL_SBS] [varchar] (8) COLLATE fr_cs_as NULL ,
	[CODE_COUVERTURE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[CODE_COASS] [varchar] (1) COLLATE fr_cs_as NULL ,
	[STATUT_POLICE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[BRANCHENKENNUNG] [varchar] (8) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_PRIMES] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[DATE_TRAITEMENT_IC] [datetime] NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[CODE_EVENEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANNEE_SOUSCRIPTION] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MOIS_ANNEE_COMPTABLE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[DATE_DEBUT_PRIME] [datetime] NULL ,
	[DATE_FIN_PRIME] [datetime] NULL ,
	[LIBERATION_TAXES] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ANNEE_TAXES] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CLASSE_TAXES] [varchar] (2) COLLATE fr_cs_as NULL ,
	[POSITION_TARIF_TAXES] [varchar] (4) COLLATE fr_cs_as NULL ,
	[PRIME_TARIF] [numeric](12, 2) NULL ,
	[FRAIS_FRACTIONNEMENT] [numeric](12, 2) NULL ,
	[FRAIS_SUSPENSION] [numeric](12, 2) NULL ,
	[INTERETS_RETARD] [numeric](12, 2) NULL ,
	[INTERETS_PRET] [numeric](12, 2) NULL ,
	[LIBERATION_PRIME] [numeric](12, 2) NULL ,
	[PARTICIPATION_EXCEDENT] [numeric](12, 2) NULL ,
	[COMPTE_PE] [numeric](12, 2) NULL ,
	[TF_PRIME_TARIF] [numeric](12, 2) NULL ,
	[TF_FRAIS_FRACTIONNEMENT] [numeric](12, 2) NULL ,
	[TF_PARTICIPATION_EXCEDENT] [numeric](12, 2) NULL ,
	[CONTRIBUTION_PREV_ACC] [numeric](12, 2) NULL ,
	[CONTRIBUTION_74_LCR] [numeric](12, 2) NULL ,
	[CONTRIBUTION_76_LCR] [numeric](12, 2) NULL ,
	[PRIME_EMISE] [numeric](12, 2) NULL ,
	[IMPOT_ARGOVIEN] [numeric](12, 2) NULL ,
	[MASSE_SALARIALE_LAA] [numeric](12, 2) NULL ,
	[FRAIS_GESTION_LAA] [numeric](12, 2) NULL ,
	[PRIME_RENCHERISSEMENT_LAA] [numeric](12, 2) NULL ,
	[RABAIS_GESTION_PRENEUR_LAA] [numeric](12, 2) NULL ,
	[RABAIS_PRIME_ELEVEE_LAA] [numeric](12, 2) NULL ,
	[RABAIS_RURAL_LAA] [numeric](12, 2) NULL ,
	[RABAIS_COLLABORATEUR] [numeric](12, 2) NULL ,
	[AUTRES_RABAIS] [numeric](12, 2) NULL ,
	[COMMISSION_COASS_GENEVOISE] [numeric](12, 2) NULL ,
	[COMMISSION_COURTIER] [numeric](12, 2) NULL ,
	[PRIME_MEMBRE_POOL] [numeric](12, 2) NULL ,
	[PRIME_NON_MEMBRE_POOL] [numeric](12, 2) NULL ,
	[PP_EPARGNE] [numeric](12, 2) NULL ,
	[PP_RISQUE] [numeric](12, 2) NULL ,
	[PP_CHARGEMENT] [numeric](12, 2) NULL ,
	[PP_RENCHERISSEMENT] [numeric](12, 2) NULL ,
	[PP_GLOBALE] [numeric](12, 2) NULL ,
	[PU_LIBRE_PASSAGE] [numeric](12, 2) NULL ,
	[PU_BONUS] [numeric](12, 2) NULL ,
	[PU_COUVERTURE] [numeric](12, 2) NULL ,
	[PU_MESURE_SPECIALE] [numeric](12, 2) NULL ,
	[PU_ACHAT_ANNEES] [numeric](12, 2) NULL ,
	[PU_GLOBALE] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_AGENCE_IC] [smallint] NULL ,
	[NO_AGENT_IC] [smallint] NULL ,
	[NO_CPTE_IC] [varchar] (10) COLLATE fr_cs_as NULL ,
	[IC_CODE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[RV_CODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_ECR_IC] [int] NULL ,
	[EDITION_TARIF_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[GESTION_SAP_IC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COMPTE_SAP_IC] [varchar] (6) COLLATE fr_cs_as NULL ,
	[C_NAT_CMPT_IC] [varchar] (4) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_PRIMES_TEMP] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[DATE_TRAITEMENT_IC] [datetime] NOT NULL ,
	[NO_MOUVEMENT] [int] NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[CODE_EVENEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[ANNEE_SOUSCRIPTION] [varchar] (4) COLLATE fr_cs_as NULL ,
	[MOIS_ANNEE_COMPTABLE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[DATE_DEBUT_PRIME] [datetime] NULL ,
	[DATE_FIN_PRIME] [datetime] NULL ,
	[LIBERATION_TAXES] [varchar] (1) COLLATE fr_cs_as NULL ,
	[ANNEE_TAXES] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CLASSE_TAXES] [varchar] (2) COLLATE fr_cs_as NULL ,
	[POSITION_TARIF_TAXES] [varchar] (4) COLLATE fr_cs_as NULL ,
	[PRIME_TARIF] [numeric](12, 2) NULL ,
	[FRAIS_FRACTIONNEMENT] [numeric](12, 2) NULL ,
	[FRAIS_SUSPENSION] [numeric](12, 2) NULL ,
	[INTERETS_RETARD] [numeric](12, 2) NULL ,
	[INTERETS_PRET] [numeric](12, 2) NULL ,
	[LIBERATION_PRIME] [numeric](12, 2) NULL ,
	[PARTICIPATION_EXCEDENT] [numeric](12, 2) NULL ,
	[COMPTE_PE] [numeric](12, 2) NULL ,
	[TF_PRIME_TARIF] [numeric](12, 2) NULL ,
	[TF_FRAIS_FRACTIONNEMENT] [numeric](12, 2) NULL ,
	[TF_PARTICIPATION_EXCEDENT] [numeric](12, 2) NULL ,
	[CONTRIBUTION_PREV_ACC] [numeric](12, 2) NULL ,
	[CONTRIBUTION_74_LCR] [numeric](12, 2) NULL ,
	[CONTRIBUTION_76_LCR] [numeric](12, 2) NULL ,
	[PRIME_EMISE] [numeric](12, 2) NULL ,
	[IMPOT_ARGOVIEN] [numeric](12, 2) NULL ,
	[MASSE_SALARIALE_LAA] [numeric](12, 2) NULL ,
	[FRAIS_GESTION_LAA] [numeric](12, 2) NULL ,
	[PRIME_RENCHERISSEMENT_LAA] [numeric](12, 2) NULL ,
	[RABAIS_GESTION_PRENEUR_LAA] [numeric](12, 2) NULL ,
	[RABAIS_PRIME_ELEVEE_LAA] [numeric](12, 2) NULL ,
	[RABAIS_RURAL_LAA] [numeric](12, 2) NULL ,
	[RABAIS_COLLABORATEUR] [numeric](12, 2) NULL ,
	[AUTRES_RABAIS] [numeric](12, 2) NULL ,
	[COMMISSION_COASS_GENEVOISE] [numeric](12, 2) NULL ,
	[COMMISSION_COURTIER] [numeric](12, 2) NULL ,
	[PRIME_MEMBRE_POOL] [numeric](12, 2) NULL ,
	[PRIME_NON_MEMBRE_POOL] [numeric](12, 2) NULL ,
	[PP_EPARGNE] [numeric](12, 2) NULL ,
	[PP_RISQUE] [numeric](12, 2) NULL ,
	[PP_CHARGEMENT] [numeric](12, 2) NULL ,
	[PP_RENCHERISSEMENT] [numeric](12, 2) NULL ,
	[PP_GLOBALE] [numeric](12, 2) NULL ,
	[PU_LIBRE_PASSAGE] [numeric](12, 2) NULL ,
	[PU_BONUS] [numeric](12, 2) NULL ,
	[PU_COUVERTURE] [numeric](12, 2) NULL ,
	[PU_MESURE_SPECIALE] [numeric](12, 2) NULL ,
	[PU_ACHAT_ANNEES] [numeric](12, 2) NULL ,
	[PU_GLOBALE] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_AGENCE_IC] [smallint] NULL ,
	[NO_AGENT_IC] [smallint] NULL ,
	[NO_CPTE_IC] [varchar] (10) COLLATE fr_cs_as NULL ,
	[IC_CODE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[RV_CODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NO_ECR_IC] [int] NULL ,
	[EDITION_TARIF_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[GESTION_SAP_IC] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COMPTE_SAP_IC] [varchar] (6) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_REPORT_PRIMES] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [int] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[DATE_TRAITEMENT_IC] [datetime] NOT NULL ,
	[NO_MOUVEMENT] [int] IDENTITY (1, 1) NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[CODE_EVENEMENT] [varchar] (2) COLLATE fr_cs_as NULL ,
	[PRIME_BASE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_COURANTE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_PRECEDENTE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_SUIVANTE] [numeric](12, 2) NULL ,
	[REPORT_ENTREE_MUTATION] [numeric](12, 2) NULL ,
	[REPORT_SORTIE_MUTATION] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[IC_CODE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[RV_CODE] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_RESERVES] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [int] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[NO_SINISTRE_IC] [char] (19) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[NO_SINISTRE] [varchar] (19) COLLATE fr_cs_as NULL ,
	[NO_RESERVE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[EVENTUALITE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_RESERVE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[MONTANT_RESERVE] [numeric](12, 2) NULL ,
	[RESERVE_RENTE] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[RUCKSTELLUNGSART] [varchar] (1) COLLATE fr_cs_as NULL ,
	[RENTENNUMMER] [varchar] (10) COLLATE fr_cs_as NULL ,
	[ERFASSUNGSDATUM] [datetime] NULL ,
	[IC_CODE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[RV_CODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SEGMENT_IC] [tinyint] NULL ,
	[SEGMENT_COURANT_IC] [tinyint] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_RESERVES_BUFFER] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[TYPE_MONTANT] [smallint] NOT NULL ,
	[LOB2] [varchar] (5) COLLATE fr_cs_as NOT NULL ,
	[SCHADEN_JAHR] [smallint] NOT NULL ,
	[KS] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[RESERVE] [numeric](12, 2) NOT NULL ,
	[LIBELLE] [varchar] (100) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_SINISTRES] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [smallint] NOT NULL ,
	[NO_SINISTRE_IC] [char] (19) COLLATE fr_cs_as NOT NULL ,
	[CODE_LIQUIDATION_IC] [char] (1) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NULL ,
	[NO_AVS_CLIENT] [varchar] (11) COLLATE fr_cs_as NULL ,
	[ZNO_AGENT] [int] NULL ,
	[ZNO_INTERMEDIAIRE] [varchar] (14) COLLATE fr_cs_as NULL ,
	[ZNO_AGENCE] [int] NULL ,
	[REGION] [tinyint] NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[CONSOLIDATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CIE_COASS_REASS] [int] NULL ,
	[TAUX_COASS_REASS] [numeric](7, 3) NULL ,
	[CODE_FONDATION] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_PROVENANCE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_SYSTEME] [varchar] (4) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[U_ACCOUNT_TEAM] [int] NULL ,
	[NO_AGENCE_AGENT] [varchar] (16) COLLATE fr_cs_as NULL ,
	[NO_POLICE_PERSONNE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[ZNO_CONTRAT] [varchar] (23) COLLATE fr_cs_as NULL ,
	[NO_TRAITE_REASS] [varchar] (16) COLLATE fr_cs_as NULL ,
	[GROUPE_CONTRATS] [varchar] (9) COLLATE fr_cs_as NULL ,
	[NO_SINISTRE] [varchar] (19) COLLATE fr_cs_as NULL ,
	[NO_GESTIONNAIRE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POSTAL_SINISTRE] [varchar] (6) COLLATE fr_cs_as NULL ,
	[CANTON_SINISTRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[PAYS_SINISTRE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[NATURE_SINISTRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CAUSE_SINISTRE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CODE_ACC_CNA] [varchar] (18) COLLATE fr_cs_as NULL ,
	[DATE_SURVENANCE] [datetime] NULL ,
	[DATE_OUVERTURE] [datetime] NULL ,
	[DATE_LIQUIDATION] [datetime] NULL ,
	[DATE_REOUVERTURE] [datetime] NULL ,
	[STATUT_SINISTRE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[OBJET_CONCERNE] [varchar] (10) COLLATE fr_cs_as NULL ,
	[TYPE_SINISTRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_CAUSE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[RAISON_SINISTRE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[CODE_COUV_INTERSIN] [varchar] (2) COLLATE fr_cs_as NULL ,
	[VZS] [varchar] (8) COLLATE fr_cs_as NULL ,
	[IMPOT_SOURCE] [numeric](12, 2) NULL ,
	[LOCALISATION_DOSSIER] [varchar] (5) COLLATE fr_cs_as NULL ,
	[TYPE_POOL] [varchar] (1) COLLATE fr_cs_as NULL ,
	[REPRESENTATION] [varchar] (5) COLLATE fr_cs_as NULL ,
	[TEXTE_CAUSE] [varchar] (40) COLLATE fr_cs_as NULL ,
	[TEXTE_CONSEQUENCE] [varchar] (40) COLLATE fr_cs_as NULL ,
	[SIN_VA] [varchar] (3) COLLATE fr_cs_as NULL ,
	[FLAG_SIN_CORP] [varchar] (1) COLLATE fr_cs_as NULL ,
	[TOTAL_PAIEMENTS] [numeric](12, 2) NULL ,
	[TOTAL_P_100000] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NO_AGENCE_IC] [int] NULL ,
	[NO_AGENT_IC] [int] NULL ,
	[NO_CPTE_IC] [varchar] (10) COLLATE fr_cs_as NULL ,
	[FREMD_REF] [varchar] (15) COLLATE fr_cs_as NULL ,
	[SCHADEN_KATEGORIE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SCHADEN_STUNDE] [tinyint] NULL ,
	[DOSSIER_STANDORT2] [varchar] (5) COLLATE fr_cs_as NULL ,
	[ZUSATZ1] [varchar] (20) COLLATE fr_cs_as NULL ,
	[ZUSATZ2] [varchar] (20) COLLATE fr_cs_as NULL ,
	[UG_SCHADENCODE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[DECKUNGS_EINREDE] [varchar] (1) COLLATE fr_cs_as NULL ,
	[SCHADEN_UBERBLICK] [varchar] (10) COLLATE fr_cs_as NULL ,
	[QUALIFIKATION] [varchar] (1) COLLATE fr_cs_as NULL ,
	[LANDER_CODE] [varchar] (4) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_SINISTRES_LAA] (
	[MOIS_DWH_IC] [int] NOT NULL ,
	[ANNEE_DWH_IC] [int] NOT NULL ,
	[NO_COMPAGNIE_IC] [int] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [int] NOT NULL ,
	[NO_SINISTRE_IC] [char] (19) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [char] (6) COLLATE fr_cs_as NULL ,
	[NO_SINISTRE] [char] (19) COLLATE fr_cs_as NULL ,
	[NO_CONTRACTANT] [char] (4) COLLATE fr_cs_as NULL ,
	[CODE_SEXE] [char] (1) COLLATE fr_cs_as NULL ,
	[DATE_NAISSANCE] [datetime] NULL ,
	[DOMAINE_ACTIVITE] [char] (1) COLLATE fr_cs_as NULL ,
	[NATIONALITE] [char] (3) COLLATE fr_cs_as NULL ,
	[ETAT_CIVIL] [char] (1) COLLATE fr_cs_as NULL ,
	[CATEGORIE_PROFESSIONNELLE] [char] (2) COLLATE fr_cs_as NULL ,
	[CAS_INDEMNITE_JOURNALIERE] [char] (1) COLLATE fr_cs_as NULL ,
	[ANCIEN_ASSUREUR] [char] (5) COLLATE fr_cs_as NULL ,
	[ECHANTILLON] [char] (1) COLLATE fr_cs_as NULL ,
	[ANCIEN_NO_SINISTRE] [char] (17) COLLATE fr_cs_as NULL ,
	[NOMBRE_JJ_TRAVAIL_SEMAINE] [char] (3) COLLATE fr_cs_as NULL ,
	[NOMBRE_HH_TRAVAIL_SEMAINE] [char] (3) COLLATE fr_cs_as NULL ,
	[TEMPS_TRAVAIL_HEBDO_USUEL] [char] (3) COLLATE fr_cs_as NULL ,
	[FLAG_TRAVAIL_TEMPORAIRE] [char] (1) COLLATE fr_cs_as NULL ,
	[FLAG_TRAVAIL_SAISONIER] [char] (1) COLLATE fr_cs_as NULL ,
	[UNTER_VA] [char] (3) COLLATE fr_cs_as NULL ,
	[SALAIRE_BASE_BRUT] [numeric](13, 2) NULL ,
	[NATURE_SALAIRE] [char] (1) COLLATE fr_cs_as NULL ,
	[COMPL_RENCHERISSEMENT] [numeric](13, 2) NULL ,
	[NATURE_COMPL_RENCHERISSEMENT] [char] (1) COLLATE fr_cs_as NULL ,
	[ACCORD] [numeric](13, 2) NULL ,
	[NATURE_ACCORD] [char] (1) COLLATE fr_cs_as NULL ,
	[COMPL_ENFANT] [numeric](13, 2) NULL ,
	[NATURE_COMPL_ENFANT] [char] (1) COLLATE fr_cs_as NULL ,
	[INDEMN_VACANCES] [numeric](13, 2) NULL ,
	[NATURE_INDEMN_VACANCES] [char] (1) COLLATE fr_cs_as NULL ,
	[AUTRE_COMPL_SALAIRE] [numeric](13, 2) NULL ,
	[NATURE_AUTRE_COMPL_SALAIRE] [char] (1) COLLATE fr_cs_as NULL ,
	[GRATIFICATION] [numeric](13, 2) NULL ,
	[NATURE_GRATIFICATION] [char] (1) COLLATE fr_cs_as NULL ,
	[SALAIRE_NATUREL] [numeric](13, 2) NULL ,
	[NATURE_SALAIRE_NATUREL] [char] (1) COLLATE fr_cs_as NULL ,
	[DATE_REPRISE_TRAVAIL] [datetime] NULL ,
	[CODE_PRISE_POSITION] [char] (2) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_SINISTRES_VEHICULES] (
	[MOIS_DWH_IC] [int] NOT NULL ,
	[ANNEE_DWH_IC] [int] NOT NULL ,
	[NO_COMPAGNIE_IC] [int] NOT NULL ,
	[TYPE_POLICE_IC] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE_IC] [int] NOT NULL ,
	[NO_UR_COUCHE_IC] [int] NOT NULL ,
	[NO_SINISTRE_IC] [char] (19) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [char] (6) COLLATE fr_cs_as NULL ,
	[NO_SINISTRE] [char] (19) COLLATE fr_cs_as NULL ,
	[BRANCHE] [char] (2) COLLATE fr_cs_as NULL ,
	[REL_COND_SIN_COUV] [char] (2) COLLATE fr_cs_as NULL ,
	[DATE_NAISS_CONDUCTEUR] [datetime] NULL ,
	[NATIONALITE_CONDUCTEUR] [char] (3) COLLATE fr_cs_as NULL ,
	[SEXE_CONDUCTEUR] [char] (1) COLLATE fr_cs_as NULL ,
	[PERMIS_COND_DEPUIS] [datetime] NULL ,
	[USAGE_VEH_SIN] [char] (2) COLLATE fr_cs_as NULL ,
	[INDICATEUR_SIN_TOTAL] [char] (2) COLLATE fr_cs_as NULL ,
	[CODE_CULPABILITE] [char] (2) COLLATE fr_cs_as NULL ,
	[LIEU_DEPOT] [char] (2) COLLATE fr_cs_as NULL ,
	[ETAT_KILOMETRAGE] [int] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_SPECIALITES] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NOT NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[REGION] [int] NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[TYPE_MONTANT] [smallint] NOT NULL ,
	[MONTANT_ANTERIEUR] [numeric](12, 2) NULL ,
	[MONTANT_COURANT] [numeric](12, 2) NULL ,
	[MONTANT] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_SPECIALITES_INUTIL] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NOT NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[REGION] [int] NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[TYPE_MONTANT] [smallint] NOT NULL ,
	[MONTANT_ANTERIEUR] [numeric](12, 2) NULL ,
	[MONTANT_COURANT] [numeric](12, 2) NULL ,
	[MONTANT] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_SPECIALITES_bof] (
	[MOIS_DWH_IC] [tinyint] NOT NULL ,
	[ANNEE_DWH_IC] [smallint] NOT NULL ,
	[NO_COMPAGNIE_IC] [smallint] NOT NULL ,
	[TYPE_POLICE_IC] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[PERIODE_LIVRAISON] [varchar] (6) COLLATE fr_cs_as NULL ,
	[ENTITE_JURIDIQUE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NOT NULL ,
	[REGION] [int] NULL ,
	[TYPE_AFFAIRE] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[TYPE_MONTANT] [smallint] NOT NULL ,
	[MONTANT_ANTERIEUR] [numeric](12, 2) NULL ,
	[MONTANT_COURANT] [numeric](12, 2) NULL ,
	[MONTANT] [numeric](12, 2) NULL ,
	[SOURCE_IC] [varchar] (4) COLLATE fr_cs_as NULL ,
	[INTERNATIONAL] [varchar] (1) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_TABLEAU_COMPTABILISATION] (
	[NUMERO] [int] IDENTITY (1, 1) NOT NULL ,
	[BET] [varchar] (4) COLLATE fr_cs_as NOT NULL ,
	[SCHADENART] [varchar] (2) COLLATE fr_cs_as NULL ,
	[BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[GAC] [varchar] (2) COLLATE fr_cs_as NOT NULL ,
	[RV] [varchar] (1) COLLATE fr_cs_as NULL ,
	[NAT] [varchar] (1) COLLATE fr_cs_as NOT NULL ,
	[VZ4] [varchar] (8) COLLATE fr_cs_as NULL ,
	[SC] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SBU] [varchar] (1) COLLATE fr_cs_as NULL ,
	[BUK] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[KONTO] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[KONTO_BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL ,
	[DLK] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[DLK_BUK] [varchar] (3) COLLATE fr_cs_as NOT NULL ,
	[DLK_BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_TABLEAU_COMPTES_LIBELLES] (
	[ORDRE] [varchar] (4) COLLATE fr_cs_as NULL ,
	[NIVEAU] [varchar] (6) COLLATE fr_cs_as NOT NULL ,
	[KONTO] [varchar] (7) COLLATE fr_cs_as NOT NULL ,
	[KONTO_BEZEICHNUNG] [varchar] (50) COLLATE fr_cs_as NULL ,
	[COMPTE_DESCRIPTION] [varchar] (50) COLLATE fr_cs_as NULL ,
	[PARENT] [varchar] (6) COLLATE fr_cs_as NULL ,
	[DISPLAY_CURRENCY_SPLITTING] [varchar] (1) COLLATE fr_cs_as NULL ,
	[RUBRIQUE] [varchar] (10) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[DWH_TYPE_MONTANT] (
	[TYPE_MONTANT] [smallint] NOT NULL ,
	[LIBELLE] [varchar] (50) COLLATE fr_cs_as NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[_bof_pren_annee] (
	[NO_PERSONNE] [varchar] (17) COLLATE fr_cs_as NOT NULL ,
	[ANNEE_ENTREE] [smallint] NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[_nicobof] (
	[ANNEE_COMPTABLE] [smallint] NOT NULL ,
	[MOIS_COMPTABLE] [tinyint] NOT NULL ,
	[TYPE_POLICE] [char] (4) COLLATE fr_cs_as NOT NULL ,
	[NO_POLICE] [int] NOT NULL ,
	[K_FONDATION] [varchar] (3) COLLATE fr_cs_as NOT NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[_rep] (
	[PERIODE_LIVRAISON] [char] (7) COLLATE fr_cs_as NOT NULL ,
	[ENTITE_JURIDIQUE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[PRIME_BASE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_COURANTE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_PRECEDENTE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_SUIVANTE] [numeric](12, 2) NULL ,
	[REPORT_ENTREE_MUTATION] [numeric](12, 2) NULL ,
	[REPORT_SORTIE_MUTATION] [numeric](12, 2) NULL 
) ON [PRIMARY]

CREATE TABLE [dbo].[_rep2000] (
	[PERIODE_LIVRAISON] [char] (7) COLLATE fr_cs_as NOT NULL ,
	[ENTITE_JURIDIQUE] [char] (2) COLLATE fr_cs_as NOT NULL ,
	[SBU] [varchar] (2) COLLATE fr_cs_as NULL ,
	[SEGMENT] [tinyint] NULL ,
	[SECTEUR_ECONOMIQUE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[CANAL_VENTE] [varchar] (2) COLLATE fr_cs_as NULL ,
	[NO_CANAL_VENTE] [int] NULL ,
	[RISQUE] [varchar] (8) COLLATE fr_cs_as NULL ,
	[LABEL_VENTE] [varchar] (5) COLLATE fr_cs_as NULL ,
	[PRODUIT] [varchar] (8) COLLATE fr_cs_as NULL ,
	[NO_POLICE] [varchar] (23) COLLATE fr_cs_as NULL ,
	[CODE_DEVISE] [varchar] (3) COLLATE fr_cs_as NULL ,
	[COURS_CHANGE] [numeric](10, 5) NULL ,
	[PRIME_BASE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_COURANTE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_PRECEDENTE] [numeric](12, 2) NULL ,
	[REPORT_PERIODE_SUIVANTE] [numeric](12, 2) NULL ,
	[REPORT_ENTREE_MUTATION] [numeric](12, 2) NULL ,
	[REPORT_SORTIE_MUTATION] [numeric](12, 2) NULL 
) ON [PRIMARY]

ALTER TABLE [dbo].[DWH_ARTEMIS_INV] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_ARTEMIS_INV] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[DEPOTNUMMER],
		[NO_VAL_DWH_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_ARTEMIS_MVT] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_ARTEMIS_MVT_1__11] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[DEPOTNUMMER],
		[NO_MAA_IC],
		[NO_MFN_IC],
		[RECORDTYP]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_CLIENTS] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_CLIENTS] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[ENTITE_JURIDIQUE],
		[NO_PERSONNE]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_CODIF_COMPTES_SAP] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_CODIF_COMPTES_SAP_1__11] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE],
		[TYPE_RESERVE],
		[TYPE_AFFAIRE],
		[CODE_MONNAIE_ISO],
		[CONSOLIDATION]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_CODIF_LOB] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_CODIF_LOB_1__11] PRIMARY KEY  CLUSTERED 
	(
		[RISQUE_DWH]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_CODIF_LOB2_VZ4] WITH NOCHECK ADD 
	CONSTRAINT [DWH_CODIF_LOB2_VZ4_PK] PRIMARY KEY  CLUSTERED 
	(
		[LOB2]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_CODIF_PC] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_CODIF_PC] PRIMARY KEY  CLUSTERED 
	(
		[RISQUE_DWH],
		[TYPE_AFFAIRE]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_CODIF_SOURCE_IC] WITH NOCHECK ADD 
	CONSTRAINT [PK_CODIF_SOURCE_IC] PRIMARY KEY  CLUSTERED 
	(
		[TBL],
		[SOURCE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_COMMISSIONS] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_COMMISSIONS] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[NO_SEQUENCE_IC],
		[NO_AGENCE_IC],
		[NO_AGENT_IC],
		[GENRE_COMMISSION_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_DESCRIPTION_EXPORT_FILES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_DESCR_FILES] PRIMARY KEY  CLUSTERED 
	(
		[GEfile],
		[ZHout_col_no]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_EXTERNAL_COMM] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_EXTERNAL_COMM] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE],
		[NO_POLICE],
		[NO_SEQUENCE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_EXTERNAL_COMM_REF] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_EXTERNAL_COMM_REF] PRIMARY KEY  CLUSTERED 
	(
		[NO_IDENTIFICATION]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_EXTERNAL_DATA] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_EXTERNAL_DATA] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[ID]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_EXTERNAL_DATA_BUFFER] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_EXTERNAL_DATA_BUFFER] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_INTERFACE_SAP_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_INTERFACE_SAP_TMP] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[BUK],
		[TYPE_POLICE],
		[SOURCE_IC],
		[TYPE_RESERVE],
		[TYPE_AFFAIRE],
		[CODE_MONNAIE_ISO],
		[RISQUE_DWH],
		[INTERNATIONAL],
		[CONSOLIDATION],
		[CIE_COASS_REASS],
		[SEGMENT]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_INTERFACE_SAP_TMP2] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_INTERFACE_SAP_TMP2] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[BUK],
		[CODE_MONNAIE_ISO],
		[INTERNATIONAL],
		[CIE_COASS_REASS],
		[SEGMENT],
		[COMPTE_SAP_BILAN],
		[COMPTE_SAP_PP],
		[LOB2],
		[PC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_INVENTAIRE] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_INVENTAIRE] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_KONTIERUNG_GA] WITH NOCHECK ADD 
	CONSTRAINT [PK_KONTIERUNG_GA] PRIMARY KEY  CLUSTERED 
	(
		[NUMERO]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_KONTIERUNG_GL] WITH NOCHECK ADD 
	CONSTRAINT [PK_KONTIERUNG_GL] PRIMARY KEY  CLUSTERED 
	(
		[NUMERO]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_PAIEMENTS] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_PAIEMENTS] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[NO_SINISTRE_IC],
		[CODE_LIQUIDATION_IC],
		[NO_PAIEMENT_IC],
		[NO_COMPTEUR]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_PARAMETRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_PARAMETRES] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_POLICES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_POLICES_NEW] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_POLICES_COMPLEMENT] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_POL_COMPL] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_POLICES_COMPLEMENT_MISSING_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_POL_COMPL_MISS_TMP] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_POLICES_GCIE_SPLITT] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_POLICES_GCIE_SPLITT] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE_IC],
		[NO_POLICE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_POLICES_GCIE_SPLITT_MC06] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_POLICES_GCIE_SPLITT_MC06] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_POLICE_IC],
		[NO_POLICE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_POLICES_MISSING_TMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_POLICES_MISS_NEW] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_PRIMES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_PRIMES] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[DATE_TRAITEMENT_IC],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_PRIMES_TEMP] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_PRIMES_TEMP] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[DATE_TRAITEMENT_IC],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_REPORT_PRIMES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_REPORT_PRIMES] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[DATE_TRAITEMENT_IC],
		[NO_MOUVEMENT]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_RESERVES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_RESERVES] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[NO_SINISTRE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_RESERVES_BUFFER] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_RESERVES_BUFFER] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[TYPE_MONTANT],
		[LOB2],
		[SCHADEN_JAHR],
		[KS]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_SINISTRES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_SINISTRES] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[NO_SINISTRE_IC],
		[CODE_LIQUIDATION_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_SINISTRES_LAA] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_SINISTRES_LAA_1__20] PRIMARY KEY  CLUSTERED 
	(
		[MOIS_DWH_IC],
		[ANNEE_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[NO_SINISTRE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_SINISTRES_VEHICULES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_SINISTRES_VEHICULE1__20] PRIMARY KEY  CLUSTERED 
	(
		[MOIS_DWH_IC],
		[ANNEE_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[NO_POLICE_IC],
		[NO_UR_COUCHE_IC],
		[NO_SINISTRE_IC]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_SPECIALITES] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_SPECIALITES_1__11] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[SEGMENT],
		[RISQUE],
		[TYPE_AFFAIRE],
		[TYPE_MONTANT],
		[CODE_DEVISE]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_SPECIALITES_INUTIL] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_SPECIALITES_INUTIL] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_DWH_IC],
		[MOIS_DWH_IC],
		[NO_COMPAGNIE_IC],
		[TYPE_POLICE_IC],
		[SEGMENT],
		[RISQUE],
		[TYPE_AFFAIRE],
		[TYPE_MONTANT],
		[CODE_DEVISE]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_TABLEAU_COMPTABILISATION] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NUMERO]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_TABLEAU_COMPTES_LIBELLES] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[KONTO],
		[NIVEAU]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_TYPE_MONTANT] WITH NOCHECK ADD 
	CONSTRAINT [PK_DWH_TYPE_MONTANT] PRIMARY KEY  CLUSTERED 
	(
		[TYPE_MONTANT]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[_bof_pren_annee] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[NO_PERSONNE]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[_nicobof] WITH NOCHECK ADD 
	CONSTRAINT [pk_nicobof] PRIMARY KEY  CLUSTERED 
	(
		[ANNEE_COMPTABLE],
		[MOIS_COMPTABLE],
		[TYPE_POLICE],
		[NO_POLICE]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_INTERFACE_SAP_TMP] ADD 
	 UNIQUE  NONCLUSTERED 
	(
		[ALT_DUMMY_PK]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[DWH_INTERFACE_SAP_TMP2] ADD 
	 UNIQUE  NONCLUSTERED 
	(
		[ALT_DUMMY_PK]
	)  ON [PRIMARY] 



 CREATE  INDEX [IDX_PAY_NO_SIN] ON [dbo].[DWH_PAIEMENTS]([NO_SINISTRE], [ANNEE_DWH_IC], [MOIS_DWH_IC], [NO_COMPAGNIE_IC]) ON [PRIMARY]

 CREATE  INDEX [IDX_PAY_NO_SIN_IC] ON [dbo].[DWH_PAIEMENTS]([NO_SINISTRE_IC], [CODE_LIQUIDATION_IC], [ANNEE_DWH_IC], [MOIS_DWH_IC], [NO_COMPAGNIE_IC]) ON [PRIMARY]

 CREATE  INDEX [IDX_RES_NOSIN] ON [dbo].[DWH_RESERVES]([NO_SINISTRE], [ANNEE_DWH_IC], [MOIS_DWH_IC], [NO_COMPAGNIE_IC]) ON [PRIMARY]

 CREATE  INDEX [IDX_SIN_NO_SIN] ON [dbo].[DWH_SINISTRES]([NO_SINISTRE], [ANNEE_DWH_IC], [MOIS_DWH_IC], [NO_COMPAGNIE_IC]) ON [PRIMARY]

 CREATE  INDEX [IDX_SIN_NO_SIN_IC] ON [dbo].[DWH_SINISTRES]([NO_SINISTRE_IC], [CODE_LIQUIDATION_IC], [ANNEE_DWH_IC], [MOIS_DWH_IC], [NO_COMPAGNIE_IC]) ON [PRIMARY]

 CREATE  INDEX [IDX_rep_PERIODE] ON [dbo].[_rep]([PERIODE_LIVRAISON], [ENTITE_JURIDIQUE], [RISQUE]) ON [PRIMARY]

 CREATE  INDEX [IDX_rep2000_PERIODE] ON [dbo].[_rep2000]([PERIODE_LIVRAISON], [ENTITE_JURIDIQUE], [RISQUE]) ON [PRIMARY]



GRANT  SELECT  ON [dbo].[DWH_ARTEMIS_INV]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_ARTEMIS_MVT]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_CLIENTS]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_CLIENTS]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_CLIENTS]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_CLIENTS]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_CLIENTS]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_CODIF_COMPTES_SAP]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_CODIF_COMPTES_SAP]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_CODIF_COMPTES_SAP]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_CODIF_COMPTES_SAP]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB2_VZ4]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB2_VZ4]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB2_VZ4]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_CODIF_LOB2_VZ4]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_CODIF_PC]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_CODIF_PC]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_CODIF_PC]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_CODIF_PC]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_CODIF_SOURCE_IC]  TO [public]

GRANT  SELECT  ON [dbo].[DWH_CODIF_SOURCE_IC]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_CODIF_SOURCE_IC]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_COMMISSIONS]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_COMMISSIONS]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_COMMISSIONS]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_COMMISSIONS]  TO [cha8751]

GRANT  SELECT  ON [dbo].[DWH_COMMISSIONS]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_DESCRIPTION_EXPORT_FILES]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_DESCRIPTION_EXPORT_FILES]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHA0747]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHA4601]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHIGOHE]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHL4628]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHV3755]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHV7113]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHVWALS]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [CHZ9690]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM]  TO [user_alibaba]

GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[DWH_EXTERNAL_COMM]  TO [walserw]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM_REF]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_COMM_REF]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHA0747]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHA4601]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHIGOHE]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHL4628]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHV3755]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHV7113]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHVWALS]

GRANT  SELECT  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [CHZ9690]

GRANT  SELECT ,  UPDATE  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [walserw]

GRANT  SELECT ,  UPDATE  ON [dbo].[DWH_EXTERNAL_DATA_BUFFER]  TO [zh_compta2]

GRANT  SELECT  ON [dbo].[DWH_INVENTAIRE]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_INVENTAIRE]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_INVENTAIRE]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_INVENTAIRE]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_INVENTAIRE]  TO [CHL4806]

GRANT  SELECT  ON [dbo].[DWH_INVENTAIRE]  TO [cha8751]

GRANT  SELECT  ON [dbo].[DWH_INVENTAIRE]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_KONTIERUNG_GA]  TO [public]

GRANT  SELECT  ON [dbo].[DWH_KONTIERUNG_GA]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_KONTIERUNG_GA]  TO [CHY0787]

GRANT  SELECT  ON [dbo].[DWH_KONTIERUNG_GL]  TO [public]

GRANT  SELECT  ON [dbo].[DWH_KONTIERUNG_GL]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_KONTIERUNG_GL]  TO [CHY0787]

GRANT  SELECT  ON [dbo].[DWH_PAIEMENTS]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_PAIEMENTS]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_PAIEMENTS]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_PAIEMENTS]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_PAIEMENTS]  TO [CHL4806]

GRANT  SELECT  ON [dbo].[DWH_PAIEMENTS]  TO [cha8751]

GRANT  SELECT  ON [dbo].[DWH_PAIEMENTS]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_PARAMETRES]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_PARAMETRES]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_PARAMETRES]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_POLICES]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_POLICES]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_POLICES_COMPLEMENT]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_POLICES_COMPLEMENT]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_POLICES_COMPLEMENT]  TO [cha8751]

GRANT  SELECT  ON [dbo].[DWH_POLICES_COMPLEMENT]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_POLICES_GCIE_SPLITT]  TO [public]

GRANT  SELECT  ON [dbo].[DWH_POLICES_GCIE_SPLITT_MC06]  TO [public]

GRANT  SELECT  ON [dbo].[DWH_PRIMES]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_PRIMES]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_PRIMES]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_PRIMES]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_PRIMES]  TO [CHL4806]

GRANT  SELECT  ON [dbo].[DWH_PRIMES]  TO [cha8751]

GRANT  SELECT  ON [dbo].[DWH_PRIMES]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_REPORT_PRIMES]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_REPORT_PRIMES]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_REPORT_PRIMES]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_REPORT_PRIMES]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_REPORT_PRIMES]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_RESERVES]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_RESERVES]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_RESERVES]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_RESERVES]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_RESERVES]  TO [CHL4806]

GRANT  SELECT  ON [dbo].[DWH_RESERVES]  TO [cha8751]

GRANT  SELECT  ON [dbo].[DWH_RESERVES]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_RESERVES_BUFFER]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_SINISTRES]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_SINISTRES]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_SINISTRES]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_SINISTRES]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_SINISTRES]  TO [CHL4806]

GRANT  SELECT  ON [dbo].[DWH_SINISTRES]  TO [cha8751]

GRANT  SELECT  ON [dbo].[DWH_SINISTRES]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_SPECIALITES]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_SPECIALITES]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_SPECIALITES]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_SPECIALITES_bof]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_TABLEAU_COMPTES_LIBELLES]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[DWH_TYPE_MONTANT]  TO [controlling]

GRANT  SELECT  ON [dbo].[DWH_TYPE_MONTANT]  TO [CHA7068]

GRANT  SELECT  ON [dbo].[DWH_TYPE_MONTANT]  TO [CHL4780]

GRANT  SELECT  ON [dbo].[DWH_TYPE_MONTANT]  TO [CHL4794]

GRANT  SELECT  ON [dbo].[DWH_TYPE_MONTANT]  TO [user_alibaba]

GRANT  SELECT  ON [dbo].[_rep]  TO [controlling]

GRANT  SELECT  ON [dbo].[_rep]  TO [user_alibaba]


