

set language french

declare @TINYINT_MAX tinyint =  255
declare @TINYINT_MIN tinyint =    0

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @INT_MAX_DATETIME int = 2958463  -- 9999-12-31 0:0:0
declare @INT_MIN_DATETIME int = -693595  -- 0001-01-01 0:0:0

declare @DATETIME_MAX datetime = '99991231 23:59:59.999'
declare @DATETIME_MIN datetime = '00010101'



-- ========== DATETIME miscellaneous ============

_assert_(cast(0 as datetime) = cast('19000101' as datetime))
_assert_(cast(1 as datetime) = cast('19000102' as datetime))

_assert_(convert(varchar, cast('99991231 23:59:59.9908134' as datetime), 121) = '9999-12-31 23:59:59.990')
_assert_(convert(varchar, cast('99991231 23:59:59.990' as datetime), 121) = '9999-12-31 23:59:59.990')
_assert_(convert(varchar, cast('99991231 23:59:59.12' as datetime), 121) = '9999-12-31 23:59:59.120')
_assert_(convert(varchar, cast('99991231 23:59:59.3' as datetime), 121) = '9999-12-31 23:59:59.300')
_assert_(convert(varchar, cast('99991231 23:59:59.0' as datetime), 121) = '9999-12-31 23:59:59.000')
_assert_(convert(varchar, cast('99991231 23:59:59' as datetime), 121) = '9999-12-31 23:59:59.000')


declare @aaa as datetime = '2011-03-04T13:22:33.1234'
_assert_(cast(@aaa as varchar(40)) = '2011-03-04 13:22:33.123')

set language [fr-FR]
set language 'fr-FR'

set language fr_FR
_assert_(cast('20330201' as datetime) in ('01-02-2033', cast('20110601' as datetime)))
set language en_us
_assert_(cast('20330201' as datetime) in ('02-01-2033', cast('20110601' as datetime)))
set language fr_fr


-- ========== DATETIME miscellaneous ============

set language FRENCH

declare @a datetime

set @a = '2011.12.20 0:20:51.164'
_assert_(cast(year(@a) as varchar) + '.' + cast(month(@a) as varchar) + '.' + cast(day(@a) as varchar) + ' ' +  cast(datepart(hour, @a) as varchar) + ':' + cast(datepart(minute, @a) as varchar) + ':' + cast(datepart(second, @a) as varchar) + '.' + cast(datepart(millisecond, @a) as varchar) = '2011.12.20 0:20:51.164')


set language ENGLISH

declare @s varchar(50) = '  21.07.2001   5   : 3 :0 '

set @a = convert(datetime, @s, 103)

_assert_(cast(year(@a) as varchar) + '.' + right(cast(month(@a)+100 as varchar), 2) + '.' + cast(day(@a) as varchar) + ' ' +  cast(datepart(hour, @a) as varchar) + ':' + cast(datepart(minute, @a) as varchar) + ':' + cast(datepart(second, @a) as varchar) + '.' + cast(datepart(millisecond, @a) as varchar) = '2001.07.21 5:3:0.0')




declare @dat date = '20110712'
declare @tim time = '10:30:3.123'

_assert_(@dat + @tim  = '20110712 10:30:3.123')


-- ========== DATETIME string not valid ============

_assert_error_(cast('99991232 23:59:59.998' as datetime), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== DATETIME string not valid ============

_assert_error_(cast('99991231 23:59:59.' as datetime), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== DATETIME string not valid ============

_assert_error_(cast('99991231 23:59:59.998132323121' as datetime), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== DATETIME string not valid ============

_assert_error_(cast('99991231 23:59:60.998' as datetime), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== DATETIME add ============

_assert_(@DATETIME_MAX + 0 = @DATETIME_MAX)
_assert_(@DATETIME_MIN + 0 = @DATETIME_MIN)

-- _assert_error_(cast(100 as datetime) + null is null, 'DATETIME_ADD_BAD_TYPE')      -- syntax error, the whole batch doen't compile
-- _assert_error_(cast(null as datetime) + null is null, 'DATETIME_ADD_BAD_TYPE')     -- _assert_error_ catches only runtime error, not compile error


-- ========== DATETIME add max overflow ============

_assert_error_(@DATETIME_MAX + 1, 'DATETIME_OVERFLOW')


-- ========== DATETIME add max overflow ============

_assert_error_(@DATETIME_MIN + -1, 'DATETIME_OVERFLOW')


-- ========== DATETIME comp op ============

_assert_(cast(122 as datetime) = cast(122 as datetime))

_assert_(not (cast(122 as datetime) = cast(123 as datetime)))

_assert_(not cast(122 as datetime) = cast(123 as datetime))

_assert_null_( cast(100 as datetime) = null)
_assert_null_( null = cast(100 as datetime))
_assert_null_( cast(null as datetime) = null)


_assert_(not (cast(122 as datetime) != cast(122 as datetime)))

_assert_(cast(122 as datetime) != cast(123 as datetime))

_assert_null_( cast(100 as datetime) != null)
_assert_null_( null != cast(100 as datetime))
_assert_null_( cast(null as datetime) != null)


_assert_(cast(123 as datetime) > cast(122 as datetime))

_assert_(not (cast(122 as datetime) > cast(122 as datetime)))

_assert_null_( cast(100 as datetime) > null)
_assert_null_( null > cast(100 as datetime))
_assert_null_( cast(null as datetime) > null)


_assert_(cast(123 as datetime) >= cast(122 as datetime))

_assert_(cast(122 as datetime) >= cast(122 as datetime))

_assert_(not (cast(121 as datetime) >= cast(122 as datetime)))

_assert_null_( cast(100 as datetime) >= null)
_assert_null_( null >= cast(100 as datetime))
_assert_null_( cast(null as datetime) >= null)


_assert_(cast(121 as datetime) < cast(122 as datetime))

_assert_(not (cast(122 as datetime) < cast(122 as datetime)))

_assert_null_( cast(100 as datetime) < null)
_assert_null_( null < cast(100 as datetime))
_assert_null_( cast(null as datetime) < null)


_assert_(cast(121 as datetime) <= cast(122 as datetime))

_assert_(cast(122 as datetime) <= cast(122 as datetime))

_assert_(not (cast(123 as datetime) <= cast(122 as datetime)))

_assert_null_( cast(100 as datetime) <= null)
_assert_null_( null <= cast(100 as datetime))
_assert_null_( cast(null as datetime) <= null)


-- ========== DATETIME is [not] null ============

_assert_(cast(NULL as datetime) is null)

_assert_(not cast(123 as datetime) is null)


_assert_(not cast(NULL as datetime) is not null)

_assert_(cast(123 as datetime) is not null)


-- ========== DATETIME [not] in list ============

_assert_(cast(12 as datetime) in (cast(12 as datetime)))

_assert_(cast(12 as datetime) in (cast(0 as datetime), cast(12 as datetime)))

_assert_(cast(12 as datetime) in (cast(0 as datetime), cast(12 as datetime), cast(9 as datetime)))

_assert_(cast(12 as datetime) in (cast(0 as datetime), NULL, cast(12 as datetime), cast(9 as datetime)))

_assert_(cast(12 as datetime) in (cast(0 as datetime), cast(12 as datetime), NULL, cast(9 as datetime)))


_assert_(not cast(12 as datetime) in (cast(0 as datetime), cast(1 as datetime), cast(9 as datetime)))

_assert_null_(cast(12 as datetime) in (cast(0 as datetime), cast(1 as datetime), NULL, cast(9 as datetime)))


_assert_(not cast(12 as datetime) not in (cast(12 as datetime)))

_assert_(not cast(12 as datetime) not in (cast(0 as datetime), cast(12 as datetime)))

_assert_(not cast(12 as datetime) not in (cast(0 as datetime), cast(12 as datetime), cast(9 as datetime)))

_assert_(not cast(12 as datetime) not in (cast(0 as datetime), NULL, cast(12 as datetime), cast(9 as datetime)))

_assert_(not cast(12 as datetime) not in (cast(0 as datetime), cast(12 as datetime), NULL, cast(9 as datetime)))


_assert_(cast(12 as datetime) not in (cast(0 as datetime), cast(1 as datetime), cast(9 as datetime)))

_assert_null_(cast(12 as datetime) not in (cast(0 as datetime), cast(1 as datetime), NULL, cast(9 as datetime)))


-- ========== DATETIME cast ============

_assert_(cast(cast('20110904 19:40:30.345' as datetime) as date) = cast('20110904' as date))
_assert_(cast(cast('20110904 19:40:30.345' as datetime) as date) = cast('20110904' as datetime))

_assert_(cast(cast('20110904 19:40:30.345' as datetime) as time) = cast('19:40:30.345' as time))


-- ========== DATE case when ============

_assert_(case when 1=1 then cast('20110101' as date) end = cast('20110101' as date))
_assert_(case when 1=4 then cast('20110101' as date) end is null)
_assert_(case when 1=1 then cast('20110101' as date) when 1=2 then cast('20110722' as date) else cast('20110930' as date) end = cast('20110101' as date))
_assert_(case when 1=1 then cast('20110101' as date) when 1=2 then cast('20110722' as date) end = cast('20110101' as date))
_assert_(case when 1=4 then cast('20110101' as date) when 2=2 then cast('20110722' as date) else cast('20110930' as date) end = cast('20110722' as date))
_assert_(case when 1=NULL then cast('20110101' as date) when 2=2 then cast('20110722' as date) else cast('20110930' as date) end = cast('20110722' as date))
_assert_(case when 1=NULL then cast('20110101' as date) when 2=NULL then cast('20100304' as date) else cast('20110930' as date) end = cast('20110930' as date))
_assert_(case when 1=NULL then cast('20110101' as date) when 2=NULL then cast('20110722' as date) end is null)

-- ========== TIME case when ============

_assert_(case when 1=1 then cast('3:45' as time) end = cast('3:45' as time))
_assert_(case when 1=4 then cast('3:45' as time) end is null)
_assert_(case when 1=1 then cast('3:45' as time) when 1=2 then cast('13:10:30.123' as time) else cast('2:10' as time) end = cast('3:45' as time))
_assert_(case when 1=1 then cast('3:45' as time) when 1=2 then cast('13:10:30.123' as time) end = cast('3:45' as time))
_assert_(case when 1=4 then cast('3:45' as time) when 2=2 then cast('13:10:30.123' as time) else cast('2:10' as time) end = cast('13:10:30.123' as time))
_assert_(case when 1=NULL then cast('3:45' as time) when 2=2 then cast('13:10:30.123' as time) else cast('2:10' as time) end = cast('13:10:30.123' as time))
_assert_(case when 1=NULL then cast('3:45' as time) when 2=NULL then cast('5:10' as time) else cast('2:10' as time) end = cast('2:10' as time))
_assert_(case when 1=NULL then cast('3:45' as time) when 2=NULL then cast('13:10:30.123' as time) end is null)

-- ========== DATETIME case when ============

_assert_(case when 1=1 then cast('20110101 3:45' as datetime) end = cast('20110101 3:45' as datetime))
_assert_(case when 1=4 then cast('20110101 3:45' as datetime) end is null)
_assert_(case when 1=1 then cast('20110101 3:45' as datetime) when 1=2 then cast('20110722 13:10:30.123' as datetime) else cast('20110930 2:10' as datetime) end = cast('20110101 3:45' as datetime))
_assert_(case when 1=1 then cast('20110101 3:45' as datetime) when 1=2 then cast('20110722 13:10:30.123' as datetime) end = cast('20110101 3:45' as datetime))
_assert_(case when 1=4 then cast('20110101 3:45' as datetime) when 2=2 then cast('20110722 13:10:30.123' as datetime) else cast('20110930 2:10' as datetime) end = cast('20110722 13:10:30.123' as datetime))
_assert_(case when 1=NULL then cast('20110101 3:45' as datetime) when 2=2 then cast('20110722 13:10:30.123' as datetime) else cast('20110930 2:10' as datetime) end = cast('20110722 13:10:30.123' as datetime))
_assert_(case when 1=NULL then cast('20110101 3:45' as datetime) when 2=NULL then cast('20100304 5:10' as datetime) else cast('20110930 2:10' as datetime) end = cast('20110930 2:10' as datetime))
_assert_(case when 1=NULL then cast('20110101 3:45' as datetime) when 2=NULL then cast('20110722 13:10:30.123' as datetime) end is null)


