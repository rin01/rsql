

set language french

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @INT_MAX_DATETIME int = 2958463  -- 9999-12-31 0:0:0
declare @INT_MIN_DATETIME int =  -693595  -- 0001-01-01 0:0:0


-- ========== TEST ============

_assert_(1=1)

-- ========== INT miscellaneous ============

_assert_(@INT_MAX = -(@INT_MIN + 1))

_assert_(@INT_MIN = -@INT_MAX - 1)


_assert_(@INT_MAX - 83647 = 2147400000)

_assert_(-@INT_MAX + 83647 = -2147400000)

_assert_(@INT_MAX * 1 = 2147483647)

_assert_(@INT_MAX * -1 - 1 = @INT_MIN)

_assert_((@INT_MIN+1) * -1 = @INT_MAX)

_assert_(@INT_MIN + @INT_MAX + 1 = 0)


_assert_(10/-2*5   = -25)

_assert_(8/4/2     = 1)
_assert_(8/+4/2    = 1)
_assert_(8/-4/2    = -1)
_assert_(8/(-4)/2  = -1)
_assert_(8/(0-4)/2 = -1)


-- ========== INT simplify CAST ============


declare @bit bit     = 1
declare @ti tinyint  = 111
declare @si smallint = 222
declare @i int       = 333333
declare @bi bigint   = 123456789

_assert_(cast(cast(@bit as smallint) as bigint) = 1)
_assert_(cast(cast(@ti as int) as bigint) = 111)
_assert_(cast(cast(@si as bigint) as float) = 222)
_assert_(cast(cast(@si as smallint) as float) = 222)
_assert_(cast(cast(@si as numeric(12,2)) as float) = 222)
_assert_(cast(cast(@si as numeric(12,2)) as numeric(12,2)) = 222)
_assert_(cast(cast(@si as numeric(5)) as numeric(11,3)) = 222)
_assert_(cast(cast(@si as numeric(4)) as numeric(11,3)) = 222)
_assert_(cast(cast(@si as numeric(4)) as numeric(3)) = 222)
_assert_(cast(cast(@si as numeric(6,2)) as numeric(3)) = 222)
_assert_(cast(cast(@si as float) as numeric(3)) = 222)
_assert_(cast(cast(cast(cast(cast(@si as float) as smallint) as float) as float) as smallint) = 222)


-- ========== INT unary minus ============

_assert_(++++400000 = 400000)
_assert_(-4000000 = -1 * 4000000)
_assert_(- -4 = 4)
_assert_(++- - -++4 = -4)
_assert_(++4 = 4)

_assert_(-cast(NULL as int) is null)


-- ========== INT unary minus overflow ============

_assert_error_(-@INT_MIN, '_INT_OVERFLOW')


-- ========== INT add ============

_assert_(100000 + 15000 = 115000)

_assert_(-100000 + -15000 = -115000)

_assert_(1000 + null is null)
_assert_(null + 1000 is null)
_assert_(cast(null as int) + null is null)


-- ========== INT add max overflow ============

_assert_error_(@INT_MAX + 1, '_INT_OVERFLOW')


-- ========== INT add max overflow ============

_assert_error_(@INT_MAX + @INT_MAX, '_INT_OVERFLOW')


-- ========== INT add min overflow ============

_assert_error_(@INT_MIN + -1, '_INT_OVERFLOW')


-- ========== INT add min overflow ============

_assert_error_(@INT_MIN + -@INT_MAX, '_INT_OVERFLOW')


-- ========== INT add min overflow ============

_assert_error_(@INT_MIN + @INT_MIN, '_INT_OVERFLOW')


-- ========== INT subtract ============

_assert_(100000 - -15000 = 115000)

_assert_(-100000 - 15000 = -115000)

_assert_(1000 - null is null)
_assert_(null - 1000 is null)
_assert_(cast(null as int) - null is null)


-- ========== INT subtract max overflow ============

_assert_error_(@INT_MAX - -1, '_INT_OVERFLOW')


-- ========== INT subtract max overflow ============

_assert_error_(@INT_MAX - @INT_MIN, '_INT_OVERFLOW')


-- ========== INT subtract min overflow ============

_assert_error_(@INT_MIN - 1, '_INT_OVERFLOW')


-- ========== INT subtract min overflow ============

_assert_error_(@INT_MIN - @INT_MAX, '_INT_OVERFLOW')


-- ========== INT multiply ============

_assert_(100000 * -2 = -200000)
_assert_(2 * -100000 = -200000)

_assert_(1000 * -150 = -150000)

_assert_(-1000 * 150 = -150000)

_assert_(1000 * null is null)
_assert_(null * 1000 is null)
_assert_(cast(null as int) * null is null)

_assert_(@INT_MAX / 2 * 2 = @INT_MAX - 1)
_assert_(@INT_MIN / 2 * 2 = @INT_MIN)


-- ========== INT multiply max overflow ============

_assert_error_(@INT_MAX * 2, '_INT_OVERFLOW')


-- ========== INT multiply max overflow ============

_assert_error_(@INT_MIN / -2 * 2, '_INT_OVERFLOW')


-- ========== INT multiply max overflow ============

_assert_error_(@INT_MIN * -1, '_INT_OVERFLOW')


-- ========== INT multiply max overflow ============

_assert_error_(@INT_MAX * @INT_MAX, '_INT_OVERFLOW')


-- ========== INT multiply max overflow ============

_assert_error_(@INT_MIN * @INT_MIN, '_INT_OVERFLOW')


-- ========== INT multiply min overflow ============

_assert_error_(@INT_MIN * 2, '_INT_OVERFLOW')


-- ========== INT multiply min overflow ============

_assert_error_(@INT_MAX * @INT_MIN, '_INT_OVERFLOW')


-- ========== INT divide ============

_assert_(100000 / 100000 = 1)

_assert_(100000 / 3 = 33333)

_assert_(100000 / -3 = -33333)

_assert_(-1000 / 3 = -333)

_assert_(-1000 / -3 = 333)

_assert_(0 / -3 = 0)

_assert_(@INT_MAX / -1 = -@INT_MAX)

_assert_(@INT_MIN / @INT_MAX = -1)

_assert_(@INT_MIN / @INT_MIN = 1)

_assert_(@INT_MAX / @INT_MIN = 0)

_assert_(1000 / null is null)
_assert_(null / 1000 is null)
_assert_(cast(null as int) / null is null)


-- ========== INT divide max overflow ============

_assert_error_(@INT_MIN / -1, '_INT_OVERFLOW')


-- ========== INT divide by zero ============

_assert_error_(100000/0, 'DIVIDE_BY_ZERO')


-- ========== INT modulo ============

_assert_(1000 % 3 = 1)

_assert_(1000 % -3 = 1)

_assert_(-1000 % 3 = -1)

_assert_(-1000 % -3 = -1)

_assert_(0 % -3 = 0)

_assert_(@INT_MAX % -1 = 0)

_assert_(@INT_MAX % 2 = 1)

_assert_(@INT_MIN % 2 = 0)

_assert_(@INT_MIN % @INT_MAX = -1)

_assert_(@INT_MAX % @INT_MIN = @INT_MAX)

_assert_(-@INT_MAX % @INT_MIN = -@INT_MAX)

_assert_(1000 % null is null)
_assert_(null % 1000 is null)
_assert_(cast(null as int) % null is null)


-- ========== INT modulo by zero ============

_assert_error_(100000 % 0, 'MODULO_BY_ZERO')


-- ========== INT binary op ============

_assert_(~123456 = -123457)

_assert_(123456 & 534627 = 8256)

_assert_(123456 | 534627 = 649827)

_assert_(123456 ^ 534627 = 641571)

_assert_(123456 | 526635 & 53678 ^ ~63781 = -14352)

_assert_(~cast(null as int) is null)

_assert_(1000 & null is null)
_assert_(null & 1000 is null)
_assert_(cast(null as int) & null is null)

_assert_(1000 | null is null)
_assert_(null | 1000 is null)
_assert_(cast(null as int) | null is null)

_assert_(1000 ^ null is null)
_assert_(null ^ 1000 is null)
_assert_(cast(null as int) ^ null is null)


-- ========== INT comp op ============

_assert_(156122 = 156122)

_assert_(not (156122 = 156123))

_assert_(not 156122 = 156123)

_assert_null_( 1000 = null)
_assert_null_( null = 1000)
_assert_null_( cast(null as int) = null)


_assert_(not (156122 != 156122))

_assert_(156122 != 156123)

_assert_null_( 1000 != null)
_assert_null_( null != 1000)
_assert_null_( cast(null as int) != null)


_assert_(156123 > 156122)

_assert_(not (156122 > 156122))

_assert_(-156121 > -156122)

_assert_(not (-156122 > -156122))

_assert_(1 > -1)

_assert_(not (-1 > 1))

_assert_null_( 1000 > null)
_assert_null_( null > 1000)
_assert_null_( cast(null as int) > null)


_assert_(156123 >= 156122)

_assert_(156122 >= 156122)

_assert_(not (156121 >= 156122))

_assert_(-156121 >= -156122)

_assert_(-156122 >= -156122)

_assert_(not (-156123 >= -156122))

_assert_(1 >= -1)

_assert_(not (-1 >= 1))

_assert_null_( 1000 >= null)
_assert_null_( null >= 1000)
_assert_null_( cast(null as int) >= null)


_assert_(156121 < 156122)

_assert_(not (156122 < 156122))

_assert_(-156123 < -156122)

_assert_(not (-156122 < -156122))

_assert_(not (1 < -1))

_assert_(-1 < 1)

_assert_null_( 1000 < null)
_assert_null_( null < 1000)
_assert_null_( cast(null as int) < null)


_assert_(156121 <= 156122)

_assert_(156122 <= 156122)

_assert_(not (156123 <= 156122))

_assert_(-156123 <= -156122)

_assert_(-156122 <= -156122)

_assert_(not (-156121 <= -156122))

_assert_(not (1 <= -1))

_assert_(-1 <= 1)

_assert_null_( 1000 <= null)
_assert_null_( null <= 1000)
_assert_null_( cast(null as int) <= null)


-- ========== INT is [not] null ============

_assert_(cast(NULL as int) is null)

_assert_(not 156123 is null)


_assert_(not cast(NULL as int) is not null)

_assert_(156123 is not null)


-- ========== INT [not] in list ============

_assert_(987612 in (987612))

_assert_(987612 in (0, 987612))

_assert_(987612 in (0, 987612, 9))

_assert_(987612 in (0, NULL, 987612, 9))

_assert_(987612 in (0, 987612, NULL, 9))


_assert_(not 987612 in (0, 1, 9))

_assert_null_(987612 in (0, 1, NULL, 9))


_assert_(not 987612 not in (987612))

_assert_(not 987612 not in (0, 987612))

_assert_(not 987612 not in (0, 987612, 9))

_assert_(not 987612 not in (0, NULL, 987612, 9))

_assert_(not 987612 not in (0, 987612, NULL, 9))


_assert_(987612 not in (0, 1, 9))

_assert_null_(987612 not in (0, 1, NULL, 9))


-- ========== INT cast ============

_assert_(cast(123 as char(10)) + 'x' = '123       x')
_assert_(cast(123 as char) = '123')
_assert_(cast(123 as char(3)) = '123')
_assert_(cast(-123 as char(4)) = '-123')
_assert_(cast(@INT_MAX as char) = '2147483647')
_assert_(cast(@INT_MIN as char) = '-2147483648')
_assert_(cast(cast(null as int) as char) is null)

_assert_(cast(123 as varchar(10)) + 'x' = '123x')
_assert_(cast(123 as varchar) = '123')
_assert_(cast(123 as varchar(3)) = '123')
_assert_(cast(-123 as varchar(4)) = '-123')
_assert_(cast(@INT_MAX as varchar) = '2147483647')
_assert_(cast(@INT_MIN as varchar) = '-2147483648')
_assert_(cast(cast(null as int) as varchar) is null)

_assert_(cast(987123 as bit) = 1)
_assert_(cast(-987123 as bit) = 1)
_assert_(cast(0 as bit) = 0)
_assert_(cast(cast(null as int) as bit) is null)

_assert_(cast(255 as tinyint) = 255)
_assert_(cast(0 as tinyint) = 0)
_assert_(cast(cast(null as int) as tinyint) is null)

_assert_(cast(32767 as smallint) = 32767)
_assert_(cast(-32768 as smallint) = -32768)
_assert_(cast(cast(null as int) as smallint) is null)

_assert_(cast(@INT_MAX as int) = @INT_MAX)
_assert_(cast(@INT_MIN as int) = @INT_MIN)
_assert_(cast(cast(null as int) as int) is null)

_assert_(cast(@INT_MAX as bigint) = @INT_MAX)
_assert_(cast(@INT_MIN as bigint) = @INT_MIN)
_assert_(cast(cast(null as int) as bigint) is null)

_assert_(cast(@INT_MAX as money) = @INT_MAX)
_assert_(cast(@INT_MIN as money) = @INT_MIN)
_assert_(cast(cast(null as int) as money) is null)

_assert_(cast(@INT_MAX as numeric(12,2)) = @INT_MAX)
_assert_(cast(@INT_MIN as numeric(12,2)) = @INT_MIN)
_assert_(cast(cast(null as int) as numeric(12,2)) is null)

_assert_(cast(12345 as numeric(5)) = 12345)
_assert_(cast(-12345 as numeric(5)) = -12345)
_assert_(cast(cast(null as int) as numeric(5)) is null)

_assert_(cast(@INT_MAX as float) = @INT_MAX)
_assert_(cast(@INT_MIN as float) = @INT_MIN)
_assert_(cast(cast(null as int) as float) is null)

_assert_(cast(@INT_MAX_DATETIME as datetime) = '99991231')
_assert_(cast(0 as datetime) = '19000101')
_assert_(cast(@INT_MIN_DATETIME as datetime) = '00010101')


-- ========== INT cast overflow ============

_assert_error_(cast(123 as char(2)), '_INT_CAST_VARCHAR_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(-123 as char(3)), '_INT_CAST_VARCHAR_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(123 as varchar(2)), '_INT_CAST_VARCHAR_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(-123 as varchar(3)), '_INT_CAST_VARCHAR_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(256 as tinyint), '_INT_CAST_TINYINT_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(-1 as tinyint), '_INT_CAST_TINYINT_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(32768 as smallint), '_INT_CAST_SMALLINT_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(-32769 as smallint), '_INT_CAST_SMALLINT_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(12345 as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(-12345 as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(@INT_MAX_DATETIME + 1 as datetime), 'DATETIME_OVERFLOW')


-- ========== INT cast overflow ============

_assert_error_(cast(@INT_MIN_DATETIME - 1 as datetime), 'DATETIME_OVERFLOW')


-- ========== INT case when ============

_assert_(case when 1=1 then cast(1000000 as int) end = cast(1000000 as int))
_assert_(case when 1=4 then cast(1000000 as int) end is null)
_assert_(case when 1=1 then cast(1000000 as int) when 1=2 then cast(2200000 as int) else cast(330000 as int) end = cast(1000000 as int))
_assert_(case when 1=1 then cast(1000000 as int) when 1=2 then cast(2200000 as int) end = cast(1000000 as int))
_assert_(case when 1=4 then cast(1000000 as int) when 2=2 then cast(2200000 as int) else cast(330000 as int) end = cast(2200000 as int))
_assert_(case when 1=NULL then cast(1000000 as int) when 2=2 then cast(2200000 as int) else cast(330000 as int) end = cast(2200000 as int))
_assert_(case when 1=NULL then cast(1000000 as int) when 2=NULL then cast(550000 as int) else cast(330000 as int) end = cast(330000 as int))
_assert_(case when 1=NULL then cast(1000000 as int) when 2=NULL then cast(2200000 as int) end is null)


