

set language french

declare @TINYINT_MAX tinyint =  255
declare @TINYINT_MIN tinyint =    0

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648


-- ========== TINYINT miscellaneous ============


_assert_(-cast(255 as tinyint) = -255)

_assert_(-cast(0 as tinyint) = 0)

_assert_(-cast(0 as tinyint) = -0)

_assert_(cast(10 as tinyint) - cast(3 as tinyint) = cast(7 as tinyint))

_assert_(cast(10 as tinyint) + cast(5 as tinyint) = cast(15 as tinyint))

_assert_(@TINYINT_MAX * cast(1 as tinyint) = cast(255 as tinyint))


-- ==========TINYINT unary minus ============

_assert_(++++cast(4 as tinyint) = cast(4 as tinyint))
_assert_(-cast(4 as tinyint) = -1 * cast(4 as tinyint))
_assert_(- -cast(4 as tinyint) = cast(4 as tinyint))
_assert_(++- - -++cast(4 as tinyint) = - cast(4 as tinyint))
_assert_(++cast(4 as tinyint) = cast(4 as tinyint))
_assert_(-cast(4 as tinyint) = -4)

_assert_(-cast(NULL as tinyint) is null)


-- ========== TINYINT add ============

_assert_(cast(100 as tinyint) + cast(15 as tinyint) = cast(115 as tinyint))
_assert_(cast(100 as tinyint) + cast(155 as tinyint) = cast(255 as tinyint))

_assert_(cast(0 as tinyint) - 1 = -1)

_assert_(cast(100 as tinyint) + null is null)
_assert_(null + cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) + null is null)


-- ========== TINYINT add max overflow ============

_assert_error_(@TINYINT_MAX + cast(1 as tinyint), 'TINYINT_OVERFLOW')


-- ========== TINYINT add max overflow ============

_assert_error_(@TINYINT_MAX + @TINYINT_MAX, 'TINYINT_OVERFLOW')


-- ========== TINYINT add max overflow ============

_assert_error_(cast(200 as tinyint) + cast(56 as tinyint), 'TINYINT_OVERFLOW')


-- ========== TINYINT subtract ============

_assert_(cast(100 as tinyint) - cast(15 as tinyint) = cast(85 as tinyint))

_assert_(cast(100 as tinyint) - null is null)
_assert_(null - cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) - null is null)


-- ========== TINYINT subtract max overflow ============

_assert_error_(@TINYINT_MAX - cast(1 as tinyint) - cast(255 as tinyint), 'TINYINT_OVERFLOW')


-- ========== TINYINT subtract min overflow ============

_assert_error_(@TINYINT_MIN - cast(1 as tinyint), 'TINYINT_OVERFLOW')


-- ========== TINYINT subtract min overflow ============

_assert_error_(@TINYINT_MIN - @TINYINT_MAX, 'TINYINT_OVERFLOW')


-- ========== TINYINT subtract max overflow ============

_assert_error_(cast(200 as tinyint) - cast(201 as tinyint), 'TINYINT_OVERFLOW')


-- ========== TINYINT multiply ============

_assert_(cast(100 as tinyint) * cast(2 as tinyint) = cast(200 as tinyint))

_assert_(cast(255 as tinyint) * cast(1 as tinyint) = cast(255 as tinyint))

_assert_(@TINYINT_MAX / cast(2 as tinyint) * cast(2 as tinyint) = @TINYINT_MAX - cast(1 as tinyint))
_assert_(@TINYINT_MIN / cast(2 as tinyint) * cast(2 as tinyint) = @TINYINT_MIN)

_assert_(cast(100 as tinyint) * null is null)
_assert_(null * cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) * null is null)


-- ========== TINYINT multiply max overflow ============

_assert_error_(@TINYINT_MAX * cast(2 as tinyint), 'TINYINT_OVERFLOW')


-- ========== TINYINT multiply max overflow ============

_assert_error_(cast(200 as tinyint) * cast(2 as tinyint), 'TINYINT_OVERFLOW')


-- ========== TINYINT multiply max overflow ============

_assert_error_(@TINYINT_MAX * @TINYINT_MAX, 'TINYINT_OVERFLOW')


-- ========== TINYINT divide ============

_assert_(cast(100 as tinyint) / cast(3 as tinyint) = cast(33 as tinyint))

_assert_(cast(100 as tinyint) / -3 = -33)

_assert_(-1000 / cast(3 as tinyint) = -333)

_assert_(cast(0 as tinyint) / cast(3 as tinyint) = cast(0 as tinyint))

_assert_(@TINYINT_MAX / cast(1 as tinyint) = @TINYINT_MAX)

_assert_(@TINYINT_MAX / @TINYINT_MAX = cast(1 as tinyint))

_assert_(@TINYINT_MAX / -1 = -@TINYINT_MAX)

_assert_(cast(100 as tinyint) / null is null)
_assert_(null / cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) / null is null)


-- ========== TINYINT divide by zero ============

_assert_error_(cast(1 as tinyint)/cast(0 as tinyint), 'DIVIDE_BY_ZERO')


-- ========== TINYINT modulo ============

_assert_(cast(100 as tinyint) % cast(3 as tinyint) = cast(1 as tinyint))

_assert_(cast(0 as tinyint) % cast(3 as tinyint) = cast(0 as tinyint))

_assert_(@TINYINT_MAX % cast(1 as tinyint) = cast(0 as tinyint))

_assert_(@TINYINT_MAX % cast(2 as tinyint) = cast(1 as tinyint))

_assert_(@TINYINT_MIN % cast(2 as tinyint) = cast(0 as tinyint))


_assert_(cast(100 as tinyint) % null is null)
_assert_(null % cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) % null is null)


-- ========== TINYINT modulo by zero ============

_assert_error_(cast(1 as tinyint) % cast(0 as tinyint), 'MODULO_BY_ZERO')


-- ========== TINYINT binary op ============

_assert_(~cast(145 as tinyint) = cast(110 as tinyint))

_assert_(cast(145 as tinyint) & cast(92 as tinyint) = cast(16 as tinyint))

_assert_(cast(145 as tinyint) | cast(92 as tinyint) = cast(221 as tinyint))

_assert_(cast(145 as tinyint) ^ cast(92 as tinyint) = cast(205 as tinyint))

_assert_(cast(145 as tinyint) | cast(92 as tinyint) & cast(188 as tinyint) ^ ~cast(234 as tinyint) = cast(137 as tinyint))

_assert_(~cast(null as tinyint) is null)

_assert_(cast(100 as tinyint) & null is null)
_assert_(null & cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) & null is null)

_assert_(cast(100 as tinyint) | null is null)
_assert_(null | cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) | null is null)

_assert_(cast(100 as tinyint) ^ null is null)
_assert_(null ^ cast(100 as tinyint) is null)
_assert_(cast(null as tinyint) ^ null is null)


-- ========== TINYINT comp op ============

_assert_(cast(122 as tinyint) = cast(122 as tinyint))

_assert_(not (cast(122 as tinyint) = cast(123 as tinyint)))

_assert_(not cast(122 as tinyint) = cast(123 as tinyint))

_assert_null_(cast(100 as tinyint) = null)
_assert_null_( null = cast(100 as tinyint))
_assert_null_(cast(null as tinyint) = null)


_assert_(not (cast(122 as tinyint) != cast(122 as tinyint)))

_assert_(cast(122 as tinyint) != cast(123 as tinyint))

_assert_null_(cast(100 as tinyint) != null)
_assert_null_( null != cast(100 as tinyint))
_assert_null_(cast(null as tinyint) != null)


_assert_(cast(123 as tinyint) > cast(122 as tinyint))

_assert_(not (cast(122 as tinyint) > cast(122 as tinyint)))

_assert_null_( cast(100 as tinyint) > null)
_assert_null_( null > cast(100 as tinyint))
_assert_null_(cast(null as tinyint) > null)


_assert_(cast(123 as tinyint) >= cast(122 as tinyint))

_assert_(cast(122 as tinyint) >= cast(122 as tinyint))

_assert_(not (cast(121 as tinyint) >= cast(122 as tinyint)))

_assert_null_(  cast(100 as tinyint) >= null)
_assert_null_( null >= cast(100 as tinyint))
_assert_null_( cast(null as tinyint) >= null)


_assert_(cast(121 as tinyint) < cast(122 as tinyint))

_assert_(not (cast(122 as tinyint) < cast(122 as tinyint)))

_assert_null_(cast(100 as tinyint) < null)
_assert_null_(null < cast(100 as tinyint))
_assert_null_( cast(null as tinyint) < null)


_assert_(cast(121 as tinyint) <= cast(122 as tinyint))

_assert_(cast(122 as tinyint) <= cast(122 as tinyint))

_assert_(not (cast(123 as tinyint) <= cast(122 as tinyint)))

_assert_null_(  cast(100 as tinyint) <= null)
_assert_null_( null <= cast(100 as tinyint))
_assert_null_( cast(null as tinyint) <= null)


-- ========== TINYINT is [not] null ============

_assert_(cast(NULL as tinyint) is null)

_assert_(not cast(123 as tinyint) is null)


_assert_(not cast(NULL as tinyint) is not null)

_assert_(cast(123 as tinyint) is not null)


-- ========== TINYINT [not] in list ============

_assert_(cast(12 as tinyint) in (cast(12 as tinyint)))

_assert_(cast(12 as tinyint) in (cast(0 as tinyint), cast(12 as tinyint)))

_assert_(cast(12 as tinyint) in (cast(0 as tinyint), cast(12 as tinyint), cast(9 as tinyint)))

_assert_(cast(12 as tinyint) in (cast(0 as tinyint), NULL, cast(12 as tinyint), cast(9 as tinyint)))

_assert_(cast(12 as tinyint) in (cast(0 as tinyint), cast(12 as tinyint), NULL, cast(9 as tinyint)))


_assert_(not cast(12 as tinyint) in (cast(0 as tinyint), cast(1 as tinyint), cast(9 as tinyint)))

_assert_null_(cast(12 as tinyint) in (cast(0 as tinyint), cast(1 as tinyint), NULL, cast(9 as tinyint)))


_assert_(not cast(12 as tinyint) not in (cast(12 as tinyint)))

_assert_(not cast(12 as tinyint) not in (cast(0 as tinyint), cast(12 as tinyint)))

_assert_(not cast(12 as tinyint) not in (cast(0 as tinyint), cast(12 as tinyint), cast(9 as tinyint)))

_assert_(not cast(12 as tinyint) not in (cast(0 as tinyint), NULL, cast(12 as tinyint), cast(9 as tinyint)))

_assert_(not cast(12 as tinyint) not in (cast(0 as tinyint), cast(12 as tinyint), NULL, cast(9 as tinyint)))


_assert_(cast(12 as tinyint) not in (cast(0 as tinyint), cast(1 as tinyint), cast(9 as tinyint)))

_assert_null_(cast(12 as tinyint) not in (cast(0 as tinyint), cast(1 as tinyint), NULL, cast(9 as tinyint)))


-- ========== TINYINT cast ============

_assert_(cast(cast(123 as tinyint) as char(10)) + 'x' = '123       x')
_assert_(cast(cast(123 as tinyint) as char) = '123')
_assert_(cast(cast(123 as tinyint) as char(3)) = '123')
_assert_(cast(@TINYINT_MAX as char) = '255')
_assert_(cast(@TINYINT_MIN as char) = '0')
_assert_(cast(cast(null as tinyint) as char) is null)

_assert_(cast(cast(123 as tinyint) as varchar(10)) + 'x' = '123x')
_assert_(cast(cast(123 as tinyint) as varchar) = '123')
_assert_(cast(cast(123 as tinyint) as varchar(3)) = '123')
_assert_(cast(@TINYINT_MAX as varchar) = '255')
_assert_(cast(@TINYINT_MIN as varchar) = '0')
_assert_(cast(cast(null as tinyint) as varchar) is null)

_assert_(cast(cast(123 as tinyint) as bit) = 1)
_assert_(cast(cast(0 as tinyint) as bit) = 0)
_assert_(cast(cast(null as tinyint) as bit) is null)

_assert_(cast(cast(255 as tinyint) as tinyint) = 255)
_assert_(cast(cast(0 as tinyint) as tinyint) = 0)
_assert_(cast(cast(null as tinyint) as tinyint) is null)

_assert_(cast(cast(255 as tinyint) as smallint) = 255)
_assert_(cast(cast(100 as tinyint) as smallint) = 100)
_assert_(cast(cast(0 as tinyint) as smallint) = 0)
_assert_(cast(cast(null as tinyint) as smallint) is null)

_assert_(cast(cast(255 as tinyint) as int) = 255)
_assert_(cast(cast(0 as tinyint) as int) = 0)
_assert_(cast(cast(null as tinyint) as int) is null)

_assert_(cast(cast(255 as tinyint) as bigint) = 255)
_assert_(cast(cast(0 as tinyint) as bigint) = 0)
_assert_(cast(cast(null as tinyint) as bigint) is null)

_assert_(cast(cast(255 as tinyint) as money) = 255)
_assert_(cast(cast(0 as tinyint) as money) = 0)
_assert_(cast(cast(null as tinyint) as money) is null)

_assert_(cast(cast(255 as tinyint) as numeric(12,2)) = 255)
_assert_(cast(cast(0 as tinyint) as numeric(12,2)) = 0)
_assert_(cast(cast(null as tinyint) as numeric(12,2)) is null)

_assert_(cast(cast(255 as tinyint) as numeric(3)) = 255)
_assert_(cast(cast(78 as tinyint) as numeric(2)) = 78)
_assert_(cast(cast(0 as tinyint) as numeric(1)) = 0)
_assert_(cast(cast(null as tinyint) as numeric(5)) is null)

_assert_(cast(cast(255 as tinyint) as float) = 255)
_assert_(cast(cast(0 as tinyint) as float) = 0)
_assert_(cast(cast(null as tinyint) as float) is null)

_assert_(cast(@TINYINT_MAX as datetime) = '19000913')
_assert_(cast(cast(0 as tinyint) as datetime) = '19000101')


-- ========== TINYINT cast overflow ============

_assert_error_(cast(cast(123 as tinyint) as char(2)), 'TINYINT_CAST_VARCHAR_OVERFLOW')


-- ========== TINYINT cast overflow ============

_assert_error_(cast(cast(123 as tinyint) as varchar(2)), 'TINYINT_CAST_VARCHAR_OVERFLOW')


-- ========== TINYINT cast overflow ============

_assert_error_(cast(cast(256 as tinyint) as tinyint), '_INT_CAST_TINYINT_OVERFLOW')


-- ========== TINYINT cast overflow ============

_assert_error_(cast(cast(123 as tinyint) as numeric(2)), 'NUMERIC_OVERFLOW')


-- ========== TINYINT case when ============

_assert_(case when 1=1 then cast(100 as tinyint) end = cast(100 as tinyint))
_assert_(case when 1=4 then cast(100 as tinyint) end is null)
_assert_(case when 1=1 then cast(100 as tinyint) when 1=2 then cast(220 as tinyint) else cast(33 as tinyint) end = cast(100 as tinyint))
_assert_(case when 1=1 then cast(100 as tinyint) when 1=2 then cast(220 as tinyint) end = cast(100 as tinyint))
_assert_(case when 1=4 then cast(100 as tinyint) when 2=2 then cast(220 as tinyint) else cast(33 as tinyint) end = cast(220 as tinyint))
_assert_(case when 1=NULL then cast(100 as tinyint) when 2=2 then cast(220 as tinyint) else cast(33 as tinyint) end = cast(220 as tinyint))
_assert_(case when 1=NULL then cast(100 as tinyint) when 2=NULL then cast(55 as tinyint) else cast(33 as tinyint) end = cast(33 as tinyint))
_assert_(case when 1=NULL then cast(100 as tinyint) when 2=NULL then cast(220 as tinyint) end is null)




