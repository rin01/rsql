
set language french

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @BIGINT_MAX bigint =  9223372036854775807
declare @BIGINT_MIN bigint = -9223372036854775808


-- ========== VARBINARY MISCELLANEOUS ============

declare @ay varbinary(40) = 0x
_assert_(cast(@ay as varbinary(4)) = 0x)

set @ay = 0x4e49434F
_assert_(cast(@ay as varbinary(4)) = 0x4e49434F)


-- ========== VARBINARY add ============

_assert_(0x1234 + 0x5678 = 0x12345678)

_assert_(0x123 + 0x5678 = 0x01235678)

_assert_(0x123 + 0x678 = 0x01230678)

_assert_(0x + 0x678 = 0x0678)

_assert_(0x + 0x = 0x)

_assert_(0x1234abC + 123 = 19090231)


_assert_(0x + NULL is null)

_assert_(NULL + 0x is null)


-- ========== VARBINARY add error ============

_assert_error_(0x1234abC + 123 + 'avdr', 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARBINARY comp op ============

_assert_(0x122 = 0x122)

_assert_(cast(0x1234 as varbinary(20)) = cast(0x1234 as varbinary(55)))

_assert_(not (0x122 = 0x123))

_assert_(not 0x122 = 0x123)

_assert_null_( 0x1000 = null)
_assert_null_( null = 0x1000)
_assert_null_( cast(null as varbinary) = null)


_assert_(not (0x122 != 0x122))

_assert_(0x122 != 0x123)

_assert_null_( 0x1000 != null)
_assert_null_( null != 0x1000)
_assert_null_( cast(null as varbinary) != null)


declare @b1 as varbinary(4) = 0x12345678
declare @b2 as varbinary(8)

set @b2 = @b1

_assert_(@b1 = @b2)


-- ========== VARBINARY is [not] null ============

_assert_(cast(NULL as varbinary) is null)

_assert_(not 0x123 is null)


_assert_(not cast(NULL as varbinary) is not null)

_assert_(0x123 is not null)


-- ========== VARBINARY [not] in list ============

_assert_(0x12 in (0x12))

_assert_(0x12 in (0x, 0x12))

_assert_(0x12 in (0x, 0x12, 0x9))

_assert_(0x12 in (0x, NULL, 0x12, 0x9))

_assert_(0x12 in (0x, 0x12, NULL, 0x9))


_assert_(not 0x12 in (0x, 0x1, 0x9))

_assert_null_(0x12 in (0x, 0x1, NULL, 0x9))


_assert_(not 0x12 not in (0x12))

_assert_(not 0x12 not in (0x, 0x12))

_assert_(not 0x12 not in (0x, 0x12, 0x9))

_assert_(not 0x12 not in (0x, NULL, 0x12, 0x9))

_assert_(not 0x12 not in (0x, 0x12, NULL, 0x9))


_assert_(0x12 not in (0x, 0x1, 0x9))

_assert_null_(0x12 not in (0x, 0x1, NULL, 0x9))



-- ========== VARBINARY cast ============

_assert_(cast(0x123 as varbinary(5)) = 0x0123)

_assert_(cast(0x123 as varbinary(2)) = 0x0123)

_assert_(cast(cast(NULL as varbinary(2)) as varbinary) is null)


_assert_(cast(0x as bit) = 0)

_assert_(cast(0x00 as bit) = 0)

_assert_(cast(0x00000 as bit) = 0)

_assert_(cast(0x1 as bit) = 1)

_assert_(cast(0x01 as bit) = 1)

_assert_(cast(cast(0x01 as varbinary(10)) as bit) = 1)

_assert_(cast(cast(NULL as varbinary(2)) as bit) is null)


_assert_(cast(0x as tinyint) = 0)

_assert_(cast(0x00 as tinyint) = 0)

_assert_(cast(0x00000 as tinyint) = 0)

_assert_(cast(0x1 as tinyint) = 1)

_assert_(cast(0x01 as tinyint) = 1)

_assert_(cast(0x0f as tinyint) = 15)

_assert_(cast(0xff as tinyint) = 255)

_assert_(cast(cast(0x01 as varbinary(10)) as tinyint) = 1)

_assert_(cast(cast(0xff as varbinary(10)) as tinyint) = 255)

_assert_(cast(cast(NULL as varbinary(2)) as tinyint) is null)


_assert_(cast(0x as smallint) = 0)

_assert_(cast(0x00 as smallint) = 0)

_assert_(cast(0x00000 as smallint) = 0)

_assert_(cast(0x1 as smallint) = 1)

_assert_(cast(0x01 as smallint) = 1)

_assert_(cast(0x0f as smallint) = 15)

_assert_(cast(0xff as smallint) = 255)

_assert_(cast(cast(0x01 as varbinary(10)) as smallint) = 1)

_assert_(cast(cast(0xff as varbinary(10)) as smallint) = 255)

_assert_(cast(cast(0x835e as varbinary(10)) as smallint) = -31906)

_assert_(cast(cast(0x7fff as varbinary(10)) as smallint) = @SMALLINT_MAX)

_assert_(cast(cast(0x8000 as varbinary(10)) as smallint) = @SMALLINT_MIN)

_assert_(cast(cast(NULL as varbinary(2)) as smallint) is null)


_assert_(cast(0x as int) = 0)

_assert_(cast(0x00 as int) = 0)

_assert_(cast(0x00000 as int) = 0)

_assert_(cast(0x1 as int) = 1)

_assert_(cast(0x01 as int) = 1)

_assert_(cast(0x0f as int) = 15)

_assert_(cast(0xff as int) = 255)

_assert_(cast(cast(0x01 as varbinary(10)) as int) = 1)

_assert_(cast(cast(0xff as varbinary(10)) as int) = 255)

_assert_(cast(cast(0x835e63ab as varbinary(10)) as int) = -2090966101)

_assert_(cast(cast(0x7fffffff as varbinary(10)) as int) = @INT_MAX)

_assert_(cast(cast(0x80000000 as varbinary(10)) as int) = @INT_MIN)

_assert_(cast(cast(NULL as varbinary(2)) as int) is null)


_assert_(cast(0x as bigint) = 0)

_assert_(cast(0x00 as bigint) = 0)

_assert_(cast(0x00000 as bigint) = 0)

_assert_(cast(0x1 as bigint) = 1)

_assert_(cast(0x01 as bigint) = 1)

_assert_(cast(0x0f as bigint) = 15)

_assert_(cast(0xff as bigint) = 255)

_assert_(cast(cast(0x01 as varbinary(10)) as bigint) = 1)

_assert_(cast(cast(0xff as varbinary(10)) as bigint) = 255)

_assert_(cast(cast(0x835e63ab12345678 as varbinary(10)) as bigint) = -8980631020534213000)

_assert_(cast(cast(0x7fffffffffffffff as varbinary(10)) as bigint) = @BIGINT_MAX)

_assert_(cast(cast(0x8000000000000000 as varbinary(10)) as bigint) = @BIGINT_MIN)

_assert_(cast(cast(NULL as varbinary(2)) as bigint) is null)


_assert_(cast(0x123 as varbinary(1)) = 0x01)


_assert_(cast(0x as bit) = 0)
_assert_(cast(0x0 as bit) = 0)
_assert_(cast(0x000000 as bit) = 0)

_assert_(cast(0x10 as bit) = 1)
_assert_(cast(0x02 as bit) = 1)
_assert_(cast(0x0234 as bit) = 1)


-- ========== VARBINARY overflow ============

_assert_error_(cast(0x0234 as tinyint), 'VARBINARY_CAST_TINYINT_OVERFLOW')


-- ========== VARBINARY overflow ============

_assert_error_(cast(0x123456 as smallint), 'VARBINARY_CAST_SMALLINT_OVERFLOW')


-- ========== VARBINARY overflow ============

_assert_error_(cast(0x123456789 as int), 'VARBINARY_CAST_INT_OVERFLOW')


-- ========== VARBINARY overflow ============

_assert_error_(cast(0x12345678123456789 as bigint), 'HEXASTRING_LENGTH_EXCEEDS_UINT64_SIZE')


-- ========== VARBINARY case when ============

_assert_(case when 1=1 then 0xabcd end = 0xabcd)
_assert_(case when 1=4 then 0xabcd end is null)
_assert_(case when 1=1 then 0xabcd when 1=2 then 0x1234567890 else 0x end = 0xabcd)
_assert_(case when 1=1 then 0xabcd when 1=2 then 0x1234567890 end = 0xabcd)
_assert_(case when 1=4 then 0xabcd when 2=2 then 0x1234567890 else 0x end = 0x1234567890)
_assert_(case when 1=NULL then 0xabcd when 2=2 then 0x1234567890 else 0x end = 0x1234567890)
_assert_(case when 1=NULL then 0xabcd when 2=NULL then 0x66778899 else 0x end = 0x)
_assert_(case when 1=NULL then 0xabcd when 2=NULL then 0x1234567890 end is null)






