

set language french

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @BIGINT_MAX bigint =  9223372036854775807
declare @BIGINT_MIN bigint = -9223372036854775808

declare @a varchar(10) = 'Hello'
declare @b varchar(10) = 'World'

declare @t_s3 varchar(10) = 'HeLLo'
declare @u_s3 varchar(10) = 'hEllo'
declare @w_es_s3 varchar(10) = 'HELlo'

declare @a_fix char(10) = 'Hello'
declare @b_fix char(10) = 'World'

declare @a_fix_empty char(10) = ''
declare @b_fix_empty char(10) = ''


-- ========== VARCHAR MISCELLANEOUS ============


_assert_('<' + cast(cast(' 000000300000000012345  ' as bigint) +1 as varchar(40)) +'>' = '<300000000012346>')

_assert_($-000000922337203.12 + $122.38 = $-922337080.7400)

declare @a5 money

set @a5 = 12.456+3.14156789f

_assert_(@a5 = $15.5976)

_assert_('ab' + '' = 'ab')
_assert_(charlen('' + 'ab' + '') = 2)

_assert_(cast(cast(cast(cast(123 as int) as int) + 123.4 as varchar(8)) + 'AAA' as varchar(2)) = '24')


decLare @ab numeric(6, 2) = 34.55

seT @ab = 100f + cast(2 as bigInt) + @Ab*2

_assert_(caSt((75648.45f * 637.3d ) / ( 34345345 + 34455 / (45.55d - cast('4456.456' as float) + 4678)) * ( @ab + 2 )as numeRic(34, 10)) = 242.9805472721)


_assert_(cast(-620 as money) = $-620)


declare @q numeric(6,2) = 12.34


_assert_(left('Nicolas', 10) + 'OLAS' + left('', 3) + '_' + right('gggggggghhhhhhhzuipqpqpw', 6) + '_' + substring('ghouaaCoolbhaawa', 0, 100) + '_' + cast(123 + len('asdf') + 3 + 70.0 as varchar) = 'NicolasOLAS_pqpqpw_ghouaaCoolbhaawa_200.0')

_assert_('<' + ltrim('      nico    ') + '>'        = '<nico    >')
_assert_('<' + rtrim('      nico    ') + '>'        = '<      nico>')
_assert_('<' + ltrim(rtrim('      nico    ')) + '>' = '<nico>')


declare @a6 varchar(16)
declare @b6 varchar(16)

set @a6 = 'nicococoCOlas'
set @b6 = @a6
_assert_(replace(@a6  collate fr_ci_ai, 'coco', 'X')   = 'niXXlas')
_assert_(@a6 collate fr_ci_ai = 'nicococoCOlas')
_assert_(replace('aXa', 'X', 'KOKO') = 'aKOKOa')

_assert_(replace(@b6 collate fr_cs_as, 'coco', 'X') = 'niXcoCOlas')

_assert_(upper('bla_') + upper(@b6 collate fr_cs_as) + ' ' + lower(@b6 collate fr_cs_as) + '_bla' = 'BLA_NICOCOCOCOLAS nicocococolas_bla')



declare @ak varchar(4) = NULL
declare @bk varchar(6) = 'xxx'

_assert_(isnull(@ak, @bk) = 'xxx')


_assert_(convert(varchar, 1234.5678, 2) = '1234.5678') -- the style parameter is just ignored

declare @i int = 4
_assert_('<' + space(@i) + '>' = '<    >')

declare @ag char(4) = 'ab'


_assert_('<' + replicate(@ag, 4) + '>' = '<ab  ab  ab  ab  >')

_assert_( STUFF('abcghi', 4, 0, 'DEF') = 'abcDEFghi')

_assert_('NIII' + cast('123.478' *4.56f -(945/35.45+2f) as varchar(40)) + 'COOO' = 'NIII534.402COOO')



_assert_(coalesce(NULL, 'asdf' , 'bla' collate es_ci_ai, 'utu' collate es_ci_ai) = 'ASDF')
_assert_(coalesce(NULL, 'asdf' , 'bla' collate es_cs_as, 'utu' collate es_cs_as) = 'asdf')
_assert_(coalesce(NULL, 'asdf' , 'bla' collate es_cs_as, 'utu' collate es_cs_as) != 'Asdf')


-- ========== VARCHAR simplify CAST ============


declare @c char(4) = 'as'
declare @d char(4) = 'asdf'
declare @e char(4) = 'as'
declare @v varchar(4) = 'as'
declare @w varchar(4) = 'asd'
declare @x varchar(4) = 'asdf'


-- C-C-C
_assert_('<' + cast(cast(@c as char(4)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as char(3)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as char(3)) collate es_ci_ai as char(3)) collate es_ci_ai + '>' = '<as >')
_assert_('<' + cast(cast(@c as char(2)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as char(4)) as char(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as char(4)) collate de_ci_ai as char(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as char(4)) as char(5)) + '>' = '<as   >')
_assert_('<' + cast(cast(@c as char(4)) collate de_ci_ai as char(5)) + '>' = '<as   >')
_assert_('<' + cast(cast(@e as char(4))  as char(4)) + '>' = '<as  >')


-- C-C-V   ( no simplification )
_assert_('<' + cast(cast(@c as char(10)) as varchar(10)) + '>' = '<as        >')
_assert_('<' + cast(cast(@d as char(10)) as varchar(10)) + '>' = '<asdf      >')


-- C-V-C
_assert_('<' + cast(cast(@c as varchar(4)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as varchar(3)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as varchar(3)) collate es_ci_ai as char(3)) collate es_ci_ai + '>' = '<as >')
_assert_('<' + cast(cast(@c as varchar(2)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as varchar(4)) as char(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as varchar(4)) collate de_ci_ai as char(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as varchar(4)) as char(5)) + '>' = '<as   >')
_assert_('<' + cast(cast(@c as varchar(4)) collate de_ci_ai as char(5)) + '>' = '<as   >')
_assert_('<' + cast(cast(@c as varchar(5)) collate de_ci_ai as char(5)) + '>' = '<as   >')
_assert_('<' + cast(cast(@d as varchar(5)) collate de_ci_ai as char(5)) + '>' = '<asdf >')


-- C-V-V
_assert_('<' + cast(cast(@c as varchar(4)) as varchar(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as varchar(3)) as varchar(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@c as varchar(3)) collate es_ci_ai as varchar(3)) collate es_ci_ai + '>' = '<as >')
_assert_('<' + cast(cast(@c as varchar(2)) as varchar(3)) + '>' = '<as>')
_assert_('<' + cast(cast(@c as varchar(4)) as varchar(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as varchar(4)) collate de_ci_ai as varchar(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as varchar(4)) as varchar(5)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as varchar(4)) collate de_ci_ai as varchar(5)) + '>' = '<as  >')
_assert_('<' + cast(cast(@c as varchar(5)) collate de_ci_ai as varchar(5)) + '>' = '<as  >')
_assert_('<' + cast(cast(@d as varchar(5)) collate de_ci_ai as varchar(5)) + '>' = '<asdf>')

-- V-C-C
_assert_('<' + cast(cast(@v as char(4)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@v as char(3)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@v as char(3)) collate es_ci_ai as char(3)) collate es_ci_ai + '>' = '<as >')
_assert_('<' + cast(cast(@v as char(2)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@v as char(4)) as char(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@v as char(4)) collate de_ci_ai as char(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@v as char(4)) as char(5)) + '>' = '<as   >')
_assert_('<' + cast(cast(@v as char(4)) collate de_ci_ai as char(5)) + '>' = '<as   >')

_assert_('<' + cast(cast(@w as char(4)) as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@w as char(3)) as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@w as char(2)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@w as char(4)) as char(4)) + '>' = '<asd >')
_assert_('<' + cast(cast(@w as char(4)) as char(5)) + '>' = '<asd  >')

_assert_('<' + cast(cast('asdf' collate ru_ci_ai as char(4)) as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@x as char(3)) collate ru_ci_ai as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@x as char(2)) collate ru_ci_ai as char(3)) + '>' = '<as >')
_assert_('<' + ( cast(cast(@x as char(4)) collate ru_ci_ai as char(4)) collate es_ci_ai ) collate de_ci_ai + '>' = '<asdf>')
_assert_('<' + cast(cast(@x as char(4)) as char(5)) + '>' = '<asdf >')
_assert_('<' + cast(cast(@x as char(5)) as char(5)) + '>' = '<asdf >')


-- V-C-V   ( no simplification )
_assert_('<' + cast(cast('as' as char(10)) as varchar(10)) + '>' = '<as        >')


-- V-V-C
_assert_('<' + cast(cast(@v as varchar(4)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@v as varchar(3)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@v as varchar(2)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@v as varchar(4)) as char(4)) + '>' = '<as  >')
_assert_('<' + cast(cast(@v as varchar(4)) as char(5)) + '>' = '<as   >')

_assert_('<' + cast(cast(@w as varchar(4)) as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@w as varchar(3)) as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@w as varchar(2)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@w as varchar(4)) as char(4)) + '>' = '<asd >')
_assert_('<' + cast(cast(@w as varchar(4)) as char(5)) + '>' = '<asd  >')

_assert_('<' + cast(cast(@x as varchar(4)) as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@x as varchar(3)) as char(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@x as varchar(2)) as char(3)) + '>' = '<as >')
_assert_('<' + cast(cast(@x as varchar(4)) as char(4)) + '>' = '<asdf>')
_assert_('<' + cast(cast(@x as varchar(4)) as char(5)) + '>' = '<asdf >')
_assert_('<' + cast(cast(@x as varchar(5)) as char(5)) + '>' = '<asdf >')


-- V-V-V
_assert_('<' + cast(cast(@v as varchar(4)) as varchar(3)) + '>' = '<as>')
_assert_('<' + cast(cast(@v as varchar(3)) as varchar(3)) + '>' = '<as>')
_assert_('<' + cast(cast(@v as varchar(2)) as varchar(3)) + '>' = '<as>')
_assert_('<' + cast(cast(@v as varchar(4)) as varchar(4)) + '>' = '<as>')
_assert_('<' + cast(cast(@v as varchar(4)) as varchar(5)) + '>' = '<as>')

_assert_('<' + cast(cast(@w as varchar(4)) as varchar(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@w as varchar(3)) as varchar(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@w as varchar(2)) as varchar(3)) + '>' = '<as>')
_assert_('<' + cast(cast(@w as varchar(4)) as varchar(4)) + '>' = '<asd>')
_assert_('<' + cast(cast(@w as varchar(4)) as varchar(5)) + '>' = '<asd>')

_assert_('<' + cast(cast(@x as varchar(4)) as varchar(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@x as varchar(3)) as varchar(3)) + '>' = '<asd>')
_assert_('<' + cast(cast(@x as varchar(2)) as varchar(3)) + '>' = '<as>')
_assert_('<' + cast(cast(@x as varchar(4)) as varchar(4)) + '>' = '<asdf>')
_assert_('<' + cast(cast(@x as varchar(4)) as varchar(5)) + '>' = '<asdf>')


-- ========== VARCHAR add ============

-- literal string constant normalized to NFC

declare @ax varchar(10) = e'\u0041\u030A'  -- LATIN CAPITAL LETTER A and COMBINING RING ABOVE (non NFC) --> LATIN CAPITAL LETTER A WITH RING ABOVE (NFC) 

_assert_('<' + @ax + '>' = '<' + e'\u00C5' + '>')  -- LATIN CAPITAL LETTER A WITH RING ABOVE
_assert_(len(@ax) = 1)
_assert_(unicode(@ax) = 0x00C5)


-- character reorganization when concatenating.

declare @cx varchar(10) = e'A\u0327'        -- LATIN CAPITAL LETTER A and COMBINING CEDILLA (NFC)
declare @dx varchar(10) = e'\u030A'         -- COMBINING RING ABOVE (NFC)
declare @ex varchar(10) = e'\u00C5\u0327'   -- LATIN CAPITAL LETTER A WITH RING ABOVE and COMBINING CEDILLA (NFC)

_assert_(unicode(@cx) = ascii('A'))
_assert_(unicode(substring(@cx, 2, 1)) = 0x0327)
_assert_(unicode(substring(@ex, 1, 1)) = 0x00C5)
_assert_(unicode(substring(@ex, 2, 1)) = 0x0327)
_assert_(len(@cx) = 2)
_assert_(len(@dx) = 1)
_assert_(len(@cx + @dx) = 2)
_assert_(len(@ex) = 2)
_assert_('<' + @cx + @dx + '>' = '<' + @ex + '>')


-- 1 UChar + 1 UChar = 3 UChar

declare @fx varchar(1) = e'\u1ebf'   -- LATIN SMALL LETTER E WITH CIRCUMFLEX AND ACUTE (NFC) (vietnamese letter)
declare @gx varchar(2) = e'\u0327'   -- COMBINING CEDILLA (NFC)
_assert_(unicode(@fx) = 0x1ebf)

_assert_(len(@fx)      = 1)
_assert_(len(@gx)      = 1)
_assert_(len(@fx + @gx) = 3)                 -- U+0229 U+0302 U+0301     LATIN SMALL LETTER E WITH CEDILLA, COMBINING CIRCUMFLEX ACCENT, COMBINING ACUTE ACCENT

_assert_(unicode(substring(@fx + @gx, 1, 1)) = 0x0229)
_assert_(unicode(substring(@fx + @gx, 2, 1)) = 0x0302)
_assert_(unicode(substring(@fx + @gx, 3, 1)) = 0x0301)


_assert_(@a + @b = 'HelloWorld')
_assert_(charlen(@a + @b) = 10)
_assert_(len(@a + @b) = 10)

_assert_(@a_fix + @b + 'x' = 'Hello     Worldx')
_assert_(charlen(@a_fix + @b) = 15)
_assert_(len(@a_fix + @b) = 15)

_assert_(@a + @b_fix + 'x' = 'HelloWorld     x')
_assert_(charlen(@a + @b_fix) = 15)
_assert_(len(@a + @b_fix) = 10)

_assert_(@a_fix + @b_fix + 'x' = 'Hello     World     x')
_assert_(charlen(@a_fix + @b_fix) = 20)
_assert_(len(@a_fix + @b_fix) = 15)

_assert_(@a_fix + @b_fix_empty + 'x' = 'Hello               x')
_assert_(charlen(@a_fix + @b_fix_empty) = 20)
_assert_(len(@a_fix + @b_fix_empty) = 5)

_assert_(@a_fix_empty + @b_fix + 'x' = '          World     x')
_assert_(charlen(@a_fix_empty + @b_fix) = 20)
_assert_(len(@a_fix_empty + @b_fix) = 15)

_assert_(@a_fix_empty + @b_fix_empty + 'x' = '                    x')
_assert_(charlen(@a_fix_empty + @b_fix_empty) = 20)
_assert_(len(@a_fix_empty + @b_fix_empty) = 0)


_assert_(@a_fix_empty + @b + 'x' = '          Worldx')
_assert_(charlen(@a_fix_empty + @b) = 15)
_assert_(len(@a_fix_empty + @b) = 15)

_assert_(@a + @b_fix_empty + 'x' = 'Hello          x')
_assert_(charlen(@a + @b_fix_empty) = 15)
_assert_(len(@a + @b_fix_empty) = 5)


_assert_(cast('' as char(10)) + cast('' as char(10)) + 'x' = '                    x')

_assert_(cast('     ' as char(10)) + cast('  ' as char(10)) + 'x' = '                    x')

_assert_(@a + @b = 'HelloWorld ')

_assert_(@a + @b = 'HelloWorld    ')

_assert_('hello' + 'WORLD' = 'helloWORLD')

_assert_('' + '' = '')

_assert_('' + '' = ' ')

_assert_('' + '' = '   ')

_assert_(' ' + ' ' = '  ')

_assert_(' ' + ' ' = '')

_assert_('hello' + ' ' = 'hello ')

_assert_('hello' + ' ' = 'hello')

_assert_(replicate('a', 4000) + replicate('a', 1000) + replicate('a', 3000) = replicate('a', 8000))

_assert_('asdf' + NULL is null)

_assert_(NULL + 'adsf' is null)


-- ========== VARCHAR add overflow ============

_assert_error_(replicate('a', 4000) + replicate('a', 1000) + replicate('a', 3001), 'DATA_VARCHAR_OVERFLOW')


-- ========== VARCHAR comp op ============

_assert_('äpple' < 'rönnbär' collate fr_cs_as)
_assert_('äpple' < 'rönnbär' collate FRENCH_CS_AS)
_assert_('äpple' > 'rönnbär' collate swedish_cs_as)

_assert_(('abcé') collate en_cs_as  = 'abcé' collate en_cs_as)
_assert_(('abcé') collate en_cs_as != 'abce' collate en_cs_as)
_assert_(('abcé') collate en_cs_as != 'ABCÉ' collate en_cs_as)

_assert_(('abcé') collate en_ci_as  = 'ABCÉ' collate en_ci_as)
_assert_(('abcé') collate en_ci_as != 'abce' collate en_ci_as)
_assert_(('abcé') collate en_ci_as != 'abce' collate en_ci_as)

_assert_(('abcé') collate en_ci_ai  = 'ABCÉ' collate en_ci_ai)
_assert_(('abcé') collate en_ci_ai  = 'ABCE' collate en_ci_ai)
_assert_(('abcé') collate en_ci_ai  = 'abce' collate en_ci_ai)

_assert_(cast('HellO    ' as varchar(20)) = cast('hello  ' collate en_ci_ai as varchar(55)))
_assert_(cast('HellO    ' as char(20)) = cast('hello  ' as varchar(55)) collate en_ci_ai )
_assert_(cast('HellO    ' as char(20)) collate en_ci_ai = cast('hello  ' as char(55)))
_assert_(cast('HellO    '  collate en_ci_ai as char(20)) = cast('hello  ' as char(55)))

_assert_('hello' = 'hello')
_assert_('hello' = 'hello ')
_assert_('hello' = 'hello  ')
_assert_('hello ' = 'hello')
_assert_('hello   ' = 'hello')
_assert_('hello     ' = 'hello  ')
_assert_('hello     ' collate en_ci_ai = 'HELLO  ')
_assert_(not 'hello' = 'world')
_assert_null_( null = 'a')
_assert_null_( 'a' = null)
_assert_null_( cast(null as varchar) = null)

_assert_('helloa' > 'hello')
_assert_('helloa' > 'hello ')
_assert_('helloa' > 'hello  ')
_assert_('helloa ' > 'hello')
_assert_('helloa   ' > 'hello')
_assert_('helloa     ' > 'hello  ')
_assert_('helloa     ' > 'HELLO  ')
_assert_('hello     ' > 'aaa  ')
_assert_('hello     ' > 'AAA  ')
_assert_(not 'hello' > 'world')
_assert_(not 'hello' > 'WORLD')
_assert_null_( null > 'a')
_assert_null_( 'a' > null)
_assert_null_( cast(null as varchar) > null)

_assert_('hello' < 'helloa')
_assert_('hello' < 'helloa ')
_assert_('hello' < 'helloa  ')
_assert_('hello ' < 'helloa')
_assert_('hello   ' < 'helloa')
_assert_('hello     ' < 'helloa  ')
_assert_('hello     ' < 'HELLOa  ')
_assert_('hello' < 'world')
_assert_('hello' < 'WORLD')
_assert_(not 'hello' < 'aworld')
_assert_(not 'hello' < 'AWORLD')
_assert_null_( null < 'a')
_assert_null_( 'a' < null)
_assert_null_( cast(null as varchar) < null)

_assert_('hello' >= 'hello')
_assert_('hello' >= 'hello ')
_assert_('hello' >= 'hello  ')
_assert_('hello ' >= 'hello')
_assert_('hello   ' >= 'hello')
_assert_('hello     ' >= 'hello  ')
_assert_('hello     ' collate en_ci_ai >= 'HELLO  ')
_assert_('hello     ' >= 'aaa  ')
_assert_('hello     ' >= 'AAA  ')
_assert_(not 'hello' >= 'world')
_assert_(not 'hello' >= 'WORLD')
_assert_null_( null >= 'a')
_assert_null_( 'a' >= null)
_assert_null_( cast(null as varchar) >= null)

_assert_('hello' <= 'hello')
_assert_('hello' <= 'hello ')
_assert_('hello' <= 'helloa  ')
_assert_('hello ' <= 'hello')
_assert_('hello   ' <= 'hello')
_assert_('hello     ' <= 'hello  ')
_assert_('hello     ' collate en_ci_ai <= 'HELLO  ')
_assert_('hello' <= 'world')
_assert_('hello' <= 'WORLD')
_assert_(not 'hello' <= 'aworld')
_assert_(not 'hello' <= 'AWORLD')
_assert_null_( null <= 'a')
_assert_null_( 'a' <= null)
_assert_null_( cast(null as varchar) <= null)

_assert_('hello' != 'helloa')
_assert_('hello' != 'helloa ')
_assert_('hello' != 'helloa  ')
_assert_('hello ' != 'helloa')
_assert_('hello   ' != 'helloa')
_assert_('hello     ' != 'helloa  ')
_assert_('hello     ' != 'HELLOa  ')
_assert_(not 'hello' collate en_ci_ai != 'HELLO  ')
_assert_null_( null != 'a')
_assert_null_( 'a' != null)
_assert_null_( cast(null as varchar) != null)

_assert_(@t_s3 collate fr_cs_as != 'hello')
_assert_(not @t_s3 collate fr_cs_as = 'hello')
_assert_(@t_s3 collate fr_cs_as = 'HeLLo')
_assert_('hello' != @t_s3 collate fr_cs_as)
_assert_(not 'hello' = @t_s3 collate fr_cs_as)
_assert_('HeLLo' = @t_s3 collate fr_cs_as)

_assert_(@t_s3 collate fr_cs_as != @u_s3 collate fr_cs_as)
_assert_(not @t_s3 collate fr_cs_as = @u_s3 collate fr_cs_as)
_assert_(lower(@t_s3 collate fr_cs_as) = lower(@u_s3 collate fr_cs_as))
_assert_(@u_s3 collate fr_cs_as != @t_s3 collate fr_cs_as)
_assert_(not @u_s3 collate fr_cs_as = @t_s3 collate fr_cs_as)

_assert_(@t_s3 collate fr_cs_as != @w_es_s3 collate fr_cs_as)
_assert_(not @t_s3 collate fr_cs_as = @w_es_s3 collate fr_cs_as)


-- ========== VARCHAR is [not] null ============

_assert_(cast(NULL as varchar) is null)

_assert_(not 'asdf' is null)


_assert_(not cast(NULL as varchar) is not null)

_assert_('asdf' is not null)


-- ========== VARCHAR [not] in list ============

_assert_('hello' in ('hello  '))

_assert_('hello' in ('world', 'hello '))

_assert_('hello' in ('world', 'hello', 'asdf'))

_assert_('hello  ' in ('world', NULL, 'hello', 'asdf'))

_assert_('hello' collate fr_ci_ai in ('world', 'HEllo', NULL, 'asdf'))

_assert_(@t_s3 collate fr_cs_as  in ('world', 'HeLLo', NULL, 'asdf'))
_assert_(not @t_s3 collate fr_cs_as  in ('world', 'hello', 'asdf'))
_assert_null_(@t_s3 collate fr_cs_as  in ('world', 'hello', NULL, 'asdf'))


_assert_(not 'hello' in ('world', 'uztztut', 'asdf'))

_assert_null_('hello' in ('world', 'uztztut', NULL, 'asdf'))


_assert_(not 'hello' not in ('hello'))

_assert_(not 'hello' not in ('world', 'hello'))

_assert_(not 'hello' not in ('world', 'hello', 'asdf'))

_assert_(not 'hello' not in ('world', NULL, 'hello', 'asdf'))

_assert_(not 'hello' not in ('world', 'hello', NULL, 'asdf'))


_assert_('hello' not in ('world', 'uztztut', 'asdf'))

_assert_null_('hello' not in ('world', 'uztztut', NULL, 'asdf'))

_assert_(@t_s3 collate fr_cs_as  not in ('world', 'hello', 'asdf'))
_assert_null_(@t_s3 collate fr_cs_as  not in ('world', 'hello', NULL, 'asdf'))


-- ========== VARCHAR like ============

_assert_null_('hello' like null)
_assert_null_( null like 'hello')

_assert_('' like '')
_assert_('   ' like '')
_assert_(not 'asdf' like '')
_assert_('asdf' not like '')

_assert_('hello' like 'hello')
_assert_('hello ' like 'hello')
_assert_('hello    ' like 'hello')

_assert_('hello' like 'hello ')

_assert_('hello'  collate FRENCH_ci_ai like 'HELLo')
_assert_('hello'  collate FR_CI_AI like 'HELLo')
_assert_('hello'  collate fr_ci_ai like 'HELLo')
_assert_('hello'  like 'HELLo'  collate fr_ci_ai)
_assert_('hello' not like 'HELLo' collate fr_cs_as)
_assert_(not 'hello' collate fr_cs_as like 'HELLo')

_assert_(@t_s3 collate fr_cs_as like 'HeLLo')
_assert_(not @t_s3 collate fr_cs_as like 'hellO')
_assert_(not @t_s3 collate fr_cs_as like @u_s3 collate fr_cs_as)
_assert_(lower(@t_s3 collate fr_cs_as) like lower(@u_s3 collate fr_cs_as))
_assert_(@t_s3 collate fr_cs_as not like @w_es_s3 collate fr_cs_as)

_assert_(not 'hello' not like 'hello')
_assert_('hello' not like 'asdf')
_assert_(not not 'hello' like '%LLO%')

_assert_('hello' like 'he%')
_assert_('hello' collate fr_ci_ai like 'HE%')
_assert_('hello' like 'HELLO%'  collate fr_ci_ai)
_assert_('hello' like '%LLO%'  collate fr_ci_ai)
_assert_('hello'  collate fr_ci_ai like '%L_O%')
_assert_('hello' like '%L[luzerejgasaa]O%'  collate fr_ci_ai)
_assert_(not 'hello'  collate fr_ci_ai like '%L[^uzerej[]gasaa]O%')
_assert_('[c]' like '[ab[]c]')
_assert_('5%' like '5[%]')
_assert_('_n' like '[_]n')
_assert_(not 'an' like '[_]n')
_assert_('a' like '[a-cdf]')
_assert_('a' like '[a - c d f]')
_assert_('abce' like 'abc[d-f]')
_assert_(not 'abce' like 'abc[d - f]')
_assert_('b' like '[a-cdf]')
_assert_('c' like '[a-cdf]')
_assert_('D'  collate fr_ci_ai like '[a-cdf]')
_assert_('F'  collate fr_ci_ai like '[a-cdf]')
_assert_(not'g' like '[a-cdf]')
_assert_('a' like '[-a-c]')
_assert_('b' like '[-a-c]')
_assert_('c' like '[-a-c]')
_assert_('-' like '[-a-c]')
_assert_(not 'd' like '[-a-c]')
_assert_('[' like '[[]')
_assert_(']' like ']')
_assert_('abc_d' like 'abc[_]d%')
_assert_('abc_de' like 'abc[_]d%')
_assert_(not 'abcud' like 'abc[_]d%')
_assert_('abcd' like 'abc[def]')
_assert_('abcF'  collate fr_ci_ai like 'abc[dEf]')
_assert_(not 'abcg' like 'abc[def]')
_assert_('abce' like 'abc[^-d]')
_assert_(not 'abc-' like 'abc[^-d]')
_assert_('abc-' like 'abc[-d]')
_assert_('abc-' like 'abc[d-]')
_assert_('.*{''}' like '.*{''}')
_assert_('*?+()$.{}^\|' like '*?+()$.{}^\|')

_assert_(@t_s3 collate fr_cs_as like 'He%')
_assert_(@t_s3 collate fr_cs_as not like 'he%')

_assert_(not @t_s3 collate fr_cs_as like @u_s3 collate fr_cs_as)

_assert_(@t_s3 collate fr_ci_ai like @w_es_s3)


declare @c_a char(10) = 'hello '

_assert_(@c_a like 'hello')
_assert_(@c_a like 'hello ')


-- ========== VARCHAR cast ============

declare @aa     varchar(30) = 'varA    '
declare @bb     varchar(30) = 'varB    '
declare @fix_aa    char(10) = 'fixA    '
declare @fix_bb    char(10) = 'fixB    '
declare @fix_cc    char(10) = '        '

_assert_('<' + @aa + @bb + '>'         = '<varA    varB    >')
_assert_('<' + @aa + @fix_bb + '>'     = '<varA    fixB      >')
_assert_('<' + @fix_aa + @bb + '>'     = '<fixA      varB    >')
_assert_('<' + @fix_aa + @fix_bb + '>' = '<fixA      fixB      >')
_assert_('<' + @fix_aa + @fix_cc + '>' = '<fixA                >')

_assert_(charlen(@aa + @bb)         = 16)
_assert_(charlen(@aa + @fix_bb)     = 18)
_assert_(charlen(@fix_aa + @bb)     = 18)
_assert_(charlen(@fix_aa + @fix_bb) = 20)
_assert_(len(@fix_aa + @fix_bb) = 14)
_assert_(charlen(@fix_aa + @fix_cc) =  20)
_assert_(len(@fix_aa + @fix_cc) =  4)

_assert_(charlen(cast(@aa + @bb as char(100)))         = 100)
_assert_(charlen(cast(@aa + @fix_bb as char(100)))     = 100)
_assert_(charlen(cast(@fix_aa + @bb as char(100)))     = 100)
_assert_(charlen(cast(@fix_aa + @fix_bb as char(100))) = 100)

_assert_(len(cast(@aa + @bb as char(100)))         = 12)
_assert_(len(cast(@aa + @fix_bb as char(100)))     = 12)
_assert_(len(cast(@fix_aa + @bb as char(100)))     = 14)
_assert_(len(cast(@fix_aa + @fix_bb as char(100))) = 14)

_assert_('<' + cast(@aa as varchar(20)) + '>'     = '<varA    >')
_assert_('<' + cast(@fix_aa as varchar(20)) + '>' = '<fixA      >')
_assert_('<' + cast(@aa as char(20)) + '>'        = '<varA                >')
_assert_('<' + cast(@fix_aa as char(20)) + '>'    = '<fixA                >')

_assert_(charlen(cast(@aa as varchar(20)))     = 8)    -- plain copy  
_assert_(charlen(cast(@fix_aa as varchar(20))) = 10)   -- precision of @fix_aa
_assert_(charlen(cast(@aa as char(20)))        = 20)
_assert_(charlen(cast(@fix_aa as char(20)))    = 20)

_assert_(len(cast(@aa as char(20)))        = 4)
_assert_(len(cast(@fix_aa as char(20)))    = 4)

-- ========== VARCHAR cast ============

_assert_(cast('' as varchar(3)) + 'x' = 'x')
_assert_(cast('' as varchar(1)) + 'x' = 'x')
_assert_(cast('' as varchar(10)) + 'x' = 'x')
_assert_(cast('' as char(10)) + 'x' = '          x')

_assert_(cast('hello' as varchar(3)) + 'x' = 'helx')
_assert_(cast('hello' as varchar(1)) + 'x' = 'hx')
_assert_(cast('hello' as varchar(10)) + 'x' = 'hellox')
_assert_(cast('hello' as char(3)) + 'x' = 'helx')
_assert_(cast('hello' as char(5)) + 'x' = 'hellox')
_assert_(cast('hello' as char(8)) + 'x' = 'hello   x')
_assert_(cast('he' as char(8)) + 'x' = 'he      x')
_assert_(cast('he   ' as char(8)) + 'x' = 'he      x')
_assert_(cast('' as char(8)) + 'x' = '        x')

_assert_(cast(cast('hello' as varchar(5)) as varchar(5)) + 'x' = 'hellox')

_assert_('<' + cast(@a_fix as varchar(2)) + '>'   = '<He>')
_assert_('<' + cast(@a_fix as varchar(5)) + '>'   = '<Hello>')
_assert_('<' + cast(@a_fix as varchar(6)) + '>'   = '<Hello >')
_assert_('<' + cast(@a_fix as varchar(10)) + '>'  = '<Hello     >')
_assert_('<' + cast(@a_fix as varchar(11)) + '>'  = '<Hello     >')
_assert_('<' + cast(@a_fix as varchar(100)) + '>' = '<Hello     >')


_assert_(cast(cast(NULL as varchar(2)) as varchar) is null)


_assert_(cast('' as bit) = 0)
_assert_(cast(' ' as bit) = 0)
_assert_(cast('   ' as bit) = 0)

_assert_(cast('0' as bit) = 0)
_assert_(cast('00000' as bit) = 0)
_assert_(cast('00000 ' as bit) = 0)
_assert_(cast('1' as bit) = 1)
_assert_(cast('2345' as bit) = 1)
_assert_(cast('2345 ' as bit) = 1)
_assert_(cast('2345   ' as bit) = 1)

_assert_(cast('true' as bit) = 1)
_assert_(cast('true ' as bit) = 1)
_assert_(cast('true      ' as bit) = 1)
_assert_(cast('TrUe' as bit) = 1)
_assert_(cast('tRuE' as bit) = 1)

_assert_(cast('false' as bit) = 0)
_assert_(cast('false ' as bit) = 0)
_assert_(cast('false      ' as bit) = 0)
_assert_(cast('FaLsE' as bit) = 0)
_assert_(cast('fAlSe' as bit) = 0)

_assert_(cast('0' as bit) = 0)
_assert_(cast('0   ' as bit) = 0)
_assert_(cast('1 ' as bit) = 1)
_assert_(cast('0001000   ' as bit) = 1)

_assert_(cast(cast(NULL as varchar) as bit) is null)


_assert_(cast('' as tinyint) = 0)
_assert_(cast(' ' as tinyint) = 0)
_assert_(cast('   ' as tinyint) = 0)
_assert_(cast('0' as tinyint) = 0)
_assert_(cast('0000000' as tinyint) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000000' as tinyint) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000008' as tinyint) = 8)
_assert_(cast('150' as tinyint) = 150)
_assert_(cast('255' as tinyint) = 255)

_assert_(cast(cast(NULL as varchar) as tinyint) is null)


_assert_(cast('' as smallint) = 0)
_assert_(cast(' ' as smallint) = 0)
_assert_(cast('   ' as smallint) = 0)
_assert_(cast('0' as smallint) = 0)
_assert_(cast('0000000' as smallint) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000000' as smallint) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000008' as smallint) = 8)
_assert_(cast('  150   ' as smallint) = 150)
_assert_(cast('255' as smallint) = 255)
_assert_(cast(' -255' as smallint) = -255)
_assert_(cast(' -255  ' as smallint) = -255)
_assert_(cast('-32768' as smallint) = -32768)
_assert_(cast('32767' as smallint) = 32767)

_assert_(cast(cast(NULL as varchar) as smallint) is null)


_assert_(cast('' as int) = 0)
_assert_(cast(' ' as int) = 0)
_assert_(cast('   ' as int) = 0)
_assert_(cast('0' as int) = 0)
_assert_(cast('0000000' as int) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000000' as int) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000008' as int) = 8)
_assert_(cast('  150   ' as int) = 150)
_assert_(cast('255' as int) = 255)
_assert_(cast(' -255' as int) = -255)
_assert_(cast(' -255  ' as int) = -255)
_assert_(cast('-32768' as int) = -32768)
_assert_(cast('32767' as int) = 32767)
_assert_(cast('-2147483648' as int) = @INT_MIN)
_assert_(cast('    +2147483647    ' as int) = @INT_MAX)
_assert_(cast('    +12345678    ' as int) = 12345678)

_assert_(cast(cast(NULL as varchar) as int) is null)


_assert_(cast('' as bigint) = 0)
_assert_(cast(' ' as bigint) = 0)
_assert_(cast('   ' as bigint) = 0)
_assert_(cast('0' as bigint) = 0)
_assert_(cast('0000000' as bigint) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000000' as bigint) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000008' as bigint) = 8)
_assert_(cast('  150   ' as bigint) = 150)
_assert_(cast('255' as bigint) = 255)
_assert_(cast(' -255' as bigint) = -255)
_assert_(cast(' -255  ' as bigint) = -255)
_assert_(cast('-32768' as bigint) = -32768)
_assert_(cast('32767' as bigint) = 32767)
_assert_(cast('-9223372036854775808' as bigint) = @BIGINT_MIN)
_assert_(cast('    +9223372036854775807    ' as bigint) = @BIGINT_MAX)
_assert_(cast('    +123456789012345678    ' as bigint) = 123456789012345678)

_assert_(cast(cast(NULL as varchar) as bigint) is null)


_assert_(cast('' as numeric(12,2)) = 0)
_assert_(cast(' ' as numeric(12,2)) = 0)
_assert_(cast('   ' as numeric(12,2)) = 0)
_assert_(cast('0' as numeric(12,2)) = 0)
_assert_(cast('0000000' as numeric(12,2)) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000000' as numeric(12,2)) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000008.25' as numeric(12,2)) = 8.25)
_assert_(cast('  150   ' as numeric(12,2)) = 150)
_assert_(cast('255.558' as numeric(12,2)) = 255.56)
_assert_(cast(' -255' as numeric(12,2)) = -255)
_assert_(cast(' -255.6588  ' as numeric(12,2)) = -255.66)
_assert_(cast(' -255.6588  ' as numeric(12)) = -256)
_assert_(cast('-32768' as numeric(12,2)) = -32768)
_assert_(cast('32767' as numeric(12,2)) = 32767)
_assert_(cast('-9223372036854775808' as numeric(21,2)) = @BIGINT_MIN)
_assert_(cast('    +9223372036854775807    ' as numeric(21,2)) = @BIGINT_MAX)
_assert_(cast('    +123456789012345678    ' as numeric(21,2)) = 123456789012345678)
_assert_(cast('    +922337203685477580700    ' as numeric(23,2)) = 922337203685477580700.00)
_assert_(cast('    +9223372036854775807e2    ' as numeric(23,2)) = 922337203685477580700.00)
_assert_(cast('26e-4' as numeric(12,4)) = 0.0026)
_assert_(cast('26e-4' as numeric(12,3)) = 0.003)
_assert_(cast('26e-4' as numeric(12,2)) = 0.00)
_assert_(cast('26e-4' as numeric(12,0)) = 0)

_assert_(cast('0.9e34' as numeric(34)) = 9000000000000000000000000000000000)

_assert_(cast('9e33' as numeric(34))   = 9000000000000000000000000000000000)
_assert_(cast('1234567890123456789012345678901234' as numeric(34)) = 1234567890123456789012345678901234)

_assert_(cast(cast('0.01234e5' as numeric(5)) as varchar)   = '1234')
_assert_(cast(cast('0.01234e5' as numeric(7,2)) as varchar) = '1234.00')
_assert_(cast('0.01234e10' as numeric(9))  = 123400000)
_assert_(cast('0.01234e34' as numeric(33)) = 123400000000000000000000000000000)
_assert_(cast('-0.01234e35' as numeric(34)) = -1234000000000000000000000000000000)
_assert_(cast('0.1234e34' as numeric(34))  = 1234000000000000000000000000000000)

_assert_(cast('0.0000001' as numeric(7,7)) = 1e-7d)
_assert_(cast('0.00000010' as numeric(8,8)) = 1.0e-7d)
_assert_(cast('0.00000019' as numeric(7,7)) = 2e-7d)
_assert_(cast('0.00000019' as numeric(6,6)) = 0e-6d)
_assert_(cast('1e-35' as numeric(34,34))   = 0e-34d)
_assert_(cast('9e-35' as numeric(34,34))   = 1e-34d)

_assert_(cast('0.0000000000000000000000000000000001' as numeric(34,34))  = 1E-34d)
_assert_(cast('0.00000000000000000000000000000000009' as numeric(34,34)) = 1E-34d)
_assert_(cast('-0.00000000000000000000000000000000009' as numeric(34,34)) = -1E-34d)
_assert_(cast('0.00000000000000000000000000000000001' as numeric(34,34)) = 0E-34d)
_assert_(cast('0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001' as numeric(34,34)) = 0E-34d)
_assert_(cast('0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001' as numeric(34,34)) = 0d)

_assert_error_(cast('0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001' as numeric(34,34)) = 0, 'NUMERIC_INVALID_TEXT_TOO_LONG')
_assert_error_(cast('0.0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001' as numeric(34,34)) = 0, 'NUMERIC_INVALID_TEXT_TOO_LONG')

_assert_(cast(cast(NULL as varchar) as numeric(12,2)) is null)


_assert_(cast('' as float) = 0)
_assert_(cast(' ' as float) = 0)
_assert_(cast('   ' as float) = 0)
_assert_(cast('0' as float) = 0)
_assert_(cast('0000000' as float) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000000' as float) = 0)
_assert_(cast('00000000000000000000000000000000000000000000000000000000000000008' as float) = 8)
_assert_(cast('  150   ' as float) = 150)
_assert_(cast('255.5' as float) = 255.5f)
_assert_(cast(' -255' as float) = -255)
_assert_(cast(' -255  ' as float) = -255)
_assert_(cast('-32768' as float) = -32768)
_assert_(cast('32767' as float) = 32767)
_assert_(cast('-2147483648' as float) = @INT_MIN)
_assert_(cast('    +2147483647    ' as float) = @INT_MAX)
_assert_(cast('    +12345678    ' as float) = 12345678)
_assert_(cast('    +12345678e300    ' as float) = 12345678e300)
_assert_(cast('    -12345678e-300    ' as float) = -12345678e-300)
_assert_(abs(cast('2.1234567890123456789012345678901234567890123872382738787477777774444e-2' as float) - 0.0212345678901235) < 0.0000000000000001)

_assert_(cast(cast(NULL as varchar) as float) is null)


-- ========== VARCHAR bad char ============

_assert_error_(cast('-2345' as bit), 'VARCHAR_CAST_BIT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast(' 2345' as bit), 'VARCHAR_CAST_BIT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast('truee ' as bit), 'VARCHAR_CAST_BIT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast('a2345' as bit), 'VARCHAR_CAST_BIT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast(' abc' as bit), 'VARCHAR_CAST_BIT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast('abc' as tinyint), 'VARCHAR_CAST_TINYINT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast(' abc' as tinyint), 'VARCHAR_CAST_TINYINT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('2147483648' as tinyint), 'VARCHAR_CAST_TINYINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('2147483647' as tinyint), 'VARCHAR_CAST_TINYINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-2147483649' as tinyint), 'VARCHAR_CAST_TINYINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-2147483648' as tinyint), 'VARCHAR_CAST_TINYINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-1' as tinyint), 'VARCHAR_CAST_TINYINT_OVERFLOW')


-- ========== VARCHAR bad char ============

_assert_error_(cast('abc' as smallint), 'VARCHAR_CAST_SMALLINT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast(' abc' as smallint), 'VARCHAR_CAST_SMALLINT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('2147483648' as smallint), 'VARCHAR_CAST_SMALLINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('2147483647' as smallint), 'VARCHAR_CAST_SMALLINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' as smallint), 'VARCHAR_CAST_SMALLINT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-2147483649' as smallint), 'VARCHAR_CAST_SMALLINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-2147483648' as smallint), 'VARCHAR_CAST_SMALLINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('32768' as smallint), 'VARCHAR_CAST_SMALLINT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-32769' as smallint), 'VARCHAR_CAST_SMALLINT_OVERFLOW')


-- ========== VARCHAR bad char ============

_assert_error_(cast('123.4' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast('123 abc' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast('123e2' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast('abc' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast(' abc' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('9223372036854775808' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('9223372036854775807' as int), 'VARCHAR_CAST_INT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-9223372036854775809' as int), 'VARCHAR_CAST_INT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-2147483649' as int), 'VARCHAR_CAST_INT_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('2147483648' as int), 'VARCHAR_CAST_INT_OVERFLOW')


-- ========== VARCHAR bad char ============

_assert_error_(cast('abc' as bigint), 'VARCHAR_CAST_BIGINT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast(' abc' as bigint), 'VARCHAR_CAST_BIGINT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('9223372036854775808' as bigint), 'VARCHAR_CAST_BIGINT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' as bigint), 'VARCHAR_CAST_BIGINT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-9223372036854775809' as bigint), 'VARCHAR_CAST_BIGINT_BAD_STRING')


-- ========== VARCHAR bad char ============

_assert_error_(cast('abc' as numeric), 'NUMERIC_DEC_CONVERSION_SYNTAX')


-- ========== VARCHAR overflow ============

_assert_error_(cast('9223372036854775808' as numeric(12,2)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('123' as numeric(3,2)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' as numeric(12,2)), 'NUMERIC_INVALID_TEXT_TOO_LONG')


-- ========== VARCHAR overflow ============

_assert_error_(cast('0.9e34d' as numeric(12,2)), 'NUMERIC_DEC_CONVERSION_SYNTAX')


-- ========== VARCHAR overflow ============

_assert_error_(cast('0.9e34' as numeric(33)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('0.9e34' as numeric(12,2)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('123.' as numeric(2)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-123.0' as numeric(2)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-123.9' as numeric(2)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('9e34' as numeric(34)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('12345678901234567890123456789012345' as numeric(34)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('0.01234e36' as numeric(34)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('0.1234e35' as numeric(34)), 'DATA_NUMERIC_OVERFLOW')


-- ========== VARCHAR overflow ============

_assert_error_(cast('1e6144' as numeric(34)), 'DATA_NUMERIC_OVERFLOW' ) -- error in decQuacQuantize()


-- ========== VARCHAR overflow ============

_assert_error_(cast('1e6145' as numeric(34)), 'DATA_NUMERIC_INFINITE')  -- error in decQuadFromString()


-- ========== VARCHAR overflow ============

_assert_error_(cast('inf' as numeric(3,2)), 'DATA_NUMERIC_INFINITE')


-- ========== VARCHAR overflow ============

_assert_error_(cast('nan' as numeric(3,2)), 'DATA_NUMERIC_NAN')


-- ========== VARCHAR bad char ============

_assert_error_(cast('abc' as float), 'VARCHAR_CAST_FLOAT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('9223372036854775808e400' as float), 'VARCHAR_CAST_FLOAT_BAD_STRING')


-- ========== VARCHAR overflow ============

_assert_error_(cast('inf' as float), 'SQLDATA_FLOAT_INFINITE')


-- ========== VARCHAR overflow ============

_assert_error_(cast('-inf' as float), 'SQLDATA_FLOAT_INFINITE')


-- ========== VARCHAR overflow ============

_assert_error_(cast('nan' as float), 'SQLDATA_FLOAT_NAN')


-- ========== VARCHAR case when ============

_assert_(case when 1=1 then 'one' end = 'one')
_assert_(case when 1=4 then 'one' end is null)
_assert_(case when 1=1 then 'one' when 1=2 then 'two' else 'otheraasdfasdfasdfasdfasdfasdfasdfafasdffffadsffffffffffasddddddddddddddddddssssssssssssssssaaaaaaaaaa' end = 'one')
_assert_(case when 1=1 then 'one' when 1=2 then 'two' end = 'one')
_assert_(case when 1=4 then 'one' when 2=2 then 'two' else 'other' end = 'two')
_assert_(case when 1=NULL then 'one' when 2=2 then 'two' else 'other' end = 'two')
_assert_(case when 1=NULL then 'one' when 2=NULL then 'two' else 'other' end = 'other')
_assert_(case when 1=NULL then 'one' when 2=NULL then 'two' end is null)

_assert_(case 20 when 202 then '7'  when 20 then 'vingt' end = 'vingt')

declare @a0 varchar(80) = 'NICO'
declare @a1 varchar(80)  = 'NICO'

_assert_(case when  'nico' =  'nico'     then 'nico c''est moi' when 33=330 then 'alex coucou' else 'blaaa' end = 'nico c''est moi')

declare @r varchar(80)
set @r   = case when  'nico'  collate fr_ci_ai =  case when 1=0                                                        then '0'
                                when 1=case 'asdf' when 'ere' then 567 when 'asdf' then 1 end   then @a0
                           end
           then @a0
           when 33=330
           then 'alex coucou'
           else 'blaaa'
      end
_assert_(@r = 'NICO')

set @r  = case when  'nico'  collate fr_cs_as =  case when 1=0                                                        then '0'
                                when 1=case 'asdf' when 'ere' then 567 when 'asdf' then 1 end   then @a1
                           end
           then @a0
           when 33=330
           then 'alex coucou'
           else 'blaaa'
      end
_assert_(@r = 'blaaa')



