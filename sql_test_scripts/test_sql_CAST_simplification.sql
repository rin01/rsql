
set language french



-- ========== CAST simplification ============


_assert_(cast(cast($999999999999999.9999 as numeric(19,4)) as bigint) = cast(999999999999999 as bigint))

_assert_(cast(cast($999999999999999.9999 as numeric(19,4)) as bigint) = 999999999999999)


_assert_(cast($999.9 as int)        = 1000)       -- round
_assert_(cast($999.9 as money)      = $999.9)     -- round
_assert_(cast($999.9 as numeric(4)) = 1000)       -- round
_assert_(cast($999.75 as float)     = 999.75e0)   -- round

_assert_(cast(999.9d as int)        =   999)      -- trunc
_assert_(cast(999.99999d as money)  = $1000)      -- round
_assert_(cast(999.9d as numeric(4)) =  1000)      -- round
_assert_(cast(999.75d as float)     = 999.75e0)   -- round

_assert_(cast(999.9f as int)        =   999)      -- trunc
_assert_(cast(999.99999f as money)  = $1000)      -- round
_assert_(cast(999.9f as numeric(4)) =  1000)      -- round
_assert_(cast(999.75f as float)     = 999.75e0)   -- round


_assert_(cast(cast(8 as numeric(10)) as numeric(6)) = 8)

_assert_(cast(cast(123 as int) as int)           = 123)
_assert_(cast(cast(123 as int) as smallint)      = 123)
_assert_(cast(cast(123 as int) as bigint)        = 123)
_assert_(cast(cast(123 as int) as money)         = 123)
_assert_(cast(cast(123 as int) as numeric(12))   = 123)
_assert_(cast(cast(123 as int) as numeric(12,2)) = 123)
_assert_(cast(cast(123 as int) as numeric(3))    = 123)
_assert_(cast(cast(123 as int) as numeric(4,1))  = 123)

_assert_(cast(cast(123 as money) as int)         = 123)
_assert_(cast(cast(123 as money) as money)       = 123)
_assert_(cast(cast(123 as money) as numeric(12,2)) = 123)
_assert_(cast(cast(123 as money) as numeric(3))  = 123)
_assert_(cast(cast(123 as money) as float)       = 123)

_assert_(cast(cast(123 as numeric(3)) as int)              = 123)
_assert_(cast(cast(123 as numeric(12)) as int)             = 123)
_assert_(cast(cast(123 as numeric(12)) as smallint)        = 123)
_assert_(cast(cast(123 as numeric(12)) as money)           = 123)
_assert_(cast(cast(123 as numeric(3)) as money)            = 123)
_assert_(cast(cast(123 as numeric(12,3)) as money)         = 123)
_assert_(cast(cast(123 as numeric(12,3)) as numeric(12,2)) = 123)
_assert_(cast(cast(123 as numeric(12,3)) as numeric(12,3)) = 123)
_assert_(cast(cast(123 as numeric(9)) as numeric(12,3))    = 123)
_assert_(cast(cast(123 as numeric(8)) as numeric(12,3))    = 123)
_assert_(cast(cast(123 as numeric(8)) as float)            = 123)
_assert_(cast(cast(123 as numeric(10)) as float)           = 123)
_assert_(cast(cast(123 as numeric(12,2)) as float)         = 123)

_assert_(cast(cast(123 as float) as int)          = 123)
_assert_(cast(cast(123 as float) as smallint)     = 123)
_assert_(cast(cast(123 as float) as money)        = 123)
_assert_(cast(cast(123 as float) as numeric(3))   = 123)
_assert_(cast(cast(123 as float) as float)        = 123)



_assert_(cast(cast($123.56 as int) as int)           = 124)
_assert_(cast(cast($123.56 as int) as smallint)      = 124)
_assert_(cast(cast($123.56 as int) as money)         = 124)
_assert_(cast(cast($123.56 as int) as numeric(3))    = 124)
_assert_(cast(cast($123.56 as int) as numeric(12,2)) = 124)
_assert_(cast(cast($123.56 as int) as numeric(12,2)) = 124)
_assert_(cast(cast($123.56 as int) as numeric(10))   = 124)
_assert_(cast(cast($123.56 as int) as numeric(12))   = 124)
_assert_(cast(cast($123.56 as int) as float)         = 124)

_assert_(abs(cast(cast($123.56 as money) as float)       -123.56e0) < 0.0001)


_assert_(cast(cast($123.56 as numeric(34,4)) as int) = 123)
_assert_(cast(cast($123.56 as numeric(10,4)) as int) = 123)
_assert_(cast(cast($123.56 as numeric(34)) as int)   = 124)



_assert_(cast(cast($123.56 as numeric(34)) as money)   = 124)
_assert_(cast(cast($123.56 as numeric(34,4)) as money) = 123.56)
_assert_(cast(cast($123.56 as numeric(34,2)) as money) = 123.56)


_assert_(cast(cast($123.56 as numeric(34,2)) as numeric(34,2)) = 123.56)
_assert_(cast(cast($123.56 as numeric(34,2)) as numeric(12,2)) = 123.56)
_assert_(cast(cast($123.56 as numeric(34,2)) as numeric(12,4)) = 123.56)

_assert_(abs(cast(cast($123.56 as numeric(34,2)) as float)  -123.56e0) < 0.0001)
_assert_(abs(cast(cast($123.56 as numeric(34,4)) as float)  -123.56e0) < 0.0001)
_assert_(cast(cast($123.56 as numeric(4)) as float) = 124)


_assert_(cast(cast($123.56 as float) as int) = 123)
_assert_(abs(cast(cast($123.56 as float) as money)  -123.56e0) < 0.0001)

_assert_(cast(cast($123.56 as float) as numeric(10))   = 124)
_assert_(cast(cast($123.56 as float) as numeric(12,2)) = 123.56)
_assert_(cast(cast($123.56 as float) as numeric(34,6)) = 123.56)

_assert_(abs(cast(cast($123.56 as float) as float) -123.56e0) < 0.0001)


_assert_(cast(cast(123.56e0 as int) as int)   = 123)
_assert_(cast(cast(123.56e0 as int) as money) = 123)
_assert_(cast(cast(123.56e0 as int) as numeric(12,2)) = 123)
_assert_(cast(cast(123.56e0 as int) as float) = 123)

_assert_(cast(cast(123.56e0 as money) as money)         = 123.56)
_assert_(cast(cast(123.56e0 as money) as numeric(34,4)) = 123.56)
_assert_(cast(cast(123.56e0 as money) as numeric(10,2)) = 123.56)
_assert_(abs(cast(cast(123.56e0 as money) as float) -123.56e0) < 0.0001)

_assert_(cast(cast(123.56e0 as numeric(12,2)) as int)   = 123)
_assert_(cast(cast(123.56e0 as numeric(12,2)) as money) = 123.56)
_assert_(cast(cast(123.56e0 as numeric(34,6)) as money) = 123.56)
_assert_(cast(cast(123.56e0 as numeric(12,2)) as numeric(6,3)) = 123.56)
_assert_(cast(cast(123.56e0 as numeric(12,2)) as numeric(6,2)) = 123.56)
_assert_(abs(cast(cast(123.56e0 as numeric(12,2)) as float) -123.56e0) < 0.0001)


_assert_(cast(cast(123.56e0 as float) as int)   = 123)
_assert_(cast(cast(123.56e0 as float) as money) = 123.56)

_assert_(abs(cast(cast(123.56e0 as float) as float) -123.56e0) < 0.0001)



_assert_(cast(cast(123.56 as int) as int)      = 123)
_assert_(cast(cast(123.56 as int) as smallint) = 123)
_assert_(cast(cast(123.56 as int) as bigint)   = 123)
_assert_(cast(cast(123.56 as int) as money)    = 123)
_assert_(cast(cast(123.56 as int) as numeric(12,2)) = 123)
_assert_(cast(cast(123.56 as int) as numeric(6)) =123)
_assert_(cast(123.56 as numeric(6))            = 124)
_assert_(cast(cast(123.56 as int) as float)    = 123)

_assert_(cast(cast(123.56 as money) as int) = 124)
_assert_(cast(cast(123.56 as money) as money) = 123.56)
_assert_(cast(cast(123.56 as money) as numeric(12,2))= 123.56)
_assert_(abs(cast(cast(123.56 as money) as float)  -123.56e0) < 0.0001)


_assert_(cast(cast(123.56 as numeric(12)) as int)   = 124)
_assert_(cast(cast(123.56 as numeric(12)) as money) = 124)
_assert_(cast(cast(123.56 as numeric(12,4)) as money) = 123.56)
_assert_(cast(cast(123.56 as numeric(12,4)) as numeric(12,3)) = 123.56)
_assert_(cast(cast(123.56 as numeric(12,1)) as numeric(12,5)) = 123.6)
_assert_(abs(cast(cast(123.56 as numeric(12,1)) as float) -123.60e0) < 0.0001)
_assert_(abs(cast(cast(123.56 as numeric(12,2)) as float) -123.56e0) < 0.0001)


_assert_(cast(cast(123.56 as float) as int)           = 123)
_assert_(cast(cast(123.56 as float) as money)         = 123.56)
_assert_(cast(cast(123.56 as float) as numeric(12,2)) = 123.56)
_assert_(cast(cast(123.56 as float) as numeric(5,2))  = 123.56)
_assert_(cast(cast(123.56 as float) as numeric(5,1))  = 123.6)
_assert_(abs(cast(cast(123.56 as float) as float) -123.56e0) < 0.0001)


_assert_(cast(cast(123d as int) as int)           = 123)
_assert_(cast(cast(123d as int) as smallint)      = 123)
_assert_(cast(cast(123d as int) as bigint)        = 123)
_assert_(cast(cast(123d as int) as money)         = 123)
_assert_(cast(cast(123d as int) as numeric(12))   = 123)
_assert_(cast(cast(123d as int) as numeric(12,2)) = 123)
_assert_(cast(cast(123d as int) as numeric(3))    = 123)
_assert_(cast(cast(123d as int) as numeric(4,1))  = 123)

declare @ddd numeric(12) = 123
_assert_(cast(cast(@ddd as int) as bigint)        = 123)
_assert_(cast(cast(cast(123d as numeric(12)) as int) as money) = 123)
_assert_(cast(cast(@ddd as int) as money) = 123)

_assert_(cast(cast(123d as money) as int)         = 123)
_assert_(cast(cast(123d as money) as money)       = 123)
_assert_(cast(cast(123d as money) as numeric(12,2)) = 123)
_assert_(cast(cast(123d as money) as numeric(3))  = 123)
_assert_(cast(cast(123d as money) as float)       = 123)

_assert_(cast(cast(123d as numeric(3)) as int)              = 123)
_assert_(cast(cast(123d as numeric(12)) as int)             = 123)
_assert_(cast(cast(123d as numeric(12)) as smallint)        = 123)
_assert_(cast(cast(123d as numeric(12)) as money)           = 123)
_assert_(cast(cast(123d as numeric(3)) as money)            = 123)
_assert_(cast(cast(123d as numeric(12,3)) as money)         = 123)
_assert_(cast(cast(123d as numeric(12,3)) as numeric(12,2)) = 123)
_assert_(cast(cast(123d as numeric(12,3)) as numeric(12,3)) = 123)
_assert_(cast(cast(123d as numeric(9)) as numeric(12,3))    = 123)
_assert_(cast(cast(123d as numeric(8)) as numeric(12,3))    = 123)
_assert_(cast(cast(123d as numeric(8)) as float)            = 123)
_assert_(cast(cast(123d as numeric(10)) as float)           = 123)
_assert_(cast(cast(123d as numeric(12,2)) as float)         = 123)

_assert_(cast(cast(123d as float) as int)          = 123)
_assert_(cast(cast(123d as float) as smallint)     = 123)
_assert_(cast(cast(123d as float) as money)        = 123)
_assert_(cast(cast(123d as float) as numeric(3))   = 123)
_assert_(cast(cast(123d as float) as float)        = 123)


_assert_(cast(cast($999999999999999.9999 as numeric(19,4)) as bigint) =  999999999999999)  -- no simplification because of money-numeric rounding difference

_assert_(cast(cast(999999999999999.9999d as money) as bigint)         = 1000000000000000)  -- no simplification because of money-numeric rounding difference


-- ========== CAST simplification, misc ============


declare @a int = 10

_assert_(case when 1=1 then case when 1=1 then case when 1=1 then case when 1=1 then case when 1=1 then case when 1=1 then case when 1=1 then case when 1=1 then case when 1=1 then 1234 end end end end end end end end end=1234)

_assert_((@a+2) between 5 and 15)

_assert_(cast(@a as int) between 5 and 15)

_assert_(case cast(@a as int) when 1 then 'one' when 10 then 'ten' end = 'ten')

_assert_(cast(@a as bigint) between 5f and 15 )

_assert_(cast(12d as int) between 10d and 20d )

_assert_(cast(cast(12d as int) as numeric(2)) = 12d )-- here, the cast numeric->big->numeric is simplified into 12d

_assert_(cast(cast(12d as int) as numeric(3)) = 12d ) -- here, the cast numeric->big->numeric is simplified into 12d->numeric(3)


_assert_(cast(cast('123' as varchar(3)) as int) = 123)


-- ========== CAST simplification, error ============

_assert_error_(cast(cast(2147483648d as bigint) as int), 'NUMERIC_CAST_INT_OVERFLOW')  -- simplified cast numeric->int







