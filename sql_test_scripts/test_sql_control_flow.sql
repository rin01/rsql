
-- this script checks that IF, WHILE, CONTINUE, are working correctly

declare @i int = 10
declare @k int
declare @tot_prime int = 0
declare @flag tinyint = 0
declare @limit int = 2000

ss:

if @flag = 0
  begin
  set @tot_prime += 536
  goto ee
  end

ss2:

while @i * (3 + @i - (@i+2)) < @limit
  begin
  if @i % 2 = 0
    begin
    if @i in (124, 200, 388)
      goto aa
    else
      goto bb

    cc:
    set @i += 1
    continue
    end

  set @k = 2
  while @k < (@i+2)-2
    begin
    if @i % @k = 0
      begin
      set @tot_prime += @k
      end
    set @k += 1
    end


  set @i += 1
  end

if @flag = 1
  begin
  set @i = 40
  set @flag = 4
  goto ss2
  end

goto the_end

aa:
set @tot_prime += @i % 31
if @tot_prime = 10904
  begin
  set @tot_prime += 3
  goto bb
  end
else
  begin
  set @tot_prime += 337
  goto cc
  end

bb:
set @tot_prime += @i % 17
goto cc

ee:
set @i = 10
set @flag = 1
goto ss2

the_end:
_assert_(@tot_prime = 483349)



