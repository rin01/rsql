

set language french

declare @NUM1_MAX numeric(34) =  9999999999999999999999999999999999
declare @NUM1_MIN numeric(34) =  -9999999999999999999999999999999999

declare @NUM2_MAX numeric(34) =  9999999999999999999999999999.999999
declare @NUM2_MIN numeric(34) =  -9999999999999999999999999999.999999

declare @NUM3_MAX numeric(34,34) =  0.9999999999999999999999999999999999
declare @NUM3_MIN numeric(34,34) =  -00.9999999999999999999999999999999999


declare @MONEY_MAX money =  999999999999999.9999
declare @MONEY_MIN money = -999999999999999.9999

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @BIGINT_MAX bigint =  9223372036854775807
declare @BIGINT_MIN bigint = -9223372036854775808


-- ========== NUMERIC miscellaneous ============


_assert_(123. + .0 = 123.)
_assert_(123. + 0. = 123.)
_assert_(123. + 0.0 = 123.)

_assert_(123. + .1 = 123.1)
_assert_(123. + 1. = 124.)
_assert_(123. + 0.1 = 123.1)
_assert_(123. + 1.1 = 124.1)

_assert_(-9999999999999999999999999999999999.1234561 = -9999999999999999999999999999999999d)


-- ========== NUMERIC unary minus ============

_assert_(++++87364.123456 = 87364.123456)
_assert_(-87364.1234 = -1 * 87364.1234)
_assert_(-87364.1234 = -+1 * 87364.1234)
_assert_(- -87364.1234 = 87364.1234)
_assert_(++- - -++87364.1234 = -87364.1234)
_assert_(++87364.1234 = 87364.1234)

_assert_(-cast(78.1234567890123456 as numeric(18,16)) = -78.123457)
_assert_(-78.1234567890123456 + 0                     = -78.123457)
_assert_(-((78.1234567890123456)) + 0                 = -78.123457)

_assert_(-cast(NULL as numeric) is null)


-- ========== NUMERIC add ============

_assert_(1000000000000.123456 + 150.000001 = 1000000000150.123457)

_assert_(-1000.123456 + -150 = -1150.123456)

_assert_(-9900.123 + -150.000456 = -10050.123456)

_assert_(999.999 + 999.999 = 1999.998)

_assert_(999.99 + 999.99  =  1999.98)

_assert_(1000.0 + null is null)
_assert_(null + 1000.0 is null)
_assert_(cast(null as numeric) + null is null)


-- ========== NUMERIC add max overflow ============

-- because result of '+' has a integer part of DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX digits.

_assert_error_(@NUM1_MAX + 0.00, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC add max overflow ============

_assert_(1234567890123456789012345678.0 + 8765432109876543210987654321.0 = 9999999999999999999999999999)

_assert_error_(1234567890123456789012345678.0 + 8765432109876543210987654322.0, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC subtract ============

_assert_(1000.0 - -150.1234 = 1150.1234)

_assert_(-1000.0 - 150.1234 = -1150.1234)

_assert_(-9900.123 - 150.000456 = -10050.123456)

_assert_(-999.999 - 999.999 = -1999.998)

_assert_(1000.0 - null is null)
_assert_(null - 1000.0 is null)
_assert_(cast(null as numeric) - null is null)


-- ========== NUMERIC subtract max overflow ============

-- because result of '-' has a integer part of DATATYPE_NUMERIC_RESULT_INTEGRALPART_MAX digits.

_assert_error_(@NUM1_MAX - 0.00, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC subtract max overflow ============

_assert_(1234567890123456789012345678.0 + 8765432109876543210987654321.0 = 9999999999999999999999999999)

_assert_error_(-1234567890123456789012345678.0 - 8765432109876543210987654322.0, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC multiply ============

_assert_(142566374.28767837126 * -87672670033.37676 = -12499174690778514568.285476)

_assert_(-142566374.28767837126 * -87672670033.37676 = 12499174690778514568.285476)

_assert_(999.99 * 999.99  =  999980.0001)

_assert_(@NUM1_MAX * .00 = 0)

_assert_(999.999 * 999.999 = 999998.000001)

_assert_(1000.0 * null is null)
_assert_(null * 1000.0 is null)
_assert_(cast(null as numeric) * null is null)


-- ========== NUMERIC multiply max overflow ============

_assert_error_(@NUM1_MAX * 1.00, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC multiply max overflow ============

_assert_error_(1234567890123456789012345678.0 * 9.0, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC divide ============

_assert_(142566374.28767837126 / -87672670033.37676 = -0.001626)

_assert_(-5567663747654.28767837126 / -87672670033.37676 = 63.505124)

_assert_(-1000. / 3. = -333.333333)

_assert_(-1000. / -3. = 333.333333)

_assert_(0. / -3. = 0.)

_assert_(-1000. / .00000000000000001 = -100000000000000000000.000000000000)

_assert_(-1000. / 3 * 3 = -999.999999)    -- for MS SQL Server, it is -999.999999

_assert_(999.999 / 0.001 = 999999.0000000)

_assert_(999.99 / 0.01    =  99999.000000000000)
_assert_(999.99 / 0.01    =  99999.000000000000)
_assert_(999.99 / 0.07    =  14285.571429)
_assert_(999.99 / 0.0000000000000000001  =  9999900000000000000000.000000000000)

_assert_(1000.0 / null is null)
_assert_(null / 1000.0 is null)
_assert_(cast(null as numeric) / null is null)


-- ========== NUMERIC divide max overflow ============

_assert_error_(-1000. / .0000000000000000000000001, 'NUMERIC_OVERFLOW')  -- integral part has more than 28 digits (has 29 digits)


-- ========== NUMERIC divide max overflow ============

_assert_error_(@NUM1_MAX / 1.00, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC divide max overflow ============

-- NUMERIC_ERROR instead of NUMERIC_OVERFLOW, because it is catched by decQuadQuantize()

_assert_error_(1234567890123456789012345678.0 / .1, 'NUMERIC_OVERFLOW')


-- ========== NUMERIC divide by zero ============

_assert_error_(1./0., 'NUMERIC_DEC_DIVISION_BY_ZERO')


-- ========== NUMERIC comp op ============

_assert_(122.7889 = 122.7889)

_assert_(not (122.7889 = 123.7889))

_assert_(not 122.7889 = 123.7889)

_assert_null_( 1000.7889 = null)
_assert_null_( null = 1000.7889)
_assert_null_( cast(null as numeric) = null)


_assert_(not (122.7889 != 122.7889))

_assert_(122.7889 != 123.7889)

_assert_null_( 1000.7889 != null)
_assert_null_( null != 1000.7889)
_assert_null_( cast(null as numeric) != null)


_assert_(123.7889 > 122.7889)

_assert_(not (122.7889 > 122.7889))

_assert_(-121.7889 > -122.7889)

_assert_(not (-122.7889 > -122.7889))

_assert_(1.7889 > -1.7889)

_assert_(not (-1.7889 > 1.7889))

_assert_null_( 1000.7889 > null)
_assert_null_( null > 1000.7889)
_assert_null_( cast(null as numeric) > null)


_assert_(123.7889 >= 122.7889)

_assert_(122.7889 >= 122.7889)

_assert_(not (121.7889 >= 122.7889))

_assert_(-121.7889 >= -122.7889)

_assert_(-122.7889 >= -122.7889)

_assert_(not (-123.7889 >= -122.7889))

_assert_(1.7889 >= -1.7889)

_assert_(not (-1.7889 >= 1.7889))

_assert_null_( 1000.7889 >= null)
_assert_null_( null >= 1000.7889)
_assert_null_( cast(null as numeric) >= null)


_assert_(121.7889 < 122.7889)

_assert_(not (122.7889 < 122.7889))

_assert_(-123.7889 < -122.7889)

_assert_(not (-122.7889 < -122.7889))

_assert_(not (1.7889 < -1.7889))

_assert_(-1.7889 < 1.7889)

_assert_null_( 1000.7889 < null)
_assert_null_( null < 1000.7889)
_assert_null_( cast(null as numeric) < null)


_assert_(121.7889 <= 122.7889)

_assert_(122.7889 <= 122.7889)

_assert_(not (123.7889 <= 122.7889))

_assert_(-123.7889 <= -122.7889)

_assert_(-122.7889 <= -122.7889)

_assert_(not (-121.7889 <= -122.7889))

_assert_(not (1.7889 <= -1.7889))

_assert_(-1.7889 <= 1.7889)

_assert_null_( 1000.7889 <= null)
_assert_null_( null <= 1000.7889)
_assert_null_( cast(null as numeric) <= null)


-- ========== NUMERIC is [not] null ============

_assert_(cast(NULL as numeric) is null)

_assert_(not 123.456789 is null)


_assert_(not cast(NULL as numeric) is not null)

_assert_(123.456789 is not null)


-- ========== NUMERIC [not] in list ============

_assert_(12.456789 in (12.456789))

_assert_(12.456789 in (0, 12.456789))

_assert_(12.456789 in (0, 12.456789, 9))

_assert_(12.456789 in (0, NULL, 12.456789, 9))

_assert_(12.456789 in (0, 12.456789, NULL, 9))


_assert_(not 12.456789 in (0, 1.87, 9))

_assert_null_(12.456789 in (0, 1, NULL, 9.5654))


_assert_(not 12.456789 not in (12.456789))

_assert_(not 12.456789 not in (0, 12.456789))

_assert_(not 12.456789 not in (0, 12.456789, 9))

_assert_(not 12.456789 not in (0, NULL, 12.456789, 9))

_assert_(not 12.456789 not in (0, 12.456789, NULL, 9))


_assert_(12.456789 not in (0, 1.765, 9))

_assert_null_(12.456789 not in (0, 1, NULL, 9))


-- ========== NUMERIC cast ============

_assert_(cast(123.45678 as char(15)) + 'x' = '123.45678      x')
_assert_(cast(123.45 as char) = '123.45')
_assert_(cast(123.5555 as char) + 'x' = '123.5555                      x')
_assert_(cast(123. as char(6)) + 'x' = '123   x')
_assert_(cast(-123.00 as char(7)) + 'x' = '-123.00x')
_assert_(cast(cast(null as numeric) as char) is null)

_assert_(cast(123.45678 as varchar(15)) + 'x' = '123.45678x')
_assert_(cast(123.45 as varchar) = '123.45')
_assert_(cast(123.5555 as varchar) + 'x' = '123.5555x')
_assert_(cast(123. as varchar(6)) + 'x' = '123x')
_assert_(cast(-123.00 as varchar(7)) + 'x' = '-123.00x')
_assert_(cast(cast(null as numeric) as varchar) is null)

_assert_(cast(0.9e34d as varchar(40)) = '9000000000000000000000000000000000')
_assert_(cast(9e33d as varchar(40))   = '9000000000000000000000000000000000')
_assert_(cast(1234567890123456789012345678901234d as varchar(40)) = '1234567890123456789012345678901234')

_assert_(cast(0.01234e5d as varchar(40))  = '1234')
_assert_(cast(0.01234e10d as varchar(40)) = '123400000')
_assert_(cast(0.01234e34d as varchar(40)) = '123400000000000000000000000000000')
_assert_(cast(0.01234e35d as varchar(40)) = '1234000000000000000000000000000000')
_assert_(cast(0.1234e34d as varchar(40))  = '1234000000000000000000000000000000')

_assert_(cast(0.0000001 as varchar(40))  = '0.0000001')
_assert_(cast(0.00000010 as varchar(40)) = '0.00000010')
_assert_(cast(1e-35d as varchar(40))     = '0.0000000000000000000000000000000000')
_assert_(cast(9e-35d as varchar(40))     = '0.0000000000000000000000000000000001')

_assert_(cast(0.0000000000000000000000000000000001 as varchar(40))  = '0.0000000000000000000000000000000001')
_assert_(cast(0.00000000000000000000000000000000009 as varchar(40)) = '0.0000000000000000000000000000000001')
_assert_(cast(0.00000000000000000000000000000000001 as varchar(40)) = '0.0000000000000000000000000000000000')
_assert_(cast(0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001 as varchar(40)) = '0.0000000000000000000000000000000000')

_assert_(cast(0.00000000000000000000000000000012345 as varchar(80)) = '0.0000000000000000000000000000001235')
_assert_(cast(0.12345e-3456d as varchar(80))                        = '0.0000000000000000000000000000000000')
_assert_(cast(0. as varchar(80))                                    = '0')


_assert_(cast(123. as bit) = 1)
_assert_(cast(123.45 as bit) = 1)
_assert_(cast(-123.45 as bit) = 1)
_assert_(cast(0.00 as bit) = 0)
_assert_(cast(0. as bit) = 0)
_assert_(cast(.00 as bit) = 0)
_assert_(cast(cast(null as numeric) as bit) is null)

_assert_(cast(255.999 as tinyint) = 255)
_assert_(cast(255.000000 as tinyint) = 255)
_assert_(cast(254.9 as tinyint) = 254)
_assert_(cast(0.0 as tinyint) = 0)
_assert_(cast(0.999 as tinyint) = 0)
_assert_(cast(-0.99999 as tinyint) = 0)
_assert_(cast(-0.9999999999999999999999999999999999 as tinyint) = 0)
_assert_(cast(cast(null as numeric) as tinyint) is null)


_assert_(cast(32767d as smallint) = 32767)
_assert_(cast(32767.99d as smallint) = 32767)
_assert_(cast(1000.9d as smallint) = 1000)
_assert_(cast(-1000.9d as smallint) = -1000)
_assert_(cast(-32768d as smallint) = -32768)
_assert_(cast(-32768.99d as smallint) = -32768)

_assert_(cast(32767.0 as smallint) = 32767)
_assert_(cast(32767.9 as smallint) = 32767)
_assert_(cast(-32768.0 as smallint) = -32768)
_assert_(cast(-32768.9 as smallint) = -32768)
_assert_(cast(cast(null as numeric) as smallint) is null)


_assert_(cast(2147483647.99d as int) = 2147483647)
_assert_(cast(2147483647d as int) = 2147483647)
_assert_(cast(123456d as int) = 123456)
_assert_(cast(10000.9d as int) = 10000)  -- truncate
_assert_(cast(-10000.9d as int) = -10000)
_assert_(cast(-123456d as int) = -123456)
_assert_(cast(-2147483648d as int) = -2147483648)
_assert_(cast(-2147483648.9999d as int) = -2147483648)
_assert_(cast(cast(null as numeric) as int) is null)

_assert_(cast(2147483647.9 as int) = @INT_MAX)
_assert_(cast(-2147483648.9 as int) = @INT_MIN)
_assert_(cast(cast(null as numeric) as int) is null)

_assert_(cast(9223372036854775807.9 as bigint) = @BIGINT_MAX)
_assert_(cast(-9223372036854775808.9 as bigint) = @BIGINT_MIN)
_assert_(cast(-9223372036854775808.0 as bigint) = @BIGINT_MIN)
_assert_(cast(cast(@MONEY_MAX as numeric(19,4)) as bigint) = cast( 999999999999999 as bigint))
_assert_(cast(cast(@MONEY_MIN as numeric(19,4)) as bigint) = cast(-999999999999999 as bigint))
_assert_(cast(cast(null as numeric) as bigint) is null)

_assert_(cast(9.99996 as money) = $+10)   -- round
_assert_(cast(-9.99996 as money) = $-10.)
_assert_(cast(999999999999999.9999 as money) = @MONEY_MAX)
_assert_(cast(-999999999999999.9999 as money) = @MONEY_MIN)
_assert_(cast(cast(null as numeric) as money) is null)

_assert_(cast(@MONEY_MAX as numeric(19,4)) = @MONEY_MAX)
_assert_(cast(@MONEY_MIN as numeric(19,4)) = @MONEY_MIN)
_assert_(cast(123.456 as numeric(12,2)) = 123.46)
_assert_(cast(123.456 as numeric(12,1)) = 123.5)
_assert_(cast(123.456 as numeric(12,0)) = 123.)
_assert_(cast(-123.456 as numeric(12)) = -123.)
_assert_(cast(cast(null as numeric) as numeric(12,2)) is null)

_assert_(cast(cast(@MONEY_MAX as numeric(34,4)) as numeric(18,2)) = 1000000000000000.00)
_assert_(cast(cast(@MONEY_MIN as numeric(34,4)) as numeric(18,2)) = -1000000000000000.00)
_assert_(cast(cast(null as numeric) as numeric(12,2)) is null)

_assert_(cast(12345.8 as numeric(5)) = 12346)
_assert_(cast(-12345.1 as numeric(5)) = -12345)
_assert_(cast(cast(12345.8 as numeric(9,4)) as varchar) = '12345.8000')
_assert_(cast(cast(null as numeric) as numeric(5)) is null)

_assert_(cast(cast(cast(@MONEY_MAX as numeric(34,4)) as float) as varchar(30)) = '1e+15')
_assert_(cast(cast(cast(@MONEY_MIN as numeric(34,4)) as float) as varchar(30)) = '-1e+15')
_assert_(cast(cast(-12345678901234567890. as float) as varchar(40)) = '-1.23457e+19')
_assert_(cast(cast(null as numeric) as float) is null)


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(123. as char(2)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-123.0 as char(3)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(123. as varchar(2)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-123.9 as varchar(3)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== NUMERIC cast ============

_assert_error_(cast(cast('0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001' as numeric(34,34)) as varchar(40)) = '0.000000000000000000000000000000000', '_NUMERIC_INVALID_TEXT_TOO_LONG')

-- ========== NUMERIC cast overflow ============

_assert_error_(cast(cast('9e34' as numeric(34,34))  as varchar(40)), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(cast('12345678901234567890123456789012345' as numeric(34,34)) as varchar(40)), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast('12345678901234567890123456789012345' as numeric(34)), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(cast('0.01234e36' as numeric(34,34)) as varchar(40)), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(cast('0.1234e35'  as numeric(34,34)) as varchar(40)), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(cast('1e6144'  as numeric(34)) as varchar(40)), 'NUMERIC_OVERFLOW')  -- error in decQuacQuantize()


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(cast('1e6145'  as numeric(34)) as varchar(40)), 'NUMERIC_INFINITE')  -- error in decQuadFromString()


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(256.0 as tinyint), 'NUMERIC_CAST_TINYINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-1.0 as tinyint), 'NUMERIC_CAST_TINYINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(2147483648.0 as tinyint), 'NUMERIC_CAST_TINYINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-2147483649.0 as tinyint), 'NUMERIC_CAST_TINYINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(32768.1 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-32769.0 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(2147483648.0 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-2147483649.0 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(2147483648.0 as int), 'NUMERIC_CAST_INT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-2147483649.0 as int), 'NUMERIC_CAST_INT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(9223372036854775808 as bigint), 'DATA_NUMERIC_CAST_BIGINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-9223372036854775809 as bigint), 'DATA_NUMERIC_CAST_BIGINT_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(999999999999999.99995 as money), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(12345. as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC cast overflow ============

_assert_error_(cast(-12345. as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== NUMERIC case when ============

_assert_(case when 1=1 then 10.55 end = 10.55)
_assert_(case when 1=4 then 10.55 end is null)
_assert_(case when 1=1 then 10.55 when 1=2 then 12345.6789 else 330000000000d end = 10.55)
_assert_(case when 1=1 then 10.55 when 1=2 then 12345.6789 end = 10.55)
_assert_(case when 1=4 then 10.55 when 2=2 then 12345.6789 else 330000000000d end = 12345.6789)
_assert_(case when 1=NULL then 10.55 when 2=2 then 12345.6789 else 330000000000d end = 12345.6789)
_assert_(case when 1=NULL then 10.55 when 2=NULL then 550000000000d else 330000000000d end = 330000000000d)
_assert_(case when 1=NULL then 10.55 when 2=NULL then 12345.6789 end is null)

_assert_(cast(case when 1=1 then 11. when 1=1 then 22334455.1234 when 1=1 then 1234.123456 end as varchar(30)) = '11.000000')




