

set language french

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648


-- ========== SMALLINT miscellaneous ============


_assert_(cast(32767 as smallint) = -(@SMALLINT_MIN + cast(1 as smallint)))

_assert_(cast(-32768 as smallint) = -@SMALLINT_MAX - cast(1 as smallint))


_assert_(cast(32767 as smallint) - cast(767 as smallint) = cast(32000 as smallint))

_assert_(cast(-32768 as smallint) + cast(768 as smallint) = cast(-32000 as smallint))

_assert_(@SMALLINT_MAX * cast(1 as smallint) = cast(32767 as smallint))

_assert_(@SMALLINT_MAX * cast(-1 as smallint) - cast(1 as smallint) = @SMALLINT_MIN)

_assert_((@SMALLINT_MIN+1) * cast(-1 as smallint) = @SMALLINT_MAX)

_assert_(@SMALLINT_MIN + @SMALLINT_MAX + cast(1 as smallint) = cast(0 as smallint))



-- ==========SMALLINT unary minus ============

_assert_(++++cast(4 as smallint) = cast(4 as smallint))
_assert_(cast(-4 as smallint) = cast(-1 as smallint) * cast(4 as smallint))
_assert_(- cast(-4 as smallint) = cast(4 as smallint))
_assert_(++- - -++cast(4 as smallint) = cast(-4 as smallint))
_assert_(++cast(4 as smallint) = cast(4 as smallint))
_assert_(-cast(30000 as smallint) = cast(-30000 as smallint))
_assert_(-@SMALLINT_MAX = cast(-32767 as smallint))

_assert_(-cast(NULL as smallint) is null)


-- ========== SMALLINT unary minus overflow ============

_assert_error_(-@SMALLINT_MIN, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT add ============

_assert_(cast(1000 as smallint) + cast(150 as smallint) = cast(1150 as smallint))

_assert_(cast(-1000 as smallint) + cast(-150 as smallint) = cast(-1150 as smallint))

_assert_(cast(1000 as smallint) + null is null)
_assert_(null + cast(1000 as smallint) is null)
_assert_(cast(null as smallint) + null is null)


-- ========== SMALLINT add max overflow ============

_assert_error_(@SMALLINT_MAX + cast(1 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT add max overflow ============

_assert_error_(@SMALLINT_MAX + @SMALLINT_MAX, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT add min overflow ============

_assert_error_(@SMALLINT_MIN + cast(-1 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT add min overflow ============

_assert_error_(@SMALLINT_MIN + -@SMALLINT_MAX, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT subtract ============

_assert_(cast(1000 as smallint) - cast(-150 as smallint) = cast(1150 as smallint))

_assert_(cast(-1000 as smallint) - cast(150 as smallint) = cast(-1150 as smallint))

_assert_(cast(1000 as smallint) - null is null)
_assert_(null - cast(1000 as smallint) is null)
_assert_(cast(null as smallint) - null is null)


-- ========== SMALLINT subtract max overflow ============

_assert_error_(@SMALLINT_MAX - cast(-1 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT subtract max overflow ============

_assert_error_(@SMALLINT_MAX - @SMALLINT_MIN, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT subtract min overflow ============

_assert_error_(@SMALLINT_MIN - cast(1 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT subtract min overflow ============

_assert_error_(@SMALLINT_MIN - @SMALLINT_MAX, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT multiply ============

_assert_(cast(1000 as smallint) * cast(-15 as smallint) = cast(-15000 as smallint))

_assert_(cast(-1000 as smallint) * cast(15 as smallint) = cast(-15000 as smallint))

_assert_(@SMALLINT_MAX / cast(2 as smallint) * cast(2 as smallint) = @SMALLINT_MAX - cast(1 as smallint))
_assert_(@SMALLINT_MIN / cast(2 as smallint) * cast(2 as smallint) = @SMALLINT_MIN)

_assert_(cast(1000 as smallint) * null is null)
_assert_(null * cast(1000 as smallint) is null)
_assert_(cast(null as smallint) * null is null)


-- ========== SMALLINT multiply max overflow ============

_assert_error_(@SMALLINT_MAX * cast(2 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT multiply max overflow ============

_assert_error_(@SMALLINT_MIN / cast(-2 as smallint) * cast(2 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT multiply max overflow ============

_assert_error_(@SMALLINT_MIN * cast(-1 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT multiply max overflow ============

_assert_error_(@SMALLINT_MAX * @SMALLINT_MAX, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT multiply max overflow ============

_assert_error_(@SMALLINT_MIN * @SMALLINT_MIN, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT multiply min overflow ============

_assert_error_(@SMALLINT_MIN * cast(2 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT multiply min overflow ============

_assert_error_(@SMALLINT_MAX * @SMALLINT_MIN, 'SMALLINT_OVERFLOW')


-- ========== SMALLINT divide ============

_assert_(cast(10000 as smallint) / cast(3 as smallint) = cast(3333 as smallint))

_assert_(cast(1000 as smallint) / cast(-3 as smallint) = cast(-333 as smallint))

_assert_(cast(-1000 as smallint) / cast(3 as smallint) = cast(-333 as smallint))

_assert_(cast(-1000 as smallint) / cast(-3 as smallint) = cast(333 as smallint))

_assert_(cast(0 as smallint) / cast(-3 as smallint) = cast(0 as smallint))

_assert_(@SMALLINT_MAX / cast(-1 as smallint) = -@SMALLINT_MAX)

_assert_(@SMALLINT_MIN / @SMALLINT_MAX = cast(-1 as smallint))

_assert_(@SMALLINT_MIN / @SMALLINT_MIN = cast(1 as smallint))

_assert_(@SMALLINT_MAX / @SMALLINT_MIN = cast(0 as smallint))

_assert_(cast(1000 as smallint) / null is null)
_assert_(null / cast(1000 as smallint) is null)
_assert_(cast(null as smallint) / null is null)


-- ========== SMALLINT divide max overflow ============

_assert_error_(@SMALLINT_MIN / cast(-1 as smallint), 'SMALLINT_OVERFLOW')


-- ========== SMALLINT divide by zero ============

_assert_error_(cast(1 as smallint)/cast(0 as smallint), 'DIVIDE_BY_ZERO')


-- ========== SMALLINT modulo ============

_assert_(cast(1000 as smallint) % cast(3 as smallint) = cast(1 as smallint))

_assert_(cast(1000 as smallint) % cast(-3 as smallint) = cast(1 as smallint))

_assert_(cast(-1000 as smallint) % cast(3 as smallint) = cast(-1 as smallint))

_assert_(cast(-1000 as smallint) % cast(-3 as smallint) = cast(-1 as smallint))

_assert_(cast(0 as smallint) % cast(-3 as smallint) = cast(0 as smallint))

_assert_(@SMALLINT_MAX % cast(-1 as smallint) = cast(0 as smallint))

_assert_(@SMALLINT_MAX % cast(2 as smallint) = cast(1 as smallint))

_assert_(@SMALLINT_MIN % cast(2 as smallint) = cast(0 as smallint))

_assert_(@SMALLINT_MIN % @SMALLINT_MAX = cast(-1 as smallint))

_assert_(@SMALLINT_MAX % @SMALLINT_MIN = @SMALLINT_MAX)

_assert_(-@SMALLINT_MAX % @SMALLINT_MIN = -@SMALLINT_MAX)

_assert_(cast(1000 as smallint) % null is null)
_assert_(null % cast(1000 as smallint) is null)
_assert_(cast(null as smallint) % null is null)


-- ========== SMALLINT modulo by zero ============

_assert_error_(cast(1 as smallint) % cast(0 as smallint), 'MODULO_BY_ZERO')


-- ========== SMALLINT binary op ============

_assert_(~cast(12345 as smallint) = cast(-12346 as smallint))

_assert_(cast(12345 as smallint) & cast(5327 as smallint) = cast(4105 as smallint))

_assert_(cast(12345 as smallint) | cast(5627 as smallint) = cast(13819 as smallint))

_assert_(cast(12345 as smallint) ^ cast(5427 as smallint) = cast(9482 as smallint))

_assert_(cast(12345 as smallint) | cast(5235 as smallint) & cast(13368 as smallint) ^ ~cast(6381 as smallint) = -cast(11478 as smallint))

_assert_(~cast(null as smallint) is null)

_assert_(cast(1000 as smallint) & null is null)
_assert_(null & cast(1000 as smallint) is null)
_assert_(cast(null as smallint) & null is null)

_assert_(cast(1000 as smallint) | null is null)
_assert_(null | cast(1000 as smallint) is null)
_assert_(cast(null as smallint) | null is null)

_assert_(cast(1000 as smallint) ^ null is null)
_assert_(null ^ cast(1000 as smallint) is null)
_assert_(cast(null as smallint) ^ null is null)


-- ========== SMALLINT comp op ============

_assert_(cast(15122 as smallint) = cast(15122 as smallint))

_assert_(not (cast(15122 as smallint) = cast(15123 as smallint)))

_assert_(not cast(15122 as smallint) = cast(15123 as smallint))

_assert_null_( cast(1000 as smallint) = null)
_assert_null_( null = cast(1000 as smallint))
_assert_null_( cast(null as smallint) = null)


_assert_(not (cast(15122 as smallint) != cast(15122 as smallint)))

_assert_(cast(15122 as smallint) != cast(15123 as smallint))

_assert_null_( cast(1000 as smallint) != null)
_assert_null_( null != cast(1000 as smallint))
_assert_null_( cast(null as smallint) != null)


_assert_(cast(15123 as smallint) > cast(15122 as smallint))

_assert_(not (cast(15122 as smallint) > cast(15122 as smallint)))

_assert_(cast(-15121 as smallint) > cast(-15122 as smallint))

_assert_(not (cast(-15122 as smallint) > cast(-15122 as smallint)))

_assert_(cast(1 as smallint) > cast(-1 as smallint))

_assert_(not (cast(-1 as smallint) > cast(1 as smallint)))

_assert_null_( cast(1000 as smallint) > null)
_assert_null_( null > cast(1000 as smallint))
_assert_null_( cast(null as smallint) > null)


_assert_(cast(15123 as smallint) >= cast(15122 as smallint))

_assert_(cast(15122 as smallint) >= cast(15122 as smallint))

_assert_(not (cast(15121 as smallint) >= cast(15122 as smallint)))

_assert_(cast(-15121 as smallint) >= cast(-15122 as smallint))

_assert_(cast(-15122 as smallint) >= cast(-15122 as smallint))

_assert_(not (cast(-15123 as smallint) >= cast(-15122 as smallint)))

_assert_(cast(1 as smallint) >= cast(-1 as smallint))

_assert_(not (cast(-1 as smallint) >= cast(1 as smallint)))

_assert_null_( cast(1000 as smallint) >= null)
_assert_null_( null >= cast(1000 as smallint))
_assert_null_( cast(null as smallint) >= null)


_assert_(cast(15121 as smallint) < cast(15122 as smallint))

_assert_(not (cast(15122 as smallint) < cast(15122 as smallint)))

_assert_(cast(-15123 as smallint) < cast(-15122 as smallint))

_assert_(not (cast(-15122 as smallint) < cast(-15122 as smallint)))

_assert_(not (cast(1 as smallint) < cast(-1 as smallint)))

_assert_(cast(-1 as smallint) < cast(1 as smallint))

_assert_null_( cast(1000 as smallint) < null)
_assert_null_( null < cast(1000 as smallint))
_assert_null_( cast(null as smallint) < null)


_assert_(cast(15121 as smallint) <= cast(15122 as smallint))

_assert_(cast(15122 as smallint) <= cast(15122 as smallint))

_assert_(not (cast(15123 as smallint) <= cast(15122 as smallint)))

_assert_(cast(-15123 as smallint) <= cast(-15122 as smallint))

_assert_(cast(-15122 as smallint) <= cast(-15122 as smallint))

_assert_(not (cast(-15121 as smallint) <= cast(-15122 as smallint)))

_assert_(not (cast(1 as smallint) <= cast(-1 as smallint)))

_assert_(cast(-1 as smallint) <= cast(1 as smallint))

_assert_null_( cast(1000 as smallint) <= null)
_assert_null_( null <= cast(1000 as smallint))
_assert_null_( cast(null as smallint) <= null)


-- ========== SMALLINT is [not] null ============

_assert_(cast(NULL as smallint) is null)

_assert_(not cast(15123 as smallint) is null)


_assert_(not cast(NULL as smallint) is not null)

_assert_(cast(15123 as smallint) is not null)


-- ========== SMALLINT [not] in list ============

_assert_(cast(12 as smallint) in (cast(12 as smallint)))

_assert_(cast(12 as smallint) in (cast(0 as smallint), cast(12 as smallint)))

_assert_(cast(1512 as smallint) in (cast(0 as smallint), cast(1512 as smallint), cast(9 as smallint)))

_assert_(cast(12 as smallint) in (cast(0 as smallint), NULL, cast(12 as smallint), cast(9 as smallint)))

_assert_(cast(12 as smallint) in (cast(0 as smallint), cast(12 as smallint), NULL, cast(9 as smallint)))


_assert_(not cast(12 as smallint) in (cast(0 as smallint), cast(1 as smallint), cast(9 as smallint)))

_assert_null_(cast(12 as smallint) in (cast(0 as smallint), cast(1 as smallint), NULL, cast(9 as smallint)))


_assert_(not cast(12 as smallint) not in (cast(12 as smallint)))

_assert_(not cast(1512 as smallint) not in (cast(0 as smallint), cast(1512 as smallint)))

_assert_(not cast(12 as smallint) not in (cast(0 as smallint), cast(12 as smallint), cast(9 as smallint)))

_assert_(not cast(12 as smallint) not in (cast(0 as smallint), NULL, cast(12 as smallint), cast(9 as smallint)))

_assert_(not cast(12 as smallint) not in (cast(0 as smallint), cast(12 as smallint), NULL, cast(9 as smallint)))


_assert_(cast(12 as smallint) not in (cast(0 as smallint), cast(1 as smallint), cast(9 as smallint)))

_assert_null_(cast(12 as smallint) not in (cast(0 as smallint), cast(1 as smallint), NULL, cast(9 as smallint)))


-- ========== SMALLINT cast ============

_assert_(cast(cast(123 as smallint) as char(10)) + 'x' = '123       x')
_assert_(cast(cast(123 as smallint) as char) = '123')
_assert_(cast(cast(123 as smallint) as char(3)) = '123')
_assert_(cast(cast(-123 as smallint) as char(4)) = '-123')
_assert_(cast(@SMALLINT_MAX as char) = '32767')
_assert_(cast(@SMALLINT_MIN as char) = '-32768')
_assert_(cast(cast(null as smallint) as char) is null)

_assert_(cast(cast(123 as smallint) as varchar(10)) + 'x' = '123x')
_assert_(cast(cast(123 as smallint) as varchar) = '123')
_assert_(cast(cast(123 as smallint) as varchar(3)) = '123')
_assert_(cast(cast(-123 as smallint) as varchar(4)) = '-123')
_assert_(cast(@SMALLINT_MAX as varchar) = '32767')
_assert_(cast(@SMALLINT_MIN as varchar) = '-32768')
_assert_(cast(cast(null as smallint) as varchar) is null)

_assert_(cast(cast(123 as smallint) as bit) = 1)
_assert_(cast(cast(-123 as smallint) as bit) = 1)
_assert_(cast(cast(0 as smallint) as bit) = 0)
_assert_(cast(cast(null as smallint) as bit) is null)

_assert_(cast(cast(255 as smallint) as tinyint) = 255)
_assert_(cast(cast(0 as smallint) as tinyint) = 0)
_assert_(cast(cast(null as smallint) as tinyint) is null)

_assert_(cast(cast(32767 as smallint) as smallint) = 32767)
_assert_(cast(cast(-32768 as smallint) as smallint) = -32768)
_assert_(cast(cast(null as smallint) as smallint) is null)

_assert_(cast(@SMALLINT_MAX as smallint) = @SMALLINT_MAX)
_assert_(cast(@SMALLINT_MIN as smallint) = @SMALLINT_MIN)
_assert_(cast(cast(null as smallint) as smallint) is null)

_assert_(cast(@SMALLINT_MAX as bigint) = @SMALLINT_MAX)
_assert_(cast(@SMALLINT_MIN as bigint) = @SMALLINT_MIN)
_assert_(cast(cast(null as smallint) as bigint) is null)

_assert_(cast(@SMALLINT_MAX as money) = @SMALLINT_MAX)
_assert_(cast(@SMALLINT_MIN as money) = @SMALLINT_MIN)
_assert_(cast(cast(null as smallint) as money) is null)

_assert_(cast(@SMALLINT_MAX as numeric(12,2)) = @SMALLINT_MAX)
_assert_(cast(@SMALLINT_MIN as numeric(12,2)) = @SMALLINT_MIN)
_assert_(cast(cast(null as smallint) as numeric(12,2)) is null)

_assert_(cast(cast(12345 as smallint) as numeric(5)) = 12345)
_assert_(cast(cast(-12345 as smallint) as numeric(5)) = -12345)
_assert_(cast(cast(null as smallint) as numeric(5)) is null)

_assert_(cast(@SMALLINT_MAX as float) = @SMALLINT_MAX)
_assert_(cast(@SMALLINT_MIN as float) = @SMALLINT_MIN)
_assert_(cast(cast(null as smallint) as float) is null)

_assert_(cast(@SMALLINT_MAX as datetime) = '19890918')
_assert_(cast(cast(0 as smallint) as datetime) = '19000101')
_assert_(cast(@SMALLINT_MIN as datetime) = '18100415')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(123 as smallint) as char(2)), 'SMALLINT_CAST_VARCHAR_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(-123 as smallint) as char(3)), 'SMALLINT_CAST_VARCHAR_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(123 as smallint) as varchar(2)), 'SMALLINT_CAST_VARCHAR_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(-123 as smallint) as varchar(3)), 'SMALLINT_CAST_VARCHAR_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(256 as smallint) as tinyint), '_INT_CAST_TINYINT_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(-1 as smallint) as tinyint), '_INT_CAST_TINYINT_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(32768 as smallint) as smallint), '_INT_CAST_SMALLINT_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(-32769 as smallint) as smallint), '_INT_CAST_SMALLINT_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(12345 as smallint) as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== SMALLINT cast overflow ============

_assert_error_(cast(cast(-12345 as smallint) as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== SMALLINT case when ============

_assert_(case when 1=1 then cast(10000 as smallint) end = cast(10000 as smallint))
_assert_(case when 1=4 then cast(10000 as smallint) end is null)
_assert_(case when 1=1 then cast(10000 as smallint) when 1=2 then cast(22000 as smallint) else cast(3300 as smallint) end = cast(10000 as smallint))
_assert_(case when 1=1 then cast(10000 as smallint) when 1=2 then cast(22000 as smallint) end = cast(10000 as smallint))
_assert_(case when 1=4 then cast(10000 as smallint) when 2=2 then cast(22000 as smallint) else cast(3300 as smallint) end = cast(22000 as smallint))
_assert_(case when 1=NULL then cast(10000 as smallint) when 2=2 then cast(22000 as smallint) else cast(3300 as smallint) end = cast(22000 as smallint))
_assert_(case when 1=NULL then cast(10000 as smallint) when 2=NULL then cast(5500 as smallint) else cast(3300 as smallint) end = cast(3300 as smallint))
_assert_(case when 1=NULL then cast(10000 as smallint) when 2=NULL then cast(22000 as smallint) end is null)



