
set language french



-- ========== BOOLEAN bool operators ============

_assert_null_(1=NULL and 1=NULL)

_assert_null_(1=NULL and 1=1)

_assert_null_(1=1 and 1=NULL)

_assert_(1=1 and 1=1)


_assert_null_(1=NULL or 1=NULL)

_assert_(1=NULL or 1=1)

_assert_(1=1 or 1=NULL)

_assert_(1=1 or 1=1)


_assert_(not 1=2)

_assert_(not ( not 1=1 and 1=2 ))

_assert_null_(1=NULL)



