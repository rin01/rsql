

-- a bug in CSE printed NULL for this expression, because it forgot to refresh dataslot of KEPT token. It is fixed now.

_assert_('hel' + 'lo' + nullif('hel' + 'lo', 'world') = 'hellohello')


-- cse

declare @a int = 10
declare @b int
declare @c int

set @a = 7 + @a

_assert_(7 + @a = 24) -- this 7 + @a is not the same as the one in line above, because @a has changed


-- cse

set @a = 1
set @b = 2
set @c = 3

if 1=2 print 999

set @b = @a
set @a = 22222
set @a = @c

_assert_(@a = 3 and @b = 1)

-- cse

set @a = 1
set @b = 22
set @c = 333

if 1=2 print 999

set @b = @a
set @a = 22222
set @a = @c
set @c = @b
set @b = @b + @a + @b
set @a=@a
set @a = @a + @b

_assert_(@a = 668 and @b = 335 and @c = 1)

-- cse

set @a = 1
set @b = 22
set @c = 333

if 1=2 print 999

set @b = @a
set @a = 22222
set @a = @c
set @c = @b
if 1=2 print 999
set @b = @b + @a + @b
set @a=@a
set @a = @a + @b

_assert_(@a = 668 and @b = 335 and @c = 1)


-- cse

set @a = 1
set @b = 22
set @c = 333

if 1=2 print 999

set @b = @a
set @a = 22222
if 1=2 print 999
set @a = @c
set @c = @b
if 1=2 print 999
set @b = @b + @a + @b
set @a=@a
if 1=2 print 999
set @a = @a + @b

_assert_(@a = 668 and @b = 335 and @c = 1)


-- cse

set @a = 0
set @b = 0
declare @bb int = 0

while @a < 1
begin
set @bb = @b
set @b = @bb + 1
set @b = @b + @bb
set @a =  1 + @a
end

_assert_(@b=1)

-- cse

set @a = 0
set @b = 0
set @bb = 0

while @a < 1
begin
set @bb = @b
set @b = @bb + 1
set @b = @b + @bb
set @a =  1
end

_assert_(@b=1)

--cse

declare @aaa int = 10

set @aaa = 33

if 1=2 print 'asdf'

_assert_(@aaa = 33)


----------------------

-- code sample from http://l-vasquez.com/category/sql-server/, a little modified

declare @Numero NUMERIC(20,2) =1234.45

declare @iii int = 0
declare @samples varchar(1000) = '1234.45, 635628934.5647389, 15.93847,   ,0.4756, 837646.0493'
declare @start_section int
declare @end_section int
declare @flag_empty int
declare @resultt varchar(8000) = ''

set @samples += ','
set @start_section = 1
while @iii < 10 
  begin
  set @end_section = charindex(',', @samples, @start_section)
  if @end_section > 0
    begin
    if ltrim(rtrim(substring(@samples, @start_section, @end_section - @start_section)))!= ''
      begin
      set @numero = cast(substring(@samples, @start_section, @end_section - @start_section) as NUMERIC(20,2))
--      print @numero
      set @flag_empty = 0
      end
    else
      set @flag_empty = 1
    set @start_section = @end_section + 1
    if @flag_empty = 1
      begin
      set @flag_empty = 0
      continue
      end
    end
  else
    break

    BEGIN
--        SET NOCOUNT ON
        DECLARE @lnEntero INT,
        @lcRetorno VARCHAR(512),
        @lnTerna INT,
        @lcMiles VARCHAR(512),
        @lcCadena VARCHAR(512),
        @lnUnidades INT,
        @lnDecenas INT,
        @lnCentenas INT,
        @lnFraccion INT
        SET @lnEntero = CAST(@Numero AS INT)
        SET @lnFraccion = (@Numero - @lnEntero) * 100
        SET @lcRetorno = ''
        SET @lnTerna = 1
        WHILE @lnEntero > 0
        BEGIN /* WHILE */
        -- Recorro terna por terna
        SET @lcCadena = ''
        SET @lnUnidades = @lnEntero % 10
        SET @lnEntero = CAST(@lnEntero/10 AS INT)
        SET @lnDecenas = @lnEntero % 10
        SET @lnEntero = CAST(@lnEntero/10 AS INT)
        SET @lnCentenas = @lnEntero % 10
        SET @lnEntero = CAST(@lnEntero/10 AS INT)
        -- Analizo las unidades
        SET @lcCadena =
        CASE /* UNIDADES */
        WHEN @lnUnidades = 1 AND @lnTerna = 1 THEN 'UNO ' + @lcCadena
        WHEN @lnUnidades = 1 AND @lnTerna <> 1 THEN 'UN ' + @lcCadena
        WHEN @lnUnidades = 2 THEN 'DOS ' + @lcCadena
        WHEN @lnUnidades = 3 THEN 'TRES ' + @lcCadena
        WHEN @lnUnidades = 4 THEN 'CUATRO ' + @lcCadena
        WHEN @lnUnidades = 5 THEN 'CINCO ' + @lcCadena
        WHEN @lnUnidades = 6 THEN 'SEIS ' + @lcCadena
        WHEN @lnUnidades = 7 THEN 'SIETE ' + @lcCadena
        WHEN @lnUnidades = 8 THEN 'OCHO ' + @lcCadena
        WHEN @lnUnidades = 9 THEN 'NUEVE ' + @lcCadena
        ELSE @lcCadena
        END /* UNIDADES */
        -- Analizo las decenas
        SET @lcCadena =
        CASE /* DECENAS */
        WHEN @lnDecenas = 1 THEN
        CASE @lnUnidades
        WHEN 0 THEN 'DIEZ '
        WHEN 1 THEN 'ONCE '
        WHEN 2 THEN 'DOCE '
        WHEN 3 THEN 'TRECE '
        WHEN 4 THEN 'CATORCE '
        WHEN 5 THEN 'QUINCE '
        ELSE 'DIECI' + @lcCadena
        END
        WHEN @lnDecenas = 2 AND @lnUnidades = 0 THEN 'VEINTE ' + @lcCadena
        WHEN @lnDecenas = 2 AND @lnUnidades <> 0 THEN 'VEINTI' + @lcCadena
        WHEN @lnDecenas = 3 AND @lnUnidades = 0 THEN 'TREINTA ' + @lcCadena
        WHEN @lnDecenas = 3 AND @lnUnidades <> 0 THEN 'TREINTA Y ' + @lcCadena
        WHEN @lnDecenas = 4 AND @lnUnidades = 0 THEN 'CUARENTA ' + @lcCadena
        WHEN @lnDecenas = 4 AND @lnUnidades <> 0 THEN 'CUARENTA Y ' + @lcCadena
        WHEN @lnDecenas = 5 AND @lnUnidades = 0 THEN 'CINCUENTA ' + @lcCadena
        WHEN @lnDecenas = 5 AND @lnUnidades <> 0 THEN 'CINCUENTA Y ' + @lcCadena
        WHEN @lnDecenas = 6 AND @lnUnidades = 0 THEN 'SESENTA ' + @lcCadena
        WHEN @lnDecenas = 6 AND @lnUnidades <> 0 THEN 'SESENTA Y ' + @lcCadena
        WHEN @lnDecenas = 7 AND @lnUnidades = 0 THEN 'SETENTA ' + @lcCadena
        WHEN @lnDecenas = 7 AND @lnUnidades <> 0 THEN 'SETENTA Y ' + @lcCadena
        WHEN @lnDecenas = 8 AND @lnUnidades = 0 THEN 'OCHENTA ' + @lcCadena
        WHEN @lnDecenas = 8 AND @lnUnidades <> 0 THEN 'OCHENTA Y ' + @lcCadena
        WHEN @lnDecenas = 9 AND @lnUnidades = 0 THEN 'NOVENTA ' + @lcCadena
        WHEN @lnDecenas = 9 AND @lnUnidades <> 0 THEN 'NOVENTA Y ' + @lcCadena
        ELSE @lcCadena
        END /* DECENAS */
        -- Analizo las centenas
        SET @lcCadena =
        CASE /* CENTENAS */
        WHEN @lnCentenas = 1 AND @lnUnidades = 0 AND @lnDecenas = 0 THEN 'CIEN ' +
        @lcCadena
        WHEN @lnCentenas = 1 AND NOT(@lnUnidades = 0 AND @lnDecenas = 0) THEN
        'CIENTO ' + @lcCadena
        WHEN @lnCentenas = 2 THEN 'DOSCIENTOS ' + @lcCadena
        WHEN @lnCentenas = 3 THEN 'TRESCIENTOS ' + @lcCadena
        WHEN @lnCentenas = 4 THEN 'CUATROCIENTOS ' + @lcCadena
        WHEN @lnCentenas = 5 THEN 'QUINIENTOS ' + @lcCadena
        WHEN @lnCentenas = 6 THEN 'SEISCIENTOS ' + @lcCadena
        WHEN @lnCentenas = 7 THEN 'SETECIENTOS ' + @lcCadena
        WHEN @lnCentenas = 8 THEN 'OCHOCIENTOS ' + @lcCadena
        WHEN @lnCentenas = 9 THEN 'NOVECIENTOS ' + @lcCadena
        ELSE @lcCadena
        END /* CENTENAS */
        -- Analizo la terna
        SET @lcCadena =
        CASE /* TERNA */
        WHEN @lnTerna = 1 THEN @lcCadena
        WHEN @lnTerna = 2 AND (@lnUnidades + @lnDecenas + @lnCentenas <> 0) THEN
        @lcCadena + ' MIL '
        WHEN @lnTerna = 3 AND (@lnUnidades + @lnDecenas + @lnCentenas <> 0) AND
        @lnUnidades = 1 AND @lnDecenas = 0 AND @lnCentenas = 0 THEN @lcCadena + '
        MILLON '
        WHEN @lnTerna = 3 AND (@lnUnidades + @lnDecenas + @lnCentenas <> 0) AND
        NOT (@lnUnidades = 1 AND @lnDecenas = 0 AND @lnCentenas = 0) THEN @lcCadena
        + ' MILLONES '
        WHEN @lnTerna = 4 AND (@lnUnidades + @lnDecenas + @lnCentenas <> 0) THEN
        @lcCadena + ' MIL MILLONES '
        ELSE ''
        END /* TERNA */
        -- Armo el retorno terna a terna
        SET @lcRetorno = @lcCadena + @lcRetorno
        SET @lnTerna = @lnTerna + 1
        END /* WHILE */
        IF @lnTerna = 1
        SET @lcRetorno = 'CERO'
        set @resultt += RTRIM(@lcRetorno) + ' CON ' + LTRIM(STR(@lnFraccion,2)) + '/100' + ': '
    END


end

_assert_(@resultt + '>' = 'UN  MIL DOSCIENTOS TREINTA Y CUATRO CON 45/100: SEISCIENTOS TREINTA Y CINCO  MILLONES SEISCIENTOS VEINTIOCHO  MIL NOVECIENTOS TREINTA Y CUATRO CON 56/100: QUINCE CON 94/100: CERO CON 48/100: OCHOCIENTOS TREINTA Y SIETE  MIL SEISCIENTOS CUARENTA Y SEIS CON 5/100: >')


-- BETWEEN --

declare @s1 varchar(10) = 'hello'
declare @mm money = 123.45
declare @nn numeric(12,2) = 123.45
declare @ff float = 123.45
declare @dt date = '20160202'
declare @tt time = '00:30'
declare @dtt datetime = '20160202'

set @a = 15

_assert_(@s1 between 'h' and 'hf')
_assert_(@s1 between 'hello' and 'hf')
_assert_(@s1 not between 'helloa' and 'hf')
_assert_(not @s1 between 'helloa' and 'hf')
_assert_(@s1 between 'hello' and 'hello')
_assert_(@s1 between 'HELLO' and 'HELLO')

_assert_(cast(@a as bit) between cast(0 as bit) and cast(20 as bit))
_assert_(cast(@a as bit) between cast(0 as bit) and cast(15 as bit))
_assert_(not cast(@a as bit) between cast(16 as bit) and cast(0 as bit))
_assert_(cast(@a as bit) between cast(0 as bit) and 15)
_assert_(not cast(@a as bit) between cast(15 as bit) and 0)

_assert_(cast(@a as tinyint) between cast(10 as tinyint) and cast(20 as tinyint))
_assert_(cast(@a as tinyint) between cast(15 as tinyint) and cast(15 as tinyint))
_assert_(not cast(@a as tinyint) between cast(16 as tinyint) and cast(10 as tinyint))
_assert_(cast(@a as tinyint) between cast(15 as tinyint) and 15)

_assert_(cast(@a as smallint) between cast(10 as smallint) and cast(20 as smallint))
_assert_(cast(@a as smallint) between cast(15 as smallint) and cast(15 as smallint))
_assert_(not cast(@a as smallint) between cast(16 as smallint) and cast(10 as smallint))
_assert_(cast(@a as smallint) between cast(15 as smallint) and 15)

_assert_(@a between 10 and 20)
_assert_(@a between 15 and 15)
_assert_(not @a between 16 and 15)

_assert_(cast(@a as bigint) between cast(10 as bigint) and cast(20 as bigint))
_assert_(cast(@a as bigint) between cast(15 as bigint) and cast(15 as bigint))
_assert_(not cast(@a as bigint) between cast(16 as bigint) and cast(10 as bigint))
_assert_(cast(@a as bigint) between cast(15 as bigint) and 15)

_assert_(@nn between 123d and 124d)
_assert_(@nn between 123.45 and 123.45)
_assert_(not @nn between 123.46 and 123.45)

_assert_(@mm between $123 and $124)
_assert_(@mm between $123.45 and $123.45)
_assert_(not @mm between $123.46 and $123.45)

_assert_(@ff between 123d and 124d)
_assert_(@ff between 123.45f and 123.45f)
_assert_(not @ff between 123.46f and 123.45f)
_assert_(not @ff between 123.451f and 123.451f)

_assert_(@dt between '20160201' and '20160203')
_assert_(@dt between '20160202' and '20160202')
_assert_(    @dt between '20160202' and '20160204')
_assert_(not @dt between '20160203' and '20160204')
_assert_(not @dt between '20160203' and '20160202')

_assert_(@tt between '00:29' and '00:30')
_assert_(@tt between '00:30' and '00:30')
_assert_(    @tt between '00:30:00.000000000' and '00:30')
_assert_(not @tt between '00:30:0.000000001' and '00:30:0.000000001')

_assert_(@dtt between '20160201' and '20160203')
_assert_(@dtt between '20160202' and '20160202')
_assert_(    @dtt between '20160202  00:00:00.000000000' and '20160204')
_assert_(not @dtt between '20160202 00:00:0.000000001' and '20160204')
_assert_(not @dtt between '20160202 00:00:0.000000001' and '20160202 00:00:0.000000001')





