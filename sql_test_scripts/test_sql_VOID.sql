


set language french


-- ========== VOID unary minus ============

_assert_(++++NULL is null)
_assert_(-NULL is null)
_assert_(-1 * NULL is null)
_assert_(- -NULL is null)
_assert_(++- - -++NULL is null)
_assert_(++NULL is null)

_assert_(-cast(NULL as int) is null)


-- ========== VOID add, subtract, multiply, divide, modulo ============

_assert_(NULL + NULL is null)

_assert_(NULL + -NULL is null)


_assert_(NULL - NULL is null)

_assert_(NULL - -NULL is null)


_assert_(NULL * NULL is null)

_assert_(NULL * -NULL is null)


_assert_(NULL / NULL is null)

_assert_(NULL / -NULL is null)


_assert_(NULL % NULL is null)

_assert_(NULL % -NULL is null)


-- ========== VOID comp operators ============

_assert_null_(NULL = NULL)

_assert_null_(NULL = -NULL)


_assert_null_(NULL > NULL)

_assert_null_(NULL > -NULL)


_assert_null_(NULL < NULL)

_assert_null_(NULL < -NULL)


_assert_null_(NULL >= NULL)

_assert_null_(NULL >= -NULL)


_assert_null_(NULL <= NULL)

_assert_null_(NULL <= -NULL)


_assert_null_(NULL != NULL)

_assert_null_(NULL != -NULL)


-- ========== VOID is null ============

_assert_(NULL is null)

_assert_(-NULL is null)


_assert_(not NULL is not null)

_assert_(not -NULL is not null)


-- ========== VOID in list ============

_assert_null_(NULL in ( NULL, NULL, NULL ))

_assert_null_(-NULL in ( NULL, NULL, NULL ))


_assert_null_(NULL not in ( NULL, NULL, NULL ))

_assert_null_(-NULL not in ( NULL, NULL, NULL ))


-- ========== VOID cast============

_assert_(cast(NULL as varbinary) is null)

_assert_(cast(NULL as char) is null)
_assert_(cast(NULL as char(10)) is null)

_assert_(cast(NULL as varchar) is null)
_assert_(cast(NULL as varchar(10)) is null)

_assert_null_('a' like NULL)
_assert_null_('a' not like NULL)

_assert_(cast(NULL as bit) is null)

_assert_(cast(NULL as tinyint) is null)

_assert_(cast(NULL as smallint) is null)

_assert_(cast(NULL as int) is null)

_assert_(cast(NULL as bigint) is null)

_assert_(cast(NULL as money) is null)

_assert_(cast(NULL as numeric(12,2)) is null)

_assert_(cast(NULL as float) is null)

_assert_(cast(NULL as date) is null)

_assert_(cast(NULL as time) is null)

_assert_(cast(NULL as datetime) is null)




