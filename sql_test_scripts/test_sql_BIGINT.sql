

set language french

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @INT_MAX_DATETIME int = 2958463  -- 9999-12-31 0:0:0
declare @INT_MIN_DATETIME int =  -693595  -- 0001-01-01 0:0:0


declare @BIGINT_MAX bigint =  9223372036854775807
declare @BIGINT_MIN bigint = -9223372036854775808



-- ========== BIGINT miscellaneous ============


_assert_(cast(9223372036854775807 as bigint) = -(@BIGINT_MIN + cast(1 as bigint)))

_assert_(cast(-9223372036854775808 as bigint) = -@BIGINT_MAX - cast(1 as bigint))


_assert_(cast(9223372036854775807 as bigint) - cast(807 as bigint) = cast(9223372036854775000 as bigint))

_assert_(cast(-9223372036854775808 as bigint) + cast(808 as bigint) = cast(-9223372036854775000 as bigint))

_assert_(@BIGINT_MAX * cast(1 as bigint) = cast(9223372036854775807 as bigint))

_assert_(@BIGINT_MAX * cast(-1 as bigint) - cast(1 as bigint) = @BIGINT_MIN)

_assert_((@BIGINT_MIN+1) * cast(-1 as bigint) = @BIGINT_MAX)

_assert_(@BIGINT_MIN + @BIGINT_MAX + cast(1 as bigint) = cast(0 as bigint))



-- ==========BIGINT unary minus ============

_assert_(++++cast(4 as bigint) = cast(4 as bigint))
_assert_(cast(-4 as bigint) = cast(-1 as bigint) * cast(4 as bigint))
_assert_(- cast(-9223372036854775807 as bigint) = cast(9223372036854775807 as bigint))
_assert_(++- - -++cast(4 as bigint) = cast(-4 as bigint))
_assert_(++cast(4 as bigint) = cast(4 as bigint))

_assert_(-cast(NULL as bigint) is null)


-- ========== BIGINT unary minus overflow ============

_assert_error_(-@BIGINT_MIN, 'BIGINT_OVERFLOW')


-- ========== BIGINT add ============

_assert_(cast(9223372036854775807 as bigint) + cast(-807 as bigint) = cast(9223372036854775000 as bigint))

_assert_(cast(-1000 as bigint) + cast(-150 as bigint) = cast(-1150 as bigint))

_assert_(cast(9223372036854775000 as bigint) + cast(807 as bigint) = cast(9223372036854775807 as bigint))
_assert_(cast(-9223372036854775000 as bigint) + cast(-808 as bigint) = cast(-9223372036854775808 as bigint))

_assert_(cast(1000 as bigint) + null is null)
_assert_(null + cast(1000 as bigint) is null)
_assert_(cast(null as bigint) + null is null)


-- ========== BIGINT add max overflow ============

_assert_error_(cast(9223372036854775000 as bigint) + cast(808 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT add max overflow ============

_assert_error_(@BIGINT_MAX + cast(1 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT add max overflow ============

_assert_error_(@BIGINT_MAX + @BIGINT_MAX, 'BIGINT_OVERFLOW')


-- ========== BIGINT add min overflow ============

_assert_error_(cast(-9223372036854775000 as bigint) + cast(-809 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT add min overflow ============

_assert_error_(@BIGINT_MIN + cast(-1 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT add min overflow ============

_assert_error_(@BIGINT_MIN + -@BIGINT_MAX, 'BIGINT_OVERFLOW')


-- ========== BIGINT subtract ============

_assert_(cast(9223372036854775807 as bigint) - cast(807 as bigint) = cast(9223372036854775000 as bigint))

_assert_(cast(-1000 as bigint) - cast(150 as bigint) = cast(-1150 as bigint))

_assert_(cast(9223372036854775000 as bigint) - cast(-807 as bigint) = cast(9223372036854775807 as bigint))
_assert_(cast(-9223372036854775000 as bigint) - cast(808 as bigint) = cast(-9223372036854775808 as bigint))

_assert_(cast(1000 as bigint) - null is null)
_assert_(null - cast(1000 as bigint) is null)
_assert_(cast(null as bigint) - null is null)


-- ========== BIGINT subtract max overflow ============

_assert_error_(cast(9223372036854775000 as bigint) - cast(-808 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT subtract max overflow ============

_assert_error_(@BIGINT_MAX - cast(-1 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT subtract max overflow ============

_assert_error_(@BIGINT_MAX - @BIGINT_MIN, 'BIGINT_OVERFLOW')


-- ========== BIGINT subtract min overflow ============

_assert_error_(cast(-9223372036854775000 as bigint) - cast(809 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT subtract min overflow ============

_assert_error_(@BIGINT_MIN - cast(1 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT subtract min overflow ============

_assert_error_(@BIGINT_MIN - @BIGINT_MAX, 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply ============

_assert_(@BIGINT_MAX * cast(0 as bigint) = 0)
_assert_(@BIGINT_MAX * cast(-0 as bigint) = 0)

_assert_(@BIGINT_MIN * cast(0 as bigint) = 0)
_assert_((@BIGINT_MIN+1) * cast(-0 as bigint) = 0)


_assert_(@BIGINT_MAX * cast(1 as bigint) = @BIGINT_MAX)
_assert_(@BIGINT_MAX * cast(-1 as bigint) = @BIGINT_MIN + 1)

_assert_(@BIGINT_MIN * cast(1 as bigint) = @BIGINT_MIN)
_assert_((@BIGINT_MIN+1) * cast(-1 as bigint) = @BIGINT_MAX)


_assert_(@BIGINT_MAX/2 * cast(2 as bigint) = @BIGINT_MAX - 1)
_assert_(round(cast(@BIGINT_MAX as numeric(32)) / 2, 0, 1) * cast(2 as numeric) = @BIGINT_MAX - 1)
_assert_(@BIGINT_MAX/2 * cast(-2 as bigint) = @BIGINT_MIN + 2)
_assert_(round(cast(@BIGINT_MAX as numeric(32)) / 2, 0, 1) * cast(-2 as numeric) = @BIGINT_MIN + 2)

_assert_(@BIGINT_MIN/2 * cast(2 as bigint) = @BIGINT_MIN)
_assert_((@BIGINT_MIN/2 + 1) * cast(-2 as bigint) = @BIGINT_MAX - 1)
_assert_(( round(cast(@BIGINT_MIN as numeric(32)) / 2, 0, 1) + 1 ) * cast(-2 as numeric) = @BIGINT_MAX - 1)


_assert_(cast(1000 as bigint) * cast(0 as bigint) = cast(0 as bigint))
_assert_(cast(0 as bigint) * cast(10000000 as bigint) = cast(0 as bigint))

_assert_(cast(1000000 as bigint) * cast(-1500000 as bigint) = cast(-1500000000000 as bigint))
_assert_(cast(-1000000 as bigint) * cast(1500000 as bigint) = cast(-1500000000000 as bigint))


_assert_(cast(2147483647 as bigint) * cast(4294967296 as bigint) + cast(4294967295 as bigint) = @BIGINT_MAX)
_assert_(cast(-2147483647 as bigint) * cast(-4294967296 as bigint) + cast(4294967295 as bigint) = @BIGINT_MAX)
_assert_(cast(-2147483648 as bigint) * cast(4294967296 as bigint) = @BIGINT_MIN)
_assert_(cast(2147483648 as bigint) * cast(-4294967296 as bigint) = @BIGINT_MIN)

_assert_(cast(2147483647 as bigint) * cast(4294967296 as bigint) + cast(4294967295 as bigint) = cast(2147483647 as numeric(32)) * cast(4294967296 as numeric(32)) + cast(4294967295 as numeric(32)))
_assert_(cast(-2147483648 as bigint) * cast(4294967296 as bigint) = cast(-2147483648 as numeric(32)) * cast(4294967296 as numeric(32)))


_assert_(cast(-1000 as bigint) * cast(15 as bigint) = cast(-15000 as bigint))

_assert_(@BIGINT_MAX / cast(2 as bigint) * cast(2 as bigint) = @BIGINT_MAX - cast(1 as bigint))
_assert_(@BIGINT_MIN / cast(2 as bigint) * cast(2 as bigint) = @BIGINT_MIN)

_assert_(cast(1000 as bigint) * null is null)
_assert_(null * cast(1000 as bigint) is null)
_assert_(cast(null as bigint) * null is null)


-- ========== BIGINT multiply max overflow ============

_assert_error_(cast(2147483648 as bigint) * cast(4294967296 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply max overflow ============

_assert_error_(cast(-2147483648 as bigint) * cast(-4294967296 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply min overflow ============

_assert_error_(cast(-2147483648 as bigint) * cast(4294967297 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply min overflow ============

_assert_error_(cast(2147483648 as bigint) * cast(-4294967297 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply max overflow ============

_assert_error_(@BIGINT_MAX * cast(2 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply max overflow ============

_assert_error_(@BIGINT_MIN / cast(-2 as bigint) * cast(2 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply max overflow ============

_assert_error_(@BIGINT_MIN * cast(-1 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply max overflow ============

_assert_error_(@BIGINT_MAX * @BIGINT_MAX, 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply max overflow ============

_assert_error_(@BIGINT_MIN * @BIGINT_MIN, 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply min overflow ============

_assert_error_(@BIGINT_MIN * cast(2 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT multiply min overflow ============

_assert_error_(@BIGINT_MAX * @BIGINT_MIN, 'BIGINT_OVERFLOW')


-- ========== BIGINT divide ============

_assert_(cast(1000000000000 as bigint) / cast(3 as bigint) = cast(333333333333 as bigint))

_assert_(cast(1000000000000 as bigint) / cast(-3 as bigint) = cast(-333333333333 as bigint))

_assert_(cast(-1000000000000 as bigint) / cast(3 as bigint) = cast(-333333333333 as bigint))

_assert_(cast(-1000000000000 as bigint) / cast(-3 as bigint) = cast(333333333333 as bigint))

_assert_(cast(-100 as bigint) / cast(-1000000000000 as bigint) = cast(0 as bigint))

_assert_(cast(0 as bigint) / cast(-3 as bigint) = cast(0 as bigint))

_assert_(@BIGINT_MAX / cast(-1 as bigint) = -@BIGINT_MAX)

_assert_(@BIGINT_MIN / @BIGINT_MAX = cast(-1 as bigint))

_assert_(@BIGINT_MIN / @BIGINT_MIN = cast(1 as bigint))

_assert_(@BIGINT_MAX / @BIGINT_MIN = cast(0 as bigint))

_assert_(cast(1000 as bigint) / null is null)
_assert_(null / cast(1000 as bigint) is null)
_assert_(cast(null as bigint) / null is null)


-- ========== BIGINT divide max overflow ============

_assert_error_(@BIGINT_MIN / cast(-1 as bigint), 'BIGINT_OVERFLOW')


-- ========== BIGINT divide by zero ============

_assert_error_(cast(1 as bigint)/cast(0 as bigint), 'DIVIDE_BY_ZERO')


-- ========== BIGINT modulo ============

_assert_(cast(1000 as bigint) % cast(3 as bigint) = cast(1 as bigint))

_assert_(cast(1000 as bigint) % cast(-3 as bigint) = cast(1 as bigint))

_assert_(cast(-1000 as bigint) % cast(3 as bigint) = cast(-1 as bigint))

_assert_(cast(-1000 as bigint) % cast(-3 as bigint) = cast(-1 as bigint))

_assert_(cast(0 as bigint) % cast(-3 as bigint) = cast(0 as bigint))

_assert_(@BIGINT_MAX % cast(-1 as bigint) = cast(0 as bigint))

_assert_(@BIGINT_MAX % cast(2 as bigint) = cast(1 as bigint))

_assert_(@BIGINT_MIN % cast(2 as bigint) = cast(0 as bigint))

_assert_(@BIGINT_MIN % @BIGINT_MAX = cast(-1 as bigint))

_assert_(@BIGINT_MAX % @BIGINT_MIN = @BIGINT_MAX)

_assert_(-@BIGINT_MAX % @BIGINT_MIN = -@BIGINT_MAX)

_assert_error_(@BIGINT_MIN % cast(-1 as bigint), 'BIGINT_OVERFLOW')

_assert_(cast(1000 as bigint) % null is null)
_assert_(null % cast(1000 as bigint) is null)
_assert_(cast(null as bigint) % null is null)


-- ========== BIGINT modulo by zero ============

_assert_error_(cast(1 as bigint) % cast(0 as bigint), 'MODULO_BY_ZERO')


-- ========== BIGINT binary op ============

_assert_(~cast(123456789012345 as bigint) = cast(-123456789012346 as bigint))

_assert_(cast(123456789012345 as bigint) & cast(523536475890392765 as bigint) = cast(123147450326585 as bigint))

_assert_(cast(123456789012345 as bigint) | cast(523536475890392765 as bigint) = cast(523536785229078525 as bigint))

_assert_(cast(123456789012345 as bigint) ^ cast(523536475890392765 as bigint) = cast(523413637778751940 as bigint))

_assert_(cast(123456789012345 as bigint) | cast(523536475890392765 as bigint) & cast(13368647362579 as bigint) ^ ~cast(6381958847700321 as bigint) = cast(-6373264230094193 as bigint))

_assert_(~cast(null as bigint) is null)

_assert_(cast(1000 as bigint) & null is null)
_assert_(null & cast(1000 as bigint) is null)
_assert_(cast(null as bigint) & null is null)

_assert_(cast(1000 as bigint) | null is null)
_assert_(null | cast(1000 as bigint) is null)
_assert_(cast(null as bigint) | null is null)

_assert_(cast(1000 as bigint) ^ null is null)
_assert_(null ^ cast(1000 as bigint) is null)
_assert_(cast(null as bigint) ^ null is null)


-- ========== BIGINT comp op ============

_assert_(cast(12345678901234122 as bigint) = cast(12345678901234122 as bigint))

_assert_(not (cast(12345678901234122 as bigint) = cast(12345678901234123 as bigint)))

_assert_(not cast(12345678901234122 as bigint) = cast(12345678901234123 as bigint))

_assert_null_( cast(1000 as bigint) = null)
_assert_null_( null = cast(1000 as bigint))
_assert_null_( cast(null as bigint) = null)


_assert_(not (cast(12345678901234122 as bigint) != cast(12345678901234122 as bigint)))

_assert_(cast(12345678901234122 as bigint) != cast(12345678901234123 as bigint))

_assert_null_( cast(1000 as bigint) != null)
_assert_null_( null != cast(1000 as bigint))
_assert_null_( cast(null as bigint) != null)


_assert_(cast(12345678901234123 as bigint) > cast(12345678901234122 as bigint))

_assert_(not (cast(12345678901234122 as bigint) > cast(12345678901234122 as bigint)))

_assert_(cast(-12345678901234121 as bigint) > cast(-12345678901234122 as bigint))

_assert_(not (cast(-12345678901234122 as bigint) > cast(-12345678901234122 as bigint)))

_assert_(cast(1 as bigint) > cast(-1 as bigint))

_assert_(not (cast(-1 as bigint) > cast(1 as bigint)))

_assert_null_( cast(1000 as bigint) > null)
_assert_null_( null > cast(1000 as bigint))
_assert_null_( cast(null as bigint) > null)


_assert_(cast(12345678901234123 as bigint) >= cast(12345678901234122 as bigint))

_assert_(cast(12345678901234122 as bigint) >= cast(12345678901234122 as bigint))

_assert_(not (cast(12345678901234121 as bigint) >= cast(12345678901234122 as bigint)))

_assert_(cast(-12345678901234121 as bigint) >= cast(-12345678901234122 as bigint))

_assert_(cast(-12345678901234122 as bigint) >= cast(-12345678901234122 as bigint))

_assert_(not (cast(-12345678901234123 as bigint) >= cast(-12345678901234122 as bigint)))

_assert_(cast(1 as bigint) >= cast(-1 as bigint))

_assert_(not (cast(-1 as bigint) >= cast(1 as bigint)))

_assert_null_( cast(1000 as bigint) >= null)
_assert_null_( null >= cast(1000 as bigint))
_assert_null_( cast(null as bigint) >= null)


_assert_(cast(12345678901234121 as bigint) < cast(12345678901234122 as bigint))

_assert_(not (cast(12345678901234122 as bigint) < cast(12345678901234122 as bigint)))

_assert_(cast(-12345678901234123 as bigint) < cast(-12345678901234122 as bigint))

_assert_(not (cast(-12345678901234122 as bigint) < cast(-12345678901234122 as bigint)))

_assert_(not (cast(1 as bigint) < cast(-1 as bigint)))

_assert_(cast(-1 as bigint) < cast(1 as bigint))

_assert_null_( cast(1000 as bigint) < null)
_assert_null_( null < cast(1000 as bigint))
_assert_null_( cast(null as bigint) < null)


_assert_(cast(12345678901234121 as bigint) <= cast(12345678901234122 as bigint))

_assert_(cast(12345678901234122 as bigint) <= cast(12345678901234122 as bigint))

_assert_(not (cast(12345678901234123 as bigint) <= cast(12345678901234122 as bigint)))

_assert_(cast(-12345678901234123 as bigint) <= cast(-12345678901234122 as bigint))

_assert_(cast(-12345678901234122 as bigint) <= cast(-12345678901234122 as bigint))

_assert_(not (cast(-12345678901234121 as bigint) <= cast(-12345678901234122 as bigint)))

_assert_(not (cast(1 as bigint) <= cast(-1 as bigint)))

_assert_(cast(-1 as bigint) <= cast(1 as bigint))

_assert_null_( cast(1000 as bigint) <= null)
_assert_null_( null <= cast(1000 as bigint))
_assert_null_( cast(null as bigint) <= null)


-- ========== BIGINT is [not] null ============

_assert_(cast(NULL as bigint) is null)

_assert_(not cast(123 as bigint) is null)


_assert_(not cast(NULL as bigint) is not null)

_assert_(cast(123 as bigint) is not null)


-- ========== BIGINT [not] in list ============

_assert_(cast(12345678901234122 as bigint) in (cast(12345678901234122 as bigint)))

_assert_(cast(12345678901234122 as bigint) in (cast(0 as bigint), cast(12345678901234122 as bigint)))

_assert_(cast(12345678901234122 as bigint) in (cast(0 as bigint), cast(12345678901234122 as bigint), cast(9 as bigint)))

_assert_(cast(12345678901234122 as bigint) in (cast(0 as bigint), NULL, cast(12345678901234122 as bigint), cast(9 as bigint)))

_assert_(cast(12345678901234122 as bigint) in (cast(0 as bigint), cast(12345678901234122 as bigint), NULL, cast(9 as bigint)))


_assert_(not cast(12345678901234122 as bigint) in (cast(0 as bigint), cast(1 as bigint), cast(9 as bigint)))

_assert_null_(cast(12345678901234122 as bigint) in (cast(0 as bigint), cast(1 as bigint), NULL, cast(9 as bigint)))


_assert_(not cast(12345678901234122 as bigint) not in (cast(12345678901234122 as bigint)))

_assert_(not cast(12345678901234122 as bigint) not in (cast(0 as bigint), cast(12345678901234122 as bigint)))

_assert_(not cast(12345678901234122 as bigint) not in (cast(0 as bigint), cast(12345678901234122 as bigint), cast(9 as bigint)))

_assert_(not cast(12345678901234122 as bigint) not in (cast(0 as bigint), NULL, cast(12345678901234122 as bigint), cast(9 as bigint)))

_assert_(not cast(12345678901234122 as bigint) not in (cast(0 as bigint), cast(12345678901234122 as bigint), NULL, cast(9 as bigint)))


_assert_(cast(12345678901234122 as bigint) not in (cast(0 as bigint), cast(1 as bigint), cast(9 as bigint)))

_assert_null_(cast(12345678901234122 as bigint) not in (cast(0 as bigint), cast(1 as bigint), NULL, cast(9 as bigint)))


-- ========== BIGINT cast ============

_assert_(cast(cast(123 as bigint) as char(10)) + 'x' = '123       x')
_assert_(cast(cast(123 as bigint) as char) = '123')
_assert_(cast(cast(123 as bigint) as char(3)) = '123')
_assert_(cast(cast(-123 as bigint) as char(4)) = '-123')
_assert_(cast(@BIGINT_MAX as char) = '9223372036854775807')
_assert_(cast(@BIGINT_MIN as char) = '-9223372036854775808')
_assert_(cast(cast(null as bigint) as char) is null)

_assert_(cast(cast(123 as bigint) as varchar(10)) + 'x' = '123x')
_assert_(cast(cast(123 as bigint) as varchar) = '123')
_assert_(cast(cast(123 as bigint) as varchar(3)) = '123')
_assert_(cast(cast(-123 as bigint) as varchar(4)) = '-123')
_assert_(cast(@BIGINT_MAX as varchar) = '9223372036854775807')
_assert_(cast(@BIGINT_MIN as varchar) = '-9223372036854775808')
_assert_(cast(cast(null as bigint) as varchar) is null)

_assert_(cast(cast(9223372036854775807 as bigint) as bit) = 1)
_assert_(cast(cast(-9223372036854775808 as bigint) as bit) = 1)
_assert_(cast(cast(-123 as bigint) as bit) = 1)
_assert_(cast(cast(0 as bigint) as bit) = 0)
_assert_(cast(cast(null as bigint) as bit) is null)

_assert_(cast(cast(255 as bigint) as tinyint) = 255)
_assert_(cast(cast(0 as bigint) as tinyint) = 0)
_assert_(cast(cast(null as bigint) as tinyint) is null)

_assert_(cast(cast(32767 as bigint) as smallint) = 32767)
_assert_(cast(cast(-32768 as bigint) as smallint) = -32768)
_assert_(cast(cast(null as bigint) as smallint) is null)

_assert_(cast(cast(@SMALLINT_MAX as bigint) as smallint) = @SMALLINT_MAX)
_assert_(cast(cast(@SMALLINT_MIN as bigint) as smallint) = @SMALLINT_MIN)
_assert_(cast(cast(null as bigint) as smallint) is null)

_assert_(cast(cast(@INT_MAX as bigint) as int) = @INT_MAX)
_assert_(cast(cast(@INT_MIN as bigint) as int) = @INT_MIN)
_assert_(cast(cast(null as bigint) as int) is null)

_assert_(cast(cast(@BIGINT_MAX as bigint) as bigint) = @BIGINT_MAX)
_assert_(cast(cast(@BIGINT_MIN as bigint) as bigint) = @BIGINT_MIN)
_assert_(cast(cast(null as bigint) as bigint) is null)

_assert_(cast(999999999999999.9999 as money) = $999999999999999.9999)
_assert_(cast(-999999999999999.9999 as money) = -999999999999999.9999)
_assert_(cast(cast(null as bigint) as money) is null)

_assert_(cast(@BIGINT_MAX as numeric(21,2)) = @BIGINT_MAX)
_assert_(cast(@BIGINT_MIN as numeric(21,2)) = @BIGINT_MIN)
_assert_(cast(cast(null as bigint) as numeric(21,2)) is null)

_assert_(cast(cast(12345 as bigint) as numeric(5)) = 12345)
_assert_(cast(cast(-12345 as bigint) as numeric(5)) = -12345)
_assert_(cast(cast(null as bigint) as numeric(5)) is null)

_assert_(str(cast(@BIGINT_MAX as float), 19) = '9223372036854775808')
_assert_(str(cast(@BIGINT_MIN as float), 20) = '-9223372036854775808')
_assert_(cast(cast(null as smallint) as float) is null)

_assert_(cast(cast(@INT_MAX_DATETIME as bigint) as datetime) = '99991231')
_assert_(cast(cast(0 as bigint) as datetime) = '19000101')
_assert_(cast(cast(@INT_MIN_DATETIME as bigint) as datetime) = '00010101')


-- ========== BIGINT overflow ============

_assert_error_(cast('9223372036854775808' as bigint), 'VARCHAR_CAST_BIGINT_BAD_STRING')

-- ========== BIGINT overflow ============

_assert_error_(cast('-9223372036854775809' as bigint), 'VARCHAR_CAST_BIGINT_BAD_STRING')


-- ========== BIGINT cast overflow ============

_assert_error_(cast(cast(123 as bigint) as char(2)), 'BIGINT_CAST_VARCHAR_OVERFLOW')


-- ========== BIGINT cast overflow ============

_assert_error_(cast(cast(-123 as bigint) as char(3)), 'BIGINT_CAST_VARCHAR_OVERFLOW')


-- ========== BIGINT cast overflow ============

_assert_error_(cast(cast(123 as bigint) as varchar(2)), 'BIGINT_CAST_VARCHAR_OVERFLOW')


-- ========== BIGINT cast overflow ============

_assert_error_(cast(cast(-123 as bigint) as varchar(3)), 'BIGINT_CAST_VARCHAR_OVERFLOW')


-- ========== BIGINT cast overflow ============

declare @bb bigint = 256

_assert_error_(cast(@bb as tinyint), 'BIGINT_CAST_TINYINT_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = -1

_assert_error_(cast(@bb as tinyint), 'BIGINT_CAST_TINYINT_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = 32768

_assert_error_(cast(@bb as smallint), 'BIGINT_CAST_SMALLINT_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = -32769

_assert_error_(cast(@bb as smallint), 'BIGINT_CAST_SMALLINT_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = 2147483648

_assert_error_(cast(@bb as int), 'BIGINT_CAST_INT_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = -2147483649

_assert_error_(cast(@bb as int), 'BIGINT_CAST_INT_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = 12345

_assert_error_(cast(@bb as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = -12345

_assert_error_(cast(@bb as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = @INT_MAX_DATETIME

_assert_error_(cast(@bb + 1 as datetime), 'DATETIME_OVERFLOW')


-- ========== BIGINT cast overflow ============

set @bb = @INT_MIN_DATETIME

_assert_error_(cast(@bb - 1 as datetime), 'DATETIME_OVERFLOW')


-- ========== BIGINT case when ============

_assert_(case when 1=1 then cast(1000000000000 as bigint) end = cast(1000000000000 as bigint))
_assert_(case when 1=4 then cast(1000000000000 as bigint) end is null)
_assert_(case when 1=1 then cast(1000000000000 as bigint) when 1=2 then cast(2200000000000 as bigint) else cast(330000000000 as bigint) end = cast(1000000000000 as bigint))
_assert_(case when 1=1 then cast(1000000000000 as bigint) when 1=2 then cast(2200000000000 as bigint) end = cast(1000000000000 as bigint))
_assert_(case when 1=4 then cast(1000000000000 as bigint) when 2=2 then cast(2200000000000 as bigint) else cast(330000000000 as bigint) end = cast(2200000000000 as bigint))
_assert_(case when 1=NULL then cast(1000000000000 as bigint) when 2=2 then cast(2200000000000 as bigint) else cast(330000000000 as bigint) end = cast(2200000000000 as bigint))
_assert_(case when 1=NULL then cast(1000000000000 as bigint) when 2=NULL then cast(550000000000 as bigint) else cast(330000000000 as bigint) end = cast(330000000000 as bigint))
_assert_(case when 1=NULL then cast(1000000000000 as bigint) when 2=NULL then cast(2200000000000 as bigint) end is null)




