

set language french

declare @TINYINT_MAX tinyint =  255
declare @TINYINT_MIN tinyint =    0

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @a0 bit = 0
declare @b0 bit = 0
declare @a1 bit = 1
declare @b1 bit = 1


-- ========== BIT binary op ============

_assert_(null & @a0 is null)
_assert_(@a0 & null is null)
_assert_(cast(null as bit) & null is null)
_assert_(@a0 & @a0 = @a0)
_assert_(@a0 & @a1 = @a0)
_assert_(@a1 & @a0 = @a0)
_assert_(@a1 & @a1 = @a1)
_assert_(@a1 & @a1 = @b1)

_assert_(null | @a0 is null)
_assert_(@a0 | null is null)
_assert_(cast(null as bit) | null is null)
_assert_(@a0 | @a0 = @a0)
_assert_(@a0 | @a1 = @a1)
_assert_(@a1 | @a0 = @a1)
_assert_(@a1 | @a1 = @a1)
_assert_(@a1 | @a1 = @b1)

_assert_(null ^ @a0 is null)
_assert_(@a0 ^ null is null)
_assert_(cast(null as bit) ^ null is null)
_assert_(@a0 ^ @a0 = @a0)
_assert_(@a0 ^ @a1 = @a1)
_assert_(@a1 ^ @a0 = @a1)
_assert_(@a1 ^ @a1 = @a0)
_assert_(@a1 ^ @a1 = @b0)

_assert_(~cast(null as bit) is null)
_assert_(~@a0 = @a1)
_assert_(~@a1 = @a0)


-- ========== BIT comp op ============

_assert_(cast(122 as bit) = cast(122 as bit))

_assert_(cast(122 as bit) = cast(123 as bit))

_assert_(not cast(122 as bit) = cast(0 as bit))

_assert_(@a0 = @a0)
_assert_(not @a0 = @a1)

_assert_null_( @a0 = null)
_assert_null_( null = @a0)
_assert_null_( cast(null as bit) = null)


_assert_(not @a0 > @a0)
_assert_(not @a0 > @a1)
_assert_(@a1 > @a0)

_assert_null_( @a0 > null)
_assert_null_( null > @a0)
_assert_null_( cast(null as bit) > null)


_assert_(not @a0 < @a0)
_assert_(@a0 < @a1)
_assert_(not @a1 < @a0)

_assert_null_( @a0 < null)
_assert_null_( null < @a0)
_assert_null_( cast(null as bit) < null)


_assert_(@a0 >= @a0)
_assert_(not @a0 >= @a1)
_assert_(@a1 >= @a0)

_assert_null_( @a0 >= null)
_assert_null_( null >= @a0)
_assert_null_( cast(null as bit) >= null)


_assert_(not @a0 != @a0)
_assert_(not @a1 != @a1)
_assert_(@a0 != @a1)
_assert_(@a1 != @a0)

_assert_null_( @a0 != null)
_assert_null_( null != @a0)
_assert_null_( cast(null as bit) != null)


-- ========== BIT is [not] null ============

_assert_(cast(NULL as bit) is null)

_assert_(not cast(123 as bit) is null)


_assert_(not cast(NULL as bit) is not null)

_assert_(cast(123 as bit) is not null)
_assert_(cast(0 as bit) is not null)


-- ========== BIT [not] in list ============

_assert_(cast(12 as bit) in (cast(12 as bit)))

_assert_(cast(12 as bit) in (cast(0 as bit), cast(12 as bit)))

_assert_(cast(12 as bit) in (cast(0 as bit), cast(12 as bit), cast(9 as bit)))

_assert_(cast(12 as bit) in (cast(0 as bit), NULL, cast(12 as bit), cast(9 as bit)))

_assert_(cast(12 as bit) in (cast(0 as bit), cast(12 as bit), NULL, cast(9 as bit)))


_assert_(not cast(0 as bit) in (cast(12 as bit)))

_assert_(cast(0 as bit) in (cast(0 as bit), cast(12 as bit)))

_assert_(cast(0 as bit) in (cast(0 as bit), cast(12 as bit), cast(9 as bit)))

_assert_(cast(0 as bit) in (cast(0 as bit), NULL, cast(12 as bit), cast(9 as bit)))

_assert_(cast(0 as bit) in (cast(80 as bit), cast(12 as bit), NULL, cast(0 as bit)))



_assert_(cast(12 as bit) in (cast(0 as bit), cast(1 as bit), cast(9 as bit)))

_assert_null_(cast(12 as bit) in (cast(0 as bit), cast(0 as bit), NULL, cast(0 as bit)))


_assert_(cast(12 as bit) not in (cast(0 as bit)))

_assert_(not cast(12 as bit) not in (cast(0 as bit), cast(1 as bit)))

_assert_(not cast(12 as bit) not in (cast(0 as bit), cast(128 as bit), cast(9 as bit)))

_assert_(not cast(12 as bit) not in (cast(0 as bit), NULL, cast(128 as bit), cast(9 as bit)))

_assert_(not cast(12 as bit) not in (cast(0 as bit), cast(12 as bit), NULL, cast(9 as bit)))


_assert_(cast(12 as bit) not in (cast(0 as bit), cast(0 as bit), cast(0 as bit)))

_assert_null_(cast(12 as bit) not in (cast(0 as bit), cast(0 as bit), NULL, cast(0 as bit)))


-- ========== BIT cast ============

_assert_(cast(+cast(123 as bit) as char) = '1')
_assert_(cast(++++cast(-123 as bit) as char) = '1')

_assert_(cast(cast(123 as bit) as char(10)) + 'x' = '1         x')
_assert_(cast(cast(123 as bit) as char) = '1')
_assert_(cast(cast(123 as bit) as char(3)) = '1')
_assert_(cast(cast(123 as bit) as char(1)) = '1       ')

_assert_(cast(cast(0 as bit) as char(10)) + 'x' = '0         x')
_assert_(cast(cast(0 as bit) as char) = '0')
_assert_(cast(cast(0 as bit) as char(3)) = '0')
_assert_(cast(cast(null as bit) as char) is null)

_assert_(cast(cast(123 as bit) as varchar(10)) + 'x' = '1x')
_assert_(cast(cast(123 as bit) as varchar) = '1')
_assert_(cast(cast(123 as bit) as varchar(3)) = '1')
_assert_(cast(cast(123 as bit) as varchar(3)) = '1       ')

_assert_(cast(cast(0 as bit) as varchar(10)) + 'x' = '0x')
_assert_(cast(cast(0 as bit) as varchar) = '0')
_assert_(cast(cast(0 as bit) as varchar(3)) = '0')
_assert_(cast(cast(null as bit) as varchar) is null)

_assert_(cast(cast(123 as bit) as bit) = 1)
_assert_(cast(cast(0 as bit) as bit) = 0)
_assert_(cast(cast(null as bit) as bit) is null)

_assert_(cast(cast(255 as bit) as tinyint) = 1)
_assert_(cast(cast(0 as bit) as tinyint) = 0)
_assert_(cast(cast(null as bit) as tinyint) is null)

_assert_(cast(cast(255 as bit) as smallint) = 1)
_assert_(cast(cast(0 as bit) as smallint) = 0)
_assert_(cast(cast(null as bit) as smallint) is null)

_assert_(cast(cast(255 as bit) as int) = 1)
_assert_(cast(cast(0 as bit) as int) = 0)
_assert_(cast(cast(null as bit) as int) is null)

_assert_(cast(cast(255 as bit) as bigint) = 1)
_assert_(cast(cast(0 as bit) as bigint) = 0)
_assert_(cast(cast(null as bit) as bigint) is null)

_assert_(cast(cast(255 as bit) as money) = 1)
_assert_(cast(cast(0 as bit) as money) = 0)
_assert_(cast(cast(null as bit) as money) is null)

_assert_(cast(cast(255 as bit) as numeric(12,2)) = 1)
_assert_(cast(cast(0 as bit) as numeric(12,2)) = 0)
_assert_(cast(cast(null as bit) as numeric(12,2)) is null)

_assert_(cast(cast(255 as bit) as numeric(3)) = 1)
_assert_(cast(cast(0 as bit) as numeric(1)) = 0)
_assert_(cast(cast(null as bit) as numeric(5)) is null)

_assert_(cast(cast(255 as bit) as float) = 1)
_assert_(cast(cast(0 as bit) as float) = 0)
_assert_(cast(cast(null as bit) as float) is null)


-- ========== BIT case when ============

_assert_(case when 1=1 then cast(1 as bit) end = cast(1 as bit))
_assert_(case when 1=4 then cast(1 as bit) end is null)
_assert_(case when 1=1 then cast(1 as bit) when 1=2 then cast(0 as bit) else cast(0 as bit) end = cast(1 as bit))
_assert_(case when 1=1 then cast(1 as bit) when 1=2 then cast(0 as bit) end = cast(1 as bit))
_assert_(case when 1=4 then cast(1 as bit) when 2=2 then cast(0 as bit) else cast(1 as bit) end = cast(0 as bit))
_assert_(case when 1=NULL then cast(1 as bit) when 2=2 then cast(0 as bit) else cast(1 as bit) end = cast(0 as bit))
_assert_(case when 1=NULL then cast(1 as bit) when 2=NULL then cast(1 as bit) else cast(0 as bit) end = cast(0 as bit))
_assert_(case when 1=NULL then cast(1 as bit) when 2=NULL then cast(0 as bit) end is null)








