
set language french

declare @FLOAT_MAX float =  1.79e308f
declare @FLOAT_MIN float = -1.79e308f

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648


-- ========== TEST ============

_assert_(1=1)



-- ========== FLOAT miscellaneous ============


_assert_(@FLOAT_MAX = -@FLOAT_MIN)

_assert_(@FLOAT_MIN = -@FLOAT_MAX)


_assert_(10000.5f + 300f = 10300.5f)

_assert_(@FLOAT_MAX * 1 = @FLOAT_MAX)

_assert_(1234.5f * -1f - 1f = -1235.5f)


-- ========== FLOAT unary minus ============

_assert_(++++4.0f = 4f)
_assert_(-4f = -1f * 4f)
_assert_(- -4f = 4f)
_assert_(++- - -++4f = -4f)
_assert_(++4f = 4f)

_assert_(-cast(NULL as float) is null)


-- ========== FLOAT add ============

_assert_(1000f + 150f = 1150f)

_assert_(-1000.5f + -150.5f = -1151f)

_assert_(1000f + null is null)
_assert_(null + 1000f is null)
_assert_(cast(null as float) + null is null)


-- ========== FLOAT add max overflow ============

_assert_error_(@FLOAT_MAX + 1e306f, 'FLOAT_INFINITE')


-- ========== FLOAT add max overflow ============

_assert_error_(@FLOAT_MAX + @FLOAT_MAX, 'FLOAT_INFINITE')


-- ========== FLOAT add min overflow ============

_assert_error_(@FLOAT_MIN + -1e306f, 'FLOAT_INFINITE')


-- ========== FLOAT subtract ============

_assert_(1000.5f - -150.5f = 1151f)

_assert_(-1000f - 150f = -1150f)

_assert_(1000f - null is null)
_assert_(null - 1000f is null)
_assert_(cast(null as float) - null is null)


-- ========== FLOAT subtract max overflow ============

_assert_error_(@FLOAT_MAX - -1e306f, 'FLOAT_INFINITE')


-- ========== FLOAT subtract max overflow ============

_assert_error_(@FLOAT_MAX - @FLOAT_MIN, 'FLOAT_INFINITE')


-- ========== FLOAT subtract min overflow ============

_assert_error_(@FLOAT_MIN - 1e306f, 'FLOAT_INFINITE')


-- ========== FLOAT subtract min overflow ============

_assert_error_(@FLOAT_MIN - @FLOAT_MAX, 'FLOAT_INFINITE')


-- ========== FLOAT multiply ============

_assert_(1000f * -150f = -150000f)

_assert_(-1000f * 150f = -150000f)

_assert_(1000f * null is null)
_assert_(null * 1000f is null)
_assert_(cast(null as float) * null is null)

_assert_(1000f / 2f * 2f = 1000f)


-- ========== FLOAT multiply max overflow ============

_assert_error_(@FLOAT_MAX * 2f, 'FLOAT_INFINITE')


-- ========== FLOAT multiply max overflow ============

_assert_error_(@FLOAT_MIN / -2f * 3f, 'FLOAT_INFINITE')


-- ========== FLOAT multiply max overflow ============

_assert_error_(@FLOAT_MAX * @FLOAT_MAX, 'FLOAT_INFINITE')


-- ========== FLOAT multiply min overflow ============

_assert_error_(@FLOAT_MIN * 2f, 'FLOAT_INFINITE')


-- ========== FLOAT divide ============

_assert_(1000f / 2f = 500f)

_assert_(0f / -3f = 0f)

_assert_(@FLOAT_MAX / -1f = -@FLOAT_MAX)

_assert_(1000f / null is null)
_assert_(null / 1000f is null)
_assert_(cast(null as float) / null is null)


-- ========== FLOAT divide max overflow ============

_assert_error_(@FLOAT_MAX / 0.1f, 'FLOAT_INFINITE')


-- ========== FLOAT divide by zero ============

_assert_error_(1f/0f, 'FLOAT_DIVIDE_BY_ZERO')


-- ========== FLOAT divide by zero ============

_assert_error_(0f/0f, 'FLOAT_DIVIDE_BY_ZERO')


-- ========== FLOAT comp op ============

_assert_(122f = 122f)

_assert_(not (122f = 123f))

_assert_(not 122f = 123f)

_assert_null_( 1000f = null)
_assert_null_( null = 1000f)
_assert_null_( cast(null as float) = null)


_assert_(not (122f != 122f))

_assert_(122f != 123f)

_assert_null_( 1000f != null)
_assert_null_( null != 1000f)
_assert_null_( cast(null as float) != null)


_assert_(123f > 122f)

_assert_(not (122f > 122f))

_assert_(-121f > -122f)

_assert_(not (-122f > -122f))

_assert_(1f > -1f)

_assert_(not (-1f > 1f))

_assert_null_( 1000f > null)
_assert_null_( null > 1000f)
_assert_null_( cast(null as float) > null)


_assert_(123f >= 122f)

_assert_(122f >= 122f)

_assert_(not (121f >= 122f))

_assert_(-121f >= -122f)

_assert_(-122f >= -122f)

_assert_(not (-123f >= -122f))

_assert_(1f >= -1f)

_assert_(not (-1f >= 1f))

_assert_null_( 1000f >= null)
_assert_null_( null >= 1000f)
_assert_null_( cast(null as float) >= null)


_assert_(121f < 122f)

_assert_(not (122f < 122f))

_assert_(-123f < -122f)

_assert_(not (-122f < -122f))

_assert_(not (1f < -1f))

_assert_(-1f < 1f)

_assert_null_( 1000f < null)
_assert_null_( null < 1000f)
_assert_null_( cast(null as float) < null)


_assert_(121f <= 122f)

_assert_(122f <= 122f)

_assert_(not (123f <= 122f))

_assert_(-123f <= -122f)

_assert_(-122f <= -122f)

_assert_(not (-121f <= -122f))

_assert_(not (1f <= -1f))

_assert_(-1f <= 1f)

_assert_null_( 1000f <= null)
_assert_null_( null <= 1000f)
_assert_null_( cast(null as float) <= null)


-- ========== FLOAT is [not] null ============

_assert_(cast(NULL as float) is null)

_assert_(not 123f is null)


_assert_(not cast(NULL as float) is not null)

_assert_(123f is not null)


-- ========== FLOAT [not] in list ============

_assert_(12f in (12f))

_assert_(12f in (0f, 12f))

_assert_(12f in (0f, 12f, 9f))

_assert_(12f in (0f, NULL, 12f, 9f))

_assert_(12f in (0f, 12f, NULL, 9f))


_assert_(not 12f in (0f, 1f, 9f))

_assert_null_(12f in (0f, 1f, NULL, 9f))


_assert_(not 12f not in (12f))

_assert_(not 12f not in (0f, 12f))

_assert_(not 12f not in (0f, 12f, 9f))

_assert_(not 12f not in (0f, NULL, 12f, 9f))

_assert_(not 12f not in (0f, 12f, NULL, 9f))


_assert_(12f not in (0f, 1f, 9f))

_assert_null_(12f not in (0f, 1f, NULL, 9f))


-- ========== FLOAT cast ============

_assert_(cast(123.45f as char(10)) + 'x'      = '123.45    x')
_assert_(cast(123.45e100f as char(15)) + 'x'  = '1.2345e+102    x')
_assert_(cast(123f as char) = '123')
_assert_(cast(123f as char(3)) = '123')
_assert_(cast(-123f as char(4)) = '-123')
_assert_(cast(@FLOAT_MAX as char) = '1.79e+308')
_assert_(cast(@FLOAT_MIN as char) = '-1.79e+308')
_assert_(cast(cast(null as float) as char) is null)

_assert_(cast(123.45f as varchar(10)) + 'x'      = '123.45x')
_assert_(cast(123.45e100f as varchar(11)) + 'x'  = '1.2345e+102x')
_assert_(cast(123f as varchar) = '123')
_assert_(cast(123f as varchar(3)) = '123')
_assert_(cast(-123f as varchar(4)) = '-123')
_assert_(cast(@FLOAT_MAX as varchar) = '1.79e+308')
_assert_(cast(@FLOAT_MIN as varchar) = '-1.79e+308')
_assert_(cast(cast(null as float) as varchar) is null)

_assert_(cast(123f as bit) = 1)
_assert_(cast(-123f as bit) = 1)
_assert_(cast(0f as bit) = 0)
_assert_(cast(cast(null as float) as bit) is null)

_assert_(cast(255f as tinyint) = 255)
_assert_error_(cast(255.9f as tinyint), 'FLOAT_CAST_TINYINT_OVERFLOW')
_assert_(cast(0f as tinyint) = 0)
_assert_(cast(cast(null as float) as tinyint) is null)

_assert_(cast(32767f as smallint) = 32767)
_assert_error_(cast(32767.99f as smallint), 'FLOAT_CAST_SMALLINT_OVERFLOW')
_assert_(cast(1000.9f as smallint) = 1000)
_assert_(cast(-1000.9f as smallint) = -1000)
_assert_(cast(-32768f as smallint) = -32768)
_assert_error_(cast(-32768.99f as smallint), 'FLOAT_CAST_SMALLINT_OVERFLOW')
_assert_(cast(cast(null as float) as smallint) is null)

_assert_error_(cast(2147483647.99f as int), 'FLOAT_CAST_INT_OVERFLOW')
_assert_(cast(2147483647f as int) = 2147483647)
_assert_(cast(123456f as int) = 123456)
_assert_(cast(10000.9f as int) = 10000)
_assert_(cast(-10000.9f as int) = -10000)
_assert_(cast(-123456f as int) = -123456)
_assert_(cast(-2147483648f as int) = -2147483648)
_assert_error_(cast(-2147483648.9999f as int), 'FLOAT_CAST_INT_OVERFLOW')
_assert_(cast(cast(null as float) as int) is null)

_assert_(cast(2147483647f as int) = 2147483647)
_assert_(cast(-2147483648f as int) = -2147483648)

_assert_(cast(1234567890123.25f as bigint) = cast(1234567890123 as bigint))
_assert_(cast(10000000000.9f as bigint) = cast(10000000000 as bigint))
_assert_(cast(-10000000000.9f as bigint) = cast(-10000000000 as bigint))
_assert_(cast(-1234567890123.25f as bigint) = cast(-1234567890123 as bigint))
_assert_(cast(9223372036854000000f as bigint) = 9223372036853999616)
_assert_(cast(9223372036854774784f as bigint) = 9223372036854774784)
_assert_(cast(cast(null as float) as bigint) is null)

_assert_(cast(12345.25f as money) = $12345.25)
_assert_(cast(-12345.25f as money) = $-12345.25)
_assert_(cast(cast(null as float) as money) is null)

_assert_(cast(12345.25f as numeric(12,2)) = 12345.25f)
_assert_(cast(-12345.25f as numeric(12,2)) = -12345.25f)
_assert_(cast(cast(null as float) as numeric(12,2)) is null)

_assert_(cast(12345.3f as numeric(5)) = 12345)
_assert_(cast(-12345.3f as numeric(5)) = -12345)
_assert_(cast(12345.6f as numeric(5)) = 12346)
_assert_(cast(-12345.6f as numeric(5)) = -12346)
_assert_(cast(cast(null as float) as numeric(5)) is null)

_assert_(cast(@FLOAT_MAX as float) = @FLOAT_MAX)
_assert_(cast(@FLOAT_MIN as float) = @FLOAT_MIN)
_assert_(cast(cast(null as float) as float) is null)


-- ========== FLOAT 2 ============

declare @at float = 123.456789e6

set @at = 123.456e200
_assert_(CAST(@at as varchar(130)) = '1.23456e+202')

set @at = 123.456789012345e0
_assert_(CAST(@at as varchar(130)) = '123.457')

set @at = 123.456e0
_assert_(CAST(@at as varchar(130)) = '123.456')

set @at = 123e0
_assert_(CAST(@at as varchar(130)) = '123')

set @at = 123e-4
_assert_(CAST(@at as varchar(130)) = '0.0123')

set @at = 123.45678901234e-4
_assert_(CAST(@at as varchar(130)) = '0.0123457')

set @at = 123.45678901234e-200
_assert_(CAST(@at as varchar(130)) = '1.23457e-198')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(123f as char(2)), 'FLOAT_CAST_VARCHAR_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-123f as char(3)), 'FLOAT_CAST_VARCHAR_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(123f as varchar(2)), 'FLOAT_CAST_VARCHAR_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-123f as varchar(3)), 'FLOAT_CAST_VARCHAR_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-0.1f as tinyint), 'FLOAT_CAST_TINYINT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(256f as tinyint), 'FLOAT_CAST_TINYINT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-1f as tinyint), 'FLOAT_CAST_TINYINT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(32768f as smallint), 'FLOAT_CAST_SMALLINT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-32769f as smallint), 'FLOAT_CAST_SMALLINT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(2147483648f as int), 'FLOAT_CAST_INT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-2147483649f as int), 'FLOAT_CAST_INT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(9223372036854775807f as bigint), 'FLOAT_CAST_BIGINT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-9223372036854775808f as bigint), 'FLOAT_CAST_BIGINT_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(922337203685477.5808e1f as money), 'NUMERIC_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(12345f as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== FLOAT cast overflow ============

_assert_error_(cast(-12345f as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== FLOAT case when ============

_assert_(case when 1=1 then 10.55f end = 10.55f)
_assert_(case when 1=4 then 10.55f end is null)
_assert_(case when 1=1 then 10.55f when 1=2 then 12345.6789f else 330000000000f end = 10.55f)
_assert_(case when 1=1 then 10.55f when 1=2 then 12345.6789f end = 10.55f)
_assert_(case when 1=4 then 10.55f when 2=2 then 12345.6789f else 330000000000f end = 12345.6789f)
_assert_(case when 1=NULL then 10.55f when 2=2 then 12345.6789f else 330000000000f end = 12345.6789f)
_assert_(case when 1=NULL then 10.55f when 2=NULL then 550000000000f else 330000000000f end = 330000000000f)
_assert_(case when 1=NULL then 10.55f when 2=NULL then 12345.6789f end is null)




