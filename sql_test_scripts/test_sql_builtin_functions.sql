

set language french

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @INT_MAX_DATETIME int = 2958463  -- 9999-12-31 0:0:0
declare @INT_MIN_DATETIME int =  -36524  -- 1800-01-01 0:0:0

declare @BIGINT_MAX bigint =  9223372036854775807
declare @BIGINT_MIN bigint = -9223372036854775808

declare @MONEY_MAX money =  $999999999999999.9999
declare @MONEY_MIN money = $-999999999999999.9999


-- ========== misc ============


declare @aqqq float = 3
declare @bqqq float
declare @cqqq float

set @bqqq = (10 + @aqqq*3)/sin(55)

set @cqqq = sin(8+(10 + @aqqq*3)/sin(55)*2/3+9)
set @bqqq = (10 + @aqqq*3)*@bqqq*@aqqq+@cqqq
set @bqqq = 77*(10 + @aqqq*3)/sin(55)+@cqqq*sin(8+(10 + @aqqq*3)/sin(55)*2/3+9)
set @cqqq = sin(8+(10 + @aqqq*3)/sin(55)*2/3+9)-(10 + @aqqq*3)*@bqqq*@aqqq+@cqqq

_assert_(@aqqq = 3f)
_assert_(abs(@bqqq - -1462.4973408325633955f) < 0.0001f)
_assert_(abs(@cqqq - 83360.4927030610560905f) < 0.0001f)


-- ========== ABS() ============

_assert_(abs(NULL) is null)

_assert_(abs(123) = 123)
_assert_(abs(-123) = 123)
_assert_(abs(0) = 0)
_assert_(abs(@INT_MAX)   = @INT_MAX)
_assert_(abs(@INT_MIN+1) = @INT_MAX)

_assert_(abs(cast(NULL as int)) is null)


_assert_(abs(cast(1230000000000 as bigint)) = cast(1230000000000 as bigint))
_assert_(abs(cast(-1230000000000 as bigint)) = cast(1230000000000 as bigint))
_assert_(abs(cast(0 as bigint)) = 0)
_assert_(abs(@BIGINT_MAX)   = @BIGINT_MAX)
_assert_(abs(@BIGINT_MIN+1) = @BIGINT_MAX)

_assert_(abs(cast(NULL as bigint)) is null)


_assert_(abs(@MONEY_MAX) = @MONEY_MAX)
_assert_(abs(@MONEY_MIN) = @MONEY_MAX)

_assert_(abs(cast(NULL as money)) is null)


_assert_(abs(123456789012345678901234567890.1234) = 123456789012345678901234567890.1234)
_assert_(abs(-123456789012345678901234567890.1234) = 123456789012345678901234567890.1234)
_assert_(abs(1234567890123456789012345678901234) = 1234567890123456789012345678901234)
_assert_(abs(-1234567890123456789012345678901234) = 1234567890123456789012345678901234)
_assert_(abs(0.0000000000) = 0)
_assert_(abs(99999999999999999999.99999999999999)   = 99999999999999999999.99999999999999)
_assert_(abs(-99999999999999999999.99999999999999)   = 99999999999999999999.99999999999999)
_assert_(abs(9999999999999999999999999999999999)   = 9999999999999999999999999999999999)
_assert_(abs(-9999999999999999999999999999999999)   = 9999999999999999999999999999999999)

_assert_(abs(cast(NULL as numeric)) is null)


_assert_(abs(1234.45e100f) = 1234.45e100f)
_assert_(abs(-1234.45e100f) =1234.45e100f)
_assert_(abs(0.0000000000e3f) = 0)

_assert_(abs(cast(NULL as float)) is null)


-- ========== ABS() overflow ============

_assert_error_(abs(@INT_MIN), 'DATA_INT_OVERFLOW')


-- ========== ABS() overflow ============

_assert_error_(abs(@BIGINT_MIN), 'DATA_BIGINT_OVERFLOW')


-- ========== CEILING() ============

_assert_(ceiling(NULL) is null)

_assert_(ceiling(123) = 123)
_assert_(ceiling(-123) = -123)
_assert_(ceiling(0) = 0)
_assert_(ceiling(@INT_MAX) = @INT_MAX)
_assert_(ceiling(@INT_MIN) = @INT_MIN)

_assert_(ceiling(cast(NULL as int)) is null)


_assert_(ceiling(cast(1230000000000 as bigint)) = cast(1230000000000 as bigint))
_assert_(ceiling(cast(-1230000000000 as bigint)) = cast(-1230000000000 as bigint))
_assert_(ceiling(cast(0 as bigint)) = 0)
_assert_(ceiling(@BIGINT_MAX) = @BIGINT_MAX)
_assert_(ceiling(@BIGINT_MIN) = @BIGINT_MIN)

_assert_(ceiling(cast(NULL as bigint)) is null)


_assert_(ceiling(@MONEY_MAX - $0.9999) = @MONEY_MAX - $0.9999)
_assert_(ceiling(@MONEY_MIN)           = @MONEY_MIN + $0.9999)

_assert_(ceiling(cast(NULL as money)) is null)


_assert_(ceiling(123456789012345678901234567890.1234) = 123456789012345678901234567891)
_assert_(ceiling(-123456789012345678901234567890.1234) = -123456789012345678901234567890)
_assert_(ceiling(1234567890123456789012345678901234) = 1234567890123456789012345678901234)
_assert_(ceiling(-1234567890123456789012345678901234) = -1234567890123456789012345678901234)
_assert_(ceiling(0.0000000000) = 0)
_assert_(ceiling(99999999999999999999.99999999999999)   = 100000000000000000000)
_assert_(ceiling(-99999999999999999999.99999999999999)  = -99999999999999999999)
_assert_(ceiling(9999999999999999999999999999999999.1)   = 9999999999999999999999999999999999)
_assert_(ceiling(-9999999999999999999999999999999999.1234561)   = -9999999999999999999999999999999999)

_assert_(ceiling(cast(NULL as numeric)) is null)


_assert_(ceiling(1234.15f) = 1235.0)
_assert_(ceiling(-1234.15f) = -1234.0)
_assert_(ceiling(1234.15e-1f) = 124.0)
_assert_(ceiling(-1234.15e-1f) = -123.0)
_assert_(ceiling(1234.15e100f) = 1234.15e100f)
_assert_(ceiling(-1234.45e100f) =-1234.45e100f)
_assert_(ceiling(0.0000000000e3f) = 0)

_assert_(ceiling(cast(NULL as float)) is null)


-- ========== CEILING() overflow ============

_assert_error_(ceiling(@MONEY_MAX), 'DATA_NUMERIC_OVERFLOW')


-- ========== FLOOR() ============

_assert_(floor(NULL) is null)

_assert_(floor(123) = 123)
_assert_(floor(-123) = -123)
_assert_(floor(0) = 0)
_assert_(floor(@INT_MAX) = @INT_MAX)
_assert_(floor(@INT_MIN) = @INT_MIN)

_assert_(floor(cast(NULL as int)) is null)


_assert_(floor(cast(1230000000000 as bigint)) = cast(1230000000000 as bigint))
_assert_(floor(cast(-1230000000000 as bigint)) = cast(-1230000000000 as bigint))
_assert_(floor(cast(0 as bigint)) = 0)
_assert_(floor(@BIGINT_MAX) = @BIGINT_MAX)
_assert_(floor(@BIGINT_MIN) = @BIGINT_MIN)

_assert_(floor(cast(NULL as bigint)) is null)


_assert_(floor(@MONEY_MAX)           = @MONEY_MAX - $0.9999)
_assert_(floor(@MONEY_MIN + $0.9999) = @MONEY_MIN + $0.9999)

_assert_(floor(cast(NULL as money)) is null)


_assert_(floor(123456789012345678901234567890.1234) = 123456789012345678901234567890)
_assert_(floor(-123456789012345678901234567890.1234) = -123456789012345678901234567891)
_assert_(floor(1234567890123456789012345678901234) = 1234567890123456789012345678901234)
_assert_(floor(-1234567890123456789012345678901234) = -1234567890123456789012345678901234)
_assert_(floor(0.0000000000) = 0)
_assert_(floor(99999999999999999999.99999999999999)   = 99999999999999999999)
_assert_(floor(-99999999999999999999.99999999999999)  = -100000000000000000000)
_assert_(floor(9999999999999999999999999999999999.1)   = 9999999999999999999999999999999999)
_assert_(floor(-9999999999999999999999999999999999.1234561)   = -9999999999999999999999999999999999)

_assert_(floor(cast(NULL as numeric)) is null)


_assert_(floor(1234.15f) = 1234.0)
_assert_(floor(-1234.15f) = -1235.0)
_assert_(floor(1234.15e-1f) = 123.0)
_assert_(floor(-1234.15e-1f) = -124.0)
_assert_(floor(1234.15e100f) = 1234.15e100f)
_assert_(floor(-1234.45e100f) =-1234.45e100f)
_assert_(floor(0.0000000000e3f) = 0)
_assert_(floor(2e-200f) = 0.f)

_assert_(floor(cast(NULL as float)) is null)


-- ========== FLOOR() 2 ============


_assert_(floor(cast(130 as tinyint)) + cast(130 as tinyint) = 260)

_assert_(floor(cast(-220 as smallint)) = -220)
_assert_(floor(-220) = -220)
_assert_(floor(cast(-220 as int)) = -220)
_assert_(floor(cast(-220 as bigint)) = -220)
_assert_(floor(cast(-620.123456 as money)) = -$621)
_assert_(floor($-620.123456) = $-621)
_assert_(floor(-720.123456) = -721.0)
_assert_(floor(-820.123456f) = -821f)


-- ========== FLOOR() overflow ============

_assert_error_(floor(@MONEY_MIN), 'DATA_NUMERIC_OVERFLOW')


-- ========== SIGN() ============

_assert_(sign(NULL) is null)

_assert_(sign(123) = 1)
_assert_(sign(-123) = -1)
_assert_(sign(0) = 0)
_assert_(sign(@INT_MAX) = 1)
_assert_(sign(@INT_MIN) = -1)

_assert_(sign(cast(NULL as int)) is null)


_assert_(sign(cast(1230000000000 as bigint)) = 1)
_assert_(sign(cast(-1230000000000 as bigint)) = -1)
_assert_(sign(cast(0 as bigint)) = 0)
_assert_(sign(@BIGINT_MAX) = 1)
_assert_(sign(@BIGINT_MIN) = -1)

_assert_(sign(cast(NULL as bigint)) is null)


_assert_(sign(@MONEY_MAX) = 1)
_assert_(sign($-0.00) = 0)
_assert_(sign(@MONEY_MIN) = -1)

_assert_(sign(cast(NULL as money)) is null)


_assert_(sign(123456789012345678901234567890.1234) = 1)
_assert_(sign(-123456789012345678901234567890.1234) = -1)
_assert_(sign(1234567890123456789012345678901234) = 1)
_assert_(sign(-1234567890123456789012345678901234) = -1)
_assert_(sign(0.0000000000) = 0)
_assert_(sign(99999999999999999999.99999999999999)   = 1)
_assert_(sign(-99999999999999999999.99999999999999)  = -1)
_assert_(sign(9999999999999999999999999999999999.1)   = 1)
_assert_(sign(-9999999999999999999999999999999999.1234561)   = -1)
_assert_(sign(0.3d) = 1d)
_assert_(sign(0.0d) = 0d)
_assert_(sign(-0.3d) = -1d)

_assert_(sign(cast(NULL as numeric)) is null)


_assert_(sign(1234.15f) = 1)
_assert_(sign(-1234.15f) = -1)
_assert_(sign(1234.15e-1f) = 1)
_assert_(sign(-1234.15e-1f) = -1)
_assert_(sign(1234.15e100f) = 1)
_assert_(sign(-1234.45e100f) =-1)
_assert_(sign(0.0000000000e3f) = 0)
_assert_(sign(2e-200f) = 1)

_assert_(sign(cast(NULL as float)) is null)


-- ========== POWER() ============

_assert_(power(NULL, 2) is null)
_assert_(power(2, NULL) is null)

_assert_(power(123, 1) = 123)
_assert_(power(100, -3) = 0)
_assert_(power(-2, 4) = 16)
_assert_(power(0, 0) = 1)
_assert_(power(@INT_MAX, 1) = @INT_MAX)
_assert_(power(@INT_MIN, 1) = @INT_MIN)
_assert_(power(2, 30) + (power(2,30) - 1) = @INT_MAX )
_assert_(power(-2, 31) = @INT_MIN )

_assert_(power(cast(NULL as int), 2) is null)
_assert_(power(2, cast(NULL as int)) is null)
_assert_(power(cast(NULL as int), cast(NULL as int)) is null)


_assert_(power(cast(123 as bigint), 1) = 123)
_assert_(power(cast(-2 as bigint), 4) = 16)
_assert_(power(cast(9 as bigint), -4) = 0)
_assert_(power(cast(0 as bigint), 0) = 1)
-- _assert_(power(@BIGINT_MAX, 1) = @BIGINT_MAX) because power converts internally bigint to float and is not precise
-- _assert_(power(@BIGINT_MIN, 1) = @BIGINT_MIN)
-- _assert_(power(cast(2 as bigint), 62) + (power(cast(2 as bigint),62) - 1) = @BIGINT_MAX )
-- _assert_(power(cast(-2 as bigint), 63) = @BIGINT_MIN)

_assert_(power(cast(NULL as bigint), 2) is null)
_assert_(power(2, cast(NULL as bigint)) is null)
_assert_(power(cast(NULL as bigint), cast(NULL as bigint)) is null)


_assert_(power(@MONEY_MAX, 1) = @MONEY_MAX)
_assert_(power($-0.00, 0) = $1)
_assert_(power(@MONEY_MIN, 1) = @MONEY_MIN)

_assert_(power(cast(NULL as money), 2) is null)
_assert_(power(2, cast(NULL as money)) is null)
_assert_(power(cast(NULL as money), cast(NULL as money)) is null)


_assert_(power(1234567890123456789012.345678901234, 1) = 1234567890123456789012.345679)
_assert_(power(-1234567890123456789012.345678901234, 1) = -1234567890123456789012.345679)
_assert_(power(0.0000000000, 1) = 0)
_assert_(power(0.0000000000, 0.0) = 1)
_assert_(power(-3.0000000000, 3.0) = -27)
_assert_(power(-30.0000000000, -3.0) = -0.000037)
_assert_(power(-3.0000000000, 0.0) = 1)
_assert_(power(99999999999999999999.99999999999999, 1) = 100000000000000000000.000000000000)
_assert_(power(99999999999999999999.999999, 1)   = 99999999999999999999.999999)
_assert_(power(cast(-2 as numeric), 63) = @BIGINT_MIN)
_assert_(power(cast(2 as numeric), 63) - 1 = @BIGINT_MAX)

_assert_(power(cast(NULL as numeric), 2) is null)
_assert_(power(2, cast(NULL as numeric)) is null)
_assert_(power(cast(NULL as numeric), cast(NULL as numeric)) is null)


_assert_(power(1234.15f, 1) = 1234.15f)
_assert_(power(-1234.15f, 1) = -1234.15f)
_assert_(power(2f, 3) = 8f)

_assert_(power(cast(NULL as float), 2) is null)
_assert_(power(2, cast(NULL as float)) is null)
_assert_(power(cast(NULL as float), cast(NULL as float)) is null)


-- ========== POWER() overflow ============

_assert_error_(power(2, 31), 'DATA_INT_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(2, 300000), 'DATA_INT_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(0, -1), 'DATA_INT_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(cast(2 as bigint), 63), 'DATA_BIGINT_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(cast(2 as bigint), 300000), 'DATA_BIGINT_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(cast(0 as bigint), -1), 'DATA_BIGINT_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(900000000.0, 30000000), 'DATA_NUMERIC_INFINITE')


-- ========== POWER() overflow ============

_assert_error_(power(100000000000000000.0, 2), 'DATA_NUMERIC_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(10000000000000000000000000000.0, 1), 'DATA_NUMERIC_OVERFLOW')


-- ========== POWER() overflow ============

_assert_error_(power(10000000000000000000000000000.0, 100000000000), 'DATA_NUMERIC_INFINITE')


-- ========== POWER() overflow ============

_assert_error_(power(0.00, -1), 'DATA_NUMERIC_INFINITE')


-- ========== POWER() overflow ============

-- _assert_error_(power(230.00f, 100e30), 'DATA_FLOAT_INFINITE') bug in Go for power function


-- ========== POWER() overflow ============

_assert_error_(power(0.00f, -1), 'DATA_FLOAT_INFINITE')


-- ========== ROUND() INT BIGINT ============

_assert_(round(NULL, 1) is null)
_assert_(round(1, NULL) is null)

_assert_(round(1234, 1000000000, 1)  = 1234)
_assert_(round(1234, 10, 1) = 1234)
_assert_(round(1234, 1, 1)  = 1234)
_assert_(round(1234, 0, 1)  = 1234)
_assert_(round(1234, -1, 1) = 1230)
_assert_(round(1234, -3, 1) = 1000)
_assert_(round(1234, -4, 1) = 0)
_assert_(round(1234, -5, 1) = 0)
_assert_(round(1239, -1, 1)  = 1230)
_assert_(round(1239, -1, 0)  = 1240)
_assert_(round(-1239, -1, 1)  = -1230)
_assert_(round(0, -1, 1)  = 0)
_assert_(round(@INT_MAX, 0, 1) = @INT_MAX)
_assert_(round(@INT_MIN, 0, 1) = @INT_MIN)
_assert_(round(123500, -3) = 124000)
_assert_(round(-123500, -3) = -124000)
_assert_(round(123499, -3) = 123000)
_assert_(round(-123499, -3) = -123000)

_assert_(round(2147483645, -2) = 2147483600)
_assert_(round(2147483645, -8) = 2100000000)
_assert_(round(2050000000, -8) = 2100000000)
_assert_(round(2049999999, -8) = 2000000000)
_assert_(round(2049999999, -10) = 0)
_assert_(round(-2147483645, -2) = -2147483600)
_assert_(round(-2147483645, -8) = -2100000000)
_assert_(round(-2050000000, -8) = -2100000000)
_assert_(round(-2049999999, -8) = -2000000000)
_assert_(round(-2049999999, -10) = 0)
_assert_(round(1250, -2, NULL) = 1300)

_assert_(round(cast(NULL as int), 0, 1) is null)
_assert_(round(cast(NULL as int), 0) is null)
_assert_(round(10, cast(NULL as int), 1) is null)
_assert_(round(10, cast(NULL as int), 0) is null)


_assert_(round(cast(1230000000000 as bigint), 1000000000, 1)  = 1230000000000)
_assert_(round(cast(1230000000000 as bigint), 10, 1) = 1230000000000)
_assert_(round(cast(1230000000000 as bigint), 1, 1)  = 1230000000000)
_assert_(round(cast(1230000000000 as bigint), 0, 1)  = 1230000000000)
_assert_(round(cast(1234567890129 as bigint), -1, 1) = 1234567890120)
_assert_(round(cast(-1234567890129 as bigint), -1, 1) = -1234567890120)
_assert_(round(cast(-1298567890129 as bigint), -10, 1) = -1290000000000)
_assert_(round(cast(-1934567890129 as bigint), -12, 1) = -1000000000000)
_assert_(round(cast(1234 as bigint), -5, 1) = 0)
_assert_(round(cast(1239 as bigint), -1, 1)  = 1230)
_assert_(round(cast(1239 as bigint), -1, 0)  = 1240)
_assert_(round(cast(-1239 as bigint), -1, 1)  = -1230)
_assert_(round(cast(-1239 as bigint), -1)  = -1240)
_assert_(round(cast(0 as bigint), -1, 1)  = 0)
_assert_(round(@BIGINT_MAX, 0, 1) = @BIGINT_MAX)
_assert_(round(@BIGINT_MIN, 0, 1) = @BIGINT_MIN)
_assert_(round(@BIGINT_MAX, 0) = @BIGINT_MAX)
_assert_(round(@BIGINT_MIN, 0) = @BIGINT_MIN)
_assert_(round(@BIGINT_MAX, -18, 1) = 9000000000000000000)
_assert_(round(@BIGINT_MIN, -18, 1) = -9000000000000000000)
_assert_(round(@BIGINT_MAX, -18) = 9000000000000000000)
_assert_(round(@BIGINT_MIN, -18) = -9000000000000000000)
_assert_(round(@BIGINT_MAX, -19, 1) = 0)
_assert_(round(@BIGINT_MIN, -19, 1) = -0)
_assert_error_(round(@BIGINT_MAX, -19), 'BIGINT_OVERFLOW')
_assert_error_(round(@BIGINT_MIN, -19), 'BIGINT_OVERFLOW')

_assert_(round(cast(12342147483645 as bigint), -2) = 12342147483600)
_assert_(round(cast(12342147483645 as bigint), -8) = 12342100000000)
_assert_(round(cast(12342050000000 as bigint), -8) = 12342100000000)
_assert_(round(cast(12342049999999 as bigint), -8) = 12342000000000)
_assert_(round(cast(-12342050000000 as bigint), -8) = -12342100000000)
_assert_(round(cast(-12342049999999 as bigint), -8) = -12342000000000)
_assert_(round(cast(12342049999999 as bigint), -10) = 12340000000000)
_assert_(round(cast(-12342147483645 as bigint), -2) = -12342147483600)
_assert_(round(cast(-12342147483645 as bigint), -8) = -12342100000000)
_assert_(round(cast(-12342050000000 as bigint), -8) = -12342100000000)
_assert_(round(cast(-12342049999999 as bigint), -8) = -12342000000000)
_assert_(round(cast(-12342049999999 as bigint), -10) = -12340000000000)
_assert_(round(cast(1250 as bigint), -2, NULL) = 1300)

_assert_(round(cast(NULL as bigint), 0, 1) is null)
_assert_(round(cast(NULL as bigint), 0) is null)
_assert_(round(cast(10 as bigint), cast(NULL as int), 1) is null)
_assert_(round(cast(10 as bigint), cast(NULL as int), 0) is null)


-- ========== ROUND() INT overflow ============

_assert_error_(round(2147483647, -1), 'DATA_INT_OVERFLOW')


-- ========== ROUND() INT overflow ============

_assert_error_(round(-2147483648, -5), 'DATA_INT_OVERFLOW')


-- ========== ROUND() BIGINT overflow ============

_assert_error_(round(cast(9223372036854775807 as bigint), -4), 'DATA_BIGINT_OVERFLOW')


-- ========== ROUND() BIGINT overflow ============

_assert_error_(round(cast(-9223372036854775808 as bigint), -3), 'DATA_BIGINT_OVERFLOW')


-- ========== ROUND() MONEY ============

_assert_(round(cast(1230000000000 as money), 1000000000, 1)  = 1230000000000)
_assert_(round(cast(1230000000000 as money), 10, 1) = 1230000000000)
_assert_(round(cast(1230000000000 as money), 1, 1)  = 1230000000000)
_assert_(round(cast(1230000000000 as money), 0, 1)  = 1230000000000)
_assert_(round(cast(1234567890129 as money), -1, 1) = 1234567890120)
_assert_(round(cast(-1234567890129 as money), -1, 1) = -1234567890120)
_assert_(round(cast(-1298567890129 as money), -10, 1) = -1290000000000)
_assert_(round(cast(-1934567890129 as money), -12, 1) = -1000000000000)
_assert_(round(cast(1234 as money), -5, 1) = 0)
_assert_(round(cast(1239 as money), -1, 1)  = 1230)
_assert_(round(cast(1239 as money), -1, 0)  = 1240)
_assert_(round(cast(-1239 as money), -1, 1)  = -1230)
_assert_(round(cast(-1239 as money), -1)  = -1240)
_assert_(round(cast(0 as money), -1, 1)  = 0)
_assert_(round(cast(@MONEY_MAX as money), 4, 1) = @MONEY_MAX)
_assert_(round(cast(@MONEY_MAX as money), 3, 1) = $999999999999999.999)
_assert_(round(cast(@MONEY_MAX as money), 2, 1) = $999999999999999.99)
_assert_(round(cast(@MONEY_MAX as money), 0, 1) = @MONEY_MAX - $0.9999)
_assert_(round(cast(@MONEY_MIN as money), 0, 1) = @MONEY_MIN + $.9999)
_assert_(round(cast(@MONEY_MAX as money), -14, 1) = $900000000000000)
_assert_(round(cast(@MONEY_MAX as money), -15, 1) = $000000000000000)
_assert_(round(cast(@MONEY_MIN as money), -14, 1) = -$900000000000000)
_assert_(round(cast(@MONEY_MIN as money), -15, 1) = -$000000000000000)

_assert_(round(cast(12342147483645 as money), -2) = 12342147483600)
_assert_(round(cast(12342147483645 as money), -8) = 12342100000000)
_assert_(round(cast(12342050000000 as money), -8) = 12342100000000)
_assert_(round(cast(12342049999999 as money), -8) = 12342000000000)
_assert_(round(cast(-12342050000000 as money), -8) = -12342100000000)
_assert_(round(cast(-12342049999999 as money), -8) = -12342000000000)
_assert_(round(cast(12342049999999 as money), -10) = 12340000000000)
_assert_(round(cast(-12342147483645 as money), -2) = -12342147483600)
_assert_(round(cast(-12342147483645 as money), -8) = -12342100000000)
_assert_(round(cast(-12342050000000 as money), -8) = -12342100000000)
_assert_(round(cast(-12342049999999 as money), -8) = -12342000000000)
_assert_(round(cast(-12342049999999 as money), -10) = -12340000000000)
_assert_(round(cast(1250 as money), -2, NULL) = 1300)

_assert_(round(cast(-123421474.8364 as money), 0) = -123421475)
_assert_(round(cast(-123421474.8364 as money), 1) = -123421474.8)
_assert_(round(cast(-123421474.8364 as money), 2) = -123421474.84)
_assert_(round(cast(-123421474.8364 as money), 4) = -123421474.8364)
_assert_(round(cast(-123421474.8363 as money), 3) = -123421474.836)
_assert_(round(cast(-123421474.8365 as money), 3) = -123421474.837)
_assert_(round(cast(-123421474.8364 as money), 5) = -123421474.8364)
_assert_(round(cast(-123421474.8364 as money), 8) = -123421474.8364)

_assert_(round(cast(NULL as money), 0, 1) is null)
_assert_(round(cast(NULL as money), 0) is null)
_assert_(round(cast(10 as money), cast(NULL as int), 1) is null)
_assert_(round(cast(10 as money), cast(NULL as int), 0) is null)


-- ========== ROUND() MONEY overflow ============

_assert_error_(round(cast(@MONEY_MAX as money), 3), 'DATA_NUMERIC_OVERFLOW')


-- ========== ROUND() MONEY overflow ============

_assert_error_(round(cast(@MONEY_MIN as money), 3), 'DATA_NUMERIC_OVERFLOW')


-- ========== ROUND() MONEY overflow ============

_assert_error_(round(cast(@MONEY_MIN as money), -3), 'DATA_NUMERIC_OVERFLOW')


-- ========== ROUND() NUMERIC ============

_assert_(round(cast(1230000000000 as numeric(34,4)), 1000000000, 1)  = 1230000000000)
_assert_(round(cast(1230000000000 as numeric(34,4)), 10, 1) = 1230000000000)
_assert_(round(cast(1230000000000 as numeric(34,4)), 1, 1)  = 1230000000000)
_assert_(round(cast(1230000000000 as numeric(34,4)), 0, 1)  = 1230000000000)
_assert_(round(cast(1234567890129 as numeric(34,4)), -1, 1) = 1234567890120)
_assert_(round(cast(-1234567890129 as numeric(34,4)), -1, 1) = -1234567890120)
_assert_(round(cast(-1298567890129 as numeric(34,4)), -10, 1) = -1290000000000)
_assert_(round(cast(-1934567890129 as numeric(34,4)), -12, 1) = -1000000000000)
_assert_(round(cast(1234 as numeric(34,4)), -5, 1) = 0)
_assert_(round(cast(1239 as numeric(34,4)), -1, 1)  = 1230)
_assert_(round(cast(1239 as numeric(34,4)), -1, 0)  = 1240)
_assert_(round(cast(-1239 as numeric(34,4)), -1, 1)  = -1230)
_assert_(round(cast(-1239 as numeric(34,4)), -1)  = -1240)
_assert_(round(cast(0 as numeric(34,4)), -1, 1)  = 0)
_assert_(round(cast(@BIGINT_MAX as numeric(34,4)), 0, 1) = @BIGINT_MAX)
_assert_(round(cast(@BIGINT_MIN as numeric(34,4)), 0, 1) = @BIGINT_MIN)
_assert_(round(cast(@BIGINT_MAX as numeric(34,4)), 0) = @BIGINT_MAX)
_assert_(round(cast(@BIGINT_MIN as numeric(34,4)), 0) = @BIGINT_MIN)
_assert_(round(cast(@BIGINT_MAX as numeric(34,4)), -18, 1) = 9000000000000000000)
_assert_(round(cast(@BIGINT_MIN as numeric(34,4)), -18, 1) = -9000000000000000000)
_assert_(round(cast(@BIGINT_MAX as numeric(34,4)), -18) = 9000000000000000000)
_assert_(round(cast(@BIGINT_MIN as numeric(34,4)), -18) = -9000000000000000000)
_assert_(round(cast(@BIGINT_MAX as numeric(34,4)), -19, 1) = 0)
_assert_(round(cast(@BIGINT_MIN as numeric(34,4)), -19, 1) = -0)
_assert_(round(cast(@BIGINT_MAX as numeric(34,4)), -19) = 10000000000000000000)
_assert_(round(cast(@BIGINT_MIN as numeric(34,4)), -19) = -10000000000000000000)

_assert_(round(cast(12342147483645 as numeric(34,4)), -2) = 12342147483600)
_assert_(round(cast(12342147483645 as numeric(34,4)), -8) = 12342100000000)
_assert_(round(cast(12342050000000 as numeric(34,4)), -8) = 12342100000000)
_assert_(round(cast(12342049999999 as numeric(34,4)), -8) = 12342000000000)
_assert_(round(cast(-12342050000000 as numeric(34,4)), -8) = -12342100000000)
_assert_(round(cast(-12342049999999 as numeric(34,4)), -8) = -12342000000000)
_assert_(round(cast(12342049999999 as numeric(34,4)), -10) = 12340000000000)
_assert_(round(cast(-12342147483645 as numeric(34,4)), -2) = -12342147483600)
_assert_(round(cast(-12342147483645 as numeric(34,4)), -8) = -12342100000000)
_assert_(round(cast(-12342050000000 as numeric(34,4)), -8) = -12342100000000)
_assert_(round(cast(-12342049999999 as numeric(34,4)), -8) = -12342000000000)
_assert_(round(cast(-12342049999999 as numeric(34,4)), -10) = -12340000000000)
_assert_(round(cast(1250 as numeric(34,4)), -2, NULL) = 1300)

_assert_(round(12342049999999d, -10) = 12340000000000)
_assert_(round(-12342147483645d, -2) = -12342147483600)
_assert_(round(-12342147483645d, -8) = -12342100000000)
_assert_(round(-12342050000000d, -8) = -12342100000000)
_assert_(round(-12342049999999d, -8) = -12342000000000)
_assert_(round(-12342049999999d, -10) = -12340000000000)
_assert_(round(1250d, -2, NULL) = 1300)

_assert_(round(cast(-123421474.83645 as numeric(34,5)), 0) = -123421475)
_assert_(round(cast(-123421474.83645 as numeric(34,5)), 1) = -123421474.8)
_assert_(round(cast(-123421474.83645 as numeric(34,5)), 2) = -123421474.84)
_assert_(round(cast(-123421474.83645 as numeric(34,5)), 2, 1) = -123421474.83)
_assert_(round(cast(-123421474.83645 as numeric(34,5)), 4) = -123421474.8365)
_assert_(round(cast(-123421474.83634 as numeric(34,5)), 4) = -123421474.8363)
_assert_(round(cast(-123421474.83635 as numeric(34,5)), 4) = -123421474.8364)
_assert_(round(cast(-123421474.83645 as numeric(34,5)), 5) = -123421474.83645)
_assert_(round(cast(-123421474.83645 as numeric(34,5)), 8) = -123421474.83645)

_assert_(round(999999999999999999999.999999999999, 0) = 1000000000000000000000.000000000000)

_assert_(round(cast(NULL as numeric(34,4)), 0, 1) is null)
_assert_(round(cast(NULL as numeric(34,4)), 0) is null)
_assert_(round(cast(10 as numeric(34,4)), cast(NULL as int), 1) is null)
_assert_(round(cast(10 as numeric(34,4)), cast(NULL as int), 0) is null)


-- ========== ROUND() NUMERIC overflow ============

_assert_error_(round(9999999999999999999999.999999999999, 0), 'DATA_NUMERIC_OVERFLOW')


-- ========== ROUND() FLOAT ============


_assert_(abs(round(123.456785f, 6) - 123.456785f) < 0.0000000001f)
_assert_(abs(round(123.45678501f, 5) - 123.45679f) < 0.00000001f)
_assert_(abs(round(123.45678501f, 1) - 123.5f) < 0.00000001f)
_assert_(abs(round(123.45678501f, 0) - 123.0f) < 0.00001f)
_assert_(abs(round(123.55678501f, 0) - 124.0f) < 0.00001f)
_assert_(abs(round(123.45678501f, -1) - 120.0f) < 0.00001f)
_assert_(abs(round(123.55678501f, -1) - 120.0f) < 0.00001f)
_assert_(abs(round(-1234567890.12345f, -4) - -1234570000f) < 0.00001f)

_assert_(abs(round(123.456785f, 6, 1) - 123.456785f) < 0.0000000001f)
_assert_(abs(round(123.45678501f, 5, 1) - 123.45678f) < 0.00000001f)
_assert_(abs(round(123.45678501f, 1, 1) - 123.4f) < 0.00000001f)
_assert_(abs(round(123.45678501f, 0, 1) - 123.0f) < 0.00001f)
_assert_(abs(round(123.55678501f, 0, 1) - 123.0f) < 0.00001f)
_assert_(abs(round(123.45678501f, -1, 1) - 120.0f) < 0.00001f)
_assert_(abs(round(123.55678501f, -1, 1) - 120.0f) < 0.00001f)
_assert_(abs(round(-1234567890.12345f, -4, 1) - -1234560000f) < 0.00001f)
_assert_(abs(round(-1234567890.12345f, -9, 1) - -1000000000f) < 0.00001f)
_assert_(abs(round(-1234567890.12345f, -10, 1)) < 0.0000000001f)
_assert_(abs(round(-1234567890.12345f, -11, 1)) < 0.0000000001f)


-- ========== ROUND() FLOAT overflow ============

_assert_error_(round(123.456785f, 600), 'DATA_FLOAT_NAN')


-- ========== ROUND() FLOAT overflow ============

_assert_error_(round(123.456785f, -600), 'DATA_FLOAT_NAN')


-- ========== TRIGONOMETRIC and EXP FLOAT functions ============

_assert_(acos(NULL) is null)

_assert_(abs(acos(0) - 1.5708f) < 0.001f)
_assert_(abs(acos(0.5) - 1.0472f) < 0.001f)
_assert_(abs(acos(1) - 0f ) < 0.001f)
_assert_(acos(NULL) is null)


_assert_(asin(NULL) is null)

_assert_(abs(asin(0) - 0f) < 0.001f)
_assert_(abs(asin(0.5) - 0.523599f) < 0.001f)
_assert_(abs(asin(1) - 1.5708f ) < 0.001f)
_assert_(asin(NULL) is null)


_assert_(atan(NULL) is null)

_assert_(abs(atan(0) - 0f) < 0.001f)
_assert_(abs(atan(0.5) - 0.463648f) < 0.001f)
_assert_(abs(atan(1) - 0.785398f ) < 0.001f)
_assert_(abs(atan(10) - 1.47113f ) < 0.001f)
_assert_(atan(NULL) is null)


_assert_(atn2(NULL, 1) is null)
_assert_(atn2(1, NULL) is null)

_assert_(abs(atn2(0, 0.5) - 0f) < 0.001f)
_assert_(abs(atn2(0.5, 05) - 0.0996687f) < 0.00001f)
_assert_(abs(atn2(1, 10) - 0.0996687f ) < 0.00001f)
_assert_(abs(atn2(10, 5) - 1.10715f ) < 0.001f)
_assert_(atn2(NULL, 9) is null)
_assert_(atn2(9, NULL) is null)


_assert_(cos(NULL) is null)

_assert_(abs(cos(0) - 1f) < 0.001f)
_assert_(abs(cos(0.5) - 0.877583f) < 0.001f)
_assert_(abs(cos(1) - 0.540302f ) < 0.001f)
_assert_(abs(cos(10) - -0.839072f ) < 0.001f)
_assert_(cos(NULL) is null)


_assert_(sin(NULL) is null)

_assert_(abs(sin(0) - 0f) < 0.001f)
_assert_(abs(sin(0.5) - 0.479426f) < 0.001f)
_assert_(abs(sin(1) - 0.841471f ) < 0.001f)
_assert_(abs(sin(10) - -0.544021f ) < 0.001f)
_assert_(sin(NULL) is null)


_assert_(tan(NULL) is null)

_assert_(abs(tan(0) - 0f) < 0.001f)
_assert_(abs(tan(0.5) - 0.546302f) < 0.001f)
_assert_(abs(tan(1) - 1.55741f ) < 0.001f)
_assert_(abs(tan(10) - 0.648361f ) < 0.001f)
_assert_(tan(pi()/2) > 10000000000f)
_assert_(tan(NULL) is null)


_assert_(cot(NULL) is null)

_assert_(abs(cot(0.5) - 1.83049f) < 0.0001f)
_assert_(abs(cot(1) - 0.642093f ) < 0.0001f)
_assert_(abs(cot(10) - 1.54235f ) < 0.0001f)
_assert_(cot(pi()/2) < 0.0000000001f)
_assert_(cot(NULL) is null)


_assert_(exp(NULL) is null)

_assert_(abs(exp(0) - 1f) < 0.0001f)
_assert_(abs(exp(1) - 2.71828f) < 0.0001f)
_assert_(abs(exp(10) - 22026.5f) < 0.1f)
_assert_(exp(NULL) is null)


_assert_(log(NULL) is null)

_assert_(abs(log(1) - 0f) < 0.0001f)
_assert_(abs(log(10) - 2.30259f) < 0.001f)
_assert_(abs(log(1000) - 6.90776f) < 0.001f)
_assert_(abs(log(2e300) - 691.469f) < 0.001f)
_assert_(log(NULL) is null)


_assert_(log10(NULL) is null)

_assert_(abs(log10(1) - 0f) < 0.0001f)
_assert_(abs(log10(10) - 1f) < 000.1f)
_assert_(abs(log10(500) - 2.69897f) < 000.1f)
_assert_(abs(log10(1000) - 3f) < 0.001f)
_assert_(abs(log10(1e300) - 300f) < 000.1f)
_assert_(log10(NULL) is null)


_assert_(sqrt(NULL) is null)

_assert_(abs(sqrt(0) - 0f) < 0.0001f)
_assert_(abs(sqrt(1) - 1f) < 0.0001f)
_assert_(abs(sqrt(10) - 3.16228f) < 0.01f)
_assert_(sqrt(NULL) is null)


_assert_(square(NULL) is null)

_assert_(abs(square(-55.51) - 3081.36f) < 0.01f)
_assert_(abs(square(-1) - 1f) < 0.0001f)
_assert_(abs(square(0) - 0f) < 0.0001f)
_assert_(abs(square(1) - 1f) < 0.0001f)
_assert_(abs(square(10) - 100f) < 0.01f)
_assert_(square(NULL) is null)


_assert_(abs(pi() - 3.14159265358f) < 0.01f)


_assert_(degrees(NULL) is null)

_assert_(abs(degrees(2.5) - 143.239448782705810f) < 0.01f)
_assert_(degrees(NULL) is null)


_assert_(radians(NULL) is null)

_assert_(abs(radians(25.4) - 0.44331363000655967f) < 0.01f)
_assert_(radians(NULL) is null)


-- ========== acos() overflow ============

_assert_error_(acos(1.1), 'DATA_FLOAT_NAN')


-- ========== asin() overflow ============

_assert_error_(asin(1.1), 'DATA_FLOAT_NAN')


-- ========== cot() overflow ============

_assert_error_(cot(0), 'DATA_FLOAT_INFINITE')


-- ========== log() overflow ============

_assert_error_(log(0), 'DATA_FLOAT_INFINITE')


-- ========== log() overflow ============

_assert_error_(log(-1), 'DATA_FLOAT_NAN')


-- ========== log10() overflow ============

_assert_error_(log10(0), 'DATA_FLOAT_INFINITE')


-- ========== log10() overflow ============

_assert_error_(log10(-1), 'DATA_FLOAT_NAN')


-- ========== sqrt() overflow ============

_assert_error_(log10(-1), 'DATA_FLOAT_NAN')


-- ========== LEN() ============

-- see also ADD() test

declare @aq char(4) = 'ni'
declare @bq char(4) = 'co'

_assert_(len(@aq) = 2)
_assert_(len(@bq) = 2)
_assert_(len(@aq+@bq) = 6)
_assert_('<' + @aq + @bq +'>' = '<ni  co  >')

_assert_(len(cast(NULL as varchar)) is null)

_assert_(len(NULL) is null)


-- ========== ASCII() ============


_assert_(ascii('ABC') = 65)
_assert_(ascii('é')   = 233)
_assert_(ascii('') is null)

_assert_(ascii(cast(NULL as varchar)) is null)

_assert_(ascii(NULL) is null)


-- ========== UNICODE() ============


_assert_(unicode('é') = 233)
_assert_(unicode('') is null)

_assert_(unicode(cast(NULL as varchar)) is null)

_assert_(unicode(NULL) is null)


declare @alf varchar(10) = e'\U00010900'  -- PHOENICIAN LETTER ALF

_assert_(unicode(@alf) = 0x10900)


DECLARE @nstring1 nchar(8)
SET @nstring1 = N'København'
_assert_(UNICODE(SUBSTRING(@nstring1, 2, 1)) = 248)
_assert_( NCHAR(UNICODE(SUBSTRING(@nstring1, 2, 1))) = 'ø')


-- ========== NCHAR() ============


_assert_(char(0) = e'\u0000')

_assert_(len(char(0)) = 1)

_assert_(char(233) = 'é')

_assert_(char(cast(null as int)) is null)

_assert_(char(NULL) is null)


-- ========== NCHAR() bad arg ============


_assert_error_(char(-1), 'VARCHAR_UNICOE_CODEPOINT_INVALID')


-- ========== LEFT() ============

_assert_(left(cast(null as varchar), 1) is null)
_assert_(left('asdf', cast(null as int)) is null)
_assert_(left(cast(null as varchar), cast(null as int)) is null)
_assert_(left(NULL, 1) is null)
_assert_(left('asdf', NULL) is null)


_assert_('<' + left('abcde', 0) + '>' = '<>')

_assert_(left('abcde', 1) = 'a')
_assert_(left('abcde', 4) = 'abcd')
_assert_(left('abcde', 5) = 'abcde')
_assert_(left('abcde', 6) = 'abcde')


DECLARE @nstring nchar(9)
SET @nstring = N'København'

_assert_('<' + left(@nstring, 1000) + '>' = '<København>')
_assert_('<' + left(@nstring, 10) + '>' = '<København>')
_assert_(left(@nstring, 9) = 'København')
_assert_(left(@nstring, 8) = 'Københav')
_assert_(left(@nstring, 3) = 'Køb')

_assert_('<' + left(@nstring, 0) + '>' = '<>')


declare @a73 char(10)  = 'hello'

_assert_('<' + left(@a73, 1000) + '>' = '<hello     >')
_assert_('<' + left(@a73, 11) + '>'   = '<hello     >')
_assert_('<' + left(@a73, 10) + '>'   = '<hello     >')
_assert_('<' + left(@a73, 9) + '>'    = '<hello    >')
_assert_('<' + left(@a73, 8) + '>'    = '<hello   >')
_assert_('<' + left(@a73, 6) + '>'    = '<hello >')
_assert_('<' + left(@a73, 5) + '>'    = '<hello>')
_assert_('<' + left(@a73, 4) + '>'    = '<hell>')
_assert_('<' + left(@a73, 1) + '>'    = '<h>')
_assert_('<' + left(@a73, 0) + '>'    = '<>')


set @a73 = ''

_assert_('<' + left(@a73, 1000) + '>' = '<          >')
_assert_('<' + left(@a73, 11) + '>'   = '<          >')
_assert_('<' + left(@a73, 10) + '>'   = '<          >')
_assert_('<' + left(@a73, 9) + '>'    = '<         >')
_assert_('<' + left(@a73, 8) + '>'    = '<        >')
_assert_('<' + left(@a73, 6) + '>'    = '<      >')
_assert_('<' + left(@a73, 5) + '>'    = '<     >')
_assert_('<' + left(@a73, 4) + '>'    = '<    >')
_assert_('<' + left(@a73, 1) + '>'    = '< >')
_assert_('<' + left(@a73, 0) + '>'    = '<>')


-- ========== LEFT() bad arg ============

_assert_error_(left('abcde', -1), 'VARCHAR_LEFT_RIGHT_BAD_ARGUMENT')


-- ========== RIGHT() ============

_assert_(right(cast(null as varchar), 1) is null)
_assert_(right('asdf', cast(null as int)) is null)
_assert_(right(cast(null as varchar), cast(null as int)) is null)
_assert_(right(NULL, 1) is null)
_assert_(right('asdf', NULL) is null)


_assert_('<' + right('abcde', 0) + '>' = '<>')

_assert_(right('abcde', 1) = 'e')
_assert_(right('abcde', 4) = 'bcde')
_assert_(right('abcde', 5) = 'abcde')
_assert_(right('abcde', 6) = 'abcde')


set @a73   = 'hello'

_assert_('<' + right(@a73, 1000) + '>' = '<hello     >')
_assert_('<' + right(@a73, 11) + '>'   = '<hello     >')
_assert_('<' + right(@a73, 10) + '>'   = '<hello     >')
_assert_('<' + right(@a73, 9) + '>'    = '<ello     >')
_assert_('<' + right(@a73, 8) + '>'    = '<llo     >')
_assert_('<' + right(@a73, 6) + '>'    = '<o     >')
_assert_('<' + right(@a73, 5) + '>'    = '<     >')
_assert_('<' + right(@a73, 4) + '>'    = '<    >')
_assert_('<' + right(@a73, 1) + '>'    = '< >')
_assert_('<' + right(@a73, 0) + '>'    = '<>')


set @a73 = ''

_assert_('<' + right(@a73, 1000) + '>' = '<          >')
_assert_('<' + right(@a73, 11) + '>'   = '<          >')
_assert_('<' + right(@a73, 10) + '>'   = '<          >')
_assert_('<' + right(@a73, 9) + '>'    = '<         >')
_assert_('<' + right(@a73, 8) + '>'    = '<        >')
_assert_('<' + right(@a73, 6) + '>'    = '<      >')
_assert_('<' + right(@a73, 5) + '>'    = '<     >')
_assert_('<' + right(@a73, 4) + '>'    = '<    >')
_assert_('<' + right(@a73, 1) + '>'    = '< >')
_assert_('<' + right(@a73, 0) + '>'    = '<>')


-- ========== RIGHT() bad arg ============

_assert_error_(right('abcde', -1), 'VARCHAR_LEFT_RIGHT_BAD_ARGUMENT')


-- ========== SUSBSTRING() ============

_assert_('<' + substring('hello', 1, 0) + '>' = '<>')
_assert_('<' + substring('hello', 1, 1) + '>' = '<h>')
_assert_('<' + substring('hello', 1, 5) + '>' = '<hello>')
_assert_('<' + substring('hello', 1, 6) + '>' = '<hello>')
_assert_('<' + substring('hello', 1, @INT_MAX) + '>' = '<hello>')

_assert_('<' + substring('hello', 4, 0) + '>' = '<>')
_assert_('<' + substring('hello', 4, 1) + '>' = '<l>')
_assert_('<' + substring('hello', 4, 2) + '>' = '<lo>')
_assert_('<' + substring('hello', 4, 3) + '>' = '<lo>')
_assert_('<' + substring('hello', 4, @INT_MAX) + '>' = '<lo>')

_assert_('<' + substring('hello', 0, 0) + '>' = '<>')
_assert_('<' + substring('hello', 0, 1) + '>' = '<>')
_assert_('<' + substring('hello', 0, 2) + '>' = '<h>')
_assert_('<' + substring('hello', 0, 3) + '>' = '<he>')
_assert_('<' + substring('hello', 0, @INT_MAX) + '>' = '<hello>')

_assert_('<' + substring('hello', -1, 3) + '>'       = '<h>')
_assert_('<' + substring('hello', -10, 13) + '>'     = '<he>')
_assert_('<' + substring('hello', -5000, 5002) + '>' = '<h>')
_assert_('<' + substring('hello', -5000, 5001) + '>' = '<>')
_assert_('<' + substring('hello', -5000, 500) + '>' = '<>')

_assert_('<' + substring('hello', @INT_MIN+1, @INT_MAX) + '>' = '<>')

_assert_('<' + substring('hello', @INT_MIN, @INT_MAX) + '>' = '<>')

_assert_('<' + substring('hello', @INT_MAX, @INT_MAX) + '>' = '<>')
_assert_('<' + substring('hello', @INT_MAX, 0) + '>'        = '<>')


_assert_(substring(cast(null as varchar), 1, 1) is null)
_assert_(substring('asdf', cast(null as int), 1) is null)
_assert_(substring('asdf', 1, cast(null as int)) is null)

_assert_(substring(NULL, 1, 1) is null)
_assert_(substring('asdf', NULL, 1) is null)
_assert_(substring('asdf', 1, NULL) is null)



-- ========== SUSBSTRING() bad arg ============


_assert_error_(substring('hello', 0, -1), 'VARCHAR_SUBSTRING_BAD_LENGTH')


-- ========== LTRIM() ============

_assert_('<' + ltrim('hello') + '>' = '<hello>')
_assert_('<' + ltrim(' hello') + '>' = '<hello>')
_assert_('<' + ltrim('       hello') + '>' = '<hello>')
_assert_('<' + ltrim('       hello ') + '>' = '<hello >')
_assert_('<' + ltrim('       hello   ') + '>' = '<hello   >')

_assert_('<' + ltrim('') + '>' = '<>')
_assert_('<' + ltrim(' ') + '>' = '<>')
_assert_('<' + ltrim('      ') + '>' = '<>')

_assert_(ltrim(cast(null as varchar)) is null)
_assert_(ltrim(NULL) is null)


-- ========== RTRIM() ============

_assert_('<' + rtrim('hello') + '>' = '<hello>')
_assert_('<' + rtrim('hello ') + '>' = '<hello>')
_assert_('<' + rtrim('hello       ') + '>' = '<hello>')
_assert_('<' + rtrim(' hello       ') + '>' = '< hello>')
_assert_('<' + rtrim('  hello   ') + '>' = '<  hello>')

_assert_('<' + rtrim('') + '>' = '<>')
_assert_('<' + rtrim(' ') + '>' = '<>')
_assert_('<' + rtrim('      ') + '>' = '<>')

_assert_(rtrim(cast(null as varchar)) is null)
_assert_(rtrim(NULL) is null)


-- ========== REPLACE() ============

_assert_('<' + replace('', 'bla', 'abc') + '>' = '<>')

_assert_('<' + replace('hello', '', 'abc') + '>' = '<hello>')

_assert_('<' + replace('hello', 'abc', 'abc') + '>' = '<hello>')

_assert_('<' + replace('hello', 'abc', 'def') + '>' = '<hello>')

_assert_('<' + replace('hello', 'l', 'L') + '>' = '<heLLo>')

_assert_('<' + replace('hello', 'o', 'X') + '>' = '<heLLX>')
_assert_('<' + replace('hello', 'o', 'X') + '>' = '<heLLX>' collate fr_ci_ai)
_assert_('<' + replace('hello', 'o', 'X') + '>' != '<heLLX>'  collate fr_cs_as)

_assert_('<' + replace('hello', 'h', 'X') + '>' = '<XeLLo>')
_assert_('<' + replace('hello', 'h', 'X') + '>' != '<XeLLo>'  collate fr_cs_as)

_assert_('<' + replace('aaa', 'a', 'X') + '>' = '<XXx>')

_assert_('<' + replace('a', 'a', 'X') + '>' = '<x>')

_assert_('<' + replace('hellohello', 'hello', 'X') + '>' = '<xX>')

_assert_('<' + replace('hello, hello, and hello again', 'hello', 'congratulations') + '>' = '<congratulations, congratulations, and congratulations again>')


declare @x varchar(20) = 'hellohello'

_assert_('<' + replace('hellohello'  collate fr_ci_ai, 'HELLO', 'X') + '>' = '<XX>' )
_assert_('<' + replace(@x collate fr_cs_as, 'HELLO', 'X') + '>' = '<hellohello>')
_assert_('<' + replace('hellohello'  collate fr_ci_ai, 'HELLO', 'X') + '>' = '<XX>')
_assert_('<' + replace(@x collate fr_cs_as, 'HELLO', 'X') + '>' = '<hellohello>')


_assert_(replace('aaaaaaaa', 'a', replicate('x', 1000)) = replicate('x', 8000))

_assert_(replace(replicate('helloXXX', 1000), 'XXX', 'ZZZ') = replicate('helloZZZ', 1000))


_assert_(replace(cast(null as varchar), 'bla', 'abc')   is null)
_assert_(replace('hello', cast(null as varchar), 'abc') is null)
_assert_(replace('hello', 'aaa', cast(null as varchar)) is null)

_assert_(replace(NULL, 'asf', 'qwee') is null)
_assert_(replace('sdfas', NULL, 'asf') is null)
_assert_(replace('eewer', 'sdfas', NULL) is null)


-- ========== REPLACE() overflow ============

_assert_error_(replace('xaaaaaaaa', 'a', replicate('x', 1000)), 'DATA_VARCHAR_OVERFLOW')


-- ========== UPPER() ============

declare @a64 char(10)  = 'élève'
declare @b834 char(10) = ''

_assert_('<' + upper('élève') + '>' = '<ÉLÈVE>')
_assert_('<' + upper('') + '>'      = '<>')

_assert_('<' + upper(@a64) + '>'      = '<ÉLÈVE     >')
_assert_('<' + upper(@b834) + '>'      = '<          >')

_assert_(upper(cast(NULL as varchar)) is null)
_assert_(upper(NULL) is null)


-- ========== LOWER() ============

set @a64 = 'ÉLÈVE'
set @b834   = ''

_assert_('<' + lower('ÉLÈVE') + '>' = '<élève>')
_assert_('<' + lower('') + '>'      = '<>')

_assert_('<' + lower(@a64) + '>'      = '<élève     >')
_assert_('<' + lower(@b834) + '>'      = '<          >')

_assert_(lower(cast(NULL as varchar)) is null)
_assert_(lower(NULL) is null)


-- ========== SUBSTRING() ============


declare @a varchar(100) = 'HelloWorld'

_assert_(charindex('lo', @a, -1)  = 4)
_assert_(charindex('lo', @a, 0)   = 4)
_assert_(charindex('lo', @a, 1)   = 4)
_assert_(charindex('lo', @a, 2)   = 4)
_assert_(charindex('lo', @a, 3)   = 4)
_assert_(charindex('lo', @a, 4)   = 4)
_assert_(charindex('lo', @a, 5)   = 0)
_assert_(charindex('lo', @a, 5000) = 0)

_assert_(charindex('HelloWorld', @a, 1) = 1)
_assert_(charindex('HelloWorld', @a, 4) = 0)
_assert_(charindex('HelloWorld', @a, 5) = 0)
_assert_(charindex('HelloWorld', @a, 50)= 0)

_assert_(charindex('World', @a, 1) = 6)
_assert_(charindex('WORLD', @a collate fr_ci_ai, 1) = 6)
_assert_(charindex('world', @a collate fr_ci_ai, 1) = 6)

_assert_(charindex('helloworld', @a collate fr_ci_ai, 1) = 1)
_assert_(charindex('helloworld', @a collate fr_ci_ai, 4) = 0)
_assert_(charindex('helloworld', @a collate fr_ci_ai, 5) = 0)
_assert_(charindex('helloworld', @a collate fr_ci_ai, 50)= 0)

_assert_(charindex('', @a, 1) = 0)
_assert_(charindex('', @a, 4) = 0)
_assert_(charindex('', @a, 5) = 0)

_assert_(charindex('d', @a, 0)  = 10)
_assert_(charindex('d', @a, 10) = 10)
_assert_(charindex('d', @a, 11) = 0)

_assert_(charindex('', '', 0)  = 0)
_assert_(charindex('', '', 1)  = 0)
_assert_(charindex('', '', 10) = 0)

_assert_(charindex('', ' ', 0)  = 0)
_assert_(charindex('', ' ', 1)  = 0)
_assert_(charindex('', ' ', 10) = 0)

_assert_(charindex(' ', ' ', 0) = 1)
_assert_(charindex(' ', ' ', 1) = 1)
_assert_(charindex(' ', ' ', 10)=0)


declare @b456 varchar(100)= 'HelloWorld'
declare @c85 varchar(100)  = 'world'
declare @d836 varchar(100)  = 'world'

_assert_(charindex(@c85, @a  collate fr_ci_ai, 1) = 6)
_assert_(charindex(@c85, @b456  collate fr_cs_as, 1) = 0)

_assert_(charindex(@d836, @a  collate fr_ci_ai, 1) = 6)
_assert_(charindex(@d836, @b456  collate fr_cs_as, 1) = 0)

_assert_(charindex('lo', @b456, -1)  = 4)
_assert_(charindex('lo', @b456, -1000)  = 4)
_assert_(charindex('lo', @b456, 0)   = 4)
_assert_(charindex('lo', @b456, 1)   = 4)
_assert_(charindex('lo', @b456, 2)   = 4)
_assert_(charindex('lo', @b456, 3)   = 4)
_assert_(charindex('lo', @b456, 4)   = 4)
_assert_(charindex('lo', @b456, 5)   = 0)
_assert_(charindex('lo', @b456, 5000) = 0)

_assert_(charindex('HelloWorld', @b456, 1) = 1)
_assert_(charindex('HelloWorld', @b456, 4) = 0)
_assert_(charindex('HelloWorld', @b456, 5) = 0)
_assert_(charindex('HelloWorld', @b456, 50)= 0)

_assert_(charindex('World', @b456, 1) = 6)
_assert_(charindex('WORLD' collate fr_cs_as, @b456, 1) = 0)
_assert_(charindex('world', @b456 collate fr_cs_as, 1) = 0)

_assert_(charindex('helloworld' collate fr_cs_as, @b456, 1) = 0)
_assert_(charindex('helloworld' collate fr_cs_as, @b456, 4) = 0)
_assert_(charindex('helloworld' collate fr_cs_as, @b456, 5) = 0)
_assert_(charindex('helloworld' collate fr_cs_as, @b456, 50)= 0)

_assert_(charindex('', @b456, 1) = 0)
_assert_(charindex('', @b456, 4) = 0)
_assert_(charindex('', @b456, 5) = 0)

_assert_(charindex('d', @b456, 0)  = 10)
_assert_(charindex('d', @b456, 10) = 10)
_assert_(charindex('d', @b456, 11) = 0)

_assert_(charindex('D', @b456 collate fr_cs_as, 0)  = 0)
_assert_(charindex('D', @b456 collate fr_cs_as, 10) = 0)
_assert_(charindex('D', @b456 collate fr_cs_as, 11) = 0)


_assert_(charindex('x', replicate('a', 7999) ) = 0)
_assert_(charindex('x', replicate('a', 7999) + 'x' ) = 8000)
_assert_(charindex('x', replicate('a', 7999) + 'x', 7999 ) = 8000)
_assert_(charindex('x', replicate('a', 7999) + 'x', 8000 ) = 8000)
_assert_(charindex('x', replicate('a', 7999) + 'x', 8001 ) = 0)


_assert_(charindex(cast(NULL as varchar), 'asdf', 1)              is null)
_assert_(charindex('HelloWorld', cast(NULL as varchar), 1)        is null)
_assert_(charindex('HelloWorld', 'HelloWorld', cast(NULL as int)) is null)

_assert_(charindex(NULL, 'asdf', 1)              is null)
_assert_(charindex('HelloWorld', NULL, 1)        is null)
_assert_(charindex('HelloWorld', 'HelloWorld', NULL) is null)


-- ========== SPACE() ============

_assert_('<' + space(0) + '>'  = '<>')
_assert_('<' + space(1) + '>'  = '< >')
_assert_('<' + space(10) + '>' = '<          >')

_assert_(charlen(space(8000)) = 8000)

_assert_('<' + left(space(8000), 1)  + '>'= '< >')
_assert_('<' + right(space(8000), 1) + '>'= '< >')

_assert_(space(cast(NULL as int)) is null)
_assert_(space(NULL) is null)

_assert_(charlen(space(8001000)) = 8000)

_assert_(space(-1) is null)


-- ========== REPLICATE() ============


_assert_(replicate(NULL, 9) is null)
_assert_(replicate('asdf', null) is null)

_assert_(replicate(cast(NULL as varchar), 9) is null)
_assert_(replicate('asdf', cast(NULL as int)) is null)


_assert_('<' + replicate('asdf', 0) + '>' = '<>')

_assert_('<' + replicate('', 6000) + '>' = '<>')

_assert_(len(replicate('', 6000)) = 0)

_assert_(len(replicate(replicate('a', 6000), 1)) = 6000)

_assert_(len(replicate(replicate('a', 2000), 4)) = 8000)

_assert_( '<' + substring(replicate('ab', 2000), 1999, 2) + '>' = '<ab>')

_assert_( '<' + replicate('ab', 1) + '>' = '<ab>')


declare @a3644 char(10) = 'ab'

_assert_(charlen(replicate(@a3644, 200)) = 2000)
_assert_(len(replicate(@a3644, 200)) = 1992)

_assert_('<' + substring(replicate(@a3644, 200), 1991, 2) + '>' = '<ab>')

_assert_(replicate(@a3644, 800) = replicate('ab        ', 800))

_assert_( '<' + substring(replicate(@a3644, 800), 7991, 10) + '>' = '<ab        >')


declare @b33 varchar(10) = 'ab'

_assert_(len(replicate(@b33, 200)) = 400)

_assert_('<' + substring(replicate(@b33, 200), 399, 2) + '>' = '<ab>')

_assert_(replicate(@b33, 800) = replicate('ab', 800))

_assert_( '<' + substring(replicate(@b33, 4000), 7991, 10) + '>' = '<ababababab>')



declare @r varchar(10) = e'\u0301abce'

_assert_(len(@r) = 5)
_assert_(unicode(substring(@r, 1, 1)) = 0x301)
_assert_(substring(@r, 2, 1)          = 'a')
_assert_(substring(@r, 3, 1)          = 'b')
_assert_(substring(@r, 4, 1)          = 'c')
_assert_(substring(@r, 5, 1)          = 'e')

_assert_('<' + replicate(e'\u0301abce', 4) + '>' = e'<\u0301' + 'abcéabcéabcéabce>')

_assert_(substring('<' + replicate(e'\u0301abce', 4) + '>', 3, 16) = 'abcéabcéabcéabce')

_assert_(len('<' + replicate(e'\u0301abce', 4) + '>') = 19)

_assert_(replicate('asdf', -1) is null)

_assert_(len(replicate('a', 8001)) = 8000)
_assert_(len(replicate(replicate('a', 8000), 2000000000)) = 8000)
_assert_(len(replicate('ab', 4001)) = 8000)
_assert_(len(replicate(replicate('a', 8000), 2)) = 8000)
_assert_(len(replicate(replicate('a', 8000), 8000)) = 8000)
_assert_(charlen(replicate(@a3644, 801)) = 8000)


-- ========== STUFF() ============

_assert_(stuff(cast(NULL as varchar), 1, 2, 'asdf') is null)
_assert_(stuff('asdf', cast(NULL as int), 2, 'asdf') is null)
_assert_(stuff('asdf', 1, cast(NULL as int), 'asdf') is null)
_assert_(stuff('helloworld', 0, 2, 'asdf') is null)
_assert_(stuff('helloworld', -1, 2, 'asdf') is null)
_assert_(stuff('helloworld', -1000, 2, 'asdf') is null)
_assert_(stuff('helloworld', 2, -1, 'asdf') is null)
_assert_(stuff('helloworld', 11, 0, 'asdf') is null)

_assert_(stuff('helloworld', 1, 2, NULL) = 'lloworld')
_assert_(stuff('', 1, 2, NULL) is null)
_assert_(stuff(NULL, 1, 2, NULL) is null)


_assert_(stuff('hello', 1, 0, 'XXXXXXXXXX') = 'XXXXXXXXXXhello')
_assert_(stuff('hello', 2, 0, 'XXXXXXXXXX') = 'hXXXXXXXXXXello')
_assert_(stuff('hello', 5, 0, 'XXXXXXXXXX') = 'hellXXXXXXXXXXo')
_assert_(stuff('hello', 6, 0, 'XXXXXXXXXX') is null)  -- unfortunately, it is not 'helloXXXXXXXXXX'. I just reproduce the behavior of MS SQL Server.
_assert_(stuff('hello', 7, 0, 'XXXXXXXXXX') is null)

_assert_(stuff('hello', 1, 1, 'XXXXXXXXXX') = 'XXXXXXXXXXello')
_assert_(stuff('hello', 2, 1, 'XXXXXXXXXX') = 'hXXXXXXXXXXllo')
_assert_(stuff('hello', 5, 1, 'XXXXXXXXXX') = 'hellXXXXXXXXXX')
_assert_(stuff('hello', 6, 1, 'XXXXXXXXXX') is null)  -- unfortunately, it is not 'helloXXXXXXXXXX'. I just reproduce the behavior of MS SQL Server.
_assert_(stuff('hello', 7, 1, 'XXXXXXXXXX') is null)

_assert_(stuff('hello', 1, 4, 'XXXXXXXXXX') = 'XXXXXXXXXXo')
_assert_(stuff('hello', 2, 3, 'XXXXXXXXXX') = 'hXXXXXXXXXXo')
_assert_(stuff('hello', 5, 1, 'XXXXXXXXXX') = 'hellXXXXXXXXXX')
_assert_(stuff('hello', 6, 1, 'XXXXXXXXXX') is null)  -- unfortunately, it is not 'helloXXXXXXXXXX'. I just reproduce the behavior of MS SQL Server.
_assert_(stuff('hello', 7, 1, 'XXXXXXXXXX') is null)

_assert_(stuff('hello', 1, 5, 'XXXXXXXXXX') = 'XXXXXXXXXX')
_assert_(stuff('hello', 2, 4, 'XXXXXXXXXX') = 'hXXXXXXXXXX')
_assert_(stuff('hello', 5, 1, 'XXXXXXXXXX') = 'hellXXXXXXXXXX')
_assert_(stuff('hello', 6, 1, 'XXXXXXXXXX') is null)  -- unfortunately, it is not 'helloXXXXXXXXXX'. I just reproduce the behavior of MS SQL Server.
_assert_(stuff('hello', 7, 1, 'XXXXXXXXXX') is null)

_assert_(stuff('hello', 1, 500, 'XXXXXXXXXX') = 'XXXXXXXXXX')
_assert_(stuff('hello', 2, 400, 'XXXXXXXXXX') = 'hXXXXXXXXXX')
_assert_(stuff('hello', 5, 100, 'XXXXXXXXXX') = 'hellXXXXXXXXXX')
_assert_(stuff('hello', 6, 100, 'XXXXXXXXXX') is null)  -- unfortunately, it is not 'helloXXXXXXXXXX'. I just reproduce the behavior of MS SQL Server.
_assert_(stuff('hello', 7, 100, 'XXXXXXXXXX') is null)

_assert_(stuff('hello', 1, 0, '') = 'hello')
_assert_(stuff('hello', 2, 0, '') = 'hello')
_assert_(stuff('hello', 5, 0, '') = 'hello')


-- ========== DATEPART() ============


set language french

declare @a73580 datetime = '20111214 17:30:15.123'  -- Wednesday

_assert_(datepart(year, @a73580)  = 2011)
_assert_(datepart(yy, @a73580)    = 2011)
_assert_(datepart(yyyy, @a73580)  = 2011)

_assert_(datepart(quarter, @a73580)  = 4)
_assert_(datepart(qq, @a73580)       = 4)
_assert_(datepart(q, @a73580)        = 4)

_assert_(datepart(month, @a73580)    = 12)
_assert_(datepart(mm, @a73580)       = 12)
_assert_(datepart(m, @a73580)        = 12)

_assert_(datepart(dayofyear, @a73580) = 348)
_assert_(datepart(dy, @a73580)        = 348)
_assert_(datepart(y, @a73580)         = 348)

_assert_(datepart(day, @a73580)       = 14)
_assert_(datepart(dd, @a73580)        = 14)
_assert_(datepart(d, @a73580)         = 14)

_assert_(datepart(week, @a73580)      = 51)
_assert_(datepart(wk, @a73580)        = 51)
_assert_(datepart(ww, @a73580)        = 51)

_assert_(datepart(weekday, @a73580)   = 3)
_assert_(datepart(dw, @a73580)        = 3)

_assert_(datepart(hour, @a73580)      = 17)
_assert_(datepart(hh, @a73580)        = 17)

_assert_(datepart(minute, @a73580)    = 30)
_assert_(datepart(mi, @a73580)        = 30)
_assert_(datepart(n, @a73580)         = 30)

_assert_(datepart(second, @a73580)    = 15)
_assert_(datepart(ss, @a73580)        = 15)
_assert_(datepart(s, @a73580)         = 15)

_assert_(datepart(millisecond, @a73580)    = 123)
_assert_(datepart(ms, @a73580)             = 123)


set @a73580 = '2007-04-21'
_assert_(datepart(week, @a73580)      = 16)
_assert_(datepart(weekday, @a73580)   =  6)


set language en_US

set datefirst 1
_assert_(datepart(week, @a73580)    = 16)
_assert_(datepart(weekday, @a73580) =  6)

set datefirst 2
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  5)

set datefirst 3
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  4)

set datefirst 4
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  3)

set datefirst 5
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  2)

set datefirst 6
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  1)

set datefirst 7
_assert_(datepart(week, @a73580)    = 16)
_assert_(datepart(weekday, @a73580) =  7)


set language french
_assert_(datepart(week, @a73580)      = 16)
_assert_(datepart(weekday, @a73580)   =  6)


set @a73580 = '2007-04-21'
_assert_(datepart(week, @a73580)      = 16)
_assert_(datepart(weekday, @a73580)   =  6)


set language french

set datefirst 1
_assert_(datepart(week, @a73580)    = 16)
_assert_(datepart(weekday, @a73580) =  6)

set datefirst 2
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  5)

set datefirst 3
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  4)

set datefirst 4
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  3)

set datefirst 5
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  2)

set datefirst 6
_assert_(datepart(week, @a73580)    = 17)
_assert_(datepart(weekday, @a73580) =  1)

set datefirst 7
_assert_(datepart(week, @a73580)    = 16)
_assert_(datepart(weekday, @a73580) =  7)



_assert_(datepart(WEEKdAy, @a73580)   =  7)


set language french
_assert_(@@datefirst = 1)

set datefirst 5
_assert_(@@datefirst = 5)

set language french
_assert_(@@datefirst = 1)

set language english
_assert_(@@datefirst = 7)

_assert_(datepart(week, '20070421')    = 16)

_assert_(datepart(qq, '20070121')  = 1)
_assert_(datepart(qq, '20070221')  = 1)
_assert_(datepart(qq, '20070321')  = 1)
_assert_(datepart(qq, '20070421')  = 2)
_assert_(datepart(qq, '20070521')  = 2)
_assert_(datepart(qq, '20070621')  = 2)
_assert_(datepart(qq, '20070721')  = 3)
_assert_(datepart(qq, '20070821')  = 3)
_assert_(datepart(qq, '20070921')  = 3)
_assert_(datepart(qq, '20071021')  = 4)
_assert_(datepart(qq, '20071121')  = 4)
_assert_(datepart(qq, '20071221')  = 4)


_assert_(datepart(week, NULL) is null)
_assert_(datepart(week, cast(NULL as datetime)) is null)



-- ========== DATENAME() ============


set language french

set @a73580 = '20111214 17:30:15.123'  -- Wednesday

_assert_(datename(year, @a73580)  = '2011')
_assert_(datename(yy, @a73580)    = '2011')
_assert_(datename(yyyy, @a73580)  = '2011')

_assert_(datename(quarter, @a73580)  = '4')
_assert_(datename(qq, @a73580)       = '4')
_assert_(datename(q, @a73580)        = '4')

_assert_(datename(month, @a73580)    = 'décembre')
_assert_(datename(mm, @a73580)       = 'décembre')
_assert_(datename(m, @a73580)        = 'décembre')

_assert_(datename(dayofyear, @a73580) = '348')
_assert_(datename(dy, @a73580)        = '348')
_assert_(datename(y, @a73580)         = '348')

_assert_(datename(day, @a73580)       = '14')
_assert_(datename(dd, @a73580)        = '14')
_assert_(datename(d, @a73580)         = '14')

_assert_(datename(week, @a73580)      = '51')
_assert_(datename(wk, @a73580)        = '51')
_assert_(datename(ww, @a73580)        = '51')

_assert_(datename(weekday, @a73580)   = 'mercredi')
_assert_(datename(dw, @a73580)        = 'mercredi')

_assert_(datename(hour, @a73580)      = '17')
_assert_(datename(hh, @a73580)        = '17')

_assert_(datename(minute, @a73580)    = '30')
_assert_(datename(mi, @a73580)        = '30')
_assert_(datename(n, @a73580)         = '30')

_assert_(datename(second, @a73580)    = '15')
_assert_(datename(ss, @a73580)        = '15')
_assert_(datename(s, @a73580)         = '15')

_assert_(datename(millisecond, @a73580)    = '123')
_assert_(datename(ms, @a73580)             = '123')


set @a73580 = '2007-04-21'
_assert_(datename(week, @a73580)      = '16')
_assert_(datename(weekday, @a73580)   =  'samedi')


set language en_US

set datefirst 1
_assert_(datename(week, @a73580)    = '16')
_assert_(datename(weekday, @a73580) =  'Saturday')

set datefirst 2
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'Saturday')

set datefirst 3
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'Saturday')

set datefirst 4
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'Saturday')

set datefirst 5
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'Saturday')

set datefirst 6
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'Saturday')

set datefirst 7
_assert_(datename(week, @a73580)    = '16')
_assert_(datename(weekday, @a73580) =  'Saturday')


set language french
_assert_(datename(week, @a73580)      = '16')
_assert_(datename(weekday, @a73580)   =  'samedi')


set @a73580 = '2007-04-21'
_assert_(datename(week, @a73580)      = '16')
_assert_(datename(weekday, @a73580)   =  'samedi')


set language french

set datefirst 1
_assert_(datename(week, @a73580)    = '16')
_assert_(datename(weekday, @a73580) =  'samedi')

set datefirst 2
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'samedi')

set datefirst 3
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'samedi')

set datefirst 4
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'samedi')

set datefirst 5
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'samedi')

set datefirst 6
_assert_(datename(week, @a73580)    = '17')
_assert_(datename(weekday, @a73580) =  'samedi')

set datefirst 7
_assert_(datename(week, @a73580)    = '16')
_assert_(datename(weekday, @a73580) =  'samedi')


_assert_(datename(WEEKdAy, @a73580)   =  'samedi')


set language french
_assert_(@@datefirst = 1)

set datefirst 5
_assert_(@@datefirst = 5)

set language french
_assert_(@@datefirst = 1)

set language english
_assert_(@@datefirst = 7)


_assert_(datename(week, '20070421')  = '16')

_assert_(datename(qq, '20070121')  = '1')
_assert_(datename(qq, '20070221')  = '1')
_assert_(datename(qq, '20070321')  = '1')
_assert_(datename(qq, '20070421')  = '2')
_assert_(datename(qq, '20070521')  = '2')
_assert_(datename(qq, '20070621')  = '2')
_assert_(datename(qq, '20070721')  = '3')
_assert_(datename(qq, '20070821')  = '3')
_assert_(datename(qq, '20070921')  = '3')
_assert_(datename(qq, '20071021')  = '4')
_assert_(datename(qq, '20071121')  = '4')
_assert_(datename(qq, '20071221')  = '4')

_assert_(datename(week, NULL) is null)
_assert_(datename(week, cast(NULL as datetime)) is null)


-- ========== DATENAME() 2 ============

DECLARE @datetime2 datetime2 = '2007-01-01 13:10:10.1111111'
_assert_( 'year       ' + cast(dateadd(year,1,@datetime2) as varchar)     = 'year       2008-01-01 13:10:10.111')

_assert_('quarter     ' + cast(dateadd(quarter,1,@datetime2) as varchar)  = 'quarter     2007-04-01 13:10:10.111')

_assert_('month       ' + cast(dateadd(month,1,@datetime2) as varchar)    = 'month       2007-02-01 13:10:10.111')

_assert_('dayofyear   ' + cast(dateadd(dayofyear,1,@datetime2) as varchar)= 'dayofyear   2007-01-02 13:10:10.111')

_assert_('day         ' + cast(dateadd(day,1,@datetime2) as varchar)      = 'day         2007-01-02 13:10:10.111')

_assert_('week        ' + cast(dateadd(week,1,@datetime2) as varchar)     = 'week        2007-01-08 13:10:10.111')

_assert_('weekday     ' + cast(dateadd(weekday,1,@datetime2) as varchar)  = 'weekday     2007-01-02 13:10:10.111')

_assert_('hour        ' + cast(dateadd(hour,1,@datetime2) as varchar)     = 'hour        2007-01-01 14:10:10.111')

_assert_('minute      ' + cast(dateadd(minute,1,@datetime2) as varchar)   = 'minute      2007-01-01 13:11:10.111')

_assert_('second      ' + cast(dateadd(second,1,@datetime2) as varchar)   = 'second      2007-01-01 13:10:11.111')

_assert_('millisecond ' + cast(dateadd(millisecond,1,@datetime2) as varchar) = 'millisecond 2007-01-01 13:10:10.112')


-- ========== CONVERT VARCHAR to DATETIME ============

set language french


_assert_(convert(datetime, '1980.03.01 10:30') = '19800301 10:30')
_assert_(convert(datetime, '  1980 .03  .  01   10   : 30   ') = '19800301 10:30')
_assert_(convert(datetime, '1980.03.01     10:30') = '19800301 10:30')



declare @a11172 varchar(30) = '01-06-2011'

_assert_(convert(datetime, @a11172)      = '20110601') -- language default
_assert_(convert(datetime, @a11172, 101) = '20110106') -- mdy
_assert_(convert(datetime, @a11172, 103) = '20110601') -- dmy


_assert_(convert(datetime, cast(NULL as varchar), 121) is null)
_assert_(convert(datetime, @a11172, cast(NULL as int))      is null)
_assert_(convert(datetime, @a11172, NULL)                   is null)

_assert_(convert(datetime, NULL, 121) is null)  -- cast VOID to DATETIME



_assert_(convert(datetime, '20070421', 121) = '20070421')
_assert_(convert(datetime, '   20070421   ', 121) = '20070421')

_assert_(convert(datetime, '', 121)  = '1900-01-01 00:00:00.000')
_assert_(convert(datetime, ' ', 121) = '1900-01-01 00:00:00.000')
_assert_(convert(datetime, '                                     ', 121) = '1900-01-01 00:00:00.000')


set dateformat mdY
_assert_(convert(datetime, @a11172)      = '20110106')

set dateformat DMY
_assert_(convert(datetime, @a11172)      = '20110601')

set dateformat mdY
_assert_(convert(datetime, @a11172)      = '20110106')

set dateformat DMY
_assert_(convert(datetime, @a11172)      = '20110601')

set dateformat mdY
_assert_(convert(datetime, @a11172)      = '20110106')


set language en_US

_assert_(convert(datetime, @a11172)      = '20110106') -- language default

set language fr_ch

_assert_(convert(datetime, @a11172)      = '20110601') -- language default

set language en_US

_assert_(convert(datetime, @a11172)      = '20110106') -- language default

set language fr_ch

_assert_(convert(datetime, @a11172)      = '20110601') -- language default



set @a11172 = '2134 - 06    -    12  13  : 20  '

_assert_(convert(datetime, @a11172)      = '21340612 13:20')

_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.1234567'), 121)   = '2000-01-01 10:15:22.123')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.123456'), 121)   = '2000-01-01 10:15:22.123')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.12345'), 121)   = '2000-01-01 10:15:22.123')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.1234'), 121)   = '2000-01-01 10:15:22.123')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.1235'), 121)   = '2000-01-01 10:15:22.123')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.123'), 121)   = '2000-01-01 10:15:22.123')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.12'), 121)   = '2000-01-01 10:15:22.120')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.1'), 121)   = '2000-01-01 10:15:22.100')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22.5'), 121)   = '2000-01-01 10:15:22.500')
_assert_(convert(varchar, convert(datetime, '2000-01-01 10:15:22'), 121)   = '2000-01-01 10:15:22.000')


set @a11172 = '2134 - 06    -    12  13  : 20  '

_assert_(convert(datetime, @a11172, 121) = '2134-06-12 13:20:00.000')


-- ========== CONVERT VARCHAR to DATETIME error ============

_assert_error_(convert(datetime, '2011-12345678-01'), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== CONVERT VARCHAR to DATETIME error ============

_assert_error_(convert(datetime, '2011 - 06    -    12  13  :'), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== CONVERT VARCHAR to DATETIME error ============

_assert_error_(convert(datetime, '1980 03 01 10:30'), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== CONVERT VARCHAR to DATETIME error ============

_assert_error_(convert(datetime, '20110101 1:2:3.4.5'), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== CONVERT VARCHAR to DATETIME error ============

_assert_error_(convert(datetime, 'a', 121), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== CONVERT VARCHAR to DATETIME error ============

_assert_error_(convert(datetime, '20110101 1:2.5'), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== CONVERT DATETIME to VARCHAR ============

set language en_us

declare @a774 datetime = '20110101 10:39:59.600'

_assert_(convert(varchar, @a774, 100) = 'Jan 1 2011 10:39AM')
_assert_(convert(varchar, @a774, 101) = '01/01/2011')
_assert_(convert(varchar, @a774, 102) = '2011.01.01')
_assert_(convert(varchar, @a774, 103) = '01/01/2011')
_assert_(convert(varchar, @a774, 104) = '01.01.2011')
_assert_(convert(varchar, @a774, 105) = '01-01-2011')
_assert_(convert(varchar, @a774, 106) = '01 Jan 2011')
_assert_(convert(varchar, @a774, 107) = 'Jan 01, 2011')
_assert_(convert(varchar, @a774, 108) = '10:39:59')
_assert_(convert(varchar, @a774, 109) = 'Jan 1 2011 10:39:59.600AM')
_assert_(convert(varchar, @a774, 110) = '01-01-2011')
_assert_(convert(varchar, @a774, 111) = '2011/01/01')
_assert_(convert(varchar, @a774, 112) = '20110101')
_assert_(convert(varchar, @a774, 113) = '01 Jan 2011 10:39:59.600')
_assert_(convert(varchar, @a774, 114) = '10:39:59.600')
_assert_(convert(varchar, @a774, 120) = '2011-01-01 10:39:59')
_assert_(convert(varchar, @a774, 121) = '2011-01-01 10:39:59.600')
_assert_(convert(varchar, @a774, 126) = '2011-01-01T10:39:59.600')


set language fr_ch

_assert_(convert(varchar, @a774, 100) = 'janv. 1 2011 10:39AM')
_assert_(convert(varchar, @a774, 106) = '01 janv. 2011')
_assert_(convert(varchar, @a774, 107) = 'janv. 01, 2011')
_assert_(convert(varchar, @a774, 109) = 'janv. 1 2011 10:39:59.600AM')
_assert_(convert(varchar, @a774, 113) = '01 janv. 2011 10:39:59.600')

set language en_us

_assert_(convert(varchar, @a774, 100) = 'Jan 1 2011 10:39AM')
_assert_(convert(varchar, @a774, 106) = '01 Jan 2011')
_assert_(convert(varchar, @a774, 107) = 'Jan 01, 2011')
_assert_(convert(varchar, @a774, 109) = 'Jan 1 2011 10:39:59.600AM')
_assert_(convert(varchar, @a774, 113) = '01 Jan 2011 10:39:59.600')

set language fr_ch

_assert_(convert(varchar, @a774, 100) = 'janv. 1 2011 10:39AM')
_assert_(convert(varchar, @a774, 106) = '01 janv. 2011')
_assert_(convert(varchar, @a774, 107) = 'janv. 01, 2011')
_assert_(convert(varchar, @a774, 109) = 'janv. 1 2011 10:39:59.600AM')
_assert_(convert(varchar, @a774, 113) = '01 janv. 2011 10:39:59.600')


-- ========== CONVERT DATE to VARCHAR ============

set language en_us

declare @a121 date = '20110101 10:39:59.600'

_assert_(convert(varchar, @a121, 100) = 'Jan 1 2011')
_assert_(convert(varchar, @a121, 101) = '01/01/2011')
_assert_(convert(varchar, @a121, 102) = '2011.01.01')
_assert_(convert(varchar, @a121, 103) = '01/01/2011')
_assert_(convert(varchar, @a121, 104) = '01.01.2011')
_assert_(convert(varchar, @a121, 105) = '01-01-2011')
_assert_(convert(varchar, @a121, 106) = '01 Jan 2011')
_assert_(convert(varchar, @a121, 107) = 'Jan 01, 2011')
--_assert_(convert(varchar, @a121, 108) = '10:39:59'
_assert_(convert(varchar, @a121, 109) = 'Jan 1 2011')
_assert_(convert(varchar, @a121, 110) = '01-01-2011')
_assert_(convert(varchar, @a121, 111) = '2011/01/01')
_assert_(convert(varchar, @a121, 112) = '20110101')
_assert_(convert(varchar, @a121, 113) = '01 Jan 2011')
--_assert_(convert(varchar, @a121, 114) = '10:39:59.600'
_assert_(convert(varchar, @a121, 120) = '2011-01-01')
_assert_(convert(varchar, @a121, 121) = '2011-01-01')
_assert_(convert(varchar, @a121, 126) = '2011-01-01')


-- ========== CONVERT TIME to VARCHAR ============

set language en_us

declare @a556 time = '20110101 10:39:59.600'

_assert_(convert(varchar, @a556, 100) = '10:39AM')
_assert_(convert(varchar, @a556, 108) = '10:39:59')
_assert_(convert(varchar, @a556, 109) = '10:39:59.600AM')
_assert_(convert(varchar, @a556, 113) = '10:39:59.600')
_assert_(convert(varchar, @a556, 114) = '10:39:59.600')
_assert_(convert(varchar, @a556, 120) = '10:39:59')
_assert_(convert(varchar, @a556, 121) = '10:39:59.600')
_assert_(convert(varchar, @a556, 126) = '10:39:59.600')



-- ========== DATEADD() ============


_assert_(dateadd(yy, NULL, '20110531') is null)
_assert_(dateadd(yy, 3, NULL) is null)


_assert_(dateadd(yy, 1, '20110531') = '20120531')
_assert_(dateadd(mm, 1, '20110531') = '20110630')
_assert_(dateadd(dd, 1, '20110531') = '20110601')

_assert_(dateadd(yy, 100, '20110531')    = '21110531')
_assert_(dateadd(mm, 12, 0) = '19010101')
_assert_(dateadd(mm, 15367, 0) = '31800801')
_assert_(dateadd(mm, -3, '20110531') = '20110228')
_assert_(dateadd(mm, -3, '20040531') = '20040229')
_assert_(dateadd(mm,  8, '20110506') = '20120106')
_assert_(dateadd(dd, 100000, '20110531') = '22850315')


_assert_(dateadd(hh, 1, '20110531 10:35:53.123')  = '2011-05-31 11:35:53.123')
_assert_(dateadd(mi, 1, '20110531 10:35:53.123')  = '2011-05-31 10:36:53.123')
_assert_(dateadd(ss, 1, '20110531 10:35:53.123')  = '2011-05-31 10:35:54.123')
_assert_(dateadd(ms, 1, '20110531 10:35:53.123')  = '2011-05-31 10:35:53.124')

_assert_(dateadd(hh, 24, '20110531 10:35:53.123')  = '2011-06-01 10:35:53.123')
_assert_(dateadd(mi, 60, '20110531 10:35:53.123')  = '2011-05-31 11:35:53.123')
_assert_(dateadd(ss, 60, '20110531 10:35:53.123')  = '2011-05-31 10:36:53.123')
_assert_(dateadd(ms, 60, '20110531 10:35:53.123')  = '2011-05-31 10:35:53.183')

_assert_(dateadd(hh, 1356, '20110531 10:35:53.123')  = '2011-07-26 22:35:53.123')
_assert_(dateadd(mi, 1356, '20110531 10:35:53.123')  = '2011-06-01 09:11:53.123')
_assert_(dateadd(ss, 1356, '20110531 10:35:53.123')  = '2011-05-31 10:58:29.123')
_assert_(dateadd(ms, 1356, '20110531 10:35:53.123')  = '2011-05-31 10:35:54.479')


_assert_(dateadd(quarter, 10, '20110531 10:35:53.123') = '20131130 10:35:53.123')
_assert_(dateadd(quarter, 10, '20110630 10:35:53.123') = '20131230 10:35:53.123')
_assert_(dateadd(quarter, 10, '20110731 10:35:53.123') = '20140131 10:35:53.123')
_assert_(dateadd(quarter, 1356, '20110531 10:35:53.123') = '2350-05-31 10:35:53.123')
_assert_(dateadd(week, 1356, '20110531 10:35:53.123')    = '2037-05-26 10:35:53.123')


-- ========== DATEADD() error ============

_assert_error_(dateadd(yy, '20110531', 1), 'DATETIME_OVERFLOW')


-- ========== DATEADD() error ============

_assert_error_(dateadd(yy, 20110531, 1), 'DATETIME_OVERFLOW')


-- ========== DATEADD() error ============

_assert_error_(dateadd(yy, 10000, '20110531'), 'DATETIME_OVERFLOW')


-- ========== DATEADD() error ============

_assert_error_(dateadd(yy, 5000000, '20110531'), 'DATETIME_OVERFLOW')


-- ========== DATEADD() error ============

_assert_error_(dateadd(yy, 6000000, '20110531'), 'DATETIME_OVERFLOW')


-- ========== DATEADD() error ============

_assert_error_(dateadd(yy, 1000000000, '20110531'), 'DATETIME_OVERFLOW')


-- ========== DATEADD() error ============

_assert_error_(dateadd(dd, 1000000000, '20110531'), 'DATETIME_OVERFLOW')


-- ========== DATEADD() error ============

_assert_error_(dateadd(mm, 1000000000, '20110531'), 'DATETIME_OVERFLOW')


-- ========== DATEDIFF() ============

_assert_(datediff(yyyy, cast(NULL as datetime), '20110101') is null)
_assert_(datediff(yyyy, cast('20110101' as datetime), cast(NULL as datetime)) is null)

_assert_(datediff(yyyy, '20110101', '20100101') = - 1)
_assert_(datediff(yyyy, '20110101', '20101231') = -1)

_assert_(datediff(month, '20110101', '20100101') = - 12)
_assert_(datediff(MONth, '20110101', '20101231') = -1)
_assert_(datediff(MONth, '99991231 23:59:59.999', '18000101') = -98399)

_assert_(datediff(D,  '20110101 10:30', '20100101 23:55') = -365)
_assert_(datediff(y,  '20110101 10:30', '20101231 23:55') = -1)
_assert_(datediff(dY, '20110101 10:30', '20100101 23:55') = -365)
_assert_(datediff(dD, '20110101 10:30', '20101231 23:55') = -1)
_assert_(datediff(dD, '20110101', '20101231 23:59:59.999') = -1)
_assert_(datediff(dD, '20101231 23:59:59.999', '20101231 23:59:59.999') = 0)

_assert_(datediff(hh, '20110101 10:30', '20100101 23:55') = -8746)
_assert_(datediff(hh, '20110101 10:30', '20101231 23:55') = -10)
_assert_(datediff(hh, '20110101', '20101231 23:59:59.999') = 0)
_assert_(datediff(hh, '20101231 23:59:59.999', '20101231 23:59:59.999') = 0)

_assert_(datediff(mi, '20110101 10:30', '20100101 23:55') = -524795)
_assert_(datediff(mi, '20110101 10:30', '20101231 23:55') = -635)
_assert_(datediff(mi, '20110101', '20101231 23:59:59.999') = 0)
_assert_(datediff(mi, '20101231 23:59:59.999', '20101231 23:59:59.999') = 0)
_assert_(datediff(mi, '20101231 23:59', '20101231 23:59:59.999') = 0)

_assert_(datediff(ss, '20110101 10:30', '20100101 23:55') = -31487700)
_assert_(datediff(ss, '20110101 10:30', '20101231 23:55') = -38100)
_assert_(datediff(ss, '20110101', '20101231 23:59:59.999')= 0)
_assert_(datediff(ss, '20101231 23:59:59.999', '20101231 23:59:59.999') = 0)
_assert_(datediff(ss, '20101231 23:59', '20101231 23:59:59.999') = 59)

_assert_(datediff(ms, '20100101 10:30', '20100101 23:55')  = 48300000)
_assert_(datediff(ms, '20110101 10:30', '20101231 23:55')  = -38100000)
_assert_(datediff(ms, '20110101', '20101231 23:59:59.999') = -1)
_assert_(datediff(ms, '20101231 23:59:59.999', '20101231 23:59:59.999') = 0)
_assert_(datediff(ms, '20101231 23:59', '20101231 23:59:59.999') = 59999)



declare @aaa time = '13:00'
declare @bbb time = '14:30'


declare @ad date = '20110201'
declare @bd date = '20110318'

_assert_(datediff(mi, @bd, @ad) = -64800)


-- ========== DATEDIFF() error ============

_assert_error_(datediff(ms, '20110101 10:30', '20100101 23:55'), 'DATETIME_DATEDIFF_OVERFLOW')


-- ========== ISNUMERIC() ============

_assert_(isnumeric('asdf') = 0)
_assert_(isnumeric('') = 0)
_assert_(isnumeric(' ') = 0)
_assert_(isnumeric('    ') = 0)
_assert_(isnumeric('123a') = 0)
_assert_(isnumeric(cast(NULL as varchar)) = 0)

_assert_(isnumeric('123') = 1)
_assert_(isnumeric('+.123') = 1)
_assert_(isnumeric('-.123') = 1)
_assert_(isnumeric('.123') = 1)
_assert_(isnumeric('    .123    ') = 1)
_assert_(isnumeric('+000.123') = 1)
_assert_(isnumeric('0.123e123') = 1)
_assert_(isnumeric('0.123e-123') = 1)
_assert_(isnumeric('1') = 1)
_assert_(isnumeric('$1.') = 0)


_assert_(isnumeric(cast(0 as bit)) = 1)
_assert_(isnumeric(cast(10 as bit)) = 1)
_assert_(isnumeric(cast(NULL as bit)) = 0)

_assert_(isnumeric(cast(0 as tinyint)) = 1)
_assert_(isnumeric(cast(10 as tinyint)) = 1)
_assert_(isnumeric(cast(NULL as tinyint)) = 0)

_assert_(isnumeric(cast(0 as smallint)) = 1)
_assert_(isnumeric(cast(10 as smallint)) = 1)
_assert_(isnumeric(cast(NULL as smallint)) = 0)

_assert_(isnumeric(cast(0 as int)) = 1)
_assert_(isnumeric(cast(10 as int)) = 1)
_assert_(isnumeric(cast(NULL as int)) = 0)

_assert_(isnumeric(cast(0 as bigint)) = 1)
_assert_(isnumeric(cast(10 as bigint)) = 1)
_assert_(isnumeric(cast(NULL as bigint)) = 0)

_assert_(isnumeric(cast(0 as money)) = 1)
_assert_(isnumeric(cast(10 as money)) = 1)
_assert_(isnumeric(cast(NULL as money)) = 0)

_assert_(isnumeric(cast(0 as numeric)) = 1)
_assert_(isnumeric(cast(10 as numeric)) = 1)
_assert_(isnumeric(cast(NULL as numeric)) = 0)

_assert_(isnumeric(cast(0 as float)) = 1)
_assert_(isnumeric(cast(10 as float)) = 1)
_assert_(isnumeric(cast(NULL as float)) = 0)


-- ========== ISDATE() ============


_assert_(isdate('asdf') = 0)
_assert_(isdate('') = 0)
_assert_(isdate(' ') = 0)
_assert_(isdate('    ') = 0)
_assert_(isdate('123a') = 0)
_assert_(isdate(cast(NULL as varchar)) = 0)

set language french
_assert_(isdate('20110101') = 1)
_assert_(isdate('25-12-2011') = 1)

set language en_US
_assert_(isdate('25-12-2011') = 0) -- not a date

set dateformat dmy
_assert_(isdate('25-12-2011') = 1) -- is a date

set dateformat mdy
_assert_(isdate('25-12-2011') = 0) -- not a date

set dateformat dmy
_assert_(isdate('25-12-2011') = 1) -- is a date
_assert_(isdate('  25.12.2011 10:30  ') = 1) -- is a date



-- ========== ISNULL() ============


_assert_null_(isnull(NULL, NULL) =  100)


_assert_(isnull('hello', 'worldxxxxx') = 'hello')
_assert_(isnull(cast(NULL as varchar(80)), 'worldxxxxx') = 'worldxxxxx')

_assert_null_(isnull(cast(NULL as varchar(80)), NULL) = 10)

_assert_(isnull(123, 456.8f)  = 123)
_assert_(isnull(cast(NULL as int), 456.8f)   = 456)
_assert_(isnull(cast(NULL as int), 1111.222) = 1111)


_assert_(isnull(cast('20110101' as date), cast(1 as datetime)) = '20110101')
_assert_(isnull(cast(NULL as date), cast(1 as datetime))       = '19000102')


_assert_(isnull(0x1234, 0x88889999) = 0x1234)
_assert_(isnull(cast(NULL as varbinary(80)), 0x88889999) = 0x88889999)

_assert_(isnull('hello', 'worldxxxxx') = 'hello')
_assert_(isnull(cast(NULL as varchar(80)), 'worldxxxxx') = 'worldxxxxx')

_assert_(isnull(cast(1 as bit), cast(0 as bit)) = 1)
_assert_(isnull(cast(1 as bit), cast(1 as bit)) = 1)
_assert_(isnull(cast(NULL as bit), cast(1 as bit)) = 1)
_assert_(isnull(cast(NULL as bit), cast(0 as bit)) = 0)

_assert_(isnull(cast(10 as tinyint), cast(200 as tinyint)) = 10)
_assert_(isnull(cast(NULL as tinyint), cast(200 as tinyint)) = 200)

_assert_(isnull(cast(10 as smallint), cast(200 as smallint)) = 10)
_assert_(isnull(cast(NULL as smallint), cast(200 as smallint)) = 200)

_assert_(isnull(cast(10 as int), cast(200 as int)) = 10)
_assert_(isnull(cast(NULL as int), cast(200 as int)) = 200)

_assert_(isnull(cast(10 as bigint), cast(200 as bigint)) = 10)
_assert_(isnull(cast(NULL as bigint), cast(200 as bigint)) = 200)

_assert_(isnull(cast(10 as money), cast(200 as money)) = 10)
_assert_(isnull(cast(NULL as money), cast(200 as money)) = 200)

_assert_(isnull(cast(10 as numeric(2)), cast(12345 as numeric(5))) = 10d)
_assert_(isnull(cast(NULL as numeric(2)), cast(12345 as numeric(5))) = 12345d)

_assert_(isnull(123.456, 12345.67890123) = 123.456)
_assert_(isnull(12345678901234567890.456, 12345.67890123) = 12345678901234567890.456)
_assert_(isnull(cast(NULL as numeric(9,3)), 1234556489982727667647.67890123) = 1234556489982727667647.678901)


_assert_(isnull(cast(10f as float), cast(200f as float)) = 10f)
_assert_(isnull(cast(NULL as float), cast(200f as float)) = 200f)

_assert_(isnull(cast('20110101' as date), cast('23450101' as date)) = '20110101')
_assert_(isnull(cast(NULL as date), cast('23450101' as date)) = '23450101')

_assert_(isnull(cast('20110101 10:30' as time), cast('23450101 20:50:05.123' as time)) = '   10:30  ')
_assert_(isnull(cast(NULL as time), cast('23450101 20:50:05.123' as time)) = '  19000101  20:50:05.123  ')

_assert_(isnull(cast('20110101 10:30' as datetime), cast('23450101 20:50:05.123' as datetime)) = ' 2011-01-01   10:30  ')
_assert_(isnull(cast(NULL as datetime), cast('  23450101   20:50:05.123' as datetime)) = '  23450101  20:50:05.123  ')

_assert_(isnull(NULL, 'worldxxxxx') = 'worldxxxxx')

_assert_(isnull(NULL, 1234) = 1234)


-- ========== NULLIF() ============

_assert_(nullif(NULL, NULL) is null)


_assert_(nullif(123, 456.8f)  = 123)
_assert_(nullif(456, 456.8f)  = 456)
_assert_(nullif(456, 456.f)  is null)


_assert_(nullif(cast('20110101' as date), cast(1 as datetime)) = '20110101')
_assert_(nullif(cast(NULL as date), cast(1 as datetime))  is null)


_assert_(nullif(cast(NULL as varbinary(80)), 0x88889999) is null)
_assert_(nullif(0x1234, cast(NULL as varbinary)) = 0x1234)
_assert_(nullif(0x1234, NULL) = 0x1234)
_assert_(nullif(0x1234, 0x88889999) = 0x1234)
_assert_(nullif(0x1234, 0x1234) is null)

_assert_(nullif(cast(NULL as char(10)), 'worldxxxxx') is null)
_assert_(nullif(cast('hello' as char(10)), cast(NULL as varchar(80))) = 'hello')
_assert_(nullif(cast('hello' as char(10)), 'worldxxxxx') = 'hello')
_assert_(nullif(cast('hello' as char(10)), 'hello') is null)
_assert_(nullif(cast('hello' as char(10)), 'HELLO' collate fr_ci_ai) is null)


declare @xx varchar(20)  = 'HELLO'
_assert_(nullif(cast('hello' as char(10)), @xx collate fr_ci_ai) is null) 

_assert_(nullif(@xx, cast('hello'  collate fr_cs_as as char(10))) = 'HELLO') 
_assert_(nullif(@xx, cast('hello'  collate fr_cs_as as char(10))) != 'hELLO'  collate fr_cs_as)


_assert_(nullif(cast(NULL as varchar(80)), 'worldxxxxx') is null)
_assert_(nullif('hello', cast(NULL as varchar(80))) = 'hello')
_assert_(nullif('hello', 'worldxxxxx') = 'hello')
_assert_(nullif('hello', 'hello') is null)
_assert_(nullif('hello', 'HELLo') is null)

_assert_(nullif(cast(NULL as bit), cast(0 as bit)) is null)
_assert_(nullif(cast(1 as bit), cast(NULL as bit)) = 1)
_assert_(nullif(cast(0 as bit), cast(NULL as bit)) = 0)
_assert_(nullif(cast(1 as bit), cast(0 as bit)) = 1)
_assert_(nullif(cast(0 as bit), cast(1 as bit)) = 0)
_assert_(nullif(cast(1 as bit), cast(1 as bit)) is null)
_assert_(nullif(cast(0 as bit), cast(0 as bit)) is null)

_assert_(nullif(cast(NULL as tinyint), cast(200 as tinyint)) is null)
_assert_(nullif(cast(10 as tinyint), cast(NULL as tinyint)) = 10)
_assert_(nullif(cast(10 as tinyint), cast(200 as tinyint)) = 10)
_assert_(nullif(cast(200 as tinyint), cast(200 as tinyint)) is null)

_assert_(nullif(cast(NULL as smallint), cast(200 as smallint)) is null)
_assert_(nullif(cast(10 as smallint), cast(NULL as smallint)) = 10)
_assert_(nullif(cast(10 as smallint), cast(200 as smallint)) = 10)
_assert_(nullif(cast(200 as smallint), cast(200 as smallint)) is null)

_assert_(nullif(cast(NULL as int), cast(200 as int)) is null)
_assert_(nullif(cast(10 as int), cast(NULL as int)) = 10)
_assert_(nullif(cast(10 as int), cast(200 as int)) = 10)
_assert_(nullif(cast(200 as int), cast(200 as int)) is null)

_assert_(nullif(cast(NULL as bigint), cast(200 as bigint)) is null)
_assert_(nullif(cast(10 as bigint), cast(NULL as bigint)) = 10)
_assert_(nullif(cast(10 as bigint), cast(200 as bigint)) = 10)
_assert_(nullif(cast(200 as bigint), cast(200 as bigint)) is null)

_assert_(nullif(cast(NULL as money), cast(200 as money)) is null)
_assert_(nullif(cast(10 as money), cast(NULL as money)) = 10)
_assert_(nullif(cast(10 as money), cast(200 as money)) = 10)
_assert_(nullif(cast(200 as money), cast(200 as money)) is null)


_assert_(nullif(cast(NULL as numeric(2)), cast(12345 as numeric(5))) is null)
_assert_(nullif(cast(10 as numeric(2)), cast(NULL as numeric(5))) = 10d)
_assert_(nullif(cast(10 as numeric(2)), cast(12345 as numeric(5))) = 10d)
_assert_(nullif(cast(12345 as numeric(10)), cast(12345.0 as numeric(7, 2))) is null)

_assert_(nullif(12345678901234567890.456, 12345.67890123) = 12345678901234567890.456)
_assert_(nullif(1234556489982727667647.67890123, 1234556489982727667647.67890123) is null)


_assert_(nullif(cast(NULL as float), cast(200f as float)) is null)
_assert_(nullif(cast(10f as float), cast(NULL as float)) = 10f)
_assert_(nullif(cast(10f as float), cast(200f as float)) = 10f)
_assert_(nullif(200f, cast(200f as float)) is null)

_assert_(nullif(cast(NULL as date), cast('23450101' as date)) is null)
_assert_(nullif(cast('20110101' as date), cast(NULL as date)) = '20110101')
_assert_(nullif(cast('20110101' as date), cast('23450101' as date)) = '20110101')
_assert_(nullif(cast('23450101' as date), cast('23450101' as date)) is null)

_assert_(nullif(cast(NULL as time), cast('23450101 20:50:05.123' as time)) is null)
_assert_(nullif(cast('20110101 10:30' as time), cast(NULL as time)) = '   20110101 10:30   ')
_assert_(nullif(cast('20110101 10:30' as time), cast('23450101 20:50:05.123' as time)) = '   20110101 10:30   ')
_assert_(nullif(cast('23450101 20:50:05.123' as time), cast('23450101 20:50:05.123' as time)) is null)

_assert_(nullif(cast(NULL as datetime), cast('  23450101   20:50:05.123' as datetime)) is null)
_assert_(nullif(cast('  23450101   20:50:05.123' as datetime), cast(NULL as datetime)) = '  23450101   20:50:05.123')
_assert_(nullif(cast('20110101 10:30' as datetime), cast('23450101 20:50:05.123' as datetime)) = '20110101 10:30')
_assert_(nullif(cast('  23450101   20:50:05.123' as datetime), cast('  23450101   20:50:05.123' as datetime)) is null)


-- ========== ISNULL() ============

_assert_(isnull(NULL, null) is null)

_assert_(isnull(cast(0x12 as varbinary(10)), 0x75649) = 0x12)
_assert_(isnull(cast(null as varbinary(10)), 0x75649) = 0x75649)

_assert_(isnull(cast('hello' as varchar(10)), 'aaa') = 'hello')
_assert_(isnull(cast(null as varchar(10)), 'aaa') = 'aaa')
_assert_error_(isnull(cast(1/0 as varchar(10)), 'aaa'), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10 as bit), 0) = 1)
_assert_(isnull(cast(null as bit), 0) = 0)
_assert_error_(isnull(cast(1/0 as bit), 20), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10 as tinyint), 20) = 10)
_assert_(isnull(cast(null as tinyint), 20) = 20)
_assert_error_(isnull(cast(1/0 as tinyint), 20), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10 as smallint), 20) = 10)
_assert_(isnull(cast(null as smallint), 20) = 20)
_assert_error_(isnull(cast(1/0 as smallint), 20), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10 as int), 20) = 10)
_assert_(isnull(cast(null as int), 20) = 20)
_assert_error_(isnull(cast(1/0 as int), 20), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10 as bigint), 20) = 10)
_assert_(isnull(cast(null as bigint), 20) = 20)
_assert_error_(isnull(cast(1/0 as bigint), 20), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10.8 as money), 20.5) = 10.8)
_assert_(isnull(cast(null as money), 20.5) = 20.5)
_assert_error_(isnull(cast(1/0 as money), 20.5), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10.8 as numeric(12,2)), 22.6) = 10.8)
_assert_(isnull(cast(null as numeric(12,2)), 22.6) = 22.6)
_assert_error_(isnull(cast(1/0 as numeric(12,2)), 22.6), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast(10.5 as float), 20.5) = 10.5)
_assert_(isnull(cast(null as float), 20.5) = 20.5)
_assert_error_(isnull(cast(1/0 as float), 20.5), 'SQLDATA_INT_DIVIDE_BY_ZERO')

_assert_(isnull(cast('20120102' as date), '33330303') = '20120102')
_assert_(isnull(cast(null as date), '33330303') = '33330303')
_assert_error_(isnull(cast('xxxx' as date), '33330303'), 'VARCHAR_CONVERT_DATE_FAILED')

_assert_(isnull(cast('13:24' as time), '11:11') = '13:24')
_assert_(isnull(cast(null as time), '11:11') = '11:11')
_assert_error_(isnull(cast('xxxx' as time), '11:11'), 'VARCHAR_CONVERT_TIME_FAILED')

_assert_(isnull(cast('20120102' as datetime), '33330303') = '20120102')
_assert_(isnull(cast(null as datetime), '33330303') = '33330303')
_assert_error_(isnull(cast('xxxx' as datetime), '33330303'), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== IIF() ============

_assert_(iif(1=1, null, null) is null)
_assert_(iif(1=0, null, null) is null)
_assert_(iif(1=null, null, null) is null)

_assert_(iif(1=1, 0xa123, 0x435267) = 0xa123)
_assert_(iif(1=0, 0xa123, 0x435267) = 0x435267)
_assert_(iif(1=null, 0xa123, 0x435267) = 0x435267)

_assert_(iif(1=1, cast('hello' as varchar(10)),'asdfghj') = 'hello')
_assert_(iif(1=0, cast('hello' as varchar(10)),'asdfghj') = 'asdfghj')
_assert_(iif(1=null, cast('hello' as varchar(10)),'asdfghj') = 'asdfghj')

_assert_(iif(1=1, cast(11 as bit),0) = 1)
_assert_(iif(1=0, cast(11 as bit),0) = 0)
_assert_(iif(1=null, cast(11 as bit),0) = 0)

_assert_(iif(1=1, cast(11 as tinyint),22) = 11)
_assert_(iif(1=0, cast(11 as tinyint),22) = 22)
_assert_(iif(1=null, cast(11 as tinyint),22) = 22)

_assert_(iif(1=1, cast(11 as smallint),22) = 11)
_assert_(iif(1=0, cast(11 as smallint),22) = 22)
_assert_(iif(1=null, cast(11 as smallint),22) = 22)

_assert_(iif(1=1, 11,22) = 11)
_assert_(iif(1=0, 11,22) = 22)
_assert_(iif(1=null, 11,22) = 22)

_assert_(iif(1=1, cast(11 as bigint),22) = 11)
_assert_(iif(1=0, cast(11 as bigint),22) = 22)
_assert_(iif(1=null, cast(11 as bigint),22) = 22)

_assert_(iif(1=1, cast(11.5 as money),22.6) = 11.5)
_assert_(iif(1=0, cast(11.5 as money),22.6) = 22.6)
_assert_(iif(1=null, cast(11.5 as money),22.6) = 22.6)

_assert_(iif(1=1, cast(11.5 as numeric(12,2)),22.6) = 11.5)
_assert_(iif(1=0, cast(11.5 as numeric(12,2)),22.6) = 22.6)
_assert_(iif(1=null, cast(11.5 as numeric(12,2)),22.6) = 22.6)

_assert_(iif(1=1, cast(11.5 as float),22.5) = 11.5)
_assert_(iif(1=0, cast(11.5 as float),22.5) = 22.5)
_assert_(iif(1=null, cast(11.5 as float),22.5) = 22.5)

_assert_(iif(1=1, cast('20120102' as date),'33330303') = '20120102')
_assert_(iif(1=0, cast('20120102' as date),'33330303') = '33330303')
_assert_(iif(1=null, cast('20120102' as date),'33330303') = '33330303')

_assert_(iif(1=1, cast('13:24' as time),'11:11') = '13:24')
_assert_(iif(1=0, cast('13:24' as time),'11:11') = '11:11')
_assert_(iif(1=null, cast('13:24' as time),'11:11') = '11:11')

_assert_(iif(1=1, cast('20120102' as datetime),'33330303') = '20120102')
_assert_(iif(1=0, cast('20120102' as datetime),'33330303') = '33330303')
_assert_(iif(1=null, cast('20120102' as datetime),'33330303') = '33330303')
_assert_error_(iif(1/0=null, cast('20120102' as datetime),'33330303'), 'SQLDATA_INT_DIVIDE_BY_ZERO')


-- ========== CHOOSE() ============

_assert_(choose(1, null, null) is null)
_assert_(choose(2, null, null) is null)
_assert_(choose(null, null, null) is null)

_assert_(choose(1, 0xa123, 0x435267) = 0xa123)
_assert_(choose(2, 0xa123, 0x435267) = 0x435267)
_assert_(choose(null, 0xa123, 0x435267) is null)

_assert_(choose(1, cast('hello' as varchar(10)),'asdfghj') = 'hello')
_assert_(choose(2, cast('hello' as varchar(10)),'asdfghj') = 'asdfghj')
_assert_(choose(null, cast('hello' as varchar(10)),'asdfghj') is null)

_assert_(choose(1, cast(11 as bit),0) = 1)
_assert_(choose(2, cast(11 as bit),0) = 0)
_assert_(choose(null, cast(11 as bit),0) is null)

_assert_(choose(1, cast(11 as tinyint),22) = 11)
_assert_(choose(2, cast(11 as tinyint),22) = 22)
_assert_(choose(null, cast(11 as tinyint),22) is null)

_assert_(choose(1, cast(11 as smallint),22) = 11)
_assert_(choose(2, cast(11 as smallint),22) = 22)
_assert_(choose(null, cast(11 as smallint),22) is null)

_assert_(choose(1, 11,22) = 11)
_assert_(choose(2, 11,22) = 22)
_assert_(choose(null, 11,22) is null)

_assert_(choose(1, cast(11 as bigint),22) = 11)
_assert_(choose(2, cast(11 as bigint),22) = 22)
_assert_(choose(null, cast(11 as bigint),22) is null)

_assert_(choose(1, cast(11.5 as money),22.6) = 11.5)
_assert_(choose(2, cast(11.5 as money),22.6) = 22.6)
_assert_(choose(null, cast(11.5 as money),22.6) is null)

_assert_(choose(1, cast(11.5 as numeric(12,2)),22.6) = 11.5)
_assert_(choose(2, cast(11.5 as numeric(12,2)),22.6) = 22.6)
_assert_(choose(null, cast(11.5 as numeric(12,2)),22.6) is null)

_assert_(choose(1, cast(11.5 as float),22.5) = 11.5)
_assert_(choose(2, cast(11.5 as float),22.5) = 22.5)
_assert_(choose(null, cast(11.5 as float),22.5) is null)

_assert_(choose(1, cast('20120102' as date),'33330303') = '20120102')
_assert_(choose(2, cast('20120102' as date),'33330303') = '33330303')
_assert_(choose(null, cast('20120102' as date),'33330303') is null)

_assert_(choose(1, cast('13:24' as time),'11:11') = '13:24')
_assert_(choose(2, cast('13:24' as time),'11:11') = '11:11')
_assert_(choose(null, cast('13:24' as time),'11:11') is null)

_assert_(choose(1, cast('20120102' as datetime),'33330303') = '20120102')
_assert_(choose(2, cast('20120102' as datetime),'33330303') = '33330303')
_assert_(choose(null, cast('20120102' as datetime),'33330303') is null)
_assert_error_(choose(1/0, cast('20120102' as datetime),'33330303'), 'SQLDATA_INT_DIVIDE_BY_ZERO')
_assert_error_(choose(1, cast('20120xxx102' as datetime),'33330303'), 'VARCHAR_CONVERT_DATETIME_FAILED')


-- ========== COALESCE() ============


_assert_(coalesce(NULL, NULL) is null)
_assert_(coalesce(NULL) is null)

_assert_(coalesce(cast(NULL as varbinary)) is null)
_assert_(coalesce(cast(NULL as varbinary), cast(NULL as varbinary)) is null)
_assert_(coalesce(0x11, 0x22, NULL) = 0x11)
_assert_(coalesce(NULL, 0x11, 0x22, NULL) = 0x11)
_assert_(coalesce(NULL, NULL, 0x11, 0x22, NULL, 0x12345678) = 0x11)

_assert_(coalesce(cast(NULL as varchar)) is null)
_assert_(coalesce(cast(NULL as varchar), cast(NULL as varchar)) is null)
_assert_(coalesce('hello', 'asdf', NULL) = 'hello')
_assert_(coalesce(NULL, 'hello', 'asdf', NULL) = 'hello')
_assert_(coalesce(NULL, NULL, 'hello', 'asdf', NULL) = 'hello')

_assert_(coalesce(cast(NULL as bit)) is null)
_assert_(coalesce(cast(NULL as bit), cast(NULL as bit)) is null)
_assert_(coalesce(cast(11 as bit), cast(22 as bit), NULL) = 1)
_assert_(coalesce(NULL, cast(0 as bit), cast(22 as bit), NULL) = 0)
_assert_(coalesce(NULL, NULL, cast(11 as bit), cast(22 as bit), NULL) = 1)

_assert_(coalesce(cast(NULL as tinyint)) is null)
_assert_(coalesce(cast(NULL as tinyint), cast(NULL as tinyint)) is null)
_assert_(coalesce(cast(11 as tinyint), cast(22 as tinyint), NULL) = 11)
_assert_(coalesce(NULL, cast(11 as tinyint), cast(22 as tinyint), NULL) = 11)
_assert_(coalesce(NULL, NULL, cast(11 as tinyint), cast(22 as tinyint), NULL) = 11)

_assert_(coalesce(cast(NULL as smallint)) is null)
_assert_(coalesce(cast(NULL as smallint), cast(NULL as smallint)) is null)
_assert_(coalesce(cast(11 as smallint), cast(22 as smallint), NULL) = 11)
_assert_(coalesce(NULL, cast(11 as smallint), cast(22 as smallint), NULL) = 11)
_assert_(coalesce(NULL, NULL, cast(11 as smallint), cast(22 as smallint), NULL) = 11)

_assert_(coalesce(cast(NULL as int)) is null)
_assert_(coalesce(cast(NULL as int), cast(NULL as int)) is null)
_assert_(coalesce(11, 22, NULL) = 11)
_assert_(coalesce(NULL, 11, 22, NULL) = 11)
_assert_(coalesce(NULL, NULL, 11, 22, NULL) = 11)

_assert_(coalesce(cast(NULL as bigint)) is null)
_assert_(coalesce(cast(NULL as bigint), cast(NULL as bigint)) is null)
_assert_(coalesce(cast(11 as bigint), cast(22 as bigint), NULL) = 11)
_assert_(coalesce(NULL, cast(11 as bigint), cast(22 as bigint), NULL) = 11)
_assert_(coalesce(NULL, NULL, cast(11 as bigint), cast(22 as bigint), NULL) = 11)

_assert_(coalesce(cast(NULL as money)) is null)
_assert_(coalesce(cast(NULL as money), cast(NULL as money)) is null)
_assert_(coalesce($11, $22, NULL) = $11)
_assert_(coalesce(NULL, $11, $22, NULL) = $11)
_assert_(coalesce(NULL, NULL, $11, $22, NULL) = $11)

_assert_(coalesce(cast(NULL as numeric(12,2))) is null)
_assert_(coalesce(cast(NULL as numeric(12,2)), cast(NULL as numeric(12,2))) is null)
_assert_(coalesce(cast(11 as numeric(12,2)), cast(22 as numeric(12,2)), NULL) = 11)
_assert_(coalesce(NULL, cast(11 as numeric(12,2)), cast(22 as numeric(12,2)), NULL) = 11)
_assert_(coalesce(NULL, NULL, cast(11 as numeric(12,2)), cast(22 as numeric(12,2)), NULL) = 11)

_assert_(cast(coalesce(cast(NULL as numeric(8,6)), NULL, cast(11 as numeric(2)), NULL, cast(22 as numeric(12,0))) as varchar(30)) = '11.000000')

_assert_(coalesce(NULL, NULL, 0x11, 0x22, NULL, 0x12345678) = 0x11)


_assert_(coalesce(cast(NULL as float)) is null)
_assert_(coalesce(cast(NULL as float), cast(NULL as float)) is null)
_assert_(coalesce(11e2, 22e2, NULL) = 11e2)
_assert_(coalesce(NULL, 11e2, 22e2, NULL) = 11e2)
_assert_(coalesce(NULL, NULL, 11e2, 22e2, NULL) = 11e2)

_assert_(coalesce(cast(NULL as date)) is null)
_assert_(coalesce(cast(NULL as date), cast(NULL as date)) is null)
_assert_(coalesce(cast('  20110203  ' as date), '19900101', NULL) = '  2011-02-03  ')
_assert_(coalesce(NULL, cast('  20110203  ' as date), '19900101', NULL) = '  2011-02-03  ')
_assert_(coalesce(NULL, NULL, cast('  20110203  ' as date), '19900101', NULL) = '  2011-02-03  ')

_assert_(coalesce(cast(NULL as time)) is null)
_assert_(coalesce(cast(NULL as time), cast(NULL as time)) is null)
_assert_(coalesce(cast('  20110203 10:30  ' as time), '19900101', NULL) = '  10:30   ')
_assert_(coalesce(NULL, cast('  20110203  10:30' as time), '19900101', NULL) = '  10:30   ')
_assert_(coalesce(NULL, NULL, cast('  20110203    10:30 ' as time), '19900101', NULL) = '  10:30   ')

_assert_(coalesce(cast(NULL as datetime)) is null)
_assert_(coalesce(cast(NULL as datetime), cast(NULL as datetime)) is null)
_assert_(coalesce(cast('  20110203 10:30  ' as datetime), '19900101', NULL) = ' 20110203 10:30   ')
_assert_(coalesce(NULL, cast('  20110203  10:30' as datetime), '19900101', NULL) = ' 20110203 10:30   ')
_assert_(coalesce(NULL, NULL, cast('  20110203    10:30 ' as datetime), '19900101', NULL) = '20110203  10:30   ')


-- ========== STR() ============

declare @a547 float = 244.1234567890123456789

_assert_('<' + str(@a547, 21) + '>'     = '<                  244>')

_assert_('<' + str(@a547) + '>'         = '<       244>')

_assert_('<' + str(@a547, 21, NULL) + '>'  = '<                  244>')

_assert_('<' + str(@a547, 21, 0) + '>'  = '<                  244>')
_assert_('<' + str(@a547, 21, 1) + '>'  = '<                244.1>')
_assert_('<' + str(@a547, 21, 5) + '>'  = '<            244.12346>')
_assert_('<' + str(@a547, 21, 10) + '>' = '<       244.1234567890>')
_assert_('<' + str(@a547, 21, 19) + '>' = '< 244.1234567890123515>')
_assert_('<' + str(@a547, 20, 19) + '>' = '<244.1234567890123515>')
_assert_('<' + str(@a547, 19, 19) + '>' = '<244.123456789012351>')
_assert_('<' + str(@a547, 18, 19) + '>' = '<244.12345678901235>')
_assert_('<' + str(@a547, 10, 19) + '>' = '<244.123457>')
_assert_('<' + str(@a547, 5, 19) + '>' = '<244.1>')
_assert_('<' + str(@a547, 4, 19) + '>' = '< 244>')
_assert_('<' + str(@a547, 3, 19) + '>' = '<244>')
_assert_('<' + str(@a547, 2, 19) + '>' = '<**>')
_assert_('<' + str(@a547, 1, 19) + '>' = '<*>')


_assert_(str(cast(NULL as float), 10, 1) is null)

_assert_(str(@a547, NULL, 0) is null)
_assert_(str(@a547,    0, 0) is null)
_assert_(str(@a547,   -1, 0) is null)

_assert_(str(@a547,  10, -1) is null)

_assert_(str(1f, 8000, 2) = space(7996) + '1.00')
_assert_(len(str(1f, 8000, 2)) = 8000)
_assert_error_(len(str(1f, 9000, 2)), 'VARCHAR_OVERFLOW')

_assert_('<' + str(1f, 7998, 2) + '>' = '<' + space(7994) + '1.00>')


-- ========== CONVERT(varchar, float, style) : format_FLOAT_to_VARCHAR() ============


set @a547 = 123.4567890123456789e-110

_assert_(convert(varchar, @a547) = '1.23457e-108')

_assert_(convert(varchar, @a547, 0) = '1.23457e-108')
_assert_(convert(varchar, @a547, 1) = '1.23457e-108')
_assert_(convert(varchar, @a547, 2) = '1.23457e-108')
_assert_(convert(varchar, @a547, cast(2 as float)) = '1.23457e-108')

_assert_(convert(varchar, cast(NULL as float), 0) is null)
_assert_(convert(varchar, @a547, cast(NULL as int)) = '1.23457e-108')


-- ========== CONVERT(varchar, float, style) : format_FLOAT_to_VARCHAR() error ============

set @a547  = 123.4567890123456789e-110

_assert_error_(convert(varchar(3), @a547, 2), 'DATA_FLOAT_CAST_VARCHAR_OVERFLOW')









