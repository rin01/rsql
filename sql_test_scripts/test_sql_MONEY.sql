

set language french

declare @MONEY_MAX money =  $999999999999999.9999
declare @MONEY_MIN money = $-999999999999999.9999

declare @SMALLINT_MAX smallint =  32767
declare @SMALLINT_MIN smallint = -32768

declare @INT_MAX int =  2147483647
declare @INT_MIN int = -2147483648

declare @BIGINT_MAX bigint =  9223372036854775807
declare @BIGINT_MIN bigint = -9223372036854775808


-- ========== TEST ============

_assert_(1=1)


-- ========== MONEY miscellaneous ============

_assert_(@MONEY_MAX = -(@MONEY_MIN + $0))

_assert_(@MONEY_MIN = -@MONEY_MAX)


_assert_(@MONEY_MAX - $0.9999 = $999999999999999)

_assert_(@MONEY_MAX * $1 = @MONEY_MAX)

_assert_(@MONEY_MAX * $-1 = @MONEY_MIN)

_assert_(@MONEY_MIN * $-1 = @MONEY_MAX)

_assert_(@MONEY_MIN + @MONEY_MAX = $0)

_assert_($123.123456789 = $123.1235)


-- ========== MONEY unary minus ============

_assert_(++++$4 = $4)
_assert_(-$4 = -$1 * $4)
_assert_($-4 = -$+1 * $4)
_assert_(- -$4 = $4)
_assert_(++- - -++$4 = -$4)
_assert_(++$4 = $4)

_assert_(-cast(NULL as money) is null)


-- ========== MONEY add ============

_assert_($1000000000000.1234 + $150.0001 = $1000000000150.1235)

_assert_($-1000 + $-150 = -$1150)

_assert_($1000 + null is null)
_assert_(null + $1000 is null)
_assert_(cast(null as money) + null is null)


-- ========== MONEY add max overflow ============

_assert_error_(@MONEY_MAX + $0.0001, 'NUMERIC_OVERFLOW')


-- ========== MONEY add max overflow ============

_assert_error_(@MONEY_MAX + @MONEY_MAX, 'NUMERIC_OVERFLOW')


-- ========== MONEY add min overflow ============

_assert_error_(@MONEY_MIN + $-0.0001, 'NUMERIC_OVERFLOW')


-- ========== MONEY add min overflow ============

_assert_error_(@MONEY_MIN + -@MONEY_MAX, 'NUMERIC_OVERFLOW')


-- ========== MONEY subtract ============

_assert_($1000 - $-150 = $1150)

_assert_($-1000 - $150 = $-1150)

_assert_($1000 - null is null)
_assert_(null - $1000 is null)
_assert_(cast(null as money) - null is null)


-- ========== MONEY subtract max overflow ============

_assert_error_(@MONEY_MAX - $-0.0001, 'NUMERIC_OVERFLOW')


-- ========== MONEY subtract max overflow ============

_assert_error_(@MONEY_MAX - @MONEY_MIN, 'NUMERIC_OVERFLOW')


-- ========== MONEY subtract min overflow ============

_assert_error_(@MONEY_MIN - $0.0001, 'NUMERIC_OVERFLOW')


-- ========== MONEY subtract min overflow ============

_assert_error_(@MONEY_MIN - @MONEY_MAX, 'NUMERIC_OVERFLOW')


-- ========== MONEY multiply ============

_assert_($1000 * $-150 = $-150000)

_assert_($-1000 * $150 = $-150000)

_assert_($1000 * null is null)
_assert_(null * $1000 is null)
_assert_(cast(null as money) * null is null)

_assert_((@MONEY_MAX / $2 - $0.0001 ) * $2/ $2 * $2 = @MONEY_MAX - $0.0001)
_assert_((@MONEY_MIN / $2 + $0.0001 ) * $2/ $2 * $2 = @MONEY_MIN + $0.0001)


-- ========== MONEY multiply max overflow ============

_assert_error_(@MONEY_MAX * $2, 'NUMERIC_OVERFLOW')


-- ========== MONEY multiply max overflow ============

_assert_error_(@MONEY_MIN / $-2 * $2, 'NUMERIC_OVERFLOW')


-- ========== MONEY multiply max overflow ============

_assert_error_(@MONEY_MAX * @MONEY_MAX, 'NUMERIC_OVERFLOW')


-- ========== MONEY multiply max overflow ============

_assert_error_(@MONEY_MIN * @MONEY_MIN, 'NUMERIC_OVERFLOW')


-- ========== MONEY multiply min overflow ============

_assert_error_(@MONEY_MIN * $2, 'NUMERIC_OVERFLOW')


-- ========== MONEY multiply min overflow ============

_assert_error_(@MONEY_MAX * @MONEY_MIN, 'NUMERIC_OVERFLOW')


-- ========== MONEY divide ============

_assert_($1000 / $3 = $333.3333)

_assert_($1000 / $-3 = -$333.3333)

_assert_($-1000 / $3 = $-333.3333)

_assert_($-1000 / $-3 = $333.3333)

_assert_($0 / $-3 = $0)

_assert_(@MONEY_MAX / $-1 = -@MONEY_MAX)
_assert_(@MONEY_MAX / -$1 = -@MONEY_MAX)

_assert_(@MONEY_MIN / @MONEY_MAX = $-1)

_assert_(@MONEY_MIN / @MONEY_MIN = $1)

_assert_(@MONEY_MAX / @MONEY_MIN = -$1)

_assert_($1000 / null is null)
_assert_(null / $1000 is null)
_assert_(cast(null as money) / null is null)


-- ========== MONEY divide max overflow ============

_assert_error_(@MONEY_MIN / $-.1, 'NUMERIC_OVERFLOW')


-- ========== MONEY divide by zero ============

_assert_error_($1/$0, 'NUMERIC_DEC_DIVISION_BY_ZERO')


-- ========== MONEY comp op ============

_assert_($122 = $122)

_assert_(not ($122 = $123))

_assert_(not $122 = $123)

_assert_null_( $1000 = null)
_assert_null_( null = $1000)
_assert_null_( cast(null as money) = null)


_assert_(not ($122 != $122))

_assert_($122 != $123)

_assert_null_( $1000 != null)
_assert_null_( null != $1000)
_assert_null_( cast(null as money) != null)


_assert_($123 > $122)

_assert_(not ($122 > $122))

_assert_($-121 > $-122)

_assert_(not ($-122 > $-122))

_assert_($1 > -$1)

_assert_(not (-$1 > $1))

_assert_null_( $1000 > null)
_assert_null_( null > $1000)
_assert_null_( cast(null as money) > null)


_assert_($123 >= $122)

_assert_($122 >= $122)

_assert_(not ($121 >= $122))

_assert_($-121 >= -$122)

_assert_($-122 >= $-122)

_assert_(not (-$123 >= $-122))

_assert_($1 >= -$1)

_assert_(not ($-1 >= $1))

_assert_null_( $1000 >= null)
_assert_null_( null >= $1000)
_assert_null_( cast(null as money) >= null)


_assert_($121 < $122)

_assert_(not ($122 < $122))

_assert_(-$123 < $-122)

_assert_(not (-$122 < -$122))

_assert_(not ($1 < -$1))

_assert_($-1 < $1)

_assert_null_( $1000 < null)
_assert_null_( null < $1000)
_assert_null_( cast(null as money) < null)


_assert_($121 <= $122)

_assert_($122 <= $122)

_assert_(not ($123 <= $122))

_assert_(-$123 <= -$122)

_assert_($-122 <= -$122)

_assert_(not ($-121 <= -$122))

_assert_(not ($1 <= $-1))

_assert_($-1 <= $1)

_assert_null_( $1000 <= null)
_assert_null_( null <= $1000)
_assert_null_( cast(null as money) <= null)


-- ========== MONEY is [not] null ============

_assert_(cast(NULL as money) is null)

_assert_(not $123 is null)


_assert_(not cast(NULL as money) is not null)

_assert_($123 is not null)


-- ========== MONEY [not] in list ============

_assert_($12 in ($12))

_assert_($12 in ($0, $12))

_assert_($12 in ($0, $12, $9))

_assert_($12 in ($0, NULL, $12, $9))

_assert_($12 in ($0, 12, NULL, $9))


_assert_(not $12 in ($0, $1, $9))

_assert_null_($12 in ($0, $1, NULL, $9))


_assert_(not $12 not in ($12))

_assert_(not $12 not in ($0, $12))

_assert_(not $12 not in ($0, $12, $9))

_assert_(not $12 not in ($0, NULL, $12, $9))

_assert_(not $12 not in ($0, $12, NULL, $9))


_assert_($12 not in ($0, $1, $9))

_assert_null_($12 not in ($0, $1, NULL, $9))


-- ========== MONEY cast ============

_assert_(cast($123 as char(10)) + 'x' = '123.00    x')
_assert_(cast($123 as char) = '123.00')
_assert_(cast($123.5555 as char) = '123.56')
_assert_(cast($123 as char(6)) = '123.00')
_assert_(cast($-123 as char(7)) = '-123.00')
_assert_(cast(@MONEY_MAX as char) = '1000000000000000.00')  -- not '999999999999999.9999'
_assert_(cast(@MONEY_MIN as char) = '-1000000000000000.00')
_assert_(cast(cast(null as money) as char) is null)

_assert_(cast($123 as varchar(10)) + 'x' = '123.00x')
_assert_(cast($123.45 as varchar(10)) + 'x' = '123.45x')
_assert_(cast($123 as varchar) = '123.00')
_assert_(cast($123 as varchar(6)) = '123.00')
_assert_(cast(-$123 as varchar(7)) = '-123.00')
_assert_(cast(@MONEY_MAX as varchar) = '1000000000000000.00')
_assert_(cast(@MONEY_MIN as varchar) = '-1000000000000000.00')
_assert_(cast(cast(null as money) as varchar) is null)

_assert_(cast($123 as bit) = 1)
_assert_(cast($123.45 as bit) = 1)
_assert_(cast($-123 as bit) = 1)
_assert_(cast($0 as bit) = 0)
_assert_(cast(cast(null as money) as bit) is null)

_assert_(cast( 255.9 as tinyint) = 255)
_assert_(cast($255.4 as tinyint) = 255)
_assert_(cast($255 as tinyint)   = 255)
_assert_(cast($254.8 as tinyint) = 255)
_assert_(cast( 254.8 as tinyint) = 254)
_assert_(cast($254.5 as tinyint) = 255)
_assert_(cast( 254.5 as tinyint) = 254)
_assert_(cast($0 as tinyint) = 0)
_assert_(cast($-0.4 as tinyint) = 0)
_assert_(cast(cast(null as money) as tinyint) is null)

_assert_(cast( 32767.9 as smallint) = 32767)
_assert_(cast($32767.4 as smallint) = 32767)
_assert_(cast($32767 as smallint) = 32767)
_assert_(cast($32766.5 as smallint) = 32767)  -- rounded
_assert_(cast( 32766.9 as smallint) = 32766)
_assert_(cast($-32768 as smallint) = -32768)
_assert_(cast($-32768.4 as smallint) = -32768)
_assert_(cast( -32768.9 as smallint) = -32768)
_assert_(cast(cast(null as money) as smallint) is null)

_assert_(cast($32766.5 as int) = 32767)  -- rounded
_assert_(cast( 2147483647.99 as int) = @INT_MAX)
_assert_(cast($2147483647.4 as int) = @INT_MAX)
_assert_(cast($2147483647 as int) = @INT_MAX)
_assert_(cast($2147483646.5 as int) = @INT_MAX)
_assert_(cast( 2147483646.999 as int) = @INT_MAX-1)
_assert_(cast($-2147483648 as int) = @INT_MIN)
_assert_(cast( -2147483647.9999 as int) = @INT_MIN + 1)
_assert_(cast($-2147483647.500 as int) = @INT_MIN)
_assert_(cast($-2147483647.800 as int) = @INT_MIN)
_assert_(cast($-2147483648.4 as int) = @INT_MIN)
_assert_(cast( -2147483648.99999 as int) = @INT_MIN)
_assert_(cast(cast(null as money) as int) is null)

_assert_(cast(@MONEY_MAX as bigint) = cast(1000000000000000 as bigint))
_assert_(cast(cast(@MONEY_MAX as numeric(19,4)) as bigint) = cast(999999999999999 as bigint))
_assert_(cast(@MONEY_MIN as bigint) = cast(-1000000000000000 as bigint))
_assert_(cast(cast(@MONEY_MIN as numeric(19,4)) as bigint) = cast(-999999999999999 as bigint))
_assert_(cast(cast(null as money) as bigint) is null)

_assert_(cast(@MONEY_MAX as money) = @MONEY_MAX)
_assert_(cast(@MONEY_MIN as money) = @MONEY_MIN)
_assert_(cast(cast(null as money) as money) is null)

_assert_(cast(@MONEY_MAX as numeric(19,4)) = @MONEY_MAX)
_assert_(cast(@MONEY_MIN as numeric(19,4)) = @MONEY_MIN)
_assert_(cast(cast(null as money) as numeric(12,2)) is null)

_assert_(cast(@MONEY_MAX as numeric(18,2)) = 1000000000000000.00)
_assert_(cast(@MONEY_MIN as numeric(18,2)) = -1000000000000000.00)
_assert_(cast(cast(null as money) as numeric(12,2)) is null)

_assert_(cast($12345 as numeric(5)) = 12345)
_assert_(cast($-12345 as numeric(5)) = -12345)
_assert_(cast(cast(null as money) as numeric(5)) is null)

_assert_(cast($-123.456 as varchar(30)) = '-123.46')
_assert_(cast(cast(@MONEY_MAX as float) as varchar(30)) = '1e+15')
_assert_(cast(cast(@MONEY_MIN as float) as varchar(30)) = '-1e+15')
_assert_(cast(cast(null as money) as float) is null)


-- ========== MONEY cast overflow ============

_assert_error_(cast($123 as char(2)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-123 as char(3)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($123 as varchar(2)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-123 as varchar(3)), 'NUMERIC_CAST_VARCHAR_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($256 as tinyint), 'NUMERIC_CAST_TINYINT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-0.5 as tinyint), 'NUMERIC_CAST_TINYINT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-1 as tinyint), 'NUMERIC_CAST_TINYINT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($32768 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($32767.5 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-32769 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-32768.5 as smallint), 'NUMERIC_CAST_SMALLINT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($2147483648 as int), 'NUMERIC_CAST_INT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($2147483647.5 as int), 'NUMERIC_CAST_INT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-2147483649 as int), 'NUMERIC_CAST_INT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-2147483648.5 as int), 'NUMERIC_CAST_INT_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($12345 as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== MONEY cast overflow ============

_assert_error_(cast($-12345 as numeric(4)), 'NUMERIC_OVERFLOW')


-- ========== MONEY overflow ============

_assert_error_(cast(cast(@MONEY_MAX as varchar) as money), 'NUMERIC_OVERFLOW')


-- ========== MONEY case when ============

_assert_(case when 1=1 then $10.55 end = $10.55)
_assert_(case when 1=4 then $10.55 end is null)
_assert_(case when 1=1 then $10.55 when 1=2 then $12345.6789 else $330000000000 end = $10.55)
_assert_(case when 1=1 then $10.55 when 1=2 then $12345.6789 end = $10.55)
_assert_(case when 1=4 then $10.55 when 2=2 then $12345.6789 else $330000000000 end = $12345.6789)
_assert_(case when 1=NULL then $10.55 when 2=2 then $12345.6789 else $330000000000 end = $12345.6789)
_assert_(case when 1=NULL then $10.55 when 2=NULL then $550000000000 else $330000000000 end = $330000000000)
_assert_(case when 1=NULL then $10.55 when 2=NULL then $12345.6789 end is null)




